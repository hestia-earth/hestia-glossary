# 📚 New Term

## Term(s) needed

<!-- 
List the term(s) you'd like us to add. The issue title must follow the following structure: "Name of glossary > add ....". 
If you want terms to be added to different glossaries, create a separate issue for each. 
-->

Add the following term(s) to the `<insert name of glossary>` glossary:

- [ ] Term 1
- [ ] Term 2
- [ ] etc.

## Reasons

<!-- 
Explain why you need the term(s). For example, add a link to the study where you found them, with a reference to the Table/paragraph where they are mentioned. 
-->


## Information

<!-- 
Look for the following information online:
- Agrovoc URIs can be found by searching for the terms [here](https://agrovoc.fao.org/browse/agrovoc/en/); please copy the URI at the bottom of the term page, not the url.
- Pubchem links can be found [here](https://pubchem.ncbi.nlm.nih.gov/). 
- feedipedia links [here](https://www.feedipedia.org/). 

If a link is not available or not relevant, please add a dash ("-"). 
-->

|                                  | `Term 1` | `Term 2` | `Term 3` | `Term 4` | `Term 5` |
|----------------------------------|----------|----------|----------|----------|----------|
| Wikipedia link                   |          |          |          |          |          |
| AGROVOC URI                      |          |          |          |          |          |
| PubChem link (chemicals)         |          |          |          |          |          |
| CAS number (chemicals)           |          |          |          |          |          |
| Scientific name (crops, animals) |          |          |          |          |          |
| Feedipedia link (crops)          |          |          |          |          |          |
| Definition                       |          |          |          |          |          |
| Synonyms                         |          |          |          |          |          |

/label ~"Type::feature"
/label ~"Priority::LOW"
/label ~"Tracking::Triage"
