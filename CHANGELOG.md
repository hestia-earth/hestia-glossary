# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.65.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.64.0...v0.65.0) (2025-03-04)


### ⚠ BREAKING CHANGES

* **operation:** rename `Oil pressing, machine unspecified` `Pressing, machine unspecified`
* **operation:** rename `Oil pressing, with screw press` `Pressing, with screw press`
* **operation:** rename `Oil pressing, with cold press` `Pressing, with cold press`
* **operation:** delete `Fruit pressing, machine unspecified`
* **model:** `harmonizedWorldSoilDatabaseV12` changes to
`harmonizedWorldSoilDatabaseV1-2`,
`hyde32` changes to
`hyde3-2` .
* **model:** For all LC-IMPACT terms, the `IMPACT`
in `name` is capitalised.
* **model:** `id` for `GFLI Database v2` changes from
`gfliV2` to `gfliDatabaseV2`

### Features

* **characterisedIndicator:** add lookup `pefTerm-methodModel-whiteList-v3-1` ([f024569](https://gitlab.com/hestia-earth/hestia-glossary/commit/f0245696011553018075c89baaea1c864cf521bd))
* **crop:** move processed terms for major crops to`processedFood` ([20dfa24](https://gitlab.com/hestia-earth/hestia-glossary/commit/20dfa24193bd0ebb52a2ae82bbbd7729e11eae79))
* **cropResidueManagement:** add `skipAggregatedNodeTypes` lookup ([74c348e](https://gitlab.com/hestia-earth/hestia-glossary/commit/74c348e63b7631f66d1c4f08cdd654904aac920e))
* **electricity:** rename lookup column for `cml2001Baseline` model ([0db0e53](https://gitlab.com/hestia-earth/hestia-glossary/commit/0db0e53c6f900f95ec015073062029af21d777db))
* **emission:** add `heavyMetalsToWater` terms ([0101aaf](https://gitlab.com/hestia-earth/hestia-glossary/commit/0101aaf2620a1dd25c0693f3b406e99c5deb5a4d))
* **emission:** add `nh3ToGroundwater` `nh3ToSurfaceWater` terms update lookups ([08b3117](https://gitlab.com/hestia-earth/hestia-glossary/commit/08b311769359923478c823c07743c5ab00601862))
* **emission:** add lookup `nEqMarineEutrophicationEnvironmentalFootprintV3` ([9416098](https://gitlab.com/hestia-earth/hestia-glossary/commit/941609847c107b1f270912374eecf860e20873c0))
* **emission:** add new term `CO, to air, natural vegetation burning` to glossary ([8fd1048](https://gitlab.com/hestia-earth/hestia-glossary/commit/8fd1048c8b5c3891c2dfb5bd573f3d96eb66ec89))
* **feedFoodAdditive:** add "ammonium phosphate" terms ([8bd8ab4](https://gitlab.com/hestia-earth/hestia-glossary/commit/8bd8ab4979200c3255a9b9c969dfc4a21f071f31)), closes [#1681](https://gitlab.com/hestia-earth/hestia-glossary/issues/1681)
* **feedFoodAdditive:** add "flavouring" terms ([02d126b](https://gitlab.com/hestia-earth/hestia-glossary/commit/02d126b1d56107ea5f56ace881a2ca57bea4505c)), closes [#1685](https://gitlab.com/hestia-earth/hestia-glossary/issues/1685)
* **feedFoodAdditive:** add `Formalin` ([23bae6b](https://gitlab.com/hestia-earth/hestia-glossary/commit/23bae6b9f96bb139c597688627681bb88c0c147a)), closes [#1653](https://gitlab.com/hestia-earth/hestia-glossary/issues/1653)
* **fuel:** rename lookup column for `cml2001Baseline` model ([9cf0984](https://gitlab.com/hestia-earth/hestia-glossary/commit/9cf0984e3048ad3187c0ae90ac81c19ec4878f50))
* **fuel:** rename lookup column for `cml2001Baseline` model ([9af0b91](https://gitlab.com/hestia-earth/hestia-glossary/commit/9af0b9102db23352250d49f20064164bbb6b4ee0))
* **model:** add `environmentalFootprintV2` `V3-1` update `environmentalFootprintV3` ([a01ec7e](https://gitlab.com/hestia-earth/hestia-glossary/commit/a01ec7e2762f291d963be65a02540c1ededd88bd))
* **model:** use `-` to represent special character `.` ([5c4901a](https://gitlab.com/hestia-earth/hestia-glossary/commit/5c4901aa1e8bfead3fdd6147c2a998d4300ca24f))
* **operation:** add `Saccharification` ([29430c5](https://gitlab.com/hestia-earth/hestia-glossary/commit/29430c5ab5ff7a9fecd6867bc2bdb135545674b0))
* **operation:** add multiple operations related to dry fractionation ([2b53739](https://gitlab.com/hestia-earth/hestia-glossary/commit/2b53739829ccdb418f78a623967ff06cf12085fa)), closes [#1700](https://gitlab.com/hestia-earth/hestia-glossary/issues/1700)
* **operation:** add multiple terms for "extruding", "cutting", "concentrating", "dehulling", "sieving" ([71925f4](https://gitlab.com/hestia-earth/hestia-glossary/commit/71925f468b3c91e6bf80629129b4f72eecd09374)), closes [#1708](https://gitlab.com/hestia-earth/hestia-glossary/issues/1708)
* **operation:** delete `Fruit pressing, machine unspecified` ([397d603](https://gitlab.com/hestia-earth/hestia-glossary/commit/397d603d6dc98e1ca9de7d51c442176cd94b2b62)), closes [#1709](https://gitlab.com/hestia-earth/hestia-glossary/issues/1709)
* **operation:** rename "oil pressing" terms "pressing" ([3c0f03d](https://gitlab.com/hestia-earth/hestia-glossary/commit/3c0f03d523b141e638a605c9fcc884cd95a5d9cb)), closes [#1709](https://gitlab.com/hestia-earth/hestia-glossary/issues/1709)
* **processedFood:** add `Blood meal, poultry`, `Blood meal, cattle`, `Blood meal, pig` ([fd9a761](https://gitlab.com/hestia-earth/hestia-glossary/commit/fd9a761a9b5f8651a86c0318e979f2cfdfcde59e)), closes [#1682](https://gitlab.com/hestia-earth/hestia-glossary/issues/1682)
* **processedFood:** add `Hemp, oil (crude)`, `Hemp, oil (refined)`, `Hemp, meal` ([61ad43a](https://gitlab.com/hestia-earth/hestia-glossary/commit/61ad43a8cfdfe3c30a2ecb5ef82452fad32fb08b)), closes [#1688](https://gitlab.com/hestia-earth/hestia-glossary/issues/1688)
* **processedFood:** add `Pasta waste meal` ([9f24e63](https://gitlab.com/hestia-earth/hestia-glossary/commit/9f24e6345ced8f30fa1128165d0c49f533392cc1))
* **processedFood:** add `Pasta, whole wheat, dry` and `Pasta, refined wheat, dry` ([b10a180](https://gitlab.com/hestia-earth/hestia-glossary/commit/b10a1803c1cf744c6f46a10e8549875c07e6fa7b))
* **processedFood:** add missing "wheat flour" terms ([9c269c7](https://gitlab.com/hestia-earth/hestia-glossary/commit/9c269c7956f147a8d3b924f8f493076349db8f92))
* **processingAid:** add "Diammonium phosphate" and "Ammonium suphate" terms ([db49aaa](https://gitlab.com/hestia-earth/hestia-glossary/commit/db49aaac633d331c7f8970bfef7ed219620bd3ed))
* **region:** add `region-resourceUse-environmentalFootprintV31WaterUse-factors-lookup` ([63bc1c0](https://gitlab.com/hestia-earth/hestia-glossary/commit/63bc1c07ed74a6e9c3d53927415ad52638e4e4aa))
* **waste:** add `Crude glycerol` ([38e9326](https://gitlab.com/hestia-earth/hestia-glossary/commit/38e93268c0c534c8f97dec6467dbe7126b95bd3a))
* **waste:** add `Lime mud waste` ([4a6f1a0](https://gitlab.com/hestia-earth/hestia-glossary/commit/4a6f1a0e845d8a16ce28aa19376bd871d9428c3b)), closes [#1708](https://gitlab.com/hestia-earth/hestia-glossary/issues/1708)
* **wasteManagement:** add terms for "incinerated" and "regenerated" ([f92b9b9](https://gitlab.com/hestia-earth/hestia-glossary/commit/f92b9b9daf7fe7da7a11f369d2ee43a27eadc171)), closes [#1707](https://gitlab.com/hestia-earth/hestia-glossary/issues/1707)


### Bug Fixes

* **emission:** minor errors in lookups and definitions ([b4e9c20](https://gitlab.com/hestia-earth/hestia-glossary/commit/b4e9c2048eda1f66b2fb99a4eca6823249ba1778))
* **fuel:** update `marineGasOil` `energyContent...` properties ([6c1b5af](https://gitlab.com/hestia-earth/hestia-glossary/commit/6c1b5afe2f1a635f552d95367e77092e582b20c4))
* **model:** error in `id` for `GFLI Database v2` ([2505a91](https://gitlab.com/hestia-earth/hestia-glossary/commit/2505a918fd9ccc673e1c6e79010999ae479ad137))
* **region:** remove `nan` values from faostat lookups ([898eae6](https://gitlab.com/hestia-earth/hestia-glossary/commit/898eae6286a572453ea74a0004d13ca18fa43cbc))
* **region:** update Geographies lookup files with new termids ([43e05d7](https://gitlab.com/hestia-earth/hestia-glossary/commit/43e05d7f39d6944ad7b939e91ebb962595d66473))

## [0.64.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.63.0...v0.64.0) (2025-02-18)


### ⚠ BREAKING CHANGES

* **landUseManagement:** Changes validator from on multicropping
terms from `sumMax100Group` to `sumIs100Group`
* **cropEstablishment:** Changes validator from `sumMax100Group`
to `sumIs100Group`

### Features

* add `skipAggregation` lookup to `soilType` and `usdaSoilType` ([e6cfdab](https://gitlab.com/hestia-earth/hestia-glossary/commit/e6cfdab67567b6d8fbdd8f2b40f7fdfa629d6deb)), closes [#1663](https://gitlab.com/hestia-earth/hestia-glossary/issues/1663)
* add `sumMax100Group` to `soilTexture`, `soilType`, `usdaSoilType` ([592de1e](https://gitlab.com/hestia-earth/hestia-glossary/commit/592de1eb27d940d198f8d650d9a54e66ff2cf244)), closes [#1662](https://gitlab.com/hestia-earth/hestia-glossary/issues/1662)
* **characterisedIndicator:** add lookups `pefTerm-normalisation-v3_1` `pefTerm-weighing-v3_1` ([25da256](https://gitlab.com/hestia-earth/hestia-glossary/commit/25da25632c5b2aafcdc61633b755b1ee4563f648))
* **crop:** add `sugarcaneStalk` duration, saplings, and density lookups ([ae21694](https://gitlab.com/hestia-earth/hestia-glossary/commit/ae216944d0c540fc3cd4b13bc6e0780dc24927ea)), closes [#1666](https://gitlab.com/hestia-earth/hestia-glossary/issues/1666)
* **crop:** add rooting depth lookups for multiple crops ([3d2742d](https://gitlab.com/hestia-earth/hestia-glossary/commit/3d2742da5d8c5f709a99f2e3b4007ed3839537bf))
* **crop:** add terms `Mustard, oil (crude)`; `Mustard, oil (cold-pressed)` ([24cb69a](https://gitlab.com/hestia-earth/hestia-glossary/commit/24cb69ac66e239a179c536a53bf6bc01fb0a6f9b)), closes [#1670](https://gitlab.com/hestia-earth/hestia-glossary/issues/1670)
* **cropEstablishment:** add `Not seeded` and change validator ([f59929c](https://gitlab.com/hestia-earth/hestia-glossary/commit/f59929c1cb1efdaaa6835f02e23718e14439c34b))
* **cropResidueManagement:** improve definition of `Residue incorporated` ([68dc9e4](https://gitlab.com/hestia-earth/hestia-glossary/commit/68dc9e47148475d769cbe65087c8c44fde1c62c3))
* **emission:** set `skipAggregation` to `false` for SOC mgmt. change ([134e924](https://gitlab.com/hestia-earth/hestia-glossary/commit/134e924fd1ad813c31ed83781648f80f4769af8f))
* **feedFoodAdditive:** add `ecoinventMapping` ([5b0aaa5](https://gitlab.com/hestia-earth/hestia-glossary/commit/5b0aaa511070de26e608e907da3fc278e270ddeb))
* **fertiliserBrandName:** add `Kendal`, `Kendal Te`, `Megafol`, and "FERTIACTYL" fertilisers ([5fd44c1](https://gitlab.com/hestia-earth/hestia-glossary/commit/5fd44c1d25526a074d5c67c2bf27eb9faeaa13bc)), closes [#1674](https://gitlab.com/hestia-earth/hestia-glossary/issues/1674)
* **forage:** add term `Pulse, fresh forage` to glossary ([75e36c7](https://gitlab.com/hestia-earth/hestia-glossary/commit/75e36c72285b49048ae7595e77cf0d464331142e))
* **landUseManagement:** add `Single cropped` and change validator ([ef13e15](https://gitlab.com/hestia-earth/hestia-glossary/commit/ef13e154ef7f9b4c85dc91ea0b8b4cd5ae2dcebd))
* **methodMeasurement:** add `Cobalt hexamine trichloride method` ([8b66935](https://gitlab.com/hestia-earth/hestia-glossary/commit/8b66935fe40caab13a4b3077afacd11dfcb5c957)), closes [#1664](https://gitlab.com/hestia-earth/hestia-glossary/issues/1664)
* **operation:** add `Storage, frozen, with freezer` ([14147c1](https://gitlab.com/hestia-earth/hestia-glossary/commit/14147c1c8f720a2b6bfb4adc3a84151a96418063)), closes [#1680](https://gitlab.com/hestia-earth/hestia-glossary/issues/1680)
* **operation:** add multiple destoning terms ([ad86ef6](https://gitlab.com/hestia-earth/hestia-glossary/commit/ad86ef6c456e626ac2318559a41bd603ee914046)), closes [#1680](https://gitlab.com/hestia-earth/hestia-glossary/issues/1680)
* **operation:** add storage term ([3e5b7eb](https://gitlab.com/hestia-earth/hestia-glossary/commit/3e5b7ebfa6c2ac7b4ebc689542e9750970112fc7))
* **organicFertiliser:** add dry matter values to glossary ([2264c5f](https://gitlab.com/hestia-earth/hestia-glossary/commit/2264c5f592335d97b492694229f37fdef1c0c841))
* **pesticideAI:** add `Benzothiodiazole` and `Propolis extract` ([44554cb](https://gitlab.com/hestia-earth/hestia-glossary/commit/44554cb391a4adf014947c016e7e249aa91b7722)), closes [#1675](https://gitlab.com/hestia-earth/hestia-glossary/issues/1675)
* **pesticideBrandName:** add `Focus Ultra` and `Corum+Dash` ([870c54a](https://gitlab.com/hestia-earth/hestia-glossary/commit/870c54a993a17ca8d7f4b95963a4c6a0092848a5)), closes [#1677](https://gitlab.com/hestia-earth/hestia-glossary/issues/1677)
* **processedFood:** add processed dairy terms ([2158da2](https://gitlab.com/hestia-earth/hestia-glossary/commit/2158da2753ba1e9872c1f8a8d4ad07543b7ae16b)), closes [#1654](https://gitlab.com/hestia-earth/hestia-glossary/issues/1654)
* **soilType:** change `skipAggregation` to FALSE for `Histosol` ([c03729f](https://gitlab.com/hestia-earth/hestia-glossary/commit/c03729f63de96f86c95077fe49af9e514515833c)), closes [#1671](https://gitlab.com/hestia-earth/hestia-glossary/issues/1671)


### Bug Fixes

* **landCover:** fix wrong `pefTermGrouping` for `ricePlantFlooded` ([9828884](https://gitlab.com/hestia-earth/hestia-glossary/commit/98288847be2102fd4245afe5ec78303a580b189b))
* **organicFertiliser:** rescale dry matter values for fertilisers in kg N ([0ceb16b](https://gitlab.com/hestia-earth/hestia-glossary/commit/0ceb16b55124eeb3c411513340c3a00845f7f286))
* **region:** fix Indonesia values not being aggregated into worl values ([57cf309](https://gitlab.com/hestia-earth/hestia-glossary/commit/57cf309b5c6d2ce8d7ea6747be2fe9e2384c0ce8))

## [0.63.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.62.1...v0.63.0) (2025-02-04)


### ⚠ BREAKING CHANGES

* **region:** rename `Consul`
`Consul (Village), Division No. 4, Saskatchewan, Canada`
* **standardsLabels:** change `unit` from `boolean` to `% area` for `standardsLabels` terms
* **liveAquaticSpecies:** rename `Roe` `Roe (kg mass)`
* **landUseManagement:** Delete `Non-productive phase (permanent crops).

### Features

* **crop:** fix lifespan lookups for `sugarcaneStalk` ([427a556](https://gitlab.com/hestia-earth/hestia-glossary/commit/427a5564dd24b7e315f6794e3c15cedd49976540)), closes [#1660](https://gitlab.com/hestia-earth/hestia-glossary/issues/1660)
* **fertiliserBrandName:** add `activeIngredient` for 9 terms ([7d38f64](https://gitlab.com/hestia-earth/hestia-glossary/commit/7d38f64e325388f40e2eef513f921394e282ac08))
* **fertiliserBrandName:** add active ingredients to remaining terms ([e6e4553](https://gitlab.com/hestia-earth/hestia-glossary/commit/e6e45539f023d6b18ca7bffc123a9f5bdd9f5a53)), closes [#1426](https://gitlab.com/hestia-earth/hestia-glossary/issues/1426)
* **fertiliserBrandName:** add missing `activeIngredients` ([1b028b5](https://gitlab.com/hestia-earth/hestia-glossary/commit/1b028b55aac4dd8a7bc5b55a5b9cbb57079b5d4e)), closes [#1426](https://gitlab.com/hestia-earth/hestia-glossary/issues/1426)
* **inorganicFertiliser:** add `sulphurContent` to `ammoniumThiosulphatekgN` ([40664ab](https://gitlab.com/hestia-earth/hestia-glossary/commit/40664ab23a980c5d466aa40262240dbb15011af4)), closes [#1651](https://gitlab.com/hestia-earth/hestia-glossary/issues/1651)
* **landUseManagement:** fix `productivePhase` terms and lookups ([258f796](https://gitlab.com/hestia-earth/hestia-glossary/commit/258f7968cc64e0143d6de3dc8ca8d560d90d0ff3)), closes [#1648](https://gitlab.com/hestia-earth/hestia-glossary/issues/1648)
* **liveAquaticSpecies:** add `Roe (number)` ([0410f6e](https://gitlab.com/hestia-earth/hestia-glossary/commit/0410f6e72f115fd476f81790cc1ac0662d05ce49)), closes [#1650](https://gitlab.com/hestia-earth/hestia-glossary/issues/1650)
* **liveAquaticSpecies:** rename `Roe` `Roe (kg mass)` ([2ba2b05](https://gitlab.com/hestia-earth/hestia-glossary/commit/2ba2b051d6a2774fcff10ee9b51b5f509fb86d85)), closes [#1650](https://gitlab.com/hestia-earth/hestia-glossary/issues/1650)
* **operation:** add `harvestingCropsWithPicker` ([eef626f](https://gitlab.com/hestia-earth/hestia-glossary/commit/eef626f4a4fca21b6edf2cbced3b5e0245cb4955)), closes [#1652](https://gitlab.com/hestia-earth/hestia-glossary/issues/1652)
* **otherInorganicChemical:** add `Sodium hydroxide (other inorganic chemical)` ([2448aaa](https://gitlab.com/hestia-earth/hestia-glossary/commit/2448aaa296635596083d9df00aeff9c4de8f45ef)), closes [#1655](https://gitlab.com/hestia-earth/hestia-glossary/issues/1655)
* **region:** rename `Consul` `Consul (Village), Division No. 4, Saskatchewan, Canada` ([f2924dd](https://gitlab.com/hestia-earth/hestia-glossary/commit/f2924ddcf5ebcb2c4a40a662e4a117bbe7c1970b))
* **standardsLabels:** change `unit` from `boolean` to `% area` ([14fb47d](https://gitlab.com/hestia-earth/hestia-glossary/commit/14fb47dbaca22958e3a470bc33fdd9834940cafe)), closes [#1658](https://gitlab.com/hestia-earth/hestia-glossary/issues/1658)
* **waste:** add `Chemical waste` ([32cdf2a](https://gitlab.com/hestia-earth/hestia-glossary/commit/32cdf2aca1a4dfbf021ef4ad2f6ecf138d8674eb)), closes [#1646](https://gitlab.com/hestia-earth/hestia-glossary/issues/1646)


### Bug Fixes

* **landUseManagement:** set `valueType` lookup to `number` for `Tillage depth` ([e88102b](https://gitlab.com/hestia-earth/hestia-glossary/commit/e88102b9bd15501fc41db51485ac20d672ef522b)), closes [#1657](https://gitlab.com/hestia-earth/hestia-glossary/issues/1657)
* **pesticideBrandName:** ensure `activeIngredient` `value` is in w/w % for FCT brands ([757a74f](https://gitlab.com/hestia-earth/hestia-glossary/commit/757a74f7225ce733dca8c047aa0b74b79a49b94a)), closes [#1636](https://gitlab.com/hestia-earth/hestia-glossary/issues/1636)
* **standardsLabels:** set `valueType` lookup value to `boolean` for consistency with `unit` ([cbe678e](https://gitlab.com/hestia-earth/hestia-glossary/commit/cbe678e6c9d1b7e224af042335023f6f731bf98d))

### [0.62.1](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.62.0...v0.62.1) (2025-01-24)


### Features

* **crop:** add term `Radish, leaf` to glossary ([cbe10e5](https://gitlab.com/hestia-earth/hestia-glossary/commit/cbe10e511db2cf8401e2196f3e81f84d21fce8e2))
* **emission:** restrict `productTermIdsAllowed` for `N2O, to air, waste treatment, direct` ([2621586](https://gitlab.com/hestia-earth/hestia-glossary/commit/26215860e10085494fe303f07fefe2839eb0bc2f)), closes [#1645](https://gitlab.com/hestia-earth/hestia-glossary/issues/1645)
* **landCover:** add `sumIs100Group` lookup ([7ec8e47](https://gitlab.com/hestia-earth/hestia-glossary/commit/7ec8e4712d886ebed69c9dac9c74c4470b76455e))
* **measurement:** add new term `Rainfall erosivity` to glossary ([c4afa8a](https://gitlab.com/hestia-earth/hestia-glossary/commit/c4afa8af35f0d7592b82b65b03ae88a846879a95))


### Bug Fixes

* **methodMeasurement:** fix error in dash in `BaCl2‐TEA method` ([1988c85](https://gitlab.com/hestia-earth/hestia-glossary/commit/1988c851e846eb41268a38afb08b9829aba7e480))

## [0.62.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.61.1...v0.62.0) (2025-01-21)


### ⚠ BREAKING CHANGES

* **model:** rename `Nutrient budget` `Mass balance model`

### Features

* **animalProduct:** add `Shell, freshwater snails`, `Shell, oyster`, `Meat, oyster (without shell)` ([1533ba1](https://gitlab.com/hestia-earth/hestia-glossary/commit/1533ba1e8f834d7ef92bc530fd0869452ea30a49))
* **crop:** add missing  `correspondingSeedTermIds` lookup to  `Maize, grain` ([61d8eb3](https://gitlab.com/hestia-earth/hestia-glossary/commit/61d8eb3543aba37a849d3c73ae5486888f77c113)), closes [#1622](https://gitlab.com/hestia-earth/hestia-glossary/issues/1622)
* **cropEstablishment:** add `subClassOf` to `fertigation` terms ([0fa55df](https://gitlab.com/hestia-earth/hestia-glossary/commit/0fa55df9bf4e77e36872cec43d222010f1d64e4a))
* **cropEstablishment:** add `transplanting-seeding` `sumMax100Group` lookup ([286b6ef](https://gitlab.com/hestia-earth/hestia-glossary/commit/286b6effe607977fb2568aab996b1d8bd95d65f9))
* **inorganicFertiliser:** add `Inorganic Calcium fertiliser, unspecified (kg Ca)` ([5b6febb](https://gitlab.com/hestia-earth/hestia-glossary/commit/5b6febbd6bd1852665e7a95d455c4bdc2167f1bd)), closes [#916](https://gitlab.com/hestia-earth/hestia-glossary/issues/916)
* **landCover:** add `Water body` ([9612f42](https://gitlab.com/hestia-earth/hestia-glossary/commit/9612f429e59cc7753df7f9597b2bfc6dba567ede))
* **landCover:** update `IPCC_LAND_USE_CATEGORY` lookup values ([7de11ba](https://gitlab.com/hestia-earth/hestia-glossary/commit/7de11ba4b5c2919f6a7a270562fe8d2795e81571))
* **liveAquaticSpecies:** add multiple finfish species ([1892f8d](https://gitlab.com/hestia-earth/hestia-glossary/commit/1892f8da8aa775d0490fb52fde39334d8d69b867)), closes [#1619](https://gitlab.com/hestia-earth/hestia-glossary/issues/1619)
* **liveAquaticSpecies:** add one finfish and two oyster species ([b653288](https://gitlab.com/hestia-earth/hestia-glossary/commit/b65328857b23e32f05de4e839cc2e5108f87c998))
* **material:** add `Plastic, recycled` ([40d3494](https://gitlab.com/hestia-earth/hestia-glossary/commit/40d3494300ae21abdb70f440a116e8a8e8e1c29a)), closes [#1620](https://gitlab.com/hestia-earth/hestia-glossary/issues/1620)
* **material:** add `Plastic, virgin` ([197a278](https://gitlab.com/hestia-earth/hestia-glossary/commit/197a278a5747f00310ba4391e1321f5d2089216e))
* **measurement:** add "Aluminum" and "Plant available aluminum" terms ([0d91e2a](https://gitlab.com/hestia-earth/hestia-glossary/commit/0d91e2a34729c30a28b0f449d3f4fa88fd9be5f7)), closes [#1641](https://gitlab.com/hestia-earth/hestia-glossary/issues/1641)
* **methodMeasurement:** add `BaCl2‐TEA method`, `Laser diffraction method`, `KCl extraction method` ([a597dd7](https://gitlab.com/hestia-earth/hestia-glossary/commit/a597dd74596315f57e7cc8ca71b468f4f5b925f9)), closes [#1642](https://gitlab.com/hestia-earth/hestia-glossary/issues/1642)
* **model:** add `MapBiomas` ([b7763bf](https://gitlab.com/hestia-earth/hestia-glossary/commit/b7763bf905cca5b2b4f16b01f649ae8946e291ee))
* **model:** rename `Nutrient budget` `Mass balance model` and add synonyms ([0c6f98a](https://gitlab.com/hestia-earth/hestia-glossary/commit/0c6f98a97a031c1a0b56dd5af82f38422f23050f)), closes [#1621](https://gitlab.com/hestia-earth/hestia-glossary/issues/1621)
* **organicFertiliser:** add `Hydrolysed protein (kg mass)` and `Hydrolysed protein (kg N)` ([5d69ac8](https://gitlab.com/hestia-earth/hestia-glossary/commit/5d69ac82b6cdb0a7aa3c44726ad3a306a82d642b))
* **pesticideBrandName:** add `Primextra Gold` ([af1a1a9](https://gitlab.com/hestia-earth/hestia-glossary/commit/af1a1a9c0d5b964e72af1bdf80806e423c786ccd)), closes [#1585](https://gitlab.com/hestia-earth/hestia-glossary/issues/1585)
* **pesticideBrandName:** add multiple terms ([0f078d5](https://gitlab.com/hestia-earth/hestia-glossary/commit/0f078d5dd39095c4781949bb861ccdec5e3c4cbc)), closes [#1294](https://gitlab.com/hestia-earth/hestia-glossary/issues/1294)
* **processedFood:** add `Algae, oil` ([a37cdf8](https://gitlab.com/hestia-earth/hestia-glossary/commit/a37cdf89f641195bda3c2b9dbe1d35fa65c22618)), closes [#1634](https://gitlab.com/hestia-earth/hestia-glossary/issues/1634)
* **processedFood:** add `Cassava, flour` ([6166243](https://gitlab.com/hestia-earth/hestia-glossary/commit/616624344da088b493996dfd4e5bf4cab98e1d2a)), closes [#1640](https://gitlab.com/hestia-earth/hestia-glossary/issues/1640)
* **processedFood:** add `Fish by-product oil` and `Finfish by-product oil` ([27f8905](https://gitlab.com/hestia-earth/hestia-glossary/commit/27f890580081d5071aae4a426eca031c1eabcb2f)), closes [#1633](https://gitlab.com/hestia-earth/hestia-glossary/issues/1633)
* **processedFood:** add `Lupin, flour` ([d31517b](https://gitlab.com/hestia-earth/hestia-glossary/commit/d31517bc3e0aed7c2022d086b6698a4270624b58))
* **processedFood:** add `Milk, cow, evaporated` and `Milk, cow, pasteurised` ([70811a9](https://gitlab.com/hestia-earth/hestia-glossary/commit/70811a9413cf708d05abd029b93f9ad76b427664)), closes [#1631](https://gitlab.com/hestia-earth/hestia-glossary/issues/1631)
* **property:** add `HANDLE_LAND_SHARE_SEPARATELY` lookup ([6fe9705](https://gitlab.com/hestia-earth/hestia-glossary/commit/6fe97055f9db61f659f0520df4832a6e6c77a9cb))


### Bug Fixes

* **emission:** errors in `typesAllowed` lookup ([e361d45](https://gitlab.com/hestia-earth/hestia-glossary/commit/e361d45b813d6d3038d29876cec4db3964f2ce53))
* **fuel:** update `abioticResourceDepletionFossilFuelsCml2001Baseline` for Peat terms to True ([fa50247](https://gitlab.com/hestia-earth/hestia-glossary/commit/fa5024736f304a1a6a16bb956f7a3b4e4de34c01))

## [0.61.1](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.61.0...v0.61.1) (2025-01-07)

### Features

* **landCove:** add 4 terms for missing parent corine+ classes needs validation.

## [0.61.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.60.0...v0.61.0) (2024-12-24)


### ⚠ BREAKING CHANGES

* **liveAquaticSpecies:** delete `allowedExcretaKgMassTermIds` lookup
* **liveAquaticSpecies:** delete `allowedExcretaKgNTermIds` lookup
* **liveAquaticSpecies:** delete `allowedExcretaKgVsTermIds` lookup
* **animalProduct:** delete `allowedExcretaKgMassTermIds` lookup
* **animalProduct:** delete `allowedExcretaKgNTermIds` lookup
* **animalProduct:** delete `allowedExcretaKgVsTermIds` lookup
* **liveAnimal:** delete `allowedExcretaKgMassTermIds` lookup
* **liveAnimal:** delete `allowedExcretaKgNTermIds` lookup
* **liveAnimal:** delete `allowedExcretaKgVsTermIds` lookup
* **liveAnimal:** delete `recommendedExcretaKgMassTermIds` lookup
* **liveAnimal:** delete `recommendedExcretaKgNTermIds` lookup
* **liveAnimal:** delete `recommendedExcretaKgVsTermIds` lookup
* **system:** change unit from `% area` to `%` for `indoorAnimalSystem`
* **system:** change unit from `% area` to `%` for `batteryCageSystem`
* **system:** change unit from `% area` to `%` for `colonyCageSystem`
* **system:** change unit from `% area` to `%` for `barnSystem`
* **system:** change unit from `% area` to `%` for `freeRangeSystem`
* **system:** change unit from `% area` to `%` for `feedlotSystem`
* **system:** change unit from `% area` to `%` for `confinedPastureSystem`
* **system:** change unit from `% area` to `%` for `hillyPastureSystem`
* **system:** change unit from `% area` to `%` for `openRangeSystem`
* **system:** change unit from `% area` to `%` for `outdoorAnimalSystem`
* **system:** change unit from `% area` to `%` for `farrowingCrateSystem`
* **resourceUse**: replace all `Land transformation, from...` terms with equivalent terms without `from ...`. Example: `Land transformation, from cropland, 20 year average, during Cycle` is now `Land transformation, 20 year average, during Cycle`.

### Features

* **animalProduct:** add "excretaTermIds" lookup values to "bee" terms ([667df59](https://gitlab.com/hestia-earth/hestia-glossary/commit/667df59955d1fa2644489a4a29a19a6bb4c69fa9))
* **animalProduct:** delete "allowedExcretaTermIds"  lookups ([a6bd8c1](https://gitlab.com/hestia-earth/hestia-glossary/commit/a6bd8c10bca004ed0374563a714f3310ed7a7d09)), closes [#1606](https://gitlab.com/hestia-earth/hestia-glossary/issues/1606)
* **crop:** add default properties for `Wheat, gluten` to `crop-property-default-lookup` ([9dfb82f](https://gitlab.com/hestia-earth/hestia-glossary/commit/9dfb82f11a6807ec0b55c0346d3007c5a77ec612)), closes [#1609](https://gitlab.com/hestia-earth/hestia-glossary/issues/1609)
* **crop:** update crop-property-lookup ([b853b11](https://gitlab.com/hestia-earth/hestia-glossary/commit/b853b11ea5312ce90f3e6dc90e451a588d75dfe6)), closes [#1610](https://gitlab.com/hestia-earth/hestia-glossary/issues/1610)
* **forage:** update forage-property-lookup ([4616c4b](https://gitlab.com/hestia-earth/hestia-glossary/commit/4616c4ba01609c66d9aceefdeaea2eb915caf977)), closes [#1610](https://gitlab.com/hestia-earth/hestia-glossary/issues/1610)
* **liveAnimal:** add "excretaTermIds" lookup values to "bee" terms ([cbf3d38](https://gitlab.com/hestia-earth/hestia-glossary/commit/cbf3d385dce67af4b5c3c0c12cb68801889055a2))
* **liveAnimal:** delete "allowedExcretaTermIds" and "recommendedExcretaTermIds" lookups ([7ecc1f2](https://gitlab.com/hestia-earth/hestia-glossary/commit/7ecc1f290d590fde75a7a68651fdce682194c75d)), closes [#1606](https://gitlab.com/hestia-earth/hestia-glossary/issues/1606)
* **liveAquaticSpecies:** add "excretaTermIds" lookup values to `Roe` ([471401f](https://gitlab.com/hestia-earth/hestia-glossary/commit/471401f1cb0cf78522c89f7e07284e60bcbce2dc))
* **liveAquaticSpecies:** delete "allowedExcretaTermIds" lookups ([9f656c2](https://gitlab.com/hestia-earth/hestia-glossary/commit/9f656c215f48dadc667d16f8252b55e11ecb1d95)), closes [#1606](https://gitlab.com/hestia-earth/hestia-glossary/issues/1606)
* **measurement:** add nitrite terms to the glossary ([b20e9b2](https://gitlab.com/hestia-earth/hestia-glossary/commit/b20e9b2c3beb3da404ee0486debdf9fb49f08423))
* **model:** add `Nutrient budget` ([a4d51b5](https://gitlab.com/hestia-earth/hestia-glossary/commit/a4d51b55c846c7355a96e8a2b1e1790246f8285a)), closes [#596](https://gitlab.com/hestia-earth/hestia-glossary/issues/596)
* **model:** add `PestLCI Consensus Model` ([ffce28f](https://gitlab.com/hestia-earth/hestia-glossary/commit/ffce28fc5db930fecb7cb2cd68ab984813b19175)), closes [#1611](https://gitlab.com/hestia-earth/hestia-glossary/issues/1611)
* **pesticideAI:** add synonyms to `pyriproxyfen` and `hexythiazox` ([c2ea5b4](https://gitlab.com/hestia-earth/hestia-glossary/commit/c2ea5b4ace32c931fa7560705e68b1ec7131788e))
* **processedFood:** update processedFood-property-lookup ([953200c](https://gitlab.com/hestia-earth/hestia-glossary/commit/953200cdb0b3f237fd4b0393c312071bf4f16ec3)), closes [#1610](https://gitlab.com/hestia-earth/hestia-glossary/issues/1610)
* **region:** add `region-liveAnimal-hoursWorkedPerDay-lookup` ([52a3e25](https://gitlab.com/hestia-earth/hestia-glossary/commit/52a3e2584e67f051db834bf13c40c0de86eefb21)), closes [#1615](https://gitlab.com/hestia-earth/hestia-glossary/issues/1615)
* **resourceUse:** simplify `Land transformation...` terms ([45a64f9](https://gitlab.com/hestia-earth/hestia-glossary/commit/45a64f98173c9a4514f8bf2b9fcd1283d3ef972e))
* **soilAmendment:** add `aminoAcidsUnspecifiedSoilAmendment` ([0c2723a](https://gitlab.com/hestia-earth/hestia-glossary/commit/0c2723a4ee527ab5e4796bb321f4bf51d0a92270)), closes [#1612](https://gitlab.com/hestia-earth/hestia-glossary/issues/1612)
* **system:** add `sumIs100Group` lookup ([9b88cef](https://gitlab.com/hestia-earth/hestia-glossary/commit/9b88cef95c099234de1f8303d3398ae7bce4890d)), closes [#1617](https://gitlab.com/hestia-earth/hestia-glossary/issues/1617)
* **system:** change unit from `% area` to `%` for terms whose value should represent a % of time ([472c417](https://gitlab.com/hestia-earth/hestia-glossary/commit/472c4179a8647c98a269ca04ad0e2c5c705367f2)), closes [#1398](https://gitlab.com/hestia-earth/hestia-glossary/issues/1398)
* **system:** remove `liveAnimal-activityCoefficient-ipcc2019-lookup` from generic terms ([25609e9](https://gitlab.com/hestia-earth/hestia-glossary/commit/25609e952bafd0ebc69b2e3375d51aa720a7b8fd)), closes [#1399](https://gitlab.com/hestia-earth/hestia-glossary/issues/1399)


### Bug Fixes

* **crop:** update property lookups ([71eb937](https://gitlab.com/hestia-earth/hestia-glossary/commit/71eb937cf2da35d26a598ba8fc75b6eb4436d90a))
* **emission:** fix errors in `scope` lookup for industrial process terms ([2b83dcd](https://gitlab.com/hestia-earth/hestia-glossary/commit/2b83dcd3ab762069ca1a7775c4ea64077bbc9a5a))
* **emission:** set `Ozone depletion potential` to `0` for `CFC-114a` ([f946eb5](https://gitlab.com/hestia-earth/hestia-glossary/commit/f946eb5483b26809b0ed7544bd362784e21dc31b))
* **emission:** set `Ozone depletion potential` to `0` for `N2O` ([8371474](https://gitlab.com/hestia-earth/hestia-glossary/commit/837147447319c8c95eeb5784d8f43ed82a66b408))
* **forage:** update property lookups ([9ba321d](https://gitlab.com/hestia-earth/hestia-glossary/commit/9ba321de78bf4a67149a5de9620caadfd42d6ed9))
* **processedFood:** update property lookups ([4aaecee](https://gitlab.com/hestia-earth/hestia-glossary/commit/4aaecee291d7b25933fb5077297bd003ac038a55))
* **property:** add multiple values to `feedipediaName` lookup for "NDF content" and "ADF content" ([39818f6](https://gitlab.com/hestia-earth/hestia-glossary/commit/39818f6bebbcb896fb92b5396c6db186855c14ea))
* **property:** update `feedipediaName` lookup for "NDF content" and "ADF content" ([7345e46](https://gitlab.com/hestia-earth/hestia-glossary/commit/7345e463c038b2761b05c56174f18e284b814174)), closes [#1610](https://gitlab.com/hestia-earth/hestia-glossary/issues/1610)
* **region:** add missing `EF_NOX` and `EF_P_C2` factors for `Indonesia` ([3606bd0](https://gitlab.com/hestia-earth/hestia-glossary/commit/3606bd097d9f5ad4d692b1e3fa3829ceec4d01f6))

## [0.60.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.59.0...v0.60.0) (2024-12-10)


### ⚠ BREAKING CHANGES

* **animalManagement:** Delete `Animal breed` term
* **region:** Renames `Granite` to
`Granite (County), Montana, United States`,
`Sandstone` to `Sandstone (Shire), Western Australia, Australia`
and `Shalë` to `Shalë (Commune), Shkodrës, Shkodër, Albania`
* **liveAquaticSpecies:** rename `excretaKgMassTermId` lookup `excretaKgMassTermIds`
* **liveAquaticSpecies:** rename `excretaKgNTermId` lookup `excretaKgNTermIds`
* **liveAquaticSpecies:** rename `excretaKgVsTermId` lookup `excretaKgVsTermIds`
* **animalProduct:** rename `excretaKgMassTermId` lookup `excretaKgMassTermIds`
* **animalProduct:** rename `excretaKgNTermId` lookup `excretaKgNTermIds`
* **animalProduct:** rename `excretaKgVsTermId` lookup `excretaKgVsTermIds`
* **liveAnimal:** rename `excretaKgMassTermId` lookup `excretaKgMassTermIds`
* **liveAnimal:** rename `excretaKgNTermId` lookup `excretaKgNTermIds`
* **liveAnimal:** rename `excretaKgVsTermId` lookup `excretaKgVsTermIds`

### Features

* **animalProduct:** update "excreta" lookups for "beef" terms to handle `feedlotSystem` ([fff1ab0](https://gitlab.com/hestia-earth/hestia-glossary/commit/fff1ab0ab899572806d3dc8ee355856570c2e149)), closes [#1544](https://gitlab.com/hestia-earth/hestia-glossary/issues/1544)
* **crop:** add default properties for `Pea, protein concentrate` ([09eb46b](https://gitlab.com/hestia-earth/hestia-glossary/commit/09eb46b4416bcb0127b70f9c1a53973d49f48e15)), closes [#1591](https://gitlab.com/hestia-earth/hestia-glossary/issues/1591)
* **emission:** fix errors in `inDefaultHestiaSystemBoundary` lookup ([0d51bfa](https://gitlab.com/hestia-earth/hestia-glossary/commit/0d51bfa2775fb85145d6e0ce68f20d8ed4a3c90f)), closes [#1594](https://gitlab.com/hestia-earth/hestia-glossary/issues/1594)
* **fertiliserBrandName:** add remaining FCT fertilisers ([d9a71d8](https://gitlab.com/hestia-earth/hestia-glossary/commit/d9a71d8bc63350397bfe9df07462c5106b274b4f))
* **liveAnimal:** update "excreta" lookups for "beef cattle" terms to handle `feedlotSystem` ([c07156f](https://gitlab.com/hestia-earth/hestia-glossary/commit/c07156fbaad704283d206c9c004f26cb7efc9824)), closes [#1544](https://gitlab.com/hestia-earth/hestia-glossary/issues/1544)
* **liveAquaticSpecies:** add `Tench` ([f1d5ff6](https://gitlab.com/hestia-earth/hestia-glossary/commit/f1d5ff6496f3baba23868c52090c6c0c7ddf146d))
* **liveAquaticSpecies:** rename "excretaKgXTermId" lookups "excretaKgXTermIds" ([2066860](https://gitlab.com/hestia-earth/hestia-glossary/commit/2066860463a527622d5c6d06ca31151507167256)), closes [#1592](https://gitlab.com/hestia-earth/hestia-glossary/issues/1592)
* **material:** add material terms from FCT ([2fb1d8b](https://gitlab.com/hestia-earth/hestia-glossary/commit/2fb1d8b143c6554e97227007c5974eeacf60512a)), closes [#1496](https://gitlab.com/hestia-earth/hestia-glossary/issues/1496)
* **model:** add new terms describing land occupation models ([a076f87](https://gitlab.com/hestia-earth/hestia-glossary/commit/a076f875498bf9ba73c46f25822d3400426fb80a)), closes [#1588](https://gitlab.com/hestia-earth/hestia-glossary/issues/1588)
* **property:** add `GAP_FILL_TO_MANAGEMENT` lookup ([7a4a633](https://gitlab.com/hestia-earth/hestia-glossary/commit/7a4a6339a52b2e2f7c921ec6957ad22196796d27))
* **property:** add `Weight at death` ([7e79f8b](https://gitlab.com/hestia-earth/hestia-glossary/commit/7e79f8b8e9fb1fde7b5b3ac774242c6c9cea1abe))
* **property:** allow `energyContent` for termType `organicFertiliser` ([855efc3](https://gitlab.com/hestia-earth/hestia-glossary/commit/855efc32627d995ee5a2776d6dac2d64665b76d2)), closes [#1595](https://gitlab.com/hestia-earth/hestia-glossary/issues/1595)
* **property:** allow add additional `termTypesAllowed` to `...content...` terms ([01671c9](https://gitlab.com/hestia-earth/hestia-glossary/commit/01671c9db8d23ee63ac40639657db0f7c3ce0efa)), closes [#1596](https://gitlab.com/hestia-earth/hestia-glossary/issues/1596)
* **resourceUse:** add land transformation terms for "other land" ([f2176c8](https://gitlab.com/hestia-earth/hestia-glossary/commit/f2176c86c70f36617d6df0efe82ce157f32f02ae))
* **resourceUse:** create model mapping lookup ([359df78](https://gitlab.com/hestia-earth/hestia-glossary/commit/359df78bc0803745ee52c1a385bc451d935dc1be))
* **seed:** add `linkedImpactAssessmentTermId` lookup ([7599a54](https://gitlab.com/hestia-earth/hestia-glossary/commit/7599a540e879a05a6c407a909c58114abf7cb5ad)), closes [#1593](https://gitlab.com/hestia-earth/hestia-glossary/issues/1593)
* **seed:** add `minimum` and `maximum` lookup ([802c62e](https://gitlab.com/hestia-earth/hestia-glossary/commit/802c62eed737d549d45cf9d25ec00618b0106fdc)), closes [#1603](https://gitlab.com/hestia-earth/hestia-glossary/issues/1603)


### Bug Fixes

* **animalManagement:** delete `Animal breed` term as redundant ([6a0b45f](https://gitlab.com/hestia-earth/hestia-glossary/commit/6a0b45fd394954afc68d6ff1d64a3376863de82e)), closes [#1601](https://gitlab.com/hestia-earth/hestia-glossary/issues/1601)
* **crop:** fix terms with `Saplings_required` lookup and `is_Plantation`=FALSE ([e258479](https://gitlab.com/hestia-earth/hestia-glossary/commit/e2584790ea3441c27edbd6d598e15658ec0e88a7)), closes [#1600](https://gitlab.com/hestia-earth/hestia-glossary/issues/1600)
* **fertiliserBrandName:** error in default property term ([2734813](https://gitlab.com/hestia-earth/hestia-glossary/commit/27348135e70aa6f13161d0c9521562fb7d219703))
* **property:** add additional `termTypesAllowed` to `...content` terms ([bc420dc](https://gitlab.com/hestia-earth/hestia-glossary/commit/bc420dc489f5101ec5541420c3f7700df94c408b))
* **seed:** remove non-generic `preparedPlantation` terms and `generic` from the other terms ([33304a5](https://gitlab.com/hestia-earth/hestia-glossary/commit/33304a55dab9591ac3336435686183c7a03d2310))


* **region:** use full name for regions duplicating with material ([8193a19](https://gitlab.com/hestia-earth/hestia-glossary/commit/8193a197ead2ab7bc2eca34c6f5929d8c124b838))

## [0.59.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.58.0...v0.59.0) (2024-11-26)


### ⚠ BREAKING CHANGES

* **seed:** Rename `Seed, depreciated amount Per Cycle` `Seed, depreciated amount per Cycle`.

### Features

* **crop:** add `maximumCycleDuration` lookup ([4e1e9cd](https://gitlab.com/hestia-earth/hestia-glossary/commit/4e1e9cdaaaf2e0daa48069a2f4d686fe480e809e))
* **electricity:** add `abioticResourceDepletionFossilFuelsCml2001Baseline` lookup column with fixed column order ([53330a7](https://gitlab.com/hestia-earth/hestia-glossary/commit/53330a7b7d4963670f83343adff4bfab97f5a64d))
* **fuel:** add lookup column `abioticResourceDepletionFossilFuelsCml2001Baseline` update 51 terms ([f0f2962](https://gitlab.com/hestia-earth/hestia-glossary/commit/f0f29620d4b55d49c1171c55cc4d22960980c83a))
* **material:** add new terms and `abioticResourceDepletionMineralsAndMetalsCml2001Baseline` column ([ea40143](https://gitlab.com/hestia-earth/hestia-glossary/commit/ea40143a971911ab28eb6e8583d0526f23300c1e))
* **methodMeasurement:** add `Thermometer`, `Gravimetric method`, and `K2SO4 extraction method` ([349320a](https://gitlab.com/hestia-earth/hestia-glossary/commit/349320aea0ec54806fb94c25f6ad86ad5704b4a9)), closes [#1581](https://gitlab.com/hestia-earth/hestia-glossary/issues/1581)
* **otherInorganicChemical:** add `abioticResourceDepletionMineralsAndMetalsCml2001Baseline` column ([87ccf2e](https://gitlab.com/hestia-earth/hestia-glossary/commit/87ccf2ee06b61b3a9f364d2d31b0216a06fafa21))
* **pesticideBrandName:** add remaining pesticides from FCT ([14f2710](https://gitlab.com/hestia-earth/hestia-glossary/commit/14f27104b21bfc43046d1ec570dc67ab0d5d1f64)), closes [#1492](https://gitlab.com/hestia-earth/hestia-glossary/issues/1492)
* **property:** update `termTypesAllowed` lookup for "element content" terms ([d476765](https://gitlab.com/hestia-earth/hestia-glossary/commit/d476765e749515fefd800fcd559f5f5fae97ec0a)), closes [#1573](https://gitlab.com/hestia-earth/hestia-glossary/issues/1573)
* **region:** add `region-crop-cropGroupingFaostatProduction-areaHarvestedUpTo20YearExpansion.csv` lookup ([6b2635e](https://gitlab.com/hestia-earth/hestia-glossary/commit/6b2635e5166ac177b1c88b9df36e5293bcff4f85))
* **region:** add `region-faostatArea-UpTo20YearExpansion` lookup ([cc5a915](https://gitlab.com/hestia-earth/hestia-glossary/commit/cc5a915552e066c64a0773d626b2515799752564))
* **resourceUse:** add 2 `resourceUseMineralsAndMetals` `DuringCycle` / `InputsProduction` terms ([30fa49c](https://gitlab.com/hestia-earth/hestia-glossary/commit/30fa49c1e0904128753ba94313955b81191475ae))
* **resourceUse:** add new `resourceUseEnergyDepletion` terms ([233e9e6](https://gitlab.com/hestia-earth/hestia-glossary/commit/233e9e685ce2511acb9992e0c162257ac50524f8))
* **soilAmendment:** add `abioticResourceDepletionMineralsAndMetalsCml2001Baseline` lookup column ([a26a031](https://gitlab.com/hestia-earth/hestia-glossary/commit/a26a0316298c958274f3cdbad9a5b46468da2060))


### Bug Fixes

* **characterisedIndicator:** use `0` rather than `-` ([9dca9d7](https://gitlab.com/hestia-earth/hestia-glossary/commit/9dca9d79e7332a584bd9f832a1ec8b3f11869670)), closes [#1413](https://gitlab.com/hestia-earth/hestia-glossary/issues/1413)
* **crop:** error in `cropGroupingFaostatArea` for `Jojoba seed` ([427fe4b](https://gitlab.com/hestia-earth/hestia-glossary/commit/427fe4bec47adb74b35ce8786de5cec4365b0232))
* **emission:** use `0` instead of `not required` ([439580b](https://gitlab.com/hestia-earth/hestia-glossary/commit/439580b6072028348198d33f950647f5a1236c31))
* **seed:** rename `Seed, depreciated amount Per Cycle` ([870b1c8](https://gitlab.com/hestia-earth/hestia-glossary/commit/870b1c8433702ac6783624670b1ea6f29db5dfe1)), closes [#1566](https://gitlab.com/hestia-earth/hestia-glossary/issues/1566)

## [0.58.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.57.0...v0.58.0) (2024-11-12)


### ⚠ BREAKING CHANGES

* **fuel:** rename `seicCode` lookup `sicCode`.
* **pesticideAI:** rename `[C(E)]-N-[(2-Chloro-5-thiazolyl)methyl]-N'-methyl-N''-nitroguanidine` `Clothianidin`

### Features

* **emission:** use `value = 0` rather than `dataState = not required` for lookups ([2542fef](https://gitlab.com/hestia-earth/hestia-glossary/commit/2542fef90ab18e8185bda1f892e5aa2086c6b7ba)), closes [#1413](https://gitlab.com/hestia-earth/hestia-glossary/issues/1413)
* **fuel:** add `Light fuel oil` ([2b0a246](https://gitlab.com/hestia-earth/hestia-glossary/commit/2b0a24610c7f053d2ec2b59153410df53fb349c2)), closes [#1548](https://gitlab.com/hestia-earth/hestia-glossary/issues/1548)
* **fuel:** rename `seicCode` lookup `sicCode` ([73c2c2b](https://gitlab.com/hestia-earth/hestia-glossary/commit/73c2c2b687cf138bfaba4ffbe15d975b25cfcca4)), closes [#1556](https://gitlab.com/hestia-earth/hestia-glossary/issues/1556)
* **operation:** add `waterRegime` lookup ([4dde1e1](https://gitlab.com/hestia-earth/hestia-glossary/commit/4dde1e1f5546cd685fbf6c7a76f381776a25cc7e)), closes [#1552](https://gitlab.com/hestia-earth/hestia-glossary/issues/1552)
* **pesticideAI:** add `Diflufenzopyr-sodium` ([ab3f56e](https://gitlab.com/hestia-earth/hestia-glossary/commit/ab3f56eae5d1aeeca5edb38cc66e7dd7351fbb5e)), closes [#1571](https://gitlab.com/hestia-earth/hestia-glossary/issues/1571)
* **pesticideAI:** change name for `CAS-210880-92-5` to `Clothianidin` ([47d4986](https://gitlab.com/hestia-earth/hestia-glossary/commit/47d49862e18b8194119a49ae772864b763d6a248)), closes [#1555](https://gitlab.com/hestia-earth/hestia-glossary/issues/1555)
* **processedFood:** add `Alcohol` ([8fdcdff](https://gitlab.com/hestia-earth/hestia-glossary/commit/8fdcdff0f8497c21d5de568c3be068a0c84bee3a)), closes [#1562](https://gitlab.com/hestia-earth/hestia-glossary/issues/1562)
* **processedFood:** add `Barley, grain (malt)` ([eb1ef95](https://gitlab.com/hestia-earth/hestia-glossary/commit/eb1ef953d8b1583b64466ce839b72971b37d2df0)), closes [#1562](https://gitlab.com/hestia-earth/hestia-glossary/issues/1562)
* **processedFood:** add `Orange, juice` ([8c0a94b](https://gitlab.com/hestia-earth/hestia-glossary/commit/8c0a94b7d6b857b7f5c78c56555bca0dd3c8fedf)), closes [#1570](https://gitlab.com/hestia-earth/hestia-glossary/issues/1570)
* **processedFood:** add `Whiskey` ([c63979f](https://gitlab.com/hestia-earth/hestia-glossary/commit/c63979fa7c43a609bc2898afb0ca9dffccf4950a)), closes [#1562](https://gitlab.com/hestia-earth/hestia-glossary/issues/1562)
* **property:** update `termTypesAllowed` lookup for multiple terms ([dbf7720](https://gitlab.com/hestia-earth/hestia-glossary/commit/dbf7720726c6e323ff9310a7ef046d8c14b6fca4)), closes [#1558](https://gitlab.com/hestia-earth/hestia-glossary/issues/1558)
* **seed:** add `depreciatedAmountPerCycle` terms ([805f2a5](https://gitlab.com/hestia-earth/hestia-glossary/commit/805f2a578c7f56165a2ed1fb18664f564e16e52d)), closes [#1566](https://gitlab.com/hestia-earth/hestia-glossary/issues/1566)
* **seed:** add `preparedPlantationGeneric` ([d8690c1](https://gitlab.com/hestia-earth/hestia-glossary/commit/d8690c1bef51bc8dd550568c9e05c8661d3ecb92)), closes [#1561](https://gitlab.com/hestia-earth/hestia-glossary/issues/1561)


### Bug Fixes

* **characterisedIndicator:** remove ability to set `aggregatedModels` ([299280e](https://gitlab.com/hestia-earth/hestia-glossary/commit/299280e72ab6484ba30eade76b17bc79b5fd3661))
* **emission:** add missing GWPs and use `0` rather than `not required` for lookups ([6dc5b52](https://gitlab.com/hestia-earth/hestia-glossary/commit/6dc5b52f3bd85c3582b27577699521f477997339)), closes [#1413](https://gitlab.com/hestia-earth/hestia-glossary/issues/1413)
* **excreta:** set `skipAggregation` to `FALSE` for all terms ([d745025](https://gitlab.com/hestia-earth/hestia-glossary/commit/d74502509b366d250c266d17ffeafec15536f6bc))
* **fuel:** move source from property `sd` to property `description` for `Olive, seed (fuel)` ([a3caa26](https://gitlab.com/hestia-earth/hestia-glossary/commit/a3caa26d0dca90846fa1dbd8738d49ec233fea63))

## [0.57.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.56.2...v0.57.0) (2024-10-29)


### ⚠ BREAKING CHANGES

* **liveAnimal:** delete `kgDayMilkForFeedingOffspring` lookup
* **liveAnimal:** rename `Goat, wethers` `Goat, wether`
* **operation:** rename `layingPlasticMulchByHand` `layingSheetMulchByHand`.
* **operation:** rename `layingPlasticMulchMachineUnspecified` `layingSheetMulchMachineUnspecified`.
* **operation:** rename `removingPlasticMulchByHand` `removingSheetMulchByHand`.
* **operation:** rename `removingPlasticMulchMachineUnspecified` `removingSheetMulchMachineUnspecified`.
* **otherOrganicChemical:** delete `Intralipid`
* **veterinaryDrug:** move `Thiram` from `veterinaryDrug` to `pesticideAI`

### Features

* **crop:** add lifespan lookups to `sugarcaneStalk` ([8677b62](https://gitlab.com/hestia-earth/hestia-glossary/commit/8677b6252fcb1801150867a1e76f7d4875e0b8a0)), closes [#1529](https://gitlab.com/hestia-earth/hestia-glossary/issues/1529)
* **feedFoodAdditive:** add `hasEnergyContent` lookup ([4ce2f62](https://gitlab.com/hestia-earth/hestia-glossary/commit/4ce2f62eacaa1c34b5131f611b1888b89750c049)), closes [#1533](https://gitlab.com/hestia-earth/hestia-glossary/issues/1533)
* **landCover:** add "Other land" ([193f0d3](https://gitlab.com/hestia-earth/hestia-glossary/commit/193f0d3c1f9c2c05d4b2c82a7c3e6259da32bbb1))
* **landCover:** add `FAOSTAT_LAND_AREA_CATEGORY` lookup ([46d8f6e](https://gitlab.com/hestia-earth/hestia-glossary/commit/46d8f6e75d5eb58d7e95d2dbda2e0c6a14ec05f0))
* **landUseManagement:** add new term 'biodegradableFilmMulching' ([5569102](https://gitlab.com/hestia-earth/hestia-glossary/commit/5569102dd927e3e8acf926fd46edcb8a291a6e4d)), closes [#1536](https://gitlab.com/hestia-earth/hestia-glossary/issues/1536)
* **liveAnimal:** add `ipcc2019MilkYieldPerAnimalTermId` lookup ([8e085d2](https://gitlab.com/hestia-earth/hestia-glossary/commit/8e085d2a07dc7d8762862fedef3bd3efca2d4edd)), closes [#1546](https://gitlab.com/hestia-earth/hestia-glossary/issues/1546)
* **liveAnimal:** delete `averagePregnancyRate lookup` ([f8c44b3](https://gitlab.com/hestia-earth/hestia-glossary/commit/f8c44b3954bf15344c5d9ec7669b9a89789f562a)), closes [#646](https://gitlab.com/hestia-earth/hestia-glossary/issues/646)
* **liveAnimal:** delete `kgDayMilkForFeedingOffspring` lookup ([e5161ce](https://gitlab.com/hestia-earth/hestia-glossary/commit/e5161ce7aadc2a01c045a89d3424596411bc63db)), closes [#1540](https://gitlab.com/hestia-earth/hestia-glossary/issues/1540)
* **material:** add `plasticBiodegradable` terms ([4d41c4d](https://gitlab.com/hestia-earth/hestia-glossary/commit/4d41c4d0fafea7b533efa917cb8fbf07b0d6b619)), closes [#1549](https://gitlab.com/hestia-earth/hestia-glossary/issues/1549)
* **operation:** add `Sowing seeds, with aircraft` ([fcde192](https://gitlab.com/hestia-earth/hestia-glossary/commit/fcde1926c55c9852c1255f06f62443b9754aa58a))
* **operation:** add `synonyms` to `Ensiling, machine unspecified` ([8902a18](https://gitlab.com/hestia-earth/hestia-glossary/commit/8902a18a524a6ac0575b00e270cde7eba08cd18c))
* **operation:** add terms for polishing, husking, and germinating seeds ([046ed7c](https://gitlab.com/hestia-earth/hestia-glossary/commit/046ed7cf21839b608a6acc2f4d6a3d28e0e61655)), closes [#1551](https://gitlab.com/hestia-earth/hestia-glossary/issues/1551)
* **operation:** rename `...plasticMulch` terms to `...sheetMulch` ([49bab08](https://gitlab.com/hestia-earth/hestia-glossary/commit/49bab081748be2df2b9de475469e52d3d6e70abc)), closes [#1538](https://gitlab.com/hestia-earth/hestia-glossary/issues/1538)
* **organicFertiliser:** add `Rapeseed, meal...` and `Rice, bran...` ([16e11fb](https://gitlab.com/hestia-earth/hestia-glossary/commit/16e11fb502c0c1695b919fe2c32e7803d0ba6338)), closes [#1542](https://gitlab.com/hestia-earth/hestia-glossary/issues/1542)
* **otherOrganicChemical:** remove the term `Intralipid` ([beb83a5](https://gitlab.com/hestia-earth/hestia-glossary/commit/beb83a52747e369001cd703877714c34822bbcd2)), closes [#1535](https://gitlab.com/hestia-earth/hestia-glossary/issues/1535)
* **pesticideAI:** add `Nitrile compounds, unspecified` ([86326a4](https://gitlab.com/hestia-earth/hestia-glossary/commit/86326a49f3f1032fbe6b80140c5428c10f78e464))
* **pesticideBrandName:** add `Pas Tor Blend` and `Pexan` ([d5a1bee](https://gitlab.com/hestia-earth/hestia-glossary/commit/d5a1bee722828cacd1c0da15140a827dabfecb7d))
* **property:** add `productTermIdsAllowed` lookup to "Pregnancy rate" terms ([4cd2989](https://gitlab.com/hestia-earth/hestia-glossary/commit/4cd29895283299eedffc0f8672cae0d6e23023da)), closes [#1539](https://gitlab.com/hestia-earth/hestia-glossary/issues/1539)
* **region:** add `liveAnimal-milkFatContent` and `live-animal-milkTrueProteinContent` lookups ([93139e9](https://gitlab.com/hestia-earth/hestia-glossary/commit/93139e97951c80bbbb7b82db52ee8be57105cc76)), closes [#1543](https://gitlab.com/hestia-earth/hestia-glossary/issues/1543)
* **region:** add `region-liveAnimal-pregnancyRateTotal-lookup` ([3f0e17c](https://gitlab.com/hestia-earth/hestia-glossary/commit/3f0e17c41483344c8e9fac94a0f2a29afcf450d0)), closes [#646](https://gitlab.com/hestia-earth/hestia-glossary/issues/646)
* **region:** add missing region level data for animal head lookups ([ef79866](https://gitlab.com/hestia-earth/hestia-glossary/commit/ef798665445e26820981345e04334f8d5c8e2856))
* **region:** update `region-liveAnimal-milkYieldPerAnimal-lookup` ([31732fa](https://gitlab.com/hestia-earth/hestia-glossary/commit/31732fa10a3098085453ed09d86e046e3830f344)), closes [#1537](https://gitlab.com/hestia-earth/hestia-glossary/issues/1537) [#647](https://gitlab.com/hestia-earth/hestia-glossary/issues/647)
* **region:** update region-liveAnimal lookups with values for continents and sub-continents ([2a3b228](https://gitlab.com/hestia-earth/hestia-glossary/commit/2a3b2289ffd64afac04470d9c9eba1c72d582ec6)), closes [#1537](https://gitlab.com/hestia-earth/hestia-glossary/issues/1537)
* **region:** update values in `region-liveAnimal-pregnancyRateTotal-lookup` ([3a9f148](https://gitlab.com/hestia-earth/hestia-glossary/commit/3a9f14886dbfe822cb30ee9f4c9d9b9064c374ec)), closes [#1541](https://gitlab.com/hestia-earth/hestia-glossary/issues/1541)


### Bug Fixes

* **animalProduct:** replace `goatWethers` with `goatWether` in `allowedLiveAnimalTermIds` lookup ([69aba3c](https://gitlab.com/hestia-earth/hestia-glossary/commit/69aba3c9bdd504a4a2e52f68f889204184715b39))
* **emission:** differentiate fossil and non-fossil in `co2EqGwp100Ipcc2021` lookup ([461a721](https://gitlab.com/hestia-earth/hestia-glossary/commit/461a721dd19a28109b1204c41315ef5821105d4e))
* **emission:** replace `goatWethers` with `goatWether` in `productTermIdsAllowed` lookup ([da90b27](https://gitlab.com/hestia-earth/hestia-glossary/commit/da90b27afca4bcc89efd7693b63016aff9e82d6f))
* **liveAnimal:** rename `Goat, wethers` `Goat, wether` ([e3f6461](https://gitlab.com/hestia-earth/hestia-glossary/commit/e3f64615a6a0093ce6bbe081121237f02aa59d29))
* **model:** allow `RAINS model` for `terrestrialAcidificationPotentialIncludingFateAverageEurope` ([47b1b80](https://gitlab.com/hestia-earth/hestia-glossary/commit/47b1b8015854ffc40b2ece54adaed98f2e5758da))
* **veterinaryDrug:** move `Thiram` to `pesticideAI` ([d1342b9](https://gitlab.com/hestia-earth/hestia-glossary/commit/d1342b97403459baa9f2d405c7f7149b245a36b4)), closes [#1524](https://gitlab.com/hestia-earth/hestia-glossary/issues/1524)

### [0.56.2](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.56.1...v0.56.2) (2024-10-15)


### Features

* **fertiliserBrandName:** add fertilisers from Farm Carbon Calculator ([2011655](https://gitlab.com/hestia-earth/hestia-glossary/commit/2011655d96a5b9f141da2c537adb606a6b4f5cdb))
* **fertiliserBrandName:** add seven new terms ([16d9f67](https://gitlab.com/hestia-earth/hestia-glossary/commit/16d9f67a14ae43a9c9f2dc713745616f2df858a5)), closes [#1523](https://gitlab.com/hestia-earth/hestia-glossary/issues/1523) [#1511](https://gitlab.com/hestia-earth/hestia-glossary/issues/1511) [#1510](https://gitlab.com/hestia-earth/hestia-glossary/issues/1510)
* **inorganicFertiliser:** add `Cyanoguanidine (kg N)` ([842a872](https://gitlab.com/hestia-earth/hestia-glossary/commit/842a872d46d8bdd0188f3c792e2b5ad44c4475c5)), closes [#1530](https://gitlab.com/hestia-earth/hestia-glossary/issues/1530)
* **landCover:** add `pefTermGrouping` lookup ([a55d466](https://gitlab.com/hestia-earth/hestia-glossary/commit/a55d466f9efcca4e54d732cb6b16e3eb2566bbcc))
* **landCover:** add `Sea or ocean`, `Agri-food processor`, and `Food retailer` ([f076a9d](https://gitlab.com/hestia-earth/hestia-glossary/commit/f076a9d1e3fd3cddf92bd31bc79f5d0c31518056))
* **landCover:** add new term `desert` ([f046c4d](https://gitlab.com/hestia-earth/hestia-glossary/commit/f046c4dd90f65dd8831fd84550df3e0cfb135a6d))
* minor fixes related to EU PEF models ([8dc440a](https://gitlab.com/hestia-earth/hestia-glossary/commit/8dc440a3e2656cbba464a7c7131d6d268a3dbded))
* **operation:** add two "incorporating residue" terms ([39317c4](https://gitlab.com/hestia-earth/hestia-glossary/commit/39317c49e9524c9ec6fea8faf75d7888f6231684))
* **region:** add eu PEF land lookups ([e83eb7c](https://gitlab.com/hestia-earth/hestia-glossary/commit/e83eb7c557109209f0fac15bb3d495ea61305d68))


### Bug Fixes

* **soilAmendment:** change `PRACTICE_INCREASING_C_INPUT` lookup value for `basalt` ([aff70b7](https://gitlab.com/hestia-earth/hestia-glossary/commit/aff70b73c12c33a8c917068a7795169a924dafcb))

### [0.56.1](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.56.0...v0.56.1) (2024-10-04)

## [0.56.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.55.0...v0.56.0) (2024-10-01)


### ⚠ BREAKING CHANGES

* **ecoClimateZone:** Rename `BIOMASS_EQUILIBRIUM_KG_C_HECTARE_ANNUAL_CROPS`
`AG_BIOMASS_EQUILIBRIUM_KG_C_HECTARE_ANNUAL_CROPS`
* **ecoClimateZone:** Rename `BIOMASS_EQUILIBRIUM_KG_C_HECTARE_COCONUT`
`AG_BIOMASS_EQUILIBRIUM_KG_C_HECTARE_COCONUT`
* **ecoClimateZone:** Rename `BIOMASS_EQUILIBRIUM_KG_C_HECTARE_FOREST`
`AG_BIOMASS_EQUILIBRIUM_KG_C_HECTARE_FOREST`
* **ecoClimateZone:** Rename `BIOMASS_EQUILIBRIUM_KG_C_HECTARE_GRASSLAND`
`AG_BIOMASS_EQUILIBRIUM_KG_C_HECTARE_GRASSLAND`
* **ecoClimateZone:** Rename `BIOMASS_EQUILIBRIUM_KG_C_HECTARE_JATROPHA`
`AG_BIOMASS_EQUILIBRIUM_KG_C_HECTARE_JATROPHA`
* **ecoClimateZone:** Rename `BIOMASS_EQUILIBRIUM_KG_C_HECTARE_JOJOBA`
`AG_BIOMASS_EQUILIBRIUM_KG_C_HECTARE_JOJOBA`
* **ecoClimateZone:** Rename `BIOMASS_EQUILIBRIUM_KG_C_HECTARE_NATURAL_FOREST`
`AG_BIOMASS_EQUILIBRIUM_KG_C_HECTARE_NATURAL_FOREST`
* **ecoClimateZone:** Rename `BIOMASS_EQUILIBRIUM_KG_C_HECTARE_OIL_PALM`
`AG_BIOMASS_EQUILIBRIUM_KG_C_HECTARE_OIL_PALM`
* **ecoClimateZone:** Rename `BIOMASS_EQUILIBRIUM_KG_C_HECTARE_OLIVE`
`AG_BIOMASS_EQUILIBRIUM_KG_C_HECTARE_OLIVE`
* **ecoClimateZone:** Rename `BIOMASS_EQUILIBRIUM_KG_C_HECTARE_ORCHARD`
`AG_BIOMASS_EQUILIBRIUM_KG_C_HECTARE_ORCHARD`
* **ecoClimateZone:** Rename `BIOMASS_EQUILIBRIUM_KG_C_HECTARE_OTHER`
`AG_BIOMASS_EQUILIBRIUM_KG_C_HECTARE_OTHER`
* **ecoClimateZone:** Rename `BIOMASS_EQUILIBRIUM_KG_C_HECTARE_PLANTATION_FOREST`
`AG_BIOMASS_EQUILIBRIUM_KG_C_HECTARE_PLANTATION_FOREST`
* **ecoClimateZone:** Rename `BIOMASS_EQUILIBRIUM_KG_C_HECTARE_RUBBER`
`AG_BIOMASS_EQUILIBRIUM_KG_C_HECTARE_RUBBER`
* **ecoClimateZone:** Rename `BIOMASS_EQUILIBRIUM_KG_C_HECTARE_SHORT_ROTATION_COPPICE`
`AG_BIOMASS_EQUILIBRIUM_KG_C_HECTARE_SHORT_ROTATION_COPPICE`
* **ecoClimateZone:** Rename `BIOMASS_EQUILIBRIUM_KG_C_HECTARE_TEA`
`AG_BIOMASS_EQUILIBRIUM_KG_C_HECTARE_TEA`
* **ecoClimateZone:** Rename `BIOMASS_EQUILIBRIUM_KG_C_HECTARE_VINE`
`AG_BIOMASS_EQUILIBRIUM_KG_C_HECTARE_VINE`
* **ecoClimateZone:** Rename `BIOMASS_EQUILIBRIUM_KG_C_HECTARE_WOODY_PERENNIAL`
`AG_BIOMASS_EQUILIBRIUM_KG_C_HECTARE_WOODY_PERENNIAL`

### Features

* **animalProduct:** add missing `nitrogenContent` to "deer", "rabbit", "milk" and "egg" terms ([3b5ce8c](https://gitlab.com/hestia-earth/hestia-glossary/commit/3b5ce8c874a39eeaccac989485cc40e5c9d9209e)), closes [#1291](https://gitlab.com/hestia-earth/hestia-glossary/issues/1291)
* **crop:** add `averagePropertiesTermIds` to generic terms ([e597a20](https://gitlab.com/hestia-earth/hestia-glossary/commit/e597a206d00bf5233ee497873d89535da0cee3c1)), closes [#1502](https://gitlab.com/hestia-earth/hestia-glossary/issues/1502)
* **crop:** add `correspondingSeedTermIds` lookup ([11a0a56](https://gitlab.com/hestia-earth/hestia-glossary/commit/11a0a56478e299c843e1f113b4ea3a15ca4e0fca)), closes [#1477](https://gitlab.com/hestia-earth/hestia-glossary/issues/1477)
* **ecoClimateZone:** add AG/BG biomass ratio lookups for belowground biomass model ([f1ead66](https://gitlab.com/hestia-earth/hestia-glossary/commit/f1ead66b9d7ecf9b709c7ab63bec1c7648d1ca1f))
* **ecoClimateZone:** add BG biomass equilibrium factor lookups ([4dfcb09](https://gitlab.com/hestia-earth/hestia-glossary/commit/4dfcb094fb4dd1ccc85f2749409d0f3f2b3cb78a))
* **emission:** add factors for `fantkeEtAl2016` emission ([d240be6](https://gitlab.com/hestia-earth/hestia-glossary/commit/d240be6c7eb5c71ac3d49ddfaf44af93a9c529d4))
* **emission:** add radioactive waste lookup table csv/md ([1b46c36](https://gitlab.com/hestia-earth/hestia-glossary/commit/1b46c36fd4c9e8c6c83d602fbf54f34104bdcbee))
* **forage:** add `averagePropertiesTermIds` lookup for various terms ([f6b6f2f](https://gitlab.com/hestia-earth/hestia-glossary/commit/f6b6f2fd7c8e1b759cb34e14dad26d4dc36fb4be)), closes [#1453](https://gitlab.com/hestia-earth/hestia-glossary/issues/1453)
* **forage:** update properties lookup ([13d9568](https://gitlab.com/hestia-earth/hestia-glossary/commit/13d95686ea799bad126904b86058f5ee6e7e7441))
* **inorganicFertiliser:** add 'disodiumZincEDTAKgZn' and 'disodiumCopperEDTAKgCu' ([01b5a80](https://gitlab.com/hestia-earth/hestia-glossary/commit/01b5a80acb9a53d4c190086b617d437f4cc96eeb)), closes [#1509](https://gitlab.com/hestia-earth/hestia-glossary/issues/1509)
* **inorganicFertiliser:** add `Inorganic Iron fertiliser, unspecified (kg Fe)` ([1e01526](https://gitlab.com/hestia-earth/hestia-glossary/commit/1e01526da5751f7e7fc82242daf31d95b991bf1f))
* **landCover:** set `generateImpactAssessment` to `true` ([1de42cf](https://gitlab.com/hestia-earth/hestia-glossary/commit/1de42cff6086bfa5a9259e77b90302e980c490f1))
* **landUseManagement:** add `sumMax100Group` and maximum values ([7ee403b](https://gitlab.com/hestia-earth/hestia-glossary/commit/7ee403b817c6d17e4c82349dad56b1a7a855459b)), closes [#1505](https://gitlab.com/hestia-earth/hestia-glossary/issues/1505)
* **landUseManagement:** add terms for (non) productive periods ([601f3c7](https://gitlab.com/hestia-earth/hestia-glossary/commit/601f3c752cf55c0f12ece4a878f4456b0da42b08)), closes [#1507](https://gitlab.com/hestia-earth/hestia-glossary/issues/1507)
* **liveAnimal:** add `nitrogenContent` to "deer", "rabbit", and "duck" terms ([58910d5](https://gitlab.com/hestia-earth/hestia-glossary/commit/58910d56a28066ed63cc513cc648f8ceb9ff4656)), closes [#1291](https://gitlab.com/hestia-earth/hestia-glossary/issues/1291)
* **liveAnimal:** add `stockingDensityAnimalHousing` lookup ([ea924d4](https://gitlab.com/hestia-earth/hestia-glossary/commit/ea924d4d1eaf77ba3b26d52fc28755c35555da01)), closes [#1450](https://gitlab.com/hestia-earth/hestia-glossary/issues/1450)
* **region:** add `region-liveAnimal-liveweightPerHead-lookup` file ([96496bb](https://gitlab.com/hestia-earth/hestia-glossary/commit/96496bb7c830896b4e92e94b7f5b68979d927e73)), closes [#1506](https://gitlab.com/hestia-earth/hestia-glossary/issues/1506)
* **region:** update `region-liveAnimal-liveweightGain-lookup` file ([61c6bcf](https://gitlab.com/hestia-earth/hestia-glossary/commit/61c6bcf0a144e40096c2bab289cf39abe74bb64a)), closes [#1506](https://gitlab.com/hestia-earth/hestia-glossary/issues/1506)
* **region:** update faostat lookups ([86519d2](https://gitlab.com/hestia-earth/hestia-glossary/commit/86519d249f72bb972407fcb5126dfe0f6e45047a))
* **standardsLabels:** add `boolean` as `units` ([d9e9f71](https://gitlab.com/hestia-earth/hestia-glossary/commit/d9e9f71e09c8cf903b19f5244395ab9bec25e06c)), closes [#1515](https://gitlab.com/hestia-earth/hestia-glossary/issues/1515)
* **system:** add 'smallholderSystem' ([4383c9e](https://gitlab.com/hestia-earth/hestia-glossary/commit/4383c9e9272cd10d89728bc090425c5f5b772ef9)), closes [#1504](https://gitlab.com/hestia-earth/hestia-glossary/issues/1504)
* **waste:** add ionising compounds ([eb3821b](https://gitlab.com/hestia-earth/hestia-glossary/commit/eb3821b571dab015336fddd6bba31c9c90d46b03))
* **waste:** add pubchem, casNumber, lookup columns and values for ionising compounds ([89ed92c](https://gitlab.com/hestia-earth/hestia-glossary/commit/89ed92c80ca56c194058f6a42f6eb608073a11fa))


### Bug Fixes

* **ecoClimateZone:** rename biomass equilibrium columns to distinguish between AG and BG ([4a18612](https://gitlab.com/hestia-earth/hestia-glossary/commit/4a1861255d23c5d83e74be5c36c09c3f7dd0d277))
* **emission:** remove `Bromo... industrial processes` from default sys boundary ([945d611](https://gitlab.com/hestia-earth/hestia-glossary/commit/945d6110b88cc0a132648c1446fba4862ab63566))
* **emission:** rename ionisingComponds terms to ionisingCompounds ([f7a9a12](https://gitlab.com/hestia-earth/hestia-glossary/commit/f7a9a12d5254da3aaaafc6b15222ec76d25e743e))
* **emission:** set `N, to surface water...` terms outside default sys boundary ([26d16ed](https://gitlab.com/hestia-earth/hestia-glossary/commit/26d16edef810aa664359cf7940a9b8982cf9be06))
* **measurement:** update `depthSensitive` lookup values ([6d27f71](https://gitlab.com/hestia-earth/hestia-glossary/commit/6d27f71d31fe8e0fd74401cf287eb118fb37372d))
* **region:** change values in `region-liveAnimal-weightAtMaturity-lookup` to final adult weights ([d125121](https://gitlab.com/hestia-earth/hestia-glossary/commit/d1251219ac1a5776f2a3d48547cbebd5bda0d338)), closes [#1506](https://gitlab.com/hestia-earth/hestia-glossary/issues/1506)

## [0.55.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.54.0...v0.55.0) (2024-09-17)


### ⚠ BREAKING CHANGES

* **ecoClimateZone:** rename lookup `IPCC_2019_TILLAGE_MANAGEMENT_FACTOR_FULL_TILLAGE`
`IPCC_2019_MANAGEMENT_FACTOR_FULL_TILLAGE`
* **ecoClimateZone:** rename lookup `IPCC_2019_TILLAGE_MANAGEMENT_FACTOR_REDUCED_TILLAGE`
`IPCC_2019_MANAGEMENT_FACTOR_REDUCED_TILLAGE`
* **ecoClimateZone:** rename lookup `IPCC_2019_TILLAGE_MANAGEMENT_FACTOR_NO_TILLAGE`
`IPCC_2019_MANAGEMENT_FACTOR_NO_TILLAGE`
* **ecoClimateZone:** rename lookup `IPCC_2019_GRASSLAND_MANAGEMENT_FACTOR_NOMINALLY_MANAGED`
`IPCC_2019_MANAGEMENT_FACTOR_NOMINALLY_MANAGED`
* **ecoClimateZone:** rename lookup `IPCC_2019_GRASSLAND_MANAGEMENT_FACTOR_HIGH_INTENSITY_GRAZING`
`IPCC_2019_MANAGEMENT_FACTOR_HIGH_INTENSITY_GRAZING`
* **ecoClimateZone:** rename lookup `IPCC_2019_GRASSLAND_MANAGEMENT_FACTOR_SEVERELY_DEGRADED`
`IPCC_2019_MANAGEMENT_FACTOR_SEVERELY_DEGRADED`
* **ecoClimateZone:** rename lookup `IPCC_2019_GRASSLAND_MANAGEMENT_FACTOR_IMPROVED_GRASSLAND`
`IPCC_2019_MANAGEMENT_FACTOR_IMPROVED_GRASSLAND`
* **ecoClimateZone:** rename lookup `IPCC_2019_CROPLAND_CARBON_INPUT_FACTOR_MEDIUM`
`IPCC_2019_CARBON_INPUT_FACTOR_CROPLAND_MEDIUM`
* **ecoClimateZone:** rename lookup `IPCC_2019_CROPLAND_CARBON_INPUT_FACTOR_LOW`
`IPCC_2019_CARBON_INPUT_FACTOR_CROPLAND_LOW`
* **ecoClimateZone:** rename lookup `IPCC_2019_CROPLAND_CARBON_INPUT_FACTOR_HIGH_WITHOUT_MANURE`
`IPCC_2019_CARBON_INPUT_FACTOR_CROPLAND_HIGH_WITHOUT_MANURE`
* **ecoClimateZone:** rename lookup `IPCC_2019_CROPLAND_CARBON_INPUT_FACTOR_HIGH_WITH_MANURE`
`IPCC_2019_CARBON_INPUT_FACTOR_CROPLAND_HIGH_WITH_MANURE`
* **ecoClimateZone:** rename lookup `IPCC_2019_GRASSLAND_CARBON_INPUT_FACTOR_MEDIUM`
`IPCC_2019_CARBON_INPUT_FACTOR_GRASSLAND_MEDIUM`
* **ecoClimateZone:** rename lookup `IPCC_2019_GRASSLAND_CARBON_INPUT_FACTOR_HIGH`
`IPCC_2019_CARBON_INPUT_FACTOR_GRASSLAND_HIGH`
* **ecoClimateZone:** delete lookup `IPCC_2019_SOC_REF_SD_HAC`
* **ecoClimateZone:** delete lookup `IPCC_2019_SOC_REF_SD_LAC`
* **ecoClimateZone:** delete lookup `IPCC_2019_SOC_REF_SD_SAN`
* **ecoClimateZone:** delete lookup `IPCC_2019_SOC_REF_SD_POD`
* **ecoClimateZone:** delete lookup `IPCC_2019_SOC_REF_SD_VOL`
* **ecoClimateZone:** delete lookup `IPCC_2019_SOC_REF_SD_WET`
* **ecoClimateZone:** delete lookup `IPCC_2019_LANDUSE_FACTOR_SD_ANNUAL_CROPS`
* **ecoClimateZone:** delete lookup `IPCC_2019_LANDUSE_FACTOR_SD_ANNUAL_CROPS_WET`
* **ecoClimateZone:** delete lookup `IPCC_2019_LANDUSE_FACTOR_SD_PADDY_RICE_CULTIVATION`
* **ecoClimateZone:** delete lookup `IPCC_2019_LANDUSE_FACTOR_SD_PERENNIAL_CROPS`
* **ecoClimateZone:** delete lookup `IPCC_2019_LANDUSE_FACTOR_SD_SET_ASIDE`
* **ecoClimateZone:** delete lookup `IPCC_2019_TILLAGE_MANAGEMENT_FACTOR_SD_REDUCED_TILLAGE`
* **ecoClimateZone:** delete lookup `IPCC_2019_TILLAGE_MANAGEMENT_FACTOR_SD_NO_TILLAGE`
* **ecoClimateZone:** delete lookup `IPCC_2019_GRASSLAND_MANAGEMENT_FACTOR_SD_HIGH_INTENSITY_GRAZING`
* **ecoClimateZone:** delete lookup `IPCC_2019_GRASSLAND_MANAGEMENT_FACTOR_SD_SEVERELY_DEGRADED`
* **ecoClimateZone:** delete lookup `IPCC_2019_GRASSLAND_MANAGEMENT_FACTOR_SD_IMPROVED_GRASSLAND`
* **ecoClimateZone:** delete lookup `IPCC_2019_CROPLAND_CARBON_INPUT_FACTOR_SD_LOW`
* **ecoClimateZone:** delete lookup `IPCC_2019_CROPLAND_CARBON_INPUT_FACTOR_SD_HIGH_WITHOUT_MANURE`
* **ecoClimateZone:** delete lookup `IPCC_2019_CROPLAND_CARBON_INPUT_FACTOR_SD_HIGH_WITH_MANURE`
* **ecoClimateZone:** delete lookup `IPCC_2019_GRASSLAND_CARBON_INPUT_FACTOR_SD_HIGH`
* **region:** rename `Titus` `Titus (County), Texas, United States`

### Features

* **biologialControlAgent:** add `Bacillus thuringiensis ssp. tenebrionis` ([31e0a11](https://gitlab.com/hestia-earth/hestia-glossary/commit/31e0a1139a147f7d320e07f6c5060c06809d1149)), closes [#1484](https://gitlab.com/hestia-earth/hestia-glossary/issues/1484)
* **ecoClimateZone:** add equilibrium factor lookups for biomass carbon model ([5bce5c2](https://gitlab.com/hestia-earth/hestia-glossary/commit/5bce5c2455211f03a6195ecb4c7efe0e060e3ff2))
* **ecoClimateZone:** add new factor lookups for SOC model ([e59166f](https://gitlab.com/hestia-earth/hestia-glossary/commit/e59166f067976299efe936ed9747d19fed2775ce))
* **ecoClimateZone:** convert SOC model lookup values from single value to grouped data ([1b00e54](https://gitlab.com/hestia-earth/hestia-glossary/commit/1b00e5429f3a5c6866e395851bf07c20f51b609f))
* **ecoClimateZone:** delete SD columns from SOC model lookups ([5dd1bbe](https://gitlab.com/hestia-earth/hestia-glossary/commit/5dd1bbe80c5efa27dd9a94d79abfe9424e68f6c8))
* **ecoClimateZone:** rename SOC model lookup columns and convert from single value to grouped data ([9624624](https://gitlab.com/hestia-earth/hestia-glossary/commit/9624624ed2ce668a20cec6f45ce359b892c1fd39))
* **emission:** add new acidification terms ([565971d](https://gitlab.com/hestia-earth/hestia-glossary/commit/565971ddc5cf120447416444218c87e8ad7fb7e3))
* **emission:** create new terms and lookup files for eutrophication model ([3c36fa0](https://gitlab.com/hestia-earth/hestia-glossary/commit/3c36fa0d129b5a625281aa7a5d0a9ebef286930c))
* **emissions:** add new emissions related to ozone depletion ([4c50adc](https://gitlab.com/hestia-earth/hestia-glossary/commit/4c50adcc010160fb2dbaa3ff4255b8be907b077a)), closes [#1461](https://gitlab.com/hestia-earth/hestia-glossary/issues/1461) [#1459](https://gitlab.com/hestia-earth/hestia-glossary/issues/1459) [#1462](https://gitlab.com/hestia-earth/hestia-glossary/issues/1462)
* **fertiliserBrandName:** add active ingredients ([6976be2](https://gitlab.com/hestia-earth/hestia-glossary/commit/6976be2a64ce3327a2d3b3d96731797b8869d104)), closes [#1426](https://gitlab.com/hestia-earth/hestia-glossary/issues/1426)
* **fuel:** add `density` to `biodiesel` ([9bae0b5](https://gitlab.com/hestia-earth/hestia-glossary/commit/9bae0b5fa9dc6599ef56d58353f774494f556e76))
* **liveAquaticSpecies:** add `defaultSalinity` lookup ([b18e2f6](https://gitlab.com/hestia-earth/hestia-glossary/commit/b18e2f60e10d5afb1e23dc61b901009e732f5f4a))
* **measurement:** add `IPCC_2019_CH4_aquaculture_EF` lookup ([bae8d9b](https://gitlab.com/hestia-earth/hestia-glossary/commit/bae8d9b3be88be668594fdb58f762c7b33bbc740)), closes [#1501](https://gitlab.com/hestia-earth/hestia-glossary/issues/1501)
* **operation:** add `beddingUpAnimals` ([878fc24](https://gitlab.com/hestia-earth/hestia-glossary/commit/878fc24c6c2ee85d9cce8e7c6d9897e07f4a468c)), closes [#1476](https://gitlab.com/hestia-earth/hestia-glossary/issues/1476)
* **pesticideBrandName:** add multiple terms for FIBL alignment ([3cacb31](https://gitlab.com/hestia-earth/hestia-glossary/commit/3cacb31bc0e9f8e7e3720043d5ab7763e7b1df30))
* **pesticideBrandName:** add pesticide brands from FCC ([6a8b2ee](https://gitlab.com/hestia-earth/hestia-glossary/commit/6a8b2ee5a6110fb32a04b8ad37f06113e8990ee1))
* **region:** rename `Titus` `Titus (County), Texas, United States` ([522d967](https://gitlab.com/hestia-earth/hestia-glossary/commit/522d96716a3966feddb68e15f0314f584a1b88de)), closes [#1484](https://gitlab.com/hestia-earth/hestia-glossary/issues/1484)
* **soilAmendment:** add `Stone meal, unspecified` ([db1090c](https://gitlab.com/hestia-earth/hestia-glossary/commit/db1090c22d6e7f2c798607fbe5425279d2f0ba4c)), closes [#1484](https://gitlab.com/hestia-earth/hestia-glossary/issues/1484)


### Bug Fixes

* **emission:** tidy glossary and fix errors ([c3c9593](https://gitlab.com/hestia-earth/hestia-glossary/commit/c3c9593d06107795d04765690bc86cfdc07a013f))
* **inorganicFertiliser:** add sulphur content to `Urea Ammonium Sulphate (kg N)` ([60533a5](https://gitlab.com/hestia-earth/hestia-glossary/commit/60533a5c9b3038f5f366407b1040b508c572844e))

## [0.54.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.53.1...v0.54.0) (2024-09-03)


### ⚠ BREAKING CHANGES

* **landUseManagement:** Renames `Short fallow period` to
`Short fallow duration` for consistency with other terms in the glossary.
* **landUseManagement:** Renames `Long fallow period` to
`Long fallow duration` for consistency with other terms in the glossary.
* **landUseManagement:** Deletes term `Previous product`. Information
describing the previous product should be added to Site Management.

### Features

* **characterisedIndicator:** add `Soil quality index` for PEF and LANCA ([d4af722](https://gitlab.com/hestia-earth/hestia-glossary/commit/d4af7229a191453a8ca6d77eb9a1b5a474fa579d))
* **crop:** upate property lookup ([cc56429](https://gitlab.com/hestia-earth/hestia-glossary/commit/cc56429068760abee6e754b665b6e2815ee01052))
* **crop:** update properties for `grassCloverMixtureHay` and `grassCloverMixtureSilage` ([19e7435](https://gitlab.com/hestia-earth/hestia-glossary/commit/19e7435f39b83a6f3bca4b0b159e5732d845e1d2))
* **emission:** ozoneDepletionPotential lookup ([937c5ae](https://gitlab.com/hestia-earth/hestia-glossary/commit/937c5aebec760998e1481971aa504dd61961cf4f)), closes [#1459](https://gitlab.com/hestia-earth/hestia-glossary/issues/1459)
* **fertiliserBrandName:** add 48 new terms from FCC ([c491907](https://gitlab.com/hestia-earth/hestia-glossary/commit/c491907985181c7cff161de5834e0d94ff9d03d1))
* **landCover:** add missing `siteTypesAllowed` ([dc0db8c](https://gitlab.com/hestia-earth/hestia-glossary/commit/dc0db8c19df096a24a1671ed2566ffd790665a2e)), closes [#1456](https://gitlab.com/hestia-earth/hestia-glossary/issues/1456)
* **landCover:** add new lookup `BIOMASS_CATEGORY` for biomass carbon stock modelling ([2c0ba61](https://gitlab.com/hestia-earth/hestia-glossary/commit/2c0ba61ffc82c64b02b0e45dd75f39619f2ba5e7))
* **landUseManagement:** delete `Crop rotation` and `Aquaculture rotation` ([fb21675](https://gitlab.com/hestia-earth/hestia-glossary/commit/fb216752748afb94caafd4805e41dec3cba40879))
* **landUseManagement:** delete `Previous product` ([41bf2b3](https://gitlab.com/hestia-earth/hestia-glossary/commit/41bf2b36cdb4b56753f98a57beb5ca4773ccccd5))
* **model:** add `Fantke et al (2016)` ([8657b5e](https://gitlab.com/hestia-earth/hestia-glossary/commit/8657b5e3b7e5af222d87e6ba62602819ada3571b))
* **operation:** add `Moving temporary fencing` ([21cb9d2](https://gitlab.com/hestia-earth/hestia-glossary/commit/21cb9d2435c420e01685943c6ed8d6537723728c)), closes [#1467](https://gitlab.com/hestia-earth/hestia-glossary/issues/1467)
* **operation:** add coffee processing terms ([4205307](https://gitlab.com/hestia-earth/hestia-glossary/commit/4205307ad6d6d0fb08405f67580979f933c4abb9)), closes [#1473](https://gitlab.com/hestia-earth/hestia-glossary/issues/1473)
* **region:** add `crop-cropGroupingFaostatProduction-areaHarvested` lookup ([8c1be16](https://gitlab.com/hestia-earth/hestia-glossary/commit/8c1be167c6439b7407c60b2dc65920db38787de8))
* **region:** rename `faostatCroplandArea` lookup to `faostatArea` and add more items ([cd553f5](https://gitlab.com/hestia-earth/hestia-glossary/commit/cd553f5edaa1013db3829b0a7439e28fb7cb1e97))
* **region:** update FAOSTAT lookups ([7a18d1f](https://gitlab.com/hestia-earth/hestia-glossary/commit/7a18d1fd5d3d56a50582102c8e82a0b58217e7b0))


### Bug Fixes

* **landCover:** errors in `siteTypesAllowed` and `subClassOf` ([30af231](https://gitlab.com/hestia-earth/hestia-glossary/commit/30af231a78114cced0e86a67910035fec9e44727)), closes [#1456](https://gitlab.com/hestia-earth/hestia-glossary/issues/1456)
* **landUseManagement:** rename `Long fallow period` to `Long fallow duration` ([b0badda](https://gitlab.com/hestia-earth/hestia-glossary/commit/b0badda2972f5f26a83fe267849eb8dde371dc5b))
* **landUseManagement:** rename `Short fallow period` to `Short fallow duration` ([2553ada](https://gitlab.com/hestia-earth/hestia-glossary/commit/2553ada43d3a7176b2ca008716353cd52ba7da95))

### [0.53.1](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.53.0...v0.53.1) (2024-08-19)


### Features

* **landUseManagement:** change `arrayTreatment` on `Pasture grass` to `sum` ([ca45a2e](https://gitlab.com/hestia-earth/hestia-glossary/commit/ca45a2e4e9fe174fed65795a7a5239a053a9b87c)), closes [#1431](https://gitlab.com/hestia-earth/hestia-glossary/issues/1431)
* **liveAnimal:** add PM and TSP lookups ([73155c1](https://gitlab.com/hestia-earth/hestia-glossary/commit/73155c1247569ec87dc0325be1733fdd3f8c941e))
* **measurement:** add new term `belowGroundBiomass` to glossary ([6b23802](https://gitlab.com/hestia-earth/hestia-glossary/commit/6b238028b55dcebf455c88eefeb50bfe8b6b1c4d))


### Bug Fixes

* **inorganicFertiliser:** error in `Nitrogen content` of `Nitric acid` ([1593bb9](https://gitlab.com/hestia-earth/hestia-glossary/commit/1593bb9dcde97ff69f151cda9e2349f91e6a3ea4)), closes [#1444](https://gitlab.com/hestia-earth/hestia-glossary/issues/1444)
* **region:** change `Laos` `subClassOf` to `region-south-eastern-asia` ([38fdefd](https://gitlab.com/hestia-earth/hestia-glossary/commit/38fdefd12ceb08bd44fee89ec3ef77a7bb1cd16d)), closes [#1454](https://gitlab.com/hestia-earth/hestia-glossary/issues/1454)
* **region:** update `area` for level 0 regions ([e9882c5](https://gitlab.com/hestia-earth/hestia-glossary/commit/e9882c5fb7f1f3ef735ac416042e0da2b66ac4e0))

## [0.53.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.52.0...v0.53.0) (2024-08-06)


### ⚠ BREAKING CHANGES

* **liveAquaticSpecies:** rename `Fish, unspecified` `Finfish, unspecified`
* **liveAquaticSpecies:** rename `Crustaceans, unspecified` `Crustacean, unspecified`
* **liveAquaticSpecies:** rename `Abalone spp.` `Abalone, unspecified`
* **liveAquaticSpecies:** rename `Amberjack spp.` `Amberjack, unspecified`
* **liveAquaticSpecies:** rename `Cupped oysters spp.` `True oyster, unspecified`
* **liveAquaticSpecies:** rename `Cupped oysters spp.` `True oyster, unspecified`
* **liveAquaticSpecies:** rename `Frogs` `Frog, unspecified`
* **liveAquaticSpecies:** rename `Groupers` `Grouper, unspecified`
* **liveAquaticSpecies:** rename `Jellyfishes` `Jellyfish, unspecified`
* **liveAquaticSpecies:** rename `Lefteye flounders spp.` `Lefteye flounder, unspecified`
* **liveAquaticSpecies:** rename `Metapenaeus shrimps spp.` `Metapenaeus shrimp, unspecified`
* **liveAquaticSpecies:** rename `Mullets spp.` `Mullet, unspecified`
* **liveAquaticSpecies:** rename `Porgies, seabreams spp.` `Porgies and seabreams, unspecified`
* **liveAquaticSpecies:** rename `Portunus swimcrabs spp.` `Portunus swimcrab, unspecified`
* **liveAquaticSpecies:** rename `River and lake turtles spp.` `Freshwater turtle, unspecified`
* **liveAquaticSpecies:** rename `Sea squirts spp.` `Sea squirt, unspecified`
* **liveAquaticSpecies:** rename `Carassius spp.` `Carassius, unspecified`
* **liveAquaticSpecies:** rename `Penaeus shrimps spp.` `Penaeus shrimp, unspecified`

### Features

* **liveAquaticSpecies:** add 350 new terms ([15abe42](https://gitlab.com/hestia-earth/hestia-glossary/commit/15abe4242fe305f7c11608afda46645ba316fc43)), closes [#1191](https://gitlab.com/hestia-earth/hestia-glossary/issues/1191)
* **material:** add `ecoinventMapping` lookup to several terms ([35ac3ff](https://gitlab.com/hestia-earth/hestia-glossary/commit/35ac3ff00925ac14cc40a6e731498d4bad52eba4))
* **measurement:** add `booleanGroup` lookup ([944cd6f](https://gitlab.com/hestia-earth/hestia-glossary/commit/944cd6f67767a30aa313811a8040be5da10aa43c)), closes [#1422](https://gitlab.com/hestia-earth/hestia-glossary/issues/1422)
* **measurement:** add `valueType` lookup ([9ab9e05](https://gitlab.com/hestia-earth/hestia-glossary/commit/9ab9e05b38ef2860a6fc89337965ad9c866d0ac7)), closes [#1447](https://gitlab.com/hestia-earth/hestia-glossary/issues/1447)
* **pesticideBrandName:** add `Dual Gold` ([14dbb9f](https://gitlab.com/hestia-earth/hestia-glossary/commit/14dbb9fe37034b860dcb2f496fd32d8ecf3a33da)), closes [#1448](https://gitlab.com/hestia-earth/hestia-glossary/issues/1448)

## [0.52.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.51.1...v0.52.0) (2024-07-23)


### ⚠ BREAKING CHANGES

* **system:** Rename `Biofloc acquaculture system` `Biofloc aquaculture system`

### Features

* **crop:** add missing `dryMatter` property to vegetable crops ([a764394](https://gitlab.com/hestia-earth/hestia-glossary/commit/a7643948e27b5b496db9650deb659f2dcc54e8f8))
* **model:** add `HESTIA` as a model ([93eb601](https://gitlab.com/hestia-earth/hestia-glossary/commit/93eb60193dbb46e9fa7a592df4ff5b7477df2931))
* **substrate:** add depreciated and unspecified terms ([e44876a](https://gitlab.com/hestia-earth/hestia-glossary/commit/e44876ade51accaeab35e1d64e17b61a178a926e)), closes [#1435](https://gitlab.com/hestia-earth/hestia-glossary/issues/1435) [#1436](https://gitlab.com/hestia-earth/hestia-glossary/issues/1436)
* **substrate:** add lookup `nitrifyingDenitrifyingBacteriaCfuRelativeToSoil` ([ae4d58e](https://gitlab.com/hestia-earth/hestia-glossary/commit/ae4d58ef61a1f96362b14cf761295ea8a887adc7))
* **system:** add `PRACTICE_INCREASING_C_INPUT` lookup to glossary ([6e534ac](https://gitlab.com/hestia-earth/hestia-glossary/commit/6e534acad7030cdc3ff3cb1b68843354db49bf76))
* **system:** add lookup `requiredForSiteTypes` ([0569f9c](https://gitlab.com/hestia-earth/hestia-glossary/commit/0569f9ced91ca4a898a63a6e874b4e57326fc71f))
* **system:** add new terms for protected cropping systems ([8c56398](https://gitlab.com/hestia-earth/hestia-glossary/commit/8c56398b4e991b15804ed4fd60040385bf1aa747)), closes [#1439](https://gitlab.com/hestia-earth/hestia-glossary/issues/1439)


### Bug Fixes

* **emission:** errors in `siteTypes` allowed for crop residue emissions ([98c6265](https://gitlab.com/hestia-earth/hestia-glossary/commit/98c626531160748a284928755b31d3648e6043e2)), closes [#1440](https://gitlab.com/hestia-earth/hestia-glossary/issues/1440)
* **model:** use uppercase for "HESTIA" in `HESTIA aggregated data` ([fed4a68](https://gitlab.com/hestia-earth/hestia-glossary/commit/fed4a68195a866a9e0053c446d9758e526177fc4))
* **system:** fix typo in term name ([3b12920](https://gitlab.com/hestia-earth/hestia-glossary/commit/3b12920b01a38ae44dc021ba9efabd7e78c8f1cc))

### [0.51.1](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.51.0...v0.51.1) (2024-07-09)


### Features

* **crop:** add `maximumCycleDuration` lookup ([7083993](https://gitlab.com/hestia-earth/hestia-glossary/commit/708399367116af0f5be6c27412f9a88252564ff0)), closes [#1428](https://gitlab.com/hestia-earth/hestia-glossary/issues/1428)
* **emission:** add `TSP, to air, animal housing` ([efc0917](https://gitlab.com/hestia-earth/hestia-glossary/commit/efc0917288f44b81795c6a67239151cd17e58717))
* **emission:** update `lsrgAccountingCategory` lookup ([80e1c6f](https://gitlab.com/hestia-earth/hestia-glossary/commit/80e1c6f00cdec78eae1e727c3d88bb8ce0bd792e))
* **fuel:** update agrovoc, wikipedia, and synonyms ([0ff1b85](https://gitlab.com/hestia-earth/hestia-glossary/commit/0ff1b8597b924775e3895d6d5d1fa2e8040e7ff6)), closes [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72)
* **material:** add `Polycarbonate, depreciated amount per Cycle` and `Polycarbonate` ([a614050](https://gitlab.com/hestia-earth/hestia-glossary/commit/a614050c7544bc61bd6e37f3edbdce3bf23e35e7))
* **operation:** add agrovoc links ([a59b65a](https://gitlab.com/hestia-earth/hestia-glossary/commit/a59b65afae0d12dcfc2d3d46ec045dd99fb743a5)), closes [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72)
* **operation:** add cogeneration ([b7d55cf](https://gitlab.com/hestia-earth/hestia-glossary/commit/b7d55cfe2cb837ad69e062a8a33ba28d33a2033d)), closes [#1367](https://gitlab.com/hestia-earth/hestia-glossary/issues/1367)
* **operation:** add diesel use related to threshing ([cc73c98](https://gitlab.com/hestia-earth/hestia-glossary/commit/cc73c98ec732f955b82785e961ef5d648f9e985c))


### Bug Fixes

* **measurement:** update `depthSensitive` lookup values ([1b11f15](https://gitlab.com/hestia-earth/hestia-glossary/commit/1b11f157de8347b6456a0b960654dd3f2284234d))
* **measurement:** update `minimum` and `maximum` for `Drainage class` ([cb5acff](https://gitlab.com/hestia-earth/hestia-glossary/commit/cb5acffcd38a4c2e5c6ce65678e8a71fdfa71542))

## [0.51.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.50.0...v0.51.0) (2024-06-24)


### ⚠ BREAKING CHANGES

* **region:** Rename `Bark` to
`Bark (Gemeinde), Leezen, Segeberg, Schleswig-Holstein, Germany`
* **emission:** Fixes typo in term name and renames
`CO2, to air, dead organic matter stock change,
land management change` to
`CO2, to air, dead organic matter stock change,
management change`.
* **forage:** Delete `N_FIXING_CROP` lookup
* **crop:** Delete `LOW_RESIDUE_PRODUCING_CROP` lookup
* **crop:** Delete `N_FIXING_CROP` lookup
* **otherOrganicChemical:** move `Monensin sodium` from `otherOrganicChemical` to `veterinaryDrug`
* **pesticideAI:** move `Monensin` from `pesticideAI` to `veterinaryDrug`
* **pesticideBrandName:** rename `Xentari WDG` `Xentari 10.3 WG`

### Features

* **animalBreed:** add fiber goat breeds ([850f4a9](https://gitlab.com/hestia-earth/hestia-glossary/commit/850f4a985d013ee5c3a3efbe89b18a99de455d25)), closes [#1412](https://gitlab.com/hestia-earth/hestia-glossary/issues/1412)
* **crop:** delete unused lookups ([ac04be8](https://gitlab.com/hestia-earth/hestia-glossary/commit/ac04be843789ee00e27a7be594f7b541fee544be))
* **emission:** add "scope" lookup to distinguish Scope 1-3 ([959e0dd](https://gitlab.com/hestia-earth/hestia-glossary/commit/959e0dd3846082e4ef310d3c9e33a48b96f3cbe0)), closes [#669](https://gitlab.com/hestia-earth/hestia-glossary/issues/669)
* **emission:** add `... inputs production` terms for most key emissions ([f38bda7](https://gitlab.com/hestia-earth/hestia-glossary/commit/f38bda72f0d60273a8b62c6eb099131fc32e73d9))
* **emission:** add LSRG accounting categories as lookups ([258146a](https://gitlab.com/hestia-earth/hestia-glossary/commit/258146ae6e1e6bce23a4cf7c7942c0299b3f1709)), closes [#669](https://gitlab.com/hestia-earth/hestia-glossary/issues/669)
* **fertiliserBrandName:** add  `Granulock® SS` and `Granulock® Z` ([b705d3e](https://gitlab.com/hestia-earth/hestia-glossary/commit/b705d3ef008af46a1589ca40a31334cf56ebf432))
* **forage:** delete unused lookup ([b1e6e05](https://gitlab.com/hestia-earth/hestia-glossary/commit/b1e6e05ea7b5e6dcb28371f8ef0fbf42e52081cc))
* **landUseManagement:** add `CO2 enrichment used` and `Biological control used` ([ee157a6](https://gitlab.com/hestia-earth/hestia-glossary/commit/ee157a6358687b9f7818809c91d813a5288c3074))
* **landUseManagement:** add new lookup `GAP_FILL_TO_MANAGEMENT` ([8501357](https://gitlab.com/hestia-earth/hestia-glossary/commit/85013571dd8cf43a86ed9e099a559739ca9de6e4))
* **methodMeasurement:** add `DTPA-extraction method` ([d2f795b](https://gitlab.com/hestia-earth/hestia-glossary/commit/d2f795b93b079cf48413aa207e425e6208f156d8))
* **operation:** add `combustionType` lookup ([d12f45c](https://gitlab.com/hestia-earth/hestia-glossary/commit/d12f45cb6179a66f503dbe0adf1b48bfa85af1fe)), closes [#669](https://gitlab.com/hestia-earth/hestia-glossary/issues/669)
* **otherOrganicChemical:** move `Monensin sodium` to `veterinaryDrug` ([c1bcfe1](https://gitlab.com/hestia-earth/hestia-glossary/commit/c1bcfe180d6992933f2daa6f1ee1f37cbfe064a7))
* **pesticideAI:** move `Monensin` to `veterinaryDrug` ([7f29069](https://gitlab.com/hestia-earth/hestia-glossary/commit/7f290697e1a1deabb55a4ddb8c8e1a535bdc053a))
* **pesticideBrandName:** add `Xentari 54 WG` ([47cf166](https://gitlab.com/hestia-earth/hestia-glossary/commit/47cf166cd7616e4f42aa205e65c001f59b9588b4))
* **pesticideBrandName:** rename `Xentari WDG` `Xentari 10.3 WG` ([3fce9cc](https://gitlab.com/hestia-earth/hestia-glossary/commit/3fce9cc8e978d7b7315e50260e19adc04de2da31))
* **soilTexture:** set `skipAggregation` to `true` ([8c69c0d](https://gitlab.com/hestia-earth/hestia-glossary/commit/8c69c0d41d774b30e3b3459310e363c59197ce72)), closes [#1423](https://gitlab.com/hestia-earth/hestia-glossary/issues/1423)
* **substrate:** create glossary ([c2ec6a2](https://gitlab.com/hestia-earth/hestia-glossary/commit/c2ec6a2f7b3cb5ca2ed5db68faec519d47fa362d)), closes [#543](https://gitlab.com/hestia-earth/hestia-glossary/issues/543)


### Bug Fixes

* **characterisedIndicator:** update model mapping file ([d28b7d5](https://gitlab.com/hestia-earth/hestia-glossary/commit/d28b7d5426db99534b720aaa3bb8bd7c84e83e11))
* **emission:** error in system boundary lookup ([b24077e](https://gitlab.com/hestia-earth/hestia-glossary/commit/b24077ef206f6df03fd3bf534205c469ab9457ac))
* **emission:** error in term name ([469f8d1](https://gitlab.com/hestia-earth/hestia-glossary/commit/469f8d150727dec5d418a66ba6f8cb0d5b9a86c9))
* **emission:** errors with delimiters in `siteTypesAllowed` lookup ([fde0f6d](https://gitlab.com/hestia-earth/hestia-glossary/commit/fde0f6d47749f1426a88b52a6466f190891fa8ab))
* **emission:** remove duplicated term ([5f635d4](https://gitlab.com/hestia-earth/hestia-glossary/commit/5f635d4fb254401a3d9ff5fc63ea3ffa2dbfb9a4))
* **measurement:** update model siteTypesAllowed lookup ([1303d28](https://gitlab.com/hestia-earth/hestia-glossary/commit/1303d283dff27b74572ea20cacab3e10bea49f0f))
* **region:** add missing `-` in emission lookups ([1e39235](https://gitlab.com/hestia-earth/hestia-glossary/commit/1e39235af4dfbbbca043b7d1dbbde8836674504b))
* **region:** add missing emissions to LC-Impact lookups ([f77bc94](https://gitlab.com/hestia-earth/hestia-glossary/commit/f77bc9410a9686ca8582706d1c0b4e876b06cc67))
* **region:** use full GADM name for "Bark" region ([74ac301](https://gitlab.com/hestia-earth/hestia-glossary/commit/74ac30154972db75950886ae5b6dace502e37a47))

## [0.50.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.49.2...v0.50.0) (2024-06-11)


### ⚠ BREAKING CHANGES

* **operation:** rename `Collecting eggs, with machine` `Collecting eggs, machine unspecified`
* **measurement:** `Total carbon (per kg soil)` deleted
* **measurement:** `Total carbon (per m3 soil)` deleted
* **measurement:** `Total carbon (per ha)` deleted
* **emission:** Replace `co2ToAirSoilCarbonStockChangeManagementChange`
with `co2ToAirSoilOrganicCarbonStockChangeManagementChange`
and `co2ToAirSoilInorganicCarbonStockChangeManagementChange`.
* **emission:** Replace `co2ToAirSoilCarbonStockChangeLandUseChange` with
`co2ToAirSoilOrganicCarbonStockChangeLandUseChange` and
`co2ToAirSoilInorganicCarbonStockChangeLandUseChange`.

### Features

* **animalProduct:** add `animalProduct-property-default-lookup.xlsx` ([ac371ff](https://gitlab.com/hestia-earth/hestia-glossary/commit/ac371ff758597621eeeac9ac1b8d977c1f4aca94)), closes [#1292](https://gitlab.com/hestia-earth/hestia-glossary/issues/1292)
* **animalProduct:** add `Lard` ([e58ce37](https://gitlab.com/hestia-earth/hestia-glossary/commit/e58ce373c215a520eef303bb512da09d7e1a8a36))
* **crop:** add `averagePropertiesTermIds` lookup to animal feeds with no feedipedia properties ([380001b](https://gitlab.com/hestia-earth/hestia-glossary/commit/380001be8ac0102dd5db5bd406a967c23f75321f)), closes [#1292](https://gitlab.com/hestia-earth/hestia-glossary/issues/1292)
* **crop:** add `Cauliflower, leaf` ([208f263](https://gitlab.com/hestia-earth/hestia-glossary/commit/208f263fa520f83d5bca144bdbca369a848bf793))
* **emission:** add NF3 and SF6 emissions ([e16cc67](https://gitlab.com/hestia-earth/hestia-glossary/commit/e16cc67eda7157cfe2a7528c7e7da80cc0b4be08))
* **emission:** split soil carbon stock change term ([b501359](https://gitlab.com/hestia-earth/hestia-glossary/commit/b5013590637b54223bfa3d51e65a68c0182d0b81)), closes [#669](https://gitlab.com/hestia-earth/hestia-glossary/issues/669)
* **emission:** update termTypes allowed ([4d0a12e](https://gitlab.com/hestia-earth/hestia-glossary/commit/4d0a12e3a7a5b433a2fe09c192d7f12182d19937))
* **feedFoodAdditive:** add `feedFoodAdditive-property-default-lookup.xlsx` ([9149855](https://gitlab.com/hestia-earth/hestia-glossary/commit/9149855bbdb239de1a19e9c666ddc0b56851beb1)), closes [#1292](https://gitlab.com/hestia-earth/hestia-glossary/issues/1292)
* **fertiliserBrandName:** add `L-CBF Boost™ 4-0-3-2S-0.5Mg` and `L-CBF 10-14-1` ([b977fae](https://gitlab.com/hestia-earth/hestia-glossary/commit/b977fae0f56e251e417329f1b4a2db1ffac38fc1)), closes [#1417](https://gitlab.com/hestia-earth/hestia-glossary/issues/1417)
* **fertiliserBrandName:** add three new terms ([26f04f5](https://gitlab.com/hestia-earth/hestia-glossary/commit/26f04f5abd28bb440318cf96187e9ed084bef056))
* **forage:** add `averagePropertiesTermIds` lookup to `Danthonia, fresh forage` ([7a088d8](https://gitlab.com/hestia-earth/hestia-glossary/commit/7a088d83b3016bd26bee1cdd6b1c9642146985fd)), closes [#1292](https://gitlab.com/hestia-earth/hestia-glossary/issues/1292)
* **landCover:** add `Annual cropland` and ` Permanent cropland` ([3afc656](https://gitlab.com/hestia-earth/hestia-glossary/commit/3afc6564f7fd3ff62b773ba88b4a8df00609ee35))
* **liveAnimal:** add `isWoolProducingAnimal` lookup ([6945ea0](https://gitlab.com/hestia-earth/hestia-glossary/commit/6945ea0bb5dc54953d6f0d9945d7e886e16e7480)), closes [#1411](https://gitlab.com/hestia-earth/hestia-glossary/issues/1411)
* **liveAnimal:** add region-liveweightGain lookup ([baf690f](https://gitlab.com/hestia-earth/hestia-glossary/commit/baf690fde47f7efb27df91620a70325e9145847c)), closes [#645](https://gitlab.com/hestia-earth/hestia-glossary/issues/645)
* **liveAnimal:** add region-milkYieldPerAnimal ([d825ffb](https://gitlab.com/hestia-earth/hestia-glossary/commit/d825ffb64b50de043aa39afd2b0f62723cce8d3b)), closes [#647](https://gitlab.com/hestia-earth/hestia-glossary/issues/647)
* **liveAnimal:** add region-weightAtMaturity lookup ([771acf5](https://gitlab.com/hestia-earth/hestia-glossary/commit/771acf5e25ad211ff6023a59b77444d65972293a)), closes [#644](https://gitlab.com/hestia-earth/hestia-glossary/issues/644)
* **measurement:** add `Saline water`, `Fresh water`, and `Brackish water` ([b5771cc](https://gitlab.com/hestia-earth/hestia-glossary/commit/b5771cc7eff62a30a7b2b2857c651ac76d37d7de)), closes [#1420](https://gitlab.com/hestia-earth/hestia-glossary/issues/1420)
* **measurement:** add particulate C and N terms and remove total C terms ([7adc3a4](https://gitlab.com/hestia-earth/hestia-glossary/commit/7adc3a4fef7d513698928f84e7a9fc08d17113cf))
* **measurement:** align soil carbon definitions to GHG Protocol ([b9e3a7f](https://gitlab.com/hestia-earth/hestia-glossary/commit/b9e3a7ff9128e4e350d80164ce806fdc42daf558)), closes [#669](https://gitlab.com/hestia-earth/hestia-glossary/issues/669)
* **operation:** add `Dry fractionation` and `Wet fractionation` ([58e34f7](https://gitlab.com/hestia-earth/hestia-glossary/commit/58e34f733b8c8e02bdf9ea30b8cb94d2fc9d4806)), closes [#1377](https://gitlab.com/hestia-earth/hestia-glossary/issues/1377)
* **operation:** add `Grazing animals` ([dfe82cd](https://gitlab.com/hestia-earth/hestia-glossary/commit/dfe82cd604f54eb232330f2135e59a56f4ba9b6d)), closes [#1415](https://gitlab.com/hestia-earth/hestia-glossary/issues/1415)
* **operation:** add `Hunting animals` ([2a50846](https://gitlab.com/hestia-earth/hestia-glossary/commit/2a5084652a581f761faefde111a048ec096affbd)), closes [#1383](https://gitlab.com/hestia-earth/hestia-glossary/issues/1383)
* **operation:** add `Land cleaning, by hand` and `Land cleaning, machine unspecified` ([102e2c3](https://gitlab.com/hestia-earth/hestia-glossary/commit/102e2c364f7f292b31b975f231a4aa82c46d0aa5)), closes [#1415](https://gitlab.com/hestia-earth/hestia-glossary/issues/1415)
* **operation:** rename `Collecting eggs, with machine` `Collecting eggs, machine unspecified` ([5ef9c8b](https://gitlab.com/hestia-earth/hestia-glossary/commit/5ef9c8bac093e76a0f03080bfb39e1c5100a489c))
* **organicFertiliser:** add `Sugarcane molasses (kg N)` and `Sugarcane molasses (kg mass)` ([ec32f8a](https://gitlab.com/hestia-earth/hestia-glossary/commit/ec32f8ae89335d86a2449994c4f9c13fffc2ceff)), closes [#1417](https://gitlab.com/hestia-earth/hestia-glossary/issues/1417)
* **pesticideBrandName:** add `grazonPro` ([46d0b5a](https://gitlab.com/hestia-earth/hestia-glossary/commit/46d0b5a7126e7f5f86c676012dde73c1d85f6aec))
* **pesticideBrandName:** add `lincano` ([6b5d5ee](https://gitlab.com/hestia-earth/hestia-glossary/commit/6b5d5ee944e88a57f4216973817a90c5045ab799))
* **pesticideBrandName:** add multiple terms ([80f6b3f](https://gitlab.com/hestia-earth/hestia-glossary/commit/80f6b3f5b7082f0a94d2ce4a5cad64123fa3dd4b))
* **processedFood:** add `Pig meal` ([03e03ba](https://gitlab.com/hestia-earth/hestia-glossary/commit/03e03ba26249d94e480449f8f0dc4e3a3acef5ca))
* **processedFood:** add `processedFood-property-default-lookup.xlsx` ([503f917](https://gitlab.com/hestia-earth/hestia-glossary/commit/503f917eb496d669737b9fbe9877d7fbc51342f3)), closes [#1292](https://gitlab.com/hestia-earth/hestia-glossary/issues/1292)
* **processedFood:** add multiple oat and broad bean processed products ([b6da455](https://gitlab.com/hestia-earth/hestia-glossary/commit/b6da4558f118af75350f8f671d9267644dfcdd27)), closes [#1373](https://gitlab.com/hestia-earth/hestia-glossary/issues/1373)
* **tillage:** add `Vertical tillage` ([677978a](https://gitlab.com/hestia-earth/hestia-glossary/commit/677978a362d1e654e1ad91dfa4e83a0ffdf6e466)), closes [#1416](https://gitlab.com/hestia-earth/hestia-glossary/issues/1416)


### Bug Fixes

* **emission:** fix typos in new terms ([79d1e64](https://gitlab.com/hestia-earth/hestia-glossary/commit/79d1e641b52522dc9c30d4e0471915dc718e5a9a))
* **fertiliserBrandName:** delete emissions related lookups ([c2aeef5](https://gitlab.com/hestia-earth/hestia-glossary/commit/c2aeef5594d735fe7f8bf169e0b253a651717b70))

### [0.49.2](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.49.1...v0.49.2) (2024-06-11)

### [0.49.1](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.49.0...v0.49.1) (2024-05-28)


### Features

* **animalProduct:** add `gfliMapping` lookup ([4a1da86](https://gitlab.com/hestia-earth/hestia-glossary/commit/4a1da8633387a8ae442bde3b67c31683b6f8a8d6))
* **crop:** add `Cereal, husk`, `Generic crop, husk`, and `Pulse, pod husk` ([3945e1e](https://gitlab.com/hestia-earth/hestia-glossary/commit/3945e1e54c59950e5781c9ee97ef235f1eb4df34)), closes [#1403](https://gitlab.com/hestia-earth/hestia-glossary/issues/1403)
* **crop:** add `Clover, hay` and `Clover, silage` ([99561a1](https://gitlab.com/hestia-earth/hestia-glossary/commit/99561a13d65519465691d7133a98406b23de8363)), closes [#1407](https://gitlab.com/hestia-earth/hestia-glossary/issues/1407)
* **crop:** add `gfliMapping` lookup ([4b372d7](https://gitlab.com/hestia-earth/hestia-glossary/commit/4b372d75f7140037a3c60ba5fc1bf0d0a490ddc5))
* **crop:** add `Sugarcane, silage` ([ee2e96f](https://gitlab.com/hestia-earth/hestia-glossary/commit/ee2e96fb21a81be8ff1035fad81e154a64c8d5e7)), closes [#1385](https://gitlab.com/hestia-earth/hestia-glossary/issues/1385)
* **emission:** add new terms for dead organic matter ([d50b450](https://gitlab.com/hestia-earth/hestia-glossary/commit/d50b45028989a1a1d78996277c6fdcbc8e53d071)), closes [#669](https://gitlab.com/hestia-earth/hestia-glossary/issues/669)
* **feedFoodAdditive:** add `gfliMapping` lookup ([2ef5dab](https://gitlab.com/hestia-earth/hestia-glossary/commit/2ef5dabf8bf398426431902427b62096a80fcef0))
* **fertiliser:** add `npkYaraSuperPk02030` ([0cae114](https://gitlab.com/hestia-earth/hestia-glossary/commit/0cae114f7ced83703ead8cc8e0a408bedf9208e0))
* **fertiliserBrandName:** add multiple brands ([6f6aeef](https://gitlab.com/hestia-earth/hestia-glossary/commit/6f6aeef8767a361f2bf4ea97f5b3c8da35e831a3))
* **landCover:** add new `setAside` term ([93c8a86](https://gitlab.com/hestia-earth/hestia-glossary/commit/93c8a8651f67a21f69973e8af1b1a8594f041d56))
* **model:** add gfli model ([b1f7978](https://gitlab.com/hestia-earth/hestia-glossary/commit/b1f7978f70bdf8fe9e428ab9f6836f1dc7015b94))
* **model:** fix description ([f5ea5cb](https://gitlab.com/hestia-earth/hestia-glossary/commit/f5ea5cba4290b69e9a15d52b4ba6045b8567f460))
* **pesticideBrandName:** add multiple pesticide terms ([d07672f](https://gitlab.com/hestia-earth/hestia-glossary/commit/d07672f70748dc731d61dc104c5f6af3bb285c2c))
* **processedFood:** add `gfliMapping` lookup ([b761604](https://gitlab.com/hestia-earth/hestia-glossary/commit/b76160451b2259c8b0ebb60730a4bbe6f0d53cd6))
* **region:** add lookups for Ember energy sources and ecoinvent mapping ([86a3521](https://gitlab.com/hestia-earth/hestia-glossary/commit/86a3521303b58caad26363dec9c1cc2471985d58))
* **soilType:** add `arrayTreatment` lookup ([a22c404](https://gitlab.com/hestia-earth/hestia-glossary/commit/a22c40462288b3936d01de4ffb642ebe8cfed911))
* **usdaSoilType:** add `arrayTreatment` lookup ([a2babd1](https://gitlab.com/hestia-earth/hestia-glossary/commit/a2babd1d5c6ec17c95e165ac867a867d02d71861))


### Bug Fixes

* **landCover:** remove min limit of days for short fallow terms ([5437ecd](https://gitlab.com/hestia-earth/hestia-glossary/commit/5437ecd0cdff653c8e38af96f7f45bcd9227e05c))

## [0.49.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.48.0...v0.49.0) (2024-05-14)


### ⚠ BREAKING CHANGES

* **liveAnimal:** rename `primaryMeatProductFAO` lookup `primaryMeatProductFaoProductionTermId`
* **animalProduct:** rename `milkYieldPracticeId` lookup `milkYieldPracticeTermId`
* **liveAnimal:** rename `milkYieldPracticeTermId` lookup `milkYieldPracticeTermIds`
* **forage:** Rename `landCoverId` `landCoverTermId`
* **crop:** Rename `landCoverId` `landCoverTermId`
* **inorganicFertiliser:** rename `mustInclude` lookup `complementaryTermIds`
* **waterRegime:** rename `allowedRiceTermId` lookup `allowedRiceTermIds`

### Features

* **animalProduct:** add `animalProductGroupingFAO` lookup values to "liveweight" terms ([1ddfb99](https://gitlab.com/hestia-earth/hestia-glossary/commit/1ddfb99ed6389da99d8e178376cf80d655d7351e)), closes [#1397](https://gitlab.com/hestia-earth/hestia-glossary/issues/1397)
* **animalProduct:** add `generateImpactAssessment` lookup ([cbf3d9d](https://gitlab.com/hestia-earth/hestia-glossary/commit/cbf3d9d0a2da36a7d949e7fa0b2a64737450789b)), closes [#1387](https://gitlab.com/hestia-earth/hestia-glossary/issues/1387)
* **animalProduct:** rename `milkYieldPracticeId` lookup `milkYieldPracticeTermId` ([0738489](https://gitlab.com/hestia-earth/hestia-glossary/commit/0738489b1c2510b2c1cd50feeb1419964dd93f96)), closes [#1390](https://gitlab.com/hestia-earth/hestia-glossary/issues/1390)
* **inorganicFertiliser:** rename `mustInclude` lookup `complementaryTermIds` ([923bf1d](https://gitlab.com/hestia-earth/hestia-glossary/commit/923bf1dfd8ec55ef0d7b99c5be99764a1a6b5039)), closes [#1393](https://gitlab.com/hestia-earth/hestia-glossary/issues/1393)
* **liveAnimal:** add  `allowedAnimalProductTermIds` lookup ([a1d0bd4](https://gitlab.com/hestia-earth/hestia-glossary/commit/a1d0bd4c3da705c521fc2dc7b15d43a58afb7886)), closes [#1394](https://gitlab.com/hestia-earth/hestia-glossary/issues/1394)
* **liveAnimal:** add `generateImpactAssessment` lookup ([4c3885a](https://gitlab.com/hestia-earth/hestia-glossary/commit/4c3885a4f2c860033f2eefa40d6b191aa74e9f6c)), closes [#1387](https://gitlab.com/hestia-earth/hestia-glossary/issues/1387)
* **liveAnimal:** add `primaryMeatProductFaoPriceTermId` lookup ([e1f9f25](https://gitlab.com/hestia-earth/hestia-glossary/commit/e1f9f25f81184e8f0149297ef94f555eb9e87dac)), closes [#1397](https://gitlab.com/hestia-earth/hestia-glossary/issues/1397)
* **liveAnimal:** rename `milkYieldPracticeTermId` lookup `milkYieldPracticeTermIds` ([289186c](https://gitlab.com/hestia-earth/hestia-glossary/commit/289186c0daebc47a9b5fa0dad5b919614bc76190)), closes [#1390](https://gitlab.com/hestia-earth/hestia-glossary/issues/1390)
* **liveAnimal:** rename `primaryMeatProductFAO` lookup `primaryMeatProductFaoProductionTermId` ([6f7f67b](https://gitlab.com/hestia-earth/hestia-glossary/commit/6f7f67b3ace9181b3d95ccf1294fff3c8a40243c)), closes [#1397](https://gitlab.com/hestia-earth/hestia-glossary/issues/1397)
* **liveAquaticSpecies:** add `generateImpactAssessment` lookup ([1a6a054](https://gitlab.com/hestia-earth/hestia-glossary/commit/1a6a05447b1b3f5f20c418b28377893f57cb0e19)), closes [#1387](https://gitlab.com/hestia-earth/hestia-glossary/issues/1387)
* **model:** add `ecoinventV3AndEmberClimate` ([b99ec7c](https://gitlab.com/hestia-earth/hestia-glossary/commit/b99ec7c16d1911dd3105ca2d8d650a082d91fe13)), closes [#1389](https://gitlab.com/hestia-earth/hestia-glossary/issues/1389)
* **operation:** add `Harvesting tea, with handheld tea picking machine` ([c49a07a](https://gitlab.com/hestia-earth/hestia-glossary/commit/c49a07a5f892d0c45e2eaa595bddb7aba421b12d)), closes [#1388](https://gitlab.com/hestia-earth/hestia-glossary/issues/1388)
* **waterRegime:** rename `allowedRiceTermId` lookup `allowedRiceTermIds` ([b84ffe4](https://gitlab.com/hestia-earth/hestia-glossary/commit/b84ffe4e3e82b69584381340b4a170f241c866ec)), closes [#1393](https://gitlab.com/hestia-earth/hestia-glossary/issues/1393)


### Bug Fixes

* **animalProduct:** `animalProductGroupingFAO` and `animalProductGroupingFAOEquivalent` lookups ([95ffd2b](https://gitlab.com/hestia-earth/hestia-glossary/commit/95ffd2b50d360c83dcc14464d09a7a0261346dcf)), closes [#1337](https://gitlab.com/hestia-earth/hestia-glossary/issues/1337)
* **crop:** rename `landCoverId` lookup ([a9dd1c7](https://gitlab.com/hestia-earth/hestia-glossary/commit/a9dd1c7262aea266410167cb02a366250af54e3c))
* **forage:** rename `landCoverId` lookup ([82cbbef](https://gitlab.com/hestia-earth/hestia-glossary/commit/82cbbef5684479cddd6bf07eccdbae1755c5cd49))

## [0.48.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.47.0...v0.48.0) (2024-04-30)


### ⚠ BREAKING CHANGES

* **region:** rename `Brass` `Brass (Local Authority), Bayelsa, Nigeria`
* **animalProduct:** rename `liveAnimal` lookup `liveAnimalTermId`
* **region:** rename `Red Deer` `Red Deer (Cité), Division No. 8, Alberta, Canada`

### Features

* **animalBreed:** add `Rex rabbit` ([626a677](https://gitlab.com/hestia-earth/hestia-glossary/commit/626a67766cd971c403411fd93ac7d5a46c32bfa8))
* **animalBreed:** add multiple deer species ([526ef0b](https://gitlab.com/hestia-earth/hestia-glossary/commit/526ef0b4569b3bb71454eafb0c0d96b064339d98)), closes [#1351](https://gitlab.com/hestia-earth/hestia-glossary/issues/1351) [#1361](https://gitlab.com/hestia-earth/hestia-glossary/issues/1361)
* **animalManagement:** add `concreteFlooringUse` ([c06d49c](https://gitlab.com/hestia-earth/hestia-glossary/commit/c06d49c089e2b49617d3ace5ceb4dbf1574fbe37)), closes [#1376](https://gitlab.com/hestia-earth/hestia-glossary/issues/1376)
* **animalProduct:** add multiple terms for "game", "rabbit" and "deer" ([b9777be](https://gitlab.com/hestia-earth/hestia-glossary/commit/b9777be0334682a876faeaaffe178cdc509b0963)), closes [#1358](https://gitlab.com/hestia-earth/hestia-glossary/issues/1358) [#1368](https://gitlab.com/hestia-earth/hestia-glossary/issues/1368) [#1360](https://gitlab.com/hestia-earth/hestia-glossary/issues/1360)
* **animalProduct:** rename `liveAnimal` lookup `liveAnimalTermId` ([82a6960](https://gitlab.com/hestia-earth/hestia-glossary/commit/82a6960ad7b812ae63f47cc2249558eba5b1512d)), closes [#1336](https://gitlab.com/hestia-earth/hestia-glossary/issues/1336)
* **biologicalControlAgent:** add `Agroverm bacillus` and `Erwix` ([55e0c96](https://gitlab.com/hestia-earth/hestia-glossary/commit/55e0c96ccff2ff2a8b56016f8a037ff08616665d)), closes [#1153](https://gitlab.com/hestia-earth/hestia-glossary/issues/1153)
* **building:** add `almeriaStyleGreenhouse` ([0e650f0](https://gitlab.com/hestia-earth/hestia-glossary/commit/0e650f0668850840dce3896d41a779041a60373d)), closes [#1345](https://gitlab.com/hestia-earth/hestia-glossary/issues/1345)
* **building:** add `animalEnclosure` ([7deb576](https://gitlab.com/hestia-earth/hestia-glossary/commit/7deb576ecabf1f23194045949d03b7a5f5e5b935)), closes [#1376](https://gitlab.com/hestia-earth/hestia-glossary/issues/1376)
* **crop:** add "rooting depth" lookups for several vegetables ([0693d71](https://gitlab.com/hestia-earth/hestia-glossary/commit/0693d71ac6e9387f0d96a0b33afa754957a4dbef)), closes [#1353](https://gitlab.com/hestia-earth/hestia-glossary/issues/1353)
* **crop:** add `Maize, bran` ([e857513](https://gitlab.com/hestia-earth/hestia-glossary/commit/e8575132d6e271d34b58d5355bd5d2978ce73f77)), closes [#1363](https://gitlab.com/hestia-earth/hestia-glossary/issues/1363)
* **cropResidueManagement:** add `arrayTreatment` lookup ([0ad3c93](https://gitlab.com/hestia-earth/hestia-glossary/commit/0ad3c939a3b79923c374a53ba0c1acdf1437c4f6))
* **crop:** update crop residue lookups for vegetable crops ([01c6d73](https://gitlab.com/hestia-earth/hestia-glossary/commit/01c6d7324b8100a1a810d8b6d335c63399df6799)), closes [#1353](https://gitlab.com/hestia-earth/hestia-glossary/issues/1353)
* **emission:** add `H2S, to air, waste treatment` ([593a225](https://gitlab.com/hestia-earth/hestia-glossary/commit/593a2256cf01c20fb4db5e295356603ea339eaa8)), closes [#1382](https://gitlab.com/hestia-earth/hestia-glossary/issues/1382)
* **excreta:** add terms for rabbits excreta ([308d666](https://gitlab.com/hestia-earth/hestia-glossary/commit/308d6665993de8a1dca74ea4c7a437dfa487f32d)), closes [#1365](https://gitlab.com/hestia-earth/hestia-glossary/issues/1365)
* **fertiliserBrandName:** add `Tecamin Max` and `Tradebor` ([7cd2b5e](https://gitlab.com/hestia-earth/hestia-glossary/commit/7cd2b5e43b96582fec7a70793f270cbfeeb51b0b)), closes [#1374](https://gitlab.com/hestia-earth/hestia-glossary/issues/1374)
* **landCover:** add `arrayTreatment` lookup ([f1006b4](https://gitlab.com/hestia-earth/hestia-glossary/commit/f1006b40472a9f8e64128057dda2a79cd693a526))
* **landCover:** add `IPCC_LAND_USE_CATEGORY` values for `longFallow` and `longBareFallow` terms ([2d3ab6b](https://gitlab.com/hestia-earth/hestia-glossary/commit/2d3ab6bc7ea512d06bbfd13f1ca69fc21f1dfae7))
* **landUseManagement:** add `arrayTreatment` lookup ([488b076](https://gitlab.com/hestia-earth/hestia-glossary/commit/488b0767e75dd3ba031694561387362b7d3a6b83))
* **liveAnimal:** add "deer" and "rabbit" terms ([945f50f](https://gitlab.com/hestia-earth/hestia-glossary/commit/945f50fce371135b6996feec27e86c36ab8c8666)), closes [#1369](https://gitlab.com/hestia-earth/hestia-glossary/issues/1369) [#1356](https://gitlab.com/hestia-earth/hestia-glossary/issues/1356)
* **material:** add terms for "Brass" and "Lead" ([7877f6b](https://gitlab.com/hestia-earth/hestia-glossary/commit/7877f6bb3de51e38f645a398f42b537511b4e5f5)), closes [#1359](https://gitlab.com/hestia-earth/hestia-glossary/issues/1359)
* **pesticideBrandName:** add `Funguran OH 50 WP` ([5d19444](https://gitlab.com/hestia-earth/hestia-glossary/commit/5d19444b091528b1a878e2c2b628df664a77e652)), closes [#1153](https://gitlab.com/hestia-earth/hestia-glossary/issues/1153)
* **processingAid:** add three terms ([4505ea4](https://gitlab.com/hestia-earth/hestia-glossary/commit/4505ea4110f3c50cb232f33aba3c5ada66a4d35a)), closes [#1364](https://gitlab.com/hestia-earth/hestia-glossary/issues/1364)
* **region:** rename `Brass` `Brass (Local Authority), Bayelsa, Nigeria` ([996cd25](https://gitlab.com/hestia-earth/hestia-glossary/commit/996cd259282d71ce42bfe3fd3773200904e482f4))
* **region:** rename `Red Deer` `Red Deer (Cité), Division No. 8, Alberta, Canada` ([eb376f3](https://gitlab.com/hestia-earth/hestia-glossary/commit/eb376f383107cd285d9de93bf8d09143f701e558))
* **soilAmendment:** add `Bioplex`, `Panorama Bio Plus`, and `Meba` ([839ed0c](https://gitlab.com/hestia-earth/hestia-glossary/commit/839ed0cfff59de94566d25644ce8c28f00478e90)), closes [#1374](https://gitlab.com/hestia-earth/hestia-glossary/issues/1374)
* **standardsLabels:** add `Wild-caught` ([fc4065f](https://gitlab.com/hestia-earth/hestia-glossary/commit/fc4065f1eb02ba793565bf09cd1716fdc23382b4)), closes [#1362](https://gitlab.com/hestia-earth/hestia-glossary/issues/1362)
* **tillage:** add `arrayTreatment` lookup ([b7a1ed1](https://gitlab.com/hestia-earth/hestia-glossary/commit/b7a1ed17582f6574694eade95ec7c4efa134e72e))
* **waterRegime:** add `arrayTreatment` lookup ([16b25e5](https://gitlab.com/hestia-earth/hestia-glossary/commit/16b25e55df86378d03600df01deea7f225ac0cc1))


### Bug Fixes

* **animalProduct:** remove double value from `liveAnimal` lookup for "Milk, cow" terms ([eee1a5f](https://gitlab.com/hestia-earth/hestia-glossary/commit/eee1a5fc8fe8e2ef614a02ea2d7c601c995a6699)), closes [#1336](https://gitlab.com/hestia-earth/hestia-glossary/issues/1336)
* **material:** `density` of "Zinc", "Copper" and "Bronze" terms ([5ab9c58](https://gitlab.com/hestia-earth/hestia-glossary/commit/5ab9c58d55b571316b39d0eb99f238783809dc88))

## [0.47.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.46.1...v0.47.0) (2024-04-15)


### ⚠ BREAKING CHANGES

* **landUseManagement:** `organicFertiliserOrSoilCarbonIncreasingAmendmentUsed` replaced by
`organicFertiliserUsed` and `amendmentIncreasingSoilCarbonUsed`

### Features

* **landUseManagement:** split `organicFertiliserOrSoilCarbonIncreasingAmendmentUsed` into two terms ([0c56a3e](https://gitlab.com/hestia-earth/hestia-glossary/commit/0c56a3e0081ba0db8b05bed953386d030ca82b31))
* **operation:** add `Irrigating, by hand` and `Irrigating, machine unspecified` ([d84bb9c](https://gitlab.com/hestia-earth/hestia-glossary/commit/d84bb9c3abab4c9d8278fb036ab00dce9fc2925b)), closes [#1352](https://gitlab.com/hestia-earth/hestia-glossary/issues/1352)
* **veterinaryDrug:** move nine terms to the `feedFoodAdditive` glossary ([76f11fd](https://gitlab.com/hestia-earth/hestia-glossary/commit/76f11fdabda61eca25a662ef3924de0e3c0e4909))
* **waterRegime:** add lookup `irrigated` ([e706431](https://gitlab.com/hestia-earth/hestia-glossary/commit/e70643178d8cfd7b314aef94929658db7c5d47dc))


### Bug Fixes

* **measurement:** fix wrong values for lookup `depthSensitive` ([564df8b](https://gitlab.com/hestia-earth/hestia-glossary/commit/564df8bd872912b85f8218ba88f49f27880101ef))

### [0.46.1](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.46.0...v0.46.1) (2024-04-02)


### Bug Fixes

* **crop:** re-generate property lookup ([7824b91](https://gitlab.com/hestia-earth/hestia-glossary/commit/7824b91238f9f88fc9289f3e904d488e06959232))

## [0.46.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.45.0...v0.46.0) (2024-04-02)


### ⚠ BREAKING CHANGES

* **processedFood:** rename `Whey` `Whey, liquid`
* **waterRegime:** Renames
`Deep water, water depth > 50-100 cm` to
`Rainfed, deep water, water depth > 50-100 cm` and
`Deep water, water depth > 100 cm` to
`Rainfed, deep water, water depth > 100 cm`
* **waterRegime:** Renames
`Irrigated, continuously flooded` to
`Irrigated, surface irrigation, continuously flooded`,
`Irrigated, single drainage period` to
`Irrigated, surface irrigation, single drainage period`, and
`Irrigated, multiple drainage periods` to
`Irrigated, surface irrigation, multiple drainage periods`
* **waterRegime:** Renames `Irrigated, surface irrigation`
to `Irrigated, surface irrigation, drainage regime unspecified`
* **waterRegime:** Changes term names from using `-` as
separators to using `,` to align with other glossaries
* **waterRegime:** Deletes term `Irrigated - Manual irrigation`

### Features

* add `depthSensitive` lookup to soil terms ([38600bc](https://gitlab.com/hestia-earth/hestia-glossary/commit/38600bcb681b5022094ee90b5b928611f2328a4b))
* add lookup `reccomendAddingDepth` to `soilType` and `usdaSoilType` ([a4fa4a9](https://gitlab.com/hestia-earth/hestia-glossary/commit/a4fa4a9293722cae75a2b039c938ee77999a1a7b))
* **crop:** add `Cassava, chip` ([b58f9e2](https://gitlab.com/hestia-earth/hestia-glossary/commit/b58f9e2f7f460f910f042f29b1c7aeba9a0403c4))
* **crop:** add `Water spinach, shoot` ([8f9e110](https://gitlab.com/hestia-earth/hestia-glossary/commit/8f9e1106fdfaab7990e9a9393cd8e6a12157d81a))
* **cropResidue:** add `sumMax100Group` lookup ([bd0e242](https://gitlab.com/hestia-earth/hestia-glossary/commit/bd0e242c7f7ce6d1db16b01d3ffee66fa4e1be12)), closes [#1348](https://gitlab.com/hestia-earth/hestia-glossary/issues/1348)
* **inorganicFertiliser:** add IPCC 2019 NH3 and NOx lookups ([680ea34](https://gitlab.com/hestia-earth/hestia-glossary/commit/680ea34c6a39be212b9c7730fdc0b04cb7414e66))
* **landCover:** add `Water spinach plant` ([e644c46](https://gitlab.com/hestia-earth/hestia-glossary/commit/e644c467c451060917a0aa91f0f92436c82712a3))
* **measurement:** add `sumMax100Group` lookup ([163fe21](https://gitlab.com/hestia-earth/hestia-glossary/commit/163fe21167eef433fab1a43b3f5f56ca1e79e210)), closes [#1348](https://gitlab.com/hestia-earth/hestia-glossary/issues/1348)
* **measurement:** set all per hectare terms as `depthSensitive = TRUE` ([fb44cf7](https://gitlab.com/hestia-earth/hestia-glossary/commit/fb44cf71d058cc059070ad9747bdd941890da589))
* **measurement:** set soil carbon concentration terms to `depthSensitive = FALSE` ([1242a9d](https://gitlab.com/hestia-earth/hestia-glossary/commit/1242a9d535f1ba99e9c4fcec1ecd81a613d77d17))
* **operation:** add `Pelletising, machine unspecified` ([abf30bd](https://gitlab.com/hestia-earth/hestia-glossary/commit/abf30bd271d8c6c3212463faf7487e5169f4d9e1))
* **organicFertiliser:** add default nitrogen properties to "kg N" terms ([0012078](https://gitlab.com/hestia-earth/hestia-glossary/commit/0012078198fd92fa307f6936b7ca98717b32cf08))
* **organicFertiliser:** add IPCC 2019 NH3 and NOx lookups ([29413fd](https://gitlab.com/hestia-earth/hestia-glossary/commit/29413fd79f7d9c52571a54745f3e7853d0f08d91))
* **processedFood:** add `Whey, powder` ([7336874](https://gitlab.com/hestia-earth/hestia-glossary/commit/73368743d88c0d9f002dbb689c6180323f2bf360))
* **processedFood:** rename `Whey` `Whey, liquid` ([fbe4a44](https://gitlab.com/hestia-earth/hestia-glossary/commit/fbe4a442df4c83e0acd4244c65ca6e5124dadc5d))
* **property:** add `Parity` ([848f97b](https://gitlab.com/hestia-earth/hestia-glossary/commit/848f97b5fffe41cacd362fd3e18ca499978a0f89)), closes [#1343](https://gitlab.com/hestia-earth/hestia-glossary/issues/1343)
* **tillage:** add `sumIs100Group` lookup ([70f0c2f](https://gitlab.com/hestia-earth/hestia-glossary/commit/70f0c2f0daf738c91478d2313a2d9a4831133664)), closes [#1348](https://gitlab.com/hestia-earth/hestia-glossary/issues/1348)
* **waterRegime:** delete `defaultValue` lookup ([3ddffab](https://gitlab.com/hestia-earth/hestia-glossary/commit/3ddffab9e5dfa9508cc8f82fa19fa4444b970e7c))
* **waterRegime:** delete term `Irrigated - Manual irrigation` ([625a962](https://gitlab.com/hestia-earth/hestia-glossary/commit/625a96250154f66b8ab3b4c002bd347f876b437f))
* **waterRegime:** rename `Deep water...` terms ([b15e64d](https://gitlab.com/hestia-earth/hestia-glossary/commit/b15e64dbb244187f60e6f95f1ca9487ccdbd42b5))
* **waterRegime:** rename `Irrigated, surface irrigation` ([ce1642a](https://gitlab.com/hestia-earth/hestia-glossary/commit/ce1642acd46b55dde7b616497a35884a37f35e28))
* **waterRegime:** renames multiple terms to specify surface irrigation ([3a183af](https://gitlab.com/hestia-earth/hestia-glossary/commit/3a183afb25d758951db41f30efb1806d379edfd3))
* **waterRegime:** replace `percentAreaSumIs100` with `sumIs100Group` ([16dcf91](https://gitlab.com/hestia-earth/hestia-glossary/commit/16dcf9123d4c213393518f67c60e33c0016790e0)), closes [#1348](https://gitlab.com/hestia-earth/hestia-glossary/issues/1348)


### Bug Fixes

* **crop:** change `landCoverId` lookup values for rice terms for gap-filling model ([59e058d](https://gitlab.com/hestia-earth/hestia-glossary/commit/59e058d1cabafcdc4199aeb005cccca732aecd6e))
* **inorganicFertiliser:** divide IPCC 2019 NOx and NH3 lookups by 100 ([3c38f99](https://gitlab.com/hestia-earth/hestia-glossary/commit/3c38f993650eb276e4ff3f85d68aed3e57787c22))
* **measurement:** set `reccomendAddingDepth` to `TRUE` for soil temperature ([a1eeb63](https://gitlab.com/hestia-earth/hestia-glossary/commit/a1eeb6350bdeb0112e1e7822e561e060ae05a78c))
* **measurement:** use FALSE vs `-` and `not required` for `depthSensitive` lookup ([0e13620](https://gitlab.com/hestia-earth/hestia-glossary/commit/0e1362045152daf7b0cc911621c5b3cde2c86707))


* **waterRegime:** changes names to use comma rather than `-` ([2823464](https://gitlab.com/hestia-earth/hestia-glossary/commit/28234640ca7b700b6d7a9154a3489727f80809ee))

## [0.45.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.44.0...v0.45.0) (2024-03-18)


### ⚠ BREAKING CHANGES

* **landUseManagement:** Moves
`Non flooded pre-season (less than 180 days)`,
`Non flooded pre-season (more than 180 days)`,
`Flooded pre-season (more than 30 days)`,
`Non flooded pre-season (more than 365 days)`,
`Unknown pre-season`, `Number of days flooded`,
`Irrigation interval`, and `Number of irrigations`
from `waterRegime` to `landUseManagement`.
* **pesticideBrandName:** Deletes `(S)-Methoprene Technical`
as duplicated with another term
* **pesticideBrandName:** Renames `Per-Ox` to
`Per-Ox (pesticide brand name)`
* **waterRegime:** delete `IPCC_2019_CH4_rice_SFw_min` lookup
* **waterRegime:** delete `IPCC_2019_CH4_rice_SFw_max` lookup
* **waterRegime:** delete `IPCC_2019_CH4_rice_SFw_sd` lookup
* **waterRegime:** delete `IPCC_2019_CH4_rice_SFp_min` lookup
* **waterRegime:** delete `IPCC_2019_CH4_rice_SFp_max` lookup
* **waterRegime:** delete `IPCC_2019_CH4_rice_SFp_sd` lookup
* **liveAquaticSpecies:** delete `Tambaqui`

### Features

* **animalProduct:** add "allowedExcreta" lookups ([3fa2ca3](https://gitlab.com/hestia-earth/hestia-glossary/commit/3fa2ca33da47b77d31a034cfd6a90ffcd6252538)), closes [#1327](https://gitlab.com/hestia-earth/hestia-glossary/issues/1327)
* **crop:** add `global_economic_value_share` to `coconutMeat` ([cc0945e](https://gitlab.com/hestia-earth/hestia-glossary/commit/cc0945e3665a3a977ce1f30093a9f6659fa34d15))
* **fertiliserBrandName:** add `Nova Mag-S` ([673bbe3](https://gitlab.com/hestia-earth/hestia-glossary/commit/673bbe366981b55a1416bb4de6702f10e6d6eead)), closes [#1325](https://gitlab.com/hestia-earth/hestia-glossary/issues/1325)
* **inorganicFertiliser:** add `Manganese sulphate (kg Mn)` ([e10d13f](https://gitlab.com/hestia-earth/hestia-glossary/commit/e10d13fac1faba9aedba935f3667b2d37193fc5d)), closes [#1340](https://gitlab.com/hestia-earth/hestia-glossary/issues/1340)
* **inorganicFertiliser:** add `Sylvinite Ore (kg K2O)` ([3c1374e](https://gitlab.com/hestia-earth/hestia-glossary/commit/3c1374ec28fa83019a70cea65b939b84887289e3)), closes [#1339](https://gitlab.com/hestia-earth/hestia-glossary/issues/1339)
* **landUseManagement:** move terms from `waterRegime` ([d5a8850](https://gitlab.com/hestia-earth/hestia-glossary/commit/d5a88503915daf01754b24a4979ae7465a9d2e2a))
* **landUseManagement:** restrict plantation terms with `productTermIdsAllowed` ([4050099](https://gitlab.com/hestia-earth/hestia-glossary/commit/405009983d22d7147c1dabcb27bf615f8819bc80)), closes [#880](https://gitlab.com/hestia-earth/hestia-glossary/issues/880)
* **liveAquaticSpecies:** add "allowedExcreta" lookups ([f1bdd3d](https://gitlab.com/hestia-earth/hestia-glossary/commit/f1bdd3d223ec259e5f71c374a962b3d3ffbfe5a4)), closes [#1327](https://gitlab.com/hestia-earth/hestia-glossary/issues/1327)
* **measurement:** set `skipAggregation` on multiple terms ([1fbbb46](https://gitlab.com/hestia-earth/hestia-glossary/commit/1fbbb46a1fcdc34b51c21b64bbba1a7fd0e28ae8))
* **operation:** add new terms for electric water pumps ([c7a53ca](https://gitlab.com/hestia-earth/hestia-glossary/commit/c7a53ca134ae2e8d04f3102eb09517f5463ba414))
* **operation:** add synonyms to stubble cultivation terms ([ae239ab](https://gitlab.com/hestia-earth/hestia-glossary/commit/ae239abaf9533357e5437a23d797e426389b0cac)), closes [#1330](https://gitlab.com/hestia-earth/hestia-glossary/issues/1330)
* **otherOrganicChemical:** add `CAS-68891-38-3` and `CAS-78330-21-9` ([1789a5b](https://gitlab.com/hestia-earth/hestia-glossary/commit/1789a5bd38a7e503457b7e903165640457f9922c))
* **pesticideBrandName:** add 16 pesticide brand names ([5586f66](https://gitlab.com/hestia-earth/hestia-glossary/commit/5586f66e1a5ef8d6a4a46c0b118fa19f6bd9e0e8))
* **soilAmendment:** add ecoinvent proxy for `gypsum` ([8d3603b](https://gitlab.com/hestia-earth/hestia-glossary/commit/8d3603bfe6a146d7ad52e4ff9f47c48f7d89fbf3))
* **soilType:** add `synonyms` to `Histosol` term ([ed8dddf](https://gitlab.com/hestia-earth/hestia-glossary/commit/ed8dddf5d8554d167fa5978b252c67fa8fd07fab)), closes [#1329](https://gitlab.com/hestia-earth/hestia-glossary/issues/1329)
* **soilType:** set `siteTypesAllowed` ([6667564](https://gitlab.com/hestia-earth/hestia-glossary/commit/66675641780629c2f23ec78be058a09023e53de7))
* **system:** add `Biofloc aquaculture system` ([ac5fb44](https://gitlab.com/hestia-earth/hestia-glossary/commit/ac5fb447bfbd0b7f766e8edbc1a967db9bde1a40)), closes [#1332](https://gitlab.com/hestia-earth/hestia-glossary/issues/1332)
* **system:** update lookups to align to latest glossaries ([8c2d860](https://gitlab.com/hestia-earth/hestia-glossary/commit/8c2d86080efe5469973adca6f2433ade52ed1c13))
* **usdaSoilType:** add `synonyms` to `Histosols` term ([ffb7638](https://gitlab.com/hestia-earth/hestia-glossary/commit/ffb763876d901941ee495388cda8c2263853f868)), closes [#1329](https://gitlab.com/hestia-earth/hestia-glossary/issues/1329)
* **waterRegime:** add SFp factor for `Unknown pre-season water regime` ([3c7bcfd](https://gitlab.com/hestia-earth/hestia-glossary/commit/3c7bcfd7f7b3d27303b740ebb091ada21e8e7dda)), closes [#1334](https://gitlab.com/hestia-earth/hestia-glossary/issues/1334)


### Bug Fixes

* **characterisedIndicator:** error on units on eutrophication term ([03be5bb](https://gitlab.com/hestia-earth/hestia-glossary/commit/03be5bba000859c516d9d86865656a9ff4ff5f0b)), closes [#1328](https://gitlab.com/hestia-earth/hestia-glossary/issues/1328)
* **emission:** add animal products to `productTermIdsAllowed` for `CH4, to air, enteric methane` ([eab0bb7](https://gitlab.com/hestia-earth/hestia-glossary/commit/eab0bb7cf467d0ee1ee235b7ca7d323557d0dd90)), closes [#1333](https://gitlab.com/hestia-earth/hestia-glossary/issues/1333)
* **landUseManagement:** errors in `dataState` ([2bf5101](https://gitlab.com/hestia-earth/hestia-glossary/commit/2bf51013e37281c15042683db474f07ad29b2909))
* **liveAquaticSpecies:** remove `Tambaqui` as it is a duplicate of `Cachama` ([4538f6c](https://gitlab.com/hestia-earth/hestia-glossary/commit/4538f6c737ada6d96c208f205686795d2f6eb1bf))
* **measurement:** remove `agri-food processor` from `siteTypesAllowed` for terms ([f20dc67](https://gitlab.com/hestia-earth/hestia-glossary/commit/f20dc671ce9cd9429bd6949a315ac0fc3d0cb362)), closes [#1341](https://gitlab.com/hestia-earth/hestia-glossary/issues/1341)
* **pesticideAI:** move `CAS-60240-47-3` from `otherOrganicChemical` ([82106a3](https://gitlab.com/hestia-earth/hestia-glossary/commit/82106a3aab736763c62c3deeb1769f52767204fb))
* **pesticideBrandName:** delete `(S)-Methoprene Technical` ([35f691a](https://gitlab.com/hestia-earth/hestia-glossary/commit/35f691a51aacf4c7f0dac3b9169bfbdaa6bf6aa1))
* **pesticideBrandName:** delete `Atroban 11% EC` ([9df3655](https://gitlab.com/hestia-earth/hestia-glossary/commit/9df3655ed886fc4822ac157c6b627dabae9899a0))
* **pesticideBrandName:** delete `Tri-Chlor Granular` ([83a21de](https://gitlab.com/hestia-earth/hestia-glossary/commit/83a21de6cfc9192dd50fddf13a9007c49cb33abc))
* **pesticideBrandName:** rename `Superchlor` to `Super Chlor Shock` ([dcec984](https://gitlab.com/hestia-earth/hestia-glossary/commit/dcec98425524bdefbfc9be7ba852a8863312b62b))


* **pesticideBrandName:** rename `Per-Ox` to `Per-Ox (pesticide brand name)` ([9e3ec57](https://gitlab.com/hestia-earth/hestia-glossary/commit/9e3ec57ea2b19973102d7e09d6661d145e560d90))
* **waterRegime:** restructure `IPCC_2019_CH4_rice_SFw` and `IPCC_2019_CH4_rice_SFp` lookups ([8e53ce3](https://gitlab.com/hestia-earth/hestia-glossary/commit/8e53ce3b4fc0e8d0aef0eed9329cdd4d649d653a))

## [0.44.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.43.0...v0.44.0) (2024-03-04)


### ⚠ BREAKING CHANGES

* move terms that are "fresh forage" from `crop` to `forage`
* move terms that are not "fresh forage" from `forage` to `crop`
* **pesticideAI:** Renames `Boron triflouride` to
`Boron trifluoride`
* **cropResidueManagement:** Deletes `Residue burnt (before allowing for combustion factor)`
* **animalProduct:** rename `mjKgEVwoolNetEnergyWoolIpcc019` lookup `mjKgEvWoolNetEnergyWoolIpcc2019`
* **liveAnimal:** rename `percentageYmMethaneConversionFactorEntericFermentationIPCC2019` lookup
`percentageYmMethaneConversionFactorEntericFermentation`

### Features

* **animalManagement:** add `defaultFatContentEvMilkIpcc2019` lookup ([dce071c](https://gitlab.com/hestia-earth/hestia-glossary/commit/dce071c060f6d6c4ae271ce6510fe4aa6c63cf6c)), closes [#1316](https://gitlab.com/hestia-earth/hestia-glossary/issues/1316)
* **animalProduct:** add `allowedLiveAnimalTermIds` lookup ([6a074f8](https://gitlab.com/hestia-earth/hestia-glossary/commit/6a074f85c05f8793846401ca1d84560b0dbbff27)), closes [#1283](https://gitlab.com/hestia-earth/hestia-glossary/issues/1283)
* **animalProduct:** add `defaultPercentageYmMethaneConversionFactorEntericFermentation-sd` lookup ([f60641c](https://gitlab.com/hestia-earth/hestia-glossary/commit/f60641c1344181ff8a7475acbc55e1a451cb2476)), closes [#1315](https://gitlab.com/hestia-earth/hestia-glossary/issues/1315)
* **animalProduct:** add `milkYieldPracticeId` lookup ([4af3363](https://gitlab.com/hestia-earth/hestia-glossary/commit/4af33630f7098222017449ba8978de3ca3df6d63)), closes [#1319](https://gitlab.com/hestia-earth/hestia-glossary/issues/1319)
* **animalProduct:** add lookups needed for enteric fermentation model ([87f98c4](https://gitlab.com/hestia-earth/hestia-glossary/commit/87f98c4be706f2ff9d3c7a3c74cfb855f78ab1d3)), closes [#1290](https://gitlab.com/hestia-earth/hestia-glossary/issues/1290)
* **animalProduct:** update `liveAnimal` lookup for "milk" terms ([8121416](https://gitlab.com/hestia-earth/hestia-glossary/commit/812141621723ccde0b6bd57b74f5805203ecc820)), closes [#1319](https://gitlab.com/hestia-earth/hestia-glossary/issues/1319)
* **crop:** add `Maize, high-moisture preserved grain` ([12e2818](https://gitlab.com/hestia-earth/hestia-glossary/commit/12e2818ebb3c803153a08f02b732d585854e2eeb)), closes [#1296](https://gitlab.com/hestia-earth/hestia-glossary/issues/1296)
* **crop:** add `Triticale, silage` ([667baa2](https://gitlab.com/hestia-earth/hestia-glossary/commit/667baa25f490e8eaae25aa4f76f93377218c3513)), closes [#1323](https://gitlab.com/hestia-earth/hestia-glossary/issues/1323)
* **crop:** add lookup values for global_economic_value_share ([74a02fe](https://gitlab.com/hestia-earth/hestia-glossary/commit/74a02feedba697f929015f38b3fa7bf3bd21845b))
* **crop:** delete lookup `grazedPastureGrassInputId` ([b45ca16](https://gitlab.com/hestia-earth/hestia-glossary/commit/b45ca16b86e6c308fa1142a02881dd7f395417c7)), closes [#1314](https://gitlab.com/hestia-earth/hestia-glossary/issues/1314)
* **cropResidue:** add lookup `decomposesOnField` ([5c7de6d](https://gitlab.com/hestia-earth/hestia-glossary/commit/5c7de6d46618b3e63098dab3119a06150d8cfbf1))
* **cropResidueManagement:** delete `Residue burnt (before allowing for combustion factor)` ([0bf0335](https://gitlab.com/hestia-earth/hestia-glossary/commit/0bf033517ebd8c1d10b04e9ab328b15a992dd6b9)), closes [#1299](https://gitlab.com/hestia-earth/hestia-glossary/issues/1299)
* **emission:** add lookup to state our tolerance for original vs recalculated ([0d4cbf7](https://gitlab.com/hestia-earth/hestia-glossary/commit/0d4cbf7f7c6ccb9546b7b970c61b64721f71f292))
* **forage:** add default properties for "ryegrass" terms ([b168a81](https://gitlab.com/hestia-earth/hestia-glossary/commit/b168a81e66a6a9e51c4973329f827cce56997426)), closes [#1161](https://gitlab.com/hestia-earth/hestia-glossary/issues/1161)
* **forage:** add remaining agrovoc links ([e243b5b](https://gitlab.com/hestia-earth/hestia-glossary/commit/e243b5bf7ebeac8c04ebbbde78012faae50105f3)), closes [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72)
* **fuel:** add higher heating values ([7f55be7](https://gitlab.com/hestia-earth/hestia-glossary/commit/7f55be7122ee64e1443106fd6176047c01faf92e)), closes [#510](https://gitlab.com/hestia-earth/hestia-glossary/issues/510)
* **liveAnimal:** add `defaultPercentageYmMethaneConversionFactorEntericFermentation-sd` lookup ([231e323](https://gitlab.com/hestia-earth/hestia-glossary/commit/231e3238e38a4eea55b8467cb8bebef42a0c3dcc)), closes [#1315](https://gitlab.com/hestia-earth/hestia-glossary/issues/1315)
* **liveAnimal:** add `defaultPercentageYmMethaneConversionFactorEntericFermentation` lookup ([19e479b](https://gitlab.com/hestia-earth/hestia-glossary/commit/19e479b427635ba5763a117b4d04d519715cb948)), closes [#1305](https://gitlab.com/hestia-earth/hestia-glossary/issues/1305)
* **liveAnimal:** add `milkYieldPracticeId` lookup ([96f41c6](https://gitlab.com/hestia-earth/hestia-glossary/commit/96f41c626550b74e26b5f5ae1a5475b740eea9d7)), closes [#1320](https://gitlab.com/hestia-earth/hestia-glossary/issues/1320)
* **material:** add `Animal bedding lime` ([7977466](https://gitlab.com/hestia-earth/hestia-glossary/commit/79774666449f4b9ce36dc2cd6bb02f365da7736a)), closes [#1302](https://gitlab.com/hestia-earth/hestia-glossary/issues/1302)
* move terms between `crop` and `forage` glossary ([06dd1d1](https://gitlab.com/hestia-earth/hestia-glossary/commit/06dd1d1e190b0a75d1b605d7a4f07f1bc95ce67e)), closes [#1285](https://gitlab.com/hestia-earth/hestia-glossary/issues/1285)
* **system:** add activity coefficient for `silvopastureSystem` to system-liveAnimal lookup ([907b768](https://gitlab.com/hestia-earth/hestia-glossary/commit/907b768fd46fd47836eedad08239eedc92e49f34))
* **waterRegime:** add lookup `IPCC_2019_N2O_rice` ([23b1aa2](https://gitlab.com/hestia-earth/hestia-glossary/commit/23b1aa2558ce32b3047d985e3a4dd9750da0c79e))


### Bug Fixes

* **animalManagement:** `mjKgEvMilkIpcc2019`  lookup value for `Milk, sheep, raw (FPCM)` ([9129490](https://gitlab.com/hestia-earth/hestia-glossary/commit/912949046f5a578184c35802ebe64a8a493dde7f)), closes [#1310](https://gitlab.com/hestia-earth/hestia-glossary/issues/1310)
* **animalProduct:** rename `mjKgEVwoolNetEnergyWoolIpcc019` lookup `mjKgEvWoolNetEnergyWoolIpcc2019` ([52f35cd](https://gitlab.com/hestia-earth/hestia-glossary/commit/52f35cd94a84a2172b7e030b654ed4d9599cf4b5)), closes [#1304](https://gitlab.com/hestia-earth/hestia-glossary/issues/1304)
* **forage:** delete `pH` from property-default lookup file ([a59e4a6](https://gitlab.com/hestia-earth/hestia-glossary/commit/a59e4a632e3f4a0b12ce02c2b7c77c9ebfea678d)), closes [#1313](https://gitlab.com/hestia-earth/hestia-glossary/issues/1313)
* **fuel:** change the higher heating value of `refineryGas` ([65e50d5](https://gitlab.com/hestia-earth/hestia-glossary/commit/65e50d55651f6220183d7fcf20f2c4c430032d62))
* **liveAnimal:** fix wrong `digestibility` lookup values ([abaa906](https://gitlab.com/hestia-earth/hestia-glossary/commit/abaa9068626e845d8893fc1e8df705f52f0018b6))
* **liveAnimal:** fix wrong `percentageYmMethaneConversionFactorEntericFermentationIPCC2019` values ([c0c5221](https://gitlab.com/hestia-earth/hestia-glossary/commit/c0c52217c2ff820c1e8829129d1e18993c606ab6)), closes [#1306](https://gitlab.com/hestia-earth/hestia-glossary/issues/1306)
* **liveAnimal:** rename `percentageYmMethaneConversionFactorEntericFermentationIPCC2019` lookup ([702e3f4](https://gitlab.com/hestia-earth/hestia-glossary/commit/702e3f4c7119c4124700c36315b245e4a621ead4)), closes [#1306](https://gitlab.com/hestia-earth/hestia-glossary/issues/1306)
* **liveAnimal:** review lookups needed for `pastureGrass` model ([a1a4f2b](https://gitlab.com/hestia-earth/hestia-glossary/commit/a1a4f2bc4a12edc07e90a13f20634cf8ac599ab0))
* **pesticideAI:** typo in term name of Boron trifluoride ([6d16d08](https://gitlab.com/hestia-earth/hestia-glossary/commit/6d16d084dc1a9f30b5c98cfbba719f878732578f)), closes [#1282](https://gitlab.com/hestia-earth/hestia-glossary/issues/1282)

## [0.43.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.42.0...v0.43.0) (2024-02-20)


### ⚠ BREAKING CHANGES

* **wasteManagement:** Rename `liquidSlurryCoverWasteManagement` `liquidSlurryWithCoverWasteManagement`
* **excretaManagement:** Rename `liquidSlurryCover` `liquidSlurryWithCover`

### Features

* **cropResidueManagement:** restrict `Pasture renewed` to `siteType` `permanent pasture` ([fd1d0b4](https://gitlab.com/hestia-earth/hestia-glossary/commit/fd1d0b4618a00460c8c81a13f1e4dadb705a47ae))
* **feedFoodAdditive:** add default energy content to `Minerals, unspecified` ([383abef](https://gitlab.com/hestia-earth/hestia-glossary/commit/383abef59d51a6057a18c01723b3e854b620ce17))
* **inorganicFertiliser:** add `potassiumContentAsK2O` value to `Monopotassium Phosphate (kg P2O5)` ([17128e0](https://gitlab.com/hestia-earth/hestia-glossary/commit/17128e0de5765638f3a903250d1a0fad0c1da946))
* **processedFood:** add `pisco` ([5e481ae](https://gitlab.com/hestia-earth/hestia-glossary/commit/5e481ae17cdf76365166bb312e3237f09aa96bb6))
* **processedFood:** add `topDressingProteinFeedUnspecified` ([8cbfacf](https://gitlab.com/hestia-earth/hestia-glossary/commit/8cbfacf5d52aec544fe7a90e7955ecbe8ec26614)), closes [#1238](https://gitlab.com/hestia-earth/hestia-glossary/issues/1238)
* **processingAid:** add `lacticAcidProcessingAid` ([3050c29](https://gitlab.com/hestia-earth/hestia-glossary/commit/3050c29bdb0cfb3b622e1e1788d14842d23da1e5)), closes [#1052](https://gitlab.com/hestia-earth/hestia-glossary/issues/1052)
* **region:** add regional data for `averageColdCarcassWeight` lookup ([015c213](https://gitlab.com/hestia-earth/hestia-glossary/commit/015c213c049178ab4f0ecf309d937ca12a88ad3b))
* **system:** add `farrowingCrateSystem` ([f252d52](https://gitlab.com/hestia-earth/hestia-glossary/commit/f252d529f687a74f73f7c8bbc20e5633d9ea1a43))
* **system:** add `outdoorAnimalSystem` ([749c49e](https://gitlab.com/hestia-earth/hestia-glossary/commit/749c49e5072137a05ab318235e4266285c847a63))


### Bug Fixes

* **characterisedIndicator:** error in model mapping lookup for `chaudharyBrooks2018` ([458f278](https://gitlab.com/hestia-earth/hestia-glossary/commit/458f278f8f26119e36a563e3b8fc9b1b58ad7615))
* **crop:** errors in combustion factors for five terms ([6a480db](https://gitlab.com/hestia-earth/hestia-glossary/commit/6a480db71eb1cf1d625413fc993e037a6ff53ca8))
* **excretaManagement:** rename `liquidSlurryCover` `liquidSlurryWithCover` ([039e130](https://gitlab.com/hestia-earth/hestia-glossary/commit/039e1307e25ec0adcb949c75c34c62b81af39c14))
* **forage:** remove duplicated properties for `genericGrassFreshForage` ([e7753f6](https://gitlab.com/hestia-earth/hestia-glossary/commit/e7753f669477c9cf8ee0d2b1f96c63a7a749b1a6))
* **wasteManagement:** rename `liquidSlurryCoverWasteManagement` ([96f72aa](https://gitlab.com/hestia-earth/hestia-glossary/commit/96f72aa51eaa08d7bbe865d176a9dedd63c5e35e))

## [0.42.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.41.0...v0.42.0) (2024-02-06)


### ⚠ BREAKING CHANGES

* **property:** change `Weight per item` unit from `g` to `kg`
* **landUseManagement:** Moves `Number of tillages` and `Tillage depth` from
`tillage` to `landUseManagement`

### Features

* **crop:** add `Celery, leaf`, `Celery, seed`, and `Celery, stalk` ([122592e](https://gitlab.com/hestia-earth/hestia-glossary/commit/122592e55412d0aab20c549049dcc195a23b9470))
* **crop:** add `Rock samphire, shoot` and `Marsh samphire, shoot` ([27ae13d](https://gitlab.com/hestia-earth/hestia-glossary/commit/27ae13df2d052a9b407900e8894c4b50b956f8c2)), closes [#1288](https://gitlab.com/hestia-earth/hestia-glossary/issues/1288)
* **feedFoodAdditive:** add `Rennet` ([e1a861c](https://gitlab.com/hestia-earth/hestia-glossary/commit/e1a861c83d08efb1a15f800caa5ef1b539dac4e8)), closes [#1052](https://gitlab.com/hestia-earth/hestia-glossary/issues/1052)
* **forage:** add `averagePropertiesTermIds` lookup ([edef4ff](https://gitlab.com/hestia-earth/hestia-glossary/commit/edef4ff966b116a4f03c1fb7c55e1185dd975750)), closes [#1293](https://gitlab.com/hestia-earth/hestia-glossary/issues/1293)
* **forage:** average terms in property lookup ([db06a6f](https://gitlab.com/hestia-earth/hestia-glossary/commit/db06a6f5100d16e88f195b3962f1dd81d0285409))
* **landCover:** add `Rock samphire plant` and `Marsh samphire plant` ([a8e8119](https://gitlab.com/hestia-earth/hestia-glossary/commit/a8e8119a8aa586c27fce907e0cc7190098395d24)), closes [#1288](https://gitlab.com/hestia-earth/hestia-glossary/issues/1288)
* **landUseManagement:** move terms from `tillage` ([4dd7f49](https://gitlab.com/hestia-earth/hestia-glossary/commit/4dd7f490f31f964e624180bf2c4845b6e90137ef))
* **region:** create `region-animalProduct-animalProductGroupingFAO-weightPerItem-lookup` ([40beacc](https://gitlab.com/hestia-earth/hestia-glossary/commit/40beacc53647da0b43c535ecadcc0c0d7fbe10b3)), closes [#1275](https://gitlab.com/hestia-earth/hestia-glossary/issues/1275)
* **region:** update all FAOSTAT lookups ([2eb0fcc](https://gitlab.com/hestia-earth/hestia-glossary/commit/2eb0fcc80b7bd40275803281d079d20a305826d0))


### Bug Fixes

* **excretaManagement:** fix wrong values in CH4 conv factor ([04dd363](https://gitlab.com/hestia-earth/hestia-glossary/commit/04dd3636890ba10cd07b7690eb8631aa7a159ed6))
* **property:** change `Weight per item` unit from `g` to `kg` ([29256fb](https://gitlab.com/hestia-earth/hestia-glossary/commit/29256fb07c2418d96693a19d59d00fe0e9716b85))
* **region:** fix incorrect `head` lookup values from FAO ([112a37b](https://gitlab.com/hestia-earth/hestia-glossary/commit/112a37b2ef9531ebd5c80f8b91d2a4664e1b3165))
* **region:** fix units for `averageColdCarcassWeight` lookup ([065a313](https://gitlab.com/hestia-earth/hestia-glossary/commit/065a313a43dfb8b2fae16840c150ff9b0f099006))

## [0.41.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.40.0...v0.41.0) (2024-01-23)


### ⚠ BREAKING CHANGES

* **landCover:** Moves `Short bare fallow` from `landUseManagement`
to `landCover`
* **aquacultureManagement:** Renames `Stocking density (m3)` to
`Stocking density, aquaculture (m3)` and renames
`Stocking density, final (m3)` to
`Stocking density, aquaculture, final (m3)`
* **liveAquaticSpecies:** rename `Fish fingerlings` `Fish fingerling (number)`

### Features

* **animalBreed:** add `Sarda` ([18bc054](https://gitlab.com/hestia-earth/hestia-glossary/commit/18bc05488316c7b41e82bad8768551ca7281ae46)), closes [#1263](https://gitlab.com/hestia-earth/hestia-glossary/issues/1263)
* **animalManagement:** add lookup `productTermTypesAllowed` ([1d7d040](https://gitlab.com/hestia-earth/hestia-glossary/commit/1d7d0409a488693dd357770bd8ef5f3b7d191aa3)), closes [#1111](https://gitlab.com/hestia-earth/hestia-glossary/issues/1111)
* **aquacultureManagement:** add `Stocking density (m2)` and `Stocking density (m2), final` ([49777bc](https://gitlab.com/hestia-earth/hestia-glossary/commit/49777bc164f6eab6a7992f32cfc18b5fed2faaba)), closes [#1208](https://gitlab.com/hestia-earth/hestia-glossary/issues/1208)
* **aquacultureManagement:** specify `Stocking density...` terms are for aquaculture ([ec21ede](https://gitlab.com/hestia-earth/hestia-glossary/commit/ec21edef3cf9f006aa216f2d58de1bf0e4eb80b6))
* **crop:** add `Carob, pulp` ([2e17f1b](https://gitlab.com/hestia-earth/hestia-glossary/commit/2e17f1bee130795f636d86f1c6f4da2fc879c1f0)), closes [#1242](https://gitlab.com/hestia-earth/hestia-glossary/issues/1242)
* **crop:** add missing AGROVOC links ([180e96d](https://gitlab.com/hestia-earth/hestia-glossary/commit/180e96d2e9ad2a4ea14f474f062bd4429fcc9a42)), closes [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72)
* **feedFoodAdditive:** add `Acidity regulator, unspecified` and `Binding agent, unspecified` ([fb196fb](https://gitlab.com/hestia-earth/hestia-glossary/commit/fb196fbe90d84a136fcd4ac78ff0923f01f1a353)), closes [#1271](https://gitlab.com/hestia-earth/hestia-glossary/issues/1271)
* **forage:** add `landCoverId` to "grass-clover" and "grass-alfalfa" terms ([de65676](https://gitlab.com/hestia-earth/hestia-glossary/commit/de656766a29d982275d1b798308ae4774a195230)), closes [#1273](https://gitlab.com/hestia-earth/hestia-glossary/issues/1273)
* **fuel:** add `Wood chip (fuel)` ([1f69c1c](https://gitlab.com/hestia-earth/hestia-glossary/commit/1f69c1c9ddf528e0888541adb99c99c684a114b9)), closes [#1098](https://gitlab.com/hestia-earth/hestia-glossary/issues/1098)
* **inorganicFertiliser:** add `calciumOxideContent` lookup to `Triple Superphosphate (kg P2O5)` ([28733fc](https://gitlab.com/hestia-earth/hestia-glossary/commit/28733fca573006eeefe5d80e5abf653f236969b7)), closes [#1108](https://gitlab.com/hestia-earth/hestia-glossary/issues/1108)
* **landCover:** add `Grass-clover sward` and `Grass-alfalfa sward` ([f6f65ad](https://gitlab.com/hestia-earth/hestia-glossary/commit/f6f65ad020daeef74ae6e55174d407483dc0f94e)), closes [#1273](https://gitlab.com/hestia-earth/hestia-glossary/issues/1273)
* **landCover:** add `Short fallow`, `Long fallow` and `Long bare fallow` ([0ffa7bd](https://gitlab.com/hestia-earth/hestia-glossary/commit/0ffa7bd78c3a9de9fd3fb4365f1d2fb330b847b7))
* **landCover:** move `Short bare fallow` from `landUseManagement` ([3545ef1](https://gitlab.com/hestia-earth/hestia-glossary/commit/3545ef1e8f6ab429d58f6afb94148efd3dfb6b5f))
* **liveAnimal:** add "excretaMixtures" terms to "allowedExcretaTermIds" lookups ([ab0c29d](https://gitlab.com/hestia-earth/hestia-glossary/commit/ab0c29dbea0f460e2063ccbe26fa0987f09f5bfb)), closes [#1155](https://gitlab.com/hestia-earth/hestia-glossary/issues/1155)
* **liveAnimal:** add `isGrazingAnimal` lookup ([a6b5d64](https://gitlab.com/hestia-earth/hestia-glossary/commit/a6b5d646e71376cbab3d3e07b20e382ee7166989)), closes [#1272](https://gitlab.com/hestia-earth/hestia-glossary/issues/1272)
* **liveAnimal:** add generic "female" and "male" terms for calves ([d60b7a0](https://gitlab.com/hestia-earth/hestia-glossary/commit/d60b7a09c29589292a4fbae18b91fc5b336c5a65)), closes [#1262](https://gitlab.com/hestia-earth/hestia-glossary/issues/1262)
* **liveAquaticSpecies:** add `Fish fingerling (kg mass)` ([77e8a24](https://gitlab.com/hestia-earth/hestia-glossary/commit/77e8a24b0f059956664555a11e3a361cc17ec4e3)), closes [#1209](https://gitlab.com/hestia-earth/hestia-glossary/issues/1209)
* **liveAquaticSpecies:** add `Fish larva (number)` and `Fish larva (kg mass)` ([ab3456d](https://gitlab.com/hestia-earth/hestia-glossary/commit/ab3456d030f24056a57c76279ac6f8de056a6f5b)), closes [#1207](https://gitlab.com/hestia-earth/hestia-glossary/issues/1207)
* **liveAquaticSpecies:** add `isGrazingAnimal` lookup ([23f2f8c](https://gitlab.com/hestia-earth/hestia-glossary/commit/23f2f8c2721b46731984d48336d272ea897d5baf)), closes [#1272](https://gitlab.com/hestia-earth/hestia-glossary/issues/1272)
* **liveAquaticSpecies:** rename `Fish fingerlings` `Fish fingerling (number)` ([6c88631](https://gitlab.com/hestia-earth/hestia-glossary/commit/6c88631911f3ed55ac134702ae0e727fd6d4140c)), closes [#1209](https://gitlab.com/hestia-earth/hestia-glossary/issues/1209)
* **liveAquaticSpecies:** rename `Fish fry` `Fish fry (number)` and add `Fish fry (kg mass)` ([7a48f51](https://gitlab.com/hestia-earth/hestia-glossary/commit/7a48f51fd20b39733b94c2b696c3ff69751a78a5)), closes [#1209](https://gitlab.com/hestia-earth/hestia-glossary/issues/1209)
* **material:** add `siteTypesAllowed` lookup ([bbddf63](https://gitlab.com/hestia-earth/hestia-glossary/commit/bbddf63e163b0ea99d4074b8d55cd2be0b516de3)), closes [#1264](https://gitlab.com/hestia-earth/hestia-glossary/issues/1264)
* **model:** add `CNCPS model` ([4d35e76](https://gitlab.com/hestia-earth/hestia-glossary/commit/4d35e7649ec992fb24d26b01a9ecd69feeb2f236)), closes [#1270](https://gitlab.com/hestia-earth/hestia-glossary/issues/1270)
* **model:** add `productTermIdsAllowed` lookup ([1ed4341](https://gitlab.com/hestia-earth/hestia-glossary/commit/1ed4341cf06f8187b26fb7e68e56b1d8bffea9f4)), closes [#1179](https://gitlab.com/hestia-earth/hestia-glossary/issues/1179)
* **operation:** add `Communal labour` ([ec5cad5](https://gitlab.com/hestia-earth/hestia-glossary/commit/ec5cad539d3cef21a7642d626211c856ba3a20a7))
* **operation:** add `Cutting trees, with chain saw` ([1959975](https://gitlab.com/hestia-earth/hestia-glossary/commit/195997539e4604e0ea51ee5ddca79eb963318308)), closes [#1267](https://gitlab.com/hestia-earth/hestia-glossary/issues/1267)
* **otherOrganicChemical:** add `Methanol (other organic chemical)` ([b861539](https://gitlab.com/hestia-earth/hestia-glossary/commit/b861539efe3c9eb8eec606ab74217310f9af4c6b))
* **otherOrganicChemical:** add `Sealant unspecified (AI)` ([bb4513a](https://gitlab.com/hestia-earth/hestia-glossary/commit/bb4513a8ed28c0d940ce1c67ffa94031ae97a9da))
* **otherOrganicChemical:** add `Tetradecyltriethylammonium bromide` ([673d455](https://gitlab.com/hestia-earth/hestia-glossary/commit/673d45565c5156b7cc72edf1cc5a1ceceb464a88)), closes [#953](https://gitlab.com/hestia-earth/hestia-glossary/issues/953)
* **pesticideAI:** add `Boron trifluoride` ([fdfbd30](https://gitlab.com/hestia-earth/hestia-glossary/commit/fdfbd30e61893e13ef27d420750285e1f05b1ff4)), closes [#1268](https://gitlab.com/hestia-earth/hestia-glossary/issues/1268)
* **pesticideAI:** add `Lenacil` ([225ef0b](https://gitlab.com/hestia-earth/hestia-glossary/commit/225ef0b1d12d6f86ec3f47b17ea4c0dbf8b6edb8))
* **processedFood:** add `Whey` ([d8c2707](https://gitlab.com/hestia-earth/hestia-glossary/commit/d8c270723afeca17f3b3a35e02cffef09dc1c660))
* **property:** add `Zinc oxide content` ([e3e6ba4](https://gitlab.com/hestia-earth/hestia-glossary/commit/e3e6ba44d55361f780d000d4f660c60d5505d803)), closes [#1259](https://gitlab.com/hestia-earth/hestia-glossary/issues/1259)


### Bug Fixes

* **emission:** error in default system boundary lookup ([3f8b36d](https://gitlab.com/hestia-earth/hestia-glossary/commit/3f8b36ddb06a3d0a1ab58d04f4935e8407f76bc1))
* **emission:** set `productTermIdsAllowed` for `CH4, to air, waste treatment` ([c2a4f2d](https://gitlab.com/hestia-earth/hestia-glossary/commit/c2a4f2db620c8ef2afe4042ccee220a32446f722))

## [0.40.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.39.0...v0.40.0) (2024-01-09)


### ⚠ BREAKING CHANGES

* **property:** Rename `dataBasisFeedipedia` lookup `feedipediaConversionTermId`.
* **pesticideAI:** Move `CAS-54774-46-8`, `CAS-54774-47-9`,
`CAS-54774-45-7`, and `CAS-70901-12-1` from `otherOrganicChemical` to
`pesticideAI` as these terms are known pesticides.
* **operation:** Rename `Maintaining ditches` `Maintaining ditches, machine unspecified`.
* **veterinaryDrug:** Rename `veterinaryDrugs` glossary `veterinaryDrug`.
* **seed:** Rename `other` glossary `seed`.
* **crop:** Rename `Safflower, seed` `Safflower, seed (whole)`.
* **feedFoodAdditive:** Rename `Methanol` `Methanol (feed food additive)`.
* **feedFoodAdditive:** Rename `Propylene glycol` `Propylene glycol (feed food additive)`.
* **feedFoodAdditive:** Rename `CAS-64-19-7` `aceticAcid`.

term id

### Features

* add `Propylene glycol (pesticide AI)` to `pesticideAI` ([4896a3e](https://gitlab.com/hestia-earth/hestia-glossary/commit/4896a3e68ae3e23dfafd5ae4471f505d1f85d810))
* **animalBreed:** add `Biellese` and `Sambucana` breeds ([8aecb34](https://gitlab.com/hestia-earth/hestia-glossary/commit/8aecb341e48b5eef4f602d134994dd4221b30481)), closes [#1240](https://gitlab.com/hestia-earth/hestia-glossary/issues/1240)
* **animalBreed:** add multiple sheep breeds ([9d8d082](https://gitlab.com/hestia-earth/hestia-glossary/commit/9d8d0820e3e8c0b7352ac63d14f0b47d8225f423)), closes [#1236](https://gitlab.com/hestia-earth/hestia-glossary/issues/1236)
* **animalProduct:** add `Animal bone` ([2043340](https://gitlab.com/hestia-earth/hestia-glossary/commit/2043340f7e7efc1ddf6f5f3cf3c1012965a44187)), closes [#1222](https://gitlab.com/hestia-earth/hestia-glossary/issues/1222)
* **biologicalControlAgent:** add multiple terms ([0dc2720](https://gitlab.com/hestia-earth/hestia-glossary/commit/0dc2720665386ea8f05f4f6c41ac46fa23713044)), closes [#710](https://gitlab.com/hestia-earth/hestia-glossary/issues/710)
* **biologicalControlAgent:** add multiple terms ([1a8842c](https://gitlab.com/hestia-earth/hestia-glossary/commit/1a8842c8f57d34ef2b5c44d94d98bd9e7474f67a))
* **crop:** add "Safflower" terms ([9f36926](https://gitlab.com/hestia-earth/hestia-glossary/commit/9f369261cc2baf7b0dedf5bec84378b8ba911d5c)), closes [#1237](https://gitlab.com/hestia-earth/hestia-glossary/issues/1237)
* **crop:** add `Coconut, shell` ([aef86a9](https://gitlab.com/hestia-earth/hestia-glossary/commit/aef86a9046e4f12253f44d363048ece15e94631c)), closes [#1253](https://gitlab.com/hestia-earth/hestia-glossary/issues/1253)
* **crop:** update property lookup ([47a2aa0](https://gitlab.com/hestia-earth/hestia-glossary/commit/47a2aa094560d52d3aa3b2cee1607d864db48bb8))
* **feedFoodAdditive:** rename `CAS-64-19-7` `aceticAcid` ([8cc0102](https://gitlab.com/hestia-earth/hestia-glossary/commit/8cc0102ba3c140389c221c3a28095f958d589acd))
* **fertiliserBrandName:** add three terms ([449ef74](https://gitlab.com/hestia-earth/hestia-glossary/commit/449ef743d0ae601fb4ee16f9145bb863189523dc))
* **forage:** add `Ervil, fresh forage`, `Ervil, hay`, and `Ervil, seed` ([e48d16f](https://gitlab.com/hestia-earth/hestia-glossary/commit/e48d16f66db527dceebeebda0d290b05f1e0d318)), closes [#1215](https://gitlab.com/hestia-earth/hestia-glossary/issues/1215)
* **forage:** update property lookup ([3646f4b](https://gitlab.com/hestia-earth/hestia-glossary/commit/3646f4bd873cc9273f9047a579cb9cd9ad794424))
* **landCover:** add `Ervil plant` ([a86f005](https://gitlab.com/hestia-earth/hestia-glossary/commit/a86f0054a534cf5c7399685dc1221c7e86b9e26a)), closes [#1215](https://gitlab.com/hestia-earth/hestia-glossary/issues/1215)
* **machinery:** add terms for CHPs ([7fb44e4](https://gitlab.com/hestia-earth/hestia-glossary/commit/7fb44e481da6593e9d1a8b0923624f0b95bb76e4)), closes [#1257](https://gitlab.com/hestia-earth/hestia-glossary/issues/1257)
* **operation:** add `Drying, with rotary dryer` ([541ac5e](https://gitlab.com/hestia-earth/hestia-glossary/commit/541ac5e3d597e0bf80dc3e2ad09d5380b3de3f0b)), closes [#1229](https://gitlab.com/hestia-earth/hestia-glossary/issues/1229)
* **operation:** add `Esterification` ([553b3b6](https://gitlab.com/hestia-earth/hestia-glossary/commit/553b3b6089a6efd68ce33620e06e5345257b082e)), closes [#1254](https://gitlab.com/hestia-earth/hestia-glossary/issues/1254)
* **operation:** add `Grating, machine unspecified` ([a0645cf](https://gitlab.com/hestia-earth/hestia-glossary/commit/a0645cfe9d8b0bc1f9ed201115cec93b80812df7)), closes [#1250](https://gitlab.com/hestia-earth/hestia-glossary/issues/1250)
* **operation:** add `Joint fungicide and fertiliser application, with sprayer` ([a374c55](https://gitlab.com/hestia-earth/hestia-glossary/commit/a374c5541f2bc21e7f8dbb1a86fe6e6563b237b3)), closes [#1241](https://gitlab.com/hestia-earth/hestia-glossary/issues/1241)
* **operation:** add `Joint pesticide and fertiliser application, with sprayer` ([c0a28b4](https://gitlab.com/hestia-earth/hestia-glossary/commit/c0a28b47dd8ddc67efad5f5ea26feaebe4866c3c)), closes [#1246](https://gitlab.com/hestia-earth/hestia-glossary/issues/1246)
* **operation:** add `Maintaining ditches, with flail mower` and rename `Maintaining ditches` ([4a12097](https://gitlab.com/hestia-earth/hestia-glossary/commit/4a12097f296be015714cd8c31d19c27483a59d46)), closes [#1241](https://gitlab.com/hestia-earth/hestia-glossary/issues/1241)
* **operation:** add terms for collecting eggs ([678956e](https://gitlab.com/hestia-earth/hestia-glossary/commit/678956edb8e01a4d5e830ea18b3ca0b463514812)), closes [#1247](https://gitlab.com/hestia-earth/hestia-glossary/issues/1247)
* **pesticideAI:** add `Bioallethrin` ([266f7f3](https://gitlab.com/hestia-earth/hestia-glossary/commit/266f7f3cdae59ac24d577ad089310adc50b6f509)), closes [#710](https://gitlab.com/hestia-earth/hestia-glossary/issues/710)
* **pesticideAI:** add `Chlorpheniramine` ([5597c53](https://gitlab.com/hestia-earth/hestia-glossary/commit/5597c53473ea86581ab975887af9d1713a7f8e25)), closes [#710](https://gitlab.com/hestia-earth/hestia-glossary/issues/710)
* **pesticideAI:** add `Phosphine` ([d475351](https://gitlab.com/hestia-earth/hestia-glossary/commit/d475351c382d5dfb06ce7dd0567c243a2ce97fde)), closes [#710](https://gitlab.com/hestia-earth/hestia-glossary/issues/710)
* **pesticideAI:** add missing pesticides identified by FEX ([45a7da9](https://gitlab.com/hestia-earth/hestia-glossary/commit/45a7da902499daa30aa300758d9bd692d4809870)), closes [#710](https://gitlab.com/hestia-earth/hestia-glossary/issues/710)
* **pesticideAI:** add multiple terms ([090e7d0](https://gitlab.com/hestia-earth/hestia-glossary/commit/090e7d0c6ea35fc240aaa52265b61c9b1b4faad1)), closes [#710](https://gitlab.com/hestia-earth/hestia-glossary/issues/710)
* **pesticideAI:** add multiple terms ([1e9e33b](https://gitlab.com/hestia-earth/hestia-glossary/commit/1e9e33b0e05ee6c4063dcf175949af7698f31106))
* **pesticideAI:** add synonym to `CAS-71526-07-3` ([4e89ac5](https://gitlab.com/hestia-earth/hestia-glossary/commit/4e89ac59223ef1a3b4f03e655d38b1fdf2f44a24))
* **pesticideBrandName:** add Kenyan maize pesticide brands ([5116572](https://gitlab.com/hestia-earth/hestia-glossary/commit/511657258781497a77ad4e84af3b987924412d78)), closes [#1233](https://gitlab.com/hestia-earth/hestia-glossary/issues/1233)
* **pesticideBrandName:** add lookup `pesticideType` to specify the type (e.g., "rodenticide") ([148c9df](https://gitlab.com/hestia-earth/hestia-glossary/commit/148c9df1073f9a826e2cc41fdf998c550e316f68))
* **pesticideBrandName:** add multiple terms ([b4d9e04](https://gitlab.com/hestia-earth/hestia-glossary/commit/b4d9e04bc133403365563c944b8675952e48763f)), closes [#1245](https://gitlab.com/hestia-earth/hestia-glossary/issues/1245)
* **pesticideBrandName:** add terms ([d89a4a2](https://gitlab.com/hestia-earth/hestia-glossary/commit/d89a4a2d42792e4f48c092fbda97a7fb9dc10379))
* **processedFood:** add `Nata de coco` ([943b4f6](https://gitlab.com/hestia-earth/hestia-glossary/commit/943b4f6d47c90e98e15fee65a1a2a1d962956dc1)), closes [#1248](https://gitlab.com/hestia-earth/hestia-glossary/issues/1248)
* **processedFood:** update property lookup ([ae4d6d7](https://gitlab.com/hestia-earth/hestia-glossary/commit/ae4d6d76e5d441a10307058f77cac53f9a24128e))
* **property:** add `feedipediaConversionEnum` lookup ([6afba62](https://gitlab.com/hestia-earth/hestia-glossary/commit/6afba6254d4fa1a89478600be18bf53048965894)), closes [#665](https://gitlab.com/hestia-earth/hestia-glossary/issues/665)
* **property:** add missing `feedipediaName` lookup for multiple terms ([bc379ee](https://gitlab.com/hestia-earth/hestia-glossary/commit/bc379ee16fc5589801a669147578b080896b979a))
* **property:** add terms for ADL and ADC content ([78bc51b](https://gitlab.com/hestia-earth/hestia-glossary/commit/78bc51bb5909b009e46e887f66b5d8ec042b89c4)), closes [#1243](https://gitlab.com/hestia-earth/hestia-glossary/issues/1243)
* **property:** rename `dataBasisFeedipedia` lookup `feedipediaConversionTermId` ([5f35d26](https://gitlab.com/hestia-earth/hestia-glossary/commit/5f35d266a7c08a47784bf2c759d70490b9ab2ae9)), closes [#665](https://gitlab.com/hestia-earth/hestia-glossary/issues/665)
* **seed:** add `Tubers`, `Root cuttings`, and `Suckers` ([7d7623b](https://gitlab.com/hestia-earth/hestia-glossary/commit/7d7623b7e04f35180374b58cd274dce054c924a0)), closes [#1143](https://gitlab.com/hestia-earth/hestia-glossary/issues/1143)
* **seed:** broaden definition of `Stem cuttings` to include grafts ([7c470ba](https://gitlab.com/hestia-earth/hestia-glossary/commit/7c470baf620ace7d103894e42ecc756895f34124)), closes [#1143](https://gitlab.com/hestia-earth/hestia-glossary/issues/1143)


### Bug Fixes

* **characterisedIndicator:** error in mappings ([ab2ecd0](https://gitlab.com/hestia-earth/hestia-glossary/commit/ab2ecd0377251904c472172621514c64b1dd3861)), closes [#284](https://gitlab.com/hestia-earth/hestia-glossary/issues/284)
* **property:** set `valueType` for `Organic (property)` to `number` ([1d36af6](https://gitlab.com/hestia-earth/hestia-glossary/commit/1d36af68e9e8fa8d03ad8b35f26d57bcd774d7e6)), closes [#1251](https://gitlab.com/hestia-earth/hestia-glossary/issues/1251)


* **otherOrganicChemical:** move terms into `pesticideAI` ([5b099c1](https://gitlab.com/hestia-earth/hestia-glossary/commit/5b099c1a127ec2905744f012ffe9f78816a8c33c))
* **seed:** rename `other` glossary to `seed` ([5ada5e0](https://gitlab.com/hestia-earth/hestia-glossary/commit/5ada5e027fd404e882507d89b42ae1ed27328a3b))
* **veterinaryDrug:** rename `veterinaryDrugs` glossary to `veterinaryDrug` ([ef65ce6](https://gitlab.com/hestia-earth/hestia-glossary/commit/ef65ce600bcda7e1d0d922eb4dcfb0725dc019f3))

## [0.39.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.38.0...v0.39.0) (2023-12-12)


### ⚠ BREAKING CHANGES

* **crop:** rename `Coconut, oil` `Coconut, oil (crude)`
* **landUseManagement:** rename `Shade tree species name(s)` `Shade tree species`, change unit from `List[string]` to `string`
* **property:** change `Organic (property)` `unit` from `boolean` to `%`

### Features

* add `valueType` lookup to practices glossaries ([8f70608](https://gitlab.com/hestia-earth/hestia-glossary/commit/8f706089b2961b77e2b21cf5739570a7d6e6fe5e)), closes [#977](https://gitlab.com/hestia-earth/hestia-glossary/issues/977)
* **crop:** add `Coconut, fatty acid distillate` ([b4f224d](https://gitlab.com/hestia-earth/hestia-glossary/commit/b4f224d7a26f748aaea183c9ff90116ce1d45cc5)), closes [#1217](https://gitlab.com/hestia-earth/hestia-glossary/issues/1217)
* **crop:** add `Coconut, leaf` ([6165ce3](https://gitlab.com/hestia-earth/hestia-glossary/commit/6165ce30dc62652c0dfe464ebb41d4102b480de5)), closes [#1217](https://gitlab.com/hestia-earth/hestia-glossary/issues/1217)
* **crop:** add `Coconut, midrib` ([6232c62](https://gitlab.com/hestia-earth/hestia-glossary/commit/6232c624455fbd5d70986ab14df3dd3064fca20d)), closes [#1217](https://gitlab.com/hestia-earth/hestia-glossary/issues/1217)
* **crop:** add `Coconut, oil (refined)` ([e12c2df](https://gitlab.com/hestia-earth/hestia-glossary/commit/e12c2df8683f4c9695bb1fdb8d661d69d6f9aa6b)), closes [#1217](https://gitlab.com/hestia-earth/hestia-glossary/issues/1217)
* **crop:** rename `Coconut, oil` `Coconut, oil (crude)` ([3e6d309](https://gitlab.com/hestia-earth/hestia-glossary/commit/3e6d309c531c95fd18f749fc8516f184d3ad7381)), closes [#1217](https://gitlab.com/hestia-earth/hestia-glossary/issues/1217)
* **fertiliserBrandName:** adds fertilisers for Kenya survey ([c7463c5](https://gitlab.com/hestia-earth/hestia-glossary/commit/c7463c5a11ca27223fe26292e007d2d8415dc4fa))
* **landCover:** add `Natural forest` and `Plantation forest` ([cb47e5e](https://gitlab.com/hestia-earth/hestia-glossary/commit/cb47e5e79cbdc5fda5bea6a38f622d6b5317fafc)), closes [#1218](https://gitlab.com/hestia-earth/hestia-glossary/issues/1218)
* **landUseManagement:** rename `Shade tree species name(s)` `Shade tree species` ([51532ac](https://gitlab.com/hestia-earth/hestia-glossary/commit/51532ac2860587372c5753f64911823d4947e0ba))
* **operation:** add `Shearing, by hand` and `Shearing, with machine shears` ([1940868](https://gitlab.com/hestia-earth/hestia-glossary/commit/19408688ba3b2beb7ba14ad92c1b8a2ddd8f83fb)), closes [#1230](https://gitlab.com/hestia-earth/hestia-glossary/issues/1230)
* **organicFertiliser:** add `Composted manure (kg mass)` and `Composted manure (kg N)` ([178c004](https://gitlab.com/hestia-earth/hestia-glossary/commit/178c004c57341c760b812cd9bc136106dd31452b)), closes [#311](https://gitlab.com/hestia-earth/hestia-glossary/issues/311)
* **other:** add `Corms` and `Bulbs` ([e82c98e](https://gitlab.com/hestia-earth/hestia-glossary/commit/e82c98e7fee0098d45d4a33dd6f55e9fada154f6))
* **other:** add `Seedlings` ([3a3c5ef](https://gitlab.com/hestia-earth/hestia-glossary/commit/3a3c5ef3358124f8588f2ac15506e6e4d2e73253))
* **otherInorganicChemical:** add `Calcium peroxide` ([33578ce](https://gitlab.com/hestia-earth/hestia-glossary/commit/33578ce759c4d018cdd1890e2f0c280b5ac4c497)), closes [#955](https://gitlab.com/hestia-earth/hestia-glossary/issues/955)
* **pesticideAI:** add `synonyms` to `CAS-8002-65-1` and `CAS-94-75-7` ([9cc9153](https://gitlab.com/hestia-earth/hestia-glossary/commit/9cc91530ae3de5ff4ea55edbfbe854426c6edb9b))
* **pesticideAI:** add `synonyms` to `Organophosphorus compounds, unspecified` ([9359abc](https://gitlab.com/hestia-earth/hestia-glossary/commit/9359abcf9d2b7a4b49188d2b7b46fdb3df7b2747))
* **processedFood:** add `Bread, unspecified` ([aa27f08](https://gitlab.com/hestia-earth/hestia-glossary/commit/aa27f084470a6eba86dcf93173fd4afd392ba239)), closes [#1231](https://gitlab.com/hestia-earth/hestia-glossary/issues/1231)
* **processedFood:** add `Copra expeller pellet` ([448f98a](https://gitlab.com/hestia-earth/hestia-glossary/commit/448f98a3676a3dddc3468db098b8a5f3d530a6a6)), closes [#1217](https://gitlab.com/hestia-earth/hestia-glossary/issues/1217)
* **processedFood:** add auto-generated lookup property ([4d3b988](https://gitlab.com/hestia-earth/hestia-glossary/commit/4d3b9881a7a1ed11b89fa9b5379ebfa0ac869a5e))
* **property:** change `Organic (property)` `unit` from `boolean` to `%` ([4fd7e01](https://gitlab.com/hestia-earth/hestia-glossary/commit/4fd7e018b593b90bae3b9caf60b195afeed35d6d)), closes [#1223](https://gitlab.com/hestia-earth/hestia-glossary/issues/1223)
* **veterinaryDrugs:** add `Propylene glycol (veterinary drug)` ([0d10a96](https://gitlab.com/hestia-earth/hestia-glossary/commit/0d10a9681488b8f984505ed1054cfe5af783379b)), closes [#1220](https://gitlab.com/hestia-earth/hestia-glossary/issues/1220)


### Bug Fixes

* **crop:** wrong `lPCC_2019_Ratio_AGRes_YieldDM` lookup value for `Vetch, seed` ([e11db2d](https://gitlab.com/hestia-earth/hestia-glossary/commit/e11db2d7835ea575f4b3bbcca0a601c19b8ee59f))
* **organicFertiliser:** fix default values of carbon & lignin content for kg N terms ([92fc0d4](https://gitlab.com/hestia-earth/hestia-glossary/commit/92fc0d46e6eebff818b2a71f1aae261bcde9d919))

## [0.38.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.37.0...v0.38.0) (2023-12-06)


### ⚠ BREAKING CHANGES

* **otherOrganicChemical:** move `Trichloromonofluoromethane` from `pesticideAI` to `otherOrganicChemical`
* **otherOrganicChemical:** move `Dichlorodifluoromethane` from `pesticideAI` to `otherOrganicChemical`
* **otherOrganicChemical:** move `Chlorodifluoromethane` from `pesticideAI` to `otherOrganicChemical`
* **pesticideAI:** move `Bactericide unspecified (AI)` from `pesticideAI` to `otherOrganicChemical`
* **pesticideAI:** move `Antiseptic unspecified (AI)` from `pesticideAI` to `otherOrganicChemical`
* **pesticideAI:** move `Disinfectant unspecified (AI)` from `pesticideAI` to `otherOrganicChemical`
* **other:** move `Liquid oxygen` from `other` to `otherInorganicChemical`
* **other:** move `Detergent unspecified (AI)` from` other` to `otherOrganicChemical`
* **other:** move `Versatic acid derivatives` from `other` to `otherOrganicChemical`
* **other:** rename `Surfactant` `Surfactant unspecified (AI)` and move it from `other` to `otherOrganicChemical`
* **forage:** rename `Echinochloa hapoclada, fresh forage` `Echinochloa haploclada, fresh forage`
* **landCover:** rename `Generic tree` `Generic tree crop`
* **landCover:** rename `Sweet sorgum plant` `Sweet sorghum plant`
* **forage:** rename `Indian coral tree, fresh forage` `Indian coral, fresh forage`
* **crop:** rename `Tallow tree, seed` `Tallow, seed`
* **crop:** rename `Hemp, fiber` `Hemp, fibre`
* **landUseManagement:** move `Native pasture` from `landUseManagement` to `landCover`
* **landUseManagement:** move `Improved pasture` from `landUseManagement` to `landCover`
* **landUseManagement:** move `Nominally managed pasture` from `landUseManagement` to `landCover`
* **landUseManagement:** move `High intensity grazing pasture` from `landUseManagement` to `landCover`
* **landUseManagement:** move `Severely degraded pasture` from `landUseManagement` to `landCover`
* **landCover:** change `units` from `ha` to `% area` in `crop`
* **landCover:** change `units` from `ha` to `% area` in `forage`
* **landCover:** move terms with `units` `% area` from `crop` to `landCover`
* **landCover:** move terms with `units` `% area` from `forage` to `landCover`
* Moves terms from `pesticideAI` which were not identified
as pesticides to `otherOrganicChemical` and `otherInorganicChemical`

### Features

* **animalBreed:** add `Hy-Line Brown` and `Hy-Line W-36` ([7727c56](https://gitlab.com/hestia-earth/hestia-glossary/commit/7727c560260c5b58b9126fa470a4b79f420365d1)), closes [#1182](https://gitlab.com/hestia-earth/hestia-glossary/issues/1182)
* **animalBreed:** add `Merino` and `Rasa Aragonesa` ([2f4b94a](https://gitlab.com/hestia-earth/hestia-glossary/commit/2f4b94a5017e367dc27a894606c5ee4483b367a3)), closes [#1205](https://gitlab.com/hestia-earth/hestia-glossary/issues/1205)
* **animalManagement:** add `Cubicle use` ([ac0134f](https://gitlab.com/hestia-earth/hestia-glossary/commit/ac0134f22b2ce41d494cb3470cd51c36742c0558)), closes [#362](https://gitlab.com/hestia-earth/hestia-glossary/issues/362)
* **animalManagement:** add `Grooved flooring use` ([7f9845b](https://gitlab.com/hestia-earth/hestia-glossary/commit/7f9845bd8984fa539718bcf735bff46f6976b424)), closes [#362](https://gitlab.com/hestia-earth/hestia-glossary/issues/362)
* **animalManagement:** add `Tight stall use` ([4d80ac4](https://gitlab.com/hestia-earth/hestia-glossary/commit/4d80ac4ae63f37dadd5a349d4c9072713473a7ee)), closes [#362](https://gitlab.com/hestia-earth/hestia-glossary/issues/362)
* **crop:** add `Alfalfa, meal` ([983ee75](https://gitlab.com/hestia-earth/hestia-glossary/commit/983ee75845b4df0233d4efa1dc2512a4343e14e1)), closes [#1200](https://gitlab.com/hestia-earth/hestia-glossary/issues/1200)
* **crop:** add `Barley, middlings` ([f7d91fa](https://gitlab.com/hestia-earth/hestia-glossary/commit/f7d91fa1150cc12587ca64a287ed61b9c950fcdf)), closes [#1203](https://gitlab.com/hestia-earth/hestia-glossary/issues/1203)
* **crop:** add `landCoverId` lookup and rename `Tallow tree, seed` `Tallow, seed` ([6199a4d](https://gitlab.com/hestia-earth/hestia-glossary/commit/6199a4de04b53870e8aac01e37d593d7b45ed9da)), closes [#668](https://gitlab.com/hestia-earth/hestia-glossary/issues/668)
* **crop:** add `Pea, flour` ([5415b2e](https://gitlab.com/hestia-earth/hestia-glossary/commit/5415b2e87eddb9b5a8efeb155bd8ab3b4624b081)), closes [#1183](https://gitlab.com/hestia-earth/hestia-glossary/issues/1183)
* **crop:** add `Rapeseed, pellet` ([8feb7d9](https://gitlab.com/hestia-earth/hestia-glossary/commit/8feb7d95a7f0334031bafbd6d73b9ef27e190993)), closes [#1184](https://gitlab.com/hestia-earth/hestia-glossary/issues/1184)
* **excreta:** add `Excreta, deer (kg mass)`, `Excreta, deer (kg N)`, and `Excreta, deer (kg VS)` ([70fa09c](https://gitlab.com/hestia-earth/hestia-glossary/commit/70fa09c60459c9c29bc6deb3c201e949149e979c)), closes [#1210](https://gitlab.com/hestia-earth/hestia-glossary/issues/1210)
* **feedFoodAdditive:** add `Sepiolite` ([237d849](https://gitlab.com/hestia-earth/hestia-glossary/commit/237d8492a50a28e185f0f34fdc9c8f156c499d5f)), closes [#1214](https://gitlab.com/hestia-earth/hestia-glossary/issues/1214)
* **forage:** add `landCoverId` lookup and rename `Indian coral tree, fresh forage` ([ce0c54b](https://gitlab.com/hestia-earth/hestia-glossary/commit/ce0c54ba0ad5c691724f1f8e78a121eb19f341ef)), closes [#668](https://gitlab.com/hestia-earth/hestia-glossary/issues/668)
* **fuel:** add `Olive, seed (fuel)` ([1fc9aa5](https://gitlab.com/hestia-earth/hestia-glossary/commit/1fc9aa54cffa4ee05f6be9d36920cfbcbe944a28))
* **fuel:** add `Petrol, burnt in 2-stroke engine` and `Petrol, burnt in 4-stroke engine` ([96e7524](https://gitlab.com/hestia-earth/hestia-glossary/commit/96e7524b1505f2a20bb7660f452ae8215f2e22c0)), closes [#1188](https://gitlab.com/hestia-earth/hestia-glossary/issues/1188)
* **landCover:** add terms to represent site types ([2cbda51](https://gitlab.com/hestia-earth/hestia-glossary/commit/2cbda51d56ba8aea20159023888a6469c3a76213)), closes [#668](https://gitlab.com/hestia-earth/hestia-glossary/issues/668)
* **landCover:** create glossary ([609d8bd](https://gitlab.com/hestia-earth/hestia-glossary/commit/609d8bd33905f30abab73741716ea5d0841a8329)), closes [#668](https://gitlab.com/hestia-earth/hestia-glossary/issues/668)
* **landCover:** rename `Generic tree` and `Sweet sorgum plant`, add `Generic forage tree` ([65d7f94](https://gitlab.com/hestia-earth/hestia-glossary/commit/65d7f944f32625640718878f34dfb79d32f9abc4)), closes [#668](https://gitlab.com/hestia-earth/hestia-glossary/issues/668)
* **landUseManagement:** add `Number of insecticide applications` ([c8f58e1](https://gitlab.com/hestia-earth/hestia-glossary/commit/c8f58e11517a655d838c80e8c83cbc6c24aed93a))
* **landUseManagement:** move pasture related terms to `landCover` ([96715c9](https://gitlab.com/hestia-earth/hestia-glossary/commit/96715c9acdc729fd4ebc0e670374c466d2b09568)), closes [#668](https://gitlab.com/hestia-earth/hestia-glossary/issues/668)
* **liveAnimal:** add `Sheep, lamb (female, unweaned)` and `Sheep, lamb (male, unweaned)` ([561fb66](https://gitlab.com/hestia-earth/hestia-glossary/commit/561fb664068f0fb45f8269a94d96bde8b31b56ff)), closes [#1196](https://gitlab.com/hestia-earth/hestia-glossary/issues/1196)
* move terms from `pesticideAI` to new glossaries for org. and inorg. chems ([3a37b61](https://gitlab.com/hestia-earth/hestia-glossary/commit/3a37b610eb41b0802a433b3a9fe668b5f31b030d)), closes [#398](https://gitlab.com/hestia-earth/hestia-glossary/issues/398)
* **operation:** add `Filtering, with drum filter` ([5b2ea33](https://gitlab.com/hestia-earth/hestia-glossary/commit/5b2ea33e7ac4c2312ed32e23fd297853155bdee0)), closes [#1206](https://gitlab.com/hestia-earth/hestia-glossary/issues/1206)
* **operation:** add `Oil extraction, with dripping method` ([563e7bb](https://gitlab.com/hestia-earth/hestia-glossary/commit/563e7bb24e3e3fd2bd10355339c133103efd330f))
* **operation:** add `Transporting animals between sites` ([2c1a67d](https://gitlab.com/hestia-earth/hestia-glossary/commit/2c1a67d51a1490661c48ce05a47a8286f19bca9f)), closes [#1213](https://gitlab.com/hestia-earth/hestia-glossary/issues/1213)
* **operation:** add terms for "Insecticide application" and "Fungicide application" ([f394890](https://gitlab.com/hestia-earth/hestia-glossary/commit/f394890d6bad5bec7839d694734f675565f8a0d5))
* **other:** add `Sexed semen` ([5262048](https://gitlab.com/hestia-earth/hestia-glossary/commit/52620486f690f29f055a29dc8a24820c3ee8bde1)), closes [#1193](https://gitlab.com/hestia-earth/hestia-glossary/issues/1193)
* **otherInorganicChemical:** add `Bleach, unspecified` ([912010e](https://gitlab.com/hestia-earth/hestia-glossary/commit/912010e437fac8eaa36a89571650ac354e54bd16)), closes [#1060](https://gitlab.com/hestia-earth/hestia-glossary/issues/1060)
* **other:** move `Liquid oxygen` to `otherInorganicChemical` glossary ([c31afe0](https://gitlab.com/hestia-earth/hestia-glossary/commit/c31afe0720066015c1f648c790e6f79238862fd0)), closes [#1037](https://gitlab.com/hestia-earth/hestia-glossary/issues/1037) [#1050](https://gitlab.com/hestia-earth/hestia-glossary/issues/1050)
* **other:** move terms to `otherOrganicChemical` glossary ([636e685](https://gitlab.com/hestia-earth/hestia-glossary/commit/636e685f294a97e9cc35ddd01da64760d813a43f)), closes [#1037](https://gitlab.com/hestia-earth/hestia-glossary/issues/1037)
* **otherOrganicChemical:** add terms for detergents with different pH ([4301bed](https://gitlab.com/hestia-earth/hestia-glossary/commit/4301bedbf2cabb2c74eaf5e3318d72f43e6c84c5)), closes [#751](https://gitlab.com/hestia-earth/hestia-glossary/issues/751)
* **otherOrganicChemical:** move refrigerant gases from `pesticideAI` and add more ([21f8f7d](https://gitlab.com/hestia-earth/hestia-glossary/commit/21f8f7dafc297ddcc6e5b3a11fe0b4be112a7007)), closes [#850](https://gitlab.com/hestia-earth/hestia-glossary/issues/850)
* **pesticideAI:** delete terms not classified as pesticides, org. chemical or inorg. chemical ([6092d63](https://gitlab.com/hestia-earth/hestia-glossary/commit/6092d630d452742023610b3996c62f0d5632fd46))
* **pesticideAI:** move multiple terms to `otherOrganicChemical` glossary ([d13b2f8](https://gitlab.com/hestia-earth/hestia-glossary/commit/d13b2f8d358095138ed11cda4e4c59651ce815ac)), closes [#751](https://gitlab.com/hestia-earth/hestia-glossary/issues/751)
* **pesticideBrandName:** add `Ag Surf II (adjuvant)` ([08b43ad](https://gitlab.com/hestia-earth/hestia-glossary/commit/08b43ad44aadccffa7dd3fa354ace8e033d412cc)), closes [#676](https://gitlab.com/hestia-earth/hestia-glossary/issues/676)
* **processedFood:** add `Potato, protein concentrate` and `Potato, protein isolate` ([3231800](https://gitlab.com/hestia-earth/hestia-glossary/commit/323180071fc5217ec3a1b4cca80d77cb978e080e)), closes [#1187](https://gitlab.com/hestia-earth/hestia-glossary/issues/1187)
* **processedFood:** add `Triticale, flour` ([a32018c](https://gitlab.com/hestia-earth/hestia-glossary/commit/a32018cabbbd6c9fb56f981e6c6df946a7d62603)), closes [#1186](https://gitlab.com/hestia-earth/hestia-glossary/issues/1186)
* **processedFood:** add `Truffle sauce` ([8ea4c4b](https://gitlab.com/hestia-earth/hestia-glossary/commit/8ea4c4b0c6dbd10724c8ed3e3b9d55650f80c207))
* **processedFood:** add `Vegetable oil, unspecified` and `Hydrogenated vegetable oil, unspecified` ([9e77cbb](https://gitlab.com/hestia-earth/hestia-glossary/commit/9e77cbb2b1b02b48dc121d08fc25c3a267b41bdc)), closes [#1199](https://gitlab.com/hestia-earth/hestia-glossary/issues/1199) [#1192](https://gitlab.com/hestia-earth/hestia-glossary/issues/1192)
* **property:** add `Gestation period` ([5e02a34](https://gitlab.com/hestia-earth/hestia-glossary/commit/5e02a34dab3a621c6391e405c4296e1e01411975)), closes [#1204](https://gitlab.com/hestia-earth/hestia-glossary/issues/1204)
* **property:** add missing vitamins content ([77aebb4](https://gitlab.com/hestia-earth/hestia-glossary/commit/77aebb4a3bd1dd3433f8402655dbc07c80f61a17)), closes [#1201](https://gitlab.com/hestia-earth/hestia-glossary/issues/1201)
* **property:** allow `Ad libitum feeding` with `feedFoodAdditive` termType ([2528699](https://gitlab.com/hestia-earth/hestia-glossary/commit/2528699d7cab5fabc4317dd4172c53602646ae66))


### Bug Fixes

* **crop:** error in `subClassOf` ([ade3420](https://gitlab.com/hestia-earth/hestia-glossary/commit/ade34207c2b3850b3c61f94648a72e44725ee616)), closes [#1211](https://gitlab.com/hestia-earth/hestia-glossary/issues/1211)
* **crop:** rename `Hemp, fiber` `Hemp, fibre` ([798cb2c](https://gitlab.com/hestia-earth/hestia-glossary/commit/798cb2ce0f2f4f6824cb3d890645e0c87c585462))
* **forage:** delete empty lookup `grazedPastureGrassInputId` ([f66a264](https://gitlab.com/hestia-earth/hestia-glossary/commit/f66a26444453c7d8aa15d2982d2795831a3c4a86)), closes [#1227](https://gitlab.com/hestia-earth/hestia-glossary/issues/1227)
* **forage:** typo in `Echinochloa hapoclada, fresh forage` term name ([d25c36a](https://gitlab.com/hestia-earth/hestia-glossary/commit/d25c36ac473aaa4071cf9082fbbfabcd606ca180))
* **measurement:** errors in `siteTypesAllowed` lookup ([7bc4140](https://gitlab.com/hestia-earth/hestia-glossary/commit/7bc4140212a67a5dec4aeade36a24bb72e227642)), closes [#1216](https://gitlab.com/hestia-earth/hestia-glossary/issues/1216)
* **operation:** errors in `siteTypesAllowed` lookup ([e7ca6ac](https://gitlab.com/hestia-earth/hestia-glossary/commit/e7ca6ac1eaf238c700be748cb91563996f089172)), closes [#1216](https://gitlab.com/hestia-earth/hestia-glossary/issues/1216)
* **otherOrganicChemical:** error in column headers ([3a574f5](https://gitlab.com/hestia-earth/hestia-glossary/commit/3a574f523f51f912d67102e4849e44dc9a2c606e))
* **otherOrganicChemical:** replace `property.term.name` with `term.id` ([aa0e403](https://gitlab.com/hestia-earth/hestia-glossary/commit/aa0e403bc156d0e9271ccb24e29d850cb1dc6b7f))
* **property:** errors in `termTypesAllowed` and `siteTypesAllowed` lookups ([9756ccf](https://gitlab.com/hestia-earth/hestia-glossary/commit/9756ccf21223b062cb85661d3c1cf5e124f60580)), closes [#1216](https://gitlab.com/hestia-earth/hestia-glossary/issues/1216)

## [0.37.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.36.0...v0.37.0) (2023-11-21)


### ⚠ BREAKING CHANGES

* **pesticideAI:** Deletes `Epoxiconazol` in favour of `Epoxiconazole`
which is an equivalent term.

### Features

* **animalManagement:** add `Slatted flooring use` ([ea5f7d1](https://gitlab.com/hestia-earth/hestia-glossary/commit/ea5f7d101f54366d51f2b83a2f08c7d6a4aae045)), closes [#1163](https://gitlab.com/hestia-earth/hestia-glossary/issues/1163)
* **animalProduct:** add `Fat, animal unspecified` ([0585222](https://gitlab.com/hestia-earth/hestia-glossary/commit/0585222c5b604976704452cd2bdc41e61ea5546b)), closes [#1185](https://gitlab.com/hestia-earth/hestia-glossary/issues/1185)
* broaden out antibiotic glossary to cover all veterinary drugs ([daad49b](https://gitlab.com/hestia-earth/hestia-glossary/commit/daad49bcf76f2c4bd11852ad542efae836cdd39f)), closes [#398](https://gitlab.com/hestia-earth/hestia-glossary/issues/398)
* **building:** add `Administrative building` ([d474783](https://gitlab.com/hestia-earth/hestia-glossary/commit/d4747832b41b5795772777902a89353d005f0f8f))
* **machinery:** add `Gravity wagon` ([5b01464](https://gitlab.com/hestia-earth/hestia-glossary/commit/5b01464908be47db887d6841d47e0ce503ae2374))
* **methodMeasurement:** add multiple methods ([10e1b99](https://gitlab.com/hestia-earth/hestia-glossary/commit/10e1b9912c10e21b3331d707ec612707a2180faa)), closes [#1202](https://gitlab.com/hestia-earth/hestia-glossary/issues/1202)
* **pesticideAI:** add `Diamide compounds, unspecified` ([4d9d4cd](https://gitlab.com/hestia-earth/hestia-glossary/commit/4d9d4cddcf1761bd6199b1a82435bb15ecff748a))
* **pesticideBrandName:** add `Camix` ([29d08fa](https://gitlab.com/hestia-earth/hestia-glossary/commit/29d08faf243240ee72c55e2e5e5c0a071af93caa))
* **pesticideBrandName:** add `Harness herbicide` ([b611b1a](https://gitlab.com/hestia-earth/hestia-glossary/commit/b611b1aca2561fad41d511bb8c920e5bb3c385aa))
* **pesticideBrandName:** add `Heat Herbicide`, `Ligate WG`, `Spider`, and `Sumisoya Top` ([9ab6b47](https://gitlab.com/hestia-earth/hestia-glossary/commit/9ab6b478fddbc28d5809a9b7475f537830beffda)), closes [#1198](https://gitlab.com/hestia-earth/hestia-glossary/issues/1198)


### Bug Fixes

* **crop:** fix `Default_ag_dm_crop_residue` and `Default_bg_dm_crop_residue` lookups ([1da82a0](https://gitlab.com/hestia-earth/hestia-glossary/commit/1da82a0d9391567e4ea6ae63838f8126c7377e16)), closes [#1190](https://gitlab.com/hestia-earth/hestia-glossary/issues/1190)
* **model:** update `RiskPoll model` name in characterisedIndicator mapping ([ca9b63d](https://gitlab.com/hestia-earth/hestia-glossary/commit/ca9b63d3215c557b85ed7434da9a70ac3a0d7488))
* **pesticideAI:** delete `Epoxiconazol` ([9212e59](https://gitlab.com/hestia-earth/hestia-glossary/commit/9212e59eeac1c57f2ae346d2c2d557b381c695fa))
* **veterinaryDrugs:** error in lookups ([96a71f3](https://gitlab.com/hestia-earth/hestia-glossary/commit/96a71f300d73ed24069b9dd839a80b40fc35d0c0))

## [0.36.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.35.0...v0.36.0) (2023-11-06)


### ⚠ BREAKING CHANGES

* **other:** move `Instrument air` from other to `processingAid`
* **other:** move `Steam (1 bar pressure)` to `processingAid`
* **measurement:** Renames `Microbial biomass (carbon, per kg soil)` to
`Microbial biomass carbon (per kg soil)`,
`Microbial biomass (nitrogen, per kg soil)` to
`Microbial biomass nitrogen (per kg soil)`,
`Microbial biomass (carbon, per m3 soil)` to
`Microbial biomass carbon (per m3 soil)`,
`Microbial biomass (nitrogen, per m3 soil)` to
`Microbial biomass nitrogen (per m3 soil)`,
`Microbial biomass (carbon, per ha)` to
`Microbial biomass carbon (per ha)`,
`Microbial biomass (nitrogen, per ha)` to
`Microbial biomass nitrogen (per ha)`.

### Features

* **machinery:** add `Electric battery` ([61761d8](https://gitlab.com/hestia-earth/hestia-glossary/commit/61761d8fea8d4016d8626f419834d9f5e3054bfd)), closes [#1173](https://gitlab.com/hestia-earth/hestia-glossary/issues/1173)
* **material:** add terms for "Zinc", "Copper", and "Bronze" ([355444b](https://gitlab.com/hestia-earth/hestia-glossary/commit/355444b2baa428b70cfc072f6fd84befe772f925)), closes [#1173](https://gitlab.com/hestia-earth/hestia-glossary/issues/1173)
* **other:** move `Instrument air` and `Steam (1 bar pressure)` to `processingAid` ([05c0cea](https://gitlab.com/hestia-earth/hestia-glossary/commit/05c0cea897de32c62a5c4803cf096b195ee69219)), closes [#1050](https://gitlab.com/hestia-earth/hestia-glossary/issues/1050)
* **pesticideBrandName:** add `Antifouling paint, unspecified` ([cc48040](https://gitlab.com/hestia-earth/hestia-glossary/commit/cc480409b310c290c2151639aa4d7eae1848caf9)), closes [#1174](https://gitlab.com/hestia-earth/hestia-glossary/issues/1174)
* **property:** update `termTypesAllowed` for `Organic (property)` ([a9ca2db](https://gitlab.com/hestia-earth/hestia-glossary/commit/a9ca2dbd688b2cee99dbca96f80b92e08f909d05)), closes [#1176](https://gitlab.com/hestia-earth/hestia-glossary/issues/1176)


### Bug Fixes

* **crop:** fix `N_Content_AG_Residue` and `N_Content_BG_Residue` lookups ([ed9cb9a](https://gitlab.com/hestia-earth/hestia-glossary/commit/ed9cb9a318704d9394defffca96dd24adc3324e4))
* **crop:** fix IPCC_2019_Ratio_AGRes_YieldDM and IPCC_2019_Ratio_BGRes_AGRes lookups ([d1c212b](https://gitlab.com/hestia-earth/hestia-glossary/commit/d1c212b2457f594afb89370c93b8c2d149280df4))
* **emission:** remove `siteTypesAllowed` restriction for background emissions ([1958421](https://gitlab.com/hestia-earth/hestia-glossary/commit/1958421625e97c4c4c11311102767efbef775a26)), closes [#1178](https://gitlab.com/hestia-earth/hestia-glossary/issues/1178)


* **measurement:** rename `Microbial biomass...` terms for consistency ([a3c0cdc](https://gitlab.com/hestia-earth/hestia-glossary/commit/a3c0cdcf24b2a212a09d0deab44780baf9e5a86a))

## [0.35.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.34.1...v0.35.0) (2023-10-31)


### ⚠ BREAKING CHANGES

* **model:** rename `RiskPoll model (Rabl and Spadaro, 2004) and Greco et al 2007` `RiskPoll model`
* **emission:** rename `Total suspended particulates, to air, fuel combustion` `TSP, to air, fuel combustion`
* **emission:** rename `Total suspended particulates, to air, inputs production` `TSP, to air, inputs production`
* **animalBreed:** rename `Sussex` `Sussex (cattle breed)`
* **animalManagement:** rename `Artificial insemination` used `Artificial insemination` and change `unit` from `boolean` to `%`
* **animalManagement:** rename `Stocking density (m2)` `Stocking density, permanent pasture (average)`
* **animalManagement:** rename `Stocking density, initial (m2)` `Stocking density, permanent pasture (initial)`
* **animalManagement:** rename `Stocking density, final (m2)` `Stocking density, permanent pasture (final)`
* **waste:** rename `Dead animal waste` `Dead animal waste (kg mass)`

### Features

* **animalBreed:** add `Rhode Island Red` and `Sussex (chicken breed)`, rename `Sussex` ([cbac06d](https://gitlab.com/hestia-earth/hestia-glossary/commit/cbac06d1cc4e47f730c767dd9b594cb64b6cf6fd)), closes [#1059](https://gitlab.com/hestia-earth/hestia-glossary/issues/1059)
* **animalManagement:** rename `Artificial insemination used` and add `Natural mating` ([1fea592](https://gitlab.com/hestia-earth/hestia-glossary/commit/1fea5927d26d1a1ff75a88a159b3c1a54ad96777)), closes [#1130](https://gitlab.com/hestia-earth/hestia-glossary/issues/1130)
* **animalManagement:** split "Stocking density (m2)" terms into "pasture" and "animal housing" ([b8a308e](https://gitlab.com/hestia-earth/hestia-glossary/commit/b8a308e1705ccdb11dd17c2cd12d1c7221295d93)), closes [#1146](https://gitlab.com/hestia-earth/hestia-glossary/issues/1146)
* **characterisedIndicator:** add `Cumulative exergy extraction from the natural environment` ([ee590b1](https://gitlab.com/hestia-earth/hestia-glossary/commit/ee590b1629ccceda495a1224c6bca7e66f4601ca)), closes [#1166](https://gitlab.com/hestia-earth/hestia-glossary/issues/1166)
* **crop:** add `Maize, steep liquor` ([402f03f](https://gitlab.com/hestia-earth/hestia-glossary/commit/402f03f38db08ad56e9063567a9cbe66f180b104)), closes [#1172](https://gitlab.com/hestia-earth/hestia-glossary/issues/1172)
* **emission:** rename "Total suspended particulates" emissions ([6dc9f54](https://gitlab.com/hestia-earth/hestia-glossary/commit/6dc9f544b3fa80349bdd64c72fb52f46b4d20fef)), closes [#1115](https://gitlab.com/hestia-earth/hestia-glossary/issues/1115)
* **excretaManagement:** add `Periodic spread` ([dc877f3](https://gitlab.com/hestia-earth/hestia-glossary/commit/dc877f3f3b5c7001cfd1a01845a11a9f06adf264)), closes [#1132](https://gitlab.com/hestia-earth/hestia-glossary/issues/1132)
* **forage:** add terms for "Greenleaf desmodium" ([163dc02](https://gitlab.com/hestia-earth/hestia-glossary/commit/163dc02262276e3be00ccff4b340f986d74c850d)), closes [#857](https://gitlab.com/hestia-earth/hestia-glossary/issues/857)
* **inorganicFertiliser:** add `Boron trioxide (kg B)` ([745aa93](https://gitlab.com/hestia-earth/hestia-glossary/commit/745aa937c93c271e0c922cbd31466d760c4814c6)), closes [#1142](https://gitlab.com/hestia-earth/hestia-glossary/issues/1142)
* **model:** add `Dewulf (2007)` ([2752260](https://gitlab.com/hestia-earth/hestia-glossary/commit/2752260b94eebdc30c2706745469e0bf8e3a4873)), closes [#1166](https://gitlab.com/hestia-earth/hestia-glossary/issues/1166)
* **model:** add `Impact 2002+ model` ([3b4bb3b](https://gitlab.com/hestia-earth/hestia-glossary/commit/3b4bb3b522bab1ea81f306a0ab475caf040d1396)), closes [#937](https://gitlab.com/hestia-earth/hestia-glossary/issues/937)
* **model:** rename `RiskPoll model (Rabl and Spadaro, 2004) and Greco et al 2007` ([ded3fab](https://gitlab.com/hestia-earth/hestia-glossary/commit/ded3fabf487d203cba283d1b3616cdb9f6cadd12))
* **processedFood:** add `Barley, grain (rolled)` ([6d3197b](https://gitlab.com/hestia-earth/hestia-glossary/commit/6d3197beef10653284d89371042ca7cb1425c87d)), closes [#1125](https://gitlab.com/hestia-earth/hestia-glossary/issues/1125)
* **processedFood:** add `Feed mix` ([192d397](https://gitlab.com/hestia-earth/hestia-glossary/commit/192d397be0e0fb88234e2b37101b9272bfa402d3)), closes [#1140](https://gitlab.com/hestia-earth/hestia-glossary/issues/1140)
* **processedFood:** add `Soybean, pulp` ([8d79f98](https://gitlab.com/hestia-earth/hestia-glossary/commit/8d79f987b234281e1997e849139fc33cd9af51b8)), closes [#1131](https://gitlab.com/hestia-earth/hestia-glossary/issues/1131)
* **processedFood:** add various flours ([6a6b8ed](https://gitlab.com/hestia-earth/hestia-glossary/commit/6a6b8ed637fd0f3756f2eedcaa72af6f0d0266b1)), closes [#1151](https://gitlab.com/hestia-earth/hestia-glossary/issues/1151)
* **processingAid:** add multiple aids used in the corn wet milling process ([b3a616c](https://gitlab.com/hestia-earth/hestia-glossary/commit/b3a616c8cca5771d1c55b7f21cefe84586da4047)), closes [#1171](https://gitlab.com/hestia-earth/hestia-glossary/issues/1171)
* **property:** add `Ad libitum feeding` ([b28b80f](https://gitlab.com/hestia-earth/hestia-glossary/commit/b28b80f010051efc36a42816b0eec6359e6f5657)), closes [#1068](https://gitlab.com/hestia-earth/hestia-glossary/issues/1068)
* **property:** add `Age at castration` ([b65e197](https://gitlab.com/hestia-earth/hestia-glossary/commit/b65e197ff6918611a6832ef97c9ec20fd81e821c)), closes [#963](https://gitlab.com/hestia-earth/hestia-glossary/issues/963)
* **property:** add `Organic (property)` ([32f78ee](https://gitlab.com/hestia-earth/hestia-glossary/commit/32f78ee6d67b9088e146ad6e823ab11201e6884b)), closes [#1047](https://gitlab.com/hestia-earth/hestia-glossary/issues/1047)
* **waste:** add H2S, NO3, and NH3 emissions factors to `Oil palm mill effluent (waste)` ([173cfa4](https://gitlab.com/hestia-earth/hestia-glossary/commit/173cfa495aab47df8b98929de95396ddf47146a9)), closes [#1113](https://gitlab.com/hestia-earth/hestia-glossary/issues/1113)
* **waste:** split `Dead animal waste` into "kg mass" and "number of animals" ([5231d79](https://gitlab.com/hestia-earth/hestia-glossary/commit/5231d798afa8857043d15e74daf581c1e393f3cd)), closes [#1149](https://gitlab.com/hestia-earth/hestia-glossary/issues/1149)


### Bug Fixes

* **emission:** update GWP of various gases to match IPCC 2013 report ([41e4779](https://gitlab.com/hestia-earth/hestia-glossary/commit/41e4779cec5e726a76ebdb89708c08e6957cb77b)), closes [#489](https://gitlab.com/hestia-earth/hestia-glossary/issues/489)

### [0.34.1](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.34.0...v0.34.1) (2023-10-23)


### Features

* **excretaManagement:** add `Composting, unspecified` ([b3265c5](https://gitlab.com/hestia-earth/hestia-glossary/commit/b3265c53dd580809d5b01cd55edbb3c4f784fc0f)), closes [#1164](https://gitlab.com/hestia-earth/hestia-glossary/issues/1164)
* **measurement:** add `Stone content` ([4fd1adf](https://gitlab.com/hestia-earth/hestia-glossary/commit/4fd1adf4b1ea9ecbf710b5ca95024ab26541117d)), closes [#1170](https://gitlab.com/hestia-earth/hestia-glossary/issues/1170)
* **methodMeasurement:** and `In vitro` and `In vivo` ([cba03d7](https://gitlab.com/hestia-earth/hestia-glossary/commit/cba03d7f7e286f7583d490225e32256feeeae9b6)), closes [#1157](https://gitlab.com/hestia-earth/hestia-glossary/issues/1157)
* **operation:** add `Heating waterbodies` ([c191659](https://gitlab.com/hestia-earth/hestia-glossary/commit/c191659c66fef70c2e800146a3fb0ac537be2c86))
* **operation:** add `Transporting inputs within site, with forklift` ([3a7838b](https://gitlab.com/hestia-earth/hestia-glossary/commit/3a7838bdc6e2af0a8508492820aea44f8c695333)), closes [#1158](https://gitlab.com/hestia-earth/hestia-glossary/issues/1158)
* **pesticideAI:** add `Pentafluoroethane` ([8db9023](https://gitlab.com/hestia-earth/hestia-glossary/commit/8db902327d9787deed9a7608ede63265fc2f1fdd))
* **pesticideAI:** decapitalise `CHLORODIFLUOROMETHANE` and `CHLOROFLUOROMETHANE` ([cb9ba99](https://gitlab.com/hestia-earth/hestia-glossary/commit/cb9ba993bbd14f55818dd7c6142b0b75417cb86b))
* **pesticideBrandName:** add `Atlantis`, `Biopower (adjuvant)`, and `Buctril M` ([d122abe](https://gitlab.com/hestia-earth/hestia-glossary/commit/d122abeafc8c97e2d85b781854d2b1871cecd48d)), closes [#1094](https://gitlab.com/hestia-earth/hestia-glossary/issues/1094)
* **pesticideBrandName:** add `Corum SL` and `Coprantol Duo` ([908505b](https://gitlab.com/hestia-earth/hestia-glossary/commit/908505b27066a073147779fb724bcf126a241e01)), closes [#1051](https://gitlab.com/hestia-earth/hestia-glossary/issues/1051)
* **pesticideBrandName:** add `Extravon (adjuvant)` ([e1f285a](https://gitlab.com/hestia-earth/hestia-glossary/commit/e1f285a828a522ee879cdbbfd490a166ad982be1))
* **property:** add `Digestible dry matter` and `Digestible organic matter` ([1604f87](https://gitlab.com/hestia-earth/hestia-glossary/commit/1604f87fb37e2bea9e7a3272e2318eef157b50e3)), closes [#1157](https://gitlab.com/hestia-earth/hestia-glossary/issues/1157)
* **property:** add `Water soluble carbohydrate content` and `Ethanol soluble carbohydrate content` ([7739a83](https://gitlab.com/hestia-earth/hestia-glossary/commit/7739a83ded04079852815297e032d5db9c878456)), closes [#1157](https://gitlab.com/hestia-earth/hestia-glossary/issues/1157)
* **property:** allow `liveAnimal` as `termType` on blank nodes ([341eefd](https://gitlab.com/hestia-earth/hestia-glossary/commit/341eefdf330defa0e8742e027f642c604052730c))
* **property:** allow `Salinity` to be used with `termType` `water` ([d8c88ca](https://gitlab.com/hestia-earth/hestia-glossary/commit/d8c88ca30214721e4a9d4226e47d8eb7176719af))
* **soilAmendment:** add `Spent lime` ([fc8034b](https://gitlab.com/hestia-earth/hestia-glossary/commit/fc8034bf6551f6f79e4b52326178d46ba6b8bcf8)), closes [#1169](https://gitlab.com/hestia-earth/hestia-glossary/issues/1169)


### Bug Fixes

* **characterisedIndicator:** fix error in model mapping file ([f57c3c3](https://gitlab.com/hestia-earth/hestia-glossary/commit/f57c3c3e41fbb4e939d5b2543f696f29755c6d57))
* **cropResidue:** remove `permanent pasture` from `siteTypesAllowed` ([2ee9c4f](https://gitlab.com/hestia-earth/hestia-glossary/commit/2ee9c4f1fd944c32f16051e84c1b91039dc6c31a)), closes [#1162](https://gitlab.com/hestia-earth/hestia-glossary/issues/1162)
* **property:** review termTypes allowed for all properties ([38a0dd2](https://gitlab.com/hestia-earth/hestia-glossary/commit/38a0dd2907661d589380adb5d2b2c82d09e6a93f))

## [0.34.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.33.0...v0.34.0) (2023-10-11)


### ⚠ BREAKING CHANGES

* **pesticideBrandName:** Deletes `Ethephon 6.0`.
* **machinery:** Deletes `Industrial machinery, unspecified`.
* **machinery:** Renames `Excavator` `Digger`.
* **machinery:** Renames `Trailed sprayer` `Trailer sprayer`.
* **machinery:** Renames `Food crusher` `Crusher`.
* **building:** Deletes `Infrastructure, unspecified` and `Agriculture storage tank`.
* **feedFoodAdditive:** rename `Monocalcium phoshate` to `Monocalcium phosphate`
* **pesticideAI:** rename `CAS-11141-17-6` `Azadirachtin`
* **pesticideAI:** rename `CAS-95507-03-2` `Azadirachtin B`
* **pesticideAI:** rename `CAS-25322-68-3` `Polyethylene Glycol`

### Features

* **animalManagement:** add `Laying period` ([aad7473](https://gitlab.com/hestia-earth/hestia-glossary/commit/aad74731e26f66e158cfeddab695702d0b0fd45b)), closes [#1137](https://gitlab.com/hestia-earth/hestia-glossary/issues/1137)
* **animalManagement:** add `Stocking density, initial (m2)` ([2b96a97](https://gitlab.com/hestia-earth/hestia-glossary/commit/2b96a97ae18f301c40a524b5d48eaf3d160acedd)), closes [#1135](https://gitlab.com/hestia-earth/hestia-glossary/issues/1135)
* **animalManagement:** add `Total animal population` ([e0f2057](https://gitlab.com/hestia-earth/hestia-glossary/commit/e0f2057b213356c5c4f6953cf22e6db8c6f6a99e)), closes [#909](https://gitlab.com/hestia-earth/hestia-glossary/issues/909)
* **animalProduct:** add `Fat, poultry` ([f6b44d2](https://gitlab.com/hestia-earth/hestia-glossary/commit/f6b44d200a8580c3f43602fbc8827c93080c5702)), closes [#1088](https://gitlab.com/hestia-earth/hestia-glossary/issues/1088)
* **aquacultureManagement:** add agrovoc links ([e2ab05f](https://gitlab.com/hestia-earth/hestia-glossary/commit/e2ab05f477886efcdc2676be2107428197718f0f)), closes [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72)
* **biologicalControlAgent:** add `Pythium oligandrum` ([382b263](https://gitlab.com/hestia-earth/hestia-glossary/commit/382b263ae3749b4d41a36321e93a55cdb0cf3524))
* **building:** reconcile terms to AGROVOC ([3400583](https://gitlab.com/hestia-earth/hestia-glossary/commit/34005838078a78b22207d66a81d376d03cfc022b)), closes [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72) [#1093](https://gitlab.com/hestia-earth/hestia-glossary/issues/1093)
* **crop:** add `Cassava, pellet` ([8c0119e](https://gitlab.com/hestia-earth/hestia-glossary/commit/8c0119ef1c4998611c82994a4b0d569e79d0908c)), closes [#1134](https://gitlab.com/hestia-earth/hestia-glossary/issues/1134)
* **crop:** add `Sugar beet, molasses` ([a896aae](https://gitlab.com/hestia-earth/hestia-glossary/commit/a896aae524f1e7f7426e09e4619e32a9e0f24eaa)), closes [#1148](https://gitlab.com/hestia-earth/hestia-glossary/issues/1148)
* **crop:** add missing crops to match LoginEko terms ([f827d3f](https://gitlab.com/hestia-earth/hestia-glossary/commit/f827d3fc109005a4d359d6f46c64ce0dcd6d142f)), closes [#1145](https://gitlab.com/hestia-earth/hestia-glossary/issues/1145)
* **cropSupport:** add AGROVOC link to `Trellis` ([887cde4](https://gitlab.com/hestia-earth/hestia-glossary/commit/887cde487dbfdec8762674ecde329f2938041b5c)), closes [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72)
* **feedFoodAdditive:** add `Calcium carbonate (feed food additive)` ([8947a8a](https://gitlab.com/hestia-earth/hestia-glossary/commit/8947a8ad2bf3bdd25256eec2b09e13ba719853c6)), closes [#1121](https://gitlab.com/hestia-earth/hestia-glossary/issues/1121)
* **feedFoodAdditive:** add `Dicalcium phosphate (feed food additive)` ([697ab32](https://gitlab.com/hestia-earth/hestia-glossary/commit/697ab3261d6263ef99d21837336ba597a278f2e6)), closes [#1121](https://gitlab.com/hestia-earth/hestia-glossary/issues/1121)
* **feedFoodAdditive:** rename `Monocalcium phoshate` to `Monocalcium phosphate` ([d79adfd](https://gitlab.com/hestia-earth/hestia-glossary/commit/d79adfd07321b1c815b5fbc6f3433191da1ee75f))
* **forage:** add AG res to yield ratio and BG to AG res ratio lookups ([b1975a9](https://gitlab.com/hestia-earth/hestia-glossary/commit/b1975a956c5b0eb75b00ec448aa9a5a4b00b0280))
* **forage:** add IPCC land-use category lookup ([dd49331](https://gitlab.com/hestia-earth/hestia-glossary/commit/dd4933125b40a558aaaa746d3094d2cca4a073c7))
* **forage:** add terms for "generic leguminous grass" and "tall fescue" ([45e6020](https://gitlab.com/hestia-earth/hestia-glossary/commit/45e60208d292aaf990b3937c98b99ae1ce2c62ce)), closes [#1147](https://gitlab.com/hestia-earth/hestia-glossary/issues/1147)
* **irrigation:** expand glossary to match to AGROVOC ([6b6bdc4](https://gitlab.com/hestia-earth/hestia-glossary/commit/6b6bdc47cf7a74163a06c3ca0f80910a0f37fa46)), closes [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72)
* **liveAquaticSpecies:** add `Brine shrimp cyst`, `Mud crab, unspecified`, and `Orange mud crab` ([bc2ea7d](https://gitlab.com/hestia-earth/hestia-glossary/commit/bc2ea7db8762332991350fa3ad7a0211ccf24837)), closes [#1080](https://gitlab.com/hestia-earth/hestia-glossary/issues/1080)
* **machinery:** expand glossary to match to AGROVOC and add new terms ([0bafda7](https://gitlab.com/hestia-earth/hestia-glossary/commit/0bafda7a5e1cb6936015a3dfc455ae2dd1f15b5d)), closes [#1021](https://gitlab.com/hestia-earth/hestia-glossary/issues/1021) [#473](https://gitlab.com/hestia-earth/hestia-glossary/issues/473) [#1138](https://gitlab.com/hestia-earth/hestia-glossary/issues/1138) [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72)
* **operation:** add `Soil compaction, machine unspecified` and `Sowing seeds, with strip-drill` ([bef2d8e](https://gitlab.com/hestia-earth/hestia-glossary/commit/bef2d8e93eb7f92774594b4b426fb19d9263a208))
* **operation:** add multiple terms to map with LoginEko operation ([0bc428c](https://gitlab.com/hestia-earth/hestia-glossary/commit/0bc428c7ac28066b2242ec119dab50ddb3c7ffc6)), closes [#1144](https://gitlab.com/hestia-earth/hestia-glossary/issues/1144)
* **pesticideAI:** add `Galigan EC`, `Silglif MK`,  and `Siaram 20 WG` ([15baa54](https://gitlab.com/hestia-earth/hestia-glossary/commit/15baa549a990e1e1c66f69bda8516933b4e3e248))
* **pesticideAI:** rename "Azadirachtin" pesticides ([f29cfd2](https://gitlab.com/hestia-earth/hestia-glossary/commit/f29cfd2cba8076e1ce71e0f9a5eb637263783c3a))
* **pesticideAI:** rename `CAS-25322-68-3` `Polyethylene Glycol` ([7ad6b2e](https://gitlab.com/hestia-earth/hestia-glossary/commit/7ad6b2e24975c158a5d33f965a22b0a0d0dcec90))
* **pesticideBrandName:** add missing formulations to map with LoginEko ([6d1263a](https://gitlab.com/hestia-earth/hestia-glossary/commit/6d1263aa600e4019cfd31b2773148bf8222ad9bd)), closes [#1153](https://gitlab.com/hestia-earth/hestia-glossary/issues/1153)
* **processedFood:** add `Biscuit mix` ([f7f8b41](https://gitlab.com/hestia-earth/hestia-glossary/commit/f7f8b4168be28cebf428c59e6102baaa2ef76f57)), closes [#1139](https://gitlab.com/hestia-earth/hestia-glossary/issues/1139)
* **processedFood:** add `Breadcrumb mix` ([b99a754](https://gitlab.com/hestia-earth/hestia-glossary/commit/b99a754c1e8b5e5132cdaaafa9db2d3e7499d213)), closes [#1139](https://gitlab.com/hestia-earth/hestia-glossary/issues/1139)
* **processedFood:** add `Brine shrimp (dried)` ([caa0ebf](https://gitlab.com/hestia-earth/hestia-glossary/commit/caa0ebfae7fd3661c7a007f7ee908b59eb78c812)), closes [#1119](https://gitlab.com/hestia-earth/hestia-glossary/issues/1119)
* **processedFood:** add `Rice waffles` ([e6fa8d9](https://gitlab.com/hestia-earth/hestia-glossary/commit/e6fa8d9677e68a01f1258f385bb1600561427d78)), closes [#1139](https://gitlab.com/hestia-earth/hestia-glossary/issues/1139)
* **processedFood:** add `Syrup, unspecified` ([4d810a9](https://gitlab.com/hestia-earth/hestia-glossary/commit/4d810a9bea1918ff7db34774a063c845ea1c0e2d)), closes [#1139](https://gitlab.com/hestia-earth/hestia-glossary/issues/1139)
* **processedFood:** add cheese terms ([d80f12f](https://gitlab.com/hestia-earth/hestia-glossary/commit/d80f12fcca7ae1828e5216b5c4219940b84682fb)), closes [#1128](https://gitlab.com/hestia-earth/hestia-glossary/issues/1128)
* **processingAid:** add `R407-A` ([dff7aa5](https://gitlab.com/hestia-earth/hestia-glossary/commit/dff7aa5768c8188b3c298c97b7f3959b0653953d)), closes [#1079](https://gitlab.com/hestia-earth/hestia-glossary/issues/1079)
* **property:** add `fertiliserBrandName` as allowed `termType` for nutrient content terms ([a3f3a03](https://gitlab.com/hestia-earth/hestia-glossary/commit/a3f3a03db5ca53a72d2f519a33f27378a401327c))
* **system:** add agrovoc links ([e8f1035](https://gitlab.com/hestia-earth/hestia-glossary/commit/e8f103548ee795c7d74e499383d069c81d3b339b)), closes [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72)


### Bug Fixes

* **organicFertiliser:** replace `methodModel.name` with `description` for `defaultProperties` ([1213066](https://gitlab.com/hestia-earth/hestia-glossary/commit/12130668f00dbddf0243fbb65710428539dd6d8b)), closes [#1154](https://gitlab.com/hestia-earth/hestia-glossary/issues/1154)
* **pesticideBrandName:** delete `Ethephon 6.0` ([71ae85f](https://gitlab.com/hestia-earth/hestia-glossary/commit/71ae85f1900b827f6407019cbb71fb559b85d8d4)), closes [#1154](https://gitlab.com/hestia-earth/hestia-glossary/issues/1154)
* replace `methodModel.name` with `description` for `defaultProperties` ([30fc71c](https://gitlab.com/hestia-earth/hestia-glossary/commit/30fc71c0c6f95daef0594c6127419f1dba872cef)), closes [#1154](https://gitlab.com/hestia-earth/hestia-glossary/issues/1154)

## [0.33.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.32.0...v0.33.0) (2023-09-25)


### ⚠ BREAKING CHANGES

* **operation:** delete `Pruning, with pruning shears`
* **processingAid:** Uses CAS numbers as id's for `Albumin`, `Activated carbon`,
and `Hexane`.
* **characterisedIndicator:** Renames `Soil quality index` to `Biotic production, total
land use effects`
* **pesticideAI:** rename `(2R)-2-[4-[(6-Chloro-2-quinoxalinyl)oxy]phenoxy]propanoic acid ethyl ester` `Quizalofop-P-ethyl`

### Features

* add `ecoinventReferenceProductId` ([3aa2214](https://gitlab.com/hestia-earth/hestia-glossary/commit/3aa2214642436b56647a57bba35058e6c30498b5))
* **building:** add `Well` ([60643a4](https://gitlab.com/hestia-earth/hestia-glossary/commit/60643a405e50543149e0977bba3d2070f90b0e81)), closes [#1116](https://gitlab.com/hestia-earth/hestia-glossary/issues/1116)
* **characterisedIndicator:** add LANCA indicators ([f73dc06](https://gitlab.com/hestia-earth/hestia-glossary/commit/f73dc0662f1cf1ae6452c3d154f8573385999b03))
* **crop:** add `Truffle fungus` and `Truffle, fruiting body` ([64ac8a6](https://gitlab.com/hestia-earth/hestia-glossary/commit/64ac8a653e0fac10fd74165d4b60ff6e6f7dd0ca)), closes [#1123](https://gitlab.com/hestia-earth/hestia-glossary/issues/1123)
* **crop:** add back `Oil palm, mill effluent` ([d849dda](https://gitlab.com/hestia-earth/hestia-glossary/commit/d849dda4256fe263baa4f862cc0ea55cb2bc0c6b)), closes [#1114](https://gitlab.com/hestia-earth/hestia-glossary/issues/1114)
* **crop:** add carbon content of crop residue lookup ([8cedc1c](https://gitlab.com/hestia-earth/hestia-glossary/commit/8cedc1c418245067491748623f2614aa74cabf3b))
* **emission:** set `CH4, to air, waste treatment` to within Hestia system boundary ([63bc243](https://gitlab.com/hestia-earth/hestia-glossary/commit/63bc2436b5ca3919089a72b4d2a8cc3b0ababe05))
* **fertiliserBrandName:** add `Zetaminol` ([59a526d](https://gitlab.com/hestia-earth/hestia-glossary/commit/59a526d3f2ad19f49effdec941e18b97a789a036)), closes [#1097](https://gitlab.com/hestia-earth/hestia-glossary/issues/1097)
* **forage:** add N content of crop residues lookup ([3fe9fff](https://gitlab.com/hestia-earth/hestia-glossary/commit/3fe9fff0671531f98c67840716e0e2de704ca988))
* **fuel:** add `ecoinventReferenceProductId` ([3a7c0c3](https://gitlab.com/hestia-earth/hestia-glossary/commit/3a7c0c370760a55132de5f998a05918001e9dafc))
* **inorganicFertiliser:** add `ecoinventReferenceProductId` ([e0107c4](https://gitlab.com/hestia-earth/hestia-glossary/commit/e0107c4a5095ed327b0843f75b5d5882d007599c))
* **inorganicFertiliser:** add `Zinc sulphate (kg Zn)` ([9f57f8d](https://gitlab.com/hestia-earth/hestia-glossary/commit/9f57f8d363de3ae722a1f1336284e50e1545a80f))
* **machinery:** add `Rotary tiller` and `Thresher` ([6663a4a](https://gitlab.com/hestia-earth/hestia-glossary/commit/6663a4ae402697cb2447ea82001b5ee44c45018b))
* **model:** add `Akiyama et al (2010)` ([3d4ca93](https://gitlab.com/hestia-earth/hestia-glossary/commit/3d4ca931df50f905b27d204fe8b650d3016ae1fe))
* **model:** add `Jarvis and Pain (1994)` ([af381c1](https://gitlab.com/hestia-earth/hestia-glossary/commit/af381c1a2e0cc63d081bda9947135214036bddde))
* **model:** add `Schmidt (2007)` ([1fa4ef1](https://gitlab.com/hestia-earth/hestia-glossary/commit/1fa4ef1b22902abca4702981dd1546af6003229f)), closes [#1120](https://gitlab.com/hestia-earth/hestia-glossary/issues/1120)
* **operation:** add `Harvesting fruits, with electric rake` ([7a8ab31](https://gitlab.com/hestia-earth/hestia-glossary/commit/7a8ab316b07934b4b35d6bb03ee64d4a5fa6b8a2)), closes [#1077](https://gitlab.com/hestia-earth/hestia-glossary/issues/1077)
* **operation:** add `Mixing, with kneading machine` ([9d6c320](https://gitlab.com/hestia-earth/hestia-glossary/commit/9d6c320da854c6b6a1221e2f80edcca84191baaa)), closes [#1077](https://gitlab.com/hestia-earth/hestia-glossary/issues/1077)
* **operation:** add `Scarifying seeds, machine unspecified` and `Scarifying seeds, by hand` ([1945278](https://gitlab.com/hestia-earth/hestia-glossary/commit/1945278210f418cb2383268243349387744bb955)), closes [#1109](https://gitlab.com/hestia-earth/hestia-glossary/issues/1109)
* **operation:** add `Sowing seeds, with broadcaster` ([717ab65](https://gitlab.com/hestia-earth/hestia-glossary/commit/717ab65f147cfca69de150b1f8632ab50b1f746c))
* **operation:** add `Threshing, by hand` ([87a8148](https://gitlab.com/hestia-earth/hestia-glossary/commit/87a8148a81df17b4126b2214c2f4603d4ba74c75))
* **operation:** add more specific "Fertilising, broadcasting" terms ([0656672](https://gitlab.com/hestia-earth/hestia-glossary/commit/06566720020769ecdcd791dfd2ca9fe829d6b356))
* **operation:** add Ploughing, with disc plough ([8d1ccfc](https://gitlab.com/hestia-earth/hestia-glossary/commit/8d1ccfc34cc22d0e0651645fc025459ad69e3a1d)), closes [#1122](https://gitlab.com/hestia-earth/hestia-glossary/issues/1122)
* **operation:** add terms for "Transporting inputs and products within site" ([bd61531](https://gitlab.com/hestia-earth/hestia-glossary/commit/bd6153158394a16f165376a640bd6c17a700fc26))
* **operation:** delete `Pruning, with pruning shears` and add `Pruning, with penumatic shears` ([527eead](https://gitlab.com/hestia-earth/hestia-glossary/commit/527eeadda37f0eb7c4a631e6a3db708efaff1c3e)), closes [#1077](https://gitlab.com/hestia-earth/hestia-glossary/issues/1077)
* **organicFertiliser:** add default properties (lignin, N and C) ([49c2516](https://gitlab.com/hestia-earth/hestia-glossary/commit/49c2516c268340e05741774b333782eaa5014171))
* **pesticideAI:** add `Benzimidazole compounds, unspecified` ([4764119](https://gitlab.com/hestia-earth/hestia-glossary/commit/476411980532af402d2e6e25839c731e7bbb6419))
* **pesticideAI:** add `Nonoxynol-9` ([f518321](https://gitlab.com/hestia-earth/hestia-glossary/commit/f51832162b98e13b4ba2542d24ef49587b9bddd2)), closes [#1075](https://gitlab.com/hestia-earth/hestia-glossary/issues/1075)
* **pesticideAI:** rename `CAS-100646-51-3` `Quizalofop-P-ethyl` ([b6538f1](https://gitlab.com/hestia-earth/hestia-glossary/commit/b6538f184007d258798284fbaa82c48b0153b2a1))
* **pesticideAI:** rename `Monoethyl ester phosphonic acid aluminum salt (3:1)` `Fosetyl-Al` ([f803ae5](https://gitlab.com/hestia-earth/hestia-glossary/commit/f803ae55c0a066a65a4360ba3a702a121be67b04))
* **processingAid:** add `Sodium methoxide` ([380d0bb](https://gitlab.com/hestia-earth/hestia-glossary/commit/380d0bbc77e2e8018fb3c6aa75c4cfb48a44ee9f)), closes [#1069](https://gitlab.com/hestia-earth/hestia-glossary/issues/1069)
* **property:** add `Total suspended solids content` ([d2eae24](https://gitlab.com/hestia-earth/hestia-glossary/commit/d2eae2480e3142e2bdbec7d2a5b0572019a11111)), closes [#1067](https://gitlab.com/hestia-earth/hestia-glossary/issues/1067)
* **region:** add LANCA lookup files ([7e87700](https://gitlab.com/hestia-earth/hestia-glossary/commit/7e877000efcac937594ee51e935c29f2b88c02a2))
* **soilAmendment:** add `ecoinventReferenceProductId` ([cf6a7f8](https://gitlab.com/hestia-earth/hestia-glossary/commit/cf6a7f8ef6f42a405ec472ad44b970be79c78e3f))
* **soilAmendment:** add default properties (lignin, N and C) ([7a5b858](https://gitlab.com/hestia-earth/hestia-glossary/commit/7a5b8582a9002bcbc7f8c232240baaed3644fb5c))


### Bug Fixes

* **characterisedIndicator:** wrong mapping for `Recipe 2008 Egalitarian` ([ec5087e](https://gitlab.com/hestia-earth/hestia-glossary/commit/ec5087e8015752b53437aa1186e82bd61d0f6bdf))
* **emission:** update `inHestiaDefaultSystemBoundary` to match available models ([5d8533a](https://gitlab.com/hestia-earth/hestia-glossary/commit/5d8533a61f0715e116e72f75d29dfe67ed2cd2c0)), closes [#1107](https://gitlab.com/hestia-earth/hestia-glossary/issues/1107)
* **operation:** add missing `CO, to air` emissions lookup ([7b73f98](https://gitlab.com/hestia-earth/hestia-glossary/commit/7b73f9818d0a41d50aafebeaace5c9efcd683c23))
* **processingAid:** use CAS numbers for more identifiers ([0b39295](https://gitlab.com/hestia-earth/hestia-glossary/commit/0b39295457783828594de2cb943749d95881aee9))

## [0.32.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.31.1...v0.32.0) (2023-09-12)


### ⚠ BREAKING CHANGES

* **property:** Renames `BOD3 content` to `BOD3`, renames
`BOD5 content` to `BOD5`, and changes `units` on `BOD3` and `BOD5` from
`%` to `mg / L`.
* **characterisedIndicator:** Renames `Abiotic depletion, fossil fuels` to
 `Abiotic resource depletion, fossil fuels`
* **characterisedIndicator:** Renames `Ionising radiation` to
`Ionising radiation (kBq Co-60 eq)`.
* **characterisedIndicator:** Renames `Freshwater ecotoxicity potential (PAF)` to
`Freshwater ecotoxicity potential (CTUe)`
* **pesticideAI:** Renames `pafM3DFreshwaterEcotoxicityUsetox` `pafM3DFreshwaterEcotoxicityUsetox2-1Hc50Ec50eq`.
* **pesticideAI:** Renames `pafM3DFreshwaterEcotoxicityUsetoxHC20EC10eq` `pafM3DFreshwaterEcotoxicityUsetox2-1Hc20Ec10eq`.
* **crop:** Renames `Oil palm, mill effluent` to
`Oil palm, mill effluent (decanted)`.
* **fuel:** Renames `Motor gasoline` to `Petrol`.

### Features

* **characterisedIndicator:** add `Abiotic resource depletion, minerals and metals` ([5d54a22](https://gitlab.com/hestia-earth/hestia-glossary/commit/5d54a222159d5840f7ffbd8856c888a257509f12))
* **characterisedIndicator:** add `Human toxicity potential... (CTUh)` terms ([35269c2](https://gitlab.com/hestia-earth/hestia-glossary/commit/35269c2f86971dcada01d1381c96e0fac6de9023))
* **characterisedIndicator:** add `Photochemical ozone creation potential, human health (NMVOC eq)` ([67666fa](https://gitlab.com/hestia-earth/hestia-glossary/commit/67666fa1ef0182aca47f3066592c96a903dafccd))
* **characterisedIndicator:** add `unitsDescription` ([8cdd2ae](https://gitlab.com/hestia-earth/hestia-glossary/commit/8cdd2ae67612dba666d9e3a1bb1ffab2e2a0d274)), closes [#717](https://gitlab.com/hestia-earth/hestia-glossary/issues/717)
* **characterisedIndicator:** improve definition of eutrophication terms ([5272069](https://gitlab.com/hestia-earth/hestia-glossary/commit/52720697d79a6bf031534a4c5d6db488743b44d0))
* **characterisedIndicator:** split `Ionising radiation` into multiple terms ([f4f7cf9](https://gitlab.com/hestia-earth/hestia-glossary/commit/f4f7cf9dec4cc0db5310f91a1cfa2cfbcfd780fe))
* **crop:** add `Sweet corn, ear` ([7cf0843](https://gitlab.com/hestia-earth/hestia-glossary/commit/7cf084345a297686d2f1d237d524a3a7b26cd8f4)), closes [#1057](https://gitlab.com/hestia-earth/hestia-glossary/issues/1057)
* **crop:** add lignin content of crop residue lookup ([01839f3](https://gitlab.com/hestia-earth/hestia-glossary/commit/01839f3c5c235678288dd8b0641ed18aac2913c6))
* **crop:** rename `Oil palm, mill effluent` to `Oil palm, mill effluent (decanted)` ([03f6c53](https://gitlab.com/hestia-earth/hestia-glossary/commit/03f6c53a42f8026f780cf719790216fb566ac6eb))
* **crop:** update grape nursery lookup values ([bcdd2ea](https://gitlab.com/hestia-earth/hestia-glossary/commit/bcdd2ea7ae48d24d0218b775d02fd9699b769bbc)), closes [#1091](https://gitlab.com/hestia-earth/hestia-glossary/issues/1091)
* **crop:** update sweet corn ear lookup ([bfb7a4c](https://gitlab.com/hestia-earth/hestia-glossary/commit/bfb7a4c5320b2ba8070ca5285238b37b409ab435))
* **emission:** add `Black carbon, to air` and `Total suspended particulates, to air` ([9404669](https://gitlab.com/hestia-earth/hestia-glossary/commit/9404669191203090fdabe951cf8cba8bf432fb3a)), closes [#1090](https://gitlab.com/hestia-earth/hestia-glossary/issues/1090)
* **emission:** add `CO, to air, inputs production` and `CO, to air, fuel combustion` ([1e39686](https://gitlab.com/hestia-earth/hestia-glossary/commit/1e39686ea087e09c7b85c59dadb5731122b25e52)), closes [#876](https://gitlab.com/hestia-earth/hestia-glossary/issues/876)
* **emission:** add `CO2` and `CH4` from `crop residue decomposition` ([be6258c](https://gitlab.com/hestia-earth/hestia-glossary/commit/be6258c92be4ebf8267de1e870d785e94df2cf5f)), closes [#988](https://gitlab.com/hestia-earth/hestia-glossary/issues/988)
* **forage:** add carbon content of crop residue lookup ([08b1796](https://gitlab.com/hestia-earth/hestia-glossary/commit/08b179603265d70c21e3cbdeab059299fe7ba38a))
* **forage:** add lignin content of crop residue lookup ([ee3b437](https://gitlab.com/hestia-earth/hestia-glossary/commit/ee3b437ae5001ce66841365959e89220d0980cb9))
* **fuel:** add `Oil palm, press fibre (fuel)` and `Oil palm, shell (fuel)` ([2c78444](https://gitlab.com/hestia-earth/hestia-glossary/commit/2c784449d335a1772e2671c6f3e05bd4f36aedfd)), closes [#1074](https://gitlab.com/hestia-earth/hestia-glossary/issues/1074)
* **fuel:** rename `Motor gasoline` to `Petrol` ([22a0d9b](https://gitlab.com/hestia-earth/hestia-glossary/commit/22a0d9b2dce7a70abcfb122805e190ef4527bdb4)), closes [#1089](https://gitlab.com/hestia-earth/hestia-glossary/issues/1089)
* **model:** add `Frischknecht et al (2000)` model for `Ionising radiation` ([315b386](https://gitlab.com/hestia-earth/hestia-glossary/commit/315b386a1413fa2263206e173484f610d52bdab0))
* **operation:** add lookups for EMEA-EEA 2019 model ([87e1495](https://gitlab.com/hestia-earth/hestia-glossary/commit/87e1495943271ac895ada8cd3fda3e61fe25430b))
* **organicFertiliser:** add default `ligninContent` to manures ([01dd252](https://gitlab.com/hestia-earth/hestia-glossary/commit/01dd2527d7ac068694d073ffe8e81cadf2182c8b))
* **property:** add `COD` or "Chemical oxygen demand" ([0ae507c](https://gitlab.com/hestia-earth/hestia-glossary/commit/0ae507c8b1cde940354aa0810513018ccba713c9)), closes [#1066](https://gitlab.com/hestia-earth/hestia-glossary/issues/1066)
* **property:** add `water` as allowed term type for `COD`, `BOD3`, and `BOD5` ([d86701b](https://gitlab.com/hestia-earth/hestia-glossary/commit/d86701be41018a95a4b7b3c0c802ed19e327e5a4))
* **property:** add `Weight per item` ([4c68eaa](https://gitlab.com/hestia-earth/hestia-glossary/commit/4c68eaac3d77b52735e2be7f48bfd24fdbfcf65d)), closes [#1048](https://gitlab.com/hestia-earth/hestia-glossary/issues/1048)
* **property:** rename `BOD content` terms to `BOD3` and `BOD5` and change `units` ([9b9ebfb](https://gitlab.com/hestia-earth/hestia-glossary/commit/9b9ebfba39244a628a5b7dfc56db304677ad26fb)), closes [#847](https://gitlab.com/hestia-earth/hestia-glossary/issues/847)
* **resourceUse:** add `unitsDescription` ([80fb2a1](https://gitlab.com/hestia-earth/hestia-glossary/commit/80fb2a1db5a088f9c7727c7f634706d800544a4e)), closes [#717](https://gitlab.com/hestia-earth/hestia-glossary/issues/717)
* **standardsLabels:** add `MSPO Certified` and `ISPO Certified` ([5990086](https://gitlab.com/hestia-earth/hestia-glossary/commit/5990086856d0ce2f9baeb0f0d7756a8256b11d60)), closes [#1061](https://gitlab.com/hestia-earth/hestia-glossary/issues/1061)
* **system:** add `Integrated pest management system` ([748b47a](https://gitlab.com/hestia-earth/hestia-glossary/commit/748b47ac52dfdda8ce3910264bfdf76f24abeb6b)), closes [#917](https://gitlab.com/hestia-earth/hestia-glossary/issues/917)
* **waste:** add `Oil palm mill effluent (waste)` ([cba34b3](https://gitlab.com/hestia-earth/hestia-glossary/commit/cba34b3be4dc3ce9497ad4f2a7e4b63d1f7dbaf4)), closes [#1064](https://gitlab.com/hestia-earth/hestia-glossary/issues/1064)
* **waste:** add lookup for CH4 emissions to `Oil palm mill effluent (waste)` term ([e8a8796](https://gitlab.com/hestia-earth/hestia-glossary/commit/e8a87962db66c471a0542cfb736eef172f6a1ff8)), closes [#1084](https://gitlab.com/hestia-earth/hestia-glossary/issues/1084)


### Bug Fixes

* **characterisedIndicator:** improve clarity of `units` for `Freshwater ecotoxicity potential` ([d329106](https://gitlab.com/hestia-earth/hestia-glossary/commit/d329106ef004912d423b69636628f7d7f71d01db))
* **characterisedIndicator:** rename `Abiotic resource depletion` term ([1eb5978](https://gitlab.com/hestia-earth/hestia-glossary/commit/1eb5978851bb7c484c534874cdcf654d66a75ca7))
* **emission:** acidification lookup errors on `H2S` emissions ([80ab32c](https://gitlab.com/hestia-earth/hestia-glossary/commit/80ab32c4c84e7c4832e73fedecc64d90d6fea433))
* **model:** rename `pecfr2017` to `pefcrGuidanceDocument2017` ([f3fbc5a](https://gitlab.com/hestia-earth/hestia-glossary/commit/f3fbc5ab09b727aa742f429f7ed8760e53ea1f95))
* **pesticideAI:** inconsistencies in USETox lookups ([c3ee535](https://gitlab.com/hestia-earth/hestia-glossary/commit/c3ee535cabdb3eb75e5dd19feb877a311d769612))

### [0.31.1](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.31.0...v0.31.1) (2023-09-05)


### Bug Fixes

* **crop:** error in column headers ([2ee3582](https://gitlab.com/hestia-earth/hestia-glossary/commit/2ee3582660489b0213112c8e2bcd62c82bb050a1))
* **wasteManagement:** errors in column headers ([c359adb](https://gitlab.com/hestia-earth/hestia-glossary/commit/c359adb79492fc268e2b7f663dc65d228c42dc41))

## [0.31.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.30.0...v0.31.0) (2023-09-04)


### ⚠ BREAKING CHANGES

* **pesticideAI:** rename `Chlormequat` `Chlormequat (herbicide adjuvant)`
* **pesticideAI:** rename `Fatty acids, Tall-oil, Ethoxylated` `Fatty acids, Tall-oil, Ethoxylated (adjuvant)`
* **pesticideAI:** rename `Quizalofop-methyl` `(R)-Quizalofop Methyl`
* **pesticideAI:** delete `1,2,4-Triazine`
* **pesticideAI:** delete `Dithiocarbamate`
* **pesticideAI:** delete `Emamectin B1A benzoate`
* **pesticideAI:** delete `Pirimiphos`
* **pesticideAI:** move `Astaxanthin` from `pesticideAI` to `feedFoodAdditive`
* **other:** delete `Monocalcium phosphate`
* **other:** move `Ice` from `other` to `water`, change `units` from `kg` to `m3`, and rename it `Ice, source unspecified`
* **other:** move `Sodium bicarbonate` from `other` to `feedFoodAdditive`
* **other:** move `Sodium chloride` from `other` to `feedFoodAdditive`
* **other:** move `Activated carbon` from `other` to `processingAid`
* **other:** move `Bleaching earth` from `other` to `processingAid`
* **other:** move `Hexane` from `other` to `processingAid`
* **other:** move `Phosphoric acid` from `other` to `processingAid` and rename it `Phosphoric acid (processing aid)`
* **other:** move `Sodium hydroxide` from `other` to `processingAid` and rename it `Sodium hydroxide (processing aid)`
* **other:** move `Sulphuric acid` from other to `processingAid` and rename it `Sulphuric acid (processing aid)`
* **liveAnimal:** rename `Insect, larva` `Insect, larva (kg mass)`
* **liveAnimal:** rename `Mealworm, larva` `Mealworm, larva (kg mass)`
* **antibiotic:** delete `Alkylbenzyldimethyl quaternary ammonium compounds chlorides`

### Features

* **animalProduct:** add `Fat, insects` and `Fat, mealworms` ([b231f83](https://gitlab.com/hestia-earth/hestia-glossary/commit/b231f83204ba737f4611cd6d826eca95d72bfa77)), closes [#939](https://gitlab.com/hestia-earth/hestia-glossary/issues/939)
* **animalProduct:** add `Meat, insects (liveweight)` and `Meat, mealworms (liveweight)` ([d23c9d2](https://gitlab.com/hestia-earth/hestia-glossary/commit/d23c9d2b98b8714f3b05cd7f258720856765025f)), closes [#931](https://gitlab.com/hestia-earth/hestia-glossary/issues/931)
* **biologicalControlAgent:** add `Bacillus amyloliquefaciens` ([380f3f2](https://gitlab.com/hestia-earth/hestia-glossary/commit/380f3f2fd13bd367ea90748fe3f48bbeb7983e1e)), closes [#1029](https://gitlab.com/hestia-earth/hestia-glossary/issues/1029)
* **emission:** update `productTermIdsAllowed` for `CH4, to air, enteric fermentation` ([b06a16c](https://gitlab.com/hestia-earth/hestia-glossary/commit/b06a16cea8a68cbc752480beca1f127c8d7dd7aa))
* **excreta:** add terms for insects excreta ([f5842dd](https://gitlab.com/hestia-earth/hestia-glossary/commit/f5842dda60a1dfde4d59fb99720263d735c90213)), closes [#933](https://gitlab.com/hestia-earth/hestia-glossary/issues/933)
* **feedFoodAdditive:** add `Annatto` ([174f2fa](https://gitlab.com/hestia-earth/hestia-glossary/commit/174f2faa1b57303ceb95f76a1521481ed798b0fd)), closes [#1052](https://gitlab.com/hestia-earth/hestia-glossary/issues/1052)
* **feedFoodAdditive:** add `Calcium chloride (feed food additive)` ([6e0298f](https://gitlab.com/hestia-earth/hestia-glossary/commit/6e0298f80723709c1635515edb4990a64685efe0)), closes [#1052](https://gitlab.com/hestia-earth/hestia-glossary/issues/1052)
* **feedFoodAdditive:** add `Herbal growth promoters, unspecified` and `Yucca extract` ([ce7223b](https://gitlab.com/hestia-earth/hestia-glossary/commit/ce7223b1db545fb3301e352453443ccf17ab2368)), closes [#975](https://gitlab.com/hestia-earth/hestia-glossary/issues/975)
* **feedFoodAdditive:** add mold inhibitors substances ([5f30601](https://gitlab.com/hestia-earth/hestia-glossary/commit/5f30601cd11e1808a6fa1b3e95d1c3b528603745)), closes [#1003](https://gitlab.com/hestia-earth/hestia-glossary/issues/1003)
* **feedFoodAdditive:** add multiple prebiotics ([08bb0b9](https://gitlab.com/hestia-earth/hestia-glossary/commit/08bb0b9c5e2cf69707c2a355e5800681410e1d8d)), closes [#962](https://gitlab.com/hestia-earth/hestia-glossary/issues/962)
* **feedFoodAdditive:** add multiple terms that were previously in `other` ([961250a](https://gitlab.com/hestia-earth/hestia-glossary/commit/961250a13652f62cab02bcc1adb90ab680612b4a)), closes [#1037](https://gitlab.com/hestia-earth/hestia-glossary/issues/1037)
* **fertiliserBrandName:** add `Polysulphate®` ([abaace1](https://gitlab.com/hestia-earth/hestia-glossary/commit/abaace16751f2b5a428469f1ce8e3e6283bf7786)), closes [#1040](https://gitlab.com/hestia-earth/hestia-glossary/issues/1040)
* **landUseManagement:** add `Number of herbicide applications` ([b06bea9](https://gitlab.com/hestia-earth/hestia-glossary/commit/b06bea978769419e7d94b84d2eebe6392025944a)), closes [#1038](https://gitlab.com/hestia-earth/hestia-glossary/issues/1038)
* **liveAnimal:** change `units` of insect terms from `number` to `kg` ([f36f4fb](https://gitlab.com/hestia-earth/hestia-glossary/commit/f36f4fbce78b63acec5685a5fa7390765bcc45ee)), closes [#931](https://gitlab.com/hestia-earth/hestia-glossary/issues/931)
* **machinery:** add multiple processing machines ([97d4a58](https://gitlab.com/hestia-earth/hestia-glossary/commit/97d4a58f94dc01810aeabf6f9044aba7187b7bea)), closes [#878](https://gitlab.com/hestia-earth/hestia-glossary/issues/878)
* **material:** add `Paraffin wax (material)` ([372cc39](https://gitlab.com/hestia-earth/hestia-glossary/commit/372cc39af0c5deececce625c31dac06db9e87abb)), closes [#1053](https://gitlab.com/hestia-earth/hestia-glossary/issues/1053)
* **material:** add `Wood, bark` ([8ddb0a7](https://gitlab.com/hestia-earth/hestia-glossary/commit/8ddb0a7d781b7b4c331d48384b4e335afcc990f0)), closes [#1042](https://gitlab.com/hestia-earth/hestia-glossary/issues/1042)
* **operation:** add `Aging food` ([3956f12](https://gitlab.com/hestia-earth/hestia-glossary/commit/3956f12d297c41eda521c1dc758fdb35579f36a6)), closes [#1044](https://gitlab.com/hestia-earth/hestia-glossary/issues/1044)
* **operation:** add `Electrodialysis` ([7a053aa](https://gitlab.com/hestia-earth/hestia-glossary/commit/7a053aa3e32ebbe2a6872e1d351b51af8591abc7)), closes [#1043](https://gitlab.com/hestia-earth/hestia-glossary/issues/1043)
* **organicFertiliser:** add `Frass (kg N)` and `Frass (kg mass)` ([5306474](https://gitlab.com/hestia-earth/hestia-glossary/commit/5306474471ffc6ede7b0a9be636fec49b1a7979b)), closes [#933](https://gitlab.com/hestia-earth/hestia-glossary/issues/933)
* **other:** delete `Monocalcium phosphate` ([3351ace](https://gitlab.com/hestia-earth/hestia-glossary/commit/3351ace5e1798145b979428dbaa09689d364937f)), closes [#1037](https://gitlab.com/hestia-earth/hestia-glossary/issues/1037)
* **other:** move `Ice` to `water` and rename it `Ice, source unspecified` ([430048e](https://gitlab.com/hestia-earth/hestia-glossary/commit/430048e57fed11a346c7f4b75ba9b8bd26debf28)), closes [#1037](https://gitlab.com/hestia-earth/hestia-glossary/issues/1037)
* **other:** move `Sodium bicarbonate` and `Sodium chloride` to `feedFoodAdditive` ([0e7cf71](https://gitlab.com/hestia-earth/hestia-glossary/commit/0e7cf71207ebeacd7658049a3eb2aa677d398328)), closes [#1037](https://gitlab.com/hestia-earth/hestia-glossary/issues/1037)
* **other:** move multiple terms to the `processingAid ` glossary ([df6d309](https://gitlab.com/hestia-earth/hestia-glossary/commit/df6d309d6b78870fe7ec36d24b38612ffcf17b43)), closes [#1037](https://gitlab.com/hestia-earth/hestia-glossary/issues/1037)
* **pesticideAI:** add `Benzalkonium chloride` ([19cca26](https://gitlab.com/hestia-earth/hestia-glossary/commit/19cca26ed1662650addaa7a95cba750d54ba1f18)), closes [#961](https://gitlab.com/hestia-earth/hestia-glossary/issues/961)
* **pesticideAI:** add `Phenylurea compounds, unspecified` ([8db6f14](https://gitlab.com/hestia-earth/hestia-glossary/commit/8db6f14138e3ce2a52d68d97d101e2ad86644e1e)), closes [#1020](https://gitlab.com/hestia-earth/hestia-glossary/issues/1020)
* **pesticideAI:** delete multiple terms which are not pesticides ([b84cef5](https://gitlab.com/hestia-earth/hestia-glossary/commit/b84cef52e715821f7d57d28fa936ac46dd584071)), closes [#398](https://gitlab.com/hestia-earth/hestia-glossary/issues/398)
* **pesticideAI:** move `Astaxanthin` to `feedFoodAdditive` ([b097475](https://gitlab.com/hestia-earth/hestia-glossary/commit/b097475a18cd9faffc82e6f747818f8fa0213372)), closes [#398](https://gitlab.com/hestia-earth/hestia-glossary/issues/398)
* **pesticideAI:** rename multiple pesticides ([8cec7a0](https://gitlab.com/hestia-earth/hestia-glossary/commit/8cec7a02b112e100f14d27a2ddb578c88297faf3)), closes [#398](https://gitlab.com/hestia-earth/hestia-glossary/issues/398)
* **processedFood:** add `Cheese, buffalo` ([cc2938b](https://gitlab.com/hestia-earth/hestia-glossary/commit/cc2938b2895e98af9922bc13897c65189dc3f08e)), closes [#1056](https://gitlab.com/hestia-earth/hestia-glossary/issues/1056)
* **processedFood:** add `Cheese, cow` ([a7aad60](https://gitlab.com/hestia-earth/hestia-glossary/commit/a7aad60ab4918230321ccbfb437923b47bd7744d)), closes [#1056](https://gitlab.com/hestia-earth/hestia-glossary/issues/1056)
* **processedFood:** add `Cheese, goat` ([33570c5](https://gitlab.com/hestia-earth/hestia-glossary/commit/33570c53c023c37cbd90213ab331743839b19779)), closes [#1056](https://gitlab.com/hestia-earth/hestia-glossary/issues/1056)
* **processedFood:** add `Cheese, sheep` ([d74f627](https://gitlab.com/hestia-earth/hestia-glossary/commit/d74f627a14f74d64bf51517f0f5900e3f7dde544)), closes [#1056](https://gitlab.com/hestia-earth/hestia-glossary/issues/1056)
* **processedFood:** add `Insect meal` and `Mealworm meal` ([f5dc77c](https://gitlab.com/hestia-earth/hestia-glossary/commit/f5dc77cdbec385360f156f3ee0c2428e63c51b25)), closes [#936](https://gitlab.com/hestia-earth/hestia-glossary/issues/936)
* **processingAid:** add `Calcium carbide (processing aid)` ([78342f1](https://gitlab.com/hestia-earth/hestia-glossary/commit/78342f157b93ee37f12c666c6632386715e61b86)), closes [#398](https://gitlab.com/hestia-earth/hestia-glossary/issues/398)
* **processingAid:** add `Nitric acid (processing aid)` ([f658b80](https://gitlab.com/hestia-earth/hestia-glossary/commit/f658b80261c7814d5f2590783bf8d4dca628c935)), closes [#1055](https://gitlab.com/hestia-earth/hestia-glossary/issues/1055)
* **property:** add `BOD5 content` and `BOD3 content` ([7173841](https://gitlab.com/hestia-earth/hestia-glossary/commit/7173841a7d262a6b799772c7d5b689ed16d30be2)), closes [#847](https://gitlab.com/hestia-earth/hestia-glossary/issues/847)
* **property:** add `Frozen` ([8025962](https://gitlab.com/hestia-earth/hestia-glossary/commit/8025962feee5814606bc4b5b87211af3cf88780c)), closes [#932](https://gitlab.com/hestia-earth/hestia-glossary/issues/932)
* **region:** add `Tibet` as synonym for `Xizang` ([6571f85](https://gitlab.com/hestia-earth/hestia-glossary/commit/6571f85ce3913e029204b7b1f713f7a5f8ec5b49))
* **wasteManagement:** add `Septic tank (waste management)` ([15c72f3](https://gitlab.com/hestia-earth/hestia-glossary/commit/15c72f3305daea9ce54c3e87a46216ebd189dc4f)), closes [#1039](https://gitlab.com/hestia-earth/hestia-glossary/issues/1039)


### Bug Fixes

* **antibiotic:** delete `Alkylbenzyldimethyl quaternary ammonium compounds chlorides` ([823d39d](https://gitlab.com/hestia-earth/hestia-glossary/commit/823d39de902e85e2b51ad5de034aa70fdc77b78a)), closes [#961](https://gitlab.com/hestia-earth/hestia-glossary/issues/961)
* **soilTexture:** error in min silt content for `Silt` term ([8398c36](https://gitlab.com/hestia-earth/hestia-glossary/commit/8398c36f0c921fd89aad73e9e3ea519b722c032d))
* **soilTexture:** errors in max sand contents in `Loamy sand` and `Sandy loam` ([426e939](https://gitlab.com/hestia-earth/hestia-glossary/commit/426e9398074639e3486c810c092e7d63540cfdd0))

## [0.30.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.29.1...v0.30.0) (2023-08-15)


### ⚠ BREAKING CHANGES

* **operation:** rename `Harrowing, with small tractor (orchard)` Harrowing, with small tractor (plantation)`
* **operation:** rename `Ploughing, in orchard` `Ploughing, in plantation`
* **operation:** rename `Shaking, with tree shaker (orchard)` `Shaking, with tree shaker`
* **operation:** rename `Heating food, machine unspecified` `Heating (food processing), machine unspecified`
* **operation:** rename `Milling grains` `Dry milling, machine unspecified`
* **operation:** rename `Grinding, by hand` `Dry milling, by hand`
* **operation:** delete `Grinding, with grinder`
* **operation:** rename `Washing, with fresh water` `Washing, by hand`
* **operation:** rename `Transporting, machine unspecified` `Transporting products within site, machine unspecified`
* **operation:** rename `Transporting, with trailer` `Transporting products within site, with trailer`
* **operation:** rename `Transporting, with forage flatbed` `Transporting products within site, with forage flatbed`
* **wasteManagement:** rename `Municipal sewarage system (waste management)` `Municipal sewerage system (waste management)`
* **crop:** delete `Ratio_Abv_to_Below_Grou_crop_residue` lookup (`IPCC_2019_Ratio_BGRes_AGRes` to be used instead)
* **crop:** rename `isOrchard` `isPlantation`
* **crop:** rename `Orchard_duration` `Plantation_lifespan`
* **crop:** rename `Orchard_density` `Plantation_density`
* **crop:** rename `Non_bearing_duration` `Plantation_non-productive_lifespan`
* **crop:** rename `Orchard_longFallowPeriod` `Plantation_longFallowPeriod`
* **crop:** rename `Saplings` lookup `Nursery_density`
* **region:** `Sugar` region was renamed `Sugar (Unknown), Sennar, Sennar, Sudan`.

### Features

* **crop:** add `Olive, seed` ([680f80b](https://gitlab.com/hestia-earth/hestia-glossary/commit/680f80b1ef9373a7a2f81cf7009115fe820d6203)), closes [#1023](https://gitlab.com/hestia-earth/hestia-glossary/issues/1023)
* **crop:** add `Sesame, oil` and `Sesame, meal` ([d26cfd3](https://gitlab.com/hestia-earth/hestia-glossary/commit/d26cfd3e2e9011d609b62a4b3789822701482626)), closes [#1001](https://gitlab.com/hestia-earth/hestia-glossary/issues/1001)
* **crop:** delete `crop-lookup.md` file ([ea85a6b](https://gitlab.com/hestia-earth/hestia-glossary/commit/ea85a6b7ff3a9ac1246648ee7c0bd24b6210ae0c)), closes [#337](https://gitlab.com/hestia-earth/hestia-glossary/issues/337)
* **crop:** rename `Saplings` lookup `Nursery_density` and add `Saplings_required` ([5ea245c](https://gitlab.com/hestia-earth/hestia-glossary/commit/5ea245c543c2df9b7ea5e52e0281dbbb8f44962a)), closes [#1026](https://gitlab.com/hestia-earth/hestia-glossary/issues/1026)
* **crop:** rename orchard related lookups to match new glossary terms ([65dbd93](https://gitlab.com/hestia-earth/hestia-glossary/commit/65dbd93273efd09775a00c1ee2277cff922f305a)), closes [#945](https://gitlab.com/hestia-earth/hestia-glossary/issues/945) [#321](https://gitlab.com/hestia-earth/hestia-glossary/issues/321)
* **crop:** update property lookup ([4d1f1c9](https://gitlab.com/hestia-earth/hestia-glossary/commit/4d1f1c926b471ec6b41403019fd4bcee227ff8b9))
* **operation:** add `Depitting, machine unspecified` ([76c76b8](https://gitlab.com/hestia-earth/hestia-glossary/commit/76c76b8abae3e3e4df3a5ef62f928c475dd2ae6b)), closes [#1008](https://gitlab.com/hestia-earth/hestia-glossary/issues/1008)
* **operation:** add `Drying, with refractance window dryer` ([f408fef](https://gitlab.com/hestia-earth/hestia-glossary/commit/f408fef2b0ba4e376fa41f4463f09fe056724bc9)), closes [#877](https://gitlab.com/hestia-earth/hestia-glossary/issues/877)
* **operation:** add `Waste incineration, machine unspecified` ([246b56b](https://gitlab.com/hestia-earth/hestia-glossary/commit/246b56b4a5d81b25c99e9eb68076d004ca9e33d6)), closes [#1000](https://gitlab.com/hestia-earth/hestia-glossary/issues/1000)
* **operation:** add food processing operations ([881ee8b](https://gitlab.com/hestia-earth/hestia-glossary/commit/881ee8be0f27cdbfbebb4af71ba84fcb7188d106)), closes [#940](https://gitlab.com/hestia-earth/hestia-glossary/issues/940)
* **operation:** rename `Heating food, machine unspecified` `Heating (food processing), machine unspecified` ([a18a83c](https://gitlab.com/hestia-earth/hestia-glossary/commit/a18a83cd8d210bd1749c846689afaf1c78747f81)), closes [#877](https://gitlab.com/hestia-earth/hestia-glossary/issues/877) [#860](https://gitlab.com/hestia-earth/hestia-glossary/issues/860)
* **operation:** rename orchard related operations ([2b732bd](https://gitlab.com/hestia-earth/hestia-glossary/commit/2b732bd3f9e6ccb669608ced2c158de4cca9fcf0))
* **operation:** split "transporting" terms into transporting inputs and products ([71a5158](https://gitlab.com/hestia-earth/hestia-glossary/commit/71a51587936f9e60aaae9b6945065716cf12daa6)), closes [#999](https://gitlab.com/hestia-earth/hestia-glossary/issues/999)
* **property:** add `termTypesAllowed` lookup ([c460064](https://gitlab.com/hestia-earth/hestia-glossary/commit/c46006483efcb7caca105e0196bf9faea7da37ec)), closes [#729](https://gitlab.com/hestia-earth/hestia-glossary/issues/729)
* **region:** rename `Sugar` `Sugar (Unknown), Sennar, Sennar, Sudan` ([887dd95](https://gitlab.com/hestia-earth/hestia-glossary/commit/887dd9513dde454479f542240169afcfd23aeee1)), closes [#1017](https://gitlab.com/hestia-earth/hestia-glossary/issues/1017)
* **wasteManagement:** add `Recycled (waste management)` ([702973c](https://gitlab.com/hestia-earth/hestia-glossary/commit/702973c2b6e9b8b034c16e392516a06a9ee8d282)), closes [#1005](https://gitlab.com/hestia-earth/hestia-glossary/issues/1005)


### Bug Fixes

* **crop:** delete `Ratio_Abv_to_Below_Grou_crop_residue` lookup ([f4adf67](https://gitlab.com/hestia-earth/hestia-glossary/commit/f4adf675de33266509672fcb4c6d2502eb258626)), closes [#337](https://gitlab.com/hestia-earth/hestia-glossary/issues/337)
* **crop:** incorrect values for `isPlantation` lookup ([fadb0b1](https://gitlab.com/hestia-earth/hestia-glossary/commit/fadb0b196721689bb2196cc4bbf3471f482a1ca2))
* **crop:** redo the lookup `IPCC_LAND_USE_CATEGORY` ([66458fb](https://gitlab.com/hestia-earth/hestia-glossary/commit/66458fb882f4a16e755e3193bbd23fbdb8826831))
* **crop:** restrict `cropGroupingFAO` lookup values to `Temporary crops` and `Permanent crops` ([eebb80b](https://gitlab.com/hestia-earth/hestia-glossary/commit/eebb80bf26fa79b622a96724c595b0b0747ca046)), closes [#1030](https://gitlab.com/hestia-earth/hestia-glossary/issues/1030)
* **wasteManagement:** typo in `Municipal sewarage system (waste management)` ([2826c5c](https://gitlab.com/hestia-earth/hestia-glossary/commit/2826c5c71139a7383ce435fee3700990cdb6d385))

### [0.29.1](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.29.0...v0.29.1) (2023-08-10)

## [0.29.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.28.0...v0.29.0) (2023-08-10)


### ⚠ BREAKING CHANGES

* **landUseManagement:** rename `Orchard duration` `Plantation lifespan`
* **landUseManagement:** rename `Orchard bearing duration` `Plantation productive lifespan`
* **landUseManagement:** rename `Orchard replacement rate` `Plantation replacement rate`
* **landUseManagement:** rename `Orchard mortality rate` `Plantation mortality rate`
* **landUseManagement:** rename `Current orchard age` `Current plantation age`
* **landUseManagement:** rename `Orchard density` `Plantation density`
* **landUseManagement:** Moved `Long fallow crop` and `Short fallow crop` to `Property`.
* **landUseManagement:** Moved `Catch crop`, `Cover crop`, and `Ground cover` to `Property`.
* **landUseManagement:** Changed units of `Ground cover` from `% area` to `boolean`.
* **building:** rename `Tank` `Aquaculture tank`

### Features

* **building:** add `Agriculture storage tank ` ([c3c6115](https://gitlab.com/hestia-earth/hestia-glossary/commit/c3c61150ffd071c9bd60aa86fe117e745128298c)), closes [#998](https://gitlab.com/hestia-earth/hestia-glossary/issues/998)
* **building:** rename `Tank` `Aquaculture tank` ([27bbf64](https://gitlab.com/hestia-earth/hestia-glossary/commit/27bbf64593ebde84bca95be1687ace77a77ce0c5)), closes [#998](https://gitlab.com/hestia-earth/hestia-glossary/issues/998)
* **irrigation:** add `Irrigation equipment, unspecified` ([ce811e1](https://gitlab.com/hestia-earth/hestia-glossary/commit/ce811e1ae3a4517c2fa09f347e666399b344c69b))
* **landUseManagement:** add `Nursery density` ([1f4e4fa](https://gitlab.com/hestia-earth/hestia-glossary/commit/1f4e4fa9a34ffcb0ec7c0c7a8524fcf79b32ab1d)), closes [#1025](https://gitlab.com/hestia-earth/hestia-glossary/issues/1025)
* **landUseManagement:** add lookup values to new terms ([94a8733](https://gitlab.com/hestia-earth/hestia-glossary/commit/94a873381f823313bf93edeabc53011c0eb0422c))
* **landUseManagement:** add new terms for soil organic carbon stock modelling ([735c2a7](https://gitlab.com/hestia-earth/hestia-glossary/commit/735c2a71dfcfaac1fa6f31a077ac75d1c8cb2507)), closes [#992](https://gitlab.com/hestia-earth/hestia-glossary/issues/992)
* **landUseManagement:** rename "Orchard" terms ([212fb55](https://gitlab.com/hestia-earth/hestia-glossary/commit/212fb554aada4304850c458fa488d424e5a5810c)), closes [#945](https://gitlab.com/hestia-earth/hestia-glossary/issues/945)
* **operation:** add `Conching, with conche` and `Tempering, with temperer` ([2097c8a](https://gitlab.com/hestia-earth/hestia-glossary/commit/2097c8aed14e7e54545fcbaae3037bcf16da760d)), closes [#1006](https://gitlab.com/hestia-earth/hestia-glossary/issues/1006)
* **organicFertiliser:** add `Human urine (kg mass)` and `Human urine (kg N)` ([4cbfc73](https://gitlab.com/hestia-earth/hestia-glossary/commit/4cbfc73d98797c00dd6bcf0cf877db60e714ce46)), closes [#994](https://gitlab.com/hestia-earth/hestia-glossary/issues/994)
* **package:** fix `isInSystemBoundary` returning `true` for undefined values ([17afd85](https://gitlab.com/hestia-earth/hestia-glossary/commit/17afd85749a314c18c1cd2def79dbcb268c58357))
* **processingAid:** add `Carbon dioxide (processing aid)` ([bb035fe](https://gitlab.com/hestia-earth/hestia-glossary/commit/bb035fede8c85963aace7ed0100154cdb6e241a2)), closes [#1013](https://gitlab.com/hestia-earth/hestia-glossary/issues/1013)
* **processingAid:** add `Nitrogen (processing aid)` ([07f4f17](https://gitlab.com/hestia-earth/hestia-glossary/commit/07f4f174edf25cda757b81a11d85266fd852f6cf)), closes [#1016](https://gitlab.com/hestia-earth/hestia-glossary/issues/1016)
* **region:** add `Conveyancing_Efficiency_Perennial_crops` lookup ([126a85d](https://gitlab.com/hestia-earth/hestia-glossary/commit/126a85da7ea93a66d5a5aeda670daeb23a45204d))
* **system:** add `Agroecological system` ([7bd8116](https://gitlab.com/hestia-earth/hestia-glossary/commit/7bd8116e5ccdcfa0a8615cf691f77164e37dad86))
* **system:** add `Peri-urban farming system` ([1cf2b58](https://gitlab.com/hestia-earth/hestia-glossary/commit/1cf2b582f50565d64ed3651a746eaa80e0be60a7))


### Bug Fixes

* **crop:** fix lookup values for `Sugarcane` terms ([2188b7a](https://gitlab.com/hestia-earth/hestia-glossary/commit/2188b7a4614f9dd4facbc50c9620fb6b0a355380))
* **crop:** fix lookups `N_FIXING_CROP` and `LOW_RESIDUE_PRODUCING_CROP` ([62f5a58](https://gitlab.com/hestia-earth/hestia-glossary/commit/62f5a58de16fe1bf8ed87a6991797da5e9aaf18d))
* **crop:** relative hyperlink error in `description` field ([232048a](https://gitlab.com/hestia-earth/hestia-glossary/commit/232048abc088b155cf03df87845641ec564165f3))
* **landUseManagement:** fix lookup `PRACTICE_INCREASING_C_INPUT` ([9e2e6b9](https://gitlab.com/hestia-earth/hestia-glossary/commit/9e2e6b92afc7e2388674a14bbf8cc0154d0a5a98))
* **organicFertiliser:** density of `Seamac Gold (kg mass)` and `Seamac Gold (kg N)` ([90c325c](https://gitlab.com/hestia-earth/hestia-glossary/commit/90c325ce18f542ef2a5dd37c4dc37f2008f49f05))
* **waterRegime:** fix lookup `PRACTICE_INCREASING_C_INPUT` ([1e9defc](https://gitlab.com/hestia-earth/hestia-glossary/commit/1e9defce25a1e36e7b4445ff67d2f804f6d051e9))

## [0.28.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.27.0...v0.28.0) (2023-07-31)


### ⚠ BREAKING CHANGES

* **operation:** rename `Grain drying, machine unspecified` `Drying, machine unspecified`
* **operation:** rename `Grain drying, with drying fan` `Drying, with drying fan`
* **operation:** rename `Grain drying, with grain dryer` `Drying, with grain dryer`
* **operation:** rename `Grain drying, with kiln dryer` `Drying, with kiln dryer`
* **operation:** rename `Food drying, with heated cabinet` `Drying, with heated cabinet`
* **operation:** rename `Food drying, with convective dryer` `Drying, with convective dryer`
* **operation:** rename `Food drying, with vacuum dryer` `Drying, with vacuum dryer`
* **transport:** rename `Forzen freight, car` `Frozen freight, car`
* **pesticideAI:** move `Potassium metabisulphite` from `pesticideAI` to `feedFoodAdditive`
* **operation:** rename `Heating, machine unspecified` `Heating food, machine unspecified`
* **operation:** rename `Chilling food, machine unspecified` `Cooling food, machine unspecified`
* **operation:** rename `Refrigeration` `Cooling food, with refrigerator`
* **processingAid:** rename `Hydrocloric acid (processing aid)`
`Hydrochloric acid (processing aid)`

### Features

* **antibiotic:** add `Cotrimoxazole` ([2118641](https://gitlab.com/hestia-earth/hestia-glossary/commit/211864103f0c914c959ecbb6d08ff48235bd706c)), closes [#946](https://gitlab.com/hestia-earth/hestia-glossary/issues/946)
* **crop:** add `African aubergine` and `African nightshade, fruit` ([1c3b18f](https://gitlab.com/hestia-earth/hestia-glossary/commit/1c3b18ffa88dd20a4f6aa1922a2dca77550f9dda))
* **crop:** add `Coconut, oil` ([27f764c](https://gitlab.com/hestia-earth/hestia-glossary/commit/27f764c8abcbf2a8d0c069e2d0ea7368558a2e8b)), closes [#981](https://gitlab.com/hestia-earth/hestia-glossary/issues/981)
* **crop:** add `Coconut, sap` ([6453114](https://gitlab.com/hestia-earth/hestia-glossary/commit/6453114ebd9b3b67e84a0fc7152ed57ee0ba55da)), closes [#954](https://gitlab.com/hestia-earth/hestia-glossary/issues/954)
* **crop:** add `Duckweed plant`, `Duckweed, thallus`, `Aquatic crop plant`,  `Aquatic crop, leaf` ([ff4e661](https://gitlab.com/hestia-earth/hestia-glossary/commit/ff4e661a8a5a8d35511c1c0c9e93da6d9abd0560)), closes [#926](https://gitlab.com/hestia-earth/hestia-glossary/issues/926)
* **crop:** add `Oil palm, soap stock` ([0b7a748](https://gitlab.com/hestia-earth/hestia-glossary/commit/0b7a748ee6fec6a204b4913f57fbc69d80064456)), closes [#913](https://gitlab.com/hestia-earth/hestia-glossary/issues/913)
* **crop:** add `Sunflower, lecithin` ([645eeb8](https://gitlab.com/hestia-earth/hestia-glossary/commit/645eeb8f7b88643673d793d41ec13e29a93e8822)), closes [#934](https://gitlab.com/hestia-earth/hestia-glossary/issues/934)
* **emission:** add "SOx, to air" emissions ([4ffc870](https://gitlab.com/hestia-earth/hestia-glossary/commit/4ffc870534dfedb000a11062ed201375f1d1efa3)), closes [#948](https://gitlab.com/hestia-earth/hestia-glossary/issues/948)
* **emission:** add `CO2, to air, waste treatment` ([db809cc](https://gitlab.com/hestia-earth/hestia-glossary/commit/db809cc8f554b6e15e067e364bf0806d43831bc6)), closes [#951](https://gitlab.com/hestia-earth/hestia-glossary/issues/951)
* **emission:** add terms for pesticide emissions ([eac121c](https://gitlab.com/hestia-earth/hestia-glossary/commit/eac121c76040b18c59bacb748a93937879d5227a)), closes [#942](https://gitlab.com/hestia-earth/hestia-glossary/issues/942)
* **landUseManagement:** add `Plant thinning rate` ([2980a76](https://gitlab.com/hestia-earth/hestia-glossary/commit/2980a76cce3e9eec41f948a1715b60632440fb58)), closes [#873](https://gitlab.com/hestia-earth/hestia-glossary/issues/873)
* **landUseManagement:** add new term `Catch crop` to glossary ([b022056](https://gitlab.com/hestia-earth/hestia-glossary/commit/b022056de736a9d746c8ef5ca08da515bb5a8b88))
* **landUseManagement:** add term `Rotation` to delimit a rotation period ([bf00824](https://gitlab.com/hestia-earth/hestia-glossary/commit/bf00824d0c3dd361468c359cd631b401a1f2734b)), closes [#992](https://gitlab.com/hestia-earth/hestia-glossary/issues/992)
* **landUseManagement:** split `Rotation` into `Crop rotation` and `Aquaculture rotation` ([2f09f06](https://gitlab.com/hestia-earth/hestia-glossary/commit/2f09f0660b3d6c005179e6e1084b4f077f6f905b))
* **model:** add `Directive 2008/29/EC of the European parliament` ([20ad327](https://gitlab.com/hestia-earth/hestia-glossary/commit/20ad327c47a9ae1ac459e84dd84493644bf051b1)), closes [#922](https://gitlab.com/hestia-earth/hestia-glossary/issues/922)
* **model:** add `Eco-indicator 99` ([967d02b](https://gitlab.com/hestia-earth/hestia-glossary/commit/967d02bef6ce018aa844bcf960d0fdd651f157e4)), closes [#904](https://gitlab.com/hestia-earth/hestia-glossary/issues/904)
* **model:** add `PECFR (2017)` ([cc056c4](https://gitlab.com/hestia-earth/hestia-glossary/commit/cc056c4e4c4bb316853b9ac4b51b8da9431956c2)), closes [#943](https://gitlab.com/hestia-earth/hestia-glossary/issues/943)
* **operation:** add `Blanching food, machine unspecified` ([c031f70](https://gitlab.com/hestia-earth/hestia-glossary/commit/c031f7042c424e397dbfbc80552a39c6618192b7)), closes [#970](https://gitlab.com/hestia-earth/hestia-glossary/issues/970)
* **operation:** add `Heating buildings, machine unspecified` and `Freezing food, with freezer` ([50037c9](https://gitlab.com/hestia-earth/hestia-glossary/commit/50037c91a0565faae10a4f8934c0a7ed1f30b126)), closes [#959](https://gitlab.com/hestia-earth/hestia-glossary/issues/959)
* **operation:** add `Maceration`,  `Devatting `,  `Spontaneous clarification `, and  `Chemical clarification` ([f59fd49](https://gitlab.com/hestia-earth/hestia-glossary/commit/f59fd49f042b46e4db03040be8b3c608348fe4ad)), closes [#959](https://gitlab.com/hestia-earth/hestia-glossary/issues/959)
* **operation:** add `Roasting, machine unspecified` ([99eac8c](https://gitlab.com/hestia-earth/hestia-glossary/commit/99eac8ca00dc98bcbd19ddee36e17e3f3a8d6afc)), closes [#980](https://gitlab.com/hestia-earth/hestia-glossary/issues/980)
* **operation:** add `Steaming, machine unspecified` and `Quality checking, by hand` ([79ad30b](https://gitlab.com/hestia-earth/hestia-glossary/commit/79ad30b685c7afecf7b01d27e7add339c90f2fb8)), closes [#960](https://gitlab.com/hestia-earth/hestia-glossary/issues/960)
* **operation:** add Animal vaccination and terms for "Beak trimming" ([2ddb398](https://gitlab.com/hestia-earth/hestia-glossary/commit/2ddb3987f29cf3d07fa08af8cde2761afa448322)), closes [#956](https://gitlab.com/hestia-earth/hestia-glossary/issues/956)
* **operation:** make "grain drying" and "food drying" terms more general ([bfc5b4e](https://gitlab.com/hestia-earth/hestia-glossary/commit/bfc5b4e6b665718ed94fd40046be160ddb673d92)), closes [#914](https://gitlab.com/hestia-earth/hestia-glossary/issues/914) [#986](https://gitlab.com/hestia-earth/hestia-glossary/issues/986) [#982](https://gitlab.com/hestia-earth/hestia-glossary/issues/982)
* **other:** add `Detergent unspecified (AI)` ([021205a](https://gitlab.com/hestia-earth/hestia-glossary/commit/021205a4ff6e41a0f121c6be4ddfe9a5b5ed997f)), closes [#938](https://gitlab.com/hestia-earth/hestia-glossary/issues/938)
* **other:** add `Instrument air` ([c2e90de](https://gitlab.com/hestia-earth/hestia-glossary/commit/c2e90def8283527e51c4ce8572ea4093f3aa2c9f)), closes [#896](https://gitlab.com/hestia-earth/hestia-glossary/issues/896)
* **pesticideAI:** add `Acaricide unspecified (AI)` ([f2bc0e9](https://gitlab.com/hestia-earth/hestia-glossary/commit/f2bc0e93d296fb4e484cdcaa87c297596b24c3a7)), closes [#974](https://gitlab.com/hestia-earth/hestia-glossary/issues/974)
* **pesticideAI:** move `Potassium metabisulphite` to `feedFoodAdditive` ([aec2ba1](https://gitlab.com/hestia-earth/hestia-glossary/commit/aec2ba15093c51112264e73436c0337a14738227)), closes [#958](https://gitlab.com/hestia-earth/hestia-glossary/issues/958)
* **pesticideAI:** rename `Allium sativum extract` to `Garlic extract` ([327c918](https://gitlab.com/hestia-earth/hestia-glossary/commit/327c918f437770eb468988f5161e169378f1890f)), closes [#972](https://gitlab.com/hestia-earth/hestia-glossary/issues/972)
* **processedFood:** add `Peanut butter`, `Couverture chocolate`, and `Milk powder` ([b1d318a](https://gitlab.com/hestia-earth/hestia-glossary/commit/b1d318aeddc1a121451dfd0cd1a6d4177bd85ef0)), closes [#971](https://gitlab.com/hestia-earth/hestia-glossary/issues/971) [#984](https://gitlab.com/hestia-earth/hestia-glossary/issues/984)
* **processingAid:** add `Albumin` and `Perlite (processing aid)` ([4e3e4cb](https://gitlab.com/hestia-earth/hestia-glossary/commit/4e3e4cb23e0da0d282607aec0e3facb56a5aac7c)), closes [#957](https://gitlab.com/hestia-earth/hestia-glossary/issues/957)
* **processingAid:** add `Aluminium sulphate (processing aid)` ([3975bd9](https://gitlab.com/hestia-earth/hestia-glossary/commit/3975bd9593cc64681a316aa3dc48cbbfffdd5d1d)), closes [#989](https://gitlab.com/hestia-earth/hestia-glossary/issues/989)
* **processingAid:** add `Calcium carbonate (processing aid)` ([b67474c](https://gitlab.com/hestia-earth/hestia-glossary/commit/b67474c0bd63a5dc622996375bbd5484a485af34)), closes [#987](https://gitlab.com/hestia-earth/hestia-glossary/issues/987)
* **transport:** add `Freight, van`, `Chilled freight, van`, and `Frozen freight, van` ([1e652ca](https://gitlab.com/hestia-earth/hestia-glossary/commit/1e652ca285a84d4a385e365da304a152afa2372d)), closes [#929](https://gitlab.com/hestia-earth/hestia-glossary/issues/929)


### Bug Fixes

* **aquacultureManagement:** capitalisation errors in term `names` ([dce8602](https://gitlab.com/hestia-earth/hestia-glossary/commit/dce860209ec9e9eb9ac9c1bb0caa7e7b68028a0f))
* **crop:** errors in `skipAggregation` and `generateImpactAssessment` lookups ([6479660](https://gitlab.com/hestia-earth/hestia-glossary/commit/6479660a7d0aaaffb8d2ef2cb295a93dc0dbc6cb))
* **emission:** set `inHestiaDefaultSystemBoundary`  to `False` for `CH4, to air, reservoir` ([d6854f4](https://gitlab.com/hestia-earth/hestia-glossary/commit/d6854f4ae4fc831629c7d4ff854821f84d06fd23))
* **landUseManagement:** update definition of terms `Catch crop` and `Cover crop` ([217211f](https://gitlab.com/hestia-earth/hestia-glossary/commit/217211f7ac02817c56b32a43dc63608b32a2d77f))
* **processingAid:** typo in `Hydrocloric acid (processing aid)` name ([768e998](https://gitlab.com/hestia-earth/hestia-glossary/commit/768e998d55076cca1594399cd402744197fb2a49)), closes [#952](https://gitlab.com/hestia-earth/hestia-glossary/issues/952)
* **region:** sum productionQuantity for regions ([7c92d99](https://gitlab.com/hestia-earth/hestia-glossary/commit/7c92d99e69278d6b0be90c00d8cd1796c0ed9afe))
* **tillage:** duplicated lookup column headers ([8b82cbb](https://gitlab.com/hestia-earth/hestia-glossary/commit/8b82cbb2b6ca6d5805008b0d5c9da2487e3c7aab))
* **transport:** fix typo in Frozen freight, car ([8c65b60](https://gitlab.com/hestia-earth/hestia-glossary/commit/8c65b60a8452c91c89ff284a56d2bdcafd6b971c))

## [0.27.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.26.0...v0.27.0) (2023-07-18)


### ⚠ BREAKING CHANGES

* **pesticideAI:** delete `Polyacrylamide`
* **pesticideAI:** rename `Hydrochloric acid` `Hydrochloric acid (pesticide AI)`
* **pesticideAI:** rename `Sodium carbonate` `Sodium carbonate (pesticide AI)`
* **crop:** Rename `Oil palm, seed` to `Oil palm, kernel`,
`Oil palm, seed meal` to `Oil palm, kernel meal`,
`Oil palm, seed oil (crude)` to `Oil palm, kernel oil (crude)`,
`Oil palm, seed oil (refined)` to `Oil palm, kernel oil (refined)`.

### Features

* **aquacultureManagement:** add `Longline system total length` ([7d198ba](https://gitlab.com/hestia-earth/hestia-glossary/commit/7d198baa063de534102a9ab5845d2f6346d25d07)), closes [#923](https://gitlab.com/hestia-earth/hestia-glossary/issues/923)
* **crop:** rename "Palm seed..." terms to "Palm kernel..." ([3f344d2](https://gitlab.com/hestia-earth/hestia-glossary/commit/3f344d2ff536e7559af5bbfec178080e4f757c4e))
* **fuel:** add `Biomethane` ([05210e3](https://gitlab.com/hestia-earth/hestia-glossary/commit/05210e3a6802e38c2b04eef35586af8e15865d52)), closes [#918](https://gitlab.com/hestia-earth/hestia-glossary/issues/918)
* **machinery:** add `Automatic manure scraper` ([d9806e8](https://gitlab.com/hestia-earth/hestia-glossary/commit/d9806e847f93373c97f45942f300bbf82acc35fa)), closes [#915](https://gitlab.com/hestia-earth/hestia-glossary/issues/915)
* **machinery:** add `Fermentor` ([3cb76f7](https://gitlab.com/hestia-earth/hestia-glossary/commit/3cb76f79ae099eb2eadee3cc072028cab7c3ca8e)), closes [#919](https://gitlab.com/hestia-earth/hestia-glossary/issues/919)
* **material:** add `Animal bedding, unspecified` ([9701950](https://gitlab.com/hestia-earth/hestia-glossary/commit/97019504d4423714d73f47c87424e7434f62c435)), closes [#653](https://gitlab.com/hestia-earth/hestia-glossary/issues/653)
* **material:** add `Sand (material)` ([12621d7](https://gitlab.com/hestia-earth/hestia-glossary/commit/12621d7ca73a664c7b72d98d05c3e5856e43d375)), closes [#653](https://gitlab.com/hestia-earth/hestia-glossary/issues/653)
* **material:** add `Sawdust (material)` ([e6aaaff](https://gitlab.com/hestia-earth/hestia-glossary/commit/e6aaaff6d5017d555ce4621aa39db016c028b32c)), closes [#653](https://gitlab.com/hestia-earth/hestia-glossary/issues/653)
* **measurement:** set `minimum` for temperature terms to `-60` ([b0cc6c8](https://gitlab.com/hestia-earth/hestia-glossary/commit/b0cc6c8b23b9261c1bba136e238ad5fe329482b0))
* **operation:** `Tartaric stabilisation` ([cb0ede9](https://gitlab.com/hestia-earth/hestia-glossary/commit/cb0ede9f93b0fc717d600b233807ecf1e93e2af0)), closes [#920](https://gitlab.com/hestia-earth/hestia-glossary/issues/920)
* **operation:** add `Heating, machine unspecified` ([eda2a06](https://gitlab.com/hestia-earth/hestia-glossary/commit/eda2a0602f530cd9c11417752a52427fe0616788)), closes [#920](https://gitlab.com/hestia-earth/hestia-glossary/issues/920)
* **operation:** add `Training plants to a trellis` ([17c0601](https://gitlab.com/hestia-earth/hestia-glossary/commit/17c0601a6bfd5e88636f0e3280e3fb29981e8074)), closes [#920](https://gitlab.com/hestia-earth/hestia-glossary/issues/920)
* **pesticideAI:** delete `polyacrylamide` and rename HCl and Na2CO3 ([1b551f2](https://gitlab.com/hestia-earth/hestia-glossary/commit/1b551f28530df8ac651f0b0a0a5597c243a46df6)), closes [#872](https://gitlab.com/hestia-earth/hestia-glossary/issues/872)
* **processedFood:** add `Cocoa, liquor`, `Cocoa, butter`, `Cocoa, meal`, and `Cocoa, powder` ([5e22ad9](https://gitlab.com/hestia-earth/hestia-glossary/commit/5e22ad92ca33496e6d6be526a981f61c476ecb47)), closes [#911](https://gitlab.com/hestia-earth/hestia-glossary/issues/911)
* **processingAid:** create glossary and add the first terms ([5236d5f](https://gitlab.com/hestia-earth/hestia-glossary/commit/5236d5fea5a622615641e664e3ae0e6748cc659e)), closes [#851](https://gitlab.com/hestia-earth/hestia-glossary/issues/851) [#870](https://gitlab.com/hestia-earth/hestia-glossary/issues/870) [#863](https://gitlab.com/hestia-earth/hestia-glossary/issues/863) [#858](https://gitlab.com/hestia-earth/hestia-glossary/issues/858)
* **waste:** add `Food processing waste` ([59d330a](https://gitlab.com/hestia-earth/hestia-glossary/commit/59d330a6ddc18df0aaafd476c0324d95dd278204)), closes [#921](https://gitlab.com/hestia-earth/hestia-glossary/issues/921)
* **waste:** add `Oil waste` ([8339761](https://gitlab.com/hestia-earth/hestia-glossary/commit/8339761a7440c3098c7e831dbf4ec36b086d7cfc)), closes [#921](https://gitlab.com/hestia-earth/hestia-glossary/issues/921)


### Bug Fixes

* **processingAid:** remove space in `casNumber` ([3488268](https://gitlab.com/hestia-earth/hestia-glossary/commit/34882685bebf98b74d614f14678b704ab3f4cafe))

## [0.26.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.25.0...v0.26.0) (2023-07-12)


### ⚠ BREAKING CHANGES

* **processedFood:** delete `Agri-food processing waste`

### Features

* **animalManagement:** add `Empty animal housing period` ([797d125](https://gitlab.com/hestia-earth/hestia-glossary/commit/797d125aab61801bb34e17c6faf59dd044e5383e)), closes [#852](https://gitlab.com/hestia-earth/hestia-glossary/issues/852)
* **animalProduct:** add `Meat, chicken, breast` ([15f015b](https://gitlab.com/hestia-earth/hestia-glossary/commit/15f015b930150e0780bfcf87764680cfb3f0d433)), closes [#853](https://gitlab.com/hestia-earth/hestia-glossary/issues/853)
* **animalProduct:** add `Meat, chicken, leg` ([72c62aa](https://gitlab.com/hestia-earth/hestia-glossary/commit/72c62aa37809e73f07e0be9cffda7a1986602a3c)), closes [#853](https://gitlab.com/hestia-earth/hestia-glossary/issues/853)
* **animalProduct:** add `Meat, chicken, thigh` ([7911db5](https://gitlab.com/hestia-earth/hestia-glossary/commit/7911db53218a3d5de12bb78787ec4d186978857d)), closes [#853](https://gitlab.com/hestia-earth/hestia-glossary/issues/853)
* **animalProduct:** add `Meat, chicken, wings` ([2e10202](https://gitlab.com/hestia-earth/hestia-glossary/commit/2e10202c4d1f424b1ae38a8efd2146a30fc9e8e8)), closes [#853](https://gitlab.com/hestia-earth/hestia-glossary/issues/853)
* **aquacultureManagement:** add `Submersion seeding` and `Spray seeding` ([0c40ca4](https://gitlab.com/hestia-earth/hestia-glossary/commit/0c40ca4c18a0c379f0c33dc8e0a9d89c92e449ab)), closes [#831](https://gitlab.com/hestia-earth/hestia-glossary/issues/831)
* **aquacultureManagement:** add terms to record yield per meter of line ([779d37f](https://gitlab.com/hestia-earth/hestia-glossary/commit/779d37f61d09c32f1c45d0bc433b7913fd7d6b43)), closes [#867](https://gitlab.com/hestia-earth/hestia-glossary/issues/867)
* **characterisedIndicator:** add `Ionising radiation` ([2be04e7](https://gitlab.com/hestia-earth/hestia-glossary/commit/2be04e7c0352ce22bfb9b0b229389e7a39c378ae)), closes [#765](https://gitlab.com/hestia-earth/hestia-glossary/issues/765)
* **crop:** add `Atlantic wakame alga` and `Atlantic wakame, leaf` ([a40240b](https://gitlab.com/hestia-earth/hestia-glossary/commit/a40240b33e92ecfeb8405fc064c4733928d70e7d)), closes [#866](https://gitlab.com/hestia-earth/hestia-glossary/issues/866)
* **crop:** add `Nitrogen uptake (above and below ground crop residue)` ([375d340](https://gitlab.com/hestia-earth/hestia-glossary/commit/375d3402d4e1b752719a2e94d58c0d29110cdf31)), closes [#859](https://gitlab.com/hestia-earth/hestia-glossary/issues/859)
* **crop:** add `Red sea plume alga` and `Red sea plume, leaf` ([d5de77f](https://gitlab.com/hestia-earth/hestia-glossary/commit/d5de77fac47006f5f05e8c4a51bf82fbb09d92d2)), closes [#881](https://gitlab.com/hestia-earth/hestia-glossary/issues/881)
* **crop:** add crop residue lookup values for Flax and Camelina ([917d27f](https://gitlab.com/hestia-earth/hestia-glossary/commit/917d27fa37dea45f327dfb633f44a69c47ba44d9)), closes [#899](https://gitlab.com/hestia-earth/hestia-glossary/issues/899)
* **crop:** add lookup `LOW_RESIDUE_PRODUCING_CROP` to crop glossary ([9c02420](https://gitlab.com/hestia-earth/hestia-glossary/commit/9c02420b9070625a00fcefcf48a349ab3fd8f42e))
* **crop:** add lookup `N_FIXING_CROP` to state which annual crops are N fixing ([98e7093](https://gitlab.com/hestia-earth/hestia-glossary/commit/98e70935651dc3ce228a69233f3f68831290bc05))
* **crop:** add rooting depth for Camelina and Flax ([0019210](https://gitlab.com/hestia-earth/hestia-glossary/commit/0019210e31ec289f4b9e448bbc82811b3eafecc4)), closes [#883](https://gitlab.com/hestia-earth/hestia-glossary/issues/883)
* **emission:** add `CH4, to air, reservoirs` ([e66d7a8](https://gitlab.com/hestia-earth/hestia-glossary/commit/e66d7a84e9da8b2c34c643f8b96480a92eb5d668))
* **feedFoodAdditive:** add `Sodium alginate` ([9e2d74c](https://gitlab.com/hestia-earth/hestia-glossary/commit/9e2d74c127b23e1c32c6f867941786780f3035cb)), closes [#888](https://gitlab.com/hestia-earth/hestia-glossary/issues/888)
* **forage:** add lookup `N_FIXING_CROP` to state which forage species are N fixing ([f1c2b4b](https://gitlab.com/hestia-earth/hestia-glossary/commit/f1c2b4bc15d024db9b7132388fa395b60c12d4d2))
* **fuel:** add `Marine diesel oil` and `Marine gas oil` ([7a4b9b5](https://gitlab.com/hestia-earth/hestia-glossary/commit/7a4b9b505c7100ae054fe18ee84b2b2c30594981)), closes [#865](https://gitlab.com/hestia-earth/hestia-glossary/issues/865)
* **measurement:** add `Darkness duration, indoor (daily)` ([2e7fb14](https://gitlab.com/hestia-earth/hestia-glossary/commit/2e7fb14b6a4c3058752e616d5b182f866e35b75b)), closes [#852](https://gitlab.com/hestia-earth/hestia-glossary/issues/852)
* **measurement:** add `Temperature, indoor (period)` and `Humidity, indoor (period)` ([85f2a1b](https://gitlab.com/hestia-earth/hestia-glossary/commit/85f2a1b11520f2eb965b81d4a6e29f1cdf6c9d7e)), closes [#822](https://gitlab.com/hestia-earth/hestia-glossary/issues/822)
* **model:** add `US EPA (1993)` ([7ffa5ad](https://gitlab.com/hestia-earth/hestia-glossary/commit/7ffa5adaf2583ec75ae6e22e30da1965b4b3c312)), closes [#809](https://gitlab.com/hestia-earth/hestia-glossary/issues/809)
* **model:** add `US EPA (1995)` ([c111fbb](https://gitlab.com/hestia-earth/hestia-glossary/commit/c111fbbcab87c1813c7073559d3aabfd8544bb1c)), closes [#809](https://gitlab.com/hestia-earth/hestia-glossary/issues/809)
* **operation:** add `Butchering` ([9e2efe0](https://gitlab.com/hestia-earth/hestia-glossary/commit/9e2efe03ad0c16fcd07cfa8bdda4bafdd5202996)), closes [#854](https://gitlab.com/hestia-earth/hestia-glossary/issues/854)
* **operation:** add `Centrifugation, machine unspecified` ([ebc3bbc](https://gitlab.com/hestia-earth/hestia-glossary/commit/ebc3bbc07c40a99d7ab535740cb2fd262c723731)), closes [#892](https://gitlab.com/hestia-earth/hestia-glossary/issues/892)
* **operation:** add `Decantation` ([637b10a](https://gitlab.com/hestia-earth/hestia-glossary/commit/637b10ab90034872fdd08423692beea06fdefa15)), closes [#891](https://gitlab.com/hestia-earth/hestia-glossary/issues/891)
* **operation:** add `Desanding, with desander` ([56e1c1b](https://gitlab.com/hestia-earth/hestia-glossary/commit/56e1c1b84182cc89060994e2cd1cc867cdd4a9d7)), closes [#893](https://gitlab.com/hestia-earth/hestia-glossary/issues/893)
* **operation:** add `Food drying, with vacuum dryer` ([4b2cddb](https://gitlab.com/hestia-earth/hestia-glossary/commit/4b2cddb159f3862c414df2ab20e8f29f434c6d52)), closes [#894](https://gitlab.com/hestia-earth/hestia-glossary/issues/894)
* **operation:** add `Fuel polishing, machine unspecified` ([1930de7](https://gitlab.com/hestia-earth/hestia-glossary/commit/1930de72cc5e3b60d2bf56a824a3e1ffc848f365)), closes [#895](https://gitlab.com/hestia-earth/hestia-glossary/issues/895)
* **operation:** add `Livestock enrichment` ([3a5f4b5](https://gitlab.com/hestia-earth/hestia-glossary/commit/3a5f4b57f1968b31252e1c5e4dff88dda578b9ac)), closes [#854](https://gitlab.com/hestia-earth/hestia-glossary/issues/854)
* **operation:** add `Mixing, machine unspecified` ([8d9143a](https://gitlab.com/hestia-earth/hestia-glossary/commit/8d9143a104c76005f214664d95c6acb0afd510b6)), closes [#854](https://gitlab.com/hestia-earth/hestia-glossary/issues/854)
* **operation:** add `Plucking, by hand` ([0278b44](https://gitlab.com/hestia-earth/hestia-glossary/commit/0278b444b64d7ab16b1dfc3bd8be6a5954196a7d)), closes [#854](https://gitlab.com/hestia-earth/hestia-glossary/issues/854)
* **operation:** add `Plucking, machine unspecified` ([f3b6cfb](https://gitlab.com/hestia-earth/hestia-glossary/commit/f3b6cfbbbf7e08600a30942c73ac0e903c22afc1)), closes [#854](https://gitlab.com/hestia-earth/hestia-glossary/issues/854)
* **operation:** add `Slaughtering` ([49c1658](https://gitlab.com/hestia-earth/hestia-glossary/commit/49c16587ad10ed5e5a70eac65c5a6bc46d29dc8e)), closes [#854](https://gitlab.com/hestia-earth/hestia-glossary/issues/854)
* **operation:** add `Surface disinfection, machine unspecified` and `Surface disinfection, by hand` ([bfafecd](https://gitlab.com/hestia-earth/hestia-glossary/commit/bfafecd0c3dc45d4ab4b021b9cfef0af0c43f764))
* **operation:** add fuel purification terms ([f57b6c9](https://gitlab.com/hestia-earth/hestia-glossary/commit/f57b6c96ca6709c95b425c0411a62d3470a8a9bd)), closes [#886](https://gitlab.com/hestia-earth/hestia-glossary/issues/886)
* **operation:** add more lighting terms ([d2115d5](https://gitlab.com/hestia-earth/hestia-glossary/commit/d2115d526d849d269382d910dc24f909ffefba37)), closes [#869](https://gitlab.com/hestia-earth/hestia-glossary/issues/869)
* **operation:** add terms for humidity regulation ([b010ce6](https://gitlab.com/hestia-earth/hestia-glossary/commit/b010ce6a4097f1bcf43a7c086794f8216c233179)), closes [#862](https://gitlab.com/hestia-earth/hestia-glossary/issues/862)
* **organicFertiliser:** add `Organic Magnesium fertiliser, unspecified (kg Mg)` ([6c6d3bb](https://gitlab.com/hestia-earth/hestia-glossary/commit/6c6d3bbc310bd2426f1b06f8f97ec35ebe5073a2)), closes [#903](https://gitlab.com/hestia-earth/hestia-glossary/issues/903)
* **organicFertiliser:** add lookup `ANIMAL_MANURE` to state which fertiliser is manure ([2f869e8](https://gitlab.com/hestia-earth/hestia-glossary/commit/2f869e8a2c137363cd89c52261821fadd01197fd))
* **organicFertiliser:** add lookup `PRACTICE_INCREASING_C_INPUT` to organic fertilisers ([b59c92d](https://gitlab.com/hestia-earth/hestia-glossary/commit/b59c92d4757151c2197da12b524c7d20ffd0d8af))
* **processedFood:** add `White chocolate`, `Dark chocolate`, and `Milk chocolate` ([2adff0c](https://gitlab.com/hestia-earth/hestia-glossary/commit/2adff0c3c3facd261702202126e84f79ec4091a7)), closes [#906](https://gitlab.com/hestia-earth/hestia-glossary/issues/906)
* **processedFood:** delete `Agri-food processing waste` ([87c72c8](https://gitlab.com/hestia-earth/hestia-glossary/commit/87c72c80d7984624e821cdb2d0c7f82ac75a1bf2)), closes [#879](https://gitlab.com/hestia-earth/hestia-glossary/issues/879)
* **property:** add `Fluoride content` and `Iodine content` ([0cb53c0](https://gitlab.com/hestia-earth/hestia-glossary/commit/0cb53c003b1144ef2ea49304628851b0bcfbd886)), closes [#908](https://gitlab.com/hestia-earth/hestia-glossary/issues/908)
* **property:** add `Mercury content`, `Arsenic content`, and `Cobalt content` ([47f23d5](https://gitlab.com/hestia-earth/hestia-glossary/commit/47f23d5a0516fbedd48eb9281d33d07011d403ce)), closes [#855](https://gitlab.com/hestia-earth/hestia-glossary/issues/855)
* **property:** add `Methane content` and `Ammonium content` ([2186685](https://gitlab.com/hestia-earth/hestia-glossary/commit/218668547cfe959c30c690a06a401a137d3fa2f5)), closes [#884](https://gitlab.com/hestia-earth/hestia-glossary/issues/884)
* **property:** add `Purity` ([24a16a9](https://gitlab.com/hestia-earth/hestia-glossary/commit/24a16a98a178b9ad5d71a06928a405e120b60fb1)), closes [#889](https://gitlab.com/hestia-earth/hestia-glossary/issues/889)
* **property:** add `Temperature` and `Salinity` ([ec0b006](https://gitlab.com/hestia-earth/hestia-glossary/commit/ec0b006613192db57ec25807504ee96fc4293000)), closes [#882](https://gitlab.com/hestia-earth/hestia-glossary/issues/882)
* **soilAmendment:** add lookup `PRACTICE_INCREASING_C_INPUT` to soil amendments ([4012f52](https://gitlab.com/hestia-earth/hestia-glossary/commit/4012f5239602d6d720a4287eefbe3faf39216d5f))
* **soilTexture:** add `defaultValue` lookup ([ac3884b](https://gitlab.com/hestia-earth/hestia-glossary/commit/ac3884b25f422d9393bf0e343b5aa447f2314c4a)), closes [#875](https://gitlab.com/hestia-earth/hestia-glossary/issues/875)
* **soilType:** add `defaultValue` lookup ([64f86b0](https://gitlab.com/hestia-earth/hestia-glossary/commit/64f86b0be59b8e05e83e2490e49c20ea8d52eefe)), closes [#875](https://gitlab.com/hestia-earth/hestia-glossary/issues/875)
* **usdaSoilType:** add `defaultValue` lookup ([a0260c0](https://gitlab.com/hestia-earth/hestia-glossary/commit/a0260c081aad6768829ca045d7e02d5063a6e644)), closes [#875](https://gitlab.com/hestia-earth/hestia-glossary/issues/875)
* **waste:** add `Aquaculture sludge` ([0f77882](https://gitlab.com/hestia-earth/hestia-glossary/commit/0f778828a8747ff805112d741b5ab941783a1902)), closes [#901](https://gitlab.com/hestia-earth/hestia-glossary/issues/901)
* **wasteManagement:** add anaerobic and aerobic digester terms ([11bc9f9](https://gitlab.com/hestia-earth/hestia-glossary/commit/11bc9f9aa87aeede30e55ad6bea6f7e61d2285b3)), closes [#868](https://gitlab.com/hestia-earth/hestia-glossary/issues/868)
* **wasteManagement:** add terms for `Liquid/slurry` and `Lagoon` ([79ff69b](https://gitlab.com/hestia-earth/hestia-glossary/commit/79ff69bdadea7b6132062bf4089b984a40d61413))


### Bug Fixes

* **characterisedIndicator:** add back missing values to model mapping file ([6a2721c](https://gitlab.com/hestia-earth/hestia-glossary/commit/6a2721cd206b490a0e17d13184523d41cfdd62c3))
* **crop:** fix lookup `IPCC_LAND_USE_CATEGORY` for miscategorised crop categories ([7768833](https://gitlab.com/hestia-earth/hestia-glossary/commit/7768833d6ad32c5ffceae9dd8ba6eeb84d64746a))
* **excretaManagement:** errors in definitions for `Liquid/slurry` terms ([569cf01](https://gitlab.com/hestia-earth/hestia-glossary/commit/569cf0140e3ecdef315c35c804b734f3aff750f9))
* **organicFertiliser:** fix typo in `Tung pumace`to `Tung pomace` ([00f301d](https://gitlab.com/hestia-earth/hestia-glossary/commit/00f301d4a7aa6ff85825a30e1363ef353198c081))
* **waterRegime:** replace `-` with `False` in `percentAreaSumIs100` lookup ([42462b8](https://gitlab.com/hestia-earth/hestia-glossary/commit/42462b8cfb1863e6e03ab92f04b90e13757b5f30)), closes [#902](https://gitlab.com/hestia-earth/hestia-glossary/issues/902)

## [0.25.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.24.0...v0.25.0) (2023-07-03)


### ⚠ BREAKING CHANGES

* **operation:** rename `Cooling, with air conditioning` `Cooling buildings, with air conditioning`
* **operation:** rename `Cooling, with evaporative cooling tower`, `Cooling buildings, with evaporative cooling tower`
* **operation:** `Cooling, machine unspecified` `Cooling buildings, machine unspecified`
* **pesticideAI:** rename `Vanillin` `Vanillin (pesticide AI)`
* **feedFoodAdditive:** move `Gelatin` from `feedFoodAdditive` to `processedFood`
* **crop:** move `Concentrate feed, unspecified` from `crop` to `processedFood`
* **crop:** move `Concentrate feed blend` from `crop` to `processedFood`
* **forage:** move `Common vetch plant` from `forage` to `crop`
* **forage:** move `Common vetch, fresh forage` from `forage` to `crop`
* **forage:** move `Common vetch, hay` from `forage` to `crop`
* **forage:** move `Common vetch, seed` from `forage` to `crop`
* **forage:** move `Common vetch, haulm` from `forage` to `crop`
* **forage:** rename `Common vetch, haulm` `Common vetch, straw`
* **crop:** rename `Cherry tree` `Sweet cherry tree` and `Sour cherry tree`
* **crop:** rename `Cherry, fruit` `Sweet cherry, fruit` and `Sour cherry, fruit`
* **crop:** rename `Oilcrop, seed/fruit` `Oil crop, seed/fruit`
* **crop:** rename `Oilcrop, straw` `Oil crop, straw`
* **crop:** rename `Fruit` `Fruit crop, fruit`
* **crop:** rename `Vegetable` `Vegetable crop, leaf`
* **crop:** rename `Root/tuber` `Root or tuber crop, root`
* **emission:** Rename lookup `pm2.5EqEgalitarianFineParticulateMatterFormationReCiPe2016`
`pm2-5EqEgalitarianFineParticulateMatterFormationReCiPe2016`.
* **emission:** Rename lookup `pm2.5EqHierarchistFineParticulateMatterFormationReCiPe2016`
`pm2-5EqHierarchistFineParticulateMatterFormationReCiPe2016`.
* **emission:** Rename lookup `pm2.5EqIndividualistFineParticulateMatterFormationReCiPe2016`
`pm2-5EqIndividualistFineParticulateMatterFormationReCiPe2016`.
* **pesticideBrandName:** rename `EPA Product Status` lookup `epaProductStatus`
* **forage:** rename `Grass silage, baled` and Grass silage, wrapped` `Generic grass, silage`
* **cropEstablishment:** rename `Planting density` `Plant density`
* **cropEstablishment:** delete `Seeding density`
* **excreta:** delete `Excreta, sheep and goats (kg mass)`
* **excreta:** delete `Excreta, sheep and goats (kg N)`
* **excreta:** delete `Excreta, sheep and goats (kg VS)`

### Features

* add `defaultValue` lookup to Practice glossaries ([ec344ee](https://gitlab.com/hestia-earth/hestia-glossary/commit/ec344ee1f3071c1e533fec468319b662f2c0d3d5)), closes [#517](https://gitlab.com/hestia-earth/hestia-glossary/issues/517)
* **animalManagement:** Add `Number of egg turns` ([c293f93](https://gitlab.com/hestia-earth/hestia-glossary/commit/c293f93b63c76eb5f7b43f082fad7a9571284790)), closes [#836](https://gitlab.com/hestia-earth/hestia-glossary/issues/836)
* **animalProduct:** add `Eggs, domestic goose, shell`, `Eggs, domestic duck, shell`, `Eggs, other bird, shell` ([9c62648](https://gitlab.com/hestia-earth/hestia-glossary/commit/9c62648bb0f6194eb217202977a48ba5f39cebd4)), closes [#842](https://gitlab.com/hestia-earth/hestia-glossary/issues/842)
* **crop:** add `Cassava, peel (fresh)` and `Cassava, peel (dried)` ([fdddbb6](https://gitlab.com/hestia-earth/hestia-glossary/commit/fdddbb6c401f3a079459949ccbaf1a81842682cf)), closes [#811](https://gitlab.com/hestia-earth/hestia-glossary/issues/811)
* **crop:** add `Common reed plant`, `Common reed, stalk`, and `Common reed, shoot` ([ae7b42a](https://gitlab.com/hestia-earth/hestia-glossary/commit/ae7b42abdd8707306b26ff861dade19696c4756e)), closes [#728](https://gitlab.com/hestia-earth/hestia-glossary/issues/728)
* **crop:** add `Oil palm, seed oil (refined)` ([3122362](https://gitlab.com/hestia-earth/hestia-glossary/commit/31223621912e6282267d44d7efc73c84bc8bff74)), closes [#835](https://gitlab.com/hestia-earth/hestia-glossary/issues/835)
* **crop:** add `Oil palm, wood`, `Oil palm, shell` and synonym for `Oil palm, press fibre` ([ed6a4fe](https://gitlab.com/hestia-earth/hestia-glossary/commit/ed6a4fec90294266c0f2bc1328cf8c3391775049)), closes [#812](https://gitlab.com/hestia-earth/hestia-glossary/issues/812) [#814](https://gitlab.com/hestia-earth/hestia-glossary/issues/814) [#829](https://gitlab.com/hestia-earth/hestia-glossary/issues/829)
* **crop:** add `Wheat, feed grain` and `Wheat milling grain` ([1575614](https://gitlab.com/hestia-earth/hestia-glossary/commit/1575614ca623465e9903a3db6b48ad86f15dfbea)), closes [#823](https://gitlab.com/hestia-earth/hestia-glossary/issues/823)
* **crop:** add baby leaves, cut flowers, fresh pulses and other terms ([a5abf3e](https://gitlab.com/hestia-earth/hestia-glossary/commit/a5abf3e48abd70eedd6cf873c4fd482052c2bbd0)), closes [#518](https://gitlab.com/hestia-earth/hestia-glossary/issues/518)
* **crop:** add missing FAOstat crops ([c541c5b](https://gitlab.com/hestia-earth/hestia-glossary/commit/c541c5bc4cedf8e82d257680aef2c7581603a3f9)), closes [#579](https://gitlab.com/hestia-earth/hestia-glossary/issues/579)
* **crop:** add more generic crop terms and re-organise generic crop section ([ac141d2](https://gitlab.com/hestia-earth/hestia-glossary/commit/ac141d2c1c96200ee967f4182ab65b02a35a8b93)), closes [#779](https://gitlab.com/hestia-earth/hestia-glossary/issues/779)
* **crop:** add multiple crops ([25de8a2](https://gitlab.com/hestia-earth/hestia-glossary/commit/25de8a2367ae0628aba638077c8633f219569f45)), closes [#779](https://gitlab.com/hestia-earth/hestia-glossary/issues/779)
* **crop:** add phosphorus uptake terms ([31b4af5](https://gitlab.com/hestia-earth/hestia-glossary/commit/31b4af5326d566239443d017d3160d1cd824bb36)), closes [#804](https://gitlab.com/hestia-earth/hestia-glossary/issues/804)
* **crop:** create overall groupings using the `subClassOf` fields ([723bc63](https://gitlab.com/hestia-earth/hestia-glossary/commit/723bc63c9a90a9400c39c951fb0c02a891d2795e)), closes [#501](https://gitlab.com/hestia-earth/hestia-glossary/issues/501)
* **cropEstablishment:** delete `Seeding density` ([771ba14](https://gitlab.com/hestia-earth/hestia-glossary/commit/771ba148ca8ce9e34fb17a6df52a321df2ba1632)), closes [#609](https://gitlab.com/hestia-earth/hestia-glossary/issues/609)
* **cropEstablishment:** rename `Planting density` `Plant density` ([c6fba89](https://gitlab.com/hestia-earth/hestia-glossary/commit/c6fba8953912420b91717b4bf83b5cf51e428af9)), closes [#609](https://gitlab.com/hestia-earth/hestia-glossary/issues/609)
* **crop:** move concentrate feed terms to `processedFood` ([b8477cb](https://gitlab.com/hestia-earth/hestia-glossary/commit/b8477cb2ff50daec3f1a888f3419e39b340a7494))
* **crop:** rename `Oil palm, seed oil` `Oil palm, seed oil (crude)` ([b4a770e](https://gitlab.com/hestia-earth/hestia-glossary/commit/b4a770e67885d18b1c5aa95b6ae2fe29c4a91281)), closes [#835](https://gitlab.com/hestia-earth/hestia-glossary/issues/835)
* **crop:** split `Cherry` terms into `Sweet cherry` and `Sour cherry` ([8d3e21b](https://gitlab.com/hestia-earth/hestia-glossary/commit/8d3e21b57fb1c12f61f0f82c2e881c02ab204579)), closes [#579](https://gitlab.com/hestia-earth/hestia-glossary/issues/579)
* **emission:** add term `PO43-, to groundwater, inputs production` ([38077bc](https://gitlab.com/hestia-earth/hestia-glossary/commit/38077bcf2267d505879cbb461c064e0549f282ce)), closes [#807](https://gitlab.com/hestia-earth/hestia-glossary/issues/807)
* **excreta:** add more specific terms for goats and sheep manure ([5d5206c](https://gitlab.com/hestia-earth/hestia-glossary/commit/5d5206cd406553623cef7e67ecbc5c662e04bd05))
* **excreta:** delete generic `Excreta, sheep and goats` terms ([c56602e](https://gitlab.com/hestia-earth/hestia-glossary/commit/c56602ef4899fbfdb39d4d9ae0b5b04b51bab41b))
* **feedFoodAdditive:** add `Argon` ([73fed54](https://gitlab.com/hestia-earth/hestia-glossary/commit/73fed54b350d2387c7137dbcb536c4774e068f34))
* **feedFoodAdditive:** add `Bentonite` ([406be66](https://gitlab.com/hestia-earth/hestia-glossary/commit/406be66037ee3cac5c3bfd1639107c48a617ed56))
* **feedFoodAdditive:** add `Calcium soap, unspecified` ([5b1bd27](https://gitlab.com/hestia-earth/hestia-glossary/commit/5b1bd277ba93367af77261f36d19bb92af6a36be)), closes [#798](https://gitlab.com/hestia-earth/hestia-glossary/issues/798)
* **feedFoodAdditive:** add `Natural vanilla extract` and `Vanillin (feed food additive)` ([9fe8284](https://gitlab.com/hestia-earth/hestia-glossary/commit/9fe8284ac5dc612024868adf8293466e46deb0f9)), closes [#837](https://gitlab.com/hestia-earth/hestia-glossary/issues/837)
* **feedFoodAdditive:** add `Propylene Glycol` and `Gelatin` and tidy other terms ([f9cc6ae](https://gitlab.com/hestia-earth/hestia-glossary/commit/f9cc6aeb6a99e4e040f917cab4c4f62a7a28a343))
* **feedFoodAdditive:** add `Sulphur dioxide (food additive)` ([9eeaefe](https://gitlab.com/hestia-earth/hestia-glossary/commit/9eeaefe74dc56a97db127b29b526cc55887d8fe3))
* **feedFoodAdditive:** add `Urea (feed food additive)` ([ed88b45](https://gitlab.com/hestia-earth/hestia-glossary/commit/ed88b45c08e136b2769f8eb2354a130c7c55a283)), closes [#825](https://gitlab.com/hestia-earth/hestia-glossary/issues/825)
* **feedFoodAdditive:** move `Gelatin` to `processedFood` ([96c4763](https://gitlab.com/hestia-earth/hestia-glossary/commit/96c47637d1d3799762c981630e0f83caab6a2684)), closes [#810](https://gitlab.com/hestia-earth/hestia-glossary/issues/810)
* **feedFoodAdditive:** move terms from `pesticideAI` ([5e29cee](https://gitlab.com/hestia-earth/hestia-glossary/commit/5e29cee91665526e30f295e928a78fed03fbdaae)), closes [#817](https://gitlab.com/hestia-earth/hestia-glossary/issues/817) [#820](https://gitlab.com/hestia-earth/hestia-glossary/issues/820) [#815](https://gitlab.com/hestia-earth/hestia-glossary/issues/815)
* **forage:** add fresh forage, hay and silage terms for grass-alfalfa and grass-clover mixtures ([3ddb1e5](https://gitlab.com/hestia-earth/hestia-glossary/commit/3ddb1e54a9067d3024d4c927f6149195da313603)), closes [#737](https://gitlab.com/hestia-earth/hestia-glossary/issues/737)
* **forage:** move Common vetch terms to `crop` ([f57b053](https://gitlab.com/hestia-earth/hestia-glossary/commit/f57b053949f7aec575c33f31a61d71e21df95012)), closes [#579](https://gitlab.com/hestia-earth/hestia-glossary/issues/579)
* **forage:** replace `Grass silage, baled` and `Grass silage, wrapped` with `Generic grass, silage` ([0ac1f28](https://gitlab.com/hestia-earth/hestia-glossary/commit/0ac1f28e4ca1a42149752756e72dd68946463745)), closes [#737](https://gitlab.com/hestia-earth/hestia-glossary/issues/737)
* **landUseManagement:** add "plantation" as `synonym` for "orchard" ([450bc40](https://gitlab.com/hestia-earth/hestia-glossary/commit/450bc40436e7a8f1841b2ee8f8df8c7b60563b62)), closes [#805](https://gitlab.com/hestia-earth/hestia-glossary/issues/805)
* **landUseManagement:** add `Number of prunings or cuttings` ([192e7ef](https://gitlab.com/hestia-earth/hestia-glossary/commit/192e7efb8cf222e74a06ac8329ac80b565c3277a)), closes [#826](https://gitlab.com/hestia-earth/hestia-glossary/issues/826)
* **liveAquaticSpecies:** add multiple carp species ([78072ea](https://gitlab.com/hestia-earth/hestia-glossary/commit/78072ea14cccdf0fd249ee943dec052863a3882c)), closes [#796](https://gitlab.com/hestia-earth/hestia-glossary/issues/796)
* **machinery:** add `Machinery, unspecified` and `Industrial machinery, unspecified` ([be51e73](https://gitlab.com/hestia-earth/hestia-glossary/commit/be51e739941c8ab9511d50df78fb242e28d6e6ec)), closes [#848](https://gitlab.com/hestia-earth/hestia-glossary/issues/848)
* **machinery:** add `Self-propelled sprayer`, `Trailed sprayer`, `Ridger`, and `Disc harrow` ([8efb36d](https://gitlab.com/hestia-earth/hestia-glossary/commit/8efb36d96ceac2cb48f560d9cc034b8715daeef1))
* **machinery:** add machineries used in algaculture ([35173fe](https://gitlab.com/hestia-earth/hestia-glossary/commit/35173fe959f6dbb3f1285803f3500c45f4ca9d46)), closes [#808](https://gitlab.com/hestia-earth/hestia-glossary/issues/808) [#831](https://gitlab.com/hestia-earth/hestia-glossary/issues/831)
* **material:** add `Synthetic rubber` and `Synthetic rubber, depreciated amount per Cycle` ([0054b77](https://gitlab.com/hestia-earth/hestia-glossary/commit/0054b77c3f2904586649e00776a60332d2acef74)), closes [#818](https://gitlab.com/hestia-earth/hestia-glossary/issues/818)
* **methodMeasurement:** add `Diethyl ether extraction method` ([17991c7](https://gitlab.com/hestia-earth/hestia-glossary/commit/17991c7dbb73ec218ab8ce81af008a3317da2a91)), closes [#785](https://gitlab.com/hestia-earth/hestia-glossary/issues/785)
* **operation:** add `Fertilising, with dribble bar` ([a0c5019](https://gitlab.com/hestia-earth/hestia-glossary/commit/a0c50193408389622f487be6d7c87048f9f918e0)), closes [#824](https://gitlab.com/hestia-earth/hestia-glossary/issues/824)
* **operation:** add `Fertilising, with precision applicator` ([04df65e](https://gitlab.com/hestia-earth/hestia-glossary/commit/04df65eeac96a1324c2ecf6936c01a58c38ae487)), closes [#824](https://gitlab.com/hestia-earth/hestia-glossary/issues/824)
* **operation:** add `Mowing, machine unspecified` ([1466243](https://gitlab.com/hestia-earth/hestia-glossary/commit/1466243986a9dffb83c1868636bc9fa26ce0ae24))
* **operation:** add `Oil refining, machine unspecified` ([7f1ad9c](https://gitlab.com/hestia-earth/hestia-glossary/commit/7f1ad9c72739a4d08b21d24b0e8c01a5c4df720c)), closes [#839](https://gitlab.com/hestia-earth/hestia-glossary/issues/839)
* **operation:** add `transesterification` ([85fc063](https://gitlab.com/hestia-earth/hestia-glossary/commit/85fc063ff11317cb0c3875dbbb686fb22ea5c34f)), closes [#819](https://gitlab.com/hestia-earth/hestia-glossary/issues/819)
* **operation:** add a term for each oil refining step ([3262aba](https://gitlab.com/hestia-earth/hestia-glossary/commit/3262aba7aca97501873f9ac7b944d1d821e6b6f9)), closes [#844](https://gitlab.com/hestia-earth/hestia-glossary/issues/844) [#849](https://gitlab.com/hestia-earth/hestia-glossary/issues/849)
* **operation:** add multiple operations for seaweed farming ([97cc1a1](https://gitlab.com/hestia-earth/hestia-glossary/commit/97cc1a1186873a8f15ab49f361e6723c3deeb847)), closes [#808](https://gitlab.com/hestia-earth/hestia-glossary/issues/808) [#831](https://gitlab.com/hestia-earth/hestia-glossary/issues/831)
* **operation:** add multiple terms ([bf826a5](https://gitlab.com/hestia-earth/hestia-glossary/commit/bf826a5b9daa7bb13307bec6fedcd5a40ac93476))
* **operation:** rename cooling terms to clarify they refer to buildings ([df01f8b](https://gitlab.com/hestia-earth/hestia-glossary/commit/df01f8b5b2245ca3475ba1fe087dd885457f9738))
* **organicFertiliser:** add specific terms for goats and sheep manure ([903f6ab](https://gitlab.com/hestia-earth/hestia-glossary/commit/903f6ab911142e0a8ed812cc2f0f9b02a53680d4)), closes [#777](https://gitlab.com/hestia-earth/hestia-glossary/issues/777)
* **pesticideAI:** rename `Vanillin` `Vanillin (pesticide AI)` ([dcff1e5](https://gitlab.com/hestia-earth/hestia-glossary/commit/dcff1e598519e12167108b73dda45c9f88be4380)), closes [#837](https://gitlab.com/hestia-earth/hestia-glossary/issues/837)
* **processedFood:** add `Lentil milk` ([da04b20](https://gitlab.com/hestia-earth/hestia-glossary/commit/da04b201cf6c58c46e00570dcbd4271d0bd858eb)), closes [#841](https://gitlab.com/hestia-earth/hestia-glossary/issues/841)
* **processedFood:** add `Soybean, flour` ([5e0e9af](https://gitlab.com/hestia-earth/hestia-glossary/commit/5e0e9afb3e2c5c3f6acfbb6a2d54cb78383a443a)), closes [#798](https://gitlab.com/hestia-earth/hestia-glossary/issues/798)
* **processedFood:** add dried fruits ([f4a6f48](https://gitlab.com/hestia-earth/hestia-glossary/commit/f4a6f483d7a351ea42c755f80ccec05963159976)), closes [#518](https://gitlab.com/hestia-earth/hestia-glossary/issues/518)
* **property:** add `alginate content` ([58195b5](https://gitlab.com/hestia-earth/hestia-glossary/commit/58195b5d04ee363f8ea6347950979bacec09db3a)), closes [#804](https://gitlab.com/hestia-earth/hestia-glossary/issues/804)
* **property:** add `Bagasse content` ([e38a052](https://gitlab.com/hestia-earth/hestia-glossary/commit/e38a0524fd445d5b6b86f710a19cc580eb939c61))
* **property:** add `Free fatty acid content` ([698cc2c](https://gitlab.com/hestia-earth/hestia-glossary/commit/698cc2c42ed2191f0f28fa49af216f1ec32a4d61)), closes [#846](https://gitlab.com/hestia-earth/hestia-glossary/issues/846)
* **property:** add terms to describe artificial insemination success rate ([eeebcd5](https://gitlab.com/hestia-earth/hestia-glossary/commit/eeebcd5a136639e0fee0a09333f1d794ba34cad4)), closes [#791](https://gitlab.com/hestia-earth/hestia-glossary/issues/791)
* **soilAmendment:** add `Spent coffee grounds` ([1146ee1](https://gitlab.com/hestia-earth/hestia-glossary/commit/1146ee1b048339dbb64e42e4548cf264348ea9f5)), closes [#816](https://gitlab.com/hestia-earth/hestia-glossary/issues/816)
* **system:** add `Grow bag farming system` ([3d2fd6c](https://gitlab.com/hestia-earth/hestia-glossary/commit/3d2fd6cda0e657a29d50d3b5e48fa03842deca33))
* **transport:** add `Freight, car`, `Chilled freight, car`, and `Frozen freight, car` ([2ab1cde](https://gitlab.com/hestia-earth/hestia-glossary/commit/2ab1cde5ee8c1240bc2cc3050b41a7aa64612b15)), closes [#792](https://gitlab.com/hestia-earth/hestia-glossary/issues/792)
* **waste:** add `Waste water` ([0db02d9](https://gitlab.com/hestia-earth/hestia-glossary/commit/0db02d97a181a66b923482650e43b6a644745898)), closes [#833](https://gitlab.com/hestia-earth/hestia-glossary/issues/833)


### Bug Fixes

* **animalProduct:** use poultry excreta lookups for poultry terms ([a8e5fb4](https://gitlab.com/hestia-earth/hestia-glossary/commit/a8e5fb47b220b409baf4bd171504cc47b46cf2a1))
* **crop:** add missing synonyms to per hectare rice terms ([2603ff6](https://gitlab.com/hestia-earth/hestia-glossary/commit/2603ff6a33e1707493d88e1a65d8cbd52a81ea47))
* **cropResidueManagement:** revert `Residue ...` units for terms to `%` ([1cc4409](https://gitlab.com/hestia-earth/hestia-glossary/commit/1cc4409d322b929b7dc34a92e4c7747d703c6162)), closes [#783](https://gitlab.com/hestia-earth/hestia-glossary/issues/783)
* **emission:** remove `.` from lookup names ([45cef69](https://gitlab.com/hestia-earth/hestia-glossary/commit/45cef6973787d2eba9b101cd6b1d23e3f5b80356))
* **fertiliserBrandName:** rename `Cocofeed` to `Cocofeed 0-30-20` ([2795c93](https://gitlab.com/hestia-earth/hestia-glossary/commit/2795c93e05fca92c45a481beda43f1aa5d11d05a))
* **material:** error in CAS number for rubber ([116b086](https://gitlab.com/hestia-earth/hestia-glossary/commit/116b086d5454e59e98bcdc66cc5c8fa1688906d2))
* **other:** delete `sensitivityAlternativeTerms` lookup ([6738a1d](https://gitlab.com/hestia-earth/hestia-glossary/commit/6738a1dd56942ad373bedec29babe43dfade02a6)), closes [#801](https://gitlab.com/hestia-earth/hestia-glossary/issues/801)
* **pesticideAI:** rename `Diesel fuels, No. 2` to `Diesel (pesticide AI)` ([622411a](https://gitlab.com/hestia-earth/hestia-glossary/commit/622411a62ab14610550e7ba5fc648d01ee2d3965))
* **soilTexture:** remove `.` from lookup names ([a0144f5](https://gitlab.com/hestia-earth/hestia-glossary/commit/a0144f5f6b4a7f4807b1ce0579359c3e8d4a36c1))
* **soilType:** fix formatting of lookup names ([5874b43](https://gitlab.com/hestia-earth/hestia-glossary/commit/5874b4306b07398d97db65bda8ed08ce7f349338))
* **usdaSoilType:** fix formatting of lookup names ([ea97ec5](https://gitlab.com/hestia-earth/hestia-glossary/commit/ea97ec59aee593234be989a91c5ba6f7ffaf0fda))


* **pesticideBrandName:** rename `EPA Product Status` lookup `epaProductStatus` ([6acef11](https://gitlab.com/hestia-earth/hestia-glossary/commit/6acef11af009761e072fc05875dc9630db82ba8c)), closes [#782](https://gitlab.com/hestia-earth/hestia-glossary/issues/782)

## [0.24.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.23.0...v0.24.0) (2023-06-19)


### ⚠ BREAKING CHANGES

* **landUseManagement:** move `Number of fertilisations` to `landUseManagement` glossary.
* **landUseManagement:** move `Number of pesticide applications` to `landUseManagement` glossary.
* **landUseManagement:** move `Number of weedings` to `landUseManagement` glossary.

### Features

* **landUseManagement:** add `Number of harvests` ([9d01c01](https://gitlab.com/hestia-earth/hestia-glossary/commit/9d01c01fa14439fcae442beac1c0adac042e0144))
* **landUseManagement:** move `Number of` terms from `cropEstablishment` glossary ([cc696fc](https://gitlab.com/hestia-earth/hestia-glossary/commit/cc696fcdece0d805657cc3739dae3d0c4c348e6f))
* **material:** add terms for `Alkyd resin` and `Alkyd resin 70%` ([24182f8](https://gitlab.com/hestia-earth/hestia-glossary/commit/24182f8b5a7e2518187455a87b1ba72245be77b6)), closes [#771](https://gitlab.com/hestia-earth/hestia-glossary/issues/771)
* **waterRegime:** add `allowedRiceTermId` lookup ([838b698](https://gitlab.com/hestia-earth/hestia-glossary/commit/838b6985cb751e34cb62dc4af641df66c3f8191b))
* **waterRegime:** add `percentAreaSumIs100` lookup ([2ba4b90](https://gitlab.com/hestia-earth/hestia-glossary/commit/2ba4b905c23c7be56ed9e7b4e5f032821e7c323a)), closes [#797](https://gitlab.com/hestia-earth/hestia-glossary/issues/797)


### Bug Fixes

* **waterRegime:** set `IPCC_2019_CH4_rice_SFw` lookup as `not required` if no flooding occurs ([6869f9b](https://gitlab.com/hestia-earth/hestia-glossary/commit/6869f9b44139364f3d093eb96e7fea0a3d7deb2e)), closes [#797](https://gitlab.com/hestia-earth/hestia-glossary/issues/797)

## [0.23.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.22.0...v0.23.0) (2023-06-16)


### ⚠ BREAKING CHANGES

* **emission:** Split term
`CO2, to air, above ground biomass stock change` into two new terms
`CO2, to air, above ground biomass stock change, management change`
and
`CO2, to air, above ground biomass stock change, land use change`.
* **emission:** Split term
`CO2, to air, below ground biomass stock change` into two new terms
`CO2, to air, below ground biomass stock change, management change`
and
`CO2, to air, below ground biomass stock change, land use change`.
* **emission:** Split term
`CO2, to air, soil carbon stock change` into two new terms
`CO2, to air, soil carbon stock change, management change` and
`CO2, to air, soil carbon stock change, land use change`.
* **emission:** Split term
`N2O, to air, diminishing soil carbon stocks` into two new terms
`N2O, to air, diminishing soil carbon stocks, management change, direct`
and
`N2O, to air, diminishing soil carbon stocks, land use change, direct`.

### Features

* **crop:** add `Cassava, stalk` ([8928df9](https://gitlab.com/hestia-earth/hestia-glossary/commit/8928df928dc4eca44d9f6de69e517c5fa4ac60b3))
* **crop:** add `Wheat, Alkalage®` ([24995e2](https://gitlab.com/hestia-earth/hestia-glossary/commit/24995e2025d3876ed722888eea020500f5179381)), closes [#761](https://gitlab.com/hestia-earth/hestia-glossary/issues/761)
* **emission:** split carbon stock change emissions into management and LUC ([8838866](https://gitlab.com/hestia-earth/hestia-glossary/commit/883886610dc09f51623dd9774dde71a5f2255bfc)), closes [#669](https://gitlab.com/hestia-earth/hestia-glossary/issues/669)
* **feedFoodAdditive:** adds `Rumen protected fat` ([61d30ec](https://gitlab.com/hestia-earth/hestia-glossary/commit/61d30ec2417a02dfdc3ae5b672ac90f8ec517c70)), closes [#760](https://gitlab.com/hestia-earth/hestia-glossary/issues/760)
* **operation:** add `Power tiller use, machine unspecified` and `Harvesting fish, by hand` ([ff3d902](https://gitlab.com/hestia-earth/hestia-glossary/commit/ff3d902427ee45ded9b59e772d659f67f3745dcf))
* **processedFood:** add `Calf milk replacer, powder (unspecified)` ([4b2ce7c](https://gitlab.com/hestia-earth/hestia-glossary/commit/4b2ce7c859ae0369f6b431f31c9a1d0455f59f31)), closes [#763](https://gitlab.com/hestia-earth/hestia-glossary/issues/763)
* **region:** include GADM level 1 in `landUseManagement` lookup ([2aa5dcf](https://gitlab.com/hestia-earth/hestia-glossary/commit/2aa5dcfebf6028701ca4e4e1ed92ac1f7e7999c9))


### Bug Fixes

* **forage:** typo in `name` of `Ryegrass, hay` ([97d0dc4](https://gitlab.com/hestia-earth/hestia-glossary/commit/97d0dc40606b955b944de3697c8d21c996140c7e))
* **operation:** typo in term id in lookup ([c8545c3](https://gitlab.com/hestia-earth/hestia-glossary/commit/c8545c3e7aa1572a2308b810306695151e6d70cd)), closes [#794](https://gitlab.com/hestia-earth/hestia-glossary/issues/794)

## [0.22.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.21.0...v0.22.0) (2023-06-15)


### ⚠ BREAKING CHANGES

* **region:** rename `Arch` to
`Arch (Commune), Büren, Bern, Switzerland`.
* **fertiliserBrandName:** rename `Euronature P26B (kg mass)` `Euronature P26B`
* **fertiliserBrandName:** rename `Lyderis N25 + S3 (kg mass`) `Lyderis N25 + S3`
* **fertiliserBrandName:** rename `Patentkali (kg mass)` `Patentkali`

### Features

* add `sensitivityAlternativeTerms` to remaining glossaries ([879b613](https://gitlab.com/hestia-earth/hestia-glossary/commit/879b61325e523509fd3ed6f5d27b8c6dd4b9f52c)), closes [#715](https://gitlab.com/hestia-earth/hestia-glossary/issues/715)
* **animalProduct:** add pigeon, quail, and guinea pig meat products ([ba3f2eb](https://gitlab.com/hestia-earth/hestia-glossary/commit/ba3f2eb7d55c5cdfffdf5caaa6463a6d01581c64)), closes [#781](https://gitlab.com/hestia-earth/hestia-glossary/issues/781)
* **animalProduct:** add terms for goose and duck eggs ([63fdf3b](https://gitlab.com/hestia-earth/hestia-glossary/commit/63fdf3bae2cf19940c244e59163a70d3c9d38e10)), closes [#781](https://gitlab.com/hestia-earth/hestia-glossary/issues/781)
* **crop:** adds lookups for IPCC landuse categories ([fb8c222](https://gitlab.com/hestia-earth/hestia-glossary/commit/fb8c2228b0e638f75565afeb71dcb50d7a63da1e))
* **cropSupport:** add new terms ([c52324c](https://gitlab.com/hestia-earth/hestia-glossary/commit/c52324c3bb4b3159ed741190402e2bcf242868d1))
* **ecoClimateZone:** add IPCC 2019 carbon stock change factors ([d2d3abf](https://gitlab.com/hestia-earth/hestia-glossary/commit/d2d3abf4d31d2c880db9b64c241940680c00e7bb))
* **ecoClimateZone:** add IPCC 2019 carbon stock change factors for grasslands ([27f79df](https://gitlab.com/hestia-earth/hestia-glossary/commit/27f79dfc414c6ceead6e60b35d68af7f718c3be8))
* **ecoClimateZone:** adds default IPCC SOCref lookups ([6914756](https://gitlab.com/hestia-earth/hestia-glossary/commit/691475613c37cbe0410e4eff3417ae43cc5af3f9))
* **electricity:** re-work glossary to differentiate on-site production ([5dab8af](https://gitlab.com/hestia-earth/hestia-glossary/commit/5dab8afb5312dc87a0a7144fa3093e10c6e80762)), closes [#669](https://gitlab.com/hestia-earth/hestia-glossary/issues/669)
* **emission:** add lookup `inHestiaDefaultSystemBoundary` ([4d21813](https://gitlab.com/hestia-earth/hestia-glossary/commit/4d21813e7a1c07caabea4d5636ce043dfdc1dc7b)), closes [#756](https://gitlab.com/hestia-earth/hestia-glossary/issues/756)
* **fertiliserBrandName:** add `Borosol® 10` and `Lokomotive®` ([dcdf5fd](https://gitlab.com/hestia-earth/hestia-glossary/commit/dcdf5fdab0cac8035131148e974061cd1ca8571b))
* **fertiliserBrandName:** add Ghanaian cocoa fertiliser brands ([fee9487](https://gitlab.com/hestia-earth/hestia-glossary/commit/fee948741da1338470617097a7057e7ec03b1f99)), closes [#778](https://gitlab.com/hestia-earth/hestia-glossary/issues/778) [#714](https://gitlab.com/hestia-earth/hestia-glossary/issues/714)
* **fertiliserBrandName:** remove `(kg mass)` from terms name ([e8443d4](https://gitlab.com/hestia-earth/hestia-glossary/commit/e8443d432a548822584ad472b9896d92a7ce41a2))
* **forage:** add terms for ryegrass and danthonia ([367244a](https://gitlab.com/hestia-earth/hestia-glossary/commit/367244a034408479485f17588f98711757313b57)), closes [#752](https://gitlab.com/hestia-earth/hestia-glossary/issues/752) [#759](https://gitlab.com/hestia-earth/hestia-glossary/issues/759)
* **inorganicFertiliser:** move `Carbon dioxide (fertiliser)` from `other` ([1e642be](https://gitlab.com/hestia-earth/hestia-glossary/commit/1e642be23febd9550915709da0cf70a6ee766f8f))
* **landUseManagement:** add IPCC lookup for practices increasing C input ([de050d8](https://gitlab.com/hestia-earth/hestia-glossary/commit/de050d819063ce80d58d7b728288bd4b2053c847))
* **measurement:** add terms for `Base saturation` and `Calcium saturation` ([1a9eb24](https://gitlab.com/hestia-earth/hestia-glossary/commit/1a9eb24ffcd8ac238d0f5643081da37799a8cfb2)), closes [#629](https://gitlab.com/hestia-earth/hestia-glossary/issues/629)
* **organicFertiliser:** add `Grape pomace, wet` ([47458a2](https://gitlab.com/hestia-earth/hestia-glossary/commit/47458a2c69c70aa2dd276a6612a8024ce1532d2b)), closes [#773](https://gitlab.com/hestia-earth/hestia-glossary/issues/773)
* **other:** add `semen` ([f348201](https://gitlab.com/hestia-earth/hestia-glossary/commit/f348201b6d38b9034049f2e6c96a4692ac41091c)), closes [#764](https://gitlab.com/hestia-earth/hestia-glossary/issues/764)
* **pesticideAI:** add `Termiticide, unspecified (AI)` ([4c8b0db](https://gitlab.com/hestia-earth/hestia-glossary/commit/4c8b0db564bd7512cb461393328033d916ed8e19)), closes [#776](https://gitlab.com/hestia-earth/hestia-glossary/issues/776)
* **pesticideAI:** rename `CAS-155569-91-8` to `Emamectin benzoate` ([f0fbfda](https://gitlab.com/hestia-earth/hestia-glossary/commit/f0fbfda3fa77a011385ee8157772888801739cdc))
* **pesticideBrandName:** add Ghanaian and Kenyan pesticides ([9511840](https://gitlab.com/hestia-earth/hestia-glossary/commit/951184099856e10c32be788dcc4089ab11b33655)), closes [#776](https://gitlab.com/hestia-earth/hestia-glossary/issues/776)
* **property:** add `Selenium content` and terms for Vitamin A, D, and E content ([dcb9b72](https://gitlab.com/hestia-earth/hestia-glossary/commit/dcb9b7241e7206c19ace6ed8978ad8d03d2a4a36)), closes [#762](https://gitlab.com/hestia-earth/hestia-glossary/issues/762)
* **region:** re-run foastat data ([f77ea27](https://gitlab.com/hestia-earth/hestia-glossary/commit/f77ea27052558b87ae45195b4064908b890ee40b))
* **soiAmendment:** move `Bioplug` from `other` glossary ([988a41a](https://gitlab.com/hestia-earth/hestia-glossary/commit/988a41a5ff5f83673c3c876d41a4197f9944173c))
* **soilAmendment:** move `Plant-growth regulator` from `other` glossary ([65dfc2e](https://gitlab.com/hestia-earth/hestia-glossary/commit/65dfc2eaf89ca57dcf217af91485b0b237b0a09d))
* **soilAmendment:** move `Slavol` from `other` glossary ([f5d65a6](https://gitlab.com/hestia-earth/hestia-glossary/commit/f5d65a6fd3383d6746acbe0d1892dd7b3a6bffac))
* **tillage:** adds IPCC tillage management category lookups ([8620369](https://gitlab.com/hestia-earth/hestia-glossary/commit/8620369f919bb8aa1c0249888a7e2d518f0fe0d2))
* **waterRegime:** add IPCC lookup for practices increasing C input ([6833d15](https://gitlab.com/hestia-earth/hestia-glossary/commit/6833d158c1131fdf2aed91a3d41b9701688c8c1c))


### Bug Fixes

* **animalProduct:** errors in `excretaKg...TermId` lookups for milk ([3b51dec](https://gitlab.com/hestia-earth/hestia-glossary/commit/3b51decfadbbe56c06e4fd417e874d06d754c13a))
* **crop:** errors in `generateImpactAssessment` and `skipAggregation` lookups ([4790910](https://gitlab.com/hestia-earth/hestia-glossary/commit/4790910e837671a62027b88f0639844542ac0993))
* **crop:** use `Palm` as proxy for `Banana` BG crop residue lookup ([b3ee816](https://gitlab.com/hestia-earth/hestia-glossary/commit/b3ee816ef37d661d386e823a9f66653d8e3d1dd8)), closes [#630](https://gitlab.com/hestia-earth/hestia-glossary/issues/630)
* **ecoClimateZone:** rename IPCC cropland carbon input factors ([4d1ef17](https://gitlab.com/hestia-earth/hestia-glossary/commit/4d1ef17295cc56fdf30c595f6b50e95166bc3fab))
* **fertiliserBrandName:** wrong ecoinventMapping for Enebe and Bortrac ([9e8b970](https://gitlab.com/hestia-earth/hestia-glossary/commit/9e8b97099137b917851a4d02f3d1356e775ea99b))
* **organicFertiliser:** inconsistent capitalisation in term names ([c2e8d08](https://gitlab.com/hestia-earth/hestia-glossary/commit/c2e8d08ae4e33703cf7e237270b11b4cd1d25f6c))
* **region:** rename `Arch` to `Arch (Commune), Büren, Bern, Switzerland` ([ae875f5](https://gitlab.com/hestia-earth/hestia-glossary/commit/ae875f51c97178b0604ca25d50cfd951e64c8951))

## [0.21.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.20.0...v0.21.0) (2023-06-05)


### ⚠ BREAKING CHANGES

* **liveAnimal:** rename `Beef cattle. bull` `Beef cattle, bull`
* **inorganicFertiliser:** Changes the units of `Euronature P26B`,
`Lyderis N25 + S3`, and `Patentkali` to `kg mass`.
* **region:** rename `Cork` to `Cork (Traditional County), Ireland`.
* **region:** rename `Papër` to `Papër (Commune), Elbasanit, Elbasan, Albania`.
* **crop:** Renames `Sugarcane, molasse` to
`Sugarcane, molasses`.
* **organicFertiliser:** `Molasses` is moved from `pesticideAI` to
`organicFertiliser` resulting in a new `id` `molassesOrganicFertiliserKgN`
or `molassesOrganicFertiliserKgMass` and `name`,
`Molasses (organic fertiliser) (kg N)` and
`Molasses (organic fertiliser) (kg mass)`.
* **inorganicFertiliser:** Changes the id of `Zinc nitrate` from
`CAS-7779-88-6` to `zincNitrate` as moved into `inorganicFertiliser`
glossary. Also changes the units from `kg active ingredient` to `kg N`.
* **pesticideAI:** Renames `Rape-oil Me esters fatty acids` to
`Rapeseed oil, methylated (pesticide AI)`.
* **pesticideAI:** Renames `BTC 776` to
`Alkyl(C12-C18) dimethyl benzyl ammonium chloride`.
* **pesticideAI:** Renames `Soaps` to `Soaps (pesticide AI)` and
`Soaps, Castile` to `Soaps, castile (pesticide AI)`.
* **pesticideAI:** Renames
`Distillates (petroleum), Solvent-refined light naphthenic` to
`Paraffin oil, solvent-refined light naphthenic (pesticide AI)`.
* **pesticideAI:** Renames
`Distillates (petroleum), Hydrotreated heavy paraffinic` to
`Paraffin oil, hydrotreated heavy paraffinic (pesticide AI)`,
and renames `Distillates (petroleum), Hydrotreated light paraffinic` to
`Paraffin oil, hydrotreated light paraffinic (pesticide AI)`,
and renames `Distillates (petroleum), Hydrotreated middle` to
`Paraffin oil, hydrotreated middle paraffinic (pesticide AI)`,
and renames `Distillates (petroleum), Solvent-refined heavy paraffinic`
to `Paraffin oil, solvent-refined heavy paraffinic (pesticide AI)`
and renames `Distillates (petroleum), Solvent-refined light paraffinic` to
`Paraffin oil, solvent-refined light paraffinic (pesticide AI)`, and
deletes `CAS-68477-31-6`.
* **pesticideAI:** Renames `Delphinidin` to `Paraffin oils (pesticide AI)`.
* **pesticideAI:** Renames `(3-Chlorophenyl)hydrazonomalononitrile`
to `Carbonyl cyanide m-chlorophenyl hydrazon`
* **forage:** Renames `Grass-clover mixture` to
`Grass-clover mixture, fresh forage` for consistency with rest of glossary.
* **pesticideAI:** rename `CAS-91465-08-6` `lambda-Cyhalothrin`
* **region:** Rename `Oberon` `Oberon (Area), New South Wales, Australia`
* **pesticideAI:** Rename `Actara` `Thiamethoxam`
* **pesticideBrandName:** Rename `Alto 100Sl Fungicide` `Alto 100 SL Fungicide`
* **pesticideBrandName:** Rename `Oberon SPeed` `Oberon Speed`
* **crop:** rename `Mexican tea, plant` `Mexican tea plant`
* **crop:** rename `Mexican tea, extract` `Mexican tea, leaf extract`

### Features

* add `sensitivityAlternativeTerms` to additional Input glossaries ([b2fde43](https://gitlab.com/hestia-earth/hestia-glossary/commit/b2fde43afec3ea1cca4a8e3eced38f28bd0ec0dc))
* **animalProduct:** add `Eggs, hen, white` ([2c5b02e](https://gitlab.com/hestia-earth/hestia-glossary/commit/2c5b02ea4df28870d3e30a56d5cfb7afe8bcee70)), closes [#741](https://gitlab.com/hestia-earth/hestia-glossary/issues/741)
* **animalProduct:** add `Eggs, other bird, white` ([3cf0af0](https://gitlab.com/hestia-earth/hestia-glossary/commit/3cf0af0d098ca1148ace11e2e03946fdb36a59bb))
* **antibiotic:** move terms identified as antibiotics from `pesticideAI` to `antibiotic` ([c8706fa](https://gitlab.com/hestia-earth/hestia-glossary/commit/c8706faaac760863f83d6faa2bb037b2ba7e148e)), closes [#398](https://gitlab.com/hestia-earth/hestia-glossary/issues/398)
* **biologicalControlAgent:** set `units` to `kg` for all terms ([6c1f207](https://gitlab.com/hestia-earth/hestia-glossary/commit/6c1f207f7d4c663951f32a8526dfeb5310aa89dc)), closes [#641](https://gitlab.com/hestia-earth/hestia-glossary/issues/641)
* **characterisedIndicator:** add `Stress weighted water footprint` ([3240316](https://gitlab.com/hestia-earth/hestia-glossary/commit/32403165ab971dbea4796b04d983dadd19225b7b))
* **characterisedIndicator:** allow ReCiPe 2016 models for `Human Toxicity Potential (1,4-DCBeq)` ([eb29205](https://gitlab.com/hestia-earth/hestia-glossary/commit/eb2920545a2f6ba976a45d3bde9f8f215811b19e)), closes [#682](https://gitlab.com/hestia-earth/hestia-glossary/issues/682)
* **crop:** add `Barley, distillers grain` ([41d4418](https://gitlab.com/hestia-earth/hestia-glossary/commit/41d4418ba28480123d61d3ea5be6e0d621456d36)), closes [#689](https://gitlab.com/hestia-earth/hestia-glossary/issues/689)
* **crop:** add `Cotton, seed hull` ([edd2478](https://gitlab.com/hestia-earth/hestia-glossary/commit/edd247806e7bfc3346c3d4ffc0147e31243dc774)), closes [#692](https://gitlab.com/hestia-earth/hestia-glossary/issues/692)
* **crop:** add `Grapes, pomace` ([e1e635b](https://gitlab.com/hestia-earth/hestia-glossary/commit/e1e635b9b9f20bc4b59dc0e24bc479a11a96c10e)), closes [#692](https://gitlab.com/hestia-earth/hestia-glossary/issues/692)
* **crop:** add `Maize, straw silage` ([1e6e42a](https://gitlab.com/hestia-earth/hestia-glossary/commit/1e6e42adb0f160680dc5bccefb9bb00fdd11f9e5)), closes [#748](https://gitlab.com/hestia-earth/hestia-glossary/issues/748)
* **crop:** add further terms for cultivated mushrooms ([4abd747](https://gitlab.com/hestia-earth/hestia-glossary/commit/4abd74705f8e541f95deb8e2685e7bae69eb8ed9)), closes [#731](https://gitlab.com/hestia-earth/hestia-glossary/issues/731)
* **crop:** add terms for tamarind, sichuan pepper, rooibos, borage, camomile, curry, hisbiscus ([542347e](https://gitlab.com/hestia-earth/hestia-glossary/commit/542347e2cd6a48871b9f43afd1c0d8666143d7d5)), closes [#518](https://gitlab.com/hestia-earth/hestia-glossary/issues/518)
* **emission:** add `NO3, to surface water, excreta` ([bb899ee](https://gitlab.com/hestia-earth/hestia-glossary/commit/bb899eea51c1c7a59347c77fcf8e1333b416e3de))
* **emission:** add `P ...` and `N, to surface water, excreta` ([3228322](https://gitlab.com/hestia-earth/hestia-glossary/commit/32283220799c275a0f98a7d989bad204e1c29388))
* **excreta:** add `skipAggregation` lookup ([884940a](https://gitlab.com/hestia-earth/hestia-glossary/commit/884940a2704431d5c539361926006029f3f51382))
* **feedFoodAdditive:** move `Ascorbic acid` from `pesticideAI` to `feedFoodAdditive` ([5888873](https://gitlab.com/hestia-earth/hestia-glossary/commit/58888736246ef4dae4cb992ff6918fca94a06f69))
* **feedFoodAdditive:** move `Cholesterol` from `pesticideAI` to `feedFoodAdditive` ([ce1a3a9](https://gitlab.com/hestia-earth/hestia-glossary/commit/ce1a3a96a3a67ba7224bc6a7e774324d38692def))
* **fertiliserBrandName:** add `ENEBE® 30-10-10` and tidy lookups ([c1e8db9](https://gitlab.com/hestia-earth/hestia-glossary/commit/c1e8db98d0867ce65764243216248a4330ab5ac6)), closes [#736](https://gitlab.com/hestia-earth/hestia-glossary/issues/736)
* **fertiliserBrandName:** add `YaraVita BORTRAC 150` ([c1c86fd](https://gitlab.com/hestia-earth/hestia-glossary/commit/c1c86fdf6d4ffbdee5b3adc32165b89d2868ef7a)), closes [#724](https://gitlab.com/hestia-earth/hestia-glossary/issues/724)
* **fertiliserBrandName:** create glossary ([234e755](https://gitlab.com/hestia-earth/hestia-glossary/commit/234e755f537fbb0d0a6ac2aabb72036d8510fbe5)), closes [#659](https://gitlab.com/hestia-earth/hestia-glossary/issues/659)
* **forage:** add `Chinese rye grass` ([a932667](https://gitlab.com/hestia-earth/hestia-glossary/commit/a932667b6540681bcb7efa78a79594d078a99053)), closes [#747](https://gitlab.com/hestia-earth/hestia-glossary/issues/747)
* **forage:** add `Generic grass, hay` ([0834b84](https://gitlab.com/hestia-earth/hestia-glossary/commit/0834b849e7c06d814395672ed25a469024ab11b5)), closes [#750](https://gitlab.com/hestia-earth/hestia-glossary/issues/750)
* **fuel:** add `Excess industrial heat` ([b5e6c2f](https://gitlab.com/hestia-earth/hestia-glossary/commit/b5e6c2ff34168eed54ccfa9a2bb40ad2a82077e2)), closes [#734](https://gitlab.com/hestia-earth/hestia-glossary/issues/734)
* **inorganicFertiliser:** move `Zinc nitrate` from `pesticideAI` to `inorganicFertiliser` ([b9d428d](https://gitlab.com/hestia-earth/hestia-glossary/commit/b9d428dc879ddee336786dce355acf3016294c2c)), closes [#398](https://gitlab.com/hestia-earth/hestia-glossary/issues/398)
* **landUseManagement:** adds `Ground cover` term ([3032567](https://gitlab.com/hestia-earth/hestia-glossary/commit/3032567fc8d3c404788caeaa84627edc994a62a6)), closes [#723](https://gitlab.com/hestia-earth/hestia-glossary/issues/723)
* **liveAnimal:** add terms for female and male unweaned calves ([0296cef](https://gitlab.com/hestia-earth/hestia-glossary/commit/0296cefc0fa9600b42950078afed6a5a5362b3c7))
* **machinery:** add multiple machineries used in algaculture ([4d8fe8f](https://gitlab.com/hestia-earth/hestia-glossary/commit/4d8fe8f73eb4643e7379198f16f6c5d27e24a5c6))
* **material:** add `Cork` and `Paper` ([b4ca372](https://gitlab.com/hestia-earth/hestia-glossary/commit/b4ca372273add6594bdfe1c69e2a8382b1dfd1ee))
* **model:** add `CML1992` ([4a6de34](https://gitlab.com/hestia-earth/hestia-glossary/commit/4a6de345598e41ad7841ca8ca00c042c1e90f9bb)), closes [#746](https://gitlab.com/hestia-earth/hestia-glossary/issues/746)
* **model:** add `Ridoutt & Pfister (2010)` ([4b56d81](https://gitlab.com/hestia-earth/hestia-glossary/commit/4b56d813ef4fe7e752da983a13d39ccfc4130ced))
* **operation:** add `Opening fruit, by hand` ([280a3e3](https://gitlab.com/hestia-earth/hestia-glossary/commit/280a3e35fd1fda86237b367cd268be9f8fc87c78)), closes [#712](https://gitlab.com/hestia-earth/hestia-glossary/issues/712)
* **operation:** add multiple terms ([df3ca6c](https://gitlab.com/hestia-earth/hestia-glossary/commit/df3ca6c7d571c5312a9730ae46fdac5b70d6be90)), closes [#694](https://gitlab.com/hestia-earth/hestia-glossary/issues/694) [#730](https://gitlab.com/hestia-earth/hestia-glossary/issues/730)
* **operation:** add multiple terms to describe algae cultivation practices ([0844aba](https://gitlab.com/hestia-earth/hestia-glossary/commit/0844abaf1961541d9eb62efb4ce539e7dee1b227))
* **organicFertiliser:** adds `carbonContent` and adds `methodModel` to `defaultProperties` ([661751e](https://gitlab.com/hestia-earth/hestia-glossary/commit/661751e27d2dd225c9608a8dc0a2f8ce5674fb1c)), closes [#735](https://gitlab.com/hestia-earth/hestia-glossary/issues/735)
* **organicFertiliser:** move `Molasses` from `pesticideAI` to `organicFertiliser` ([25490a4](https://gitlab.com/hestia-earth/hestia-glossary/commit/25490a40a0b895811f07ceb7b692bb21e685ffa5)), closes [#398](https://gitlab.com/hestia-earth/hestia-glossary/issues/398)
* **other:** add `Spawn` ([d85b903](https://gitlab.com/hestia-earth/hestia-glossary/commit/d85b903060180aa0d17a5e7fa5c12446af48b902)), closes [#733](https://gitlab.com/hestia-earth/hestia-glossary/issues/733)
* **other:** add `spores` ([f0f562e](https://gitlab.com/hestia-earth/hestia-glossary/commit/f0f562e9b554ff416d80a2c50865532b730986d2))
* **pesticideAI:** delete `CAS-76703-65-6` ([ac8d221](https://gitlab.com/hestia-earth/hestia-glossary/commit/ac8d221cb9ca6f37e8688202e885573354a12582))
* **pesticideAI:** rename `CAS-555-60-2` to `Carbonyl cyanide m-chlorophenyl hydrazon` ([184b2c0](https://gitlab.com/hestia-earth/hestia-glossary/commit/184b2c0514d3a847d9e2a7bca759de73678c42ca))
* **pesticideAI:** rename `CAS-85586-25-0` `Rapeseed oil, methylated (pesticide AI)` ([079205e](https://gitlab.com/hestia-earth/hestia-glossary/commit/079205e370b00c25a4b0a7ec7352cf878b68e991))
* **pesticideAI:** rename `CAS-91465-08-6` `lambda-Cyhalothrin` ([99674d3](https://gitlab.com/hestia-earth/hestia-glossary/commit/99674d301eab22c4983d373c428ce447cb81e773))
* **pesticideAI:** rename `Delphinidin` to `Paraffin oils (pesticide AI)` ([a50902b](https://gitlab.com/hestia-earth/hestia-glossary/commit/a50902b81e4102dcbeefd2ae9ca74d92d699bd84))
* **pesticideAI:** rename soap terms ([aac0539](https://gitlab.com/hestia-earth/hestia-glossary/commit/aac053983703411b4d9cd1d20137ebacce15f78f))
* **pesticideAI:** simplify Paraffin oil term names ([a8ad95f](https://gitlab.com/hestia-earth/hestia-glossary/commit/a8ad95fd9ea6c467791e282d993052a11fea9e77))
* **pesticideBrandName:** add multiple pesticides ([81087ac](https://gitlab.com/hestia-earth/hestia-glossary/commit/81087acc25cee8cbb76e887b7bb327fb57bf1712)), closes [#713](https://gitlab.com/hestia-earth/hestia-glossary/issues/713)
* **processedFood:** add `generateImpactAssessment` and `skipAggregation` lookups ([b26b8c3](https://gitlab.com/hestia-earth/hestia-glossary/commit/b26b8c307f9a0535b8f5c6ee3f1a8b1f47f0407e))
* **processedFood:** add `Wine` ([0a3aa8c](https://gitlab.com/hestia-earth/hestia-glossary/commit/0a3aa8cbd092f4d7dad5aa014f5cd1a7b0b1c609))
* **region:** change term names `Cork` and `Päper` ([19a4d01](https://gitlab.com/hestia-earth/hestia-glossary/commit/19a4d01417f1b5ee90894dc0fbb1f33e94de456c))
* **region:** rename `Oberon` `Oberon (Area), New South Wales, Australia` ([a19dcd5](https://gitlab.com/hestia-earth/hestia-glossary/commit/a19dcd5ccc1047c28a97d5a9afcbe41e9f889f5b))
* **soilAmendment:** add `Wood chip` ([b23da8d](https://gitlab.com/hestia-earth/hestia-glossary/commit/b23da8dd402dbc356556436ea2c14cdf013b5a8b)), closes [#732](https://gitlab.com/hestia-earth/hestia-glossary/issues/732)
* **soilType:** adds IPCC soil category lookups ([69a2bb4](https://gitlab.com/hestia-earth/hestia-glossary/commit/69a2bb46be7f29eddd51e901fcbac3186f8287aa))
* **system:** add `longline system` and `floating raft system` ([6126b6e](https://gitlab.com/hestia-earth/hestia-glossary/commit/6126b6eb29358a0cd309363b3e297400760cf818))
* **system:** add `Urban farming system` and `Circular farming system` ([eb241f0](https://gitlab.com/hestia-earth/hestia-glossary/commit/eb241f0b5d6da7f4a37c12b26b98fc4694fe9ea4))
* **usdaSoilType:** adds IPCC soil category lookups and fixes mistakes & typos ([7daceff](https://gitlab.com/hestia-earth/hestia-glossary/commit/7daceffaba01bc601baaa6b69b4d080ef2aa78b2))
* **waste:** add `Dead animal waste` term ([d9eaacf](https://gitlab.com/hestia-earth/hestia-glossary/commit/d9eaacff023cb693fbea3bdc596e754c2aad36d8)), closes [#636](https://gitlab.com/hestia-earth/hestia-glossary/issues/636)
* **waste:** add `synonym` for `Municipal solid waste` ([14899d9](https://gitlab.com/hestia-earth/hestia-glossary/commit/14899d93098a75be37f1498f65fecb8d430e627a))
* **waste:** create glossary ([d223bf6](https://gitlab.com/hestia-earth/hestia-glossary/commit/d223bf65954126a6ee34d1cf565caa12b40f030e))
* **wasteManagement:** create glossary ([9e725c4](https://gitlab.com/hestia-earth/hestia-glossary/commit/9e725c4da5bf609b1ad0ba24e9aad3e677f48160)), closes [#544](https://gitlab.com/hestia-earth/hestia-glossary/issues/544)


### Bug Fixes

* **animalManagement:** specify `boolean` units where relevant ([2200a98](https://gitlab.com/hestia-earth/hestia-glossary/commit/2200a98f56dbd494449d8c12b4b93bc87dc55ec0)), closes [#738](https://gitlab.com/hestia-earth/hestia-glossary/issues/738)
* **characterisedIndicator:** add missing terms to `model-mapping` lookup table ([a4ab786](https://gitlab.com/hestia-earth/hestia-glossary/commit/a4ab786beb13916e66fc9b920c9be59c4e6835ad))
* **characterisedIndicator:** allow ReCiPe as method for fine particulate matter in model mapping ([7d09ea2](https://gitlab.com/hestia-earth/hestia-glossary/commit/7d09ea27c61be410b46b7cc5271da8f7fedd8293))
* **characterisedIndicator:** error in model mapping for `cumulativeFossilEnergyDemand` ([a8e14af](https://gitlab.com/hestia-earth/hestia-glossary/commit/a8e14af8ee4ef139fbfbde0f8f841b107b1a9588))
* **characterisedIndicator:** errors in model mapping lookup ([624234a](https://gitlab.com/hestia-earth/hestia-glossary/commit/624234adaa884e64f5236f62d2fa45bf669edbc8))
* **crop:** rename `Mexican tea, plant` `Mexican tea plant` ([1db2af5](https://gitlab.com/hestia-earth/hestia-glossary/commit/1db2af5dd2fbb8c40f0dccb231019cdfb4643339))
* **crop:** rename `Sugarcane, molasse` to `Sugarcane, molasses` ([7ce1322](https://gitlab.com/hestia-earth/hestia-glossary/commit/7ce1322ebcd1e72cbbd7747c416b79564cc7e83b))
* delete `CAS-14515-52-5` as CAS number does not exist ([fb620fd](https://gitlab.com/hestia-earth/hestia-glossary/commit/fb620fd63f18a8f1ee6e433c3972b4efb1c5d7f1))
* **emission:** add missing terms to `model-siteTypesAllowe` lookup table ([4e868b8](https://gitlab.com/hestia-earth/hestia-glossary/commit/4e868b8ebbabb5e57733d77c9c70e50e26a81889))
* **emission:** error in `productTermIdsAllowed` for `CH4, to air, excreta` ([a07b7d3](https://gitlab.com/hestia-earth/hestia-glossary/commit/a07b7d32bc7b6bfa1c8eac9c28e7e6081047f19e)), closes [#754](https://gitlab.com/hestia-earth/hestia-glossary/issues/754)
* **forage:** rename `Grass-clover mixture` to `Grass-clover mixture, fresh forage` ([438f082](https://gitlab.com/hestia-earth/hestia-glossary/commit/438f082192f94e35a533836c2632d5da0011276a))
* **inorganicFertiliser:** fix units ([11b2c4d](https://gitlab.com/hestia-earth/hestia-glossary/commit/11b2c4d3e6aa50d19fc6d9d56c082344f0bd3519))
* **liveAnimal:** rename `Beef cattle. bull` `Beef cattle, bull` ([0954105](https://gitlab.com/hestia-earth/hestia-glossary/commit/0954105f5cee435cc578f3a0cd7665993cbf5ef0))
* **operation:** correct typos in glossary ([25fd7bc](https://gitlab.com/hestia-earth/hestia-glossary/commit/25fd7bcef104d6ead021bd1e43219826f8115afa))
* **pesticideAI:** delete terms identified as personal care products ([fe2c456](https://gitlab.com/hestia-earth/hestia-glossary/commit/fe2c456fe820ac353d4fb45c0236a05eb5d0c659)), closes [#398](https://gitlab.com/hestia-earth/hestia-glossary/issues/398)
* **pesticideAI:** delete terms not identified as pesticides ([0eaf72c](https://gitlab.com/hestia-earth/hestia-glossary/commit/0eaf72cc5bd71dbc99cea4763698f6334ff9af78)), closes [#398](https://gitlab.com/hestia-earth/hestia-glossary/issues/398)
* **pesticideAI:** move `Beauveria bassiana` to `biologicalControlAgent` glossary ([ded9a6b](https://gitlab.com/hestia-earth/hestia-glossary/commit/ded9a6b3024894bd4158731b8093768c91922e83)), closes [#713](https://gitlab.com/hestia-earth/hestia-glossary/issues/713)
* **pesticideAI:** rename `Actara` `Thiamethoxam` ([8202fac](https://gitlab.com/hestia-earth/hestia-glossary/commit/8202fac696ee8899d2a9905e2988c113ae35eb43))
* **pesticideAI:** rename `BTC 776` to `Alkyl(C12-C18) dimethyl...` ([6f26321](https://gitlab.com/hestia-earth/hestia-glossary/commit/6f26321ddd85da481cee679f0e4bd25dc034ba3c))
* **pesticideAI:** rename `CAS-64741-97-5` ([0378830](https://gitlab.com/hestia-earth/hestia-glossary/commit/03788308b310caed8daaf6074c80a2410a59f00e))
* **pesticideAI:** rename multiple terms ([3b33a60](https://gitlab.com/hestia-earth/hestia-glossary/commit/3b33a60669cb077c8df74ea515c06a1b48ea97f5))
* **pesticideBrandName:** delete `Raze Roach And Ant Spray` ([627c35c](https://gitlab.com/hestia-earth/hestia-glossary/commit/627c35cd931fb1dd3c842a3149041eae437bddfe))
* **soilAmendement:** capitalisation error in `Wood ash` `name` ([9683b40](https://gitlab.com/hestia-earth/hestia-glossary/commit/9683b400d2056ae9c65e93d92be52fc45c361138))
* **system:** add missing terms to `liveAnimal` lookup ([9496d20](https://gitlab.com/hestia-earth/hestia-glossary/commit/9496d20f5156734c39329c9f7f88afad565fd33d))

## [0.20.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.19.0...v0.20.0) (2023-05-22)


### ⚠ BREAKING CHANGES

* **material:** Rename `Stainless steel,depreciated amount per Cycle` `Stainless steel, depreciated amount per Cycle`
* **crop:** Rename `Kelp algae` `Kelp alga`
* **model:** Renames `EMEA-EEA (2019)` `EMEP-EEA (2019)`
* **model:** Renames `EMEA-EEA (2019) Country Average Fertiliser Mix`
`EMEP-EEA (2019) Country Average Fertiliser Mix`

### Features

* **building:** add `Plant nursery` ([4ed439c](https://gitlab.com/hestia-earth/hestia-glossary/commit/4ed439ca618ff146e476e1ef972f96b1c54d0647))
* **characterisedIndicator:** enable ReCiPe - FPMF combination in model mapping ([f5d00a4](https://gitlab.com/hestia-earth/hestia-glossary/commit/f5d00a40df5a4ca6502c27520dcc8fb1dbcf438d))
* **crop:** rename `Kelp algae` `Kelp alga` and add terms for nori, dulse, and gracilaria ([8194aab](https://gitlab.com/hestia-earth/hestia-glossary/commit/8194aab66c03f52c19605e63f94da6a37a3a75d4))
* **inorganicFertiliser:** add `Trisodium phosphate (kg P2O5)` ([25f6370](https://gitlab.com/hestia-earth/hestia-glossary/commit/25f63702d0e6d6b892e9167e44b1dce3ffc8c50b))
* **material:** add new terms for cement, polyamide, polyurethane, crushed stone, acrylic ([5c868ad](https://gitlab.com/hestia-earth/hestia-glossary/commit/5c868adec3d9ef05b8496a7eb4a98e2114df69f7))
* **methodMeasurement:** add `Lotti method` ([5b375d2](https://gitlab.com/hestia-earth/hestia-glossary/commit/5b375d242f044cb47ec11e9686ffbf539a9f76cd))
* **model:** add `SEMC (2000)` ([5a098bc](https://gitlab.com/hestia-earth/hestia-glossary/commit/5a098bc6d5d6e891d7a0602a6cab7f6f34ef31ab)), closes [#703](https://gitlab.com/hestia-earth/hestia-glossary/issues/703)
* **operation:** add `Draining flooded field, machine unspecified` ([5ef01d3](https://gitlab.com/hestia-earth/hestia-glossary/commit/5ef01d3703779b24d22ad563e8df7691608f2951))
* **property:** add `fixed carbon content`, `hydrogen content`, `molybdenum content` ([c3c5b98](https://gitlab.com/hestia-earth/hestia-glossary/commit/c3c5b9812a11427177de572ac85fa52733fc0eae)), closes [#687](https://gitlab.com/hestia-earth/hestia-glossary/issues/687)
* **soilAmendment:** add `biochar` ([8f21fa8](https://gitlab.com/hestia-earth/hestia-glossary/commit/8f21fa8f6236559ebfb047b38c38e741467ee7b7)), closes [#720](https://gitlab.com/hestia-earth/hestia-glossary/issues/720)
* **soilAmendment:** add `Xurian` and `Pseudomonas putida` ([c41465f](https://gitlab.com/hestia-earth/hestia-glossary/commit/c41465fe13d82ccca760a98fae32ec9d218736a6)), closes [#708](https://gitlab.com/hestia-earth/hestia-glossary/issues/708)


### Bug Fixes

* **material:** add missing space in `stainlessSteelDepreciatedAmountPerCycle` term name ([a53d0cd](https://gitlab.com/hestia-earth/hestia-glossary/commit/a53d0cd44ad9f1dace23f9180f0318819143778f))
* **model:** rename `EMEA-EEA (2019)` `EMEP-EEA (2019)` ([8f94cab](https://gitlab.com/hestia-earth/hestia-glossary/commit/8f94cab6b43c636cc9b412472ce9f4938eaff546))
* **region:** update sub-class of GADM-USA ([b14ce99](https://gitlab.com/hestia-earth/hestia-glossary/commit/b14ce997219cc596fb241e79244f905d2dbdc9a7))

## [0.19.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.18.2...v0.19.0) (2023-05-08)


### ⚠ BREAKING CHANGES

* **other:** All terms in `other` that used CAS numbers for `id` now use camel case term names. Use of CAS numbers as `term.id` restricted to `pesticideAI`.
* **inorganicFertiliser:** Renames `Lyderis N25 + S3` to `Lyderis N25 + S3 (kg N)`.

### Features

* **biologicalControlAgent:** add additional terms ([19610b5](https://gitlab.com/hestia-earth/hestia-glossary/commit/19610b58e91f626278f82f22382b33ecaec6e289))
* **characterisedIndicator:** allow ReCiPe 2008 - HTP combination in model mapping ([7d7c0cd](https://gitlab.com/hestia-earth/hestia-glossary/commit/7d7c0cd38892a0fba1030ed52397817f0a6de566)), closes [#700](https://gitlab.com/hestia-earth/hestia-glossary/issues/700)
* **crop:** add `Mexican tea` ([cdcae6e](https://gitlab.com/hestia-earth/hestia-glossary/commit/cdcae6e3bc009c6be9ab294e30ef1029c4327f26))
* **inorganicFertiliser:** add `sensitivityAlternativeTerms` lookup ([097de61](https://gitlab.com/hestia-earth/hestia-glossary/commit/097de61f0b9cfe940adcac93456b9936eb1e135a))
* **inorganicFertiliser:** add additional term to `sensitivityAlternativeTerms` ([5ef34b5](https://gitlab.com/hestia-earth/hestia-glossary/commit/5ef34b520db2a6e0f8a93e6cff8158638b8e5553)), closes [#715](https://gitlab.com/hestia-earth/hestia-glossary/issues/715)
* **machinery:** add `tree shaker` ([dbe4f27](https://gitlab.com/hestia-earth/hestia-glossary/commit/dbe4f27cc5ac016dbb7b8a86218aaba64b83ad27))
* move `L-Lysine, Hydrochloride` to `feedFoodAdditive` glossary ([d9c1d23](https://gitlab.com/hestia-earth/hestia-glossary/commit/d9c1d2385b3f0515fc131a06766d993eb10ee2e3))
* **other:** add term for CO2 fertiliser ([4f54d1f](https://gitlab.com/hestia-earth/hestia-glossary/commit/4f54d1fd450190e51fbadcfca42b2c7cf399e5d1)), closes [#522](https://gitlab.com/hestia-earth/hestia-glossary/issues/522)
* **other:** do not use CAS numbers for `term.id` ([1b1c85e](https://gitlab.com/hestia-earth/hestia-glossary/commit/1b1c85ee441d5ca336ccd601e348cfce922ed1be))
* **pesticideAI:** add `canonicalSmiles` ([f685461](https://gitlab.com/hestia-earth/hestia-glossary/commit/f685461ad846350e4dc119f899e9da072b78fb77))
* **pesticideAI:** add `density` data where available ([b02198a](https://gitlab.com/hestia-earth/hestia-glossary/commit/b02198a5a6f06163e63c9beddfa42649fe81af20)), closes [#704](https://gitlab.com/hestia-earth/hestia-glossary/issues/704)
* **pesticideAI:** add `Phenoxy compounds, unspecified` ([c9191ad](https://gitlab.com/hestia-earth/hestia-glossary/commit/c9191ad64e431cd14dce71db6c23add299ca0ac5)), closes [#701](https://gitlab.com/hestia-earth/hestia-glossary/issues/701)
* **pesticideAI:** add `subClassOf` to oils ([a8883ad](https://gitlab.com/hestia-earth/hestia-glossary/commit/a8883ad88eb0c6d234cfa1c6033b1d058a2f1060))
* **pesticideAI:** add back `Pyrethins` term ([62571ce](https://gitlab.com/hestia-earth/hestia-glossary/commit/62571ce4cbb8a5efad328a2179a430bf2a870030))
* **pesticideAI:** add missing `pesticideAI` based on FEX ([bbaa748](https://gitlab.com/hestia-earth/hestia-glossary/commit/bbaa74811441e7d6d823a8d2c66543bfc71f4f17)), closes [#664](https://gitlab.com/hestia-earth/hestia-glossary/issues/664) [#707](https://gitlab.com/hestia-earth/hestia-glossary/issues/707)
* **pesticideAI:** add missing terms identified by FEX ([ea82eab](https://gitlab.com/hestia-earth/hestia-glossary/commit/ea82eab85b01e7db092715695fcaeb6cfb44e481)), closes [#664](https://gitlab.com/hestia-earth/hestia-glossary/issues/664)
* **pesticideAI:** rename `Hexadrin` to `Endrin` ([ef1c7eb](https://gitlab.com/hestia-earth/hestia-glossary/commit/ef1c7ebd64df186b36a1ebb182b7a86a96d71166))
* **soilAmendment:** add `Manganese oxide` ([b297c24](https://gitlab.com/hestia-earth/hestia-glossary/commit/b297c244c9b55ad2059c454033dde1554e6c89d1))
* **transport:** add `Freight, with tractor and trailer` and terms for transport with lorry ([f70f9ff](https://gitlab.com/hestia-earth/hestia-glossary/commit/f70f9ff96764016ef6a57625dc87ee8c8dd70da9)), closes [#695](https://gitlab.com/hestia-earth/hestia-glossary/issues/695)
* **transport:** add ecoinvent links ([29125ef](https://gitlab.com/hestia-earth/hestia-glossary/commit/29125efd011951edefb8901f23219d9b08b5aaf1))


### Bug Fixes

* **animalBreed:** error in `liveweightPerHead` lookup ([2e4179e](https://gitlab.com/hestia-earth/hestia-glossary/commit/2e4179e2a825323de8f54b6faba77721cc162cca))
* **crop:** use `spp.` to denote many species rather than `ssp.` ([d92219e](https://gitlab.com/hestia-earth/hestia-glossary/commit/d92219e7fcc1e032a55ce7d1df5e9a219198d0f0))
* **inorganicFertiliser:** rename `Lyderis N25 + S3` `Lyderis N25 + S3 (kg N)` ([d3262bc](https://gitlab.com/hestia-earth/hestia-glossary/commit/d3262bcfae6dc4f093afb374ff151014026799f9))
* **liveAquaticSpecies:** use `spp.` instead of `nei` ([a0b82ab](https://gitlab.com/hestia-earth/hestia-glossary/commit/a0b82abaa1df780eee71b1d899466f4a76e10a29))
* **operation:** errors in `isTillage` lookup column ([5e746dd](https://gitlab.com/hestia-earth/hestia-glossary/commit/5e746dd966390733d6222241ee3cbfb3b798811a))
* **pesticideAI:** delete `Metaldehyde (unspecified)` ([4cf9203](https://gitlab.com/hestia-earth/hestia-glossary/commit/4cf9203be6957c7fae227200ce3303d9b3abb1ca)), closes [#691](https://gitlab.com/hestia-earth/hestia-glossary/issues/691)
* **pesticideAI:** delete `Phosphorus` ([c1f80dc](https://gitlab.com/hestia-earth/hestia-glossary/commit/c1f80dc0519f57273c22441656bfb2a2fa095ddd))
* **pesticideAI:** delete terms which are not pesticides ([53c561a](https://gitlab.com/hestia-earth/hestia-glossary/commit/53c561aa11c74fcc06d0c5558d5bcec5fb97d9fb))
* **pesticideAI:** duplicate `name`/`id` errors ([63dbd76](https://gitlab.com/hestia-earth/hestia-glossary/commit/63dbd76409a9c9094c17fb2098038f33166291a9))
* **pesticideAI:** errors in temporary column headers ([f0cf498](https://gitlab.com/hestia-earth/hestia-glossary/commit/f0cf498c4f549e794f4358393257429cb0f27ccc))
* **pesticideAI:** fix capitalisation errors ([83ed5b6](https://gitlab.com/hestia-earth/hestia-glossary/commit/83ed5b60bbb675b92ea811fa91cc836277040a58))
* **pesticideAI:** remove incorrect values for `canonicalSmiles` ([a6b1199](https://gitlab.com/hestia-earth/hestia-glossary/commit/a6b119979d6fe8167d76a6ad6823dc2daea5573a))
* **pesticideAI:** rename `Pyrethrins and Pyrethroids` to `Pyrethrin and pyrethroid compounds, unspecified` ([55903a3](https://gitlab.com/hestia-earth/hestia-glossary/commit/55903a3240fde9f8d6ca71215dc23b4cce8d4195))
* **pesticideAI:** use `(pesticide AI)` rather than `(pesticide active ingredient)` ([1c2e671](https://gitlab.com/hestia-earth/hestia-glossary/commit/1c2e671fa4a08f587f413e6fa804aae8e304bb14))
* **pesticideBrandName:** fix capitalisation error in `name` of `125Naocl` ([9bdbbf8](https://gitlab.com/hestia-earth/hestia-glossary/commit/9bdbbf8b299e81ab33ac7707446e299e70db1c9e))

### [0.18.2](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.18.1...v0.18.2) (2023-04-25)


### Bug Fixes

* **pesticideBrandName:** delete `Blossom Protect` and `Botector` ([f9ec455](https://gitlab.com/hestia-earth/hestia-glossary/commit/f9ec4550cbe9852ad1a31c46c5b55d3638607ab0))

### [0.18.1](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.18.0...v0.18.1) (2023-04-25)


### Features

* **biologicalControlAgent:** add CAS number to `Bacillus thuringiensis` ([f0055e3](https://gitlab.com/hestia-earth/hestia-glossary/commit/f0055e300f53c389b0a79852d98ea29194c1235d))
* **biologicalControlAgent:** add CAS number to `Pseudomonas syringae` ([bef837d](https://gitlab.com/hestia-earth/hestia-glossary/commit/bef837d1d9503314e3e20b2ffaabfc256a3793ae))
* **crop:** add `Hemp, flower` ([456d208](https://gitlab.com/hestia-earth/hestia-glossary/commit/456d208d76fd4f40ea4bedd4610b50832d597cc3))
* **crop:** add `Onion, leaf` ([2e8da39](https://gitlab.com/hestia-earth/hestia-glossary/commit/2e8da39db493c91c74f64c3b0b63db04590c5ce6))
* **crop:** add `Potato, leaf` ([7568b7b](https://gitlab.com/hestia-earth/hestia-glossary/commit/7568b7b52bab3696220f98acc1152f8ed9c65a8d))
* **crop:** add `Rice, grain (whole), parboiled` and `Rice, grain (refined), parboiled` ([7bd57fd](https://gitlab.com/hestia-earth/hestia-glossary/commit/7bd57fdecff8ddda79ade719a3694a2ac0c14b27))
* **crop:** add `soybeanOilEthoxylated` ([720b3ab](https://gitlab.com/hestia-earth/hestia-glossary/commit/720b3abfb0e30dbd13e3ffeade803777928d8660))
* **crop:** add additional `husk` `synonyms` to `hull` terms ([5b0bb24](https://gitlab.com/hestia-earth/hestia-glossary/commit/5b0bb24f20b0674720fdc9c508cc454515c59c4b)), closes [#532](https://gitlab.com/hestia-earth/hestia-glossary/issues/532)
* **material:** add `Cardboard` and `Cardboard, depreciated amount per Cycle` ([b3e15fe](https://gitlab.com/hestia-earth/hestia-glossary/commit/b3e15fef3d6270ef218a10d41060124c90fead35)), closes [#702](https://gitlab.com/hestia-earth/hestia-glossary/issues/702)
* **measurement:** add soil measurement stock terms with `per ha` functional unit ([4e43985](https://gitlab.com/hestia-earth/hestia-glossary/commit/4e43985398d499aa7ab5a2689709b901d1e0ad90)), closes [#688](https://gitlab.com/hestia-earth/hestia-glossary/issues/688)
* **operation:** add `Land planing, with laser guided land plane` and `Incorporating residue, with plough` ([82cbea8](https://gitlab.com/hestia-earth/hestia-glossary/commit/82cbea82a39edd1254099fdf36bf52fd015dae32)), closes [#699](https://gitlab.com/hestia-earth/hestia-glossary/issues/699)
* **operation:** add `Packaging, machine unspecified` and `Packaging, by hand` ([9132b4c](https://gitlab.com/hestia-earth/hestia-glossary/commit/9132b4c5b0297bde3a524024743538946915bee0)), closes [#702](https://gitlab.com/hestia-earth/hestia-glossary/issues/702)
* **operation:** add `Parboiling` and `Oil extraction, with solvent` ([dbf9a0f](https://gitlab.com/hestia-earth/hestia-glossary/commit/dbf9a0fe01405350d4d184cd4546594b9aca10e9)), closes [#696](https://gitlab.com/hestia-earth/hestia-glossary/issues/696)
* **other:** add `Activated charcoal` as `synoym` for `Activated carbon` ([1654592](https://gitlab.com/hestia-earth/hestia-glossary/commit/1654592a7228135912e22e1e4b7dc1566387c5b7))
* **pesticideAI:** add `N-(n-Butyl)thiophosphoric triamide` ([6b536cd](https://gitlab.com/hestia-earth/hestia-glossary/commit/6b536cd8d6b451698961126ae42490a9b39a1fb9))
* **pesticideAI:** rename `Tetris` to `Profoxydim` ([028d2c4](https://gitlab.com/hestia-earth/hestia-glossary/commit/028d2c4150468b06935ba7f95c8211ae69958473)), closes [#671](https://gitlab.com/hestia-earth/hestia-glossary/issues/671)
* **pesticideAI:** rename to `CAS-141517-21-7` to `Trifloxystrobin` ([e818920](https://gitlab.com/hestia-earth/hestia-glossary/commit/e818920d479a21850c7d4371c3fd074085d2c168)), closes [#671](https://gitlab.com/hestia-earth/hestia-glossary/issues/671)
* **soilTexture:** add `% area` as unit for all terms ([f99e356](https://gitlab.com/hestia-earth/hestia-glossary/commit/f99e3564f3974cc25fd02cb22e9e2188839ec638))


### Bug Fixes

* **crop:** errors in `cropGroupingFBS` column ([a1d7882](https://gitlab.com/hestia-earth/hestia-glossary/commit/a1d788216f82bf21072897f5f78989e8ab98e290))
* **crop:** fix hyperlink for rapeseed terms ([ce08251](https://gitlab.com/hestia-earth/hestia-glossary/commit/ce08251be8518a9c322cab556329341c86199276))
* **measurement:** change units from `g` to `kg` for per ha terms ([f7797f1](https://gitlab.com/hestia-earth/hestia-glossary/commit/f7797f11a0f5cf960ec7061d7e58676fb64707b4))
* **pesticideAI:** add `Alkylphenol ethoxylates, unspecified` ([9f8078f](https://gitlab.com/hestia-earth/hestia-glossary/commit/9f8078fa5f516d90521cb818a4cf437a7f55bab3))
* **pesticideAI:** delete `SUCROSE ACETATE ISOBUTYRATE` ([7bef571](https://gitlab.com/hestia-earth/hestia-glossary/commit/7bef571de0d627c375a329abdc6f2a3ae50e716d))
* **pesticideAI:** delete `Sucrose, Octaacetate` as duplicated ([227b712](https://gitlab.com/hestia-earth/hestia-glossary/commit/227b7120e6cb1251f2e895a35a1a483892a18a2a))
* **pesticideBrandName:** change `CAS-8007-45-2` to `coalTar` ([8d0b733](https://gitlab.com/hestia-earth/hestia-glossary/commit/8d0b7330c04f546959a0ca5716d1c1dae41983e1))
* **pesticideBrandName:** delete `Bull Run Fly Attractant` ([f049511](https://gitlab.com/hestia-earth/hestia-glossary/commit/f049511ae60592fe2c76a3beae6f6e9183d65f04))
* **pesticideBrandName:** delete `Fennosurf 586` and `Nalco 60620` ([b2c5145](https://gitlab.com/hestia-earth/hestia-glossary/commit/b2c5145db356f396708e5e07a9edced4ffe45553))
* **pesticideBrandName:** delete `Noxilizer No2 Sterilant` ([3fe415d](https://gitlab.com/hestia-earth/hestia-glossary/commit/3fe415d65093199e826198f64e39490c67f6c93e))
* **pesticideBrandName:** delete bactericides based on silver ([5ee17c7](https://gitlab.com/hestia-earth/hestia-glossary/commit/5ee17c780617643064987900e0f30e1298ba0fdb))
* **pesticideBrandName:** delete carbon dioxide based pesticide brands ([71ce55d](https://gitlab.com/hestia-earth/hestia-glossary/commit/71ce55d592a01095388572f1821ffe9de6e04c81))
* **pesticideBrandName:** delete pesticides containing `CAS-1066-33-7` ([a3f41b7](https://gitlab.com/hestia-earth/hestia-glossary/commit/a3f41b7c84ace109d9b3590b84802e604a1813ad))
* **pesticideBrandName:** delete pesticides containing `CAS-57-13-6` ([9540d54](https://gitlab.com/hestia-earth/hestia-glossary/commit/9540d541e459fed8cd6fe55499ae288bb7335434))
* **pesticideBrandName:** delete pesticides containing `CAS-7631-99-4` ([249031c](https://gitlab.com/hestia-earth/hestia-glossary/commit/249031ca1741d6bc581d355f1d45846201308e28))
* **pesticideBrandName:** delete pesticides containing `CAS-7778-77-0` ([eb1209f](https://gitlab.com/hestia-earth/hestia-glossary/commit/eb1209ffee9e9c37c1fa0ea3270a5d0355998546))
* **pesticideBrandName:** delete zinc based `bactekillerAz` ([7736d6e](https://gitlab.com/hestia-earth/hestia-glossary/commit/7736d6eb1e653aab82f31b36e2e8de59bce17813))
* **pesticideBrandName:** error in `Enhance (herbicide adjuvant)` CAS number ([7ee7617](https://gitlab.com/hestia-earth/hestia-glossary/commit/7ee7617e6f0465f97cbf9136299d963c0ec45ceb))
* **pesticideBrandName:** error in AI composition for `Activator 90` ([230d3d5](https://gitlab.com/hestia-earth/hestia-glossary/commit/230d3d5ba8f4289f661a17cc59ad6ed8f8b449c1))
* **pesticideBrandName:** link term to `Alkylphenol ethoxylates, unspecified` ([108cbf7](https://gitlab.com/hestia-earth/hestia-glossary/commit/108cbf78a72c85a70530926367007ba1b065eae5))
* **pesticideBrandName:** replace `CAS-120962-03-0` with `Rapeseed oil` ([dd69cf0](https://gitlab.com/hestia-earth/hestia-glossary/commit/dd69cf086ffdfbf59d3ea06cfe65a4a49657a111))
* **pesticideBrandName:** replace `CAS-68038-71-1` with `bacillusThuringiensis` ([9122889](https://gitlab.com/hestia-earth/hestia-glossary/commit/9122889268ce24583fefb51a70cd730415e277e1))
* **pesticideBrandName:** replace `CAS-68583-32-4` with `pseudomonasSyringae` ([cee32e0](https://gitlab.com/hestia-earth/hestia-glossary/commit/cee32e0096bdb9a40512b18ab6a07fb43812f856))
* **pesticideBrandName:** replace `CAS-7440-44-0` with `activatedCarbon` ([877f637](https://gitlab.com/hestia-earth/hestia-glossary/commit/877f637e514696a964e27f7180f4f9bcd94fe356))
* **pesticideBrandName:** replace `CAS-8001-22-7` with `soybeanOilRefined` ([f524f7f](https://gitlab.com/hestia-earth/hestia-glossary/commit/f524f7fb6b009dd5c5aded7870298b8014d469a2))
* **soilType:** rename `Calcisols terms` that should have been `Cambisols` ([4b7b003](https://gitlab.com/hestia-earth/hestia-glossary/commit/4b7b0038275f8b94b4312589fe162073b1c712e1)), closes [#684](https://gitlab.com/hestia-earth/hestia-glossary/issues/684)

## [0.18.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.17.0...v0.18.0) (2023-04-19)


### ⚠ BREAKING CHANGES

* **machinery:** Renames `Trailor` to `Trailer`

### Features

* **crop:** add `Camelina, straw` ([98ded1d](https://gitlab.com/hestia-earth/hestia-glossary/commit/98ded1dca1e93c7148acd3f0b9fcd03b5bded4fb)), closes [#681](https://gitlab.com/hestia-earth/hestia-glossary/issues/681)
* **crop:** add `Rice, flour` ([08141c4](https://gitlab.com/hestia-earth/hestia-glossary/commit/08141c4f7b588b9c39662e1845e7335539a0a820)), closes [#674](https://gitlab.com/hestia-earth/hestia-glossary/issues/674)
* **crop:** add more terms for Flax ([c1b0784](https://gitlab.com/hestia-earth/hestia-glossary/commit/c1b078445a4bdda41801eaeb4fdb6635dc88457e)), closes [#656](https://gitlab.com/hestia-earth/hestia-glossary/issues/656)
* **crop:** add terms for `Abyssinian kale` ([8d19520](https://gitlab.com/hestia-earth/hestia-glossary/commit/8d1952070599920755cc57d098772396cc9a49e8)), closes [#681](https://gitlab.com/hestia-earth/hestia-glossary/issues/681)
* **cropEstablishment:** add `Number of weedings` ([342220d](https://gitlab.com/hestia-earth/hestia-glossary/commit/342220d466e0e1b5bc27e12a65dbf56ef1c80c2c)), closes [#686](https://gitlab.com/hestia-earth/hestia-glossary/issues/686)
* **emission:** add `SO2, to air, crop residue burning` ([a107f64](https://gitlab.com/hestia-earth/hestia-glossary/commit/a107f647df031866f6fcca818d204c03dd2efe6b))
* **emission:** add PM2.5 and PM10 from crop residue burning ([66bcb73](https://gitlab.com/hestia-earth/hestia-glossary/commit/66bcb730bad109ccb09dbf75091a8f28f669e4e5)), closes [#674](https://gitlab.com/hestia-earth/hestia-glossary/issues/674)
* **inorganicFertiliser:** add `Lyderis 25N + 3S` ([1f7426c](https://gitlab.com/hestia-earth/hestia-glossary/commit/1f7426c238b4be09080ece7b579319e90192104a))
* **machinery:** add additional terms ([3dfd6f6](https://gitlab.com/hestia-earth/hestia-glossary/commit/3dfd6f62c491ac4323f8333b08174ba54e415bed)), closes [#674](https://gitlab.com/hestia-earth/hestia-glossary/issues/674)
* **machinery:** rename `Trailor` to `Trailer` and add `Trailor` as `synonym` ([909f6ab](https://gitlab.com/hestia-earth/hestia-glossary/commit/909f6abf43bd69a2bb2e7c804820e8a83ec3a78b))
* **model:** add `RiskPoll model (Rabl and Spadaro, 2004) and Greco et al 2007` ([dfbd1e0](https://gitlab.com/hestia-earth/hestia-glossary/commit/dfbd1e009bd74f4e9bb3ede594a9c4c0af93b4ce)), closes [#674](https://gitlab.com/hestia-earth/hestia-glossary/issues/674)
* **model:** add `World Meteorological Organization (1999)` ([887e57a](https://gitlab.com/hestia-earth/hestia-glossary/commit/887e57a0c247ecdd0ab7f25f4909c8fb0e9bdf27)), closes [#674](https://gitlab.com/hestia-earth/hestia-glossary/issues/674)
* **operation:** add `Maintaining banks`, `Milling grain`, `Maintaining ditches` ([d118c26](https://gitlab.com/hestia-earth/hestia-glossary/commit/d118c26577e1b97f04d383e39923377564c8a242)), closes [#674](https://gitlab.com/hestia-earth/hestia-glossary/issues/674)
* **operation:** add `Sun drying` and `Oil pressing, with screw press` ([7f89eba](https://gitlab.com/hestia-earth/hestia-glossary/commit/7f89eba9cbd6fe482f82c88e2885f2118f7cf0df)), closes [#686](https://gitlab.com/hestia-earth/hestia-glossary/issues/686)
* **operation:** add `Weeding, with spring-tine harrow` and `Sowing seeds, with seed cum fertiliser` ([36afe5d](https://gitlab.com/hestia-earth/hestia-glossary/commit/36afe5dda130e745dac7774b1926ba755a572fff)), closes [#686](https://gitlab.com/hestia-earth/hestia-glossary/issues/686)
* **pesticideBrandName:** add `Refine SG Herbicide` and `Mcpa Ester 600 Liquid Herbicide` ([9c7827f](https://gitlab.com/hestia-earth/hestia-glossary/commit/9c7827f5b758479b2a957b32b9725517e623c280)), closes [#675](https://gitlab.com/hestia-earth/hestia-glossary/issues/675)
* **pesticideBrandName:** add `Tilt`, `Gallant Super`, `Lontrel`, and `Roundup 360 SL` ([0aec439](https://gitlab.com/hestia-earth/hestia-glossary/commit/0aec4393a5a78169ae68f5b1d1b58c7afdb3e03c)), closes [#680](https://gitlab.com/hestia-earth/hestia-glossary/issues/680)
* **property:** add `Live animals per birth` and `Dead animals per birth` ([0e2cae3](https://gitlab.com/hestia-earth/hestia-glossary/commit/0e2cae3f629ef48899ee9ad2697c02da8ff2e87a)), closes [#678](https://gitlab.com/hestia-earth/hestia-glossary/issues/678)
* **region:** add irrigation water use lookup ([bc64cd9](https://gitlab.com/hestia-earth/hestia-glossary/commit/bc64cd9f708be9ce8e291dbd53e83d5db82d86f8))


### Bug Fixes

* **characterisedIndicator:** error in `model` mapping file ([53ea534](https://gitlab.com/hestia-earth/hestia-glossary/commit/53ea5346d3c55426fb81ed7d1ed03721bc4ea831))
* **emission:** remove `chemIdPlus` field ([de2f210](https://gitlab.com/hestia-earth/hestia-glossary/commit/de2f210dbb60cb51fea17c150b987c186d479ba2))
* **pesticideBrandName:** add missing `activeIngredient` value for Refine SG Herbicide ([7b56c65](https://gitlab.com/hestia-earth/hestia-glossary/commit/7b56c656f41ec0d59af10d467173653f42022f24)), closes [#685](https://gitlab.com/hestia-earth/hestia-glossary/issues/685)
* **property:** remove erroneous text from `feedipedia` column ([bb9e96e](https://gitlab.com/hestia-earth/hestia-glossary/commit/bb9e96e2c6f1aa84945460efd51d0368ca5628e8))

## [0.17.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.16.1...v0.17.0) (2023-04-04)


### ⚠ BREAKING CHANGES

* **liveAnimal:** Renamed all `liveAnimal` terms from plural to singular.
* **methodMeasurement:** `methodMeasurement.xlsx` has been moved to "Methods & Models" folder
* **methodEmissionResourceUse:** `methodEmissionResourceUse.xlsx` has been moved to "Methods & Models" folder
* **model:** `model.xlsx` has been moved to "Methods & Models" folder

### Features

* **experimentDesign:** create glossary ([36b2b8c](https://gitlab.com/hestia-earth/hestia-glossary/commit/36b2b8cb11d5f0237e3700afa22be294341d3716))
* **landUseManagement:** sets `% area` as units on additional terms ([395f7f1](https://gitlab.com/hestia-earth/hestia-glossary/commit/395f7f1d507c4d9d1b155ea6041bc0be13f5ea62))
* **liveAnimal:** apply homogenous naming rules and fix lookups ([72e4901](https://gitlab.com/hestia-earth/hestia-glossary/commit/72e49012e6bbd3140d916b5a36f809b93183ea55)), closes [#658](https://gitlab.com/hestia-earth/hestia-glossary/issues/658)
* **machinery:** add `Trailor`, `Combine harvester`, and `Agricultural machinery, unspecified` ([cfbf9bb](https://gitlab.com/hestia-earth/hestia-glossary/commit/cfbf9bb9fc5e361d8e75f3959d58ffa6c3937f37)), closes [#673](https://gitlab.com/hestia-earth/hestia-glossary/issues/673)
* move `Agroforesty` to `system` glossary and rename term `Agroforestry system` ([23920ac](https://gitlab.com/hestia-earth/hestia-glossary/commit/23920accb6e0ffdfbcd6c8b013d1114fad27566f))
* move `Silvopasture` to `system` glossary and rename `Silvopasture system` ([c2b06e5](https://gitlab.com/hestia-earth/hestia-glossary/commit/c2b06e5055e5ed6e16c09486a907c24c97dd68c2))
* move aquaculture system terms to `System` glossary ([84427c2](https://gitlab.com/hestia-earth/hestia-glossary/commit/84427c280b0530a2bcfd3470cf253b54e496c5a5))
* **operation:** add `Sowing seeds, with row planter` ([fbe9336](https://gitlab.com/hestia-earth/hestia-glossary/commit/fbe9336f1f5e9f5f36b80cf7284780ec7b00a9d5))
* **pesticideAI:** add `Florpyrauxifen-benzyl` ([5360de3](https://gitlab.com/hestia-earth/hestia-glossary/commit/5360de3d7370edcebc793941be7fc07d662bac19))
* **sampleDesign:** create glossary ([dbe014e](https://gitlab.com/hestia-earth/hestia-glossary/commit/dbe014ef3d3fbf0bc68d727338f9e2cab36d2881))
* **system:** move additional aquaculture terms to glossary ([c993a11](https://gitlab.com/hestia-earth/hestia-glossary/commit/c993a11cb9b2d0b3b0d34e5548c4aa0a43d22bec))


### Bug Fixes

* **animalProduct:** update `liveAnimal` lookup for new term ids ([b5460d5](https://gitlab.com/hestia-earth/hestia-glossary/commit/b5460d5bbc33b26334cf17f9a4794d5202212ef8)), closes [#667](https://gitlab.com/hestia-earth/hestia-glossary/issues/667)
* **inorganicFertiliser:** add `name` to all lookups and fix lookup errors ([4b04cee](https://gitlab.com/hestia-earth/hestia-glossary/commit/4b04cee796863448d4b9ea4b3f84178fa168f63b))
* move amino acid terms from `pesticideAI` to `feedFoodAdditive` ([e3c177c](https://gitlab.com/hestia-earth/hestia-glossary/commit/e3c177c2533ea11ade53f602d50589a9f1a641e5))
* **pesticideAI:** add missing space to `Distillates (petroleum), Catalytic reformer...` ([9f9fe1e](https://gitlab.com/hestia-earth/hestia-glossary/commit/9f9fe1e4eeca599c256cc60eff3603acd0c72a7d))
* **pesticideAI:** delete `Gas oils (petroleum), Straight-run` ([9c81ffe](https://gitlab.com/hestia-earth/hestia-glossary/commit/9c81ffef8fe71fd8eaaf24519276c8e42c6a3e60))
* **pesticideAI:** delete `Merge` which duplicates `Monosodium methanearsonate` ([56736a3](https://gitlab.com/hestia-earth/hestia-glossary/commit/56736a363b7944aeed274e3357067e25681c4a12))
* **pesticideAI:** delete `Spreader sticker` term ([bf45c5b](https://gitlab.com/hestia-earth/hestia-glossary/commit/bf45c5bbce94ed30c62abc8e83725b51a83773cc))
* **pesticideAI:** delete `Zeolites, NaA` as duplicated in `soilAmendment` ([8ea215d](https://gitlab.com/hestia-earth/hestia-glossary/commit/8ea215def94376c1aebaa84e19436a4cb366ce3c))
* **pesticideAI:** delete non-pesticide terms ([5798b0f](https://gitlab.com/hestia-earth/hestia-glossary/commit/5798b0f707018caa0ce627fd45e28aa357092586))
* **pesticideAI:** deletes terms not thought to be pesticides ([956466f](https://gitlab.com/hestia-earth/hestia-glossary/commit/956466fbb650c2e4000d2441132ebfe6b7c8f4ee))
* **pesticideAI:** rename `Ag zeolites` to `Zeolites (pesticide AI)` ([c91c10e](https://gitlab.com/hestia-earth/hestia-glossary/commit/c91c10e73ecba40208aedf3bb94f5a8aaf47388a))
* **pesticideAI:** rename `gasoline` to `gasoline (pesticide AI)` ([e774428](https://gitlab.com/hestia-earth/hestia-glossary/commit/e774428dfc8c37662120ef74c343a6df26e3fece))
* **pesticideAI:** rename `Kerosine (petroleum)` to `Kerosine (pesticide AI)` ([9d9fec3](https://gitlab.com/hestia-earth/hestia-glossary/commit/9d9fec351e9109f3353e5c2cf48f91c6a5481dfb))
* **pesticideAI:** rename `Paraffin waxes (Active Ingredient)` ([155ea10](https://gitlab.com/hestia-earth/hestia-glossary/commit/155ea10ea96de0055ff044f6acd35172431abe7c))
* **region:** fix double space in lookup header for `Edible offals of horses...` ([fd402ae](https://gitlab.com/hestia-earth/hestia-glossary/commit/fd402ae8d4012b77f3ad9d9e3c4fc41a53588931))


* **methodEmissionResourceUse:** move to "Methods & Models" folder ([b481ae7](https://gitlab.com/hestia-earth/hestia-glossary/commit/b481ae743103c2e0ed543d64ca585f838b3ffa83))
* **methodMeasurement:** move to "Methods & Models" folder ([bb4af9e](https://gitlab.com/hestia-earth/hestia-glossary/commit/bb4af9e6348b5539b871e7a0f6cafe2afb4973fd))
* **model:** move to "Methods & Models" folder ([72a5032](https://gitlab.com/hestia-earth/hestia-glossary/commit/72a50320236052a488ab611fd419a3ae6f7f34d4))

### [0.16.1](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.16.0...v0.16.1) (2023-03-15)

## [0.16.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.15.1...v0.16.0) (2023-03-15)


### ⚠ BREAKING CHANGES

* **property:** Rename `pregnancyRate` `pregnancyRatePerEstrousCycle`
* **property:** Rename `Lipid content` `Fat content`
* **property:** Rename `Deliveries per year` `Deliveries per Cycle` and
`Weaned animals per mother per year` `Weaned animals per Cycle`
* **operation:** Delete operation term `Bailing, with round bailer (straw)`.
* Change `units` from `%` to `% area` for: `cropEstablishment`, `cropResidueManagement`, `landUseManagement`, `system`, `tillage`, and `waterRegime` glossaries.

### Features

* **animalBreed:** create glossary ([0cb02a6](https://gitlab.com/hestia-earth/hestia-glossary/commit/0cb02a6a2a24f9afe863bb42d17091157da7be1f))
* **animalManagement:** add `Animal breed` term ([4f2d7d7](https://gitlab.com/hestia-earth/hestia-glossary/commit/4f2d7d73e9929c87820a5d5cf86b37d0eb0f216b))
* **aquacultureManagement:** set `% area` as `units` where no units specified ([7cd0fc7](https://gitlab.com/hestia-earth/hestia-glossary/commit/7cd0fc7a10427d6c98695212f9ec93f3078bc224))
* **biologicalControlAgent:** move `trichogrammaWasps` from `other` glossary ([19ec263](https://gitlab.com/hestia-earth/hestia-glossary/commit/19ec2631c4c69c79778863e3509acb39da7b2078))
* **crop:** add agrovoc links ([24fdb20](https://gitlab.com/hestia-earth/hestia-glossary/commit/24fdb20ef8fa5cc8888b34803c1ab9b49132d8ac)), relates to [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72)
* **crop:** add terms for `sea kale`, `rocket`, `wild rocket`, and `bitterleaf` ([e4ca3ff](https://gitlab.com/hestia-earth/hestia-glossary/commit/e4ca3ffde7a9aa70e90baf4ae1fe362489a2b7ba)), closes [#518](https://gitlab.com/hestia-earth/hestia-glossary/issues/518)
* **excretaManagement:** add `anaerobicDigesterUnspecified` to lookups ([8651ea4](https://gitlab.com/hestia-earth/hestia-glossary/commit/8651ea42f41793ce18aefbc3143ea34466f59abd))
* **feedFoodAdditive:** create glossary from `other` glossary ([1f282bf](https://gitlab.com/hestia-earth/hestia-glossary/commit/1f282bf9a14ef4cefeefe5f9146e50a565ed3d0b)), closes [#654](https://gitlab.com/hestia-earth/hestia-glossary/issues/654)
* **forage:** add `genericGrassPlant` and `genericGrassForage` terms ([4ccfe53](https://gitlab.com/hestia-earth/hestia-glossary/commit/4ccfe53391b6407344d5672de6b1b42a423338af))
* **fuel:** move `Mineral oil` from `other` glossary ([7c663d4](https://gitlab.com/hestia-earth/hestia-glossary/commit/7c663d45991a7fe6eeb7760e2642663b597b45bf))
* **liveAnimal:** add `averagePregnancyRate` lookup ([7edde49](https://gitlab.com/hestia-earth/hestia-glossary/commit/7edde4929b4939f141139af2128c17232f882540)), closes [#646](https://gitlab.com/hestia-earth/hestia-glossary/issues/646)
* **liveAnimal:** add `ipcc2019AnimalTypeGrouping` lookup ([37ee1ed](https://gitlab.com/hestia-earth/hestia-glossary/commit/37ee1ed97dd188eca137bc281bb9b91f2921b10e))
* **liveAnimal:** add `kgDayMilkForFeedingOffspring` lookup ([a7448c2](https://gitlab.com/hestia-earth/hestia-glossary/commit/a7448c2d0140eed1a5f0fcf29b4df1fb53c300ae)), closes [#647](https://gitlab.com/hestia-earth/hestia-glossary/issues/647)
* **liveAnimal:** add dry and lactating ewes and nannies ([3e8d80e](https://gitlab.com/hestia-earth/hestia-glossary/commit/3e8d80e9cad608f018ad00b28ac27193659d0b66)), closes [#647](https://gitlab.com/hestia-earth/hestia-glossary/issues/647)
* **pesticideAI:** add `Alcohol ethoxylates, unspecified` and `Fatty acids, tall oil` ([0026072](https://gitlab.com/hestia-earth/hestia-glossary/commit/0026072bd4de6bb52883a089740c8a5132632d0d)), closes [#635](https://gitlab.com/hestia-earth/hestia-glossary/issues/635)
* **pesticideBrandName:** move `Adigor` and `Activator 90` from pesticideAI ([df9441e](https://gitlab.com/hestia-earth/hestia-glossary/commit/df9441e261c0faf443a85ce1230f03fc1611530f)), closes [#635](https://gitlab.com/hestia-earth/hestia-glossary/issues/635)
* **property:** add `Animals delivered per Cycle` ([60a04c5](https://gitlab.com/hestia-earth/hestia-glossary/commit/60a04c5bec52d31f5a8b9a54be4e3ab63efce927))
* **property:** add `dataBasisFeedipedia` lookup ([e5179b0](https://gitlab.com/hestia-earth/hestia-glossary/commit/e5179b038614b5fb4ff896132ee67cc0caa4f8dc))
* **property:** add `Hours worked per day` ([71df146](https://gitlab.com/hestia-earth/hestia-glossary/commit/71df146e96afe9ffd225d003058c7c15d3f49f3b))
* **property:** add weight at weaning, one year, maturity and slaughter ([3b528a7](https://gitlab.com/hestia-earth/hestia-glossary/commit/3b528a71fe2641aff37ef5adf6f06be8f5517a97))
* **property:** delete terms replaced by the new schema field `fate` ([d49d818](https://gitlab.com/hestia-earth/hestia-glossary/commit/d49d81878c9b79a17ef458f1cc644ef19527e0d7)), closes [#639](https://gitlab.com/hestia-earth/hestia-glossary/issues/639)
* **property:** rename `Lipid content` `Fat content`, add `Total lipid content`, fix definitions ([17b55cd](https://gitlab.com/hestia-earth/hestia-glossary/commit/17b55cdce21ee0f28342778a9145e0fedd8ad9b2))
* **property:** rename `pregnancyRate` and add `pregnancyRateTotal` ([b661e33](https://gitlab.com/hestia-earth/hestia-glossary/commit/b661e3337af12653b685f9f882443b7ec07eff3e)), closes [#646](https://gitlab.com/hestia-earth/hestia-glossary/issues/646)
* **property:** replace `per year` with `per Cycle` in term names ([70c294a](https://gitlab.com/hestia-earth/hestia-glossary/commit/70c294a3e7f011ca1bb2cf24aa9d5bf8e19722b6))
* **region:** add regional/global averages for `price` ([48fc63a](https://gitlab.com/hestia-earth/hestia-glossary/commit/48fc63af930be0ca378fa8430fdd9e7751b2236b))
* where terms units refer to `% area`, change units from `%` to `% area` ([ffc0b52](https://gitlab.com/hestia-earth/hestia-glossary/commit/ffc0b52dd7dff66bacddc5b3072e39233cd9c8b9))


### Bug Fixes

* **animalManagement:** error in `defaultProperties` numbering ([cc0fc1b](https://gitlab.com/hestia-earth/hestia-glossary/commit/cc0fc1b75d5b6c399e6f07f89319e94c5784def5))
* **animalProduct:** camelCase in `MJKgEVwoolNetEnergyWoolIPCC2019` lookup ([4358394](https://gitlab.com/hestia-earth/hestia-glossary/commit/43583941afb1b553511a328db9c160e1a7f51228))
* **animalProduct:** typo in lookup sources ([cb2aec7](https://gitlab.com/hestia-earth/hestia-glossary/commit/cb2aec7ff78edf26c5d6daf59417c12dc639144d))
* **liveAnimal:** correct camelCase for `mJDayKgCfiNetEnergyMaintenanceIpcc2019` lookup ([2259cc6](https://gitlab.com/hestia-earth/hestia-glossary/commit/2259cc6b9ab0addca67b78f77a26e33aebae670b))
* **liveAnimal:** typo in `excretaBuffaloKgMass` and `excretaBuffaloKgN` lookup values ([464606a](https://gitlab.com/hestia-earth/hestia-glossary/commit/464606a507358a0f1a0c0b09c4a42b2d8940404f))
* missing terms in lookup tables ([4086180](https://gitlab.com/hestia-earth/hestia-glossary/commit/40861809f83234f8c391a87e69b6f73ae4d30c85))
* **operation:** delete term `Bailing, with round bailer (straw)` with units ha ([eb836f1](https://gitlab.com/hestia-earth/hestia-glossary/commit/eb836f1e1351c52bfbc2bf5b8b955c74def57e44))
* **pesticideBrandName:** remove unused or erroneous terms ([90302a1](https://gitlab.com/hestia-earth/hestia-glossary/commit/90302a1a64234506958f84d55b9b19c705438e9e))
* **property:** change unit of `Deliveries per year` from  `% ` to  `number ` ([246969c](https://gitlab.com/hestia-earth/hestia-glossary/commit/246969c8a4a0b0d873305c93c70aa1347feaced6))
* remove additional terms in lookup tables ([d33b7af](https://gitlab.com/hestia-earth/hestia-glossary/commit/d33b7afbc9a2dc590378b22492f847f9b672983f))
* **system:** delete `cassowary` values from `liveAnimal-activityCoefficient-ipcc2019-lookup` ([17dc96b](https://gitlab.com/hestia-earth/hestia-glossary/commit/17dc96b3e18f03421d80a616a598b49e83abdd73))

### [0.15.1](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.15.0...v0.15.1) (2023-02-28)


### Features

* **animalManagement:** rename `mortality` and `culling rate` and delete unnecessary terms ([2481d31](https://gitlab.com/hestia-earth/hestia-glossary/commit/2481d313cc066f1b0f3756a33219464eeeed3873))
* **animalProduct:** specify `units` to match functional unit ([76a244b](https://gitlab.com/hestia-earth/hestia-glossary/commit/76a244b0071ad21b7f7e5831487ed5313e01766b))
* **emission:** add `inputTermTypesAllowed` and set for excreta emissions ([524924a](https://gitlab.com/hestia-earth/hestia-glossary/commit/524924a62ad611fb68a1c4d2aaafce6151f55a07))
* **property:** add `age` and new metrics for animal pregnancy ([39bde2f](https://gitlab.com/hestia-earth/hestia-glossary/commit/39bde2fd51f72459baa4d6189655415e58d0a959)), closes [#633](https://gitlab.com/hestia-earth/hestia-glossary/issues/633)
* **property:** move terms from `animalManagement` to properties ([279e2b8](https://gitlab.com/hestia-earth/hestia-glossary/commit/279e2b8c2c4e8e2b27bc578c9c2b7f2951ebb1dd)), closes [#632](https://gitlab.com/hestia-earth/hestia-glossary/issues/632)


### Bug Fixes

* **crop:** fix typo in ginseng terms ([f981219](https://gitlab.com/hestia-earth/hestia-glossary/commit/f98121952ed343c3e5642af3db31a2d17f17faeb))
* **emission:** add all liveAnimals to `CH4... enteric` `productTermIdsAllowed` lookup ([a1a23bb](https://gitlab.com/hestia-earth/hestia-glossary/commit/a1a23bb7c32e41b5e1159728d87711fc9acadc94))
* **emission:** add missing allow product ids to `CH4, to air, enteric fermentation` ([32ea806](https://gitlab.com/hestia-earth/hestia-glossary/commit/32ea806332b8b94ead7e7ce930222495a31d720e)), closes [#622](https://gitlab.com/hestia-earth/hestia-glossary/issues/622)
* **inorganicFertiliser:** replace `P205` with `P2O5` in `mustInlcudeId` lookups ([b7ce144](https://gitlab.com/hestia-earth/hestia-glossary/commit/b7ce144f8d1dec274a5d1980db33f2ef6d8bb6fb)), closes [#640](https://gitlab.com/hestia-earth/hestia-glossary/issues/640)
* **property:** update broken feedipedia links for `Fructose content` and `Starch content` ([a74934c](https://gitlab.com/hestia-earth/hestia-glossary/commit/a74934c3897453b11de238eb9ac686c360bb0343))
* **region:** convert `0.1g/An` products to `hg/An` for consistency ([b7686f8](https://gitlab.com/hestia-earth/hestia-glossary/commit/b7686f82fe48d3f5cc41e8e7d564a9a073a2e418))

## [0.15.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.14.2...v0.15.0) (2023-02-27)


### ⚠ BREAKING CHANGES

* **liveAnimal:** renamed `percentageSDMethaneConversionFactorEntericFermentationIPCC2019`
into `percentageYmMethaneConversionFactorEntericFermentationIPCC2019-sd`
* **crop:** renamed `Seed_Output_kg_avg` to `seedPerKgYield`
* **crop:** renamed `Seed_Output_kg_sd` to `seedPerKgYield-sd`

### Features

* **antibiotic:** add `Apramycin sulphate` and delete `term.chemIdPlus` field ([b4cdbe4](https://gitlab.com/hestia-earth/hestia-glossary/commit/b4cdbe461f3aea1572ddd0b34410af99158627c8)), closes [#621](https://gitlab.com/hestia-earth/hestia-glossary/issues/621)
* **crop:** add `Mustard, oil`, `Mustard, meal`, and cotton meal terms ([eb0dc15](https://gitlab.com/hestia-earth/hestia-glossary/commit/eb0dc1510ca5bfad7cf663c25b0186c861639746))
* **crop:** add new terms for galanga, gingseng, caraway, fenugreek, goji, rose, strawberry ([b1f1502](https://gitlab.com/hestia-earth/hestia-glossary/commit/b1f15027db5b651e39d79a14b1f94fa790c4af63)), closes [#518](https://gitlab.com/hestia-earth/hestia-glossary/issues/518)
* **crop:** add straw and residue terms for several crops ([68d87ff](https://gitlab.com/hestia-earth/hestia-glossary/commit/68d87ff50beb6c8b01767b7db20a0edd4f7b44f6))
* **liveAnimal:** change all `units` to `number` ([a81e22d](https://gitlab.com/hestia-earth/hestia-glossary/commit/a81e22dec0effe87d4bb9ac5f61c6f87693059e9)), closes [#631](https://gitlab.com/hestia-earth/hestia-glossary/issues/631)
* **liveAnimal:** move `percentageYm...sd` lookup into `sd` field of `percentageYm...` ([6b7cd40](https://gitlab.com/hestia-earth/hestia-glossary/commit/6b7cd4077364656cf8740d80f876ef9ad24f8916))
* **operation:** add `Attached labour` ([4dbf578](https://gitlab.com/hestia-earth/hestia-glossary/commit/4dbf578e04409eb6f5c49ebaa73eb1e3796325d3))
* **operation:** add `labor` as `synonym` to `labour` terms. ([8fc9a55](https://gitlab.com/hestia-earth/hestia-glossary/commit/8fc9a5584e86f96dcb8bb8051a31cce0bed531d9))
* **operation:** add `oil pressing, machine unspecified` and sieving terms ([ab18dc9](https://gitlab.com/hestia-earth/hestia-glossary/commit/ab18dc90bf3b17f922adad100506aecd73eaf5f2))
* **operation:** add `raising beds, machine unspecified` and `raising beds, with bedder` ([d478512](https://gitlab.com/hestia-earth/hestia-glossary/commit/d478512db92714c7b273d8ef7f2a662d42ae7270))
* **operation:** add `sheep dipping` ([2eb4317](https://gitlab.com/hestia-earth/hestia-glossary/commit/2eb4317f9e648caef023a0488557cf8ed77474ed))
* **operation:** add polytunnels and greenhouses related terms and `Alcoholic distillation` ([4ee8312](https://gitlab.com/hestia-earth/hestia-glossary/commit/4ee83121421bb4bc5343c02ec12b146aad42f9c7)), closes [#624](https://gitlab.com/hestia-earth/hestia-glossary/issues/624)
* **operation:** add transplanting terms and `placing plant supports` ([8d9c6f7](https://gitlab.com/hestia-earth/hestia-glossary/commit/8d9c6f723f2bc6f9a26b2f5a017fda843659e1b0))
* **operation:** delete `Grain drying (kg grain)` and rename `Grain drying (hours)` `Grain drying, machine unspecified` ([8077437](https://gitlab.com/hestia-earth/hestia-glossary/commit/8077437d7e7a850622da20fe03c47e6ee7458ee5))
* **other:** add `inputGroup` lookup ([9afaf06](https://gitlab.com/hestia-earth/hestia-glossary/commit/9afaf066e4a7a0d9db8f3f8ed8a05495364f504a))
* **other:** add `Versatic acid derivatives` ([dc2c4f4](https://gitlab.com/hestia-earth/hestia-glossary/commit/dc2c4f4a2ca73f8e055dae649712e9e99c5425e6))
* **pesticideAI:** add `Bipyridine compounds, unspecified` and `Diphenyl ether compounds, unspecified` ([cf735af](https://gitlab.com/hestia-earth/hestia-glossary/commit/cf735af0f88d8382e5cbe0ef8e3e5b875fb9e196))
* **pesticideAI:** add `dithiocarbate compounds, unspecified` and `alkyl sulphate compounds, unspecified` ([94e5142](https://gitlab.com/hestia-earth/hestia-glossary/commit/94e5142201b4d8dd2715f4cf4795f1e41489e77e))
* **pesticideAI:** rename `Silwet 800` `Polyether modified polysiloxane` and add `Soybean oil, ethoxylated` ([e32bc20](https://gitlab.com/hestia-earth/hestia-glossary/commit/e32bc208b8f702e355b870de79703ac40a412c61)), closes [#625](https://gitlab.com/hestia-earth/hestia-glossary/issues/625)
* **pesticideBrandName:** move `Enhance (herbicide adjuvant)` from pesticideAI and add `Silwet L-77` ([0e09e3f](https://gitlab.com/hestia-earth/hestia-glossary/commit/0e09e3ffb4abd1b11c1c0b3b30c699683474ab33)), closes [#625](https://gitlab.com/hestia-earth/hestia-glossary/issues/625)
* **processedFood:** add `Agri-food processing waste` ([f9b684f](https://gitlab.com/hestia-earth/hestia-glossary/commit/f9b684fb90ef8362f2a08e166b9c594f49d97ef1)), closes [#544](https://gitlab.com/hestia-earth/hestia-glossary/issues/544)
* **processedFood:** add `Tea oil camellia, oil` and `Tea oil camellia, meal` ([9ef68aa](https://gitlab.com/hestia-earth/hestia-glossary/commit/9ef68aaf5a67addaf65910d52bd3e710ce8e9feb)), closes [#568](https://gitlab.com/hestia-earth/hestia-glossary/issues/568)
* **processedFood:** add sugar terms ([9ac7e21](https://gitlab.com/hestia-earth/hestia-glossary/commit/9ac7e2177703d628f12a564810b8e6b984eb4001)), closes [#606](https://gitlab.com/hestia-earth/hestia-glossary/issues/606)
* **processedFood:** move processed `animalProduct`` terms to `processedFood` glossary ([1c3336e](https://gitlab.com/hestia-earth/hestia-glossary/commit/1c3336e0b667b6c1c17d60683c6125f38dc8337c))
* **soilAmendment:** add `inputGroup` lookup ([f3c34b2](https://gitlab.com/hestia-earth/hestia-glossary/commit/f3c34b2f8ef9aa3eacb4f5708d5b03bf20462d75))
* **soilAmendment:** add `name` to all lookups ([aae7c97](https://gitlab.com/hestia-earth/hestia-glossary/commit/aae7c97c0840864fc8efaafed69b7c1b86c3ea22))
* **soilAmendment:** add `Sodium oxide` ([dd4ea52](https://gitlab.com/hestia-earth/hestia-glossary/commit/dd4ea525370ed6dce6431511360f2ebcd22eb11f))
* **system:** add terms to describe animals feeding situation ([3ae4529](https://gitlab.com/hestia-earth/hestia-glossary/commit/3ae4529e8a43639c7196ba432be18a24ed07042d)), closes [#582](https://gitlab.com/hestia-earth/hestia-glossary/issues/582)
* **system:** create `system-liveAnimal-activityCoefficient-ipcc2019-lookup` ([fbe1146](https://gitlab.com/hestia-earth/hestia-glossary/commit/fbe1146a7a8ccfe88c16d51629e2cd6e5dee411a)), closes [#582](https://gitlab.com/hestia-earth/hestia-glossary/issues/582)


### Bug Fixes

* **animalManagement:** add `lookups.2.dataState` ([a8676d6](https://gitlab.com/hestia-earth/hestia-glossary/commit/a8676d68a84f6e63d642d1a19d47f5d833335885))
* **crop:** move `Seed_Output_kg_sd` lookup into `sd` field and rename lookup ([bbb3dee](https://gitlab.com/hestia-earth/hestia-glossary/commit/bbb3deeb6a676cd9bc571db09bf9de92f7059770))
* **fuel:** error in heating value `defaultProperties` of `diesel` ([824378b](https://gitlab.com/hestia-earth/hestia-glossary/commit/824378bc16d5048c780aa00c25fbaf5d20552822))
* **operation:** set `fuelUse` for `labour` and animal terms to `not required` ([22c6291](https://gitlab.com/hestia-earth/hestia-glossary/commit/22c629174800e2df98035597e29d069711b1af3c))
* **other:** delete term.chemIdPlus field as links do not work ([67ce9f2](https://gitlab.com/hestia-earth/hestia-glossary/commit/67ce9f2c15f1b25dad5db33839b1e6713ed703ce)), closes [#621](https://gitlab.com/hestia-earth/hestia-glossary/issues/621)
* **other:** set `inputGroup` lookup to `None` instead of `-` ([f05eb16](https://gitlab.com/hestia-earth/hestia-glossary/commit/f05eb1697d35860945a0e8f35d9c0a097a97d7a6))
* **pesticideAI:** add `lookups.5.dataState` ([4206e90](https://gitlab.com/hestia-earth/hestia-glossary/commit/4206e907580641ba523cdbe3bd6388be0a19b237))
* **pesticideAI:** delete `term.chemIdPlus` field as the links do not work ([2e3534b](https://gitlab.com/hestia-earth/hestia-glossary/commit/2e3534b88f8a7c067f92706a100cf5499a6dc42e)), closes [#621](https://gitlab.com/hestia-earth/hestia-glossary/issues/621)
* **pesticideBrandName:** update `active ingredient` of `Xentari WDG` and add synonym for `Serenade` ([93bceee](https://gitlab.com/hestia-earth/hestia-glossary/commit/93bceeed6bd65300b117bf6ec38ecc8d1f5d3fd1))
* **soilAmendment:** errors in lookups for `Sodium oxide` ([7abcb00](https://gitlab.com/hestia-earth/hestia-glossary/commit/7abcb004455154b3540aeb4c7eb4c7df86e7300e))

### [0.14.2](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.14.1...v0.14.2) (2023-02-14)


### Features

* **animalManagement:** add `EVmilk` lookup to milk yield per sheep and goat terms ([f67c2a4](https://gitlab.com/hestia-earth/hestia-glossary/commit/f67c2a489ed0fe944941ba5fc92404fba4d8dacd)), closes [#581](https://gitlab.com/hestia-earth/hestia-glossary/issues/581)
* **animalManagement:** add milk yield per sheep, goat, camel, buffalo ([ed5c6eb](https://gitlab.com/hestia-earth/hestia-glossary/commit/ed5c6eb8e1a26e95623719ed5702b66668d287b8)), closes [#581](https://gitlab.com/hestia-earth/hestia-glossary/issues/581)
* **animalProduct:** add `lipidContent` and `trueProteinContent` lookups for milk terms ([2c9bf59](https://gitlab.com/hestia-earth/hestia-glossary/commit/2c9bf59484c0e500fe1def3bfa32e1b0415913e7)), closes [#580](https://gitlab.com/hestia-earth/hestia-glossary/issues/580)
* **animalProduct:** rename milk terms and add default properties and definitions ([fdc99cc](https://gitlab.com/hestia-earth/hestia-glossary/commit/fdc99cc4bfe53fc62767504826bba7087ab031e5)), closes [#580](https://gitlab.com/hestia-earth/hestia-glossary/issues/580)
* **characterisedIndicator:** add mid to endpoint CFs for `GWP100` and `GWP20` under `ipcc2021` ([5f6c9b9](https://gitlab.com/hestia-earth/hestia-glossary/commit/5f6c9b91d6f9a8df130f7adc15201faff4066bd5)), closes [#594](https://gitlab.com/hestia-earth/hestia-glossary/issues/594)
* **crop:** add `Pulse, straw` ([cf7ec93](https://gitlab.com/hestia-earth/hestia-glossary/commit/cf7ec934d9c0e71558f6841271f838fa8ec8cb88)), closes [#571](https://gitlab.com/hestia-earth/hestia-glossary/issues/571)
* **crop:** add `Rice, bran oil` and `Rice, bran (de-oiled)` ([3936ed0](https://gitlab.com/hestia-earth/hestia-glossary/commit/3936ed00194eba997e411f244ad3d368bde56786)), closes [#567](https://gitlab.com/hestia-earth/hestia-glossary/issues/567)
* **crop:** add `Tea oil camellia tree` and `Tea oil camellia, seed` ([82ef095](https://gitlab.com/hestia-earth/hestia-glossary/commit/82ef09599a6f380e4ed5b484dc0897459857e640)), closes [#568](https://gitlab.com/hestia-earth/hestia-glossary/issues/568)
* **crop:** add new terms for Achiote, Araza and Peach palm ([63e034e](https://gitlab.com/hestia-earth/hestia-glossary/commit/63e034e58960f333735a844e448d7a7f6d69fca0)), closes [#589](https://gitlab.com/hestia-earth/hestia-glossary/issues/589) [#601](https://gitlab.com/hestia-earth/hestia-glossary/issues/601)
* **landUseManagement:** add replacement and mortality rate for orchard and shade trees ([ba6a0dd](https://gitlab.com/hestia-earth/hestia-glossary/commit/ba6a0dde9536e8cbf429362aa935baaa382442d7)), closes [#597](https://gitlab.com/hestia-earth/hestia-glossary/issues/597)
* **liveAnimal:** add `Insect (larvae)` and `Mealworm (larvae)` ([baf84ac](https://gitlab.com/hestia-earth/hestia-glossary/commit/baf84ac0a99fc00f4d04434e593d17ab5ce4ace4)), closes [#592](https://gitlab.com/hestia-earth/hestia-glossary/issues/592)
* **model:** add `Haversine Formula` ([1a07d7e](https://gitlab.com/hestia-earth/hestia-glossary/commit/1a07d7ee6feb55a60539577c301d62d39382fade))
* **operation:** add grain processing operations ([f31b709](https://gitlab.com/hestia-earth/hestia-glossary/commit/f31b709eeab3b9d66cf13ab9b0a09a3e59e5ac44)), closes [#616](https://gitlab.com/hestia-earth/hestia-glossary/issues/616)
* **operation:** add terms for weeding, incorporating fertiliser, thinning and heaping manure ([11ff219](https://gitlab.com/hestia-earth/hestia-glossary/commit/11ff21983122d0049a2f21edf7f4af71ebabf07e))
* **processedFood:** add plant and insect milks ([a35eb4a](https://gitlab.com/hestia-earth/hestia-glossary/commit/a35eb4a87a886027bc110d74cb3ea45182da4414)), closes [#595](https://gitlab.com/hestia-earth/hestia-glossary/issues/595)
* **processedFood:** add single cell protein terms ([0599949](https://gitlab.com/hestia-earth/hestia-glossary/commit/0599949ef070050a3e5ba35900b83a23fb4f6fa8)), closes [#600](https://gitlab.com/hestia-earth/hestia-glossary/issues/600)
* **property:** add `dietary fibre content` ([f5614c8](https://gitlab.com/hestia-earth/hestia-glossary/commit/f5614c84e9989e563329154bd7ff8140ba2259de))
* **property:** add `percentage of product saved for seed` ([4a48e75](https://gitlab.com/hestia-earth/hestia-glossary/commit/4a48e75c650e4a4546871b29d3fcad9ab7a4bb61)), closes [#602](https://gitlab.com/hestia-earth/hestia-glossary/issues/602)
* **property:** add `true protein content`, `protein N content` and `non-protein N content` ([094789f](https://gitlab.com/hestia-earth/hestia-glossary/commit/094789f60e94b554b9c05c31439d5a36778ce53c)), closes [#580](https://gitlab.com/hestia-earth/hestia-glossary/issues/580) [#581](https://gitlab.com/hestia-earth/hestia-glossary/issues/581)
* **property:** add additional carbohydrate content terms ([30e0e45](https://gitlab.com/hestia-earth/hestia-glossary/commit/30e0e45bcd108a8005562ff4c8e5e1206812f5c5)), closes [#603](https://gitlab.com/hestia-earth/hestia-glossary/issues/603)
* **property:** add additional lipid content terms ([cbdda59](https://gitlab.com/hestia-earth/hestia-glossary/commit/cbdda5985d9f9534d80277d7869f509f19df78df))
* **property:** rename `NDF content` and `ADF content` and improve definitions ([faab7d3](https://gitlab.com/hestia-earth/hestia-glossary/commit/faab7d36cb4ae270d926f47b909187e035b2efa1))
* **region:** add `Global` as `synonym` for `World` ([40a3831](https://gitlab.com/hestia-earth/hestia-glossary/commit/40a38317998482cb623baf0b0f64f30a28ba5c22))
* **region:** add defaults for all land irrigation ([0995285](https://gitlab.com/hestia-earth/hestia-glossary/commit/09952855d1813f1856ade8c2fe40d9f438f9ee9e))
* **region:** add pesticide usage from FAO ([17eabad](https://gitlab.com/hestia-earth/hestia-glossary/commit/17eabada5c3cfb1212f3944c0b77228d532c54e9))
* **region:** add regional data for organic share ([fcc56b4](https://gitlab.com/hestia-earth/hestia-glossary/commit/fcc56b4bb2478cd96c4c615adaeb7822713d9d8c))
* **region:** add regional values on crop yield ([3214020](https://gitlab.com/hestia-earth/hestia-glossary/commit/32140207cfa3a1dbd7aef1c1eb7096051977272f))
* **region:** add regional values on pesticides and fertilisers usage ([0e8a3dd](https://gitlab.com/hestia-earth/hestia-glossary/commit/0e8a3dd92e711f48b3e14b5097fdb34583afb82e))
* **waterRegime:** add `Unknown pre-season water regime` and various synonyms ([1b256a5](https://gitlab.com/hestia-earth/hestia-glossary/commit/1b256a5a5fae34c7ae369d27e72d838d329a90e8)), closes [#570](https://gitlab.com/hestia-earth/hestia-glossary/issues/570)


### Bug Fixes

* **liveAnimal:** split `sd` lookup into new lookup column and fix typo in `ym` lookup ([0dcdff9](https://gitlab.com/hestia-earth/hestia-glossary/commit/0dcdff990df00a5851ed8d7c779d60c1d512e83d)), closes [#612](https://gitlab.com/hestia-earth/hestia-glossary/issues/612)
* **processedFood:** delete `pepperoni pizza` ([342cbdf](https://gitlab.com/hestia-earth/hestia-glossary/commit/342cbdf9e2b9a74f5ffb5be36d0f0db3875ef5a8))
* **region:** rename lookup on fertilisers usage for consistency ([6a9cca1](https://gitlab.com/hestia-earth/hestia-glossary/commit/6a9cca191dde13550941ee59e572752806c6d864))

### [0.14.1](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.14.0...v0.14.1) (2023-01-30)


### Features

* **characterisedIndicator:** make `IPCC 2021` the default model for GWP100 ([7cc87de](https://gitlab.com/hestia-earth/hestia-glossary/commit/7cc87de4e9615448bd8c2cb3560f764ba0b3666b))
* **crop:** add `Lagos spinach, seed` ([5675f2c](https://gitlab.com/hestia-earth/hestia-glossary/commit/5675f2c06e99764d2088f5d96a35bfb32962a00f))
* **crop:** add rooting depth to crops of the Amaranthaceae family ([0e05ea1](https://gitlab.com/hestia-earth/hestia-glossary/commit/0e05ea1e639425d56a7ad13a1c461f86d3f36544))
* **measurement:** add plant available calcium and sodium terms ([416bbad](https://gitlab.com/hestia-earth/hestia-glossary/commit/416bbadf77f1ad5ab5989c6741dd7b9c756a6535))
* **measurement:** rename `exchangeable potassium` `plant available potassium` for consistency ([ade78dc](https://gitlab.com/hestia-earth/hestia-glossary/commit/ade78dc3b0ba052f9f2dbb11659718316bb1bd45))
* **methodMeasurement:** rename `NH4OAc method, exchangeable potassium` `Ammonium acetate method` ([5b1a79d](https://gitlab.com/hestia-earth/hestia-glossary/commit/5b1a79d4b6351a9a68cb3d72b9b67517ea69f1a9))
* **model:** add `Emission not relevant` ([bfb0bc7](https://gitlab.com/hestia-earth/hestia-glossary/commit/bfb0bc7596b09801e81b7470d86737bdeb71bb2b)), closes [#566](https://gitlab.com/hestia-earth/hestia-glossary/issues/566)
* **operation:** add `hoeing, by hand` and `earthing up, by hand` ([6969a3f](https://gitlab.com/hestia-earth/hestia-glossary/commit/6969a3f285530e7d2f32b9fc273f6b23a02e5771))
* **property:** add more plant available minerals ([159ea72](https://gitlab.com/hestia-earth/hestia-glossary/commit/159ea7213ec6ae2a9c83805efb1994acde9645c8))
* **tillage:** add `ridge tillage` ([3cb935d](https://gitlab.com/hestia-earth/hestia-glossary/commit/3cb935db9cd0cbf024343649700e7c0fcedd1c59))


### Bug Fixes

* **animalProduct:** update `animalProductGroupingFAO` for egg terms ([b8d299c](https://gitlab.com/hestia-earth/hestia-glossary/commit/b8d299cb9be9155db8b0935b069d422f5f6b2637))

## [0.14.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.13.2...v0.14.0) (2023-01-26)


### ⚠ BREAKING CHANGES

* **region:** lookup `region-crop-faostatArea` renamed in `region-faostatCroplandArea`

### Features

* **animalProduct:** add lookup to store EVwool value ([eaeed88](https://gitlab.com/hestia-earth/hestia-glossary/commit/eaeed88e5f24dfebb404ecd25858f7e170da834b)), closes [#584](https://gitlab.com/hestia-earth/hestia-glossary/issues/584)
* **animalProduct:** rename `Wool, greasy`, add `Wool, goat (greasy)` ([01ca1a6](https://gitlab.com/hestia-earth/hestia-glossary/commit/01ca1a685c954fd12343254c6999442f19485cd7))
* **characterisedIndicator:** add `IPCC 2021` to emission lookup ([408cb6a](https://gitlab.com/hestia-earth/hestia-glossary/commit/408cb6a3dd0c7e321261487813d6d324a3477ee1)), closes [#585](https://gitlab.com/hestia-earth/hestia-glossary/issues/585)
* **crop:** add `grazedPastureGrassInputId` lookup ([331801e](https://gitlab.com/hestia-earth/hestia-glossary/commit/331801e30ea48ff34c2ccd2d34501fdadcb86ee0)), closes [#541](https://gitlab.com/hestia-earth/hestia-glossary/issues/541)
* **crop:** add missing crops in the Amaranthaceae family ([39f1dfc](https://gitlab.com/hestia-earth/hestia-glossary/commit/39f1dfce057511b61f8a90ed3d7b742c394e004f)), closes [#572](https://gitlab.com/hestia-earth/hestia-glossary/issues/572)
* **crop:** add terms for runner bean ([5576e40](https://gitlab.com/hestia-earth/hestia-glossary/commit/5576e403e710ff9e9552a26e959b9728ed87d5e1)), closes [#575](https://gitlab.com/hestia-earth/hestia-glossary/issues/575)
* **emission:** add `IPCC 2021` `GWP 100` factors as lookup ([1e04afc](https://gitlab.com/hestia-earth/hestia-glossary/commit/1e04afc0d73c54962cb709378600dbb0895153c2)), closes [#585](https://gitlab.com/hestia-earth/hestia-glossary/issues/585)
* **emission:** add synonyms to organic carbon stock terms ([4fe0577](https://gitlab.com/hestia-earth/hestia-glossary/commit/4fe05770bb26fab37882b0e0b5209e0551935b8c))
* **forage:** add `possibleCoverCrop` lookup ([9455f84](https://gitlab.com/hestia-earth/hestia-glossary/commit/9455f848c6e5eac5216dca752b6381b2fd3e1faa))
* **forage:** set `grazedPastureGrassInputId` lookup to not required for trees ([c495e9d](https://gitlab.com/hestia-earth/hestia-glossary/commit/c495e9dc5d98f9ec0736c4a4aeb8a4027d758ffa)), closes [#541](https://gitlab.com/hestia-earth/hestia-glossary/issues/541)
* **fuel:** add `bioethanol` ([21bb6cd](https://gitlab.com/hestia-earth/hestia-glossary/commit/21bb6cde4bceff8a6d0a562e527db38f528aab70)), closes [#591](https://gitlab.com/hestia-earth/hestia-glossary/issues/591)
* **liveAnimal:** add lookups to store parameters for gross energy needs calculations ([97560d5](https://gitlab.com/hestia-earth/hestia-glossary/commit/97560d5b21559375514e3b9da395d4c21835c372)), closes [#583](https://gitlab.com/hestia-earth/hestia-glossary/issues/583)
* **liveAnimal:** add new terms to describe sheep, goats and buffaloes ([82927fc](https://gitlab.com/hestia-earth/hestia-glossary/commit/82927fc5b834318403a5a51a4abcba17a146634f)), closes [#583](https://gitlab.com/hestia-earth/hestia-glossary/issues/583)
* **liveAnimal:** integrate `ipcc2019Tier2Ch4 lookup` ([ce05003](https://gitlab.com/hestia-earth/hestia-glossary/commit/ce05003951b796da4196953bc24a7819b6e86d47))
* **methodMeasurement:** add `Organic elemental analysis` ([1781f99](https://gitlab.com/hestia-earth/hestia-glossary/commit/1781f991fbaa829aa6678fde6eef865f88954693))
* **model:** add `IPCC (2021)` ([7ef61d9](https://gitlab.com/hestia-earth/hestia-glossary/commit/7ef61d9a40d3dc44c97d21043151823f3675c322))
* **operation:** add `Coating seeds`, `Encrusting seeds`, `Pelleting seeds` ([56b8a56](https://gitlab.com/hestia-earth/hestia-glossary/commit/56b8a56306fd9c90b54b6b9675e026a301f8cd59))
* **operation:** add `Fertilising, broadcasting` ([fac0a7e](https://gitlab.com/hestia-earth/hestia-glossary/commit/fac0a7e74b8d3cfd5daa6d3a02d6a04937ac4d0d)), closes [#589](https://gitlab.com/hestia-earth/hestia-glossary/issues/589)
* **operation:** add `Spontaneous fermentation` and `Inoculated fermentation` ([27ec44a](https://gitlab.com/hestia-earth/hestia-glossary/commit/27ec44a4f4b75656ae53dd3424cc91d348a596af))
* **operation:** add `Treating seeds, with pesticide` ([9dae242](https://gitlab.com/hestia-earth/hestia-glossary/commit/9dae242138c104a0aebd5cc15759c3784b28b030)), closes [#587](https://gitlab.com/hestia-earth/hestia-glossary/issues/587)
* **operation:** rename threshing operations ([11f2c1d](https://gitlab.com/hestia-earth/hestia-glossary/commit/11f2c1dedd4db1c16094a6de4163218e6d7884fd))
* **pesticideAI:** remove 10% AI on generic diluted pesticide term ([114f965](https://gitlab.com/hestia-earth/hestia-glossary/commit/114f9658dcb8a2c5a9e2274ad9d93b2dae23ce2e))
* **pesticideBrandName:** remove ecoinvent mapping ([4b6d259](https://gitlab.com/hestia-earth/hestia-glossary/commit/4b6d25940dc9319a61827cb35595f0f022801282))
* **region:** add `liveAnimal` related FAO lookups ([c5ad05d](https://gitlab.com/hestia-earth/hestia-glossary/commit/c5ad05d8b38b6451314734ccc2136c3c4a125719))
* **region:** update `animalProduct` related FAO lookups ([3a3689a](https://gitlab.com/hestia-earth/hestia-glossary/commit/3a3689aa0939f8d61f19304bafd7949453456adb))
* **region:** update `crop` related FAO lookups ([9b670ac](https://gitlab.com/hestia-earth/hestia-glossary/commit/9b670ac3d4223c19a11e6a6339adaf3aa455d79a))
* **system:** add `integrated livestock-fish farming system" ([7410ee0](https://gitlab.com/hestia-earth/hestia-glossary/commit/7410ee0e9ba61288391dd1e9a067c82148968f32)), closes [#574](https://gitlab.com/hestia-earth/hestia-glossary/issues/574)
* **waterRegime:** add `irrigation interval` ([629cd1e](https://gitlab.com/hestia-earth/hestia-glossary/commit/629cd1e181eee7b3291af733d7c1a7f94493ce8e))


### Bug Fixes

* **fuel:** set `ecoinventMapping` to `false` for all terms without mapping ([41e540c](https://gitlab.com/hestia-earth/hestia-glossary/commit/41e540c8271b810531b991d941fa1a033e4e0ac7))
* **operation:** correct typos in seed treatment terms ([cdfecfb](https://gitlab.com/hestia-earth/hestia-glossary/commit/cdfecfb7ed73461fd00973abf4740619274707b3))
* **operation:** typo in term name `Harvesting crops, with bailing machine` ([51a18fa](https://gitlab.com/hestia-earth/hestia-glossary/commit/51a18fa5999b40c40dc38d20326e666c7f29c2bc))


* **region:** rename faostatArea lookup ([85d9724](https://gitlab.com/hestia-earth/hestia-glossary/commit/85d9724dd07ebd11f49356378e7f3d4369f462d6))

### [0.13.2](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.13.1...v0.13.2) (2023-01-16)


### Bug Fixes

* **transport:** remove double space in three term names ([0bee247](https://gitlab.com/hestia-earth/hestia-glossary/commit/0bee2474cee4327a052a1f466edc6b45be058fe1))

### [0.13.1](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.13.0...v0.13.1) (2023-01-11)


### Features

* **animalProduct:** add AGROVOC links ([f9bd707](https://gitlab.com/hestia-earth/hestia-glossary/commit/f9bd707016d6690e3147a773f9c87478b9a96117)), closes [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72)
* **antibiotic:** add AGROVOC links ([11a3740](https://gitlab.com/hestia-earth/hestia-glossary/commit/11a374052f983c2e3e795f770ee4f883de653220)), closes [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72)
* **biologicalControlAgent:** add AGROVOC links ([509c715](https://gitlab.com/hestia-earth/hestia-glossary/commit/509c7158e42bfb4650eb46ea88591c1973eccd1b)), closes [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72)
* **crop:** add `fibre` term for all fibre crops ([95ec60c](https://gitlab.com/hestia-earth/hestia-glossary/commit/95ec60c1026eead92639cb8754eda8bc0cd68f5e)), closes [#562](https://gitlab.com/hestia-earth/hestia-glossary/issues/562)
* **crop:** add `gum arabic tree` and `gum arabic, gum` ([b41f314](https://gitlab.com/hestia-earth/hestia-glossary/commit/b41f31456c5fecc95ef0f56d1440986ed14cd125)), closes [#546](https://gitlab.com/hestia-earth/hestia-glossary/issues/546)
* **crop:** add missing `straw` terms for leguminous crops ([83c4888](https://gitlab.com/hestia-earth/hestia-glossary/commit/83c488844f4b0746e04e18e7cec194580715ef66)), closes [#562](https://gitlab.com/hestia-earth/hestia-glossary/issues/562)
* **crop:** add missing synonyms and exotic fruits ([512a80b](https://gitlab.com/hestia-earth/hestia-glossary/commit/512a80bd6e9e5c036cfb53ab9ab816cdf0b20759)), closes [#452](https://gitlab.com/hestia-earth/hestia-glossary/issues/452) [#518](https://gitlab.com/hestia-earth/hestia-glossary/issues/518)
* **crop:** add orchard duration and density lookups for bananas ([cbbab25](https://gitlab.com/hestia-earth/hestia-glossary/commit/cbbab25aae90f0197d9c87ebf459c463dba8698f)), closes [#559](https://gitlab.com/hestia-earth/hestia-glossary/issues/559)
* **cropEstablishment:** add `number of pesticide applications` ([5ac1c0a](https://gitlab.com/hestia-earth/hestia-glossary/commit/5ac1c0ac269b5fd9246443542d74107a5fd64d51))
* **cropResidue:** add discarded crop terms ([2551dea](https://gitlab.com/hestia-earth/hestia-glossary/commit/2551dea961aae2a869c2579c1c91402ff0fb1716))
* **electricity:** add AGROVOC links ([f11ec3c](https://gitlab.com/hestia-earth/hestia-glossary/commit/f11ec3cfc4e26aa05e95dd8213c01baf357d949f)), closes [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72)
* **forage:** add partial list of AGROVOC links ([456a6d3](https://gitlab.com/hestia-earth/hestia-glossary/commit/456a6d39caecc7cd9b3bbab4e3b180b9bf4fa3b2)), closes [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72)
* **fuel:** add `ethanol` ([e24f6bc](https://gitlab.com/hestia-earth/hestia-glossary/commit/e24f6bcc7f9d54a6ff09ec25a33e91599054aec9)), closes [#555](https://gitlab.com/hestia-earth/hestia-glossary/issues/555)
* **fuel:** add energy contents from "Engineering Toolbox" ([e2c562b](https://gitlab.com/hestia-earth/hestia-glossary/commit/e2c562b5ce09359a22a2a4f36bacb4aadb6fd8b6)), closes [#476](https://gitlab.com/hestia-earth/hestia-glossary/issues/476)
* **fuel:** add wikipedia links ([863dd95](https://gitlab.com/hestia-earth/hestia-glossary/commit/863dd9544867e2051245d68e622c67ef64092788))
* **inorganicFertilizer:** add AGROVOC links ([7dd6cae](https://gitlab.com/hestia-earth/hestia-glossary/commit/7dd6caef250868d188faae8a5174e3ca02a9e995)), closes [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72)
* **landUseManagement:** add `agroforestry` ([281ac08](https://gitlab.com/hestia-earth/hestia-glossary/commit/281ac08fd6f8cf4eaff456b0ad41e73e76579a49))
* **material:** add AGROVOC links ([73d0661](https://gitlab.com/hestia-earth/hestia-glossary/commit/73d06610da5c1a14bbe628655ad887c30739f1f6)), closes [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72)
* **measurement:** add `plant available magnesium` terms ([8d4689d](https://gitlab.com/hestia-earth/hestia-glossary/commit/8d4689db902d2667f027e4679e14160ec27477c5)), closes [#563](https://gitlab.com/hestia-earth/hestia-glossary/issues/563)
* **measurement:** add `siteType` restriction for running `netPrimaryProduction` ([599958c](https://gitlab.com/hestia-earth/hestia-glossary/commit/599958c925bd3569e97b19d5ac5e1862c020e51e))
* **measurement:** add `water salinity` ([3702bcd](https://gitlab.com/hestia-earth/hestia-glossary/commit/3702bcd46712bc98072636bed03ab5c25422a74f)), closes [#564](https://gitlab.com/hestia-earth/hestia-glossary/issues/564)
* **operation:** add `removing suckers, by hand` and `marking with ribbons` ([872e480](https://gitlab.com/hestia-earth/hestia-glossary/commit/872e480c7980f078c6eb4e42faa8cb501925fbee))
* **operation:** add `Weeding, with flamer` and `Washing, with fresh water` ([162407f](https://gitlab.com/hestia-earth/hestia-glossary/commit/162407f6ad30a6c28991ada2cc455386541637b8))
* **organicFertiliser:** add `bokashi` and `vermicompost` ([f3eb493](https://gitlab.com/hestia-earth/hestia-glossary/commit/f3eb4936a9012f336d6c30c9a6edaa524fbae1bd))
* **organicFertilizer:** add AGROVOC links ([813f674](https://gitlab.com/hestia-earth/hestia-glossary/commit/813f6743ceb9176580d3b7085e74d54d558eda7f)), closes [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72)
* **other:** add `plant-growth regulator` ([0a576f9](https://gitlab.com/hestia-earth/hestia-glossary/commit/0a576f96bc05ff42041e8cd06d986b0fa1b3d467))
* **other:** add AGROVOC links ([1ee02d6](https://gitlab.com/hestia-earth/hestia-glossary/commit/1ee02d6a17ef16828cb33b1a3e9d09d8ae2d427f)), closes [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72)
* **other:** specify chemical formula in units for consistency with `soilAmendment` ([0b66c2a](https://gitlab.com/hestia-earth/hestia-glossary/commit/0b66c2a2db04fc9dffba6f0e22e070cb00c63f4e))
* **pesticideAI:** add `pesticide adjuvant, unspecified` ([0634fb2](https://gitlab.com/hestia-earth/hestia-glossary/commit/0634fb2435a688c96fc64d266ee30dcf471ba084))
* **pesticideAI:** change name of `Ethanol` to `Ethanol (pesticide active ingredient)` ([5ed6520](https://gitlab.com/hestia-earth/hestia-glossary/commit/5ed65201f9fb555c3665258cc20f184a40be74b8))
* **pesticideBrandName:** add `BC-1000` ([77ce059](https://gitlab.com/hestia-earth/hestia-glossary/commit/77ce059bfd645eb3623c0f5adf2b1a705013d6a6))
* **pesticideBrandName:** add `Nuvan` ([febf052](https://gitlab.com/hestia-earth/hestia-glossary/commit/febf0529e96289c38381d2fc1c4411a6b3401a65))
* **property:** add `glucose content` and `fructose content` ([5e01399](https://gitlab.com/hestia-earth/hestia-glossary/commit/5e01399816671f026ed53b3ebc1284afb3f9e198))
* set `generateImpactAssessment` to `false` for most Inputs ([d2d1e62](https://gitlab.com/hestia-earth/hestia-glossary/commit/d2d1e6234adf3799ef60f6b0f0914a49fd33d4c6))
* **soilAmendment:** add `basalt` ([1d87e92](https://gitlab.com/hestia-earth/hestia-glossary/commit/1d87e92de4868a81129d78403c2bb9e141d87b06)), closes [#549](https://gitlab.com/hestia-earth/hestia-glossary/issues/549)
* **soilAmendment:** add `Zeolite` ([748bf11](https://gitlab.com/hestia-earth/hestia-glossary/commit/748bf11ff18c303dbae52e93abeda9f2955c8bc1)), closes [#556](https://gitlab.com/hestia-earth/hestia-glossary/issues/556)
* **soilAmendment:** add AGROVOC links ([a917017](https://gitlab.com/hestia-earth/hestia-glossary/commit/a91701717a3da67ff153bcd4ec736af6e37111a1)), closes [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72)
* **soilAmendment:** add wikipedia links and `synonyms` ([e9434be](https://gitlab.com/hestia-earth/hestia-glossary/commit/e9434be1c3764c85ea5a8a10f6f281918f8f7f9d))
* **soilAmendment:** change unit for bacteria species from `kg` to `CFU` ([8e5029e](https://gitlab.com/hestia-earth/hestia-glossary/commit/8e5029e9ab80a241d4b34a462f7d3a61542412dd)), closes [#547](https://gitlab.com/hestia-earth/hestia-glossary/issues/547)
* **soilAmendment:** rename `inoculant` `inoculant formulation` and add `rhizobacteria` ([f878437](https://gitlab.com/hestia-earth/hestia-glossary/commit/f878437f235a0783d2b4b341d4841cabf915796d)), closes [#547](https://gitlab.com/hestia-earth/hestia-glossary/issues/547)
* **soilTexture:** add AGROVOC links ([7c1cc66](https://gitlab.com/hestia-earth/hestia-glossary/commit/7c1cc66b9a65b94bf23be351933b95f004a514bc)), closes [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72)
* **soilType:** add AGROVOC links ([3041067](https://gitlab.com/hestia-earth/hestia-glossary/commit/3041067e290088a5f9a2d0b30f0138893217e4b6)), closes [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72)
* **transport:** add AGROVOC links ([9a69656](https://gitlab.com/hestia-earth/hestia-glossary/commit/9a696563d39727c65e7531df083dfc2d47aa5676)), closes [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72)
* **water:** add AGROVOC links ([a4273d3](https://gitlab.com/hestia-earth/hestia-glossary/commit/a4273d34a8e0074a60d6ecc6025eefcb231f65dc)), closes [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72)


### Bug Fixes

* **characterisedIndicator:** allow `CML2001 baseline` model for `Abiotic depletion, fossil fuels` ([88ca59e](https://gitlab.com/hestia-earth/hestia-glossary/commit/88ca59ef691c4832036ebcf5e8975c5419b874f3))
* **organicFertiliser:** add missing lookup `OrganicFertiliserClassification` for three terms ([921f940](https://gitlab.com/hestia-earth/hestia-glossary/commit/921f940e36beec84e29c359d27f59f9e66b8e686))
* **region:** update values for irrigated areas ([0f4c915](https://gitlab.com/hestia-earth/hestia-glossary/commit/0f4c91504edea8fde1cf08d7d1212f630b6a818f))
* **soilAmendment:** set units of `Magnesium oxide` to `kg MgO` ([1181c66](https://gitlab.com/hestia-earth/hestia-glossary/commit/1181c66bddd386bd5df32c695da60d6fe9f9bb8b))

## [0.13.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.12.0...v0.13.0) (2022-12-12)


### ⚠ BREAKING CHANGES

* **inorganicFertiliser:** renamed `NP Compounds` terms `NP Blend`,
renamed `NK Compounds` terms `NK Blend`, renamed `PK Compounds` terms `PK Blend`

### Features

* **characterisedIndicator:** add `mineral resource scarcity` ([7901c67](https://gitlab.com/hestia-earth/hestia-glossary/commit/7901c67a868abd4bd8f2f39d5963f924cb733be6)), closes [#496](https://gitlab.com/hestia-earth/hestia-glossary/issues/496)
* **cropEstablishment:** add `band fertilisation` ([d4e8962](https://gitlab.com/hestia-earth/hestia-glossary/commit/d4e8962a6dda5a27d21349278e7d7da0dc3e91a1))
* **cropResidueManagement:** add `pasture renewed` ([47fd05c](https://gitlab.com/hestia-earth/hestia-glossary/commit/47fd05c0e3994da3c9caa4a4147f95a681a49ef5)), closes [#537](https://gitlab.com/hestia-earth/hestia-glossary/issues/537)
* **inorganicFertiliser:** replace `compounds` with `blend` in term name ([07673d5](https://gitlab.com/hestia-earth/hestia-glossary/commit/07673d5fa36c23e83500988036485540e81307ff))
* **landUseManagement:** add `Silvopasture` and `Canopy cover, pasture` ([7f129a3](https://gitlab.com/hestia-earth/hestia-glossary/commit/7f129a37af543f8129e912fca7f5cf9f35fb0277))
* **landUseManagement:** rename `Pasture grasses` `Pasture grass` and update unit and definition ([14166f1](https://gitlab.com/hestia-earth/hestia-glossary/commit/14166f1507ac062417ef26ee6ca71af934946b0c)), closes [#539](https://gitlab.com/hestia-earth/hestia-glossary/issues/539)
* **landUseManagement:** rename `shade tree cover `canopy cover, shade trees` ([97d5808](https://gitlab.com/hestia-earth/hestia-glossary/commit/97d5808139250eef655fd4ee1ff893c14040a09f))
* **liveAnimal:** add `Cattle (dry beef cows)` and `Cattle (lactating beef cows)` ([3233e71](https://gitlab.com/hestia-earth/hestia-glossary/commit/3233e7137cf7a75b2f87c393dd1032bc4735cb1a)), closes [#540](https://gitlab.com/hestia-earth/hestia-glossary/issues/540)
* **measurement:** add `Sodium (per kg soil)` and `Sodium (per m3 soil)` ([ba2db20](https://gitlab.com/hestia-earth/hestia-glossary/commit/ba2db2037bde2e5105fe2cd5130f7715df2efb4a))
* **model:** add `Environmental Footprint v3` model ([769b6c5](https://gitlab.com/hestia-earth/hestia-glossary/commit/769b6c52e8211cbf2f3a68c689e622a4c9b63dab)), closes [#523](https://gitlab.com/hestia-earth/hestia-glossary/issues/523)
* **operation:** add `bagging fruit` ([cc0445f](https://gitlab.com/hestia-earth/hestia-glossary/commit/cc0445f5c31862ae4a8c3acbe220593d22c0694e))
* **operation:** add `herbicide application, with sprayer` ([a41c2a0](https://gitlab.com/hestia-earth/hestia-glossary/commit/a41c2a018be79910f096b03ccef2c3ae54cb1ec9))
* **operation:** add `transporting workers` ([a10116c](https://gitlab.com/hestia-earth/hestia-glossary/commit/a10116c49a44e81e4a050af9d7b212fd7f978046))
* **operation:** add manual operations ([06421e6](https://gitlab.com/hestia-earth/hestia-glossary/commit/06421e64276ebd26fcc4c1f0766e02cb34d5637a))
* **operation:** add pesticide, herbicide and fertiliser application with aircraft ([2b890ed](https://gitlab.com/hestia-earth/hestia-glossary/commit/2b890edceda676ab5a77e79acb41693923bd816d))
* **pesticideAI:** add lookup with the new USEtox HC20-EC10eq CFs ([c275877](https://gitlab.com/hestia-earth/hestia-glossary/commit/c275877d4003e35dea419a61e1889913674b18d9)), closes [#523](https://gitlab.com/hestia-earth/hestia-glossary/issues/523)
* **pesticideAI:** rename `CAS-138261-41-3` to `Imidacloprid` ([35634a8](https://gitlab.com/hestia-earth/hestia-glossary/commit/35634a82ca5f530ee652db870ba9881ec716829e))
* **pesticideAI:** rename `CAS-8018-01-7` to `Mancozeb` ([85919a3](https://gitlab.com/hestia-earth/hestia-glossary/commit/85919a3e97a4640779f3c805176b720bf95a1dc3))
* **pesticideBrandName:** add `Dithane M 45` ([8c81c53](https://gitlab.com/hestia-earth/hestia-glossary/commit/8c81c5357eee3c17c3d3d2dbcab51c6fbf6aee53)), closes [#526](https://gitlab.com/hestia-earth/hestia-glossary/issues/526)
* **property:** add `biodiesel content` ([0b67745](https://gitlab.com/hestia-earth/hestia-glossary/commit/0b67745fbcc0b61f86e288e787bb1c7d63b3df19))
* **tillage:** add `Full inversion tillage` ([f004228](https://gitlab.com/hestia-earth/hestia-glossary/commit/f004228ef821d39529c95767808e775a93240531)), closes [#537](https://gitlab.com/hestia-earth/hestia-glossary/issues/537)
* **waterRegime:** delete `tidalWetlands` ([a463930](https://gitlab.com/hestia-earth/hestia-glossary/commit/a4639301769ded2902d71550df4bc9733343edc2))


### Bug Fixes

* **grass:** rename to `forage` ([e9359d7](https://gitlab.com/hestia-earth/hestia-glossary/commit/e9359d71261189da30c6bf8235971d5fb11eb287))
* **pesticideAI:** add missing `-` to second Excel column ([73f2f74](https://gitlab.com/hestia-earth/hestia-glossary/commit/73f2f74826ad3ca33dc08c604336bc6f7f6c8390))

## [0.12.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.11.1...v0.12.0) (2022-11-22)


### ⚠ BREAKING CHANGES

* **landUseManagement:** `fallowCorrection` replaced by `longFallowRatio`

### Features

* **crop:** add generic terms for lupins ([286c1f4](https://gitlab.com/hestia-earth/hestia-glossary/commit/286c1f45569961d8481b6d3bf99f99908964b0d1)), closes [#533](https://gitlab.com/hestia-earth/hestia-glossary/issues/533)
* **landUseManagement:** rename `fallowCorrection` to `longFallowRatio` ([11e27cd](https://gitlab.com/hestia-earth/hestia-glossary/commit/11e27cd82d6a2743543b1e9f4bbc7fd639313180))

### [0.11.1](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.11.0...v0.11.1) (2022-11-18)


### Features

* **animalProduct:** add `global_economic_value_share` for pork products ([1c83774](https://gitlab.com/hestia-earth/hestia-glossary/commit/1c8377492a2f320fe700b22252d8102d5b671def))
* **crop:** harmonise terms, add definitions and naming rules ([7622973](https://gitlab.com/hestia-earth/hestia-glossary/commit/7622973da32a4c72651e379c0ba5b2fe8cd77c60))
* **crop:** rename cereal grain, anise, fennel, grapes terms ([64d6f6c](https://gitlab.com/hestia-earth/hestia-glossary/commit/64d6f6c2019bb2599b33a4ef9b55daa56f9dc4ef))
* **emission:** update `productTermIdsAllowed` for `CH4, to air, flooded rice` ([1d4b5f1](https://gitlab.com/hestia-earth/hestia-glossary/commit/1d4b5f185fe057298d96841e15709131cf86e220)), closes [#528](https://gitlab.com/hestia-earth/hestia-glossary/issues/528)
* **pesticideBrandName:** delete `boricAcid` ([f00f260](https://gitlab.com/hestia-earth/hestia-glossary/commit/f00f26064ce6bd9600e971db45a6a1f8686e57fe))
* **region:** rename `Raze` ([6cf98c3](https://gitlab.com/hestia-earth/hestia-glossary/commit/6cf98c3c94ad7d02bf24c815608960813394be53))


### Bug Fixes

* **crop:** add missing IPPC 2019 lookup for `pigeonPeaSeedWhole` ([b3c70cf](https://gitlab.com/hestia-earth/hestia-glossary/commit/b3c70cf4294b05e6b9a608226a92555ec90cc2cb)), closes [#528](https://gitlab.com/hestia-earth/hestia-glossary/issues/528)
* **crop:** delete terms from property-default file ([a48c13a](https://gitlab.com/hestia-earth/hestia-glossary/commit/a48c13a96698633fed5fa521e72a40d25718f272))
* fix crop and grass property lookups ([c6353bb](https://gitlab.com/hestia-earth/hestia-glossary/commit/c6353bb2d691b02ff15901cb5fc7c785a470af36))
* **grass:** empty default property overrides ([5a34071](https://gitlab.com/hestia-earth/hestia-glossary/commit/5a34071725fc76da9933c07714e33a6d71a2a7c5))

## [0.11.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.10.0...v0.11.0) (2022-10-25)


### ⚠ BREAKING CHANGES

* **landUseManagement:** `fallowCorrection` and `croppingIntensity` moved to `landUseManagement`
* **building:** `cropProtection` glossary has been moved to `building`.

### Features

* **building:** merge with cropProtection and add misting system ([58fc727](https://gitlab.com/hestia-earth/hestia-glossary/commit/58fc727346a57ff1bf5660c45cda4c153ea6411a))
* **cropEstablishment:** add `foliar fertilisation` ([7863717](https://gitlab.com/hestia-earth/hestia-glossary/commit/7863717c465623e5c7477d7bf33af30391e14c80))
* **cropEstablishment:** britainise fertilization terms ([bd17844](https://gitlab.com/hestia-earth/hestia-glossary/commit/bd1784400f5cd9ec2efba6374200b9a63c5deb86))
* **landUseManagement:** add `minimum` and `maximum` lookups to terms ([7dec753](https://gitlab.com/hestia-earth/hestia-glossary/commit/7dec753ef87ef96db6247660d1b4a334090d71b8))
* **landUseManagement:** move cropping intensity and fallow allocation from `measurement` ([5ff38a3](https://gitlab.com/hestia-earth/hestia-glossary/commit/5ff38a373bc16379c21cf2fa228623bc7472c93a))
* **methodMeasurement:** add `anaerobic slurry technique` ([0d1ea68](https://gitlab.com/hestia-earth/hestia-glossary/commit/0d1ea68234178fbe799d09b57a8c7e277f892cb1))
* **methodMeasurement:** add `chromotropic acid method` ([b0122a5](https://gitlab.com/hestia-earth/hestia-glossary/commit/b0122a5f69f6a40631771e7b069252f9259ea15f))
* **methodMeasurement:** add `conductivity bridge` ([8cc7134](https://gitlab.com/hestia-earth/hestia-glossary/commit/8cc71347664db48dcfad2a4e59af42720d0b7fe8))
* **methodMeasurement:** add `EDTA-extraction method` ([735cd4f](https://gitlab.com/hestia-earth/hestia-glossary/commit/735cd4fcb4e363ca7291b7a7306901b0b4ea58a1))
* **methodMeasurement:** add `hydrometer method` ([a2fba20](https://gitlab.com/hestia-earth/hestia-glossary/commit/a2fba20b045c0d549cddfee272ec7d01203997bb))
* **methodMeasurement:** add `photoelectric flame photometer` ([9734583](https://gitlab.com/hestia-earth/hestia-glossary/commit/97345839e7440d077644a4ae6fcdc9aabb1a0602))
* **methodMeasurement:** add `vence method` ([8b03220](https://gitlab.com/hestia-earth/hestia-glossary/commit/8b03220e2bc1616131993d3fe26feb21dffc4745))
* **operation:** add `planking, with plank` ([e583b08](https://gitlab.com/hestia-earth/hestia-glossary/commit/e583b08821507cd8cb347e7ef71ec111bf985f14))
* **operation:** add `puddling, with puddler` ([cd4ff8b](https://gitlab.com/hestia-earth/hestia-glossary/commit/cd4ff8b4ecc3fdd895729e236fd1d44e1781f55c))
* **region:** add LC-IMPACT CFs for Central America ([7584386](https://gitlab.com/hestia-earth/hestia-glossary/commit/75843868eab5d8c182a6c62d5a76dee027e1c413)), closes [#514](https://gitlab.com/hestia-earth/hestia-glossary/issues/514)
* **region:** move `measurement` lookup terms into `landUseManagement` ([a0b5bd9](https://gitlab.com/hestia-earth/hestia-glossary/commit/a0b5bd9bd16d2874c353cedc6336246ce4413420)), closes [#516](https://gitlab.com/hestia-earth/hestia-glossary/issues/516)


### Bug Fixes

* **characterisedIndicator:** missing space in units for scarcity wtd water use ([d8203cf](https://gitlab.com/hestia-earth/hestia-glossary/commit/d8203cfbd979cefe69b56cdbe2378ce82ad9a50f))

## [0.10.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.9.1...v0.10.0) (2022-10-10)


### ⚠ BREAKING CHANGES

* renamed all terms with `fertilizer` to `fertiliser`

### Features

* **grass:** add `grazedPastureGrassesInputId` lookup ([550aa80](https://gitlab.com/hestia-earth/hestia-glossary/commit/550aa80d93079b33737d8f10a8c25f1238646af9)), closes [#506](https://gitlab.com/hestia-earth/hestia-glossary/issues/506)
* **grass:** add `Ravenna grass, fresh forage` and `St Augustine grass, fresh forage` ([2d8b608](https://gitlab.com/hestia-earth/hestia-glossary/commit/2d8b6081e3f51236c678edb5f9c77160122d9639))
* **grass:** rename `Bahia grass, fresh` `Bahia grass, fresh forage` ([bf53062](https://gitlab.com/hestia-earth/hestia-glossary/commit/bf530623f3039c94ab93398168a288bcd1aa2c6c))
* **grass:** rename `Land grass, fresh` `Land grass, fresh forage` ([dc86263](https://gitlab.com/hestia-earth/hestia-glossary/commit/dc86263c8d0b4ed7a755c31e5341d6bc0da39b9c))
* **grass:** rename `Wild cane, fresh leaves and stems` `Wild cane, fresh forage` ([c902507](https://gitlab.com/hestia-earth/hestia-glossary/commit/c902507ae4e3d920ab27d0f69a132b456f9aca30))
* **region:** add CFs for H2S, NO3 and NO emissions to LC-IMPACT acidification lookup ([5330b19](https://gitlab.com/hestia-earth/hestia-glossary/commit/5330b190c769d414d71489fee1996978dd503a64)), closes [#479](https://gitlab.com/hestia-earth/hestia-glossary/issues/479)
* **region:** add CFs for N compounds emissions to LC-IMPACT marine eutrophication lookup ([34b298d](https://gitlab.com/hestia-earth/hestia-glossary/commit/34b298d160d2c23a0bedf6c8835ab15f9e7e2e35)), closes [#479](https://gitlab.com/hestia-earth/hestia-glossary/issues/479)
* **region:** add CFs for PO43- emissions to LC-IMPACT Freshwater eutrophication lookup ([ae340b3](https://gitlab.com/hestia-earth/hestia-glossary/commit/ae340b30bd76349518e40569a173502346cf3dca)), closes [#479](https://gitlab.com/hestia-earth/hestia-glossary/issues/479)


### Bug Fixes

* rename `fertilizer` to `fertiliser` ([60692e4](https://gitlab.com/hestia-earth/hestia-glossary/commit/60692e410e78529a0030126cffc0be29f3b3a45b))

### [0.9.1](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.9.0...v0.9.1) (2022-10-04)


### Features

* **crop:** add `lentil, straw`, `lentil, hulls` and `lentil, pod husks` ([0938fe2](https://gitlab.com/hestia-earth/hestia-glossary/commit/0938fe283fdc9e520dfc2944ee95be364215f6a3))
* **crop:** add `maize, bioethanol` ([9560010](https://gitlab.com/hestia-earth/hestia-glossary/commit/95600108f9a85cd9b169fe6ea9bba8e57342c55f))
* **crop:** add `Puddling, machine unspecified` ([d04e945](https://gitlab.com/hestia-earth/hestia-glossary/commit/d04e9451218b86357051678ea82bf160d6531707))
* **crop:** add `quinoa, chaff` and `quinoa, fresh forage` ([8675082](https://gitlab.com/hestia-earth/hestia-glossary/commit/8675082cef70300d6109256c6174c23542b4b7fe))
* **crop:** add agaricus bisporus mushroom terms ([3209192](https://gitlab.com/hestia-earth/hestia-glossary/commit/320919288109990138109579bf8a2652ba7bb749))
* **crop:** add olive terms ([ecc4eee](https://gitlab.com/hestia-earth/hestia-glossary/commit/ecc4eeea2527e57711aedaa6719642e093d96d2e))
* **crop:** rename `garlic, dry` `garlic, bulb` and add definition ([84b4c30](https://gitlab.com/hestia-earth/hestia-glossary/commit/84b4c307ad6734980e813741a75af012c207055a))
* **crop:** rename `lentil` `lentil, seed` ([89c4cf4](https://gitlab.com/hestia-earth/hestia-glossary/commit/89c4cf429a850ec06a29cbcd5c7eace25153f9ca))
* **crop:** rename `quinoa` `quinoa, grain` ([07ad484](https://gitlab.com/hestia-earth/hestia-glossary/commit/07ad484405d4232d62c588a6a52a9b767ae53656))
* **crop:** rename `sesame` `sesame, seeds` ([1805937](https://gitlab.com/hestia-earth/hestia-glossary/commit/18059372d319b0ab4a09b07f711efa96353a2124))
* **landUseManagement:** add mulching and intercropping practices ([e798dd9](https://gitlab.com/hestia-earth/hestia-glossary/commit/e798dd9d5d772da235e1901cf3767d25cf63eebf))
* **liveAnimal:** add `primaryMeatProductFAO` lookup ([9b11762](https://gitlab.com/hestia-earth/hestia-glossary/commit/9b11762b6f5918ff6ebd0ce9ff81d0511119bb99)), closes [#503](https://gitlab.com/hestia-earth/hestia-glossary/issues/503)
* **operation:**  add `raking hay or straw, with hay rake` ([c7921d9](https://gitlab.com/hestia-earth/hestia-glossary/commit/c7921d929bb8e05a957a78a59f29331f87bf979f))
* **operation:** add `creating ditches, with ditcher` ([52eeb12](https://gitlab.com/hestia-earth/hestia-glossary/commit/52eeb125d0ccf4d2c364bbe91e3aebd724a0904a))
* **operation:** add `ploughing, with rotary tiller` ([b31d0fd](https://gitlab.com/hestia-earth/hestia-glossary/commit/b31d0fdca3cca1a36ac4d809f088a122be0f62f6))
* **operation:** rename `tilling, unspecified` `tilling, machine unspecified` ([297c533](https://gitlab.com/hestia-earth/hestia-glossary/commit/297c533390d25bf74a5824fb9b7ffc9d9615bf53))
* **region:** add fao inorganic fertiliser indicators ([a5fe475](https://gitlab.com/hestia-earth/hestia-glossary/commit/a5fe47599ab322b6672652b2db25624e9c86ad80))
* **waterRegime:** delete `Alternate wetting and drying` ([e2634cb](https://gitlab.com/hestia-earth/hestia-glossary/commit/e2634cbb17d47ba1182ca792a76472b870b416b6))


### Bug Fixes

* **crop:** lowercase error in `id` ([a9a398b](https://gitlab.com/hestia-earth/hestia-glossary/commit/a9a398bb6f94be353863298fe5f3ab59d891c741))
* **measurement:** increase maximum value lookup for phosphorus terms ([019ad51](https://gitlab.com/hestia-earth/hestia-glossary/commit/019ad5183e9f5b372c5896f181327bf812d49342))

## [0.9.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.8.0...v0.9.0) (2022-09-20)


### ⚠ BREAKING CHANGES

* **excretaManagement:** renames `month_x` lookup in
`excretaManagement-animalProduct-NH3_EF_2019` to `x_months`

### Features

* **excreta:** add `defaultProperties` to `kg VS` terms ([1ab6345](https://gitlab.com/hestia-earth/hestia-glossary/commit/1ab6345cd22495f23e6ad9670532eed77a0ac3c0))
* **measurement:** add `bacterial population (per kg soil)` and `bacterial population (per m3 soil)` ([a0e84da](https://gitlab.com/hestia-earth/hestia-glossary/commit/a0e84da734a2187036af21b688f448669a1d7a42))
* **measurement:** add terms for bacterial population counts and soil DEA ([8683145](https://gitlab.com/hestia-earth/hestia-glossary/commit/868314504345f972f2197cd43ae56bbbbfad21df))
* **measurement:** add terms for nitrifying bacterial population ([a5f2a3b](https://gitlab.com/hestia-earth/hestia-glossary/commit/a5f2a3b956afb4f37614d14405e5d28990232437))
* **methodMeasurement:** add `acetylene inhibition method` ([375b4d7](https://gitlab.com/hestia-earth/hestia-glossary/commit/375b4d7c3cf2f97706dbd7cb5c32a0f0a6d6e165))
* **methodMeasurement:** add `MPN method` and `CFU method` ([7140ba2](https://gitlab.com/hestia-earth/hestia-glossary/commit/7140ba2e3a93f87221be69e3e740dad7369cb203))
* **system:** add vertical farming and indoor animal systems ([cf363f6](https://gitlab.com/hestia-earth/hestia-glossary/commit/cf363f66af06bb2281266e70a1fe9a5e818501db))
* **system:** rename terms to include "system" in all term names ([e39d93e](https://gitlab.com/hestia-earth/hestia-glossary/commit/e39d93e032f26123cdbbd2491b7355b9cfca3e22))
* **system:** set percent as `units` for all systems ([77e3884](https://gitlab.com/hestia-earth/hestia-glossary/commit/77e3884cae9b8e0bfd34b6497ded868ed7acc9dc))


* **excretaManagement:** rename lookup field from month_x to x_months for clarity ([86894a5](https://gitlab.com/hestia-earth/hestia-glossary/commit/86894a5dcf98a243864149a4abe1702d1f6c1ebf))

## [0.8.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.7.1...v0.8.0) (2022-09-06)


### ⚠ BREAKING CHANGES

* **animalProduct:** renames all dressed carcass weight terms to cold
dressed carcass weight
* **property:** renamed carcass weight terms to specify in cold weight
* **animalProduct:** Changed offal terms from plural to singular

### Features

* **animalProduct:** change `carcass weight` terms to `cold carcass weight` ([91da26f](https://gitlab.com/hestia-earth/hestia-glossary/commit/91da26f3a875755d2a649a40e6a91faba62bc3aa))
* **animalProduct:** improve `units` and `definition` clarify terms in cold carcass weight ([05d7362](https://gitlab.com/hestia-earth/hestia-glossary/commit/05d7362bcdac80fd6acc32ca374d1f869324b6af))
* **animalProduct:** improve naming and groupings of `offal` terms ([f3f86ba](https://gitlab.com/hestia-earth/hestia-glossary/commit/f3f86ba5e1d6bcbac50daff33197239d95bdda81))
* **animalProduct:** rename dressed carcass weight terms to cold dressed carcass weight ([aba7e8e](https://gitlab.com/hestia-earth/hestia-glossary/commit/aba7e8e03ac4fd5e49edb649e7a7c5d83dfcfbbf))
* **crop:** add wood co-products for grape, citrus, olive and tree nuts ([7163d4f](https://gitlab.com/hestia-earth/hestia-glossary/commit/7163d4f559b50d770035bba00d6fcaebcf0629b6))
* **crop:** rename `citrus fruit` `citrus, fruit` ([212ade9](https://gitlab.com/hestia-earth/hestia-glossary/commit/212ade9d15e6141fe0522eed49af288ddf0d89dd))
* **crop:** rename `olive` `olive, fruit` ([ed135f2](https://gitlab.com/hestia-earth/hestia-glossary/commit/ed135f20ab15874d5f96260297df516a1e2b8ccb))
* **emission:** add `COD, to water, industrial processes` and `COD, to water, inputs production` ([f38e0e8](https://gitlab.com/hestia-earth/hestia-glossary/commit/f38e0e800fbb167c8f616d4d96cd20e5f8cde36c)), closes [#478](https://gitlab.com/hestia-earth/hestia-glossary/issues/478)
* **emission:** add missing LC-Impact CFs to region-emission lookups ([13098bb](https://gitlab.com/hestia-earth/hestia-glossary/commit/13098bbe3f904c7a813b6825b7f662585eeebce0)), closes [#444](https://gitlab.com/hestia-earth/hestia-glossary/issues/444)
* **emission:** add missing ReCiPe CFs for ozone depletion ([d63ecf2](https://gitlab.com/hestia-earth/hestia-glossary/commit/d63ecf2397da4c1df8510b341ba029d87ed1f7cd)), closes [#492](https://gitlab.com/hestia-earth/hestia-glossary/issues/492)
* **emission:** add missing ReCiPe CFs for ozone formation ([acd0f8d](https://gitlab.com/hestia-earth/hestia-glossary/commit/acd0f8df871c522a1e203afbc24d920d32357dbc)), closes [#494](https://gitlab.com/hestia-earth/hestia-glossary/issues/494)
* **excretaManagement:** change lookup `term.id` to cold carcass weight ([8d13a9d](https://gitlab.com/hestia-earth/hestia-glossary/commit/8d13a9d408ad9c7fa625bb80bc09bff3f3d347e5))
* **excretaManagement:** rename dressed carcass weight terms to cold dressed carcass weight ([e5a96ea](https://gitlab.com/hestia-earth/hestia-glossary/commit/e5a96ea518c4bde2065d83961b835731a64f1d41))
* **liveAnimal:** add `allowedExcreta...` and `recommendedExcreta...` lookups ([92a26df](https://gitlab.com/hestia-earth/hestia-glossary/commit/92a26df897b700eecca06ae3ec7cf63e3905fb83)), closes [#172](https://gitlab.com/hestia-earth/hestia-glossary/issues/172)
* **methodMeasurement:** add `alkaline permanganate method` ([bd27866](https://gitlab.com/hestia-earth/hestia-glossary/commit/bd27866db01f202b77345b72ba706033deeddcd4))
* **methodMeasurement:** add `core sampling method` ([3b70a63](https://gitlab.com/hestia-earth/hestia-glossary/commit/3b70a635d771a51d035092436b7205e815f36c30))
* **methodMeasurement:** add `pressure plate method` ([2321c76](https://gitlab.com/hestia-earth/hestia-glossary/commit/2321c765a493d3917b4db4f32f9ce7b4ab71e644))
* **property:** add `Total solids content` ([0023bd4](https://gitlab.com/hestia-earth/hestia-glossary/commit/0023bd4555a358f678dcdb41db855b13b9e686b6))
* **property:** distinguish between hot and cold carcass weight in term names ([d0c0e86](https://gitlab.com/hestia-earth/hestia-glossary/commit/d0c0e86d76d5bd7baaced6b94a0aebc588b4f167))
* **region:** change lookup name to mention "cold" carcass weigh ([311c0e8](https://gitlab.com/hestia-earth/hestia-glossary/commit/311c0e8825bd518141fefdfc4ce2e33d2794f5c7))


### Bug Fixes

* **characterisedIndicator:** fix model name in ReCiPe lookups `GWP20 ` ([6e71a79](https://gitlab.com/hestia-earth/hestia-glossary/commit/6e71a793965e726828ad07b1e4198609c2d52bb0)), closes [#491](https://gitlab.com/hestia-earth/hestia-glossary/issues/491)
* **characterisedIndicator:** remove ReCiPe marine ecosystem damage CFs for `freshwater depletion` ([1f65685](https://gitlab.com/hestia-earth/hestia-glossary/commit/1f6568555a95e3e391f03f98dae5b2a9e4ddb080)), closes [#495](https://gitlab.com/hestia-earth/hestia-glossary/issues/495)
* **emission:** fix typo in ozone depletion LC-Impact lookup name ([59bb3b0](https://gitlab.com/hestia-earth/hestia-glossary/commit/59bb3b0c13647515c976680fbf78ae609226c443))
* **emission:** fix typo in ozone formation ReCiPe lookup name ([7757523](https://gitlab.com/hestia-earth/hestia-glossary/commit/7757523dffe1bca49218d628f66659453d9ed9b3))
* **liveAnimal:** error in properties for pullets ([0c9e52e](https://gitlab.com/hestia-earth/hestia-glossary/commit/0c9e52eec153de44c5694a4e5d5f84d81397258a))
* **pesticideAI:** remove ReCiPe ozone depletion lookups ([a6e3d2c](https://gitlab.com/hestia-earth/hestia-glossary/commit/a6e3d2cc43795c05cde7744435989c4df4db66d3)), closes [#492](https://gitlab.com/hestia-earth/hestia-glossary/issues/492)
* **pesticideAI:** remove ReCiPe ozone formation lookups ([92e1227](https://gitlab.com/hestia-earth/hestia-glossary/commit/92e1227294edb46441f3573f422bbf931ea4fdc6)), closes [#494](https://gitlab.com/hestia-earth/hestia-glossary/issues/494)
* **region:** error in `croppingIntensity` in region-measurement-lookup ([f6d4745](https://gitlab.com/hestia-earth/hestia-glossary/commit/f6d4745076e16f50f55e58bfd836183799b8a362)), closes [#460](https://gitlab.com/hestia-earth/hestia-glossary/issues/460)
* **region:** fix incorrect end of sequences ([7875e84](https://gitlab.com/hestia-earth/hestia-glossary/commit/7875e840dfab224582fc0d5c847476ec52a79ed9))

### [0.7.1](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.7.0...v0.7.1) (2022-08-23)


### Features

* **crop:** add `soybean, straw` ([7f86f44](https://gitlab.com/hestia-earth/hestia-glossary/commit/7f86f444b07bca7dfb8f153725410cc995748f9b))
* **emission:** add emissions for `waste treatment` ([20048e7](https://gitlab.com/hestia-earth/hestia-glossary/commit/20048e7c86df85710bab659239c3c81487a82a1b))
* **emission:** add lookup `typesAllowed` ([68f1d42](https://gitlab.com/hestia-earth/hestia-glossary/commit/68f1d42b68181b419abcedf974c96f5fe3c88533)), closes [#484](https://gitlab.com/hestia-earth/hestia-glossary/issues/484)
* **emission:** make `definition` field consistent for indirect N2O terms ([7da4abe](https://gitlab.com/hestia-earth/hestia-glossary/commit/7da4abe738da8a688a9bdd029256852070f15967))
* **liveAnimal:** add additional terms for male and female pigs ([0ef9bfe](https://gitlab.com/hestia-earth/hestia-glossary/commit/0ef9bfee210f5d9141647a75f5a308c1ba1488f4))
* **model:** add `IPCC (1996)` and improve descriptions of IPCC models ([ec2242e](https://gitlab.com/hestia-earth/hestia-glossary/commit/ec2242e1099868d1cfe7f8e98159b776dd49ba6f))
* **operation:** add `bund forming, with bund former` ([d28b16d](https://gitlab.com/hestia-earth/hestia-glossary/commit/d28b16dcc56b8076b8fdedc197a4b677db019ebb))
* **operation:** add terms for cooling and maintaining buildings ([4df1926](https://gitlab.com/hestia-earth/hestia-glossary/commit/4df192663f740294186a8e24ac46510a3478659f))
* **water:** add "reticulated supply" as `synonym` to `Water, municipal tap` ([1209fbe](https://gitlab.com/hestia-earth/hestia-glossary/commit/1209fbef4bc7803d8a88fb6d8dac6fe2b4674d36))


### Bug Fixes

* **landUseManagement:** delete `number of hives` (duplicate term) ([cbce712](https://gitlab.com/hestia-earth/hestia-glossary/commit/cbce71250a5ef7225072519608e35c4b58526b50))
* **liveAnimal:** hyphenate `un-weaned` in term name ([7457c14](https://gitlab.com/hestia-earth/hestia-glossary/commit/7457c140265ce2df9aac7a14ba2b119a89bd6798))

## [0.7.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.6.1...v0.7.0) (2022-08-15)


### ⚠ BREAKING CHANGES

* **property:** new term `name` and `@id` for all `Digestible energy content` terms

### Features

* **animalManagement:** add `animal growth rate` ([8562a91](https://gitlab.com/hestia-earth/hestia-glossary/commit/8562a9126fe7147c507bc2e3f398d9d0d37ee3e7))
* **animalManagement:** add `culling rate` terms ([320fbca](https://gitlab.com/hestia-earth/hestia-glossary/commit/320fbcab013ca678e8fafd8925dbd9785680ac7d))
* **animalManagement:** add new pig management terms ([2e55c90](https://gitlab.com/hestia-earth/hestia-glossary/commit/2e55c90bd519dc8a74c3f311a2d85296dcf1cd12))
* **animalManagement:** rename `animal growth rate` to `growth rate`, better define `units` ([0920f7e](https://gitlab.com/hestia-earth/hestia-glossary/commit/0920f7e54fee3fdf061e3fd0242f5aab9c67d2d7))
* **animalManagement:** rename `mortality` terms and extend to more animal types ([fae8299](https://gitlab.com/hestia-earth/hestia-glossary/commit/fae8299d197109e6edf8aaea9b9e0c3d835ea4eb))
* **animalManagement:** rename boolean terms and add `units` ([35f6c14](https://gitlab.com/hestia-earth/hestia-glossary/commit/35f6c1476a61c5eb1b42926880de1def43c1bb34))
* **animalManagement:** update pig mortality terms to match recent changes ([b7c20c8](https://gitlab.com/hestia-earth/hestia-glossary/commit/b7c20c8724e0c8d4aa2823bbd54c2e6dfdc20ab9))
* **crop:** gap-fill property values ([203ee6f](https://gitlab.com/hestia-earth/hestia-glossary/commit/203ee6f19c489f93246e28bdb3235b5603587916))
* **crop:** update property lookups ([d31997f](https://gitlab.com/hestia-earth/hestia-glossary/commit/d31997f951c8c371444f0b76189c5ed3a328372c))
* **grass:** add `signal grass` terms ([9e83467](https://gitlab.com/hestia-earth/hestia-glossary/commit/9e834675c82e218352dd8153c842ba5ed8114ba9))
* **grass:** gap-fill property values ([ac24be6](https://gitlab.com/hestia-earth/hestia-glossary/commit/ac24be6350f25e195fea2b416408121cd8ca4fda))
* **grass:** update property lookups ([fcc788f](https://gitlab.com/hestia-earth/hestia-glossary/commit/fcc788faa8fcf554b62ab846aba196dbf771102f))
* **landUseManagement:** add `continuous grazing` and `rotational grazing` ([29bfe17](https://gitlab.com/hestia-earth/hestia-glossary/commit/29bfe1798baa246593ef0c1569eb5e2e03fe348a)), closes [#186](https://gitlab.com/hestia-earth/hestia-glossary/issues/186)
* **landUseManagement:** add `pasture grasses` ([fc0d7e4](https://gitlab.com/hestia-earth/hestia-glossary/commit/fc0d7e4aa2a33badcd101c9e1cb169cab3a50f7b)), closes [#474](https://gitlab.com/hestia-earth/hestia-glossary/issues/474)
* **landUseManagement:** add grassland management related terms from IPCC (2006) ([200060c](https://gitlab.com/hestia-earth/hestia-glossary/commit/200060c12f9af243d301e67ec41ae9b762b8666a)), closes [#186](https://gitlab.com/hestia-earth/hestia-glossary/issues/186)
* **liveAnimal:** add `animalProductId` lookup ([bcca008](https://gitlab.com/hestia-earth/hestia-glossary/commit/bcca008742868da7b6eaaf10155a0efa69ac45fe)), closes [#229](https://gitlab.com/hestia-earth/hestia-glossary/issues/229)
* **liveAnimal:** rework terms for describing pigs ([bcdf90e](https://gitlab.com/hestia-earth/hestia-glossary/commit/bcdf90ef465396b56a69b322cdb0f5720f0a4d7b))
* **material:** add `density` to concrete terms ([ae5b025](https://gitlab.com/hestia-earth/hestia-glossary/commit/ae5b02512b61a4d987cb03693165922516211440)), closes [#482](https://gitlab.com/hestia-earth/hestia-glossary/issues/482)
* **property:** rename `Digestible energy content` to `Digestible energy` for clarity ([55366ad](https://gitlab.com/hestia-earth/hestia-glossary/commit/55366ad709491c541ca520f2b0f4a349a38d3f77))
* **scripts:** add method `getDefaultModels` to retrieve list of default models ([86fd8ad](https://gitlab.com/hestia-earth/hestia-glossary/commit/86fd8ad5b7714487a6a1609ab7f9af10eb682c90))


### Bug Fixes

* **animalManagement:** fix typos in `description` and `units` ([842ad70](https://gitlab.com/hestia-earth/hestia-glossary/commit/842ad707d17d605dba6541e92bb0da04c0e7a4e4))
* **animalManagement:** use `/` rather than `-1` for `units` ([d6e79c2](https://gitlab.com/hestia-earth/hestia-glossary/commit/d6e79c2f7f45f4f987eb890d93fc6aa141f939d8))
* **liveAnimal:** corrects errors in cattle definitions ([9b3e475](https://gitlab.com/hestia-earth/hestia-glossary/commit/9b3e475b0933a161d6c9576df13ff9d97c0a747c))
* **property:** error in `feedipediaName` lookup for `digestible energy` terms ([81c8477](https://gitlab.com/hestia-earth/hestia-glossary/commit/81c8477721ac2c58ac27eeba2b9e46672b4ceea7))
* **property:** errors in feedipedia links for digestible energy ([6316922](https://gitlab.com/hestia-earth/hestia-glossary/commit/631692202d2f2dfe25a23f62aecb58378c718b32))

### [0.6.1](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.6.0...v0.6.1) (2022-08-10)


### Features

* **emission:** add `synonym` to `BOD5, to water, inputs production` ([3e36b4e](https://gitlab.com/hestia-earth/hestia-glossary/commit/3e36b4ee7007fc8193d19b65418dcc5edff0aa7f))
* **emission:** add term `BOD5, to water, industrial processes` ([0f0d91d](https://gitlab.com/hestia-earth/hestia-glossary/commit/0f0d91dc6fba6bae56001482873f7138e478790b))
* **emission:** add term `NO3, to surface water, industrial processes` ([829c076](https://gitlab.com/hestia-earth/hestia-glossary/commit/829c076d040651768132de8be308270a97520ccf))
* **grass:** add `stylosanthes campo grande plants` and `stylosanthes campo grande, fresh forage` ([27f8f5e](https://gitlab.com/hestia-earth/hestia-glossary/commit/27f8f5ea101cc4f2eb90d74a6b89f83f4e61f4f7))
* **property:** add lookup `commonToSupplementInAnimalFeed` ([21becf4](https://gitlab.com/hestia-earth/hestia-glossary/commit/21becf495078eab957eae29284c4d0d822d6a94a))
* **system:** add `liveAnimal` digestibility lookup ([bf5793f](https://gitlab.com/hestia-earth/hestia-glossary/commit/bf5793f3a593fe76c59157c0c0e00aaec839b811)), closes [#458](https://gitlab.com/hestia-earth/hestia-glossary/issues/458)
* **system:** add additional terms for animal production systems ([af18067](https://gitlab.com/hestia-earth/hestia-glossary/commit/af18067d876f7f8be7f0d637d6647d34351aa8f1)), closes [#458](https://gitlab.com/hestia-earth/hestia-glossary/issues/458)


### Bug Fixes

* **resourceUse:** remove `permanent pasture` from `allowedSiteType` for land transf. from pasture ([89ba5eb](https://gitlab.com/hestia-earth/hestia-glossary/commit/89ba5eb06ca29aafc617e5eff9e362ca27a612f9)), closes [#472](https://gitlab.com/hestia-earth/hestia-glossary/issues/472)
* **resourceUse:** remove siteType from `siteTypeAllowed` if land transformation from same siteType ([926948b](https://gitlab.com/hestia-earth/hestia-glossary/commit/926948b1e43ab587cdd59f502c7909b8d85d761c)), closes [#472](https://gitlab.com/hestia-earth/hestia-glossary/issues/472)
* **soilAmendment:** rename `Innoculant` `Inoculant` ([bb666d9](https://gitlab.com/hestia-earth/hestia-glossary/commit/bb666d959a213ec7b222519d8fbbaf0a0c2344a8))

## [0.6.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.5.2...v0.6.0) (2022-08-04)


### ⚠ BREAKING CHANGES

* **animalProduct:** renamed `animalProduct` lookup
`excreta` to `excretaKgNTermId` and
`excretaVs` to `excretaKgVsTermId`
* **liveAquaticSpecies:** renamed liveAquaticSpecies lookup
`excreta` to `excretaKgNTermId` and
`excretaVs` to `excretaKgVsTermId`
* **liveAnimal:** renamed liveAnimal lookup `excreta` to `excretaKgNTermId` and
`excretaVs` to `excretaKgVsTermId`

### Features

* **animalProduct:** add lookup `excretaKgMass` and rename excreta lookups ([81b3be5](https://gitlab.com/hestia-earth/hestia-glossary/commit/81b3be5a0464caae03da235138366de2d94ae342)), closes [#466](https://gitlab.com/hestia-earth/hestia-glossary/issues/466)
* **biologicalControlAgent:** create glossary ([f144173](https://gitlab.com/hestia-earth/hestia-glossary/commit/f144173ac60dd178cb04391899ff9b5b9867ec38)), closes [#271](https://gitlab.com/hestia-earth/hestia-glossary/issues/271)
* **characterisedIndicator:** set `defaultModelId` lookup for all `damageTo...` terms ([4476156](https://gitlab.com/hestia-earth/hestia-glossary/commit/447615693ea4fe33bfb51a88634bb087d469d5ba))
* **emission:** add `model` restrictions on `site.siteType` allowed ([c6dc425](https://gitlab.com/hestia-earth/hestia-glossary/commit/c6dc425ad46169cd724bfa07848a7439de88394e))
* **emission:** set `productTermTypesAllowed` to `excreta` only for excreta terms ([e53981a](https://gitlab.com/hestia-earth/hestia-glossary/commit/e53981aa90d20cbc33d10b8be1ee69ca04af03cb)), closes [#459](https://gitlab.com/hestia-earth/hestia-glossary/issues/459)
* **excretaManagement:** change NH3 lookups to use `excreta` terms instead of `liveAnimal` ([144ad99](https://gitlab.com/hestia-earth/hestia-glossary/commit/144ad99b706c869758e4181a73a56846c2ffc568)), closes [#459](https://gitlab.com/hestia-earth/hestia-glossary/issues/459)
* **liveAnimal:** add lookup `excretaKgMass` and rename excreta lookups ([a07ade3](https://gitlab.com/hestia-earth/hestia-glossary/commit/a07ade3164237224f62de09e971abd8e7e652d07))
* **liveAquaticSpecies:** add lookup `excretaKgMass` and rename excreta lookups ([ff7c899](https://gitlab.com/hestia-earth/hestia-glossary/commit/ff7c899efcf006b00e2d3b9dadd7cf2fc8521737)), closes [#466](https://gitlab.com/hestia-earth/hestia-glossary/issues/466)
* **measurement:** limit `siteTypesAllowed` for `Heavy winter precipitation` to cropland and pasture ([7861529](https://gitlab.com/hestia-earth/hestia-glossary/commit/7861529c774af604aa1e2424d5aa3136628cd8cd)), closes [#145](https://gitlab.com/hestia-earth/hestia-glossary/issues/145)
* **other:** moves `Phytoseiulus persimilis` to `biologicalControlAgent` ([af9f602](https://gitlab.com/hestia-earth/hestia-glossary/commit/af9f60289552231d36b3d795d83bae62c6e31b36)), closes [#271](https://gitlab.com/hestia-earth/hestia-glossary/issues/271)
* **pesticideAI:** move terms to to `biologicalControlAgent` ([7fd2297](https://gitlab.com/hestia-earth/hestia-glossary/commit/7fd22974bbc29858b0986c5e137b29f01dbe2cc9)), closes [#271](https://gitlab.com/hestia-earth/hestia-glossary/issues/271)
* **region:** rename `Alga` and `Mite` as duplicated in `biologicalControlAgent` ([aa99dd6](https://gitlab.com/hestia-earth/hestia-glossary/commit/aa99dd66131c27b562da62c3671ddb9aa8e6a41a)), closes [#271](https://gitlab.com/hestia-earth/hestia-glossary/issues/271)


### Bug Fixes

* **characterisedIndicator:** delete wrong lookup for marine eutrophication LC-impact indicator ([ce47aab](https://gitlab.com/hestia-earth/hestia-glossary/commit/ce47aab8578d60b6a6aa8c0414f1a8d658388260))
* **characterisedIndicator:** delete wrong lookup for marine eutrophication LC-impact indicator ([8ca619c](https://gitlab.com/hestia-earth/hestia-glossary/commit/8ca619c6f4e10c5d44ae5a2538395003c223186c))
* **crop:** error in `hempSeed` `id` in `crop-property-lookup` ([da97beb](https://gitlab.com/hestia-earth/hestia-glossary/commit/da97bebcc184b23ba9af321c443d1bf65bf4bdd3))
* **crop:** errors in tree terms, area lookups, add missing `subClassOf` ([d0e8ef5](https://gitlab.com/hestia-earth/hestia-glossary/commit/d0e8ef5e2f911b0aa2485473060e334cbd7a740b))
* **crop:** update errors in term names and lookups ([66bf860](https://gitlab.com/hestia-earth/hestia-glossary/commit/66bf86035617db99d615216e2d4a9eb6d44e81f0)), closes [#441](https://gitlab.com/hestia-earth/hestia-glossary/issues/441) [#448](https://gitlab.com/hestia-earth/hestia-glossary/issues/448) [#470](https://gitlab.com/hestia-earth/hestia-glossary/issues/470) [#454](https://gitlab.com/hestia-earth/hestia-glossary/issues/454) [#455](https://gitlab.com/hestia-earth/hestia-glossary/issues/455)
* **grass:** rename `lemon grass` to `wire lemon grass` ([95293e2](https://gitlab.com/hestia-earth/hestia-glossary/commit/95293e2b69f00e67dd4793ad56d6a7acbd36f402))
* **grass:** update `grass-property-lookup` term `lemonGrassFreshForage` ([2ee73fd](https://gitlab.com/hestia-earth/hestia-glossary/commit/2ee73fda0f0752109d31f4d344b236089ed889d7))

### [0.5.2](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.5.1...v0.5.2) (2022-07-26)


### Features

* create `region-measurements-lookups` ([b8e5d51](https://gitlab.com/hestia-earth/hestia-glossary/commit/b8e5d51b69a0ab5be6318c6aef846fa2313b56a5)), closes [#460](https://gitlab.com/hestia-earth/hestia-glossary/issues/460)
* **cropResidue:** add `productTermTypes` allowed lookup ([007b850](https://gitlab.com/hestia-earth/hestia-glossary/commit/007b8503c89cdad44b2e2fa3c0f427023e2b4193)), closes [#467](https://gitlab.com/hestia-earth/hestia-glossary/issues/467)
* **cropResidueManagement:** add `productTermTypes` allowed lookup ([ab2edd3](https://gitlab.com/hestia-earth/hestia-glossary/commit/ab2edd3a0e2704f6dfd0fe21ea5832e801bdf5b1)), closes [#467](https://gitlab.com/hestia-earth/hestia-glossary/issues/467)
* **property:** add `siteTypesAllowed` lookup ([38faaa3](https://gitlab.com/hestia-earth/hestia-glossary/commit/38faaa3b99e748f67d4c2dcf23d8e83bb74ac6a3)), closes [#423](https://gitlab.com/hestia-earth/hestia-glossary/issues/423)

### [0.5.1](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.5.0...v0.5.1) (2022-07-26)


### Features

* add buffalo excreta term and corresponding lookups ([edd5632](https://gitlab.com/hestia-earth/hestia-glossary/commit/edd56320be4329b802a5df18368ef9c141641377)), closes [#465](https://gitlab.com/hestia-earth/hestia-glossary/issues/465)
* add camel excreta terms and corresponding lookups ([5b33499](https://gitlab.com/hestia-earth/hestia-glossary/commit/5b3349903af111e292616c48084b2eda7b43e3f5)), closes [#465](https://gitlab.com/hestia-earth/hestia-glossary/issues/465)
* add ducks excreta term and corresponding lookups ([bb5e9e2](https://gitlab.com/hestia-earth/hestia-glossary/commit/bb5e9e2160e4e97ee82810fac115a1ca06cc3cef)), closes [#465](https://gitlab.com/hestia-earth/hestia-glossary/issues/465)
* add geese excreta terms and corresponding lookups ([8a80cfe](https://gitlab.com/hestia-earth/hestia-glossary/commit/8a80cfe604dd863e9f413f9a41ec6e6bbba07d8b)), closes [#465](https://gitlab.com/hestia-earth/hestia-glossary/issues/465)
* add horses, mules, and asses excreta term and corresponding lookups ([9e06923](https://gitlab.com/hestia-earth/hestia-glossary/commit/9e069234d0bbfe158d929c3d9fcfa3557b2016db)), closes [#465](https://gitlab.com/hestia-earth/hestia-glossary/issues/465)
* add turkeys excreta terms and corresponding lookups ([778407c](https://gitlab.com/hestia-earth/hestia-glossary/commit/778407c47878fab7997a9bbc1d0d597f866eb96c)), closes [#465](https://gitlab.com/hestia-earth/hestia-glossary/issues/465)
* **animalProduct:** add `Animal meal` and `Animal by-product meal` ([50b00d8](https://gitlab.com/hestia-earth/hestia-glossary/commit/50b00d8740e917b6aa826e8e919482f4efbf8dc7)), closes [#443](https://gitlab.com/hestia-earth/hestia-glossary/issues/443)
* **characterisedIndicator:** add `damage to terrestrial ecosystems, total land use effects` ([abd0714](https://gitlab.com/hestia-earth/hestia-glossary/commit/abd0714c7fc435dd4c73de44ea9ee59b6b5d6163))
* **crop:** add subclasses to crops ([b3885e5](https://gitlab.com/hestia-earth/hestia-glossary/commit/b3885e567c4cb5ed1c9ba9e8aeaa6d0b3a76d8af)), closes [#427](https://gitlab.com/hestia-earth/hestia-glossary/issues/427)
* **cropEstablishment:** change `Sowing density` to `Seeding density` ([685ecf2](https://gitlab.com/hestia-earth/hestia-glossary/commit/685ecf28b79937109a1dbc1583873b52261bc7e7)), closes [#435](https://gitlab.com/hestia-earth/hestia-glossary/issues/435)
* **emission:** add lookup `causesExcretaMassLoss` ([04e1a97](https://gitlab.com/hestia-earth/hestia-glossary/commit/04e1a970e322923cc0481b2d425e0567f63044ff)), closes [#464](https://gitlab.com/hestia-earth/hestia-glossary/issues/464)
* **emission:** set `liveAnimal` as `productTermTypesAllowed` allowed for excreta emissions ([ef85fa4](https://gitlab.com/hestia-earth/hestia-glossary/commit/ef85fa4b2f7606f31645ac81a5aa6cfd5fd2ba11))
* **endpointIndicator:** add `defaultModelId` lookup ([e6e5cc1](https://gitlab.com/hestia-earth/hestia-glossary/commit/e6e5cc1a40d3b8f3e5ec1057cee9295d2ea7f183)), closes [#461](https://gitlab.com/hestia-earth/hestia-glossary/issues/461)
* **liveAnimal:** add `geese` as synonym for `Goose (domestic)` ([e25c983](https://gitlab.com/hestia-earth/hestia-glossary/commit/e25c98381044e3b7504949c1804b3da695786943))
* **measurement:** add lookup for `arrayTreatmentLargerUnitOfTime` ([bcc17d1](https://gitlab.com/hestia-earth/hestia-glossary/commit/bcc17d109ae5f89172c1ea2d418f160996aa73fc))
* **operation:** add `pruning, with pruning shears` ([fb7f308](https://gitlab.com/hestia-earth/hestia-glossary/commit/fb7f3083f2f7e616d9857fdb3505e5b4f2746c07))
* **other:** add lookup for `siteTypesAllowed` ([dc89af9](https://gitlab.com/hestia-earth/hestia-glossary/commit/dc89af90767b83b4dd5b49973e6a3108fd1aea03)), closes [#423](https://gitlab.com/hestia-earth/hestia-glossary/issues/423)
* **package:** export method to get `Term` restrictions ([f59e5dd](https://gitlab.com/hestia-earth/hestia-glossary/commit/f59e5dd19838682c2dae900d3be81351ff0f05db))
* **pesticideAI:** add LC-impact CFs for human and ecotoxicity ([f7dd3f1](https://gitlab.com/hestia-earth/hestia-glossary/commit/f7dd3f15984b2edb877b794304f98e84af8f1744))
* **pesticideBrandName:** add `Intrepid 2F Insecticide` ([5ea927a](https://gitlab.com/hestia-earth/hestia-glossary/commit/5ea927aa75e2c2daf6556bf298b967c9d20af881))
* **region:** add `excreta` terms in all `units` to `region-excreta-excretaManagement-ch4B0` lookup ([b95a559](https://gitlab.com/hestia-earth/hestia-glossary/commit/b95a5595450d77b589e9b2ed525868e747af482d))


### Bug Fixes

* **crop:** ensure `plants` in term.name is always plural ([706428c](https://gitlab.com/hestia-earth/hestia-glossary/commit/706428c370498cbcada3d72f0c6599fedfeb565b)), closes [#447](https://gitlab.com/hestia-earth/hestia-glossary/issues/447)
* **crop:** ensure `trees` and `plants` in `term.name` is singular ([adf6479](https://gitlab.com/hestia-earth/hestia-glossary/commit/adf64793693d14d9aff2bd4f6f35046bad6c1c74)), closes [#447](https://gitlab.com/hestia-earth/hestia-glossary/issues/447)
* **crop:** inconsistencies in `term.name` where functional unit is `ha` ([9d7c334](https://gitlab.com/hestia-earth/hestia-glossary/commit/9d7c3343a65bb320d858c5a0537c8a6562d28c0e)), closes [#447](https://gitlab.com/hestia-earth/hestia-glossary/issues/447)
* **crop:** tidy up `dataStates` of lookups `N_Content_AG_Residue` and `N_Content_BG_Residue` ([38ffea0](https://gitlab.com/hestia-earth/hestia-glossary/commit/38ffea0c02b878bde78cba58ffaab60ec6455667)), closes [#457](https://gitlab.com/hestia-earth/hestia-glossary/issues/457)
* **liveAnimal:** error in excreta lookups for `lamb` ([2e86173](https://gitlab.com/hestia-earth/hestia-glossary/commit/2e8617368c1edc2a7adc531c5bb12910b10cb2d8))
* **measurement:** error in units of `Sunshine duration` ([a771abe](https://gitlab.com/hestia-earth/hestia-glossary/commit/a771abe2a303de7259df4667d82dee4431553790))
* **measurement:** remove synonyms for air pressure which were actually units ([b632c6f](https://gitlab.com/hestia-earth/hestia-glossary/commit/b632c6fea47125651c64359c902bfa8b1bbe848d)), closes [#431](https://gitlab.com/hestia-earth/hestia-glossary/issues/431)
* **operation:** error in synonym for `Bailing, machine unspecified` ([8eed615](https://gitlab.com/hestia-earth/hestia-glossary/commit/8eed615c6e8681ac7dac5d42033ca2e204b593da)), closes [#424](https://gitlab.com/hestia-earth/hestia-glossary/issues/424)
* **region:** move LC-Impact water stress CFs to new lookup ([55a0054](https://gitlab.com/hestia-earth/hestia-glossary/commit/55a005401144cf931b499ccdefe87343fe34f6b1))
* **region:** remove `certainEffects` and `AllEffects` from awareWaterBasinId-resourceUse lookup ([0153c28](https://gitlab.com/hestia-earth/hestia-glossary/commit/0153c2836154cd2f5d9609dc03a35ee4244929e3))
* **region:** replace term names with ids in region-emission Freshwater eutrophication lookup ([772a60c](https://gitlab.com/hestia-earth/hestia-glossary/commit/772a60c9a64956b8f8c9ca4abcd356f4e143ac01))
* **region:** split LC-Impact region-emission PM formation lookup into allEffects and certainEffects ([564edf8](https://gitlab.com/hestia-earth/hestia-glossary/commit/564edf8131821124a29eb4419790667f4f90b5ea))

## [0.5.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.4.2...v0.5.0) (2022-07-12)


### ⚠ BREAKING CHANGES

* **measurement:** renamed
`availableNitrogenPerKgSoil` to `plantAvailableNitrogenPerKgSoil`
`availablePhosphorusPerKgSoil` to `plantAvailablePhosphorusPerKgSoil`
`availableNitrogenPerM3Soil` to `plantAvailableNitrogenPerM3Soil`
`availablePhosphorusPerM3Soil` to `plantAvailablePhosphorusPerM3Soil`
* **measurement:** `phosphorusPerKgSoil` renamed to `totalPhosphorusPerKgSoil`,
`phosphorusPerM3Soil` renamed to `totalPhosphorusPerM3Soil`
* **measurement:** `phosphatePerM3Soil` and `phosphatePerKgSoil` have been removed

### Features

* add `siteTypesAllowed` to remaining practice terms ([7871b5a](https://gitlab.com/hestia-earth/hestia-glossary/commit/7871b5a2ca9c94d071fd7eaab822554790995d56)), closes [#409](https://gitlab.com/hestia-earth/hestia-glossary/issues/409)
* add `Tjurin method` ([d857008](https://gitlab.com/hestia-earth/hestia-glossary/commit/d8570085a61d7241cb8e529d15c6d006b0c92afd))
* add ecoregion/region-siteType lookups for land stress LC-Impact ([fe38abf](https://gitlab.com/hestia-earth/hestia-glossary/commit/fe38abf3e55d98aed8c107380bcf9609812e35d5))
* **characterisedIndicator:** add LC-impact climate change indicators ([73b8098](https://gitlab.com/hestia-earth/hestia-glossary/commit/73b80980af444f831c35869fdb6a0e4c2cf2b9fb)), closes [#400](https://gitlab.com/hestia-earth/hestia-glossary/issues/400)
* **characterisedIndicator:** add LC-Impact Eutrophication indicators ([7fa08f6](https://gitlab.com/hestia-earth/hestia-glossary/commit/7fa08f6fb7b34aa17617155f2d579463fe15dd78)), closes [#418](https://gitlab.com/hestia-earth/hestia-glossary/issues/418) [#420](https://gitlab.com/hestia-earth/hestia-glossary/issues/420)
* **characterisedIndicator:** add LC-impact ozone depletion indicators ([5968afd](https://gitlab.com/hestia-earth/hestia-glossary/commit/5968afd21a0829d7483d16bc717ab11f963767c4)), closes [#411](https://gitlab.com/hestia-earth/hestia-glossary/issues/411)
* **characterisedIndicator:** add LC-Impact Ozone Formation CFs ([07c5df4](https://gitlab.com/hestia-earth/hestia-glossary/commit/07c5df48a88327dada8e0e28a30721cd9c06aa3e))
* **characterisedIndicator:** add LC-Impact PM formation and acidification indicators ([065ea1a](https://gitlab.com/hestia-earth/hestia-glossary/commit/065ea1a554b034b1db701fdc69c56ec2c6f8c402)), closes [#416](https://gitlab.com/hestia-earth/hestia-glossary/issues/416) [#417](https://gitlab.com/hestia-earth/hestia-glossary/issues/417)
* **characterisedIndicator:** add LC-Impact toxicity indicators ([f67fb15](https://gitlab.com/hestia-earth/hestia-glossary/commit/f67fb154d75ba43f48c18c0db52f92182de226d0)), closes [#421](https://gitlab.com/hestia-earth/hestia-glossary/issues/421) [#426](https://gitlab.com/hestia-earth/hestia-glossary/issues/426)
* **crop:** add `Pea, plants` ([506171b](https://gitlab.com/hestia-earth/hestia-glossary/commit/506171b48ac7ffad2bcfa789d94e5878f4049d6b))
* **crop:** add `possibleCoverCrop` lookup ([6bc27d4](https://gitlab.com/hestia-earth/hestia-glossary/commit/6bc27d4544ed4f3b127df5933e02ca77afd44eae))
* **crop:** add `teff, hay` and `teff, straw` and rename `teff` ([f7e635d](https://gitlab.com/hestia-earth/hestia-glossary/commit/f7e635dfc3d1c67909804f2bfdeae5e9e31432f3)), closes [#202](https://gitlab.com/hestia-earth/hestia-glossary/issues/202)
* **crop:** add sudangrass and sisal terms ([cc7af9e](https://gitlab.com/hestia-earth/hestia-glossary/commit/cc7af9e029a07f96d4cfa34533fdd935ac04cf5b))
* **cropResidue:** add `possibleCoverCrop` lookup ([3a5865e](https://gitlab.com/hestia-earth/hestia-glossary/commit/3a5865ef39ec9a3b6fb73ae3949da635e5326ee4))
* **cropResidue:** add `siteTypesAllowed` ([4e14215](https://gitlab.com/hestia-earth/hestia-glossary/commit/4e142150782b58bf369032b1ffebff71b444d613))
* **emission:** add LC-Impact Climate Change CFs ([2d4d5fe](https://gitlab.com/hestia-earth/hestia-glossary/commit/2d4d5fec91a992a4de65a227076994a41d565c49)), closes [#400](https://gitlab.com/hestia-earth/hestia-glossary/issues/400)
* **emission:** add LC-Impact Ozone Depletion CFs ([0b6bfbb](https://gitlab.com/hestia-earth/hestia-glossary/commit/0b6bfbba4b1a769764f13a5716388de75252c842)), closes [#411](https://gitlab.com/hestia-earth/hestia-glossary/issues/411)
* **emission:** make `productTermTypesAllowed` less restrictive ([1ad9ac5](https://gitlab.com/hestia-earth/hestia-glossary/commit/1ad9ac51d320e4357ef04023a853c39d54b687f7))
* **endpointIndicator:** add LC-Impact endpoints ([9fa6fa5](https://gitlab.com/hestia-earth/hestia-glossary/commit/9fa6fa5913701725bb681617f9460e22be54fb1f)), closes [#407](https://gitlab.com/hestia-earth/hestia-glossary/issues/407)
* **endpointIndicator:** rename ReCiPe endpoints ([dc3604b](https://gitlab.com/hestia-earth/hestia-glossary/commit/dc3604beba8fa1c0fa4dea138faac4d83e83a459))
* **excreta:** add lookup colour coding and `dataState` ([5a715ae](https://gitlab.com/hestia-earth/hestia-glossary/commit/5a715ae7c17f1a55726ca1bbb939455bfe1d0e27)), closes [#344](https://gitlab.com/hestia-earth/hestia-glossary/issues/344)
* **fuel:** change unit from `m3` to `kg` for `Liquefied petroleum gases (LPG)` ([3398c26](https://gitlab.com/hestia-earth/hestia-glossary/commit/3398c267080bcb872ad5372f2d75324758c5e479)), closes [#363](https://gitlab.com/hestia-earth/hestia-glossary/issues/363)
* **grass:** add property lookup ([a629df0](https://gitlab.com/hestia-earth/hestia-glossary/commit/a629df0387fa1e65d94c0ed3ed59c99338cfb46f))
* **grass:** create grass glossary ([2050c75](https://gitlab.com/hestia-earth/hestia-glossary/commit/2050c75dc2d24362cda4c0b35cd42324510768a4)), closes [#399](https://gitlab.com/hestia-earth/hestia-glossary/issues/399)
* **landUseManagement:** add `number of hives` from biodiversity glossary ([b1fe245](https://gitlab.com/hestia-earth/hestia-glossary/commit/b1fe2459f161cedf97f0a07323fc25ebce292315))
* **landUseManagement:** add term `Cover crop` ([2f334bb](https://gitlab.com/hestia-earth/hestia-glossary/commit/2f334bb17b477a1f7acd059919cea021f4fb250d))
* **liveAnimal:** add lookup colour coding and `dataState` ([f85929c](https://gitlab.com/hestia-earth/hestia-glossary/commit/f85929c4298797f2c9c87e9098a9fe9bde0bd8bf)), closes [#344](https://gitlab.com/hestia-earth/hestia-glossary/issues/344)
* **measurement:** add `soil solution P` as synonym for `Available phosphorus` ([61903dd](https://gitlab.com/hestia-earth/hestia-glossary/commit/61903ddb08e75390ebe524ac9f160e47dcd15efe)), closes [#428](https://gitlab.com/hestia-earth/hestia-glossary/issues/428)
* **measurement:** add `subClassOf` to soil measurements ([a3a8c95](https://gitlab.com/hestia-earth/hestia-glossary/commit/a3a8c9553221acca016a11a42d7e3155a5246255))
* **measurement:** add additional soil phosphorus terms and create hierarchy ([c49fcaa](https://gitlab.com/hestia-earth/hestia-glossary/commit/c49fcaae333aad7e33171ecca1c6d00fe1b61f6a)), closes [#428](https://gitlab.com/hestia-earth/hestia-glossary/issues/428)
* **measurement:** delete soil phosphate terms ([fe4634e](https://gitlab.com/hestia-earth/hestia-glossary/commit/fe4634e2c7667a703d7617a9688ff67088c5d579)), closes [#428](https://gitlab.com/hestia-earth/hestia-glossary/issues/428)
* **measurement:** improve definition of `Available phosphorus...` terms ([a8a86b3](https://gitlab.com/hestia-earth/hestia-glossary/commit/a8a86b3eb250236cb0b680464d487ad352f0f581)), closes [#428](https://gitlab.com/hestia-earth/hestia-glossary/issues/428)
* **measurement:** rename `Phosphorus (per...` to `Total phosphorus (per...` ([83c17f8](https://gitlab.com/hestia-earth/hestia-glossary/commit/83c17f8edb337f8c81d8c97b2d61af0b562b0e68)), closes [#428](https://gitlab.com/hestia-earth/hestia-glossary/issues/428)
* **measurement:** rename all `Available...` terms to `Plant available...` ([c9802c6](https://gitlab.com/hestia-earth/hestia-glossary/commit/c9802c6c78810a374b6f61565ef501ebe3c63693))
* **methodMeasurement:** add `Tinsley method` and improve synonyms for related terms ([145d77b](https://gitlab.com/hestia-earth/hestia-glossary/commit/145d77b1f034837c5427eb2d6d77f232dad7032f))
* **methodMeasurement:** add Egner-Riehm method ([18fec26](https://gitlab.com/hestia-earth/hestia-glossary/commit/18fec260054603517861bc20db74e80697c85131))
* **model:** add LC-Impact models ([2127bed](https://gitlab.com/hestia-earth/hestia-glossary/commit/2127bed50f94ab5bc3c5a4e5a202867e25073584)), closes [#405](https://gitlab.com/hestia-earth/hestia-glossary/issues/405)
* **model:** delete `LC-Impact` ([c7ecd23](https://gitlab.com/hestia-earth/hestia-glossary/commit/c7ecd23f2f4ca31979bd92c35d847b1e356ec82a))
* **model:** delete `Other background database` add `Linked Impact Assessment` ([3ee1534](https://gitlab.com/hestia-earth/hestia-glossary/commit/3ee1534f292e3e04066370184462f7cce9cec8d6)), closes [#402](https://gitlab.com/hestia-earth/hestia-glossary/issues/402)
* **operation:** add `watering animals` and `cleaning animal housing` ([a6141eb](https://gitlab.com/hestia-earth/hestia-glossary/commit/a6141eb0f7ba45558c4dcadfb24b1c1fce1136c7)), closes [#415](https://gitlab.com/hestia-earth/hestia-glossary/issues/415)
* **operation:** add `watering animals` and `cleaning animal housing` ([c036ed8](https://gitlab.com/hestia-earth/hestia-glossary/commit/c036ed862b52cc44bd87f7f1f422d5559edf92c0)), closes [#415](https://gitlab.com/hestia-earth/hestia-glossary/issues/415)
* **region:** add  region-MarineEutrophicationLCImpactCF lookup ([9a826df](https://gitlab.com/hestia-earth/hestia-glossary/commit/9a826df6aa01e1470cbff388ff019004fafc02f5)), closes [#418](https://gitlab.com/hestia-earth/hestia-glossary/issues/418)
* **region:** add LC-Impact water stress CFs to awareWaterBasinId lookup ([7424cd3](https://gitlab.com/hestia-earth/hestia-glossary/commit/7424cd380ea503f2ced212f65a26954cdd3a8933))
* **region:** add region-FreshwaterEutrophicationLCImpactCF lookup ([c4cb983](https://gitlab.com/hestia-earth/hestia-glossary/commit/c4cb983e9c4819e675430e3c1f0558a2992491f6)), closes [#420](https://gitlab.com/hestia-earth/hestia-glossary/issues/420)
* **region:** add region-OzoneFormationLCImpactCF lookups ([b0684bf](https://gitlab.com/hestia-earth/hestia-glossary/commit/b0684bf0c2841768bb02a7411076c250471b53bf))
* **region:** add region-ParticulateMatterFormationLCImpactCF lookup ([2b7d652](https://gitlab.com/hestia-earth/hestia-glossary/commit/2b7d652f4f406b7c23195782480e56e2cc218586)), closes [#416](https://gitlab.com/hestia-earth/hestia-glossary/issues/416)
* **region:** add region-resourceUse lookups for LC-Impact water stress CFs ([ac9260f](https://gitlab.com/hestia-earth/hestia-glossary/commit/ac9260f387c78402485cff801ee4944560be2d27))
* **region:** add region-TerrestrialAcidificationLCImpactCF lookup ([b7cd669](https://gitlab.com/hestia-earth/hestia-glossary/commit/b7cd669185f682c0c925bfc92b36ec43114ad443)), closes [#417](https://gitlab.com/hestia-earth/hestia-glossary/issues/417)


### Bug Fixes

* **characterisedIndicator:** fix mapping to model `linkedImpactAssessment` ([812683f](https://gitlab.com/hestia-earth/hestia-glossary/commit/812683fa36302baf734d8851ce0623b4bd27415a))
* **characterisedIndicator:** rename LC-Impact indicators to remove model from name ([51cb68f](https://gitlab.com/hestia-earth/hestia-glossary/commit/51cb68f18d9537f7546c172d4f3388c07fd907a4))
* **endpointIndicator:** rename ReCiPe and LC-Impact indicators and fix units ([70e09e6](https://gitlab.com/hestia-earth/hestia-glossary/commit/70e09e6c88edb3bb056954271fc2bc702e65d63f))
* **inorganicFertilizer:** add space in `Calcium metaphosphate (kg P2O5)` term name ([e3c6afb](https://gitlab.com/hestia-earth/hestia-glossary/commit/e3c6afb8409ef3aa049ae6d23bbb42c914f78cd9))
* **measurement:** set minimum humidity lookup to `0.1` ([7005a27](https://gitlab.com/hestia-earth/hestia-glossary/commit/7005a273d2edda75cbd3284a70df952061b9545e))

### [0.4.2](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.4.1...v0.4.2) (2022-06-21)


### Features

* **crop:** add pearl millet forage, hay and straw ([3b41ae9](https://gitlab.com/hestia-earth/hestia-glossary/commit/3b41ae90c4ed43d5fc91a9c1c883961f234dd929))
* **crop:** remove grasses ([11bef75](https://gitlab.com/hestia-earth/hestia-glossary/commit/11bef7506ed0e07823e6c7b08e08d5013cdbc6fe)), closes [#103](https://gitlab.com/hestia-earth/hestia-glossary/issues/103)
* **liveAnimal:** add `chick` ([3b2553a](https://gitlab.com/hestia-earth/hestia-glossary/commit/3b2553a02c98ed463bf7cc45e5d6e55a865866f6))
* **measurement:** add `(growing season)` terms ([c1585d1](https://gitlab.com/hestia-earth/hestia-glossary/commit/c1585d11300a7eafc1f32170059202d21dc284a0))
* **measurement:** add `Soil saturated hydraulic conductivity` ([a0b3633](https://gitlab.com/hestia-earth/hestia-glossary/commit/a0b3633ca58e896332745176336b43684a033ac4))
* **methodMeasurement:** add `NH4OAc method, exchangeable potassium` ([d8b6827](https://gitlab.com/hestia-earth/hestia-glossary/commit/d8b682790e495f435314eb052da4a3761ca5653c))
* **organicFertilizer:** add `Seamac Gold` ([d34967f](https://gitlab.com/hestia-earth/hestia-glossary/commit/d34967fefa82cee252f06a307017238fd35d1ae3))
* **pesticideAI:** rename `magnesium sulphate` `magnesium sulfate anhydrous` ([971cfc5](https://gitlab.com/hestia-earth/hestia-glossary/commit/971cfc5c17e2493cf7cb2c43904e778318f506e5)), closes [#365](https://gitlab.com/hestia-earth/hestia-glossary/issues/365)
* **soilAmendment:** add `kieserite` ([9154d93](https://gitlab.com/hestia-earth/hestia-glossary/commit/9154d934b9fb0acc03df787bc1e188da59a91c7f)), closes [#365](https://gitlab.com/hestia-earth/hestia-glossary/issues/365)


### Bug Fixes

* **emission:** error in `siteTypesAllowed` for excreta emissions ([bbfe9f3](https://gitlab.com/hestia-earth/hestia-glossary/commit/bbfe9f3e30b8fca8101367aa24811557620e487f))
* **inorganicFertilizer:** fix errors introduced in previous commit ([883773f](https://gitlab.com/hestia-earth/hestia-glossary/commit/883773ff960c626fb1f7ebf98885d6f584613df1))

### [0.4.1](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.4.0...v0.4.1) (2022-06-08)


### Features

* **characterisedIndicator:** add `defaultModelId` ([94d384a](https://gitlab.com/hestia-earth/hestia-glossary/commit/94d384a4772c0e1f72a16307a2abd2c4c2ded820))
* **operation:** add 'top dressing, machine unspecified` ([4050b49](https://gitlab.com/hestia-earth/hestia-glossary/commit/4050b49cc7adb9cecc556548e3cc63aa47facf5f))
* **pesticideAI:** add `vegetable oil, unspecified (pesticide AI)` ([f03575a](https://gitlab.com/hestia-earth/hestia-glossary/commit/f03575a23547ad7c6aff27c241b0d87bbae32cbc))
* **pesticideAI:** add main urease inhibitors ([7b50c6b](https://gitlab.com/hestia-earth/hestia-glossary/commit/7b50c6b66f83378d8362e5507e446b1bb5fdfb0a)), closes [#395](https://gitlab.com/hestia-earth/hestia-glossary/issues/395)
* **property:** add `starch content` ([fba832b](https://gitlab.com/hestia-earth/hestia-glossary/commit/fba832babcd1b0532ee06d656886955c828c6fb5))
* **soilType:** add principal qualifiers and re-work glossary ([15d44f2](https://gitlab.com/hestia-earth/hestia-glossary/commit/15d44f2259362a0f730b0cfebe940b6ff8e334f1)), closes [#56](https://gitlab.com/hestia-earth/hestia-glossary/issues/56)


### Bug Fixes

* remove duplicated columns from excreta lookup table ([7bf5686](https://gitlab.com/hestia-earth/hestia-glossary/commit/7bf5686f86d4f69d2e9f7fde2dac0071f1b1bca3)), closes [#367](https://gitlab.com/hestia-earth/hestia-glossary/issues/367)

## [0.4.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.3.2...v0.4.0) (2022-06-07)


### ⚠ BREAKING CHANGES

* **model:** `usetox2008` and `usetox2010` replace by `usetoxV2`

### Features

* add `siteTypesAllowed` to key practices ([069d4e7](https://gitlab.com/hestia-earth/hestia-glossary/commit/069d4e73a333cd2a8a102416d0378c3220cb7ba6))
* **biodiversity:** add `number of hives` ([cc893c3](https://gitlab.com/hestia-earth/hestia-glossary/commit/cc893c339a891bddc167a4c670ac5824d8c569e6)), closes [#357](https://gitlab.com/hestia-earth/hestia-glossary/issues/357)
* **crop:** add walnut terms ([0060616](https://gitlab.com/hestia-earth/hestia-glossary/commit/00606167ec789951643c74551ad650626a4b8bdb)), closes [#384](https://gitlab.com/hestia-earth/hestia-glossary/issues/384)
* **crop:** rename `walnut` to `walnut, kernel` ([087a8da](https://gitlab.com/hestia-earth/hestia-glossary/commit/087a8da412f27a5ede5992d01bb7b14e455147bb)), closes [#384](https://gitlab.com/hestia-earth/hestia-glossary/issues/384)
* **ecoClimateZone:** add `name` lookup ([6c159a1](https://gitlab.com/hestia-earth/hestia-glossary/commit/6c159a160a4bcfd3602fde746b32e2c1c0e440b6))
* **ecoClimateZone:** add `wet` lookup ([e739c40](https://gitlab.com/hestia-earth/hestia-glossary/commit/e739c40af322d93f78e2a8237cefb7607816122d))
* **emission:** add `glass or high accessible cover` to `siteTypesAllowed` ([b055bd3](https://gitlab.com/hestia-earth/hestia-glossary/commit/b055bd3223fb128675334e1088a21e0c35047d61)), closes [#338](https://gitlab.com/hestia-earth/hestia-glossary/issues/338)
* **inorganicFertilizer:** add fertilizers from Nebraska ([f3fc5c7](https://gitlab.com/hestia-earth/hestia-glossary/commit/f3fc5c7560cba6e0c39bd38bf26ddeb195bc36f2)), closes [#216](https://gitlab.com/hestia-earth/hestia-glossary/issues/216)
* **inorganicFertilizer:** rename `Triammonium phosphate (kg N)` ([24cd188](https://gitlab.com/hestia-earth/hestia-glossary/commit/24cd188ca53a30e43e693fab502ae587ce4e93bf))
* **landUseManagement:** add `number of fruits per tree` ([3100539](https://gitlab.com/hestia-earth/hestia-glossary/commit/3100539823718c0f17d37a593c3e6c685d9a0ea1)), closes [#378](https://gitlab.com/hestia-earth/hestia-glossary/issues/378)
* **measurement:** add `glass or high accessible cover` to `siteTypesAllowed` ([799ddad](https://gitlab.com/hestia-earth/hestia-glossary/commit/799ddadc33a7be676603b02c55c3412ea9b00fe3))
* **measurement:** add `soil water holding capacity` ([7fa00bc](https://gitlab.com/hestia-earth/hestia-glossary/commit/7fa00bca9bb2e15c916849d4ff132114297eb4fc)), closes [#375](https://gitlab.com/hestia-earth/hestia-glossary/issues/375)
* **measurement:** add number of frozen days terms ([62fa14f](https://gitlab.com/hestia-earth/hestia-glossary/commit/62fa14fa1e8d07bbc48f39e823a8557c83bcbc6e)), closes [#387](https://gitlab.com/hestia-earth/hestia-glossary/issues/387)
* **measurement:** add soil DOC, DON, NO3-, NH4+, and available P ([fabfa13](https://gitlab.com/hestia-earth/hestia-glossary/commit/fabfa136f503d6e174ac869709ab4e74fc5f8ce1)), closes [#370](https://gitlab.com/hestia-earth/hestia-glossary/issues/370)
* **measurement:** add sunshine duration terms ([fa2aa9c](https://gitlab.com/hestia-earth/hestia-glossary/commit/fa2aa9c90c242307bf000cac21d9a2ab2967666c)), closes [#387](https://gitlab.com/hestia-earth/hestia-glossary/issues/387)
* **model:** add `USEtox v2` and remove `USEtox 2008` and `USEtox 2010` ([a3f49d6](https://gitlab.com/hestia-earth/hestia-glossary/commit/a3f49d694bd565c1512c77c6fa14137a14a5ecfd))
* **operation:** add `harvesting fruits, with trunk shaker ` ([9670bc3](https://gitlab.com/hestia-earth/hestia-glossary/commit/9670bc39e1445c1bdb53f84a883803a174e698bf)), closes [#379](https://gitlab.com/hestia-earth/hestia-glossary/issues/379)
* **operation:** add `mowing, with disc mower` ([0eaf197](https://gitlab.com/hestia-earth/hestia-glossary/commit/0eaf1972b64e238f0f9fac127c8140924c571acd)), closes [#379](https://gitlab.com/hestia-earth/hestia-glossary/issues/379)
* **organicFertilizer:** add `nebraskaExstensionFertilizerCode` lookup and rename `precipitated bone phosphate (kg)` ([721c752](https://gitlab.com/hestia-earth/hestia-glossary/commit/721c7527dcfe65fa585ec0c471e88c4de202d0ab))
* **organicFertilizer:** add fertilizers from Nebraska Ag Glossary ([836408f](https://gitlab.com/hestia-earth/hestia-glossary/commit/836408f63434fb6cdb4d21f4e3bd0e1c7591e2c1)), closes [#216](https://gitlab.com/hestia-earth/hestia-glossary/issues/216)
* **pesticideAI:** add `3,4-dimethylpyrazole phosphate` ([2e1e810](https://gitlab.com/hestia-earth/hestia-glossary/commit/2e1e810285c921599af68eedda81391656118b3c))
* **property:** add `chloride content` ([c126792](https://gitlab.com/hestia-earth/hestia-glossary/commit/c126792fb7d35f64fd899c2ea03786ec8eaf1c7a)), closes [#377](https://gitlab.com/hestia-earth/hestia-glossary/issues/377)
* **property:** add `kernel recovery rate` ([866ef46](https://gitlab.com/hestia-earth/hestia-glossary/commit/866ef462f5663ef12b4ca74c7e28d783067e7554)), closes [#376](https://gitlab.com/hestia-earth/hestia-glossary/issues/376)
* **resourceUse:** add `glass or high accessible cover` to `siteTypesAllowed` ([2cd2bf3](https://gitlab.com/hestia-earth/hestia-glossary/commit/2cd2bf32b538051024f152347c45e1c4634178a7))
* **soilTexture:** add `siteTypesAllowed` ([6242e91](https://gitlab.com/hestia-earth/hestia-glossary/commit/6242e91ebf9361ce1aa1bea45c721b2bc56eebfe))
* **usdaSoilType:** add `siteTypesAllowed` ([125fdab](https://gitlab.com/hestia-earth/hestia-glossary/commit/125fdab3ec443f57aef83e80e541127e389b038b))


### Bug Fixes

* **biodiversity:** capitalize uncapitalized terms ([f1bbeae](https://gitlab.com/hestia-earth/hestia-glossary/commit/f1bbeae37179b2843b02f6fddf32694e83ad15f3))
* **material:** fix ecoinvent coefficient for concrete and wood terms ([11e4f9f](https://gitlab.com/hestia-earth/hestia-glossary/commit/11e4f9f2de955e0238b604ff3cf8d5a73d23dad9)), closes [hestia-earth/hestia-engine-models#265](https://gitlab.com/hestia-earth/hestia-engine-models/issues/265)
* **organicFertilizer:** fix term id for bone meal terms ([709f754](https://gitlab.com/hestia-earth/hestia-glossary/commit/709f75476aea6cbe28476620b55bc9676b3ca228))
* **organicFertilizer:** remove `nitrogenContent` from `sewageSludgeHeatDriedKgMass` ([ff80a3c](https://gitlab.com/hestia-earth/hestia-glossary/commit/ff80a3c230bed379c07246180b166bb5be357f62))
* **region:** rename `Antarctica` and `Chives` to avoid duplicates ([3cba559](https://gitlab.com/hestia-earth/hestia-glossary/commit/3cba5595ddfa0a29f37fc20ebb9de1583e4b0ef7))

### [0.3.2](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.3.1...v0.3.2) (2022-05-24)


### Features

* **crop:** add brazil nut and shea nut terms ([e3cf9e8](https://gitlab.com/hestia-earth/hestia-glossary/commit/e3cf9e87412b87fcea5d82ff3f792cbaa06cc043))
* **emission:** add `productTermIdsAllowed` lookup ([d6b9c58](https://gitlab.com/hestia-earth/hestia-glossary/commit/d6b9c58b9fd618851cb54d4c619cc100e4dae14b)), closes [#351](https://gitlab.com/hestia-earth/hestia-glossary/issues/351)
* **region:** add landTransformation lookups for 20 and 100 years average ([e1a9571](https://gitlab.com/hestia-earth/hestia-glossary/commit/e1a957141cd8be09b96f4e25dec4921cd7267516))

### [0.3.1](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.3.0...v0.3.1) (2022-05-10)


### Features

* **animalProduct:** add `tallow` ([2901dec](https://gitlab.com/hestia-earth/hestia-glossary/commit/2901deca713e124a1cf87cd6dffc5207d8ea8fae))
* **crop:** add `rapeseed leaf` ([35da2db](https://gitlab.com/hestia-earth/hestia-glossary/commit/35da2db6bce2873b0c0c0a95177340c673ac5c7b)), closes [#364](https://gitlab.com/hestia-earth/hestia-glossary/issues/364)
* **crop:** add leguminous feeds to glossary ([6b97be9](https://gitlab.com/hestia-earth/hestia-glossary/commit/6b97be923791262532ccca452df168cf7c1ba402))
* **excretaManagement:** add `excreta management, unspecified` ([28c07c8](https://gitlab.com/hestia-earth/hestia-glossary/commit/28c07c89ad353b26106a5ab1b88e89ccc00f5fc7))
* **liveAnimal:** add pig weaner, grower and fattener ([7f92c6c](https://gitlab.com/hestia-earth/hestia-glossary/commit/7f92c6c4e9c9c1b36b53f7afb2d76353b09e4e02))
* **measurement:** update allowedSiteType mapping ([4734ec6](https://gitlab.com/hestia-earth/hestia-glossary/commit/4734ec6e1d4ff764e1f98defddf18a256894a5b5))
* **pesticideAI:** add `l-lysine hydrochloride` ([0efa4b6](https://gitlab.com/hestia-earth/hestia-glossary/commit/0efa4b686b84c893bd2492f1e9bfdfc4825a9980))
* **pesticideAI:** add usetox freshwater ecotoxicity factors ([6e34106](https://gitlab.com/hestia-earth/hestia-glossary/commit/6e34106c923b2e6dc0d3e26899a47aaf2f973535)), closes [#5](https://gitlab.com/hestia-earth/hestia-glossary/issues/5)
* **water:** add `water, harvested rainfall` ([921d425](https://gitlab.com/hestia-earth/hestia-glossary/commit/921d4251f296fcc671248c2c79cd187fe2a0e225))

## [0.3.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.2.5...v0.3.0) (2022-04-23)


### ⚠ BREAKING CHANGES

* **soilTexture:** rename `siltyLoamSoilTexture` to `siltLoamSoilTexture`

### Features

* **crop:** add herbs ([d0e35f7](https://gitlab.com/hestia-earth/hestia-glossary/commit/d0e35f7eb845934440bfa92b0675e58f35eab264)), closes [#258](https://gitlab.com/hestia-earth/hestia-glossary/issues/258)
* **emission:** add `productTermTypesAllowed` ([ec5b8d9](https://gitlab.com/hestia-earth/hestia-glossary/commit/ec5b8d962ba7f9686f739b4adc9daa0b3b627bcb)), closes [#332](https://gitlab.com/hestia-earth/hestia-glossary/issues/332)
* **inorganicFertilizer:** add `euronature p26b (kg p2o5)` ([2d63c24](https://gitlab.com/hestia-earth/hestia-glossary/commit/2d63c243a758a9f775c186af29fae8579fd22b90))
* **measurement:** add missing `wind direction` terms ([f55c802](https://gitlab.com/hestia-earth/hestia-glossary/commit/f55c802b0781b8b7775ec74443e00b58d752a733))
* **model:** add `Pribyl (2010)` ([833adbf](https://gitlab.com/hestia-earth/hestia-glossary/commit/833adbf499bbce81783e580c6e89e0f50f00b692))
* **other:** add `bioplug` ([7c4572f](https://gitlab.com/hestia-earth/hestia-glossary/commit/7c4572fd8477a8353c917736a2f1361c4f291825))
* **other:** add `slavol` ([734cf29](https://gitlab.com/hestia-earth/hestia-glossary/commit/734cf29c22eabb9228f0ca1047397a430bda4b59))
* **pesticideAI:** add `succinate dehydrogenase inhibitors` ([df598a2](https://gitlab.com/hestia-earth/hestia-glossary/commit/df598a2dd8bf9cdee667eb45986edb38453b3da0)), closes [#345](https://gitlab.com/hestia-earth/hestia-glossary/issues/345)
* **property:** add terms for CaO, CaCO3 and MgO content ([20fdabc](https://gitlab.com/hestia-earth/hestia-glossary/commit/20fdabc78cbf40b0de941e21d3173f674221797f))
* **region:** add lookup cropland area ([6ddf70b](https://gitlab.com/hestia-earth/hestia-glossary/commit/6ddf70b250896b9fb27734455ead683f6dd2ac37))
* **resourceUse:** add terms for `land transformation, from cropland` ([7bd345a](https://gitlab.com/hestia-earth/hestia-glossary/commit/7bd345adda6c330f0d54222260da72a2d8a11cb1))
* **soilAmendment:** add `magnesium oxide` ([c0ba564](https://gitlab.com/hestia-earth/hestia-glossary/commit/c0ba5647f432e6c26d6584ee4444847de74bc365))
* **soilTexture:** rename `silty loam` to `silt loam` ([23e97ef](https://gitlab.com/hestia-earth/hestia-glossary/commit/23e97efbdc60bcade697871bdd05ee90fb6a8eb0))


### Bug Fixes

* **crop:** adds `dryMatter` to `asafoetida` ([29865fd](https://gitlab.com/hestia-earth/hestia-glossary/commit/29865fd5adf8154f83104ef0c7629cab45f94add)), closes [#340](https://gitlab.com/hestia-earth/hestia-glossary/issues/340)
* **crop:** store `dryMatter` as numbers not text ([ff45b69](https://gitlab.com/hestia-earth/hestia-glossary/commit/ff45b690fb82eec860c1cd9d5ec738a013bf6883))
* **measurement:** change unit for `Organic matter` terms ([da277e6](https://gitlab.com/hestia-earth/hestia-glossary/commit/da277e6220c5ffea4178358d57e4084a21610d12))
* **soilTexture:** rename `Silty loam (soil texture)` to `Silt loam (soil texture)` ([0031831](https://gitlab.com/hestia-earth/hestia-glossary/commit/00318311ef47d93d3b68a4139af0e3c191141ef3))

### [0.2.5](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.2.4...v0.2.5) (2022-04-12)


### Features

* **crop:** add herbs ([36d1a4a](https://gitlab.com/hestia-earth/hestia-glossary/commit/36d1a4a306bccac020206c5929fd70ec0ecfd8a1)), closes [#258](https://gitlab.com/hestia-earth/hestia-glossary/issues/258)
* **crop:** rename `bergamot` to `bergamot, fruit` ([19118d7](https://gitlab.com/hestia-earth/hestia-glossary/commit/19118d7965018e5b5ec5f4675d17a73ae7bfbf5f))


### Bug Fixes

* **region:** add missing ecoregion factors ([7491ec1](https://gitlab.com/hestia-earth/hestia-glossary/commit/7491ec1d32a257207b483c406b9b0c351cb88ae1))

### [0.2.4](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.2.3...v0.2.4) (2022-04-07)


### Bug Fixes

* **package:** fix `getArrayTreatment` file not found ([e3ecf68](https://gitlab.com/hestia-earth/hestia-glossary/commit/e3ecf68b80de6fbe07675ed901271114e9140205))

### [0.2.3](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.2.2...v0.2.3) (2022-04-07)


### Features

* **crop:** add `macadamia trees` ([6147221](https://gitlab.com/hestia-earth/hestia-glossary/commit/6147221cfbd54841c4593415adb31538053ce945))
* **crop:** add `macadamia, kernel` and `macadamia, shell` ([b98f0f0](https://gitlab.com/hestia-earth/hestia-glossary/commit/b98f0f0aede1771df758c77baf4fed765ab12a40))
* **crop:** add `pistachio trees` ([d068dea](https://gitlab.com/hestia-earth/hestia-glossary/commit/d068dea5f034e3752bdf43b988c019ee2095a636))
* **crop:** add `pistachio, kernel` and `pistachio, shell` ([ef83a3a](https://gitlab.com/hestia-earth/hestia-glossary/commit/ef83a3a99cd5352aecb8f02f35f24f490e1ea2b7))
* **crop:** add terms for `allspice` and `angelica` ([81423fc](https://gitlab.com/hestia-earth/hestia-glossary/commit/81423fc35834ea541ce159a990d7c188afe3858c)), closes [#258](https://gitlab.com/hestia-earth/hestia-glossary/issues/258)
* **crop:** rename `macadamia` to `macadamia, in shell` ([7a8c2ae](https://gitlab.com/hestia-earth/hestia-glossary/commit/7a8c2aec34bc26273e2d97d6ba94035806fabfd2))
* **crop:** rename `pistachio nut` to `pistachio, in shell` ([1000a17](https://gitlab.com/hestia-earth/hestia-glossary/commit/1000a1700d08b9d771b4afb2b3684eaa2c92d0f7))
* **measurement:** add `cation exchange capacity (per m3 soil)` ([3b3274d](https://gitlab.com/hestia-earth/hestia-glossary/commit/3b3274d0b6fdf4ca3995589bb67ca7408cd9edad))
* **measurement:** add `siteTypesAllowed` lookup ([eae1ecc](https://gitlab.com/hestia-earth/hestia-glossary/commit/eae1eccc1573a3f4b96a120751d1017d12567175)), closes [#334](https://gitlab.com/hestia-earth/hestia-glossary/issues/334)
* **measurement:** added missing mineral content ([4c00956](https://gitlab.com/hestia-earth/hestia-glossary/commit/4c00956a6cfb81445d094b806dfbe549351aea79)), closes [#333](https://gitlab.com/hestia-earth/hestia-glossary/issues/333)
* **package:** export `getArrayTreatment` function ([2a6fb05](https://gitlab.com/hestia-earth/hestia-glossary/commit/2a6fb053e1cdf076f1ffd0bfa1aba5b2bc7e349e))
* **property:** add `boron content` and `organic matter content` ([647d4c0](https://gitlab.com/hestia-earth/hestia-glossary/commit/647d4c07fcb331f96cd005beee32d2937b39427b))
* **property:** add `electrical conductivity` ([01a1cfe](https://gitlab.com/hestia-earth/hestia-glossary/commit/01a1cfe78af8c8f619da1a58269a28b160d39223))
* **soilAmendment:** add `liquid humus` and `solid humus` ([25ed774](https://gitlab.com/hestia-earth/hestia-glossary/commit/25ed774d70f9a5c5c31c65d3bae10904ba5e2cbc))


### Bug Fixes

* **crop:** errors in crop residue nitrogen content for `Bay leaf` ([df93e69](https://gitlab.com/hestia-earth/hestia-glossary/commit/df93e696a852a929ac5dce023619d4b6887aee27)), closes [#299](https://gitlab.com/hestia-earth/hestia-glossary/issues/299)

### [0.2.2](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.2.1...v0.2.2) (2022-03-28)


### Features

* **antibiotic:** add new antibiotics ([b6b892d](https://gitlab.com/hestia-earth/hestia-glossary/commit/b6b892d96a658ccf6e48b2fd3a74cc1eb56d808f)), closes [#129](https://gitlab.com/hestia-earth/hestia-glossary/issues/129)
* **crop:** add `subClassOf` for hazelnuts ([4e631ac](https://gitlab.com/hestia-earth/hestia-glossary/commit/4e631acf847afd64dcfd87f7a1bd5db8f178e508))
* **emission:** add `siteTypesAllowed` lookup ([88cf8d5](https://gitlab.com/hestia-earth/hestia-glossary/commit/88cf8d565085e99ff83ef3a01cbefd79f56fe7fa)), closes [#330](https://gitlab.com/hestia-earth/hestia-glossary/issues/330)
* **region:** index `area` data ([0c492ce](https://gitlab.com/hestia-earth/hestia-glossary/commit/0c492ce4cf8088cecad48619e246ccf11711e773))
* **resourceUse:** add `siteTypesAllowed` lookup ([f5b2074](https://gitlab.com/hestia-earth/hestia-glossary/commit/f5b20743d6848e35412281c955f96f7f27976ee5)), closes [#330](https://gitlab.com/hestia-earth/hestia-glossary/issues/330)
* add crop lookup template file ([fcfb9ee](https://gitlab.com/hestia-earth/hestia-glossary/commit/fcfb9ee042bca80f46bc26c707318bc76fa27aef))


### Bug Fixes

* **crop:** error in faostat lookups ([cd17a23](https://gitlab.com/hestia-earth/hestia-glossary/commit/cd17a237d4ac0dcdedf74f55ee3749739a2ceac1))

### [0.2.1](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.2.0...v0.2.1) (2022-03-15)


### Features

* **building:** add `infrastructure, unspecified` ([d0437d7](https://gitlab.com/hestia-earth/hestia-glossary/commit/d0437d772d792a7b9855aea7fa2b30d8a02a6c57))
* **crop:** update dry matter, rooting depth and seed rate ([d03d152](https://gitlab.com/hestia-earth/hestia-glossary/commit/d03d152a8d6c8733a27f77f6328d2e73d6b3776c))
* **crop:** update lookups for mushroom ([9b0f88b](https://gitlab.com/hestia-earth/hestia-glossary/commit/9b0f88bd91b37995de0ff101bcfc1923c8d0ab79))
* **material:** add `material, depreciated amount per cycle` terms ([ccf642b](https://gitlab.com/hestia-earth/hestia-glossary/commit/ccf642bdf55230003a1033983622f6d18c34586a)), closes [#285](https://gitlab.com/hestia-earth/hestia-glossary/issues/285)
* **measurement:** add earthworm counts ([e0ddbaa](https://gitlab.com/hestia-earth/hestia-glossary/commit/e0ddbaaa737b046b64da263be7cb834b5097e04e)), closes [#112](https://gitlab.com/hestia-earth/hestia-glossary/issues/112)
* **region:** add animal product average carcass weight lookup ([715a83e](https://gitlab.com/hestia-earth/hestia-glossary/commit/715a83ede90bc737ebd2c545ea30f8cd2d4b2642))
* **region:** generate continent data for crop Faostart yield ([2ead1c6](https://gitlab.com/hestia-earth/hestia-glossary/commit/2ead1c620e4f8d6ab86497a5d0e920da962f3cfb))
* **region:** replace `Commune` by `Comune` in Italia regions ([e1a2d95](https://gitlab.com/hestia-earth/hestia-glossary/commit/e1a2d952e6fb7b560b3a5115f96bad9ce94fa06d))


### Bug Fixes

* **crop:** error in FAOStat production linkage ([83b3a38](https://gitlab.com/hestia-earth/hestia-glossary/commit/83b3a385440603169b6cbdeca4aff5c62b3bde2f))
* **region lookup:** error in conveyancing factors ([66ef6e1](https://gitlab.com/hestia-earth/hestia-glossary/commit/66ef6e12b025961b3007af2023192eb52c9e9f20)), closes [#320](https://gitlab.com/hestia-earth/hestia-glossary/issues/320)
* **region:** fix lowercase id on `Western Asia` ([c333b79](https://gitlab.com/hestia-earth/hestia-glossary/commit/c333b79a295002a7e1675eac3cad2ca02657875b))
* **region:** replace `nan` values with `-` in ecoregion lookup ([e81edda](https://gitlab.com/hestia-earth/hestia-glossary/commit/e81edda8f046b6e7cee9c4ac0486e62468042c77))

## [0.2.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.1.2...v0.2.0) (2022-02-22)


### ⚠ BREAKING CHANGES

* **emission:** all emissions ending with `Ponds` have been renamed to `Systems`

### Features

* **animalProduct:** add `crustacean meal`, `eggs, hen, yolk` and `eggs, other bird, yolk` ([f450ab0](https://gitlab.com/hestia-earth/hestia-glossary/commit/f450ab0c3d17e6e85e380c06d7f3e49a0495c37c))
* **animalProduct:** add `krill meal` ([5500ef9](https://gitlab.com/hestia-earth/hestia-glossary/commit/5500ef9570ac47705d6c21defc34606827a70bde))
* **animalProduct:** add `poultry meal` ([7d9a353](https://gitlab.com/hestia-earth/hestia-glossary/commit/7d9a35348849c4b38eb4da53e6c009078be34489))
* **aquacultureManagement:** add `daily water exchange (m3)` ([4d2cfff](https://gitlab.com/hestia-earth/hestia-glossary/commit/4d2cfffdf0c2da4b4be30dd4762f53b91cdf95c4))
* **aquacultureManagement:** add `open net pen system` and `floating bag system` ([7332a1b](https://gitlab.com/hestia-earth/hestia-glossary/commit/7332a1b2d2445106325eb8f626e95835bb5c1353))
* **aquacultureManagement:** add `rearing volume` and `daily water exchange` ([1487b5a](https://gitlab.com/hestia-earth/hestia-glossary/commit/1487b5a429006912da34a91d3c4c6d014f59b748))
* **characterisedIndicator:** add `abiotic depletion potential` ([af8a979](https://gitlab.com/hestia-earth/hestia-glossary/commit/af8a979d0c073bf10671027e12cb83eff9e35212))
* **crop:** add `isOrchard` lookup ([92be05c](https://gitlab.com/hestia-earth/hestia-glossary/commit/92be05c09fb2cc6fe6518717a857448cef66c333)), closes [#310](https://gitlab.com/hestia-earth/hestia-glossary/issues/310)
* **crop:** add `wheat, middlings` ([effe1b1](https://gitlab.com/hestia-earth/hestia-glossary/commit/effe1b15faef5d1bb2b9e5c197d8eff7f0e28d31))
* **crop:** add crop residue values ([d53d4ff](https://gitlab.com/hestia-earth/hestia-glossary/commit/d53d4ffd9affaf41a4153db2bf54dd11cbdc306d))
* **crop:** add further rooting depth lookups for rapeseed ([1121cba](https://gitlab.com/hestia-earth/hestia-glossary/commit/1121cba27e17b562d525b8273ca068c3188a38b6))
* **crop:** add missing IPCC 2019 factors for AG residue ([8e353e2](https://gitlab.com/hestia-earth/hestia-glossary/commit/8e353e27da769de05bc7ab39e0d1965c5bae3bd8)), closes [#152](https://gitlab.com/hestia-earth/hestia-glossary/issues/152)
* **crop:** add multiple terms ([d5b96f1](https://gitlab.com/hestia-earth/hestia-glossary/commit/d5b96f11edda44a284d2ebbabf0e010e5a49a853))
* **cropEstablishment:** add synonyms for `Seed depth` ([b644877](https://gitlab.com/hestia-earth/hestia-glossary/commit/b644877f4d03520cf1502a33755b4d73d85f9b03))
* **cropEstablishment:** add terms `seed spacing` and `seed density` ([08fc135](https://gitlab.com/hestia-earth/hestia-glossary/commit/08fc1356cef7494503bf9831cc9edd979eec92cb))
* **crop:** fix errors in lookups, colour code lookup values ([6024af7](https://gitlab.com/hestia-earth/hestia-glossary/commit/6024af76cdfb889dd20c18694e106cd8f7e516c4))
* **crop:** update lookups for coconuts ([270349c](https://gitlab.com/hestia-earth/hestia-glossary/commit/270349c086b6fba876193c1a0424d1ebca534083))
* **crop:** update property lookup ([644b15b](https://gitlab.com/hestia-earth/hestia-glossary/commit/644b15baf907d38da4ce7c010d98cacdd2f473a0))
* **crop:** update slope and intercept look-up values ([113a9aa](https://gitlab.com/hestia-earth/hestia-glossary/commit/113a9aa1851526f0c5f22aacdbd5faf9f2d40d1a))
* **electricity:** add `electricity, biogas` ([7cbcdf6](https://gitlab.com/hestia-earth/hestia-glossary/commit/7cbcdf68e9da7288ef6540b3aa64cea615dc26b2))
* **fuel:** add `coal, unspecified` ([5982ec8](https://gitlab.com/hestia-earth/hestia-glossary/commit/5982ec8e5a947005fc3bb400fde0e3b0dd470ae3))
* **fuel:** add `heating oil` ([b5bf0c6](https://gitlab.com/hestia-earth/hestia-glossary/commit/b5bf0c6a5ef1d6022e004ce1926aa6c61f6536ef))
* **liveAquaticSpecies:** add `arctic char` and `atlantic cod` ([d94b94c](https://gitlab.com/hestia-earth/hestia-glossary/commit/d94b94c92af071f73387367fc8c7cbad199c9a7e))
* **liveAquaticSpecies:** add `broodfish` ([5eaebbd](https://gitlab.com/hestia-earth/hestia-glossary/commit/5eaebbdee50a82a0e6507b8c53100aeef17baf90))
* **liveAquaticSpecies:** add `roe` ([28ffa98](https://gitlab.com/hestia-earth/hestia-glossary/commit/28ffa98d16e6d5365d8336fba5f328cdec1629d8))
* **liveAquaticSpecies:** add `shrimp, unspecified` and `brine shrimp` ([b7dc360](https://gitlab.com/hestia-earth/hestia-glossary/commit/b7dc360b15cc321872ce4ee96f323c28b46c81ff))
* **material:** add `low-density polyethylene` and `high-density polyethylene` ([67c552c](https://gitlab.com/hestia-earth/hestia-glossary/commit/67c552c1e2785e44c1f40eacbcab35292c559d5f))
* **material:** add multiple materials ([6a1d1e6](https://gitlab.com/hestia-earth/hestia-glossary/commit/6a1d1e68895b824f2456d046163523219a552d14))
* **material:** add multiple terms ([a33f98b](https://gitlab.com/hestia-earth/hestia-glossary/commit/a33f98b55d1fc9d01bb8e75a2ba33292ad73514b))
* **measurement:** add `air temperature` as synonym of temperature terms ([e67c291](https://gitlab.com/hestia-earth/hestia-glossary/commit/e67c29143e548068ffa2cff214ce4449e67b51e8))
* **measurement:** add `dew point` terms ([2d68382](https://gitlab.com/hestia-earth/hestia-glossary/commit/2d68382812f3f8ff08bb93b598de276a7a0a3803))
* **measurement:** add `point measurement` options for temperature and pressure ([d53e949](https://gitlab.com/hestia-earth/hestia-glossary/commit/d53e949478c8af6aa3b086c5bb627b57ea9ea826)), closes [#309](https://gitlab.com/hestia-earth/hestia-glossary/issues/309)
* **measurement:** add `precipitation` terms ([46b6697](https://gitlab.com/hestia-earth/hestia-glossary/commit/46b669789137858749291549111e148058b1c67a)), closes [#135](https://gitlab.com/hestia-earth/hestia-glossary/issues/135)
* **measurement:** add `wind speed` ([10af912](https://gitlab.com/hestia-earth/hestia-glossary/commit/10af91261b0ec5a414c1e3b13d3d21124044ac36))
* **measurement:** add air pressure terms ([717afdb](https://gitlab.com/hestia-earth/hestia-glossary/commit/717afdb7a3aa57bd744dd692cd9fe689521aa9df))
* **measurement:** add leaf wetness term ([d73f20d](https://gitlab.com/hestia-earth/hestia-glossary/commit/d73f20d01abc1a4e50fa6c01a7f0de5d541c8b09))
* **measurement:** add synonyms to `soil temperature` ([cf18bfe](https://gitlab.com/hestia-earth/hestia-glossary/commit/cf18bfe1c69c7a3e8608d1582956aaffd6265573))
* **measurement:** add synonyms to soil moisture ([a144659](https://gitlab.com/hestia-earth/hestia-glossary/commit/a14465909d166c4cca246c0864f6bd45544853e0))
* **measurement:** add terms for soil CaCO3 ([8c82484](https://gitlab.com/hestia-earth/hestia-glossary/commit/8c82484fa552b3be0f0a4a79a92ad8b656193b90))
* **measurement:** add wet bulb temperature ([f590939](https://gitlab.com/hestia-earth/hestia-glossary/commit/f590939c5c9dbff14f457b979e3ce6cffebb80e3))
* **measurement:** add wind direction ([977d934](https://gitlab.com/hestia-earth/hestia-glossary/commit/977d934ed1492f11f87e433b1ec82e2a1c345c90))
* **measurement:** split solar radiance into total, direct, and diffuse ([a7e8937](https://gitlab.com/hestia-earth/hestia-glossary/commit/a7e89371b7d875eef8c7e601b4f60471599d4cd4))
* **methodMeasurement:** add `anemometer` ([433b8be](https://gitlab.com/hestia-earth/hestia-glossary/commit/433b8be77836c52598ceecf540dce654102c770c))
* **methodMeasurement:** add Bray P1, Mehlich 3, and Olsen P tests ([4073885](https://gitlab.com/hestia-earth/hestia-glossary/commit/4073885e3781aadcef74b0b0487977c05e7511f8))
* **methodMeasurement:** add multiple tests for soil p ([81d09da](https://gitlab.com/hestia-earth/hestia-glossary/commit/81d09dad47eb42d57b54c35d51ec7965639e1c44))
* **methodMeasurement:** add terms for measuring solar radiation ([6f8b7b3](https://gitlab.com/hestia-earth/hestia-glossary/commit/6f8b7b3ee2bb7a8ac0adab293ab487a5e8e83dd5))
* **model:** add `cumulative energy demand (CED) method` ([353d772](https://gitlab.com/hestia-earth/hestia-glossary/commit/353d77217320af0f0917d73062e998dbff4fef4f))
* **model:** add `eco-indicator 95` ([75de064](https://gitlab.com/hestia-earth/hestia-glossary/commit/75de064316aed85b68372e6584f60eb0639c3b95))
* **model:** add `Hestia aggregated data` as model ([8c4dd8f](https://gitlab.com/hestia-earth/hestia-glossary/commit/8c4dd8f578b529b49070759b69678576cae42f90)), closes [#301](https://gitlab.com/hestia-earth/hestia-glossary/issues/301)
* **model:** add missing models in indicator-model mapping ([b5c097b](https://gitlab.com/hestia-earth/hestia-glossary/commit/b5c097b852274cacf8d91ac8e7e231d45845a458))
* **model:** add ReCiPe 2008 and `edip (2003)` ([c46f6fb](https://gitlab.com/hestia-earth/hestia-glossary/commit/c46f6fb6f6d19bc63fc52ec85984bd86867714ec))
* **operation:** add `fertigating` and `fertilizing, deep placement` ([8863ec6](https://gitlab.com/hestia-earth/hestia-glossary/commit/8863ec612f2e1a0a7f0cf8e4884a7055a42a3919)), closes [#97](https://gitlab.com/hestia-earth/hestia-glossary/issues/97)
* **operation:** add `pond aeration` ([865b2d3](https://gitlab.com/hestia-earth/hestia-glossary/commit/865b2d3242efec5745e81aff5f409afa7699b120)), closes [#246](https://gitlab.com/hestia-earth/hestia-glossary/issues/246)
* **operation:** add `refrigeration` ([a0c8a21](https://gitlab.com/hestia-earth/hestia-glossary/commit/a0c8a21d1f5b1bf7c514c2a96adbbf2c388c5ae7))
* **other:** add `ice` and `liquid oxygen` ([94c50de](https://gitlab.com/hestia-earth/hestia-glossary/commit/94c50de2d6cfa93ae424b99625de1e5f0cc2d78c))
* **property:** add `dha content` and `epa content` ([c9bc35c](https://gitlab.com/hestia-earth/hestia-glossary/commit/c9bc35ce1cd0da4043b834773919bee785fb06a6))
* **region:** add `animal product` price lookup ([24287bc](https://gitlab.com/hestia-earth/hestia-glossary/commit/24287bc986998752f37ed0aaccb79c6632860ae8))
* **region:** assign subclass to additional regions ([9f60272](https://gitlab.com/hestia-earth/hestia-glossary/commit/9f60272d4fc8fb1170d3c4ebdbca377ceab148ce)), closes [#250](https://gitlab.com/hestia-earth/hestia-glossary/issues/250)
* **region:** assign subclass to regions ([831b015](https://gitlab.com/hestia-earth/hestia-glossary/commit/831b015351fcd892a5984f423669fd7d36b5a515)), closes [#250](https://gitlab.com/hestia-earth/hestia-glossary/issues/250)
* **region:** assign subclass to regions ([617ae4d](https://gitlab.com/hestia-earth/hestia-glossary/commit/617ae4df7c7f2289f42abc5599b1a8e11c78bf47)), closes [#250](https://gitlab.com/hestia-earth/hestia-glossary/issues/250)


### Bug Fixes

* **characterisedIndicator:** change `fossile resource scarcity` to `fossil resource scarcity` ([d98bb4c](https://gitlab.com/hestia-earth/hestia-glossary/commit/d98bb4c4de48c045b046422d45631622ef68c40c))
* **characterisedIndicator:** update column headers for ReCiPe indicators in model mapping ([61cd703](https://gitlab.com/hestia-earth/hestia-glossary/commit/61cd703caa579ca7ff73cb97fda245a8cf96ed47))
* **crop:** correct residue N content for `mung bean, dry` in lookup ([af37023](https://gitlab.com/hestia-earth/hestia-glossary/commit/af37023da1ddb3862ee7b21a0e06e3ab475e8c01))
* **cropEstablishment:** typo in units for `planting density` ([8ca8f1a](https://gitlab.com/hestia-earth/hestia-glossary/commit/8ca8f1adc8fd740eecd9897ce8584e9f25132661))
* **crop:** update feedipedia link for `tomato, leaves` and `tomato, stems` ([48ac082](https://gitlab.com/hestia-earth/hestia-glossary/commit/48ac082831ca383893db15e22682d9cb2950ab5c))
* **emission:** replace `aquaculture ponds` with `aquaculture systems` ([e5cb419](https://gitlab.com/hestia-earth/hestia-glossary/commit/e5cb419b91d0558ba0f33d52eed1a1298dfb7e54))
* **excretaManagement:** update`excretion into water body` lookup ([f1899dc](https://gitlab.com/hestia-earth/hestia-glossary/commit/f1899dc5133d14ad46de7125ec1229ecdbcec8e8))
* **measurement:** correct typo in definition of evapotranspiration ([113a353](https://gitlab.com/hestia-earth/hestia-glossary/commit/113a35314a0466b265dcdffbad71911b85992bfd))
* **methodMeasurement:** remove duplicated olsen method ([a4a015f](https://gitlab.com/hestia-earth/hestia-glossary/commit/a4a015fc1e7e9179b5fbbbae6144959d5488bb89))
* **model:** fix links in `ecoIndicator95` and `edip2003` definitions ([c168052](https://gitlab.com/hestia-earth/hestia-glossary/commit/c1680520b017485f73b19937cb96dffa5bcd9cee))
* **other:** correct density for `hexane` and `mineral oil` ([56a8058](https://gitlab.com/hestia-earth/hestia-glossary/commit/56a8058de1fc0ee4f30bb6d8bc81af41ee02192b))
* **pesticideAI:** remove `glass fibers` ([9a8fc2c](https://gitlab.com/hestia-earth/hestia-glossary/commit/9a8fc2cccaa11c6788b6c8921d118a237470f69c))
* **region:** add missing FAO groups for `animalProduct` price ([5ef23c9](https://gitlab.com/hestia-earth/hestia-glossary/commit/5ef23c997b142405ca69258fddc71614954c52fe))
* **region:** update region id in lookups ([74a61bd](https://gitlab.com/hestia-earth/hestia-glossary/commit/74a61bd91912a65f62358aed9e61a15178f51b97))

### [0.1.2](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.1.1...v0.1.2) (2022-01-12)


### Features

* **animalProduct:** add `finfish meal` and `finfish by-product meal` ([f9bcd10](https://gitlab.com/hestia-earth/hestia-glossary/commit/f9bcd105e3e6ea8230a5d725f0bea296e521e329))
* **aquacultureManagement:** add `flow-through system` ([8da08bc](https://gitlab.com/hestia-earth/hestia-glossary/commit/8da08bc2511cfac00af75b8b98af8a40d3d7b36e))
* **aquacultureManagement:** add `recirculating aquaculture system` ([7653e87](https://gitlab.com/hestia-earth/hestia-glossary/commit/7653e8732ef1ba64dc91422880c0aa3447ecc4c4))
* **aquacultureManagement:** improve definition of `monoculture, single batch production` and `monoculture, multiple batch production` ([72f5f34](https://gitlab.com/hestia-earth/hestia-glossary/commit/72f5f340b4199e46eedaf533cafde8b2c61fc9d2))
* **building:** delete `recirculating aquaculture system` ([d3d6bbe](https://gitlab.com/hestia-earth/hestia-glossary/commit/d3d6bbe9ced7168ddf3a3108ffb065fdd30edc3e))
* **characterisedIndicator:** add `agricultural land occupation potential` and `fine particulate matter formation` ([be89b9d](https://gitlab.com/hestia-earth/hestia-glossary/commit/be89b9d01414e5f72ac121b7d415d43a773505ad))
* **characterisedIndicator:** add `ReCiPe` as synonym for ReCiPe indicators ([6238d0c](https://gitlab.com/hestia-earth/hestia-glossary/commit/6238d0c28c74a86ebf3e1cc80909ce1cd6ab0d6b))
* **characterisedIndicator:** add missing ReCiPe midpoint indicators ([f575f87](https://gitlab.com/hestia-earth/hestia-glossary/commit/f575f87998efec15cbc3ca995e500136ddb540a5))
* **characterisedIndicator:** add ReCiPe conversion factors for damage to human health ([c495936](https://gitlab.com/hestia-earth/hestia-glossary/commit/c4959365616d4dbe4549ade3a4322142533a44b5))
* **characterisedIndicator:** add ReCiPe midpoint to endpoint conversion factors for ecosystems damage ([f9583ca](https://gitlab.com/hestia-earth/hestia-glossary/commit/f9583ca0e0be7fd89e5d1cdb2db279e36e9362f7))
* **characterisedIndicator:** add synonyms for `photochemical ozone creation potential`, `freshwater aquatic ecotoxicity potential` and `marine aquatic ecotoxicity potential` ([a48482e](https://gitlab.com/hestia-earth/hestia-glossary/commit/a48482eec3da8d52702e0d78b4b4ce26efc90a15))
* **characterisedIndicator:** fix unit for `eutrophication potential, excluding fate` ([e161fdc](https://gitlab.com/hestia-earth/hestia-glossary/commit/e161fdcbbff57845ab9f7ef8fa01c8b92a654b18))
* **characterisedIndicator:** update model mapping with ReCiPe 2016 perspectives ([39ccddb](https://gitlab.com/hestia-earth/hestia-glossary/commit/39ccddbd811b763ba47fabc3c83db54b6e4726b9))
* **characterisedIndicator:** update ReCiPe 2017 allowed model mapping ([89a3bf5](https://gitlab.com/hestia-earth/hestia-glossary/commit/89a3bf5b42a55efc9440c0fa0992280a3b36a9f5))
* **crop:** add `coconut, fresh meat`, `coconut, dry meat` and 'coconut, meal` ([be3392c](https://gitlab.com/hestia-earth/hestia-glossary/commit/be3392c0b4256aeac743de8795986c09ea1d7d32))
* **crop:** add `maize, flour` ([8524064](https://gitlab.com/hestia-earth/hestia-glossary/commit/852406487d5477d23e07e6bd6fe38b07b33bf396))
* **crop:** add further crop residue factors for vegetables ([5fc0e83](https://gitlab.com/hestia-earth/hestia-glossary/commit/5fc0e836b399fba609bd85fbc56ebb9831b09868))
* **crop:** add new terms for canola oil and meal and update rapeseed fields ([7211ce3](https://gitlab.com/hestia-earth/hestia-glossary/commit/7211ce3510c01323241720eb78396d42995f9055))
* **crop:** add synonyms for `soybean, meal` ([c481c9e](https://gitlab.com/hestia-earth/hestia-glossary/commit/c481c9e146a25f23e3f637137f0a07c685ff5cfc))
* **emission:** add hierarchist CFs for fine particulate matter formation ([e62248c](https://gitlab.com/hestia-earth/hestia-glossary/commit/e62248c4bb56cf9fbf6d463ceb853404e2017687))
* **emission:** add ozone related ReCiPe CFs ([2589494](https://gitlab.com/hestia-earth/hestia-glossary/commit/258949436755bee964bd22b26ac172009f363112))
* **emission:** add ReCiPe 2017 CFs for fine particulate matter formation ([e32079e](https://gitlab.com/hestia-earth/hestia-glossary/commit/e32079ee9db327fb2197dad5a4f83c4eb2d9e526))
* **emission:** add ReCiPe CFs for freshwater eutrophication ([17a42e7](https://gitlab.com/hestia-earth/hestia-glossary/commit/17a42e759821bc85069a4a48940bf76b8e892b57))
* **emission:** add ReCiPe CFs for marine eutrophication ([e5cd712](https://gitlab.com/hestia-earth/hestia-glossary/commit/e5cd712848ca6a07925984dba94e666364fa73c6))
* **emission:** add ReCiPe CFs for terrestrial acidification ([ccce0d9](https://gitlab.com/hestia-earth/hestia-glossary/commit/ccce0d954ba3f872c6a1d7abfc582098f3cb1b7c))
* **endpointIndicator:** create new glossary and add ReCiPe endpoints ([1f31ce3](https://gitlab.com/hestia-earth/hestia-glossary/commit/1f31ce323fcafad3ed2c6d9e2e6a2730318c7f2f))
* **fuel:** add ReCiPe CFs for fossil resource scarcity and synonym for `lignite` ([652f254](https://gitlab.com/hestia-earth/hestia-glossary/commit/652f254ac0c4a46025cb3bf892dfa438f4aeea2a))
* **fuel:** add ReCiPe conversion factors for damage to resource availability ([2791eae](https://gitlab.com/hestia-earth/hestia-glossary/commit/2791eae65fdfe7e1df900d9d0182734575a0bfff))
* **landUseManagement:** add `green manure incorporated` ([f4ffe1f](https://gitlab.com/hestia-earth/hestia-glossary/commit/f4ffe1f9341c5a13c6101ce4a209416fd22117c4))
* **liveAquaticSpecies:** add `walking catfish` ([1cb8aab](https://gitlab.com/hestia-earth/hestia-glossary/commit/1cb8aab621747edb40712fb31c2580605d209b18))
* **liveAquaticSpecies:** add Myanmar names as synonyms for multiple fish species ([b14700f](https://gitlab.com/hestia-earth/hestia-glossary/commit/b14700f92c56a5725a8831708ada665d4a1b48a4))
* **measurements:** add `water pH` and improve `soil pH` definition ([8baef97](https://gitlab.com/hestia-earth/hestia-glossary/commit/8baef97f53a906124317511ef85d12aa2d97db45))
* **model:** add links to `ReCiPe 2017` description ([f528a03](https://gitlab.com/hestia-earth/hestia-glossary/commit/f528a03d72f8592a2a1fd10b31491ebceb238aa6))
* **model:** rename ReCiPe 2016 models ([aae22c1](https://gitlab.com/hestia-earth/hestia-glossary/commit/aae22c11423fb414598b88a5f1f7cf9f7b3a0efa))
* **model:** split `ReCipe 2017` into three perspectives and change to 2016 ([62b0ab2](https://gitlab.com/hestia-earth/hestia-glossary/commit/62b0ab276e6bb77aea0f3e1b0d686db698cbc507))
* **pesticideAI:** add `alkylphenol ethoxylates` and delete `partna` ([fa4c9cb](https://gitlab.com/hestia-earth/hestia-glossary/commit/fa4c9cbbedaf82ce7674e9426735fe8dfebda329))
* **pesticideAI:** add ozone related ReCiPe CFs ([4cbbea9](https://gitlab.com/hestia-earth/hestia-glossary/commit/4cbbea9ca4401a01ec467e291f66bca54950a0d0))
* **pesticideAI:** add ReCiPe CFs for ecotoxity and human toxicity ([f5c1e0f](https://gitlab.com/hestia-earth/hestia-glossary/commit/f5c1e0feb600716117eb3cdf7492033b5918cf90))
* **pesticideAI:** delete dental materials ([b953852](https://gitlab.com/hestia-earth/hestia-glossary/commit/b953852dcb5831ad15d0ac886560624008baf40c))
* **pesticideBrandName:** add `majestik` ([83f6a90](https://gitlab.com/hestia-earth/hestia-glossary/commit/83f6a9015511faf73d05dfdcf11ebe2906b3af31))
* **pesticideBrandName:** add `partna (adjuvant)` ([42996bc](https://gitlab.com/hestia-earth/hestia-glossary/commit/42996bc910d16e24eed518f551d9bb51c367a9c8))
* **pesticideBrandName:** add `signum fungicide` ([0deaa03](https://gitlab.com/hestia-earth/hestia-glossary/commit/0deaa032788636b4d6f91a1e829b0052941c1fc5))
* **pesticideBrandName:** add `stomp 400 SC` ([fc61834](https://gitlab.com/hestia-earth/hestia-glossary/commit/fc61834cb0c78bb29962218a6cca30c6ef84fbe6))
* **pesticideBrandName:** add synonym for `kresoxim-methyl 50 WDG fungicide` ([b01085e](https://gitlab.com/hestia-earth/hestia-glossary/commit/b01085e1c4305f007afbe361f93a96673abb4c40))
* **pesticideBrandName:** correct % active ingredient for `amistar fungicide` ([a8d8702](https://gitlab.com/hestia-earth/hestia-glossary/commit/a8d8702c6c3f86200bedfac96f46a3500ed5bf55))
* **pesticideBrandName:** group all brands with 19.7% myclobutanil under `myclobutanil 20 EW` ([bf1c347](https://gitlab.com/hestia-earth/hestia-glossary/commit/bf1c3472af64732f8c4b0083115daf81dfd93bc8))
* **resourceUse:** add ReCiPe 2017 CFs for agricultural land occupation potential ([60f3aa5](https://gitlab.com/hestia-earth/hestia-glossary/commit/60f3aa5687f8f6267bca5b5e2453059a7f087cfd))
* **soilAmendment:** add `quicklime` as synonym for `burnt lime` ([30280d6](https://gitlab.com/hestia-earth/hestia-glossary/commit/30280d6784821fdde1657ce0e133e6f8514f22d8))
* **soilAmendment:** add synonyms for `epsom salts` ([e1ea8b3](https://gitlab.com/hestia-earth/hestia-glossary/commit/e1ea8b3acb16f5f8f0730c3985d93c5412c7bcb9))
* **system:** add `mixed farming` ([900ba96](https://gitlab.com/hestia-earth/hestia-glossary/commit/900ba96617d1b50c1cfcaebbd9db0188cc9a0555))
* **system:** add definition for `integrated production` and wikipedia links for all terms ([e9a000d](https://gitlab.com/hestia-earth/hestia-glossary/commit/e9a000d37bd0e4bc30a28b9b6552e481452b96bc))


### Bug Fixes

* **characterisedIndicator:** fix error in lookup ([055a714](https://gitlab.com/hestia-earth/hestia-glossary/commit/055a7147380e813f807d07bff1994d69a6fcfdfd))
* **characterisedIndicator:** fix lookup cells format ([a08826e](https://gitlab.com/hestia-earth/hestia-glossary/commit/a08826e4549565b9c0ce893ad0422a093f44a5fb))
* **characterisedIndicator:** replace semicolons with commas in model mapping file ([353250c](https://gitlab.com/hestia-earth/hestia-glossary/commit/353250c76c5a0f93f3b93d3e57b4313a4cd732ba))
* **emission:** remove double `"` from lookup term and rename it ([ba69df1](https://gitlab.com/hestia-earth/hestia-glossary/commit/ba69df161b5dec0bcff40c0a3dfad58350be7612))
* **pesticideAI:** add missing value in lookup ([5d98e54](https://gitlab.com/hestia-earth/hestia-glossary/commit/5d98e542fad5b33ba6a068c9445470023d58ae62))
* **pesticideAI:** correct errors in ReCiPe individualist CFs ([0c83b00](https://gitlab.com/hestia-earth/hestia-glossary/commit/0c83b001f6bbfe98c754b218fbbf78e8fc074a84))
* **pesticideAI:** fix lookup cells format ([8a80a53](https://gitlab.com/hestia-earth/hestia-glossary/commit/8a80a53d47c9d2b7889f57e25194de5398fc015f))
* **pesticideAI:** replace `hierarchic` with `hierarchist` ([b836d0d](https://gitlab.com/hestia-earth/hestia-glossary/commit/b836d0d0cb89184f39fdb05d84573f27b2a96c96))

### [0.1.1](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.1.0...v0.1.1) (2021-11-19)


### Features

* **antibiotic:** add 'vaccine unspecified (AI)' ([e3d6a50](https://gitlab.com/hestia-earth/hestia-glossary/commit/e3d6a50d21c0fb3ab33b7cf6b2a5479d5e931055))
* **crop:** add 'generic crop, meal' ([fb5b6ca](https://gitlab.com/hestia-earth/hestia-glossary/commit/fb5b6caa774999fe214abe13bac1204b8ca9f251))
* **crop:** add `bamboo` terms and associated lookups ([43ec0fc](https://gitlab.com/hestia-earth/hestia-glossary/commit/43ec0fc39e2f33646f90261e2f279c11fa6f4b84))
* **crop:** add brewers' spent grain terms ([b06d92a](https://gitlab.com/hestia-earth/hestia-glossary/commit/b06d92a264bff033d70aeb63a7be10e4ef507858))
* **crop:** add crop residue slope and intercept for 'bamboo, shoots' ([ec40886](https://gitlab.com/hestia-earth/hestia-glossary/commit/ec40886a6d33914e72befc850860498049dfdf42))
* **crop:** add synonyms and definitions for bamboo terms ([6d99da3](https://gitlab.com/hestia-earth/hestia-glossary/commit/6d99da33ac0151423064f66b904ca0ae211b09a5))
* **crop:** rename 'grapes (for wine)' to 'grapes (for wine and liquor)' ([7495c69](https://gitlab.com/hestia-earth/hestia-glossary/commit/7495c69ec526838dd5848f8670879d08dc0a6b35))
* **crop protection:** add 'polytunnel' and update 'venlo greenhouse' ([7f5974b](https://gitlab.com/hestia-earth/hestia-glossary/commit/7f5974bdb9e718d9d00f336512fdde18db649f11))
* **land use management:** fix error in 'permanent raised beds' definition ([fbe7deb](https://gitlab.com/hestia-earth/hestia-glossary/commit/fbe7deba00fa07fccca3c59a20d1b486d49a7388))
* **land use management:** update 'cropping duration' definition ([3398c9c](https://gitlab.com/hestia-earth/hestia-glossary/commit/3398c9cacffd02f74bd5ae7cb2231fd98b1bfcfd))
* **live animal:** add 'chicken (semi-broilers)' and add chicken definitions ([c4b5394](https://gitlab.com/hestia-earth/hestia-glossary/commit/c4b5394e88512d49593527da8df5c1cdc9f81d78))
* **live animal:** add `chicken (semi-broilers)` to IPCC 2019 tier 2 CH4 lookup ([a785dbd](https://gitlab.com/hestia-earth/hestia-glossary/commit/a785dbdf064c55ebb21b35bb1c8f61a0ff635287))
* **live animal:** remove duplicated column ([321f767](https://gitlab.com/hestia-earth/hestia-glossary/commit/321f767e7f6cc12cba60c565422b36243ff27e5b))
* **live aquatic species:** add 'carp, unspecified' ([c8f3529](https://gitlab.com/hestia-earth/hestia-glossary/commit/c8f3529b4adbdcc40a36775aa1afb21dcfbd6317))
* **live aquatic species:** add synonym and scientific name for 'migral carp' ([0664eca](https://gitlab.com/hestia-earth/hestia-glossary/commit/0664eca192510f8df235908df4a5d04e77f68d0f))
* **live aquatic species:** add synonyms for 'catla' and 'roho labeo' ([ce81ae8](https://gitlab.com/hestia-earth/hestia-glossary/commit/ce81ae8d62a44a9bab1d5563b4b13575a603ab26))
* **live aquatic species:** add synonyms for 'pirapatinga' and 'striped catfish' ([e02ec57](https://gitlab.com/hestia-earth/hestia-glossary/commit/e02ec57b821e5822d688956fffc5a1aa4f342f76))
* **operation:** add 'soil fumigation, with shank injection equipment' ([23edf09](https://gitlab.com/hestia-earth/hestia-glossary/commit/23edf095fe0fe5f259435e4f68e7bc573a1eff44))
* **operation:** add multiple aquaculture operations ([abdf474](https://gitlab.com/hestia-earth/hestia-glossary/commit/abdf474bf450e6254934ed252070ed3a5e8038df)), closes [#284](https://gitlab.com/hestia-earth/hestia-glossary/issues/284)
* **other:** add 'vitamins, unspecified' ([b17d845](https://gitlab.com/hestia-earth/hestia-glossary/commit/b17d845d9539f2eff9c9b292ddf455782addef8b))
* **pesticide AI:** add 'bacillus subtilis QST 713' ([cedbfa7](https://gitlab.com/hestia-earth/hestia-glossary/commit/cedbfa7386886ba68d551162b14abdd115428622))
* **pesticide AI:** add 'bactericide unspecified (AI)', 'Disinfectant unspecified (AI) and 'Antiseptic unspecified (AI) ([b62b459](https://gitlab.com/hestia-earth/hestia-glossary/commit/b62b4590288c2447cdb79f4348f01d848fc35b9c))
* **pesticide AI:** add 'Phthalimide compounds, unspecified' ([5744cdb](https://gitlab.com/hestia-earth/hestia-glossary/commit/5744cdb2ef77c74d0231eecfff4236a203d3ef7f))
* **pesticide brand name:** add 'serenade' ([e41fe79](https://gitlab.com/hestia-earth/hestia-glossary/commit/e41fe798567aeb8533b60c1b9c7b2e6c5d4eaca0))
* **pesticide brand name:** add 'xentari WDG' ([6a9db22](https://gitlab.com/hestia-earth/hestia-glossary/commit/6a9db22f5a37f0b7c6f4c94c356601d6cf9bf3c8))
* **property:** add 'post-harvest losses', 'percentage of product sold' and 'percentage of self-consumption' ([af72e31](https://gitlab.com/hestia-earth/hestia-glossary/commit/af72e31ca37c0b6da106f5826bded2e0c4b683fd))
* **system:** add 'container-grown crops' ([2a54032](https://gitlab.com/hestia-earth/hestia-glossary/commit/2a54032f096f86db36d9de3e90205aed8bf2bb91))

## [0.1.0](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.0.11...v0.1.0) (2021-11-06)


### Features

* add Dammgen (2009) model ([e11e5fe](https://gitlab.com/hestia-earth/hestia-glossary/commit/e11e5fe75d9bab0f372b22ac0d3b2c22f3b79bea))
* add NH3 lookups ([09072da](https://gitlab.com/hestia-earth/hestia-glossary/commit/09072da69158905c14b5a209d84988caed80201b))
* link each term to activity in ecoinvent database ([cb02d13](https://gitlab.com/hestia-earth/hestia-glossary/commit/cb02d13ebc59da839702b01e16a0a1ac3a22fa90))
* **antibiotic:** add 'antibiotic unspecified (AI)' ([f4ba9e1](https://gitlab.com/hestia-earth/hestia-glossary/commit/f4ba9e1dab8504105ca68ee48dfa3abe84e536e0))
* **antibiotic:** add 'oxytetracycline' ([bb73952](https://gitlab.com/hestia-earth/hestia-glossary/commit/bb73952335a839edd0d5aad6b1ccde034a99e216))
* **antibiotic:** update 'oxytetracycline' definition ([0f91752](https://gitlab.com/hestia-earth/hestia-glossary/commit/0f91752c01b45d305d9c39c0b1d10f69ea5b31f0))
* **aquacultureManagement:** delete `Grow-out time` term, as this is captured with `cycleDuration` ([f798b87](https://gitlab.com/hestia-earth/hestia-glossary/commit/f798b87e9ad7c0ece10fa32801425ddda4957ac6))
* **aquacultureManagement:** improve name and definition of "yield of target species" ([f604d09](https://gitlab.com/hestia-earth/hestia-glossary/commit/f604d098665d96315088120402f774b515fc31ae))
* **aquacultureManagement:** remove `Survival to harvest` term as poorly defined ([5ee36d0](https://gitlab.com/hestia-earth/hestia-glossary/commit/5ee36d0bdcf290381bd5899e9d516677ee68ba71))
* **aquacultureManagement:** rename `yield of target species` to `Yield of primary aquaculture product (liveweight per m2)` ([9385547](https://gitlab.com/hestia-earth/hestia-glossary/commit/9385547f75bf00913899f6ced4daa5931fbbcfe6))
* **aquacultureManagement:** specify units for stocking density ([dee357c](https://gitlab.com/hestia-earth/hestia-glossary/commit/dee357cc94bb08d28c400c6751ec418dc19e9ac2))
* **aquacultureManagement animalManagement:** standardise `stocking density` terms ([abb5fc0](https://gitlab.com/hestia-earth/hestia-glossary/commit/abb5fc083c194812a14e282bf2b207cc14f954ba))
* **characterised indicator:** add 'cumulative fossil energy demand' and 'cumulative renewable energy demand' ([5550321](https://gitlab.com/hestia-earth/hestia-glossary/commit/55503215567b7b5afa507e126d4829a562efc491))
* **characterisedIndicator:** add synonyms for toxicity terms ([e2d69f1](https://gitlab.com/hestia-earth/hestia-glossary/commit/e2d69f1e968f0dd86a98fed4b6ab31479482be8e))
* **characterisedIndicator:** add total toxicity ([44a6ee7](https://gitlab.com/hestia-earth/hestia-glossary/commit/44a6ee7b6086d33639255a079609b81aaaf7eeaa))
* **crop:** add 'bay leaf' ([7e31346](https://gitlab.com/hestia-earth/hestia-glossary/commit/7e31346fee58c80fe8133e8fc49e5cb14ceec2c4))
* **crop:** add 'cashew nut, shell' ([9571cfc](https://gitlab.com/hestia-earth/hestia-glossary/commit/9571cfc60709bd1e2fae7ad4e37aa40099de1f3a))
* **crop:** add 'kelp, fresh' and 'kelp, dry' ([939201b](https://gitlab.com/hestia-earth/hestia-glossary/commit/939201b5fc1f564e6c221a62f6610cea19fe88c9))
* **crop:** add 'Maize, ear (cob and grain), dry' ([b38031e](https://gitlab.com/hestia-earth/hestia-glossary/commit/b38031e49d78795ce0418ff58a5a680061a90ff6))
* **crop:** add 'parsley, dry' and 'bay leaf, dry' ([9aa7c90](https://gitlab.com/hestia-earth/hestia-glossary/commit/9aa7c90a928db2e2d5e07ca11332dbe4c399a780))
* **crop:** add 'parsley, fresh' ([9e9abb5](https://gitlab.com/hestia-earth/hestia-glossary/commit/9e9abb5121f15887296978df9259e04f43ca7a6c))
* **crop:** add 'rice, meal' ([8090411](https://gitlab.com/hestia-earth/hestia-glossary/commit/80904110ba3f06567b7a313ca49b5eb7c9f5fbb0))
* **crop:** add 'sweet sorghum, stems' ([5deee65](https://gitlab.com/hestia-earth/hestia-glossary/commit/5deee652ccd44215f5ab2a5ff9c9b82d6242322a))
* **crop:** add 'urad bean' and 'mung bean' terms ([b1991f5](https://gitlab.com/hestia-earth/hestia-glossary/commit/b1991f59240f57132ca6f69a69d315067cc3497c))
* **crop:** add 'watercress, fresh' ([b9a6163](https://gitlab.com/hestia-earth/hestia-glossary/commit/b9a6163b0b5a05bcc30b27f250e12ada777beaf2))
* **crop:** add 'white pepper' and update 'black pepper' ([d437828](https://gitlab.com/hestia-earth/hestia-glossary/commit/d437828c3b2780e0b7952e4c33d4504bb02bb54b))
* **crop:** add 'wild garlic, fresh' and 'wild garlic, dry' ([6a055d2](https://gitlab.com/hestia-earth/hestia-glossary/commit/6a055d20a9ea9e7e919ff001f02b5a688e9d9334))
* **crop:** add "runner beans" as synonym to green beans ([a694a15](https://gitlab.com/hestia-earth/hestia-glossary/commit/a694a1545ebdd66cb4471b49f9af1106963d98c4))
* **crop:** add `Orchard_longFallowPeriod` lookup ([e2fbaa4](https://gitlab.com/hestia-earth/hestia-glossary/commit/e2fbaa462d2a244ff5f103bc4efdf7e28eddc568))
* **crop:** add cashew nut with shell ([b22eae3](https://gitlab.com/hestia-earth/hestia-glossary/commit/b22eae38fd55a8dc985b965f770c362b84c43ab4))
* **crop:** add crop residue and rooting depth info for 'mustard, seeds' ([950bdd2](https://gitlab.com/hestia-earth/hestia-glossary/commit/950bdd2d33982ed623c611de3501f24bb386ecf2)), closes [#278](https://gitlab.com/hestia-earth/hestia-glossary/issues/278)
* **crop:** add lookup for IPCC 2019 crop residue estimation ([bf1a501](https://gitlab.com/hestia-earth/hestia-glossary/commit/bf1a501708c9c8184f26484dcf5c6a12b1a21bb4))
* **crop:** add rooting depth for all legumes ([1acf024](https://gitlab.com/hestia-earth/hestia-glossary/commit/1acf024e816ade6d76057598c3c1971a58f6ba13))
* **crop:** add synonyms for 'fixed nitrogen' ([ea58602](https://gitlab.com/hestia-earth/hestia-glossary/commit/ea586026596590dcc7368e52320e11ccfe79943d))
* **crop:** add term `Concentrate feed blend` ([95cb417](https://gitlab.com/hestia-earth/hestia-glossary/commit/95cb41767ebfef81aa4d46656d21f52dbd2bdeac))
* **crop:** delete term `Fixed nitrogen` as duplicated in `organicFertilizer` glossary ([9f1784c](https://gitlab.com/hestia-earth/hestia-glossary/commit/9f1784ce04b3184da7021abe226c728c6bd49c9c))
* **crop:** rename 'cashew nut, with shell' ([6a2487f](https://gitlab.com/hestia-earth/hestia-glossary/commit/6a2487f3e7e2ac6f855da9126f9c3d4418f0aac9))
* **crop:** rename 'cashew nut' ([603f9bc](https://gitlab.com/hestia-earth/hestia-glossary/commit/603f9bc76a4d6d87638d60f68178b4b8260bd040))
* **crop:** rename 'sweet sorghum' into 'sweet sorghum, grain' ([599124e](https://gitlab.com/hestia-earth/hestia-glossary/commit/599124e7cc1083c33bbd57109fa33ea79f3831ff))
* **crop:** set `P_EF_C1` for all legumes ([a87f3d5](https://gitlab.com/hestia-earth/hestia-glossary/commit/a87f3d516fbbc811801919a99284de9bc3183205))
* **crop:** set `skipAggregation` to `false` for `Generic crop, grain` ([7a748b3](https://gitlab.com/hestia-earth/hestia-glossary/commit/7a748b32c101d6c0b29f38a1e30436e03547267b))
* **crop:** set default global econ value share to 0 for nitrogen uptake ([f9929c9](https://gitlab.com/hestia-earth/hestia-glossary/commit/f9929c93bf125f6145a1d5fa6779e713c0ef6684))
* **crop:** split `cropGroupingFAOSTAT` into `cropGroupingFaostatArea` and `cropGroupingFaostatProduction` ([157a8cb](https://gitlab.com/hestia-earth/hestia-glossary/commit/157a8cb14e2a64466e13de985e0e23a26790ef13))
* **crop:** update 'onion, dry' and 'onion, green' definition ([7240058](https://gitlab.com/hestia-earth/hestia-glossary/commit/7240058fbd722983c5c6446c4e00d5d82eec170b))
* **crop property:** update lookup for cashews ([4c512d5](https://gitlab.com/hestia-earth/hestia-glossary/commit/4c512d5b120528fca3834a73281523377647a8df))
* **crop property:** update lookup values ([4fc22a5](https://gitlab.com/hestia-earth/hestia-glossary/commit/4fc22a54ff6beecf948ec0e3e4ce4f20a88af147))
* **cropEstablishment:** improve definitions, change units of some terms to `%` ([6f506c0](https://gitlab.com/hestia-earth/hestia-glossary/commit/6f506c0df1f973cfcfcdf2592549e2f43299bf30))
* **electricity:** add global average market mix ([0b877cd](https://gitlab.com/hestia-earth/hestia-glossary/commit/0b877cdf5b1630fca1dc5682de607a85119a0a9c))
* **emission:** add 'CO2, to air, soil flux' ([10fca2f](https://gitlab.com/hestia-earth/hestia-glossary/commit/10fca2f0b1b01d10e8892db1a6b1e116e09a0bb2))
* **emission:** add `BOD5` ([1fbe1ac](https://gitlab.com/hestia-earth/hestia-glossary/commit/1fbe1acc491b98b32ac9bf029b175f1f27119016))
* **emission:** add CML acidification characterisation factors to H2S ([05a8047](https://gitlab.com/hestia-earth/hestia-glossary/commit/05a804706c8b6ed2ac62281851cf208eb6d64e29))
* **emission:** add lookup `deleteFromRecalculatedCycle` ([fff22e7](https://gitlab.com/hestia-earth/hestia-glossary/commit/fff22e7ac598c3a8e548e84d2522ca7870a8ac12))
* **emission:** remove `all origins` and `fertilizer` terms and add `soil flux` terms ([e9f1564](https://gitlab.com/hestia-earth/hestia-glossary/commit/e9f1564e014b4f533f950f7190cfe24937375744)), closes [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72) [#149](https://gitlab.com/hestia-earth/hestia-glossary/issues/149)
* **excreta:** set `global_economic_value_share` to `0` ([a2840d6](https://gitlab.com/hestia-earth/hestia-glossary/commit/a2840d6c3d91995783513c556b75eb3f7f8a0d68))
* **excreta:** split fish excreta into liquid and solid ([1313155](https://gitlab.com/hestia-earth/hestia-glossary/commit/13131551016f8f94df1226da02f091a12d438d4f))
* **excreta management:** add 'anaerobic digester, unspecified' ([1770a47](https://gitlab.com/hestia-earth/hestia-glossary/commit/1770a472c2051e8ea6711ece8adfe262ffcba9b2))
* **excretaManagement:** add excreta management N2O emission factors lookup ([2856683](https://gitlab.com/hestia-earth/hestia-glossary/commit/285668327f364202cd936f07f44d0e8bcda2894f))
* **excretaManagement:** add excreta management NO-N emission factors lookup ([89b0e9d](https://gitlab.com/hestia-earth/hestia-glossary/commit/89b0e9dda0c45d17369912b683ef7d446234ff7f))
* **excretaManagement:** add excreta management NO3-N emission factors lookup ([d196676](https://gitlab.com/hestia-earth/hestia-glossary/commit/d196676477a2599755fe71f975f965fb4a82dfc7))
* **excretaManagement:** add new excretaManagement terms ([cd98582](https://gitlab.com/hestia-earth/hestia-glossary/commit/cd98582b49bae704fb31a1a3b5c9b32b3bae8fae))
* **fertilizer:** add 'liquid ammonia' as synonym for 'aqueous ammonia' ([1ec033b](https://gitlab.com/hestia-earth/hestia-glossary/commit/1ec033bfcec94c100ceb65cc0b36103d58afce17))
* **fuel:** add 'wood (fuel)' ([fb53f68](https://gitlab.com/hestia-earth/hestia-glossary/commit/fb53f6849b4c121f8ec005f1babda3939031fe0d))
* **fuel:** update 'lubricant' term ([bd4bfd8](https://gitlab.com/hestia-earth/hestia-glossary/commit/bd4bfd851d3f2205d6b0b559251df293a69cc787))
* **geographies:** add AWARE factors by country ([7888718](https://gitlab.com/hestia-earth/hestia-glossary/commit/78887180545fd0a0d25abda78815fd704c6df9aa))
* **grographies:** add crop land transformation percentages per region ([1e2d53e](https://gitlab.com/hestia-earth/hestia-glossary/commit/1e2d53ea7a32a13199d894bd584719f686fbaea2))
* **inorganic fertilizer:** add 'Ammonium Nitrate Phosphate' ([c03329b](https://gitlab.com/hestia-earth/hestia-glossary/commit/c03329b1dfc4eccdd7ec81980f9ef8cda718fd00))
* **inorganic fertilizer:** add 'Calcium Ammonium Phosphate' ([86d7254](https://gitlab.com/hestia-earth/hestia-glossary/commit/86d7254fe3f4381078ac8d42a064a1957832673f))
* **inorganic fertilizer:** add 'Patentkali (K2O5)' ([78e8990](https://gitlab.com/hestia-earth/hestia-glossary/commit/78e89909c10143c4bb7968070ed541b428c2b498))
* **inorganic fertilizer:** add 'Thomas Slag (kg P2O5)' ([41d2409](https://gitlab.com/hestia-earth/hestia-glossary/commit/41d2409e46a3817541e4dc549a055282d302a672))
* **inorganic fertilizer:** add 'triammonium phosphate (kg N)' and 'triammonium phosphate (kg P2O5)' ([36ea9cf](https://gitlab.com/hestia-earth/hestia-glossary/commit/36ea9cf49b38d561d7b243f27fdb06a214ceefc8))
* **inorganic fertilizer:** add N and P2O5 content for 'ammonium nitrate phosphate' ([625bd72](https://gitlab.com/hestia-earth/hestia-glossary/commit/625bd72bc18cc4853e002eeb2d43be0b3b09e2f3))
* **landUseManagement tillage:** create new `tillage` glossary from existing `landUseManagement` terms ([bf068f6](https://gitlab.com/hestia-earth/hestia-glossary/commit/bf068f61f38e649db2b5ba74d4cefca2f5473ed7))
* **live aquatic species:** add 'catfish, unspecified' ([c13582f](https://gitlab.com/hestia-earth/hestia-glossary/commit/c13582f6615ee57f13ccea4558e0ce36556ec304))
* **live aquatic species:** add 'flathead grey mullet' ([b3e5a7e](https://gitlab.com/hestia-earth/hestia-glossary/commit/b3e5a7ecca50d6966507044993a4b592a175af9b))
* **live aquatic species:** add 'thinlip mullet' ([b741980](https://gitlab.com/hestia-earth/hestia-glossary/commit/b7419807cf756201d31beeb217729659376cf414))
* **liveAnimal-ipcc2019Tier2Ch4-lookup:** add digestible energy content ([1ffe574](https://gitlab.com/hestia-earth/hestia-glossary/commit/1ffe574c7702c2298ebf5ee218824e7848c5b092))
* **liveAquaticSpecies:** add unspecified fish and crustaceans ([1926562](https://gitlab.com/hestia-earth/hestia-glossary/commit/1926562bb6347b8bab1248a48162b8fe9b5b5d27))
* **material:** rename 'wood' ([634de20](https://gitlab.com/hestia-earth/hestia-glossary/commit/634de204880256a1cf6b9131f52ba97d8db43748))
* **measurement:** add synonym to `organic matter` term ([57dde50](https://gitlab.com/hestia-earth/hestia-glossary/commit/57dde509f3dd73f03f3911d25e3fb1fa9810b765))
* **measurement:** set `min` and `max` on `soilPh` to `2` and `11` ([ccde731](https://gitlab.com/hestia-earth/hestia-glossary/commit/ccde73178229fa20d150e8fc76cd53331a9ed6b1))
* **measurement:** simplify `units` ([3993267](https://gitlab.com/hestia-earth/hestia-glossary/commit/3993267226dc693211ca8d78dc4cebdee7c08052))
* **model:** add epa2014 model ([4427c6c](https://gitlab.com/hestia-earth/hestia-glossary/commit/4427c6c380141910335a6dec6855c48e50487fd1))
* **operation:** add `isTillage` lookup ([1890c1d](https://gitlab.com/hestia-earth/hestia-glossary/commit/1890c1dddd82d06b981f81940a9b0d70f9dbb82b))
* **operation:** move fuel use lookups into a single column ([d2dd448](https://gitlab.com/hestia-earth/hestia-glossary/commit/d2dd44806d6a7c049ddf0b57a2d78de886066283))
* **organic fertilizer:** add 'horn meal (kg mass)' and 'horn meal (kg N)' ([fcae2cd](https://gitlab.com/hestia-earth/hestia-glossary/commit/fcae2cd9215c80445a53f657497a8fc07908e431))
* **organic fertilizer:** update 'vinasse (kg N)' and 'vinasse (kg mass)' ([527c643](https://gitlab.com/hestia-earth/hestia-glossary/commit/527c64352da6592a200cbc2e830187704640e2d2))
* **organicFertilizer:** add "sea weed" as synonym to `seaweed` ([5d6391c](https://gitlab.com/hestia-earth/hestia-glossary/commit/5d6391cbf49da349cb3fb356409f4178a790ab86))
* **organicFertilizer:** add nutrient contents of sheep manure ([53a2095](https://gitlab.com/hestia-earth/hestia-glossary/commit/53a20950eb6a8da114692c6c89c94461272c6e51)), closes [#266](https://gitlab.com/hestia-earth/hestia-glossary/issues/266)
* **organicFertilizer:** rename poultry manure terms to match animal glossary ([fbdae1e](https://gitlab.com/hestia-earth/hestia-glossary/commit/fbdae1ebc5f8c85f75a6278294561066094c37cd))
* **other:** add 'additives, unspecified' ([7e35690](https://gitlab.com/hestia-earth/hestia-glossary/commit/7e356908011b8e6755e27cda9962db2406205faa))
* **other:** add 'phytoseiulus persimilis' ([325873e](https://gitlab.com/hestia-earth/hestia-glossary/commit/325873ef5b2700ba66198586befa2cd5ef0926bf)), closes [#275](https://gitlab.com/hestia-earth/hestia-glossary/issues/275)
* **other:** add 'probiotic unspecified (AI)' ([05e3873](https://gitlab.com/hestia-earth/hestia-glossary/commit/05e3873a4458fb81320270a79d59334cb0fe7508))
* **other:** add 'Trichogramma Wasps' ([d96737d](https://gitlab.com/hestia-earth/hestia-glossary/commit/d96737d8d14873693a55c36518ccd923c2fad6c6))
* **other:** add `Antioxidants, unspecified` ([52ff308](https://gitlab.com/hestia-earth/hestia-glossary/commit/52ff308a399122e118fc473e6fac5f990e1b0c65))
* **pesticide AI:** add 'acetanilide compounds, unspecified' ([d78c05f](https://gitlab.com/hestia-earth/hestia-glossary/commit/d78c05f11351e3e2699230eee038f958c12d3a13))
* **pesticide AI:** add 'beta-Cyfluthrin' ([e1b398f](https://gitlab.com/hestia-earth/hestia-glossary/commit/e1b398f3f7220852d87f5f5af87bdcf79555cc0a))
* **pesticide AI:** add 'chlortoluron' as synonym for 'chlorotoluron' ([9e407ca](https://gitlab.com/hestia-earth/hestia-glossary/commit/9e407caaf330ff6c8b5f49684518bdf479bfdb25))
* **pesticide AI:** add 'mefenpyr' ([3f0287b](https://gitlab.com/hestia-earth/hestia-glossary/commit/3f0287bac3cf891f85bc58cbef4e67bcd10a5b27))
* **pesticide AI:** add 'zeta-cypermethrin' ([156106d](https://gitlab.com/hestia-earth/hestia-glossary/commit/156106da515f0528705e1226a76701c243837ad7))
* **pesticide AI:** add general categories ([4fb5564](https://gitlab.com/hestia-earth/hestia-glossary/commit/4fb5564494a8d14f72371239ea38ce30ba4df755))
* **pesticide AI:** add general categories ([75ea582](https://gitlab.com/hestia-earth/hestia-glossary/commit/75ea58272b3bf0c50c81a12a9489bde7bdddc00f))
* **pesticide AI:** add more categories, subclasses and wikipedia links ([a389589](https://gitlab.com/hestia-earth/hestia-glossary/commit/a389589bc07a34855a55513267b05eebbca4aa4a))
* **pesticide AI:** add synonyms for 'ouragan' ([80e3e4f](https://gitlab.com/hestia-earth/hestia-glossary/commit/80e3e4feaac0db3d595b8175409530cc49034bbd))
* **pesticide AI:** delete 'fish oil' ([33e6ba4](https://gitlab.com/hestia-earth/hestia-glossary/commit/33e6ba435cfc594b50e65c3833643b4440c01d3a))
* **pesticide AI:** delete 'fish oil' ([c74426a](https://gitlab.com/hestia-earth/hestia-glossary/commit/c74426a69d33980fbc729093b7e68689c311fad5))
* **pesticide AI:** delete 'oxytetracycline' ([8c9500f](https://gitlab.com/hestia-earth/hestia-glossary/commit/8c9500f032d0c07526e0c2a4fae57914fb561ec8))
* **pesticide brand name:** add 'gesapax 500' and 'gesapax combi 80' ([1218f5b](https://gitlab.com/hestia-earth/hestia-glossary/commit/1218f5bddc80bf6ef78ad6e822fbcd267568a466))
* **pesticide brand name:** add 'laco EC' ([915fa35](https://gitlab.com/hestia-earth/hestia-glossary/commit/915fa35ff18bb6d533185e134b606d3b8d1ec925))
* **pesticide brand name:** add 'U 46 BR' ([0274ed6](https://gitlab.com/hestia-earth/hestia-glossary/commit/0274ed6626a53e8e6cf0fed2f0b7bb3bc393e326))
* **pesticideAI:** add plural synonyms to unspecified pesticides ([d6accfe](https://gitlab.com/hestia-earth/hestia-glossary/commit/d6accfeed113b1ed2eb5da5c29954a11f8d59ed0))
* **production practices:** add lookup excretaMangement ecoClimateZone CH4 ([c9c6dfc](https://gitlab.com/hestia-earth/hestia-glossary/commit/c9c6dfcd7080c16f3cd644517c6d96aeb8ee440e))
* **products:** update crop property lookup ([f6b43bd](https://gitlab.com/hestia-earth/hestia-glossary/commit/f6b43bd9ab02cd69c454867cc5ebe03856c517f0))
* **property:** add 'pH' ([8f91c1a](https://gitlab.com/hestia-earth/hestia-glossary/commit/8f91c1a9f31713b2aaf5fbf5a6445738599e8618))
* **property:** add digestible energy content terms ([7a47635](https://gitlab.com/hestia-earth/hestia-glossary/commit/7a476359c44f197f77496f572387a25046c0a14b))
* **property:** add further mineral contents and missing wikipedia links ([dbbc281](https://gitlab.com/hestia-earth/hestia-glossary/commit/dbbc2813d55b672fd2d6a9768e712c1a90a73394))
* **property:** add lipid content ([fc114c1](https://gitlab.com/hestia-earth/hestia-glossary/commit/fc114c1c20ddfc8551a003e399611ec4d3b538e0))
* **property:** add synonyms for `Dry Matter` ([4eddbfd](https://gitlab.com/hestia-earth/hestia-glossary/commit/4eddbfd6e2450bf7a3ace299abd530f448e43e6a))
* **property:** add synonyms to energy content ([c06c957](https://gitlab.com/hestia-earth/hestia-glossary/commit/c06c9576b9b6c8e388c38275314cc87bfcd85038))
* **property:** add terms for several minerals content and sucrose content ([94c198b](https://gitlab.com/hestia-earth/hestia-glossary/commit/94c198babcb5602967d53a8e446b9e2ca056f0dd))
* **region:** add `animalProduct` / `excretaManagement` ch4 B0 lookup ([373fc77](https://gitlab.com/hestia-earth/hestia-glossary/commit/373fc7719ddc52bb0c4defe37975587d471f032b))
* **region:** add `liveAnimal` / `excretaManagement` ch4 B0 lookup ([b8f69b9](https://gitlab.com/hestia-earth/hestia-glossary/commit/b8f69b9334c16751ac81873206be6e32099a401b))
* **region:** add `liveAquqticSpecies` / `excretaManagement` ch4 B0 lookup ([a92b630](https://gitlab.com/hestia-earth/hestia-glossary/commit/a92b630231e746aceee44be8fe1c4227510ba634))
* **region:** add aware factors for level 1 ([3efddd2](https://gitlab.com/hestia-earth/hestia-glossary/commit/3efddd24827198c428c5ccec4b17054471fde071))
* **region:** add ecoregion factors for level 0 and level 1 ([a2e187d](https://gitlab.com/hestia-earth/hestia-glossary/commit/a2e187df3c3960b5eabeba88ae6e5441cf64bdae))
* **region lookups:** rename to either `cropGroupingArea` or `cropGroupingProduction` ([d9fc67e](https://gitlab.com/hestia-earth/hestia-glossary/commit/d9fc67ef6c9782ed8792a1a2f4bd7ed580528554))
* **region-lookup:** add conveyancing efficiency ([761a2fe](https://gitlab.com/hestia-earth/hestia-glossary/commit/761a2fef9aa44574452e87c5d949036bd7ff0d3a))
* **resourceUse:** split terms into `during Cycle` and `inputs production` ([5d5ba0e](https://gitlab.com/hestia-earth/hestia-glossary/commit/5d5ba0eb732efc029e823d4809f39715716daf0c))
* **soil amendment:** correct 'dolomitic lime' unit ([2faab14](https://gitlab.com/hestia-earth/hestia-glossary/commit/2faab145d7fb0deb8d8b58eab9a201484dc33e60))
* **soilType usdaSoilType:** set `units` to percent ([eea84e7](https://gitlab.com/hestia-earth/hestia-glossary/commit/eea84e79bae44165945a8a005b444c266a71f1a5))
* **system:** add `Integrated production` ([a93058b](https://gitlab.com/hestia-earth/hestia-glossary/commit/a93058bc9994b7756a5fe2cc69eb555a65e36de3))
* **tillage:** add `unique` lookup ([514a418](https://gitlab.com/hestia-earth/hestia-glossary/commit/514a418ba1ca67b76ffc9ffbeb7b2fb36e324927))


### Bug Fixes

* **animalProduct:** rename `Fishmeal` `Fish meal` ([d59bc46](https://gitlab.com/hestia-earth/hestia-glossary/commit/d59bc46cc8c2bd187384c4625d9e69243b177cb7))
* **characterised indicator model mapping:** allow `cml2001Baseline` model ([f58c9ee](https://gitlab.com/hestia-earth/hestia-glossary/commit/f58c9eee38f5dd7a0244dbb23b5c91f9f32cf01c))
* **characterised indicator model mapping:** allow `cml2001NonBaseline` model ([0ed9d9f](https://gitlab.com/hestia-earth/hestia-glossary/commit/0ed9d9f2a8d817368ff3226de5a65ebc9e14f4f4))
* **characterisedIndicator-model lookup:** allow `other` model for CED and FWD ([d9262e2](https://gitlab.com/hestia-earth/hestia-glossary/commit/d9262e215614452f2cdde990d75f0aaa6576fbc4))
* **crop:** add missing lookups for `carrot` ([007bbd8](https://gitlab.com/hestia-earth/hestia-glossary/commit/007bbd8bafcba38a9fe74aa30b8a9febcb5bb05f))
* **crop:** errors in `dryMatter` content ([fb3c2a2](https://gitlab.com/hestia-earth/hestia-glossary/commit/fb3c2a24722213f9e485a373e9ff6dd839244278))
* **crop:** remove AG residue lookups not on main crop term ([395850d](https://gitlab.com/hestia-earth/hestia-glossary/commit/395850d409b6e3b03868c64bb6fccfe83d74a41b))
* **crop:** update 'subClassOf' id ([774c031](https://gitlab.com/hestia-earth/hestia-glossary/commit/774c0315dcc7c208e858feb6e8c476a11e7adc05))
* **crop-property-lookup:** rename term `cashewNutInShell` ([aa5daf7](https://gitlab.com/hestia-earth/hestia-glossary/commit/aa5daf7c92f90faf4b2799b904ed2dd97002233c))
* **emission:** correct errors in surface water terms ([55f871a](https://gitlab.com/hestia-earth/hestia-glossary/commit/55f871a7d9a2a0dd30b8f0c01c3f469fb9552ff6))
* **emission:** delete `deleteFromRecalculatedCycle` lookup ([5c45c5e](https://gitlab.com/hestia-earth/hestia-glossary/commit/5c45c5e62b4de63739ec0ff42cfa753bee0a48a2))
* **excreta:** remove `nContent` property without `value` ([a01ed65](https://gitlab.com/hestia-earth/hestia-glossary/commit/a01ed655ffa7eb118f4be743cd0e8fa2e9eba3ec))
* **excretaManagement:** fix typo in excreta management column names ([5cc7cd5](https://gitlab.com/hestia-earth/hestia-glossary/commit/5cc7cd5f6d29b4ed66d08f69719b11a90e7e9d23))
* **fuel:** update 'wood (fuel)' and 'wood pellets' definition ([fa4ff22](https://gitlab.com/hestia-earth/hestia-glossary/commit/fa4ff2259d2a7ee4445a0ee6f6e364802a4583d7))
* **inorganic fertilizer:** fix decimal separator in lookup ([d5da4bd](https://gitlab.com/hestia-earth/hestia-glossary/commit/d5da4bd3e4338098228999c46285303cd0438f40))
* **inorganic fertilizer:** fix error in lookup ([dca2524](https://gitlab.com/hestia-earth/hestia-glossary/commit/dca2524976708964b751c2e70cde54d414c570d2))
* **inorganic fertilizer, organic fertilizer:** fix decimal separator problem ([bba2e32](https://gitlab.com/hestia-earth/hestia-glossary/commit/bba2e322c3d4c286c2b0feace2ee855f2529da05))
* **inorganicFertilizer:** remove double space in term name ([a6150a7](https://gitlab.com/hestia-earth/hestia-glossary/commit/a6150a7e2d71097ba03266284d51bafed5aa82d6))
* **liveAquaticSpecies:** error in excreta lookup ([ee71a70](https://gitlab.com/hestia-earth/hestia-glossary/commit/ee71a70e0dd1a20cfcf643124b145203a931530a))
* **liveAquaticSpecies:** remove duplicated fingerlings and fry terms ([d48d063](https://gitlab.com/hestia-earth/hestia-glossary/commit/d48d0632ee2d972e8ea83e059ccb184f0ff32944))
* **model:** remove double in space in 'RAINS model' ([022332e](https://gitlab.com/hestia-earth/hestia-glossary/commit/022332e984de800c23e9d9c1c8415819d38376ac))
* **organicFertilizer:** error in poultry manure terms ([9db2ad5](https://gitlab.com/hestia-earth/hestia-glossary/commit/9db2ad51c84e886a623a734dc596aedebae344ee))
* **pesticide AI:** fix typos in subClass.id ([ebd024b](https://gitlab.com/hestia-earth/hestia-glossary/commit/ebd024bb71fe92c601935b34d17bf65c557da12f))
* **pesticide AI:** remove inconsistent 'subclassOf' for 'Pesticide unspecified (AI)' ([80a9e08](https://gitlab.com/hestia-earth/hestia-glossary/commit/80a9e08a1b649964370a57ba82c53bbf66d7aec0))
* **pesticideBrandName:** fix decimal error in lookup ([3ee7c6d](https://gitlab.com/hestia-earth/hestia-glossary/commit/3ee7c6d36c7e9cd0464ac7a07cd0799bb7088088))
* **property:** correct typo in 'readyToCookWeightPerHead' ([add22ac](https://gitlab.com/hestia-earth/hestia-glossary/commit/add22ac4c3a4c3c4ec0786495eb704b4cab0abc0))
* **property pesticideBrandName:** errors in column headers and decimal separators ([75ab05c](https://gitlab.com/hestia-earth/hestia-glossary/commit/75ab05c5e1e2630fa5003f852b8c6804cbd19f60))
* **tillage:** set `unique` lookup to `false` ([54b6278](https://gitlab.com/hestia-earth/hestia-glossary/commit/54b6278064685fb10f309fc11f207f235e9d9ee3))

### [0.0.11](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.0.10...v0.0.11) (2021-08-12)


### Features

* **animal management:** add 'includeForTransformation' lookup column ([69dff25](https://gitlab.com/hestia-earth/hestia-glossary/commit/69dff25a999fb22bcf57eaa10da37d06193e5ec1))
* **animal management:** delete slaughter age subclasses ([d9569ce](https://gitlab.com/hestia-earth/hestia-glossary/commit/d9569cebf7e26d1c40259f288e64db4fd01a3082))
* **animal product:** add excretaKgVs lookup ([80ad3fe](https://gitlab.com/hestia-earth/hestia-glossary/commit/80ad3fe708b3c73e346ea245539a633842ca4f02)), closes [#236](https://gitlab.com/hestia-earth/hestia-glossary/issues/236)
* **animalProduct:** add `liveAnimal` lookup ([adb4699](https://gitlab.com/hestia-earth/hestia-glossary/commit/adb4699f9ba6e5aa81caa8310accabe0b1b233e6))
* **animalProduct:** add `subClassOf` to all terms ([96107f1](https://gitlab.com/hestia-earth/hestia-glossary/commit/96107f1b22a503c373ae9e0f79a8591b6451dd53))
* **animalProduct:** add excreta lookup in animalProduct ([5abfb23](https://gitlab.com/hestia-earth/hestia-glossary/commit/5abfb23b7403c51f142c84376cc52c4100bc549d))
* **animalProduct:** complete nitrogenContent lookup ([562e09f](https://gitlab.com/hestia-earth/hestia-glossary/commit/562e09ff67a0b081f1cdf0a2b25fa387ceee117d))
* **animalProduct:** fix parsing ([787f5d7](https://gitlab.com/hestia-earth/hestia-glossary/commit/787f5d7f0122f708949d231e72afbcb84321666f))
* **animalProduct lookups:** add lookups for FAOSTAT mapping ([d8513c3](https://gitlab.com/hestia-earth/hestia-glossary/commit/d8513c3a472689e4d093b1dcd8b10815546922b9)), closes [#234](https://gitlab.com/hestia-earth/hestia-glossary/issues/234)
* **aquaculture management:** add 'includeForTransformation' lookup column ([de7e5c5](https://gitlab.com/hestia-earth/hestia-glossary/commit/de7e5c5ff469c4f8353442a266c31e989b6a97c8))
* **aquacultureManagement:** set model for `yieldOfTargetSpecies` ([22de9d2](https://gitlab.com/hestia-earth/hestia-glossary/commit/22de9d2b10dde3527342d6eb9866f6d8a1c8a018)), closes [#214](https://gitlab.com/hestia-earth/hestia-glossary/issues/214)
* **crop:** add 'pea, straw' and 'pea, fresh forage' ([9d5d0d3](https://gitlab.com/hestia-earth/hestia-glossary/commit/9d5d0d3c3003d3491d16ac07dd3b8caa350b1ce7))
* **crop:** add new crops and add synonyms ([a4e20a0](https://gitlab.com/hestia-earth/hestia-glossary/commit/a4e20a0242eb3763f1352e83a19f52fb9d45a2ce)), closes [#179](https://gitlab.com/hestia-earth/hestia-glossary/issues/179)
* **crop:** add new nitrogen uptake term ([6ccfde4](https://gitlab.com/hestia-earth/hestia-glossary/commit/6ccfde4b7b2d8fbf7240c857b4f1876148de83b6))
* **crop:** improve terms for nitrogen uptake ([7dfd191](https://gitlab.com/hestia-earth/hestia-glossary/commit/7dfd191c866c82606d864eac7106e072d91ce1d9))
* **crop:** rework nitrogen uptake terms ([e8fbc95](https://gitlab.com/hestia-earth/hestia-glossary/commit/e8fbc956b49f49e5aa4405099433213cdf456bda))
* **crop:** set 'skipAggregation' true fro generic crops ([8cc9640](https://gitlab.com/hestia-earth/hestia-glossary/commit/8cc96406c3b81ddb29a0c6ad3e17894d23627fb5)), closes [#224](https://gitlab.com/hestia-earth/hestia-glossary/issues/224)
* **crop establishment:** add 'sowing density' ([da22392](https://gitlab.com/hestia-earth/hestia-glossary/commit/da223929bd39df0b0ae6f61ae312e69ae187f084))
* **crop property:** update lookup from crops update ([35f01bc](https://gitlab.com/hestia-earth/hestia-glossary/commit/35f01bcb7e3fd5db3b2d642c5ba2d565c1ed4572))
* **crop residue:** add `skipAggregation` lookup ([d548908](https://gitlab.com/hestia-earth/hestia-glossary/commit/d548908b9d57a43ba5f38d0e9ad328904d6529cf))
* **crop residue management:** add new terms and assign CFOA for SFo calculation ([da94c91](https://gitlab.com/hestia-earth/hestia-glossary/commit/da94c91ae24ba83ff67fa486bee0792bdafdf473)), closes [hestia-engine-models#106](https://gitlab.com/hestia-earth/hestia-engine-models/issues/106)
* **emission:** add PO43- to water from fertilizer ([99b5fe2](https://gitlab.com/hestia-earth/hestia-glossary/commit/99b5fe2107f69db5d820e0128cbf3e5687cc2972))
* **excreta:** add `tanContent` of fish excreta ([6276356](https://gitlab.com/hestia-earth/hestia-glossary/commit/62763562c4f2829a24be1aba421eebff6febfe97))
* **excreta:** add volatile solids terms and name terms "kg" instead of "as" ([d2bf2fc](https://gitlab.com/hestia-earth/hestia-glossary/commit/d2bf2fc5c512b2d46c345bd5d3880eb0ac7c97e8))
* **excretaManagement:** enable `ch4ToAirAquaculturePonds` model for `excretionIntoWaterBody` ([4d2463b](https://gitlab.com/hestia-earth/hestia-glossary/commit/4d2463bde9d88f3278d69bae5891ecb3e63b5491))
* **excretaManagement:** update excreta into water bodies emissions lookup ([d5da6b0](https://gitlab.com/hestia-earth/hestia-glossary/commit/d5da6b04b76be9f6cb870254b9eb997b86bbb0d3))
* **geographies:** add lookup `animalProduct` head count ([a9f6d2c](https://gitlab.com/hestia-earth/hestia-glossary/commit/a9f6d2c4ee70c1c62cecad4a0f1a408e70743546))
* **inorganic fertilizer:** add synonyms for 'urea (kg N)' and 'urea ammonium nitrate (kg N)' ([e3df6fe](https://gitlab.com/hestia-earth/hestia-glossary/commit/e3df6feddb72062b70f3d7852f680621f05d6597))
* **inorganicFertilizer:** add further sulphur fertilizer terms ([91e02e5](https://gitlab.com/hestia-earth/hestia-glossary/commit/91e02e57f0c1668e2d41635347b02691bbaaafc8))
* **inorganicFertilizer:** add unspecified sulphur and magnesium fertilizer ([d1693ff](https://gitlab.com/hestia-earth/hestia-glossary/commit/d1693ffa9727fac1e57eac7ee076c9fd4a78b38a))
* **land use management:** change definition of 'previous product' ([2aa6868](https://gitlab.com/hestia-earth/hestia-glossary/commit/2aa6868c9beed3d5f10229465ba25678a32ef9e7))
* **live animal:** add 'excretaKgVs' lookup ([75e87f0](https://gitlab.com/hestia-earth/hestia-glossary/commit/75e87f04ca414c3bc6b579a0e270e07b08f6f983))
* **live aquatic species:** add 'excretaKgVs' lookup column ([79a7968](https://gitlab.com/hestia-earth/hestia-glossary/commit/79a79686f9f4cfffa3472da5aaffe2cda4c3491b))
* **liveAnimal:** add excreta lookup for live animals ([0ee9945](https://gitlab.com/hestia-earth/hestia-glossary/commit/0ee99451e33d73d9c78ae8cd550ce61524c97770))
* **liveAnimal:** add Mule ([b0216a3](https://gitlab.com/hestia-earth/hestia-glossary/commit/b0216a3220ec3156a0a904fee0c1bf971b09adfc))
* **liveAnimal:** add nitrogenContent as a defaultProperty ([7806e31](https://gitlab.com/hestia-earth/hestia-glossary/commit/7806e31d4324fbe94c3a237765a3414e4838e286))
* **liveAquaticSpecies:** add excreta lookup for liveAquaticSpecies ([9763801](https://gitlab.com/hestia-earth/hestia-glossary/commit/976380178695015680eb1a4b685a165929e3cf7c))
* **liveAquaticSpecies:** add nitrogenContent as a defaultProperty ([938af32](https://gitlab.com/hestia-earth/hestia-glossary/commit/938af32f63b484ad7b747fbdded20b4b4d462c57))
* **measurement:** add `arrayTreatment` lookup ([e4f5e40](https://gitlab.com/hestia-earth/hestia-glossary/commit/e4f5e40f5546547b25d885a5a54ac7ee9cf317ca))
* **measurement:** add lookup `needStartDateEndDate` ([5db24b3](https://gitlab.com/hestia-earth/hestia-glossary/commit/5db24b35fe19192cf6763e4aa5e97c88c1f83ca1))
* **measurement:** add Net Primary Production ([abb12af](https://gitlab.com/hestia-earth/hestia-glossary/commit/abb12aff7bf6c96d2417668ab86e3ef6786dbda8))
* **measurement:** add volume units for soil measurements ([6efaad7](https://gitlab.com/hestia-earth/hestia-glossary/commit/6efaad769fccaf6a5dfba18f7db71372d835b648)), closes [#223](https://gitlab.com/hestia-earth/hestia-glossary/issues/223)
* **measurement:** rename terms referencing a time period ([a15f3bf](https://gitlab.com/hestia-earth/hestia-glossary/commit/a15f3bf4853e13075a28ff049b2a38c7c38621b9))
* **measurement:** reorganise terms in Excel to reflect groupings ([822d1c3](https://gitlab.com/hestia-earth/hestia-glossary/commit/822d1c3273759eaed58fe7e0cab96536e2ce305f))
* **measurement soilTexture:** add lookup `recommendAddingDepth` for data validation ([d1f530b](https://gitlab.com/hestia-earth/hestia-glossary/commit/d1f530b07825a8712c375b1504d31ffbf4fcae38))
* **method measurement:** add 'Olsen method' ([8cc7a63](https://gitlab.com/hestia-earth/hestia-glossary/commit/8cc7a6335c74bd3fe79462b7890924d40a94a1ac))
* **model:** add `Papatryphon et al 2004` ([a1d8e8c](https://gitlab.com/hestia-earth/hestia-glossary/commit/a1d8e8c77bda3a88d1c7b2df794ad299e7fe26b1))
* **model:** add harmonized world soil database ([9b2115d](https://gitlab.com/hestia-earth/hestia-glossary/commit/9b2115de9f13ad50d0ba740c6717bd451a80b6ef))
* **model:** add Koble (2014) ([f30fb8f](https://gitlab.com/hestia-earth/hestia-glossary/commit/f30fb8f58bfed6d47f0ac651ddacb935c752bb13))
* **model:** add term `aggregated models` ([8208725](https://gitlab.com/hestia-earth/hestia-glossary/commit/820872545bb46f210d5ee57fbde883c331539d08))
* **operation:** add 'bailing, machine unspecified' ([31f3741](https://gitlab.com/hestia-earth/hestia-glossary/commit/31f3741627aa45a9a61aba290f0c3d6310e1e559))
* **operation:** add 'chopping stalks' and 'crop scouting' ([9db9657](https://gitlab.com/hestia-earth/hestia-glossary/commit/9db96571b0e0c8d2d0c9e1e4a8a679c3cc13ab61))
* **operation:** add 'cultivator use, operation unspecified' ([c0e4fdb](https://gitlab.com/hestia-earth/hestia-glossary/commit/c0e4fdbf3b7aa55d805427d36c006fa4ddc96c42))
* **operation:** add 'deep ripping' ([c14bbc2](https://gitlab.com/hestia-earth/hestia-glossary/commit/c14bbc2a60f596c82c2bed5b972afe9714d1282d))
* **operation:** add 'fluffing and shaking (hay)' ([a97c7f9](https://gitlab.com/hestia-earth/hestia-glossary/commit/a97c7f9fd1e6ed1902ea6da0c8d16f65a13816f3))
* **operation:** add 'grain drying' ([0bf12dd](https://gitlab.com/hestia-earth/hestia-glossary/commit/0bf12dde97994598ff69e1050fd973c2cfc531fa))
* **operation:** add 'interrow tilling, machine unspecified' ([b5fbe05](https://gitlab.com/hestia-earth/hestia-glossary/commit/b5fbe05062eea58e4146f8a4418051235bdd8d44))
* **operation:** add 'land planing, with land plane' ([ce76b7f](https://gitlab.com/hestia-earth/hestia-glossary/commit/ce76b7f62966c8c78230e6696dc755f5be5e45e2))
* **operation:** add 'plant protection, spraying, with self-propelled sprayer' ([bfe3806](https://gitlab.com/hestia-earth/hestia-glossary/commit/bfe3806a3d9650de21488178d227257e608847a3))
* **operation:** add 'ploughing, with chisel plough' ([5f93f72](https://gitlab.com/hestia-earth/hestia-glossary/commit/5f93f72f7d124aaf5b1cffa000cdc840a32c63b1))
* **operation:** add 'seed cleaning, with seed pre-cleaner' ([87532f0](https://gitlab.com/hestia-earth/hestia-glossary/commit/87532f0d5078b21d23ffa4525b96725e76f96b53))
* **operation:** add 'soil digging, machine unspecified' ([c1664d1](https://gitlab.com/hestia-earth/hestia-glossary/commit/c1664d17d24bee2a6f5a7861b6d8eef57c76a571))
* **operation:** add 'soil preparation, with cultivator' ([b239678](https://gitlab.com/hestia-earth/hestia-glossary/commit/b239678722b32fc9c58d3b166bccb8995345d830))
* **operation:** add synonym 'secondary tillage' ([375c9b5](https://gitlab.com/hestia-earth/hestia-glossary/commit/375c9b50d66e4a7c6745c6af3adfcf7ad386433a))
* **operation:** add synonym for 'chopping stalks, machine unspecified' ([caaee62](https://gitlab.com/hestia-earth/hestia-glossary/commit/caaee6230fbf8680127b9cc0bce5072a6b62851b))
* **operation:** add synonyms for 'plant protection' ([445deab](https://gitlab.com/hestia-earth/hestia-glossary/commit/445deab10be38718e20e5b3f6fa19dc2039812cd))
* **operation:** add synonyms for 'ploughing' and 'soil preparation, with disc harrow' ([98af2ed](https://gitlab.com/hestia-earth/hestia-glossary/commit/98af2eda6ecaf3926de1663b7efdd995ffd612f2))
* **operation:** delete terms ending with 'processing' ([da9b474](https://gitlab.com/hestia-earth/hestia-glossary/commit/da9b474be118b5b9f1429bc4918b0abb597249a2))
* **operation:** rename 'rolling, with roller' and update definition ([112c9f6](https://gitlab.com/hestia-earth/hestia-glossary/commit/112c9f612698767ea75789c469df3dedde74b18f))
* **operation:** update definitions and reorganize terms ([ffa5002](https://gitlab.com/hestia-earth/hestia-glossary/commit/ffa5002bba13fa59eae01aa290d3e6e48064523e))
* **operation:** update ploughing terms ([12040fe](https://gitlab.com/hestia-earth/hestia-glossary/commit/12040fe160a9f4f8a30690e7ea9635e0f1bbef54))
* **organic fertilizer:** add 'digestate (kg mass)' and 'digestate (as N)' ([5a3da7a](https://gitlab.com/hestia-earth/hestia-glossary/commit/5a3da7ac26ba6afd484e324c88197526c72a68c6))
* **organic fertilizer:** add 'pig slurry' as synonym for 'pigs, liquid manure' ([33c40d7](https://gitlab.com/hestia-earth/hestia-glossary/commit/33c40d7b75d27ceb14f3fcdca892fef628cb1810))
* **organic fertilizer:** assign conversion factors for SFo calculation ([f269969](https://gitlab.com/hestia-earth/hestia-glossary/commit/f2699695dddae7c2016979e73d8c4460b50f9f32)), closes [hestia-engine-models#106](https://gitlab.com/hestia-earth/hestia-engine-models/issues/106)
* **organic fertilizer:** rename 'nitrogen in rainfall (kg N)' ([029a7c7](https://gitlab.com/hestia-earth/hestia-glossary/commit/029a7c76ba3eb9e88b3945b0599d2d8a316832bc))
* **organicFertilizer inorganicFertilizer:** replace "as nutrient" with "kg nutrient" ([d2d4bf9](https://gitlab.com/hestia-earth/hestia-glossary/commit/d2d4bf9a458979b18b850db85d943d55ae08c83e)), closes [#147](https://gitlab.com/hestia-earth/hestia-glossary/issues/147)
* **pesticide brand name:** add 'prowl H2O' ([f77f359](https://gitlab.com/hestia-earth/hestia-glossary/commit/f77f359118496daee595b0ddc28d528f69b6a93c))
* **pesticide brand name:** delete duplicated term 'clothianidin technical' ([87e7e04](https://gitlab.com/hestia-earth/hestia-glossary/commit/87e7e04b0a257c386f5eb8c2cbca6b9d6829b759))
* **products:** add `animalProduct` to `liveAnimal` lookup ([9661436](https://gitlab.com/hestia-earth/hestia-glossary/commit/966143699ace5354e77778837c3146ad10b30f04))
* **property:** add 'plant available nitrogen content' ([cfafead](https://gitlab.com/hestia-earth/hestia-glossary/commit/cfafead3a822ee42085f17dea1e2b730552b21f0))
* **property:** add lookup column 'valueType' ([e692a1d](https://gitlab.com/hestia-earth/hestia-glossary/commit/e692a1da75d69472085bc626ba1d47b2f222f01f)), closes [#232](https://gitlab.com/hestia-earth/hestia-glossary/issues/232)
* **property:** add magnesium and sulfur content terms ([9d5679a](https://gitlab.com/hestia-earth/hestia-glossary/commit/9d5679ad5d7f4c4f9eae633a7cfd899765391990))
* **region:** rename duplicated region by lowercase names ([b59efe7](https://gitlab.com/hestia-earth/hestia-glossary/commit/b59efe7a0ac83290239dca1368aaabfe71e68a35))
* **region lookups:** assign default rice cultivation period to countries ([8cae31c](https://gitlab.com/hestia-earth/hestia-glossary/commit/8cae31cabee52b601399d4e6056f290336d96665)), closes [hestia-engine-models#106](https://gitlab.com/hestia-earth/hestia-engine-models/issues/106)
* **region lookups, waterRegime:** add flooded rice lookups ([21d55d2](https://gitlab.com/hestia-earth/hestia-glossary/commit/21d55d2288755453d4e0d7fc740e5436cc0669b5))
* **soil amendment:** add 'boron' and fix typo ([52aef38](https://gitlab.com/hestia-earth/hestia-glossary/commit/52aef38d9c1b29687097033de2385b5c107e0ade))
* **water regime:** add 'irrigated - furrow irrigation' ([bc53a2a](https://gitlab.com/hestia-earth/hestia-glossary/commit/bc53a2a46a4973b892d8189a4ba430f47bc6758c))
* add validator to match `characterisedIndicator` to `model` ([b821165](https://gitlab.com/hestia-earth/hestia-glossary/commit/b821165ce86de5cc22844e55f3430378a3ddd163)), closes [#190](https://gitlab.com/hestia-earth/hestia-glossary/issues/190)


### Bug Fixes

* move nitrogen in rainfall/irrigation to `organicFertilizer` ([cc89d22](https://gitlab.com/hestia-earth/hestia-glossary/commit/cc89d22bb3fe9bfb78e1769d8817fe483bc713c8))
* UK English change sulfur to sulphur ([9d775f8](https://gitlab.com/hestia-earth/hestia-glossary/commit/9d775f8654585d67caa2f73c8ca1038be8c7a86d))
* **animal management:** fix first lookup column ([1375df8](https://gitlab.com/hestia-earth/hestia-glossary/commit/1375df875ee311b93809bc9f1d82bc0f695b402b))
* **animal products, live animal, live aquatic species:** add 'excretaVs' column ([3cdcfcc](https://gitlab.com/hestia-earth/hestia-glossary/commit/3cdcfccb95cc444e7f1f7fe9abc39be765dc7123))
* **aqualculture management:** remove lookup emissions allowed model ([43ec3f7](https://gitlab.com/hestia-earth/hestia-glossary/commit/43ec3f744492724e500b4e024c17e02130cf77d4))
* **characterised-indicator model mapping:** fix typo in `aggregatedModels` ([b4dd1ff](https://gitlab.com/hestia-earth/hestia-glossary/commit/b4dd1ffc411e29f05ad0a96462d5d792173ff4c0))
* **crop:** missing `value` on `dryMatter` properties ([d4d83ab](https://gitlab.com/hestia-earth/hestia-glossary/commit/d4d83abc68cd4547dc83422538a3c94cc2bd8e67)), closes [#241](https://gitlab.com/hestia-earth/hestia-glossary/issues/241)
* **cropResidueManagement:** fix first column of lookup table ([eba9656](https://gitlab.com/hestia-earth/hestia-glossary/commit/eba9656de35e1a20494f76b1eb5c820e067a196a))
* **geographies:** add missing `animalProduct` head counts ([1498a5b](https://gitlab.com/hestia-earth/hestia-glossary/commit/1498a5b170e356d8229f4a353be47c49884afc40))
* **geographies:** add missing columns for animalGroupingFAO ([dd83c30](https://gitlab.com/hestia-earth/hestia-glossary/commit/dd83c3030103ce08dc904b216190a9646e6267d3))
* **geographies:** remove duplicated rows `animalProduct` quantity lookup ([bd1794d](https://gitlab.com/hestia-earth/hestia-glossary/commit/bd1794d95dd9b99ab9d2f2f5ead2e66d16b413d0))
* **inorganicFertilizer lookups:** change "as nutrient" to "kg nutrient" ([73f5955](https://gitlab.com/hestia-earth/hestia-glossary/commit/73f5955d497ad48488fa76d33f02f1043ff8a065))
* **liveAquaticSpecies:** add carbonContent lookup ([10388ab](https://gitlab.com/hestia-earth/hestia-glossary/commit/10388ab4db137e9fa719122f55ceaa49756af63e))
* **liveAquaticSpecies:** missing `value` on `dryMatter` property ([f8131e4](https://gitlab.com/hestia-earth/hestia-glossary/commit/f8131e485a557248bed8d01375bf96965f129863))
* **operation:** delete double column header ([3a44e13](https://gitlab.com/hestia-earth/hestia-glossary/commit/3a44e13878e9367d54cc6d5ceb11bd20968b5c38)), closes [#237](https://gitlab.com/hestia-earth/hestia-glossary/issues/237)
* **operation:** remove double space in 'grain drying (hours)' ([d7103f2](https://gitlab.com/hestia-earth/hestia-glossary/commit/d7103f2760de3b76266ad6a9b8e89bd8bef77d5b))
* move `concentrateFeedUnspecified` from `other` to `crop` ([b020f8b](https://gitlab.com/hestia-earth/hestia-glossary/commit/b020f8b518fb849f331f4201c49125063c058774))
* **measurement:** add "FALSE" to "needStartDate" ([87cf587](https://gitlab.com/hestia-earth/hestia-glossary/commit/87cf5872dcd2928ece40cb3825f99e81e405aadf))
* **measurement:** change unit for 'organic matter (per kg soil)' and 'organic matter (per m3 soil)' ([73cd6c0](https://gitlab.com/hestia-earth/hestia-glossary/commit/73cd6c090fdc017fc1cd5ead456b7a3e91fcd40a))
* **method measurement:** fix typo in 'olsen method' definition ([94c6d91](https://gitlab.com/hestia-earth/hestia-glossary/commit/94c6d9154dea24ec4278faa9e17eb59ed0e2c147))
* **pesticide ai:** remove duplicates ([ae8e713](https://gitlab.com/hestia-earth/hestia-glossary/commit/ae8e7130f6e18d63568e837853807de423fe4240))
* **pesticide brand name:** remove duplicates ([35acd14](https://gitlab.com/hestia-earth/hestia-glossary/commit/35acd14b9af5ee70fd093bddca578e17468fae02))
* **pesticideAI:** remove duplicate terms ([a06b39c](https://gitlab.com/hestia-earth/hestia-glossary/commit/a06b39cd6c502541c32bf53d0e360b365271ef69))
* **property:** fix 'valueType' lookup column ([90f029f](https://gitlab.com/hestia-earth/hestia-glossary/commit/90f029f2ca1af5029da767f38afa14a526b99d2a))
* **region ch4ef lookup:** use `,` instead of `;` ([e98f949](https://gitlab.com/hestia-earth/hestia-glossary/commit/e98f949b5c36550a89f93678659ec7b0b97f3629))

### [0.0.10](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.0.9...v0.0.10) (2021-07-12)


### Features

* export function to convert from GADM id to iso 3166-2 code ([c7e487d](https://gitlab.com/hestia-earth/hestia-glossary/commit/c7e487ddef0fbec5a178129ad4953d019c6821d8))

### [0.0.9](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.0.8...v0.0.9) (2021-07-12)


### Features

* add additional animal feeds ([f0f490e](https://gitlab.com/hestia-earth/hestia-glossary/commit/f0f490eaf8f67226464b9b0e9e4d8820312ee095)), closes [#208](https://gitlab.com/hestia-earth/hestia-glossary/issues/208) [#197](https://gitlab.com/hestia-earth/hestia-glossary/issues/197)
* move phosphoric acid to other glossary ([15ddf88](https://gitlab.com/hestia-earth/hestia-glossary/commit/15ddf8857429351d547e66ae5ec52e753634b835))
* update `liveAnimal` lookup for IPCC 2019 factors ([d4cafa0](https://gitlab.com/hestia-earth/hestia-glossary/commit/d4cafa026b417ff8eea069fa08164396305589b4))
* update processing conversion property name ([7eab51a](https://gitlab.com/hestia-earth/hestia-glossary/commit/7eab51a8ab63968f30f9b8d53d3ba9ec3f4dba4b))
* **animal management:** add 'live birth rate' ([f4a20f2](https://gitlab.com/hestia-earth/hestia-glossary/commit/f4a20f21900b064e590a2ee17f2af0a1e0f114b9))
* **animal management:** add 'weight at birth' ([bf3a685](https://gitlab.com/hestia-earth/hestia-glossary/commit/bf3a68511e909ba45a4df2b9c3128a3f16f7dc0e))
* **animalManagement:** add `herd composition` ([841c939](https://gitlab.com/hestia-earth/hestia-glossary/commit/841c9399eaf1c5b7d603de9e277c1e7a6586c627))
* **animalProduct:** split cattle into beef and dairy ([d34f725](https://gitlab.com/hestia-earth/hestia-glossary/commit/d34f72598f32e39dff1f6725cd47ede2a4d9688b)), closes [#182](https://gitlab.com/hestia-earth/hestia-glossary/issues/182)
* **aquacultureManagement:** add 'yield of target species' ([e60f7f3](https://gitlab.com/hestia-earth/hestia-glossary/commit/e60f7f31319f02dfec6676e6ddeb5cf0d1e7cb2a))
* **characterisedIndicator:** add biotic resource use ([09fca73](https://gitlab.com/hestia-earth/hestia-glossary/commit/09fca738876d0573543368c3b0bd2f99a8f59eba)), closes [#209](https://gitlab.com/hestia-earth/hestia-glossary/issues/209)
* **characterisedIndicator:** add water depletion ([bd8ca21](https://gitlab.com/hestia-earth/hestia-glossary/commit/bd8ca2107a1de1a1fba9ee74154ec83e9febd6be)), closes [#210](https://gitlab.com/hestia-earth/hestia-glossary/issues/210)
* **characterisedIndicator:** update toxicity terms ([6ee28f4](https://gitlab.com/hestia-earth/hestia-glossary/commit/6ee28f456dc01f165754242ae7aab15022f7c2d2)), closes [#211](https://gitlab.com/hestia-earth/hestia-glossary/issues/211)
* **crop:** add `steamFlakedMaize` ([d3257b6](https://gitlab.com/hestia-earth/hestia-glossary/commit/d3257b691fd0c6489978beb15b3fd7f38e3aced9))
* **crop:** add lookup for properties from feedipedia ([9083fec](https://gitlab.com/hestia-earth/hestia-glossary/commit/9083fec75dfeb4c9533bd6967763baaa0b1a0e78))
* **crop:** add missing feedipedia links and crops ([9cb7fb0](https://gitlab.com/hestia-earth/hestia-glossary/commit/9cb7fb077a043d6fa013fcde23b4927989e78b72)), closes [#203](https://gitlab.com/hestia-earth/hestia-glossary/issues/203)
* **crop:** add multiple terms for elephant grass ([7c4dec8](https://gitlab.com/hestia-earth/hestia-glossary/commit/7c4dec8b158bf74c47d8af7c9cee35f1d31eb159))
* **crop:** break oils into crude and refined ([fe1d880](https://gitlab.com/hestia-earth/hestia-glossary/commit/fe1d880e15e627406614270675a053c8d8fac860))
* **crop:** update `dryMatter` data using feedipedia ([5e5b5ae](https://gitlab.com/hestia-earth/hestia-glossary/commit/5e5b5ae0ffb039d7c3fc17a3f9581be3abc9af34))
* **crop property:** add lookup data for `camelinaOilMeal` ([a4a4cbc](https://gitlab.com/hestia-earth/hestia-glossary/commit/a4a4cbc3cbcf25a29dd8bf87a12855c25a32d19b))
* **crop property lookup:** add additional `energyDigestibility` properties ([b27a713](https://gitlab.com/hestia-earth/hestia-glossary/commit/b27a7130380ed3fae319b1f53b1968cabe0b656f))
* **crop property lookup:** add new terms ([cc75b50](https://gitlab.com/hestia-earth/hestia-glossary/commit/cc75b50ab018e4c1054467d3a201bb4c469dd76e))
* **crop property lookup:** update values ([274e38f](https://gitlab.com/hestia-earth/hestia-glossary/commit/274e38f704e0e37b59ae713976f25c912963a5c9))
* **emission:** add 'ground water' and 'surface water' as synonyms ([35b97fd](https://gitlab.com/hestia-earth/hestia-glossary/commit/35b97fdd8a72ff16bb43e9a1021c2a88472011d9))
* **emission:** add Hexane and H2S emissions ([3913194](https://gitlab.com/hestia-earth/hestia-glossary/commit/3913194612a768df449897882fe0be794a8194b7))
* **emission:** add N2O diminishing soil C stocks ([1d3bec0](https://gitlab.com/hestia-earth/hestia-glossary/commit/1d3bec026256a1fe2edd23c5db5c02bcb761cf95)), closes [#150](https://gitlab.com/hestia-earth/hestia-glossary/issues/150)
* **emissions:** add `inputProdutionGroupId` lookup ([5c5f73c](https://gitlab.com/hestia-earth/hestia-glossary/commit/5c5f73cf67d0f91bd92c804ba7ae9c4b0171089d))
* **excreta management:** add lookups for models to run given term ([6a3a092](https://gitlab.com/hestia-earth/hestia-glossary/commit/6a3a0929b3c58f83af4140e17a08eab7027b404e))
* **excretaManagement:** add `excretion into water body` ([88690d0](https://gitlab.com/hestia-earth/hestia-glossary/commit/88690d0efd76ae7d04fee57fc58c781d46281cd1))
* **excretaManagement:** add synonym for 'Liquid/Slurry' ([c57ada7](https://gitlab.com/hestia-earth/hestia-glossary/commit/c57ada778b7d5bfb27e0418e994552dbb60a3678))
* **excretaManagement:** add terms for lagoon storage ([1c5099e](https://gitlab.com/hestia-earth/hestia-glossary/commit/1c5099e52e682af9e50682efb10ed45839ee2bab)), closes [#206](https://gitlab.com/hestia-earth/hestia-glossary/issues/206)
* **excretaManagement:** update to match IPCC 2019 terms ([8bb9922](https://gitlab.com/hestia-earth/hestia-glossary/commit/8bb992273ec6ee48f718f970d88fcf9c7260c252))
* **fuel:** add wikipedia links and extra synonyms ([a545962](https://gitlab.com/hestia-earth/hestia-glossary/commit/a5459628a39577a82753a641e212b851b8ea7583))
* **geographies:** add ecoregion lookup ([c86e956](https://gitlab.com/hestia-earth/hestia-glossary/commit/c86e9568ed03b324e75325df5b38220d98fad888))
* **inorganic fertilizer:** add synonyms for 'NPK blend' ([d9eadc5](https://gitlab.com/hestia-earth/hestia-glossary/commit/d9eadc5142c23e79636516daf16179c8971f08fd))
* **inorganicFertilizer:** add synonyms to super phosphate ([d8b3d6e](https://gitlab.com/hestia-earth/hestia-glossary/commit/d8b3d6ebd82252bf55de512a8896a4cb30bcbb13)), closes [#205](https://gitlab.com/hestia-earth/hestia-glossary/issues/205)
* **inorganicFertilizer lookup:** add validator for fertilizers with multiple nutrients ([f86ab9e](https://gitlab.com/hestia-earth/hestia-glossary/commit/f86ab9e0140fa6c179d3b893e65d536fe697b07b)), closes [#191](https://gitlab.com/hestia-earth/hestia-glossary/issues/191)
* **landUseManagement:** add `Cultivation duration` as synonym to `Cropping duration` ([be9f629](https://gitlab.com/hestia-earth/hestia-glossary/commit/be9f6295df852de95ca5bb52fe9dabdc282d648b))
* **landUseManagement:** add `tillageDepth` ([3f2ceef](https://gitlab.com/hestia-earth/hestia-glossary/commit/3f2ceef222538478bd10989f1950f0997129ceb7))
* **landUseManagement:** add fertilizer placement depth ([3f2dd9d](https://gitlab.com/hestia-earth/hestia-glossary/commit/3f2dd9ddc6153da0da5dab80d4b141e37b277586))
* **live aquatic species:** add 'tambaqui' ([75ddf10](https://gitlab.com/hestia-earth/hestia-glossary/commit/75ddf1002c023fffb7b1465bc7c1febbd7e227cf))
* **live aquatic species:** add 'tilapia, unspecified' ([13e91b5](https://gitlab.com/hestia-earth/hestia-glossary/commit/13e91b5d7e809eaa669381ac6cfeccf84f4a1934))
* **liveAnimal:** add lactating dairy cows ([33ee026](https://gitlab.com/hestia-earth/hestia-glossary/commit/33ee026446fb0c0b6fe353a2c8ae577960ed448b))
* **liveAnimal:** add terms for dry cows ([f6e2e99](https://gitlab.com/hestia-earth/hestia-glossary/commit/f6e2e99ee815f6f6754088627aea96890ada7e11))
* **liveAnimal:** differentiate weaned and unweaned calves ([b6e635a](https://gitlab.com/hestia-earth/hestia-glossary/commit/b6e635a0ee68f51f4d1dbdfdf26ba18c465df783))
* **liveAnimal:** disaggretate buffalo ([6f20ae4](https://gitlab.com/hestia-earth/hestia-glossary/commit/6f20ae4d720850e15248ffac692b25d5d8b7befc))
* **liveAnimal CH4 lookup:** revise to match IPCC 2019 guidelines ([e757808](https://gitlab.com/hestia-earth/hestia-glossary/commit/e7578085a614ac5c9860d34e64a2bc2edb5fe635))
* **measurement:** add 'water depth' ([5656d5f](https://gitlab.com/hestia-earth/hestia-glossary/commit/5656d5f81ee16d2a5239fd9400d1229d1579968f))
* **measurement:** add soil depth ([cfe05ad](https://gitlab.com/hestia-earth/hestia-glossary/commit/cfe05ad266da986a16ef7c2da48d5cee0662b689)), closes [#169](https://gitlab.com/hestia-earth/hestia-glossary/issues/169)
* **measurement:** add terms for slow and fast flowing water ([26998d0](https://gitlab.com/hestia-earth/hestia-glossary/commit/26998d0bd76e757a1b978250727af7687235f1b4))
* **methodEmissionResourceUse:** add `open chamber` ([015677c](https://gitlab.com/hestia-earth/hestia-glossary/commit/015677ca13833316bb35675a5980224b1adbd012))
* **model:** add 'IPCC (2001)' ([b8d3e85](https://gitlab.com/hestia-earth/hestia-glossary/commit/b8d3e854580dbda3de08b49dbb78fcfb041c9af0))
* **model:** add `HYDE 3.2` ([96903cb](https://gitlab.com/hestia-earth/hestia-glossary/commit/96903cbde80feb9d12ac8d0ffe366aa25942efdd))
* **model:** add SALCA and SQCB models ([8012cf7](https://gitlab.com/hestia-earth/hestia-glossary/commit/8012cf7e5006b47caa413323e639298dbf26d55f))
* **operation:** add 'human labour', 'hired labour' and 'family labour' ([8883a96](https://gitlab.com/hestia-earth/hestia-glossary/commit/8883a96ce45d07a85bd58770768b493f053683ff))
* **operation:** add 'irrigating' ([90e56c6](https://gitlab.com/hestia-earth/hestia-glossary/commit/90e56c69c69e4ab29e4007b3a48a6fe7c5285099))
* **operation:** add 'machinery use, operation unspecified' ([5a95fcb](https://gitlab.com/hestia-earth/hestia-glossary/commit/5a95fcb69ad3ce9fb82b0c9a3c9de8f4d3fecca5))
* **operation:** add draft animals ([127d9e9](https://gitlab.com/hestia-earth/hestia-glossary/commit/127d9e9a7ba5436eab6bc503ac96aadd9d763abd))
* **operation:** add general categories and subclasses ([fed965c](https://gitlab.com/hestia-earth/hestia-glossary/commit/fed965c2c2c3cc2adb11d79e8b74bc56c5ae0920))
* **operation:** add terms for threshing and residue management ([d134ef0](https://gitlab.com/hestia-earth/hestia-glossary/commit/d134ef08200eb02e54cc37e7a8881cdbd14c90c1))
* **organic fertilizer:** add synonym 'chicken manure' ([8192fcd](https://gitlab.com/hestia-earth/hestia-glossary/commit/8192fcd1b3a21a71de2628e6fe125fb8c573b7be))
* **other:** add `activated carbon` ([aa987bd](https://gitlab.com/hestia-earth/hestia-glossary/commit/aa987bd44b32fbe2bd20f5e1ce57bcb80ce1c5bc))
* **other:** add `steam` and `hexane` ([4e0e86e](https://gitlab.com/hestia-earth/hestia-glossary/commit/4e0e86e981a00cc21208aaecf540a16cf7660fe4))
* **other:** add concentrate feed ([c3e7f63](https://gitlab.com/hestia-earth/hestia-glossary/commit/c3e7f6334b0f5cda0cd8af4f756122e7554b4637))
* **other:** add methane inhibitors ([c46875c](https://gitlab.com/hestia-earth/hestia-glossary/commit/c46875c52b78af8a9111ae97c79f21d5a52c60e1))
* **other:** add mineral oil ([01a9488](https://gitlab.com/hestia-earth/hestia-glossary/commit/01a9488e2c2d20b989af71877e38bc0d853a6ba1))
* **other:** add monocalcium phosphate ([8744b6f](https://gitlab.com/hestia-earth/hestia-glossary/commit/8744b6f35c4dd6bb6264911a43f251b435d612db)), closes [#207](https://gitlab.com/hestia-earth/hestia-glossary/issues/207)
* **other:** add term for 'micronutrients, unspecified' ([4428b8e](https://gitlab.com/hestia-earth/hestia-glossary/commit/4428b8e5f0564a5931353544ac1395dc887e2e3d))
* **other:** add term for 'stem cuttings' ([b5dbc83](https://gitlab.com/hestia-earth/hestia-glossary/commit/b5dbc83662dad530df44c188178e5543755eabc6))
* **other:** add term for `bleaching earth` ([af69316](https://gitlab.com/hestia-earth/hestia-glossary/commit/af6931608062907ddab6831e7c550cfd783fa333))
* **other:** add term for `surfactant` ([44f4a43](https://gitlab.com/hestia-earth/hestia-glossary/commit/44f4a436184a6cd5722ca7b87f2cd1cd97ba10d7))
* **other:** set `id` manually where terms have a CAS number ([b984d22](https://gitlab.com/hestia-earth/hestia-glossary/commit/b984d2276f3510c1e61378c64f80aa1e293681a2))
* **products:** add `animalProduct` to ch4 / ipcc2019 / Tier 2 lookup ([771f2e2](https://gitlab.com/hestia-earth/hestia-glossary/commit/771f2e26909b7a368de29df7f2597f9ca92d6f6e))
* **property:** add further digestibility terms ([305311e](https://gitlab.com/hestia-earth/hestia-glossary/commit/305311ecb876093ea4f17ae45a0c299b1e51e50f)), closes [#201](https://gitlab.com/hestia-earth/hestia-glossary/issues/201)
* **property:** align terms to feedipedia ([9f72385](https://gitlab.com/hestia-earth/hestia-glossary/commit/9f72385a5b7f93fca38f54cd35bfe23db87960d5))
* **property:** update definition of `density` to account for `steam` ([bb49fb0](https://gitlab.com/hestia-earth/hestia-glossary/commit/bb49fb07ca5c58b5dca36b9175087e04950fb9bf))
* **property lookup:** add `validationAllowedExceptions` field ([f68a9f1](https://gitlab.com/hestia-earth/hestia-glossary/commit/f68a9f13c7936aa5e9eea855398c023ca4f122fa)), closes [#204](https://gitlab.com/hestia-earth/hestia-glossary/issues/204)
* **region:** add lookup FAOSTAT crop yield per country ([17919db](https://gitlab.com/hestia-earth/hestia-glossary/commit/17919dbcee2f3f1f4db80b4fbecbe2f59019d066))
* **soilAmendement:** add "plant ash" as synonym for `woodAsh` ([d7c1c22](https://gitlab.com/hestia-earth/hestia-glossary/commit/d7c1c22697876646be9821b392b0e1def2ec60a8))
* add formula to get data from `crop-property-lookup` ([796a9f5](https://gitlab.com/hestia-earth/hestia-glossary/commit/796a9f5dfc98d8be9638890478bd0b8f7222c2c2))
* **property:** match conversions to animal FUs ([25902e0](https://gitlab.com/hestia-earth/hestia-glossary/commit/25902e09ce8e78b17057c82008937b7f1d3b4e8a))
* **resourceUse:** update land transformation terms ([312ca17](https://gitlab.com/hestia-earth/hestia-glossary/commit/312ca1719e7acf494932125e221846d5354930a4))
* **waterRegime:** update terms to match IPCC 2019 guidelines ([938f585](https://gitlab.com/hestia-earth/hestia-glossary/commit/938f5851cbfa088e640d6272e2d05c30cfcbf9f8))


### Bug Fixes

* errors in `subClassOf` and `id` ([b873a03](https://gitlab.com/hestia-earth/hestia-glossary/commit/b873a03054d2e13de6bba75e51871d094063f0bd))
* move snails from `animalProduct` to `liveAnimal` ([6473dad](https://gitlab.com/hestia-earth/hestia-glossary/commit/6473dad09716a8fb3c3a8c6a65752da148e0cca0))
* remove duplicates from `pesticideAI` glossary ([68791db](https://gitlab.com/hestia-earth/hestia-glossary/commit/68791db5a12fb3ed59eb13ab3e78321a02ce5f4a))
* **animal management:** correct unit for 'milk yield per cow (raw)' and 'milk yield per cow (FPCM)' ([18351a8](https://gitlab.com/hestia-earth/hestia-glossary/commit/18351a89221fcdbb5a83497cbef1b1b02b2d95fe))
* **animal management:** rename 'infant mortality' ([0ac578e](https://gitlab.com/hestia-earth/hestia-glossary/commit/0ac578eb8cbdf1cd9d0dd43673d0551b35ef51ba))
* **animalManagement:** change milk yield units ([71fde92](https://gitlab.com/hestia-earth/hestia-glossary/commit/71fde9298c97f361a60ae0b4c4f179e20dea4fd3))
* **animalManagement:** resolve merge conflicts ([e667e15](https://gitlab.com/hestia-earth/hestia-glossary/commit/e667e1595cf2fcfce8584b9a49d7e62862ac853e))
* **animalProduct:** error in `nitrogenContent` of poultry meat ([e85eed4](https://gitlab.com/hestia-earth/hestia-glossary/commit/e85eed40cd305133f738fc6a3e96636d558157ee))
* **characterised indicator:** change unit for term 'soil organic matter loss' ([50a418e](https://gitlab.com/hestia-earth/hestia-glossary/commit/50a418e1a02bc752934a120e01e38d1606685723))
* **characterisedIndicator:** remove permanent effects; fix typo ([068745a](https://gitlab.com/hestia-earth/hestia-glossary/commit/068745a37dbead91cc2460c748ffb1147e4a44a1))
* **characterisedIndicator:** rename ozone depletion/creation terms ([c3861fe](https://gitlab.com/hestia-earth/hestia-glossary/commit/c3861fef928c7edbe367b369cf6e57147537e9e3))
* **crop:** add missing `term.id` for properties ([db434c1](https://gitlab.com/hestia-earth/hestia-glossary/commit/db434c1ae50be3236716b27b9765ef1257fd106b))
* **crop:** correct term.id for elephant grass ([7ccd689](https://gitlab.com/hestia-earth/hestia-glossary/commit/7ccd6895c89e618e3549be81a038bd5b2e6bf05e))
* **crop:** remove energy content lookup ([7d85e9e](https://gitlab.com/hestia-earth/hestia-glossary/commit/7d85e9e75df6c9065bd2954f5ae1ce44a8d04887))
* **crop:** rename 'peanut, hulls', 'peanut, in hull', 'peanut, kernel', 'peanut, in shell' ([3450251](https://gitlab.com/hestia-earth/hestia-glossary/commit/34502511539e758c6a9c85decb51b51242dbf1ba))
* **crop:** rename 'peanut, in hull' and 'peanut, hulls' ([92b8c46](https://gitlab.com/hestia-earth/hestia-glossary/commit/92b8c46e256fc8e2927e2b0bd29544b91933f7cb))
* **crop:** update feedipedia links ([b98ce76](https://gitlab.com/hestia-earth/hestia-glossary/commit/b98ce76c12a23a00611fc5ec8bece913b88eec83))
* **crop property:** rename peanut crops ([c569da2](https://gitlab.com/hestia-earth/hestia-glossary/commit/c569da23b99b16cdbff5f5f10ff887c5f89419da))
* **ecoClimateZone-lookup:** rename column headers ([4ee4ac1](https://gitlab.com/hestia-earth/hestia-glossary/commit/4ee4ac1df5a89baedba4fd4c512dcb20e4efaba6))
* **ecoClimateZone-lookup:** rename column headers to include units ([411d2b8](https://gitlab.com/hestia-earth/hestia-glossary/commit/411d2b8f8b14941c535e2176f8a4b453528f1670))
* **excreta:** correct typo in 'excreta, sheep and goats (kg mass)' ([43f9275](https://gitlab.com/hestia-earth/hestia-glossary/commit/43f9275c71e50930f276cb13093b95be14bbc2b0))
* **excreta:** typos in term names ([5dca19f](https://gitlab.com/hestia-earth/hestia-glossary/commit/5dca19f8ccacb42230cca0227d6cbee358e7ff66))
* **fuel:** delete `animalWaste` ([1f1a78d](https://gitlab.com/hestia-earth/hestia-glossary/commit/1f1a78ddc1ee00b69b3c862c7bc4216acdcde8eb))
* **liveAnimal:** split calf into beef and dairy calves ([99f615e](https://gitlab.com/hestia-earth/hestia-glossary/commit/99f615e35f13ad09a524f6ec0f2655628b3efed0))
* **liveAnimal CH4-lookup:** add `needHerdComposition` and fixed lookups ([5fbba3d](https://gitlab.com/hestia-earth/hestia-glossary/commit/5fbba3df3ef971ae7fd002e1f1486e7ed511122c))
* **liveAnimal lookup:** fix error in `buffaloDryCows` key ([8afa947](https://gitlab.com/hestia-earth/hestia-glossary/commit/8afa9473e95f7dd134ef3fcf9cb57b97deec937b))
* **liveAnimal lookup:** remove commas ([8a032de](https://gitlab.com/hestia-earth/hestia-glossary/commit/8a032def0eecf04664db7b99ca731080e7018628))
* **material:** correct definitions of 'Irrigation infrastructure, depreciated amount per Cycle' and 'Machinery infrastructure, depreciated amount per Cycle' ([5ebdfab](https://gitlab.com/hestia-earth/hestia-glossary/commit/5ebdfab6166a987660dce240d619d39bc587d21f))
* **measurement:** set `fallowCorrection` validator to `6` ([f77fd22](https://gitlab.com/hestia-earth/hestia-glossary/commit/f77fd22277623efa4655f904feca22f0533789d2))
* **operation:** correct subClassOf.id ([41fe9d3](https://gitlab.com/hestia-earth/hestia-glossary/commit/41fe9d3b910a020bfaae2cc214a95c41e502b33d))
* **operation:** correct subClassof.id for 'soil decompactation, machine unspecified' ([ac9d1cb](https://gitlab.com/hestia-earth/hestia-glossary/commit/ac9d1cbf666ffc2ce44748d5768c1cc7d0f21b81))
* **operation:** correct typos ([4e7f24a](https://gitlab.com/hestia-earth/hestia-glossary/commit/4e7f24a33a216dda31d8e2ad1eacb69b52df6409))
* **operation:** correct typos ([aafff0c](https://gitlab.com/hestia-earth/hestia-glossary/commit/aafff0c76e495af39707575f90d9db4bc3549780))
* **operation:** remove '-' from openLCAid field ([f639149](https://gitlab.com/hestia-earth/hestia-glossary/commit/f639149630138890ffc8ce034514f6dd22531cf8))
* **operation:** remove redundant subClassOf.id ([d53f543](https://gitlab.com/hestia-earth/hestia-glossary/commit/d53f5433d689bdec21f7d666a6b2d7b08b6b48ad))
* **operation:** update terms and units for draft animals ([876108e](https://gitlab.com/hestia-earth/hestia-glossary/commit/876108eba5e5a84acdcaadc66904ac5b3b9f7828))
* **organicFertilizer:** change default property from `asK` to `asK2O` ([acc9e3b](https://gitlab.com/hestia-earth/hestia-glossary/commit/acc9e3b30680c59fd61185aa42871aea577e31e7)), closes [#181](https://gitlab.com/hestia-earth/hestia-glossary/issues/181)
* **pesticideAI:** fix merge conflict ([bcbfd9f](https://gitlab.com/hestia-earth/hestia-glossary/commit/bcbfd9f71ebf85c825a3d979563f83de3a7ed823))
* **pesticideAI:** remove `methionine` ([95c9816](https://gitlab.com/hestia-earth/hestia-glossary/commit/95c9816aad8525e1d676b56b5c0466047903905b))
* **pesticideAI:** remove `water` and other non-pesticides ([7cb202b](https://gitlab.com/hestia-earth/hestia-glossary/commit/7cb202bdf93ba8a6bf84163502eaef1aa2b7f081))
* **pesticideBrandName:** add missing `term.id` for properties ([639631a](https://gitlab.com/hestia-earth/hestia-glossary/commit/639631a8b6e2a60f120ffe207eac6b7fd9d0a6a9))
* **property:** rename energy content terms to specify heating value ([107fca1](https://gitlab.com/hestia-earth/hestia-glossary/commit/107fca1754f15d4382c9e64570ae5f09281f98b1))
* **property:** rename NHV terms for consistency ([fc9ec26](https://gitlab.com/hestia-earth/hestia-glossary/commit/fc9ec26f100a9cf07831b3f4f1ff9d8f4a4f3ad1))
* **water:** change 'ground water' to 'groundwater' and add synonym ([f4e6dc5](https://gitlab.com/hestia-earth/hestia-glossary/commit/f4e6dc5d7ff6ff2d395347d7a57c5b8512d6433f))

### [0.0.8](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.0.7...v0.0.8) (2021-05-31)


### Bug Fixes

* **excreta lookup:** resolve error in JSON ([0aaa2dc](https://gitlab.com/hestia-earth/hestia-glossary/commit/0aaa2dc3d4536d17d1722ba7cada287704d8cecd))

### [0.0.7](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.0.6...v0.0.7) (2021-05-31)


### Features

* simplify total ammoniacal nitrogen property ([d9e8a6d](https://gitlab.com/hestia-earth/hestia-glossary/commit/d9e8a6d6e834bbc38758d8f64d6dc542bbe85bb4))
* **animalManagement:** add cow breeding terms ([a893b02](https://gitlab.com/hestia-earth/hestia-glossary/commit/a893b02bd9e021559411664f4f22fec89fdb944e))
* **animalProduct:** add `nitrogenContent` of liveweight as property ([59b3ef9](https://gitlab.com/hestia-earth/hestia-glossary/commit/59b3ef972ffa5e61964209a0337b23a3d4b15e13))
* **animalProduct:** add default processing conversions ([c6a401f](https://gitlab.com/hestia-earth/hestia-glossary/commit/c6a401fa448d583edece774df831b9526f640f92))
* **animalProduct:** add processing conversion properties to carcass and retail wt terms ([ac18d48](https://gitlab.com/hestia-earth/hestia-glossary/commit/ac18d4896ffc72520ee350f86adaddae06b5fa3a))
* **characterisedIndicator:** add more synonyms to `GWP` terms ([311dea0](https://gitlab.com/hestia-earth/hestia-glossary/commit/311dea066ff9a6c53c6dd25e09c5374bc470cdb4))
* **emission:** add `NH3` and `N2O indirect` from fuel combustion ([9225700](https://gitlab.com/hestia-earth/hestia-glossary/commit/9225700b7c3aaf8bb35d9555d5b4656a78e9599d))
* **emission:** add N2 emissions; improve overall documentation ([fb6087f](https://gitlab.com/hestia-earth/hestia-glossary/commit/fb6087f44c51c1a216c6bf609f87782eff11bccc))
* **emission:** remove `excreta management` terms and use `excreta` only ([6992d88](https://gitlab.com/hestia-earth/hestia-glossary/commit/6992d88d5ad0d7b168c93724291e282e5fcc9bd7))
* **excreta:** add term for processed excreta as P ([07575f6](https://gitlab.com/hestia-earth/hestia-glossary/commit/07575f6bfdfeb0e98740863dd9b5bdad7c41f8b5))
* **excreta:** add terms for processed excreta ([8229599](https://gitlab.com/hestia-earth/hestia-glossary/commit/8229599b992e9667b9c3300d96b7db536c108fa9))
* **measurement:** add 'croppingIntensity' to the `measurement` glossary ([87cd38e](https://gitlab.com/hestia-earth/hestia-glossary/commit/87cd38eec790d9e11c379dc0c3fd7c66740234a5))


### Bug Fixes

* **excreta lookup:** set `generateImpactAssessment` to false for all terms ([6ef3868](https://gitlab.com/hestia-earth/hestia-glossary/commit/6ef38680640940829023d0900f18d963f123d709))
* **pesticideAI:** rename `MCPA` ([fb339c6](https://gitlab.com/hestia-earth/hestia-glossary/commit/fb339c615b405acf97ee2080b0a36367ca789ebc))

### [0.0.6](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.0.5...v0.0.6) (2021-05-20)


### Features

* add Blonk co2 land use change emissions lookup ([81a82c2](https://gitlab.com/hestia-earth/hestia-glossary/commit/81a82c2ee48524c95dfcc3ec77e07957767d7040))
* add lookup tables necessary to implement Blonk model ([377ba0c](https://gitlab.com/hestia-earth/hestia-glossary/commit/377ba0c4371559fa897cf7b5c842a96106d54baa))
* create nitrogen fertilizer grouping ([7e94025](https://gitlab.com/hestia-earth/hestia-glossary/commit/7e94025b34c4472b76d138b0510a2f78afa55543))
* move model part of `characterisedIndicator` into `model` glossary ([3b9cf40](https://gitlab.com/hestia-earth/hestia-glossary/commit/3b9cf40a9585f33b1a9a480b7686f469e5e7974c)), closes [#161](https://gitlab.com/hestia-earth/hestia-glossary/issues/161)
* update model descriptions to match glossary; rename fuel combustion terms ([6aaacb3](https://gitlab.com/hestia-earth/hestia-glossary/commit/6aaacb37237cdfb9c94073c2b57915edb4fe2363))
* **animal-product:** add world production quantity ([4e78ac9](https://gitlab.com/hestia-earth/hestia-glossary/commit/4e78ac9eb2fdf09c25b1a92b621a9e451b6b057e))
* **animalManagement:** add beef herd management terms ([7980c0c](https://gitlab.com/hestia-earth/hestia-glossary/commit/7980c0c1f79d6d72d58e41e756d677306da68e80))
* **animalProduct:** add snailMeal ([1ead799](https://gitlab.com/hestia-earth/hestia-glossary/commit/1ead79961ccb401e3efc3eb23a92b616a3c4860f))
* **animalProduct:** differentiate terms into liveweight, carcass weight, dressed weight ([8cb5a81](https://gitlab.com/hestia-earth/hestia-glossary/commit/8cb5a8121cc55923ab322be21506241d3dac54e5))
* **characterisedIndicator:** remove `GWP20`, `GWP100`, `GWP500`, improve `description` ([74bd27b](https://gitlab.com/hestia-earth/hestia-glossary/commit/74bd27b16ea384640df35d0a70b667f12254ed18))
* **co2LandUseChange lookup:** update to directly match original source ([4d58381](https://gitlab.com/hestia-earth/hestia-glossary/commit/4d58381bb75b7db4654a354c1571c61dbedb050c))
* **crop:** add `nonBearingDuration` to the crop lookup ([95db82d](https://gitlab.com/hestia-earth/hestia-glossary/commit/95db82d6d17a5f8ddd9dea6576f8616fb0500334)), closes [#175](https://gitlab.com/hestia-earth/hestia-glossary/issues/175)
* **crop:** add `orchard_duration`, `orchard_density`, `saplings` and `nursery_duration` to orchard crops within the crop glossary ([5fa7ca4](https://gitlab.com/hestia-earth/hestia-glossary/commit/5fa7ca48e63e15d9913694f8a8c742c505d6e4da))
* **crop:** add default above and below ground crop residue values, combustion factors, above and below ground N content values for tree crops ([9480a9a](https://gitlab.com/hestia-earth/hestia-glossary/commit/9480a9af0c360dbd1e83f80db14ff195b21f5c2f))
* **crop:** add lookup table for N2O fertilizer emissions ([9938246](https://gitlab.com/hestia-earth/hestia-glossary/commit/993824691f5be11749c289952a12a440fc5574e7))
* **crop:** add new crops, improve crop grouping ([6340d03](https://gitlab.com/hestia-earth/hestia-glossary/commit/6340d03f92c1fd6b8b810e27720e1e9c54100c52)), closes [#178](https://gitlab.com/hestia-earth/hestia-glossary/issues/178)
* **crop:** add terms for banana stalks, leaves, cassava stalks, maize stalks ([34e38d1](https://gitlab.com/hestia-earth/hestia-glossary/commit/34e38d1f27f000eba89bd185e9775166bf3019cd))
* **crop:** add world production quantity ([4eb270d](https://gitlab.com/hestia-earth/hestia-glossary/commit/4eb270dd5504a39c5e80e76eaaf2785b2c5c94e0))
* **crop:** update P_EF_C1 ([235858e](https://gitlab.com/hestia-earth/hestia-glossary/commit/235858e2b6c0928bf81ad31d21d90e2acbfa615d))
* **crop biomassBurning lookup:** split into ch4 and n2o tables; match original source ([4b53545](https://gitlab.com/hestia-earth/hestia-glossary/commit/4b53545386f31f7d76ba70dd245d14ae93cd5ef6))
* **crop co2LandUseChange lookup:** add default values for cropland and pasture ([99c1dd5](https://gitlab.com/hestia-earth/hestia-glossary/commit/99c1dd53f0b752a79ec2ce3d3cf0fa92fc3785e5))
* **crop co2LandUseChange lookup:** set units to kg ([b70f28f](https://gitlab.com/hestia-earth/hestia-glossary/commit/b70f28f73190f954d90fc5a50b14c2a3e991b5cd))
* **crop price lookup:** match column headers to crop glossary ([196c3ac](https://gitlab.com/hestia-earth/hestia-glossary/commit/196c3acbaeebd1dbd6466cbbc80b8fdb36a7e2be))
* **emission:** add emissions from organic soil cultivation ([31cd588](https://gitlab.com/hestia-earth/hestia-glossary/commit/31cd5889904283ee1117f92fac9f4ef934c198bf))
* **emission:** add generic terms for N and P losses to water ([d4280f2](https://gitlab.com/hestia-earth/hestia-glossary/commit/d4280f292dc1defdf8d6c83815b2bb1b8cb8bcd6))
* **emission:** add term `CO2, to air, biomass and soil carbon stock change` ([556ccf2](https://gitlab.com/hestia-earth/hestia-glossary/commit/556ccf22dba789a182e42de77e80e5ee80817047))
* **excreta:** add fish and crustacean excreta ([26ec09b](https://gitlab.com/hestia-earth/hestia-glossary/commit/26ec09b853b1272da77d3f35715d0f9076480e45))
* **excreta:** split out of animalProduct glossary and add properties ([c220a30](https://gitlab.com/hestia-earth/hestia-glossary/commit/c220a306186fef7f4d1ce0c2520093a21b5a2ff8))
* **geographies:** add `ecoClimateZone` lookup ([40e2ee6](https://gitlab.com/hestia-earth/hestia-glossary/commit/40e2ee6f7f2a4e306fb436ed1d9d0291822dfaa0))
* **geographies:** add FAO production quantity for crops and animal products ([357fa63](https://gitlab.com/hestia-earth/hestia-glossary/commit/357fa6314438e07624623782b1eaee93c33c24ec))
* **geographies:** add irrigation efficiency lookup table ([4e54338](https://gitlab.com/hestia-earth/hestia-glossary/commit/4e543380813bfb1d1aaed727ac152e032f80c3c6))
* **geographies:** update `Cottonseed` column to `Seed cotton` ([721d8a4](https://gitlab.com/hestia-earth/hestia-glossary/commit/721d8a4ff2e401dc364a7dd4285e35281b825af6))
* **inorganicFertilizer:** add `Monopotassium Phosphate (as K2O)` and K2O content lookup ([bae30ab](https://gitlab.com/hestia-earth/hestia-glossary/commit/bae30ab3121ceb4ed1464db83ca637d0e4fd0d78))
* **inorganicFertilizer:** add lookup for CO2 emissions from urea based fertilizers": ([eb869e9](https://gitlab.com/hestia-earth/hestia-glossary/commit/eb869e905bf19444b5493ee13bb417ea955ab6a2))
* **inorganicFertilizer:** add NH3 emissions lookup table ([9eccbbb](https://gitlab.com/hestia-earth/hestia-glossary/commit/9eccbbb205c44cc547be55c554cea80a046d1133)), closes [#167](https://gitlab.com/hestia-earth/hestia-glossary/issues/167)
* **inputs:** add awareWaterBasinId lookup ([b5ef2e6](https://gitlab.com/hestia-earth/hestia-glossary/commit/b5ef2e6566b04115383e934fa0229a1ce7ae6157))
* **inputs:** add new `pesticideAI` terms ([0364fc7](https://gitlab.com/hestia-earth/hestia-glossary/commit/0364fc7b01730a1898bf54f3f7c4a7388f69a883))
* **land use management terms:** add `Nursery duration` and `Orchard density` to the land use management glossary ([e9d7360](https://gitlab.com/hestia-earth/hestia-glossary/commit/e9d736072b76c273920b50b0b0b3b7d689ced9b0))
* **landUseManagement:** add C2_FACTORS lookup table ([1d35a82](https://gitlab.com/hestia-earth/hestia-glossary/commit/1d35a82d13fc53d491993fd47fd1c61532c33343))
* **liveAnimal:** add ch4 enteric fermentation lookup ([849a8aa](https://gitlab.com/hestia-earth/hestia-glossary/commit/849a8aa4cd9d4ae2d3748db5e4855d1772745579))
* **liveAnimal:** add terms for cows, bulls, heifers, steers, laying chickens ([5c24d6e](https://gitlab.com/hestia-earth/hestia-glossary/commit/5c24d6e922197137823ffed93c6eb5ce6d115bb4))
* **lookup:** rename `region-crop-cropGroupingFAOSTAT` to `region-crop-cropGroupingFAOSTAT-price` ([e82b24b](https://gitlab.com/hestia-earth/hestia-glossary/commit/e82b24bd90fc270eccfe1d14034c901faac31f89))
* **measurement:** add `fallowCorrection` term ([fdd5c4f](https://gitlab.com/hestia-earth/hestia-glossary/commit/fdd5c4fe665b6359f8e60c770d0254b8a4731205)), closes [#177](https://gitlab.com/hestia-earth/hestia-glossary/issues/177)
* **measurement:** add `Soil sulfate content` and `Soil electrical conductivity` ([bf8555c](https://gitlab.com/hestia-earth/hestia-glossary/commit/bf8555cb8c18262db4db3a195ec763156a8b7e94))
* **model:** add "other background database" field; change ecoinvent description ([da020c5](https://gitlab.com/hestia-earth/hestia-glossary/commit/da020c57d7ccfe88b0cb928e19ed1470b1726fc6))
* **model:** add `IPCC (1995)` model term ([621e4e9](https://gitlab.com/hestia-earth/hestia-glossary/commit/621e4e9b5c8d8b874177e0c978fd5869b385f5ad))
* **model:** add `IPCC 2013` characterisation models ([97b58d8](https://gitlab.com/hestia-earth/hestia-glossary/commit/97b58d800c14524560921c93bb001fd73d332dee))
* **model:** add `spatial` term ([d007782](https://gitlab.com/hestia-earth/hestia-glossary/commit/d0077827fba1ec0e046e689a099224eb30e48e80)), closes [#176](https://gitlab.com/hestia-earth/hestia-glossary/issues/176)
* **model:** add additional IPCC 2019 model ([f6ecf47](https://gitlab.com/hestia-earth/hestia-glossary/commit/f6ecf477db0273e3944864e1b1c4d79aa940537a))
* **model:** add chaudhary biodiversity models ([362e923](https://gitlab.com/hestia-earth/hestia-glossary/commit/362e92366647f362357b189a381e5394b25ae707))
* **model:** add new EMEA-EEA model ([a236832](https://gitlab.com/hestia-earth/hestia-glossary/commit/a236832ebc7c119e92009be93554f1fc203b30f9))
* **model:** delete `geospatial` term, add `other model` term ([61a5006](https://gitlab.com/hestia-earth/hestia-glossary/commit/61a5006fc9094e4fae29400e9aa15002131a7590))
* **organicFertilizer:** add lookup to classify fertilizers into type ([e9e87b4](https://gitlab.com/hestia-earth/hestia-glossary/commit/e9e87b494e1b6debbaf71a31ba31c747965baf5b))
* **organicFertilizer:** add nutrient composition properties to organic fertilizer unspecified ([9014647](https://gitlab.com/hestia-earth/hestia-glossary/commit/9014647331757fdfc54c084fbe11f99a943dbcec))
* **pesticide brand name:** add 3 terms ([342bbe7](https://gitlab.com/hestia-earth/hestia-glossary/commit/342bbe725bbde9be7f40bb7557bb8c5bf6433c88))
* **pesticideBrandName:** add diluted pesticide unspecified ([efa2003](https://gitlab.com/hestia-earth/hestia-glossary/commit/efa200369d6ae08bc4126801077b8767705b5a31))
* **practices:** add `operation` terms ([4d99031](https://gitlab.com/hestia-earth/hestia-glossary/commit/4d990315636dc15837ee29ae6d5419b95551af04))
* **property:** add `Potassium content (as K2O)` ([c381cba](https://gitlab.com/hestia-earth/hestia-glossary/commit/c381cba89570f31d2783f5f65c6c6175fdeba45a))
* **property:** add energy content terms ([437672b](https://gitlab.com/hestia-earth/hestia-glossary/commit/437672bb52ecb9cd3a74ae544f632a2fb463b671))
* **property:** add various slaughter weights per head ([a808794](https://gitlab.com/hestia-earth/hestia-glossary/commit/a808794361711bd46d974ab166928f0ca1ef8937))
* **property:** add volatile solids content ([36633bd](https://gitlab.com/hestia-earth/hestia-glossary/commit/36633bda8badba0e27a46cb6fdeb0ac7ba86a687))
* **region-inorganicFertilizerBreakdown-lookup:** add lookup table of country level inorganic fertilizers ([6c0121d](https://gitlab.com/hestia-earth/hestia-glossary/commit/6c0121d04af8ec5eac26e4748417dbb4f771cb99))
* **soilAmendement:** add lookup for CO2 emissions from lime ([0554268](https://gitlab.com/hestia-earth/hestia-glossary/commit/05542687e82082dd7ee33c4f0fe3b16ed4a14b36))
* **soilAmendment:** add epsom salts ([6304a3d](https://gitlab.com/hestia-earth/hestia-glossary/commit/6304a3d82c62ff912eb87eaafcc3c8ec89da1c6b))
* **water:** add definitions to terms ([4013158](https://gitlab.com/hestia-earth/hestia-glossary/commit/401315854d662ec3a49cd91052e7fc792c6ae7d4))
* **water:** remove reference to irrigation and make terms more general ([0ff6f20](https://gitlab.com/hestia-earth/hestia-glossary/commit/0ff6f203fba6500cc27777cf61679c1d4c9d3604))


### Bug Fixes

* move the "includes" or "excludes feedbacks" GWP suffix from `characterisedIndicator` to `model` ([102c80d](https://gitlab.com/hestia-earth/hestia-glossary/commit/102c80df3439070747d1a587170e6eb72fe2a8fb))
* remove `-` from `subClassOf` and fix broken `subClassOf` links ([ebde9eb](https://gitlab.com/hestia-earth/hestia-glossary/commit/ebde9eb115a3b5a83d4e011923a4005363344758)), closes [#163](https://gitlab.com/hestia-earth/hestia-glossary/issues/163)
* remove `-`, `<`, and `>` from `id` ([efabc20](https://gitlab.com/hestia-earth/hestia-glossary/commit/efabc20ea939102e245224f514ceb38b7cf9029c))
* rename cropGroupingPrice as cropGroupingFAOSTAT ([9fa3d7a](https://gitlab.com/hestia-earth/hestia-glossary/commit/9fa3d7a8170327b7ad27ba8a562273604dd2faca))
* **animalProduct:** resolve error in CH4 lookup ([1f220e0](https://gitlab.com/hestia-earth/hestia-glossary/commit/1f220e068242379086eb292b813c60d10474f0d4))
* **area yield lookups:** delete unused lookups ([c9beb45](https://gitlab.com/hestia-earth/hestia-glossary/commit/c9beb45019030fdb13ca791015981d5db3778b55))
* **crop:** add `seed_output_kg_sd` which was accidentally removed in the previous merge ([0d33650](https://gitlab.com/hestia-earth/hestia-glossary/commit/0d33650af3998d8a315d6951b1b879bbf88f6196))
* **crop:** change `cropGroupingResidue` for `satsuma` to `citrus`, and fix hyperlink ([404ec08](https://gitlab.com/hestia-earth/hestia-glossary/commit/404ec08d02566f4407c076842da931386843aadf))
* **crop:** correct unit error in orchard crop n content values ([3c04bfa](https://gitlab.com/hestia-earth/hestia-glossary/commit/3c04bfa28a8e1703d2df9d66146e8c6100c6c374))
* **crop:** remove filter ([2710ff0](https://gitlab.com/hestia-earth/hestia-glossary/commit/2710ff0a79ff499399a5e923ad76fd18d7baa498))
* **crop:** remove term name `Seed_Output_kg_sd` where values are missing ([6533c4d](https://gitlab.com/hestia-earth/hestia-glossary/commit/6533c4d58fe4d4dc4e881ad3719500a5e40ed49b))
* **crop:** set nitrogen content units to match glossary ([dc8bc33](https://gitlab.com/hestia-earth/hestia-glossary/commit/dc8bc33c86b25789c60c5b7c847fa4bee3980ef5))
* **crop FAOSTAT lookups:** add missing crops to fix reconciliation errors ([4a67fda](https://gitlab.com/hestia-earth/hestia-glossary/commit/4a67fda46a6994fbe195f253eaa4fb19d9248a9e))
* **crop landUseChange lookup:** remove redundant columns ([e79b317](https://gitlab.com/hestia-earth/hestia-glossary/commit/e79b3171c0481957bb9147f39177175aa92a064d))
* **ecoClimateZone-lookup:** error in semicolon place ([dc812b7](https://gitlab.com/hestia-earth/hestia-glossary/commit/dc812b7084cbbf8dbf9b16d53dac6b5612771bf2))
* **emissions:** rename "manure management" "excreta management" ([7a749ad](https://gitlab.com/hestia-earth/hestia-glossary/commit/7a749adc7ba95b0960bb8d117f24efc73caa5c6a))
* **geographies:** add missing cropGrouping ([1cb799f](https://gitlab.com/hestia-earth/hestia-glossary/commit/1cb799fe85fd5419ee7bc233ba5162a05d5ea60c))
* **geographies:** replace column names in cells with default no-value ([b8d88d2](https://gitlab.com/hestia-earth/hestia-glossary/commit/b8d88d2a361811b4abd5d97d3e708e0720bede86))
* **index:** rmeove use of `path` ([c2f15a3](https://gitlab.com/hestia-earth/hestia-glossary/commit/c2f15a3188f1af2f2a557ac77f13c6fca47b7e70))
* **inorganicFertilzer lookup:** remove space in header ([83439c6](https://gitlab.com/hestia-earth/hestia-glossary/commit/83439c68a0c7b94df2a32298c9d4c42595ac6581))
* **inputs:** remove unused columns for `awareWaterBasinId` lookup ([0c2c89a](https://gitlab.com/hestia-earth/hestia-glossary/commit/0c2c89a6df762a1f85e739cdb706a0fcca1e2d82))
* **lookup:** match energy content lookup to glossary id ([cb15dfd](https://gitlab.com/hestia-earth/hestia-glossary/commit/cb15dfd9aaa20b668aea0b5faa5258d227da2080))
* **manureManagement:** rename glossary excretaManagement ([e155d23](https://gitlab.com/hestia-earth/hestia-glossary/commit/e155d23b087f8b7a9a8866e9c2ab9baecd4cead0))
* **measurement:** revert definition for `heavyWinterPrecipitation` to 0, 1 ([ef39c01](https://gitlab.com/hestia-earth/hestia-glossary/commit/ef39c0161089136c8bb0bf85d9afe67a1baaaf37))
* **measurement:** set units on `heavyWinterPrecipitation` to boolean ([dd552db](https://gitlab.com/hestia-earth/hestia-glossary/commit/dd552dbbcb46816feb772e304107b12617171980))
* **model:** remove `pooreNemecek2018NitrateLeaching` ([ab41bff](https://gitlab.com/hestia-earth/hestia-glossary/commit/ab41bff4ecaf8529740462c07e017b16013f23b0))
* **model:** rename `IPCC 2013...` models to just `IPCC 2013` ([dd05a71](https://gitlab.com/hestia-earth/hestia-glossary/commit/dd05a715185f825f06ca12f6a149a7f9c3d609e6))
* **operation:** fix wrong column name for `openLCAId` ([f648fd7](https://gitlab.com/hestia-earth/hestia-glossary/commit/f648fd7dab10552de3423d4ea88a6fe7ac244463))
* **operation:** remove duplicated `openLCAId` ([637bf18](https://gitlab.com/hestia-earth/hestia-glossary/commit/637bf18840f13020bea3ef1c9bebb424b22afb7a))
* **pesticideAI:** remove term `Soybean oil, Me ester` ([05814b5](https://gitlab.com/hestia-earth/hestia-glossary/commit/05814b524ce0c106ef3409db5b3b85d781f8c9ae)), closes [#180](https://gitlab.com/hestia-earth/hestia-glossary/issues/180)
* **productionQuantity lookup:** rename column headers ([653684e](https://gitlab.com/hestia-earth/hestia-glossary/commit/653684eefd49a9c9ba80cfee51d12ddf38474689))
* **region:** rename `Cherry` which duplicates with crop ([43e7d76](https://gitlab.com/hestia-earth/hestia-glossary/commit/43e7d76258ee0a4ba663b6317a8f896d6aa82d51))

### [0.0.5](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.0.4...v0.0.5) (2021-04-03)


### Features

* spell out full chemical element names in term names ([c8f4cef](https://gitlab.com/hestia-earth/hestia-glossary/commit/c8f4cef36b9f9d968b4caac5b9362631924044d8)), closes [#142](https://gitlab.com/hestia-earth/hestia-glossary/issues/142)
* **crop:** add `cropGroupingPrice` to crop glossary ([cc075bd](https://gitlab.com/hestia-earth/hestia-glossary/commit/cc075bddc138c93485150473729a2d29c2ccb048)), closes [#99](https://gitlab.com/hestia-earth/hestia-glossary/issues/99)
* **crop:** add FBS lookup; add seed sd lookup; improve seed data ([6a18bbb](https://gitlab.com/hestia-earth/hestia-glossary/commit/6a18bbb8915bca63a938bdd3f92b5d89d8dd748c))
* **crop:** add lookup `isAboveGroundCropResidueRemoved` ([40a51b1](https://gitlab.com/hestia-earth/hestia-glossary/commit/40a51b1597d506b90f32da4618e76488326ebf8c))
* **crop:** add lookup values for above and below ground crop residue nitrogen ([3948299](https://gitlab.com/hestia-earth/hestia-glossary/commit/3948299736347c1f74adb3ec3d96e6daa08f56ed)), closes [hestia-gap-filling-engine#50](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/50)
* **crop-lookup:** add global `economicValueShare` values ([6cc1091](https://gitlab.com/hestia-earth/hestia-glossary/commit/6cc109197216a312cb9b538dae1f74d8355024b2))
* **cropResidue:** simplify term names to remove "dry matter" ([3a37997](https://gitlab.com/hestia-earth/hestia-glossary/commit/3a37997f5a2cc216fb95a2995d106c64c73fb7f2))
* **emission:** add CO2 emissions from drained organic soils ([95f3dda](https://gitlab.com/hestia-earth/hestia-glossary/commit/95f3dda1ee6b7cfa506c1ae9af8656e207faaf76)), closes [#143](https://gitlab.com/hestia-earth/hestia-glossary/issues/143)
* **emission:** add term for drained organic soils N2O emissions ([fac4e37](https://gitlab.com/hestia-earth/hestia-glossary/commit/fac4e3727cd14ce6c6cbf4eaa6e375fd0ea3e8de)), closes [#143](https://gitlab.com/hestia-earth/hestia-glossary/issues/143)
* **emissions:** add terms for emissions from inputs production; remove "all inputs" terms ([78d6d4a](https://gitlab.com/hestia-earth/hestia-glossary/commit/78d6d4a7166747aefd94b4d837d9abc63957ebc0)), closes [#132](https://gitlab.com/hestia-earth/hestia-glossary/issues/132)
* **geographies:** add lookup between region and `organic` `standardLabels` ([d693e4a](https://gitlab.com/hestia-earth/hestia-glossary/commit/d693e4a75ac2a1828e3a3bdbf53dc19adffbf0f0))
* **geographies:** add region irrigated lookup ([2b50ab3](https://gitlab.com/hestia-earth/hestia-glossary/commit/2b50ab3021ab63b048ac3ca5fa95ff26f61d0ae8))
* **inorganicFertilizer:** rename N, P, and K, to Nitrogen, Phosphorus, and Potassium ([8e983a3](https://gitlab.com/hestia-earth/hestia-glossary/commit/8e983a37504d24b1dde4c0ec9a7a54e41c1a74ae)), closes [#140](https://gitlab.com/hestia-earth/hestia-glossary/issues/140)
* **lookups:** migrate 2 lookups from gap-filling ([cb34598](https://gitlab.com/hestia-earth/hestia-glossary/commit/cb3459802032b8da04713859d79cf3fb2872310d)), closes [#99](https://gitlab.com/hestia-earth/hestia-glossary/issues/99)
* **lookups:** migrate FAO_Price lookup ([cb3108f](https://gitlab.com/hestia-earth/hestia-glossary/commit/cb3108faca2e8479f9f204658ddc740733b097d6)), closes [#99](https://gitlab.com/hestia-earth/hestia-glossary/issues/99)
* **material:** add terms for metals, glass, plastic, and wood ([bb321a7](https://gitlab.com/hestia-earth/hestia-glossary/commit/bb321a795fafcb1001f40db8506b40e999be7d21))
* **measurement:** add lookup for data validation called oneMeasurementPerSite ([d6e0b3a](https://gitlab.com/hestia-earth/hestia-glossary/commit/d6e0b3a60d86ae34c56b8da47d522f14bbe8bc4a))
* **measurement:** add terms for nitrogen in precipitation and irrigation ([1d2ef2d](https://gitlab.com/hestia-earth/hestia-glossary/commit/1d2ef2d3da70a46f7a503b33af8077ff97d0ff8f))
* **methodEmissionResourceUse:** add terms for soil water measurement ([875ddb7](https://gitlab.com/hestia-earth/hestia-glossary/commit/875ddb73c72a7c67ed5e9ff7eba9ca8cb43abd21))
* **organicFertilizer:** improve term names for "unspecified" and "other" fertilizers ([3c02a73](https://gitlab.com/hestia-earth/hestia-glossary/commit/3c02a738f63aa06d5ca431bea8ccf46da3520f72)), closes [#141](https://gitlab.com/hestia-earth/hestia-glossary/issues/141)
* **other:** add term for `saplings` ([f3fa65c](https://gitlab.com/hestia-earth/hestia-glossary/commit/f3fa65c2d0b15291f10d33c6d331122c7d035299))
* **region:** add `gadmCountry` ([6723315](https://gitlab.com/hestia-earth/hestia-glossary/commit/6723315bbd826028d54a9c2a6010221fb800818b)), closes [#133](https://gitlab.com/hestia-earth/hestia-glossary/issues/133)
* **standardsLabels:** add `isOrganic` lookup ([62acf87](https://gitlab.com/hestia-earth/hestia-glossary/commit/62acf87ae776b28968d269fe2e8588e238a57223))


### Bug Fixes

* **crop:** add missing "" ([555bb68](https://gitlab.com/hestia-earth/hestia-glossary/commit/555bb68807d139d30c7f0c33477d752975471075))
* **crop:** make average rooting depth lookup a formula ([58d6dd8](https://gitlab.com/hestia-earth/hestia-glossary/commit/58d6dd8deef25c756ccf309b60db65766be26aba))
* **crop:** set false values on residue removed lookup ([77706e2](https://gitlab.com/hestia-earth/hestia-glossary/commit/77706e278c3987e5d001efba3ae84128ff1d6ab0))
* **crop-lookup:** add missing colons ([3c6f195](https://gitlab.com/hestia-earth/hestia-glossary/commit/3c6f195d7f8f75cf72aab12dce7c1089ff60ec5f))
* **cropGroupingPrice:** fix missing term names issue ([ff34c1e](https://gitlab.com/hestia-earth/hestia-glossary/commit/ff34c1e6e7e78a7eb5dcc9c570d41a1bb66f4da9))
* **fuel:** error in property numbering ([150b64e](https://gitlab.com/hestia-earth/hestia-glossary/commit/150b64ebf879c0c781ddfb837b6555f8ea859db5)), closes [#136](https://gitlab.com/hestia-earth/hestia-glossary/issues/136)
* **geographies:** remove `ecoregion` ([55a4bfe](https://gitlab.com/hestia-earth/hestia-glossary/commit/55a4bfe378a18ca5a4549edcaf60873995169883))
* **inorganicFertilizer:** remove term "Inorganic fertilizer, unspecified" ([b5d87cf](https://gitlab.com/hestia-earth/hestia-glossary/commit/b5d87cf7af7370822fa52ad8dec94d66fae6c158))
* **landUseManagement:** update term descriptions following changes to how `cycleDuration` is defined in schema ([79f845d](https://gitlab.com/hestia-earth/hestia-glossary/commit/79f845d1847e9e7f4e66e49f6fa120aabe3753d2))
* **measurement:** increase `maximum` for cation exchange capacity ([b08d525](https://gitlab.com/hestia-earth/hestia-glossary/commit/b08d5255e39f3a1ee4ada6ebe7393a9be04e5087))
* **measurement:** increase `minimum` and `maximum` range for soil bulk density ([a1ab7fa](https://gitlab.com/hestia-earth/hestia-glossary/commit/a1ab7fa31bc6d1d237846792f87b54a8629a6ea9))
* **measurement:** increase `minimum` and `maximum` range for soil bulk density ([41c0053](https://gitlab.com/hestia-earth/hestia-glossary/commit/41c0053690bb86f652d67cd3f36c0cb3bbe95859))
* **measurement:** rename "year of Cycle" and "during Cycle" terms to "annual" and "period" ([823c5c8](https://gitlab.com/hestia-earth/hestia-glossary/commit/823c5c812ce5109e46d45287eab10e6827d7d8e9)), closes [#137](https://gitlab.com/hestia-earth/hestia-glossary/issues/137)
* **measurements:** increase `maximum` on soil nutrient measurements ([2f0674e](https://gitlab.com/hestia-earth/hestia-glossary/commit/2f0674e97dd33cf931f020e2ce115be84b201e6c))
* **organicFertilizer:** remove "type" from "type unspecified" ([cc11082](https://gitlab.com/hestia-earth/hestia-glossary/commit/cc110824e229fecde6ba1fd5b3212f838a1a4bb1))
* **organicFertilizer:** remove `valueType` field ([181058c](https://gitlab.com/hestia-earth/hestia-glossary/commit/181058c01da41c4a8abd602a70b352b08b469e3a))
* **pesticideAI:** add back `copper` following prior deletion ([233ecfd](https://gitlab.com/hestia-earth/hestia-glossary/commit/233ecfd0097784411fdb7b593d8e00d51b8d6cd5))
* **pesticideAI:** remove pesticides which are elements or fertilizers ([2428ed5](https://gitlab.com/hestia-earth/hestia-glossary/commit/2428ed5b22f0c1a60c9ba5d8a10be2b2f5289567))
* **pesticideBrandName:** remove brands with missing CAS numbers ([da40bee](https://gitlab.com/hestia-earth/hestia-glossary/commit/da40beee7fb4b5caa5be6836cc2b91f714730c07)), closes [#16](https://gitlab.com/hestia-earth/hestia-glossary/issues/16)
* **property:** delete `latitude`, `longitude` and `areaKm2` terms ([fb557c5](https://gitlab.com/hestia-earth/hestia-glossary/commit/fb557c553e4a0f5112fb1dc850e2dc2d858c2386))
* **region:** set `gadmLevel` to text (string) in Excel ([7219062](https://gitlab.com/hestia-earth/hestia-glossary/commit/7219062d0f1600f5352770fc49a198059ee7daf0))
* **region-crop-lookup:** correct term name ([5f4b152](https://gitlab.com/hestia-earth/hestia-glossary/commit/5f4b1524a50dbc78592a9bb1f678bde5391588b7))
* **region-crop-lookup:** remove unused crop ([7a366a8](https://gitlab.com/hestia-earth/hestia-glossary/commit/7a366a879767ba63aef066df51d8cc44fd7aa2ba))
* **region-lookup:** remove term.name field from lookup table ([c9deab2](https://gitlab.com/hestia-earth/hestia-glossary/commit/c9deab22dffb849f05530b24462c6ccd63c280d8))
* **region-lookup:** update practice factors for errors ([764df91](https://gitlab.com/hestia-earth/hestia-glossary/commit/764df91f97a7a282c5da5777ccb56524e6f09178))
* **water:** set units to `m3` instead of `mm` ([786742c](https://gitlab.com/hestia-earth/hestia-glossary/commit/786742cf78d42781a6e2e8a50bbb878d04d4ecbe))

### [0.0.4](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.0.3...v0.0.4) (2021-02-19)


### Features

* **antibiotic:** create new termType ([b46d535](https://gitlab.com/hestia-earth/hestia-glossary/commit/b46d5358c7bfaefc84289d893ad85d728204dfb1))
* **cropResidue:** create new termType ([c2c1d23](https://gitlab.com/hestia-earth/hestia-glossary/commit/c2c1d23f52a18d71aaa2e7aef32913439a3a13d9))


### Bug Fixes

* move `fry` and `fingerlings` to `liveAquaticSpecies` ([bbcb173](https://gitlab.com/hestia-earth/hestia-glossary/commit/bbcb17351468c5140718dfc3cd39c7de5b89c0ba)), closes [#131](https://gitlab.com/hestia-earth/hestia-glossary/issues/131)
* move `minimum` and `maximum` properties into lookup tables ([381e316](https://gitlab.com/hestia-earth/hestia-glossary/commit/381e316f4db16cace37e81553a44815005812ab8))
* **antibiotic:** move `amoxicillin` to `antibiotic` ([46bb880](https://gitlab.com/hestia-earth/hestia-glossary/commit/46bb8807c1504988378614457dd538951fafe7fd))
* **crop:** remove broken url ([6975ffb](https://gitlab.com/hestia-earth/hestia-glossary/commit/6975ffb34863b4186028b189782c1b3ff9288bc1)), closes [#128](https://gitlab.com/hestia-earth/hestia-glossary/issues/128)
* **crop:** resolve further encoding issues in wikipedia urls ([b0f9100](https://gitlab.com/hestia-earth/hestia-glossary/commit/b0f9100dceabe9fa4d0e2b30fe5b4e60f947150c)), closes [#128](https://gitlab.com/hestia-earth/hestia-glossary/issues/128)
* **cropResidue:** delete fresh matter terms; add subClassOf ([29cbc29](https://gitlab.com/hestia-earth/hestia-glossary/commit/29cbc29cf0ab965844c16f6d7fc19a5762c5f27e))
* **liveAquaticSpecies:** update `fishbase` column header name ([58eaeea](https://gitlab.com/hestia-earth/hestia-glossary/commit/58eaeeac356311de265b0e5eb62105ce1139b219)), closes [#128](https://gitlab.com/hestia-earth/hestia-glossary/issues/128)
* **other:** move `lime` into `soilAmendment` ([451fa7d](https://gitlab.com/hestia-earth/hestia-glossary/commit/451fa7deb0b21594bcda23ce6a508833afcdfb1e))
* **region:** error in `latitude` and `longitude` for Indonesia ([3c2e1aa](https://gitlab.com/hestia-earth/hestia-glossary/commit/3c2e1aad26b5843208f1202cc955fb6c2ca8b6b5))
* **region:** force `gadmLevel` to `integer` ([4ded6c8](https://gitlab.com/hestia-earth/hestia-glossary/commit/4ded6c89520ecd39ab3b6090c7c2cf30cffef63f))
* **region:** reorder columns following changes to schema ([619fce7](https://gitlab.com/hestia-earth/hestia-glossary/commit/619fce7d7a1619b2ae68659f62cfae3b2c454c56)), closes [#113](https://gitlab.com/hestia-earth/hestia-glossary/issues/113)
* **waterRegime:** remove `uplandRice` term ([9c9f653](https://gitlab.com/hestia-earth/hestia-glossary/commit/9c9f6537fd118ac4937db5ebdf6afdea94616817)), closes [#131](https://gitlab.com/hestia-earth/hestia-glossary/issues/131)

### [0.0.3](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.0.2...v0.0.3) (2021-02-11)


### Bug Fixes

* **impact assessment:** fix generated resource ([2941ee9](https://gitlab.com/hestia-earth/hestia-glossary/commit/2941ee99f01dddfe4ec22ad0e0c1d6624cdcb1bc))

### [0.0.2](https://gitlab.com/hestia-earth/hestia-glossary/compare/v0.0.1...v0.0.2) (2021-02-11)


### Features

* add `impactAssessment` resource ([f36b7e1](https://gitlab.com/hestia-earth/hestia-glossary/commit/f36b7e1d9e7cade6590e15be9d08e1f085c488b9))
* add CF's lookup table to `emission`; fix typo in `characterisedIndicator` ([325cf4a](https://gitlab.com/hestia-earth/hestia-glossary/commit/325cf4a27def38cc95c08bb6c6745c2b96339872))
* add lookup `generateImpactAssessment` ([397e6f8](https://gitlab.com/hestia-earth/hestia-glossary/commit/397e6f8c81ffdb721b6640a52b6be645917bc604))
* move Properties which are identifiers or hyperlinks to schema fields ([f679f47](https://gitlab.com/hestia-earth/hestia-glossary/commit/f679f472e6fef533ba039b11c2073bf3d8fc6ba6))
* **animalProduct:** add CH4 emissions factor per head ([edcde51](https://gitlab.com/hestia-earth/hestia-glossary/commit/edcde51abb4ed3c10f6cbd53b3fc5c0266b47ef7))
* **animalProduct:** add new terms for animal derived feed products ([d17027a](https://gitlab.com/hestia-earth/hestia-glossary/commit/d17027a5dcdf86f02cf206dfd48640c79377f411))
* **convert_to_lookups:** add lookup converter ([1f50897](https://gitlab.com/hestia-earth/hestia-glossary/commit/1f50897d2e5569b77d90631d1fe7dee541898483))
* **convert_to_lookups:** add revised lookup converter ([5ffaac9](https://gitlab.com/hestia-earth/hestia-glossary/commit/5ffaac92437187c48bfa93609ebb183327ad95f6))
* **convert_to_lookups:** allow lookups with string values ([1590b30](https://gitlab.com/hestia-earth/hestia-glossary/commit/1590b3058309467243e7d35c49e15378bd2054f3))
* **convert_to_lookups:** allow multiple keys in json lookups ([2d215d3](https://gitlab.com/hestia-earth/hestia-glossary/commit/2d215d3a3f8ae57dd08cca79f3ae8d944d160c55))
* **crop:** add Adzuki bean, dry ([cb1cb9e](https://gitlab.com/hestia-earth/hestia-glossary/commit/cb1cb9e7a24391183b1e6f2313d57fd36d5c5aa9))
* **crop:** add Default_economic_allocation_global ([25d64d6](https://gitlab.com/hestia-earth/hestia-glossary/commit/25d64d635b77d8005d981335924fabb244452658))
* **crop:** add dry matter composition to crop ([361f321](https://gitlab.com/hestia-earth/hestia-glossary/commit/361f321462251f1d19a90b26e2a935164f1153d6))
* **crop:** add energy composition data ([b639d7b](https://gitlab.com/hestia-earth/hestia-glossary/commit/b639d7b6b46be1f4f38543f5f2276b97852c872d))
* **crop:** add guar and related products ([7378cce](https://gitlab.com/hestia-earth/hestia-glossary/commit/7378cce59f44a61beae4768dba0130ec8852e1c2))
* **crop:** add kiwifruit ([49289ac](https://gitlab.com/hestia-earth/hestia-glossary/commit/49289ac6213a406fd06041269e6a6864e106d16f))
* **crop:** add lookups for rooting depth and crop phosphorus parameters ([a4b9e41](https://gitlab.com/hestia-earth/hestia-glossary/commit/a4b9e4162dbae9f95a8496719b72e9573a1b2681))
* **crop:** add more detail on hazelnuts ([11e6b5f](https://gitlab.com/hestia-earth/hestia-glossary/commit/11e6b5f57dab2eb6a1fc7aebd35a34c28ccdb3ba))
* **crop:** add multiple higher level crop groupings, such as cereals, vegetables ([bcd7b49](https://gitlab.com/hestia-earth/hestia-glossary/commit/bcd7b49a9a6013d86549d7fa1f784f17e9edae9b)), closes [#73](https://gitlab.com/hestia-earth/hestia-glossary/issues/73)
* **crop:** add nitrogen uptake as a term ([e8f4e24](https://gitlab.com/hestia-earth/hestia-glossary/commit/e8f4e24d15f77f670aa76abd14fbdbc42761bd2c)), closes [#91](https://gitlab.com/hestia-earth/hestia-glossary/issues/91)
* **crop:** add rapeseed meal and oil ([62d82fd](https://gitlab.com/hestia-earth/hestia-glossary/commit/62d82fd9c23d316cc29ee325a1c1c568c42e16bf))
* **crop:** add rice by-products ([0b515b9](https://gitlab.com/hestia-earth/hestia-glossary/commit/0b515b948f9ae1410116137bf344d8453e331246))
* **crop:** add selected crop products from feedipedia ([dcd5b9e](https://gitlab.com/hestia-earth/hestia-glossary/commit/dcd5b9ee83c3f0ce82e2ec2a16e8f4d1f2e0f9a2)), closes [#77](https://gitlab.com/hestia-earth/hestia-glossary/issues/77)
* **crop:** add synonyms for soybeans ([ec35a0e](https://gitlab.com/hestia-earth/hestia-glossary/commit/ec35a0ee5ea41a252a9eb5e359aceb329d35d2e4))
* **cropEstablishment:** add fertigation and number of fertilizations ([2c524fe](https://gitlab.com/hestia-earth/hestia-glossary/commit/2c524fed71de998153351c833b017639c70bcab3))
* **cropEstablishment:** add methods for hill planting ([15611de](https://gitlab.com/hestia-earth/hestia-glossary/commit/15611de13c97af245cc44de4251873ad4ca8f9bf))
* **cropEstablishment:** add row spacing ([eb30f73](https://gitlab.com/hestia-earth/hestia-glossary/commit/eb30f735e992aabaaab1a2a661eab5962979995f))
* **cropResidueManagement:** add "Residue incorporated" ([19297b4](https://gitlab.com/hestia-earth/hestia-glossary/commit/19297b440af54ff3a6101769bf668ae492fd97ff)), closes [#104](https://gitlab.com/hestia-earth/hestia-glossary/issues/104)
* **cropResidueManagement:** add term `Residue burnt (before allowing for combustion factor)` ([ef11903](https://gitlab.com/hestia-earth/hestia-glossary/commit/ef11903922744b6defaa00e1619969acaa915ccb))
* **ecoregion:** change HESTIA id to match ecoregion id ([d20d0ce](https://gitlab.com/hestia-earth/hestia-glossary/commit/d20d0ce2e1956cceed1419cc638fddfcbd325ea5))
* **ecoregion-lookup:** create an ecoregion-lookup containing biodiversity CF from Chaudhary et al 2015 ([2d19934](https://gitlab.com/hestia-earth/hestia-glossary/commit/2d1993409034c3b9f2ad5a4256b71ee396467c18)), closes [#119](https://gitlab.com/hestia-earth/hestia-glossary/issues/119)
* **ecoregion-lookup:** create an ecoregion-lookup containing biodiversity CF from Chaudhary et al 2015 ([f1e6b2a](https://gitlab.com/hestia-earth/hestia-glossary/commit/f1e6b2a252aff1223d98c41a3655ece46f96763d)), closes [#119](https://gitlab.com/hestia-earth/hestia-glossary/issues/119)
* **emission:** add animal products emissions ([67e124a](https://gitlab.com/hestia-earth/hestia-glossary/commit/67e124a0ea3ac98802a895734ae001952c35e16a))
* **emission:** add CFCs and HFCs ([693b68c](https://gitlab.com/hestia-earth/hestia-glossary/commit/693b68ca27c909b3e3899d57e22138e270fcc275)), closes [#71](https://gitlab.com/hestia-earth/hestia-glossary/issues/71)
* **emission:** add CH4, to air, flooded rice ([73a5f58](https://gitlab.com/hestia-earth/hestia-glossary/commit/73a5f580900095eef2f5cf8f0a716002cf35260f))
* **emission:** add further emissions from fuel combustion ([7dcb879](https://gitlab.com/hestia-earth/hestia-glossary/commit/7dcb87977f5b39aa752ff380c817ba85d16eaf47))
* **emission:** add further terms ([91780c1](https://gitlab.com/hestia-earth/hestia-glossary/commit/91780c1e45652610c207667eb1e52a27b1ccf159))
* **emission:** add merged direct and indirect N2O from fertilizer ([f2b894d](https://gitlab.com/hestia-earth/hestia-glossary/commit/f2b894d41eda7f35ea5647141b2cac3c32f74c23))
* **emission:** add N2O background soil emissions ([e5733a9](https://gitlab.com/hestia-earth/hestia-glossary/commit/e5733a9df466dec746567cd7741e921ff067af64))
* **emission:** add nitrogen oxide ([5b7b1f8](https://gitlab.com/hestia-earth/hestia-glossary/commit/5b7b1f87625788bfef3210ca75db631867f3ebf8))
* **emission:** add NOx fuel combustion ([3f3fa20](https://gitlab.com/hestia-earth/hestia-glossary/commit/3f3fa204318657bcda853d74642f05ae90a36db6))
* **emission:** add P and N erosion and drainage water losses ([6ea5c98](https://gitlab.com/hestia-earth/hestia-glossary/commit/6ea5c989366751df2b896b0fea133e550afda24a))
* **emission:** add PM10, PM2.5, and NMVOCs ([02ccac1](https://gitlab.com/hestia-earth/hestia-glossary/commit/02ccac11f9f997bf5d3fc7a35ff7ec35cafcc9b3)), closes [#90](https://gitlab.com/hestia-earth/hestia-glossary/issues/90)
* **emission:** add synonyms ([c86bfc2](https://gitlab.com/hestia-earth/hestia-glossary/commit/c86bfc245fef18a5019dd8bbd686d548789e542f))
* **emission:** improve definitions of "all origins" terms ([29a1bb7](https://gitlab.com/hestia-earth/hestia-glossary/commit/29a1bb7e8ee89602700c310388412251f5d97e07))
* **emission:** remove background emission terms ([d914394](https://gitlab.com/hestia-earth/hestia-glossary/commit/d914394679d73de8730741e00a0103226e678b36))
* **gadm:** add additional region groupings ([61345aa](https://gitlab.com/hestia-earth/hestia-glossary/commit/61345aa35d1a0d9bb1c93bdf0bbac5c950fb2b8a))
* **gadm:** add lookup table ([18512e1](https://gitlab.com/hestia-earth/hestia-glossary/commit/18512e1055996e784247c275118f09a965295eca))
* **gadm mappings:** add synonyms ([a154258](https://gitlab.com/hestia-earth/hestia-glossary/commit/a154258943be44d345c85148cc7da666c3bd8faf))
* **inorganicFertilizer:** add ABC as synonym for ammonium bicarbonate ([779f9df](https://gitlab.com/hestia-earth/hestia-glossary/commit/779f9dfc104d99b85a9e828cf92cdc36c7bb337a))
* **inorganicFertilizer:** add Ammonium Sulphate Nitrate (as N) ([bdf1462](https://gitlab.com/hestia-earth/hestia-glossary/commit/bdf14621ee191abb321778f195e0934c273fb583))
* **inorganicFertilizer:** add Aqueous Ammonia, fix typo in Anhydrous Ammonia name ([60ee35c](https://gitlab.com/hestia-earth/hestia-glossary/commit/60ee35cb8bdb8312e9c3be1e6648d9ef153da9e7))
* **inorganicFertilizer:** add Dicalcium Phosphate ([62e3610](https://gitlab.com/hestia-earth/hestia-glossary/commit/62e3610cac9e3dc065491dfa44bd70df3b81228c))
* **inorganicFertilizer:** add NK compounds, fix synonyms ([0e1c44a](https://gitlab.com/hestia-earth/hestia-glossary/commit/0e1c44a192a04c09a31800640fb4961dde008fa2))
* **inorganicFertilizer:** add Sodium Nitrate ([46effe8](https://gitlab.com/hestia-earth/hestia-glossary/commit/46effe80afb5322922d125354be5ecf4d4bd72b6))
* **inputs:** add transport and update other and electricity ([20fecdd](https://gitlab.com/hestia-earth/hestia-glossary/commit/20fecdd82ec9609633622ccb4856179295f93443))
* **landUseManagement:** add multi cropping practices ([38d942a](https://gitlab.com/hestia-earth/hestia-glossary/commit/38d942a4b783b2f955747104d9d81a0e47e2fde9))
* **landUseManagement:** add orchard density and shade tree cover ([e33b53e](https://gitlab.com/hestia-earth/hestia-glossary/commit/e33b53e6d73c244501a03a07e66f2f2074b297e4)), closes [#44](https://gitlab.com/hestia-earth/hestia-glossary/issues/44)
* **landUseManagement:** add shade tree spacing and species name ([d3b020e](https://gitlab.com/hestia-earth/hestia-glossary/commit/d3b020ee95c9bd2f3103a4f8412b6cb675dc30ff)), closes [#44](https://gitlab.com/hestia-earth/hestia-glossary/issues/44)
* **landUseManagement:** add shade trees ([a4ff57c](https://gitlab.com/hestia-earth/hestia-glossary/commit/a4ff57c329621eeb03d5f03c10344901d69225b6)), closes [#44](https://gitlab.com/hestia-earth/hestia-glossary/issues/44)
* **liveAnimal:** add default processing conversions ([11b5cff](https://gitlab.com/hestia-earth/hestia-glossary/commit/11b5cff48898cec8990d0bad112173165ff77f73))
* **liveAnimal:** add juvenile animals ([49d906f](https://gitlab.com/hestia-earth/hestia-glossary/commit/49d906f88007aef63b9cac3bddf8174561d8a540))
* **liveAquaticSpecies:** add juvenile fish and fingerlings ([53efd8d](https://gitlab.com/hestia-earth/hestia-glossary/commit/53efd8d77a1538ff0767c9ba6574ecba932fc726))
* **look ups:** Add crop based gap filling look ups to glossary look up ([e172717](https://gitlab.com/hestia-earth/hestia-glossary/commit/e172717428c85d68f1176aaa4e83d62029d41b2c)), closes [#99](https://gitlab.com/hestia-earth/hestia-glossary/issues/99)
* **manureManagement:** create glossary ([d290307](https://gitlab.com/hestia-earth/hestia-glossary/commit/d2903071acee584be8585cda4afe9c697a6a1f2a)), closes [#107](https://gitlab.com/hestia-earth/hestia-glossary/issues/107)
* **mappings:** add synonym for Gilan ([82777d4](https://gitlab.com/hestia-earth/hestia-glossary/commit/82777d4886d1be27171e5939fdd8774c54286693))
* **measuement:** add water table height ([a112c39](https://gitlab.com/hestia-earth/hestia-glossary/commit/a112c39e7bc5395f698a8c3dad45c9e55a901e5c))
* **measurement:** add cadmium and water filled pore space; fix definition of percentages ([6a9608f](https://gitlab.com/hestia-earth/hestia-glossary/commit/6a9608f80ed6be543dc13776df242cfce9c51b30))
* **measurement:** add elevation as synonym for altitude ([a9aaff5](https://gitlab.com/hestia-earth/hestia-glossary/commit/a9aaff519514ec615f46fe326d21edf1ffdeff77))
* **measurement:** add hard pan depth ([49748b3](https://gitlab.com/hestia-earth/hestia-glossary/commit/49748b32f5febe8be90017e075f85635a3321396))
* **measurement:** add maximum and minimum values ([c4d510e](https://gitlab.com/hestia-earth/hestia-glossary/commit/c4d510e9776639f0a32ca9beee2be1ba894cf560)), closes [#82](https://gitlab.com/hestia-earth/hestia-glossary/issues/82)
* **measurement:** add soil mean weight diameter ([2dacc80](https://gitlab.com/hestia-earth/hestia-glossary/commit/2dacc8087c7b3f1b010b4e7c8b4438d4d196d8b4))
* **measurement:** add soil microbial measurements ([2be77ef](https://gitlab.com/hestia-earth/hestia-glossary/commit/2be77ef5295593454280041f4eb2f932f365c33e))
* **measurement:** add soil redox potential ([edcd82e](https://gitlab.com/hestia-earth/hestia-glossary/commit/edcd82e2038cd55cde40aa72249b12ecbf97e518))
* **measurement:** add soil temperature ([af7de8f](https://gitlab.com/hestia-earth/hestia-glossary/commit/af7de8fc10871b74c2d93552b708fc77df156148))
* **measurement:** add solar radiation ([6448bb7](https://gitlab.com/hestia-earth/hestia-glossary/commit/6448bb789c93023687c3ddb2428b7edaa2a03049))
* **measurement:** improve definition of soil water potential ([2235923](https://gitlab.com/hestia-earth/hestia-glossary/commit/2235923a9b459af115de56ccc2180bdd47756837))
* **measurement:** redefine time periods for daily average measurements ([7662e1b](https://gitlab.com/hestia-earth/hestia-glossary/commit/7662e1be64a061c944dcf0a7071b3a018d6e50ec))
* **measurements:** add soil total C and N ([b4d0006](https://gitlab.com/hestia-earth/hestia-glossary/commit/b4d00067e71072f52f76940139421849b75773a8))
* **method:** add closed chamber methods, and other soil methods ([fb823c8](https://gitlab.com/hestia-earth/hestia-glossary/commit/fb823c8e55547fd525c50ab72125cfe52643d16b))
* **method:** add different methods for measuring soil pH ([046b5f0](https://gitlab.com/hestia-earth/hestia-glossary/commit/046b5f032186275d927e5040d1c7ada2b875d013))
* **method:** add KCl method for soil pH ([73e1bfa](https://gitlab.com/hestia-earth/hestia-glossary/commit/73e1bfac5ce97e01f0f2c81a555113f844a811b1))
* **method:** add measures for soil mineral analysis ([c90478e](https://gitlab.com/hestia-earth/hestia-glossary/commit/c90478e0fdee8941bc71dbb7502551ca1af7210e))
* **method:** add terms for lysimeters ([c92015f](https://gitlab.com/hestia-earth/hestia-glossary/commit/c92015f96d92709ba00cf3d3c22043b4d0f4c54a))
* **method:** create glossary for methods for taking measurements ([8731c02](https://gitlab.com/hestia-earth/hestia-glossary/commit/8731c028ac6525ef69c9be28a921bfa98871f734))
* **method:** split `methods` into `emissionsResourceUse` and `measurements` ([feecfdb](https://gitlab.com/hestia-earth/hestia-glossary/commit/feecfdbf4485b55836f675f2795b25689065af05)), closes [#126](https://gitlab.com/hestia-earth/hestia-glossary/issues/126)
* **model:** add CO2 from soil cabon stock change models ([974fbbd](https://gitlab.com/hestia-earth/hestia-glossary/commit/974fbbd068d2f4aa82679a0a6e1deec74e32b298))
* **model:** add ecoinvent background dataset as models ([3dbf35a](https://gitlab.com/hestia-earth/hestia-glossary/commit/3dbf35a864151177680064f8c368a540852a5ae9)), closes [#111](https://gitlab.com/hestia-earth/hestia-glossary/issues/111)
* **model:** add IPCC CH4 enteric models ([a89543c](https://gitlab.com/hestia-earth/hestia-glossary/commit/a89543cceea5319fed4127042e5c7ed23ea955a9))
* **model:** add phosphorus loss models ([931cca9](https://gitlab.com/hestia-earth/hestia-glossary/commit/931cca97b3ee326cb4e8a6e5a8a8e386c15ee311))
* **model:** add remaining combustion models ([056e550](https://gitlab.com/hestia-earth/hestia-glossary/commit/056e550b0d4578a0bc9c9a33903482a20babc14b))
* **model:** add Tier 1 version of Stehfest & Bouwman ([5161cf5](https://gitlab.com/hestia-earth/hestia-glossary/commit/5161cf5c4fbb1796e7d20218f87f403e563fb033))
* **organicFertilizer:** add Jeevamirtham as fertilizer ([7a49f18](https://gitlab.com/hestia-earth/hestia-glossary/commit/7a49f18d50ee3825e0d9b3d6d5785db0cd0f8205))
* **organicFertilizer:** add poultry litter and manure synonyms ([50587b8](https://gitlab.com/hestia-earth/hestia-glossary/commit/50587b8d4fcf8e540a759faac792286fa8a82a9d))
* **organicFertilizer:** add wastewater ([e80be86](https://gitlab.com/hestia-earth/hestia-glossary/commit/e80be86cf4129fc455e7c82dd78afc4b6aa1d2eb))
* **other:** add Minerals, unspecified ([d19d2a5](https://gitlab.com/hestia-earth/hestia-glossary/commit/d19d2a5cccc97f632fe422641579b2b45255ddea))
* **pesticideAI:** add "generic" synonyms ([5d0bc9f](https://gitlab.com/hestia-earth/hestia-glossary/commit/5d0bc9fa452cbabc6de3cfe57df4a41f230c743a))
* **pesticideAI:** change from grams to kg ([afcfa27](https://gitlab.com/hestia-earth/hestia-glossary/commit/afcfa278000cc922ac78831c2d624082895f8eae))
* **processedFood:** add FoodEx Level 7 data ([105b07e](https://gitlab.com/hestia-earth/hestia-glossary/commit/105b07e3e8bf76edd4b021a0d53bc54a4c492f2e)), closes [#87](https://gitlab.com/hestia-earth/hestia-glossary/issues/87)
* **processedFood:** add raw GENuS identifiers ([4216805](https://gitlab.com/hestia-earth/hestia-glossary/commit/4216805942dba82448b3e2dc1856c24be480ea6e)), closes [#87](https://gitlab.com/hestia-earth/hestia-glossary/issues/87)
* **production practices:** update content ([bd99168](https://gitlab.com/hestia-earth/hestia-glossary/commit/bd9916863b9aa41b81cac7ca08b7d682e1ef3c5b))
* **products:** update animalProduct ([d5be107](https://gitlab.com/hestia-earth/hestia-glossary/commit/d5be107237e7f7a104840aca9979debf55d2abbd))
* **properties:** add GTIN identifier ([af29bdd](https://gitlab.com/hestia-earth/hestia-glossary/commit/af29bddb4aa028aee64c13973786b6b52f45fff9))
* **property:** add AGROVOC link ([c39154e](https://gitlab.com/hestia-earth/hestia-glossary/commit/c39154e6a746f7ff6d935be5da403f8d277a1e1c)), closes [#72](https://gitlab.com/hestia-earth/hestia-glossary/issues/72)
* **property:** add controlled release fertilizer ([86fba72](https://gitlab.com/hestia-earth/hestia-glossary/commit/86fba72cd0baa022a95bd3570392d77f357a5906)), closes [#58](https://gitlab.com/hestia-earth/hestia-glossary/issues/58)
* **property:** add extra property for economic allocation during meat processing ([2d439db](https://gitlab.com/hestia-earth/hestia-glossary/commit/2d439dbde199e70a60832cd75e0822c51fca7abb))
* **property:** add minimum and maximum ([1cc0349](https://gitlab.com/hestia-earth/hestia-glossary/commit/1cc0349796f4ff2ef60226628804b621422114bc)), closes [#82](https://gitlab.com/hestia-earth/hestia-glossary/issues/82)
* **property:** add organic carbon content ([59688de](https://gitlab.com/hestia-earth/hestia-glossary/commit/59688de64911ee5a636b5bd9b7c67ba3cda63aa5))
* **property:** add properties to link to impact assessment method ([75b4964](https://gitlab.com/hestia-earth/hestia-glossary/commit/75b49641612a5dc1d1604caf8b598c2ca0253329)), closes [#81](https://gitlab.com/hestia-earth/hestia-glossary/issues/81)
* **property:** add TAN content of N; update percentages to allow >100 ([1e928f7](https://gitlab.com/hestia-earth/hestia-glossary/commit/1e928f791900cdb7e4678c136eeee84efc356ff8))
* **property:** add valueType field ([b1c95eb](https://gitlab.com/hestia-earth/hestia-glossary/commit/b1c95ebb86acb3023f09092f2118f8df2096f96b))
* **property:** break into three subgroups ([fe3dcf3](https://gitlab.com/hestia-earth/hestia-glossary/commit/fe3dcf34799c6fbb42155a655617b9baf2434689)), closes [#59](https://gitlab.com/hestia-earth/hestia-glossary/issues/59)
* **region:** add emissions factors by country ([c135509](https://gitlab.com/hestia-earth/hestia-glossary/commit/c13550952814112d76cfe927cf9f027bb0efb71e))
* **region:** add extra synonym converted from name without special chars ([f3a731b](https://gitlab.com/hestia-earth/hestia-glossary/commit/f3a731bc86f88e7ff0fbae9ab944c5c98151d7e5)), closes [#75](https://gitlab.com/hestia-earth/hestia-glossary/issues/75)
* **region:** add ISO 2 letter codes ([547bced](https://gitlab.com/hestia-earth/hestia-glossary/commit/547bced51ad9bc6d3eda56d063b886e648e1dd64))
* **region:** add iso3166-2 codes and areaKm2 ([cc47f7e](https://gitlab.com/hestia-earth/hestia-glossary/commit/cc47f7eb1890e4f0bb1356b327284ca02bec5820))
* **region:** add regions groupings based on FAOSTAT ([c6e3748](https://gitlab.com/hestia-earth/hestia-glossary/commit/c6e3748115af2a5d084a3e515eaddc80a81ce189)), closes [#84](https://gitlab.com/hestia-earth/hestia-glossary/issues/84)
* **region:** move lookups to region-lookup file ([11c6c6d](https://gitlab.com/hestia-earth/hestia-glossary/commit/11c6c6d64b74a572da87ddc47de3779d2c2f53db))
* **region:** remove unique children ([fb52392](https://gitlab.com/hestia-earth/hestia-glossary/commit/fb523920de224ad83e723dcfec3193f538f9580e))
* **region-lookup:** add country based biodiversity CFs from Chaudhary et al 2015 ([b1fc823](https://gitlab.com/hestia-earth/hestia-glossary/commit/b1fc82318c6303c0f67de068929d0d6dab87b5cb)), closes [#119](https://gitlab.com/hestia-earth/hestia-glossary/issues/119)
* **region-lookup:** add country based biodiversity CFs from Chaudhary et al 2015 ([992f0ce](https://gitlab.com/hestia-earth/hestia-glossary/commit/992f0ce9db47ddc78a991ed38ece037639d804f7)), closes [#119](https://gitlab.com/hestia-earth/hestia-glossary/issues/119)
* **region-lookup:** add practiceFactor data ([e6902ab](https://gitlab.com/hestia-earth/hestia-glossary/commit/e6902ab1556d6b50bd800b2bcfe95a403afa3cdf))
* **soilAmendement:** add Azospirillum, Phosphobacteria, and Pseudomonas fluorescens ([453ce45](https://gitlab.com/hestia-earth/hestia-glossary/commit/453ce45048648c60d81872103cf65ecc014ddb5f))
* **soilAmendement:** add potassium humate and fulvic acid ([1b841e0](https://gitlab.com/hestia-earth/hestia-glossary/commit/1b841e01aaa42d2278c1b83164060289524fcfd2)), closes [#92](https://gitlab.com/hestia-earth/hestia-glossary/issues/92)
* **soilAmendement:** improve definitions of units ([b8a6f0e](https://gitlab.com/hestia-earth/hestia-glossary/commit/b8a6f0e4546362316a7fbb73cc21b70b1973ce64))
* **soilTexture:** add lookup table for sand, silt and clay content ([9d0fb06](https://gitlab.com/hestia-earth/hestia-glossary/commit/9d0fb06d686a319f441077341f239c0079d64bad))
* **soilType:** add Typic Hapludands ([71ef475](https://gitlab.com/hestia-earth/hestia-glossary/commit/71ef475522cc7f778b710ed2283a829fa6d83afb))
* **standardsLabels:** add sustinable rice standard ([17b9f19](https://gitlab.com/hestia-earth/hestia-glossary/commit/17b9f19a22322b4e48d1d58313880e7aa71049ab))
* **system:** add new production practice termType ([8146452](https://gitlab.com/hestia-earth/hestia-glossary/commit/81464527fc957fd42a8a6306fa76041f80bb6313))
* **transport:** add transport unspecified ([067c4f8](https://gitlab.com/hestia-earth/hestia-glossary/commit/067c4f83b86682c8ac80d578c1c1b67e629f35ec))
* **waterRegime:** add alternative wetting and drying ([2556e53](https://gitlab.com/hestia-earth/hestia-glossary/commit/2556e5387159e81271773ca0eafe15a4e0becc6e))
* **waterRegime:** add Number of days flooded ([5b69cec](https://gitlab.com/hestia-earth/hestia-glossary/commit/5b69cec207cf5cc1ce094dd578e346ff648648f5))
* **waterRegime:** add units ([df4e24d](https://gitlab.com/hestia-earth/hestia-glossary/commit/df4e24db1315f8f6c55607be5b70363a615bb106))
* merge crop and cropProduct, and add cropGrouping ([914d9fa](https://gitlab.com/hestia-earth/hestia-glossary/commit/914d9fa75dfaa8b33cacdc6707ac94789fdd9abd))


### Bug Fixes

* add lookups in JSON with concatenation formula ([7909012](https://gitlab.com/hestia-earth/hestia-glossary/commit/79090120fd00f67909635753adb40eefb5dd3396))
* allow excel JSON generator to handle strings ([feecc93](https://gitlab.com/hestia-earth/hestia-glossary/commit/feecc932231542ca6448bdf656c895f4b6e2cb1e))
* change definitions for N and P erosion ([f95ae5d](https://gitlab.com/hestia-earth/hestia-glossary/commit/f95ae5d739ed6b5a7958a43ec5547d32642bd7bc))
* delete `includedInSystemBoundary` ([821cd03](https://gitlab.com/hestia-earth/hestia-glossary/commit/821cd03e0a800a3ed27b339b2b48a80f93405d2b)), closes [#106](https://gitlab.com/hestia-earth/hestia-glossary/issues/106)
* fix `defaultProperties` linked to non-existing terms ([28025f8](https://gitlab.com/hestia-earth/hestia-glossary/commit/28025f885deb31f2d16ed36ce165aa5306eb07f5)), closes [#102](https://gitlab.com/hestia-earth/hestia-glossary/issues/102)
* fix convert lookup errors ([3b9f693](https://gitlab.com/hestia-earth/hestia-glossary/commit/3b9f693de60fd2199e1a9e93b67fa5e0629e3746))
* move fcl code into lookup table ([b01d925](https://gitlab.com/hestia-earth/hestia-glossary/commit/b01d925f119655ccdcce2ee9bc5ec41ba3d342a5))
* move lime from soilAmendement to other ([9e9d27f](https://gitlab.com/hestia-earth/hestia-glossary/commit/9e9d27f3d338569b45e44ac6ed27bad901435f5b))
* remove `#` special character from id ([9e0bfb1](https://gitlab.com/hestia-earth/hestia-glossary/commit/9e0bfb1e6d25a77788f35c12755409006dbbdf42)), closes [#114](https://gitlab.com/hestia-earth/hestia-glossary/issues/114)
* remove duplicate "Lamb (county), Texas" ([9d2940a](https://gitlab.com/hestia-earth/hestia-glossary/commit/9d2940ac64275ff5e25b8c6c11cc4f84889016dc))
* **animalProduct:** set default weight to carcass weight ([8330277](https://gitlab.com/hestia-earth/hestia-glossary/commit/8330277081a481b86eda63d89974174f7ba84f07))
* **biodiversity:** move conservation evidence scores into lookup tables ([cb5cbce](https://gitlab.com/hestia-earth/hestia-glossary/commit/cb5cbceffab359c41bf7e3e36963d8acfc2a9eeb)), closes [#106](https://gitlab.com/hestia-earth/hestia-glossary/issues/106)
* **characterisation:** change characterisation to characterisedIndicator ([9a99ea4](https://gitlab.com/hestia-earth/hestia-glossary/commit/9a99ea4ef60e4aea771664152e5bad8eef603540))
* **characterisation:** correct errors and tidy formatting ([272f0b9](https://gitlab.com/hestia-earth/hestia-glossary/commit/272f0b91224d1c57f9fa70da421716b368a8a6d6))
* **characterisation:** update all names to new naming convention ([ef31f96](https://gitlab.com/hestia-earth/hestia-glossary/commit/ef31f96267fa6ddd59821aa7230a4f9c421a1336))
* **crop:** Add dryMatter values as a default properties, remove cropGrouping as now in a lookup ([1e4d21a](https://gitlab.com/hestia-earth/hestia-glossary/commit/1e4d21a72770c128c171effc4050a9d0106b2d37))
* **crop:** added cropGrouping to the glossary lookups format. ([a5ec6d7](https://gitlab.com/hestia-earth/hestia-glossary/commit/a5ec6d742820cda8e26ca8404a8f4f0f2fa9ae34))
* **crop:** Additional default dryMatter values for several crops ([8867fc6](https://gitlab.com/hestia-earth/hestia-glossary/commit/8867fc6104a6c2dbaa35ecf465e8f9ac55bad56d)), closes [#55](https://gitlab.com/hestia-earth/hestia-glossary/issues/55)
* **crop:** colon was missing in some rows ([1c0c4b1](https://gitlab.com/hestia-earth/hestia-glossary/commit/1c0c4b12e1ff116d0d1d378423f875639e62f50e))
* **crop:** fix cropGroupingFAO data ([9b3ed2c](https://gitlab.com/hestia-earth/hestia-glossary/commit/9b3ed2cd2c2d456e84d2f130c26949be6aec082f)), closes [#121](https://gitlab.com/hestia-earth/hestia-glossary/issues/121)
* **crop:** fix error in cropGroupings name ([e1ad760](https://gitlab.com/hestia-earth/hestia-glossary/commit/e1ad760a8b01cf5336282b3673262fa5d24cc009))
* **crop:** fix typo in almond economic allocation factor ([8520e11](https://gitlab.com/hestia-earth/hestia-glossary/commit/8520e110ce1f6493443ec9ff0b2302b349921923))
* **crop:** move cropGroupings into lookup table, move dry matter into default property ([c0c4ab1](https://gitlab.com/hestia-earth/hestia-glossary/commit/c0c4ab1856bd6b7fac45d961d80ca88829772182))
* **crop:** remove `hclTitle` ([00f3720](https://gitlab.com/hestia-earth/hestia-glossary/commit/00f3720395ffdcf65678439319130ed499b91286))
* **crop:** split cropGrouping lookup into two columns ([ee069c1](https://gitlab.com/hestia-earth/hestia-glossary/commit/ee069c175ed78ecdb081b5f803778162d0eb7669))
* **crop_residue:** Added the different above ground crop residue destinations as new terms ([67dfb27](https://gitlab.com/hestia-earth/hestia-glossary/commit/67dfb27212120d10a70b98f72f7c30fdadc7b30f))
* **crop_residue:** Added the different above ground crop residue destinations as new terms ([c2886b2](https://gitlab.com/hestia-earth/hestia-glossary/commit/c2886b27fc29f87c09a15331639c7b5798044df2))
* **crop_residue:** correct units for dry products ([eec8d0c](https://gitlab.com/hestia-earth/hestia-glossary/commit/eec8d0c62421a4d4c18ee33a6d6416990f0dd806))
* **crop_residue_practices:** removed destination description text ([29caef3](https://gitlab.com/hestia-earth/hestia-glossary/commit/29caef3071f653f6e87cd50c09895ac9b8275fa6))
* **crop_residue_practices:** removed destination description text ([3e70af9](https://gitlab.com/hestia-earth/hestia-glossary/commit/3e70af931dc37e699617d593b26da70e150ba724))
* **crop, fuel:** error in lookup tables ([bd07487](https://gitlab.com/hestia-earth/hestia-glossary/commit/bd07487f3fc193029bbc896fcc3276665f9ed68c))
* **destination:** remove termType ([ab4648d](https://gitlab.com/hestia-earth/hestia-glossary/commit/ab4648da28003cd764eeaf59f3d9b7607c0915b0))
* **destination:** remove termType ([ef9d072](https://gitlab.com/hestia-earth/hestia-glossary/commit/ef9d0726127987701541c24fde3019775e3879d4))
* **ecoregion:** delete realm and biome which can be derived from ecoRegionCode, and move g200Region, area, globalStatus and global200Status into lookup tables ([af92589](https://gitlab.com/hestia-earth/hestia-glossary/commit/af92589bac4d226bd49e93b2a3009a85d2c1cf48)), closes [#106](https://gitlab.com/hestia-earth/hestia-glossary/issues/106)
* **ecoregion:** remove spaces from Rock and Ice ecoregion `id` ([8d1a137](https://gitlab.com/hestia-earth/hestia-glossary/commit/8d1a137ff83c86240fb60a5305fbc7cca12ad178)), closes [#116](https://gitlab.com/hestia-earth/hestia-glossary/issues/116)
* **ecoregion-lookup:** replace NaNs with dash. ([a33a5e2](https://gitlab.com/hestia-earth/hestia-glossary/commit/a33a5e2b76aabe8dc07079dc0c9745271d0f134c))
* **ecoregion-lookup:** replace NaNs with dash. ([c1cb5eb](https://gitlab.com/hestia-earth/hestia-glossary/commit/c1cb5ebe99b212442c524d94fad4a9cc0fe03c7b))
* **electricity:** delete naics code ([ee01e24](https://gitlab.com/hestia-earth/hestia-glossary/commit/ee01e244c87a7dd766253ae52f7f3f74fdd2714b))
* **electricity:** fix typos ([783ab56](https://gitlab.com/hestia-earth/hestia-glossary/commit/783ab56585435d63fb5c2e3b6594d6b25fe3f2af))
* **emission:** add last column to the JSONLS cells ([8ae381a](https://gitlab.com/hestia-earth/hestia-glossary/commit/8ae381afff53056d6f68b87223f36b676174f4d2))
* **emission:** delete deforestation from synonym of soil carbon ([73a78a3](https://gitlab.com/hestia-earth/hestia-glossary/commit/73a78a3c8edb91da966fb4881674930e979dc8cf))
* **emission:** error in subClassOf id for NO emissions ([5cbc985](https://gitlab.com/hestia-earth/hestia-glossary/commit/5cbc985556a222c840950ca4c07b5f3e84ca14ad)), closes [#120](https://gitlab.com/hestia-earth/hestia-glossary/issues/120)
* **emission:** fix subClassOf ([90df17b](https://gitlab.com/hestia-earth/hestia-glossary/commit/90df17bc717a8b89b0902cdd39a3c11781a95769))
* **emission:** fix typo in CFC name ([d2ab0fc](https://gitlab.com/hestia-earth/hestia-glossary/commit/d2ab0fc45b4de162c95af04a2bead4c9c8ab4385))
* **fuel:** error in density id ([2c6a977](https://gitlab.com/hestia-earth/hestia-glossary/commit/2c6a977914635addbe31653635bd1a91fc998df0))
* **fuel:** error in lookup table ([c44fc53](https://gitlab.com/hestia-earth/hestia-glossary/commit/c44fc53d5699196664557465b2307332715e9cfc))
* **fuel:** move seicCode and cpcVersion2Code into lookup tables ([0aaeaa1](https://gitlab.com/hestia-earth/hestia-glossary/commit/0aaeaa11e6d1aafe3419b131431295d83b336193))
* **fuel:** remove plurals and delete shale gas term ([0cd0abc](https://gitlab.com/hestia-earth/hestia-glossary/commit/0cd0abc3145ed67397483ab3b29d89b9fe5de85c))
* **fuel:** update density following change in property name ([39a62a8](https://gitlab.com/hestia-earth/hestia-glossary/commit/39a62a8e742b880af80461de89d3f2a24f8310ab))
* **inorganicFertilizer:** add inorganic unspecified per kg ([d9d66f5](https://gitlab.com/hestia-earth/hestia-glossary/commit/d9d66f55dcd047c0bd2b891e283208ff7add6f68))
* **inorganicFertilizer:** fix fclCode ([3f261da](https://gitlab.com/hestia-earth/hestia-glossary/commit/3f261dae7d354f2cd7a6f9af3ea544c7ce2c7636)), closes [#70](https://gitlab.com/hestia-earth/hestia-glossary/issues/70)
* **inorganicFertilizer:** move N and P2O5 content into lookup tables ([ec25615](https://gitlab.com/hestia-earth/hestia-glossary/commit/ec2561581bd6acfa4fefbc7af5f2f9a6bae6e469)), closes [#106](https://gitlab.com/hestia-earth/hestia-glossary/issues/106)
* **landUseManagement:** error in iri ([725c1dc](https://gitlab.com/hestia-earth/hestia-glossary/commit/725c1dcd5eb8ac02d31d188fe686c29d846805b9)), closes [#65](https://gitlab.com/hestia-earth/hestia-glossary/issues/65)
* **liveAnimal:** typo in beehives ([fe1dda3](https://gitlab.com/hestia-earth/hestia-glossary/commit/fe1dda316b99e80057c8f94585036c378b6c0f0f)), closes [#66](https://gitlab.com/hestia-earth/hestia-glossary/issues/66)
* **liveAquaticSpecies:** move processing conversion from lookup to property ([99c9684](https://gitlab.com/hestia-earth/hestia-glossary/commit/99c9684e47794f37ccfb9dd376b098c723446e93)), closes [#122](https://gitlab.com/hestia-earth/hestia-glossary/issues/122)
* **liveAquaticSpecies:** move processing conversion to lookup table ([7122893](https://gitlab.com/hestia-earth/hestia-glossary/commit/71228938115a05895eb6e6e184739a77c227fb30)), closes [#106](https://gitlab.com/hestia-earth/hestia-glossary/issues/106)
* **measurement:** change units for altitude from "m" to "meters" ([fe1b80c](https://gitlab.com/hestia-earth/hestia-glossary/commit/fe1b80c0823dd5c41bf401843d1b099ed0549952))
* **measurement:** improve term names around water level depth ([e3eb20c](https://gitlab.com/hestia-earth/hestia-glossary/commit/e3eb20c291802010274dd8fce026ed21fb573ee0))
* **measurement:** simplify definitions of sand, silt, clay ([74af55c](https://gitlab.com/hestia-earth/hestia-glossary/commit/74af55c473f0fb4a1011b8f3e3a0d743e3fe9f93))
* **measurement:** unit definition from boolean to numeric for heavy winter precipitation ([f67df49](https://gitlab.com/hestia-earth/hestia-glossary/commit/f67df490b021484f85368de608b846cedc446ca1))
* **measurement:** update soil bulk density definiton, synoyms, and minimum ([89fcd0b](https://gitlab.com/hestia-earth/hestia-glossary/commit/89fcd0bb98461afda2052b5cc192a2e8a127047e))
* **model:** simplify IPCC 2006 model description to direct and indirect ([9fb5888](https://gitlab.com/hestia-earth/hestia-glossary/commit/9fb588865d9d43340da17fb5ae26a03ea5d658b4))
* **model:** swap description for definition; improve source definition ([d967168](https://gitlab.com/hestia-earth/hestia-glossary/commit/d967168866ab7bba5de89b83e3c8d19a166c55fd))
* **organicFertilizer:** fix hierarchies on cattle manure ([9e3fd62](https://gitlab.com/hestia-earth/hestia-glossary/commit/9e3fd6287d3eaf27c0d0ac8eba356bf50bc895f6))
* **organicFertilizer:** move sd to value of property; add dry matter ([00c8b0c](https://gitlab.com/hestia-earth/hestia-glossary/commit/00c8b0c7f5d7f12c2d8d7a5d3df31410a8521b72))
* **pesticideAI:** add extra synonyms to pesticide generic ([c782e99](https://gitlab.com/hestia-earth/hestia-glossary/commit/c782e9939d6b7b9fc2c3f776ea06ed2eee2eb7b2))
* **pesticideAI:** delete diesel fuel from pesticide glossary ([9f558c8](https://gitlab.com/hestia-earth/hestia-glossary/commit/9f558c8c96425fb9beb9f7f20d53e2fd75c4ba06))
* **pesticideAI:** delete limestone and urea terms ([b6d6f08](https://gitlab.com/hestia-earth/hestia-glossary/commit/b6d6f08459081e101874643a977071024c51c100))
* **pesticideAI:** remove broken links; rename pestides AI's based on PubChem or ChemIDPlus ([2a8f943](https://gitlab.com/hestia-earth/hestia-glossary/commit/2a8f943ad4deff82784f447697ed331e0e7faede)), closes [#36](https://gitlab.com/hestia-earth/hestia-glossary/issues/36) [#93](https://gitlab.com/hestia-earth/hestia-glossary/issues/93)
* **pesticideAI:** remove canola oil ([f011ed2](https://gitlab.com/hestia-earth/hestia-glossary/commit/f011ed29abc26cdc14f863ee540bb8da5ad9e706)), closes [#88](https://gitlab.com/hestia-earth/hestia-glossary/issues/88)
* **pesticideAI:** remove Fertilizers, Nitrogen-phosphorus-potassium ([13e25f3](https://gitlab.com/hestia-earth/hestia-glossary/commit/13e25f3dcdf746ec1f3e280314cf8dcc77f74bd8))
* **pesticideBrandName:** move productStatus into lookup table ([6d30c7e](https://gitlab.com/hestia-earth/hestia-glossary/commit/6d30c7efff95fbcead8911961cf1874e30587490)), closes [#106](https://gitlab.com/hestia-earth/hestia-glossary/issues/106)
* **pesticideBrandName:** remove dollar from `name` of term ([57a5dbe](https://gitlab.com/hestia-earth/hestia-glossary/commit/57a5dbe303d2a969b4c0b86830441ab95322df12)), closes [#117](https://gitlab.com/hestia-earth/hestia-glossary/issues/117)
* **pesticideBrandName:** remove duplicate terms ([af3884b](https://gitlab.com/hestia-earth/hestia-glossary/commit/af3884bb20ffcc0637bbc6ae5b4b60104b8a8408))
* **property:** add back processing conversions for animal products ([ce4d147](https://gitlab.com/hestia-earth/hestia-glossary/commit/ce4d1473caf0d4e593992efd55dd264e2fdc35f8)), closes [#122](https://gitlab.com/hestia-earth/hestia-glossary/issues/122)
* **property:** add c content as a synonym ([2ed1e62](https://gitlab.com/hestia-earth/hestia-glossary/commit/2ed1e62069c072d54d21d7943f65d30de28eb1c8))
* **property:** change "Nitrogen content" to N content for consistency; add TAN content ([c058316](https://gitlab.com/hestia-earth/hestia-glossary/commit/c058316853a89aff6570e65aa2410e73d586535d))
* **property:** change definition of area ([96dcee7](https://gitlab.com/hestia-earth/hestia-glossary/commit/96dcee722355d59ed1b413f61fa995daebc82318))
* **property:** change definition of cropGrouping ([eeb557e](https://gitlab.com/hestia-earth/hestia-glossary/commit/eeb557eaafde2ad39311c22e90a03043e99d26c5))
* **property:** change land use type to a generic crop grouping ([94e9c57](https://gitlab.com/hestia-earth/hestia-glossary/commit/94e9c57fc6dd4c04323e8533167be8a83dfc4a7e))
* **property:** change property names to lower case ([1daeb79](https://gitlab.com/hestia-earth/hestia-glossary/commit/1daeb79d3cbefe6ccefa79b8d312b9121ffa4572))
* **property:** change units from % to % (0-100) ([3291ee7](https://gitlab.com/hestia-earth/hestia-glossary/commit/3291ee759770a39e14ddf7f2e68f223692fe6d53))
* **property:** delete elpca term as their website is not working ([fd10523](https://gitlab.com/hestia-earth/hestia-glossary/commit/fd1052331d2051ab2fda0a2818aaad106256761c))
* **property:** fix naming of default processing conversions ([0661207](https://gitlab.com/hestia-earth/hestia-glossary/commit/06612074ad313f987e5d1b5ef6f5cc855a7bd41e))
* **property:** improve definition of conservation evidence term ([6651c6f](https://gitlab.com/hestia-earth/hestia-glossary/commit/6651c6fc65da8d8fb23215bcf99dc19d599852f1))
* **property:** move terms to lookup and remove unused terms ([d693e6c](https://gitlab.com/hestia-earth/hestia-glossary/commit/d693e6c8e9ebb4a6a44cab23c0f9fa65b5d252bd))
* **property:** move units out of area ([3a79875](https://gitlab.com/hestia-earth/hestia-glossary/commit/3a79875b282eeb706d3f52893677c66e4155bcac))
* **property:** remove redundant terms ([8dc691f](https://gitlab.com/hestia-earth/hestia-glossary/commit/8dc691fb451addef251340cc6ea85d8d3e6dc59f)), closes [#106](https://gitlab.com/hestia-earth/hestia-glossary/issues/106)
* **property:** remove term.valueType field which should not be present ([ab1bedd](https://gitlab.com/hestia-earth/hestia-glossary/commit/ab1bedd46cbcf59702b03b189c83248ed68665b3))
* **property:** rename density, fix units ([415613d](https://gitlab.com/hestia-earth/hestia-glossary/commit/415613dccd5cc00ff230c4b85fad1b997473bc65)), closes [#59](https://gitlab.com/hestia-earth/hestia-glossary/issues/59)
* **property:** rename liveweight and carcass weight processing conversions and add synonyms ([51f3768](https://gitlab.com/hestia-earth/hestia-glossary/commit/51f3768da8d556abb4c8529dd76bf17759907b35))
* **property:** rename max to maximum, min to minimum, to avoid GADM duplication ([f1a9a34](https://gitlab.com/hestia-earth/hestia-glossary/commit/f1a9a3461ba5767d1400b6ea5f6d8d6cf3dbfb01)), closes [#82](https://gitlab.com/hestia-earth/hestia-glossary/issues/82)
* **property:** validation issues ([1a4ad98](https://gitlab.com/hestia-earth/hestia-glossary/commit/1a4ad9887d9ec152d9b996a1f469adb2af06865d))
* **region:** fix multiple region same line ([39f1307](https://gitlab.com/hestia-earth/hestia-glossary/commit/39f130749d91f55a891aedfd024b604321a4088e))
* **region:** remove precision changes ([95c53d8](https://gitlab.com/hestia-earth/hestia-glossary/commit/95c53d8eab153771a38692bf318e28abc1bd8a8b))
* **region:** rename region called urea due to duplication ([edb1838](https://gitlab.com/hestia-earth/hestia-glossary/commit/edb18388d1fb437390193968b28f832a91bcde32))
* **region-lookup:** convert to comma delimited rather than spaces ([d401892](https://gitlab.com/hestia-earth/hestia-glossary/commit/d401892ee27a49d005c3995514a51574826bd56f))
* **soilType:** delete acronym property ([01d0a4e](https://gitlab.com/hestia-earth/hestia-glossary/commit/01d0a4e87a348d37abf21fdee5a9865fbd575a5f)), closes [#106](https://gitlab.com/hestia-earth/hestia-glossary/issues/106)
* **standards_labels:** remove special characters from names; fix nordic swan text ([57ddd11](https://gitlab.com/hestia-earth/hestia-glossary/commit/57ddd1103076cd3947783946855ccef0feaba27c))
* **standardsLabels:** fix spaces in id field ([2031edf](https://gitlab.com/hestia-earth/hestia-glossary/commit/2031edf2546c0d206dbd14caf31e495f82fa5a2d))
* **usdaSoilType:** delete second sheet ([b494a1d](https://gitlab.com/hestia-earth/hestia-glossary/commit/b494a1d9c7f40fd44900477f55ad15787b394f80))
* **usdaSoilType:** move usdaSoilTextureFormingName into lookup table ([cd58112](https://gitlab.com/hestia-earth/hestia-glossary/commit/cd581121b39dbebf5384baedf155bcc62f120cd9))

### 0.0.1 (2020-09-09)


### Features

* add altitude, add synonyms for rainfall ([e9514ca](https://gitlab.com/hestia-earth/hestia-glossary/commit/e9514caa09e2ba5dccbf3c61c3dce652def128ed))
* add municipal water and crop support ([19c01b9](https://gitlab.com/hestia-earth/hestia-glossary/commit/19c01b992601716a9d8b5f1322fccdd21b239ac3))
* add new terms enabling practices to be represeted like inputs ([44736b5](https://gitlab.com/hestia-earth/hestia-glossary/commit/44736b577933749e30d617e8b72b1b7fd5a6d4ed))
* add PO43- emissions and related models ([886cf6a](https://gitlab.com/hestia-earth/hestia-glossary/commit/886cf6a7a8a0bfce4d99c4206a94f970a7d5a356))
* add sulfate as synonym for suphate ([5cd6a80](https://gitlab.com/hestia-earth/hestia-glossary/commit/5cd6a8002333c5fd8fd346ea7ed21d35ba43d32e))
* differentiate crop and cropProduct ([80eaf9a](https://gitlab.com/hestia-earth/hestia-glossary/commit/80eaf9a63726203701bf0e4aa77cf545b97496ab)), closes [#33](https://gitlab.com/hestia-earth/hestia-glossary/issues/33)
* improve LUC definitions ([08cffb3](https://gitlab.com/hestia-earth/hestia-glossary/commit/08cffb3280b8acc881a80d5ee093367ee363e421)), closes [#40](https://gitlab.com/hestia-earth/hestia-glossary/issues/40)
* update file format and content ([59ac32d](https://gitlab.com/hestia-earth/hestia-glossary/commit/59ac32d7310246548d389df488ddc7de6cd43388))
* **animalProduct:** add excreta ([f324cff](https://gitlab.com/hestia-earth/hestia-glossary/commit/f324cff025ffef3789a0776dbd1b291a79d6df0c)), closes [#24](https://gitlab.com/hestia-earth/hestia-glossary/issues/24)
* **check duplicate:** raise error missing column ([9eb2445](https://gitlab.com/hestia-earth/hestia-glossary/commit/9eb24453bf200d5a1fe7283952db4e142511ae92))
* **crop:** add new terms and edit old ones ([261c9d5](https://gitlab.com/hestia-earth/hestia-glossary/commit/261c9d5be7a26f275a3e80cfc9123807b1db7a17))
* **cropEstablishment:** add new terms ([7f5818e](https://gitlab.com/hestia-earth/hestia-glossary/commit/7f5818eac23ca84c4a2dca57cb390561769ca2fb))
* **cropEstablishment:** add seed drilling and depth ([50f8a31](https://gitlab.com/hestia-earth/hestia-glossary/commit/50f8a31a9661366713dbea6bff70e90442e9d95a))
* **cropProduct:** add cocoa definition ([44ee5ef](https://gitlab.com/hestia-earth/hestia-glossary/commit/44ee5eff1de92f8f66727552c28c491bc7a920cd))
* **cropProduct:** update crop groupings, remove duplicate entries ([31625d6](https://gitlab.com/hestia-earth/hestia-glossary/commit/31625d60899b3b80e585f48b1a7fcecf96c75714)), closes [#23](https://gitlab.com/hestia-earth/hestia-glossary/issues/23)
* **cropResidueManagement:** add further terms ([50f3627](https://gitlab.com/hestia-earth/hestia-glossary/commit/50f36277e2d3da032794c28bca533babec0a833c))
* **csv:** convert excel files to simple csv files ([fab4e2f](https://gitlab.com/hestia-earth/hestia-glossary/commit/fab4e2f8e0afdd47115b938f2f93ba08c0b702e1))
* **electricity:** add definitions ([720d1a8](https://gitlab.com/hestia-earth/hestia-glossary/commit/720d1a8227991653a10b44f1d1a054bfdecb3199))
* **emissions:** update content ([c8aa9dd](https://gitlab.com/hestia-earth/hestia-glossary/commit/c8aa9dd32eddd3e650782e6f69fffc25b70d85ca))
* **Emissions:** differentiate background emissions using new terms ([d79e627](https://gitlab.com/hestia-earth/hestia-glossary/commit/d79e6274d013f1f386b4d9021f732bb60f61b0ac))
* **fuel:** add gasoil as synonym ([401be0c](https://gitlab.com/hestia-earth/hestia-glossary/commit/401be0c80cf60f86989342edb8fb15ac22ece0b4))
* **gadm:** use iso31662ToName to get a name from iso3166-2 code ([86555e0](https://gitlab.com/hestia-earth/hestia-glossary/commit/86555e0c4277643474713b3755ff105795b30a2e))
* **geographies:** add new termTypes ([eb565d1](https://gitlab.com/hestia-earth/hestia-glossary/commit/eb565d134cf36658408e40af37c19a091bf55cd6))
* **index:** add sample page ([e4d2167](https://gitlab.com/hestia-earth/hestia-glossary/commit/e4d216735486f6f44824191413d87ac69cc3e157))
* **infrastructure:** remove default units, add terms ([310df3c](https://gitlab.com/hestia-earth/hestia-glossary/commit/310df3c451bd083acc81fea88c144bec1bc8a11c))
* **inorganicFertilizer:** add ammonium bicarbonate ([34a6b9e](https://gitlab.com/hestia-earth/hestia-glossary/commit/34a6b9ef4ce6712a23292842912dd6e3e3e60718))
* **inputs:** update content ([d2bdf12](https://gitlab.com/hestia-earth/hestia-glossary/commit/d2bdf1206811546d991244f5cf9bf845692bca33))
* **landUseManagement:** add different tillage types ([91e439f](https://gitlab.com/hestia-earth/hestia-glossary/commit/91e439f20d9cadf4fe9bb92c6f077ad8e6d09acd))
* **landUseManagement:** add permanent raised beds ([44b5742](https://gitlab.com/hestia-earth/hestia-glossary/commit/44b574248fb2315d5147bfbe2aabef75ce0e6de8))
* **material:** add Infrastructure terms ([f169dd9](https://gitlab.com/hestia-earth/hestia-glossary/commit/f169dd9c9a95601fba2b5e7b4bb6bcab98142429))
* **material:** add plastics ([fde88f5](https://gitlab.com/hestia-earth/hestia-glossary/commit/fde88f5b5dfbae632e07d0b576fb5d8e2ca3c0ba))
* **measurement:** add biomass stock measurements ([f59020f](https://gitlab.com/hestia-earth/hestia-glossary/commit/f59020fdc6118aead4578a15c43b7bffb26e0cbd)), closes [#49](https://gitlab.com/hestia-earth/hestia-glossary/issues/49)
* **measurement:** add further precipitation terms ([a0eb92f](https://gitlab.com/hestia-earth/hestia-glossary/commit/a0eb92f923087b6e931298c0958b1a5811b12070))
* **measurement:** add humidity ([fac7aec](https://gitlab.com/hestia-earth/hestia-glossary/commit/fac7aec6b101ff1668b417c00c543ccf7807c7a4))
* **measurement:** add terms, improve naming ([da972a4](https://gitlab.com/hestia-earth/hestia-glossary/commit/da972a476e80355703e43b9fc98a60e6ac311901))
* **measurement:** differentiate mineral and total N and P ([dc17252](https://gitlab.com/hestia-earth/hestia-glossary/commit/dc172526b1fd04df6e4b7140c5f346225818884a))
* **measurements:** add further terms ([1d4a195](https://gitlab.com/hestia-earth/hestia-glossary/commit/1d4a195d9ac478a34abfba103cb6b9cd5fc89dde))
* **model:** add fuel combustion model ([ace68eb](https://gitlab.com/hestia-earth/hestia-glossary/commit/ace68eb0158fdf535574259627846062bcf0ba12))
* **model:** add further emissions models ([6fbf872](https://gitlab.com/hestia-earth/hestia-glossary/commit/6fbf8729064f50a3b762d93fb9cb2e8fd042b811))
* **Model:** add further model options ([e208b52](https://gitlab.com/hestia-earth/hestia-glossary/commit/e208b52604081949b6c9825df6a8528af39b9584))
* **organicFertilizer:** add further terms, improve definitions ([160d62f](https://gitlab.com/hestia-earth/hestia-glossary/commit/160d62f2c3acadcca342ab6c436dca43235e21cd))
* **organicFertilizer:** add nitrogen fixation by current crop ([8bc3298](https://gitlab.com/hestia-earth/hestia-glossary/commit/8bc32988c1f7b6f34344aacdcd881f9135353ed8))
* **organicFertilizer:** add type unknown ([087f767](https://gitlab.com/hestia-earth/hestia-glossary/commit/087f7675e5fc02443e2876c3307e3c35618171c2))
* **organicFertilizer:** differentiate fresh and dry ([3b844f6](https://gitlab.com/hestia-earth/hestia-glossary/commit/3b844f6a87847fbd7ce46328d53e256699e460c3))
* **pesticideAI:** add formicide ([de8336e](https://gitlab.com/hestia-earth/hestia-glossary/commit/de8336e899e9bbc3f768364fb6b342f5dfc5b919))
* **production practices:** update content ([adf2f5b](https://gitlab.com/hestia-earth/hestia-glossary/commit/adf2f5b2cc4f7747b39d11b8905fcc832281bfcc))
* **products:** update content and add new termType ([d56aa06](https://gitlab.com/hestia-earth/hestia-glossary/commit/d56aa06d6cddae853f89a7bff77c5c3d252b37bc))
* **properties:** add soil texture property ([d52fc0c](https://gitlab.com/hestia-earth/hestia-glossary/commit/d52fc0c319aee5fef43c74477dd72e16f42aa6ad)), closes [#32](https://gitlab.com/hestia-earth/hestia-glossary/issues/32)
* **properties:** update content ([e687df3](https://gitlab.com/hestia-earth/hestia-glossary/commit/e687df3a09afd33c65711f914b6a51de3c1a9b3e))
* **property:** add crop variety ([0ac3255](https://gitlab.com/hestia-earth/hestia-glossary/commit/0ac325523ed1a13a9b2521c1f169ef2b5c9d41e5))
* **property:** add GADM Name term ([d787eb6](https://gitlab.com/hestia-earth/hestia-glossary/commit/d787eb6b70922ebf2212a5176feb5c2adc8c2232))
* **property:** add ISO 3166-2 Code ([1bae472](https://gitlab.com/hestia-earth/hestia-glossary/commit/1bae472340c4833841948d5acf2a9956b17cb1ca)), closes [#48](https://gitlab.com/hestia-earth/hestia-glossary/issues/48)
* **property:** add variety description term ([eb464d5](https://gitlab.com/hestia-earth/hestia-glossary/commit/eb464d596176f94e18e53746c720821c8c5e8e0b))
* **property:** add website ([13d90c9](https://gitlab.com/hestia-earth/hestia-glossary/commit/13d90c98759c863ddb3432b91dcf8b3ae294ca42)), closes [#46](https://gitlab.com/hestia-earth/hestia-glossary/issues/46) [#50](https://gitlab.com/hestia-earth/hestia-glossary/issues/50)
* **property:** new property to represent practices like inputs ([009d1ec](https://gitlab.com/hestia-earth/hestia-glossary/commit/009d1ecc4c666bb37fb3fecc1e2280fe8a2a0e69))
* **region:** add readable names, deprecate subRegion etc. ([d033941](https://gitlab.com/hestia-earth/hestia-glossary/commit/d03394109bf27e165e0c0e4c2eb71fc871afc842))
* **resourceUse:** add water terms ([0afef25](https://gitlab.com/hestia-earth/hestia-glossary/commit/0afef25a884bec01b6e2f798a73783995887a65d)), closes [#41](https://gitlab.com/hestia-earth/hestia-glossary/issues/41)
* **soilAmendement:** add lime synonyms ([0e5575a](https://gitlab.com/hestia-earth/hestia-glossary/commit/0e5575ab314150c3300afd57c533d180979aacee))
* **soilAmendement:** add zinc term ([3338123](https://gitlab.com/hestia-earth/hestia-glossary/commit/333812322decfe8d9d7689682d4512e72a06b0dd))
* **soilTexture:** create Glossary ([e43a51f](https://gitlab.com/hestia-earth/hestia-glossary/commit/e43a51fa16a5dab44cf18f08297d74fe33bc32ca))
* **stadardsLabels:** add glossary ([8e29bfd](https://gitlab.com/hestia-earth/hestia-glossary/commit/8e29bfde93b4e9e95d9f73ee0d75d877c35c8d36)), closes [#46](https://gitlab.com/hestia-earth/hestia-glossary/issues/46) [#50](https://gitlab.com/hestia-earth/hestia-glossary/issues/50)
* **usdaSoilType:** create glossary ([85b6296](https://gitlab.com/hestia-earth/hestia-glossary/commit/85b6296e844e26164ca62c155d56af62829b3686)), closes [#32](https://gitlab.com/hestia-earth/hestia-glossary/issues/32)
* **waterRegime:** add number of irrigations term ([61907db](https://gitlab.com/hestia-earth/hestia-glossary/commit/61907db6c99ff837df3d89d52c78c97d4684ec07))
* **waterRegime:** add synonyms ([dc54dd9](https://gitlab.com/hestia-earth/hestia-glossary/commit/dc54dd93197b387508ce9c924020b01b1deda5fa))


### Bug Fixes

* change floodingRegime to waterRegime, add more terms ([90bd011](https://gitlab.com/hestia-earth/hestia-glossary/commit/90bd01102bb9e8fbc5ebc8f452f84845d7f97336))
* error in subClassOf ([ed27126](https://gitlab.com/hestia-earth/hestia-glossary/commit/ed27126ffe790b9a1fd546de816121c8e1d4a3cd))
* fix duplicate terms ([ece048d](https://gitlab.com/hestia-earth/hestia-glossary/commit/ece048d1cfc67baaf9b6a406502ac9deb33a1d2d))
* futher subClassOf issues ([a223546](https://gitlab.com/hestia-earth/hestia-glossary/commit/a223546cb72378db2e7e3cc340ebb3b734021ed9))
* improving organic fertilizer glossary ([fe5ea63](https://gitlab.com/hestia-earth/hestia-glossary/commit/fe5ea6349d508ab1c54c2e5c2dfd09ce386f471a))
* incorrectly named column headers ([5297616](https://gitlab.com/hestia-earth/hestia-glossary/commit/529761610c7b4fc22753fbb81367f854eef900fa))
* refer to terms only with id ([c232ee2](https://gitlab.com/hestia-earth/hestia-glossary/commit/c232ee2fcd7a2231c52777c885fa7762d14d72f3))
* remove duplicates ([c70622b](https://gitlab.com/hestia-earth/hestia-glossary/commit/c70622b3b33cb93e5d8cc07f2e403586e3b37ac1))
* remove productGroup and macroGroup ([78ec822](https://gitlab.com/hestia-earth/hestia-glossary/commit/78ec822a66cd8ca393a0d0d609631fbe8ca1b717))
* remove termType in id ([6c614b8](https://gitlab.com/hestia-earth/hestia-glossary/commit/6c614b8c45ef795bc3cf1e034bb5c1bb4d9439c4)), closes [#38](https://gitlab.com/hestia-earth/hestia-glossary/issues/38)
* update sources ([eb60bba](https://gitlab.com/hestia-earth/hestia-glossary/commit/eb60bba613b98385b9556bbed0ca3a9578f0a2f9))
* **characterisation:** fix subClassOf errors ([a7a0a85](https://gitlab.com/hestia-earth/hestia-glossary/commit/a7a0a850e69084ce6737d747219a480bc0f99216))
* **characterisation:** fix typo in source ([fe3ed80](https://gitlab.com/hestia-earth/hestia-glossary/commit/fe3ed80c9f2b315978235d1a6d83b95f91387377))
* **characterisation:** remove subClassOf issues ([8abcd9c](https://gitlab.com/hestia-earth/hestia-glossary/commit/8abcd9c5aeabb2e37558c2814c8905bfaab63b3d))
* **crop:** remove duplicate with cropProduct ([24405cd](https://gitlab.com/hestia-earth/hestia-glossary/commit/24405cd08286ca706bea06afa790fb44fbd43a51))
* **crop:** update definition ([6a8f604](https://gitlab.com/hestia-earth/hestia-glossary/commit/6a8f604c1fd924cac9694c0cab26fb354f293e75))
* **cropProduct:** fix errors in terms ([190da1e](https://gitlab.com/hestia-earth/hestia-glossary/commit/190da1e0615b3f34d6bb20c479a343781c080dd5))
* **destination:** update term names ([7d20bdd](https://gitlab.com/hestia-earth/hestia-glossary/commit/7d20bdd6a59b7e4fb93fcf304b8bd1604477ad5d))
* **electricity:** error in id column ([36be587](https://gitlab.com/hestia-earth/hestia-glossary/commit/36be5877c59f15335278132fa1d13c1d7fd3988a)), closes [#53](https://gitlab.com/hestia-earth/hestia-glossary/issues/53)
* **emission:** error in synonyms column header ([d2d4334](https://gitlab.com/hestia-earth/hestia-glossary/commit/d2d4334af921b7de4792d77579ca816b343464bc)), closes [#45](https://gitlab.com/hestia-earth/hestia-glossary/issues/45)
* **Emission:** minor fixes ([c71b705](https://gitlab.com/hestia-earth/hestia-glossary/commit/c71b705bbd1f6e016f3a79f745daa41d1bff2ab9))
* **Emission:** wrong units ([73dba48](https://gitlab.com/hestia-earth/hestia-glossary/commit/73dba48a6ee4318e8c86a1e61e27409caf918f38))
* **index:** fix about link ([e91fbd0](https://gitlab.com/hestia-earth/hestia-glossary/commit/e91fbd0b433e217826ae1718260642b36f329a41))
* **irrigation:** improve definitions ([305c041](https://gitlab.com/hestia-earth/hestia-glossary/commit/305c0410ce485f9dde28df41308683fe7dc2e935))
* **landUseManagement:** fix typo in definition ([3908b22](https://gitlab.com/hestia-earth/hestia-glossary/commit/3908b22213a88597fe503cff3310866fe71ce38e))
* **measurement:** remove duplicate terms ([51a780c](https://gitlab.com/hestia-earth/hestia-glossary/commit/51a780c9ab694ad15e6f2a2e24a4b9a276e9ad74))
* **model:** fix typo ([13d495d](https://gitlab.com/hestia-earth/hestia-glossary/commit/13d495d0f03e02f591d533cb053be1ded2e8ec59))
* **organicFertilizer:** typos ([99198b0](https://gitlab.com/hestia-earth/hestia-glossary/commit/99198b0901a30f999396d7f6df2a753a061cca13))
* **pesticideBrandName:** add pesticideUnspecifiedAi where AI CAS missing ([d7175b3](https://gitlab.com/hestia-earth/hestia-glossary/commit/d7175b3df79decf8be6e8e84de456e10acf260f9)), closes [#16](https://gitlab.com/hestia-earth/hestia-glossary/issues/16)
* **pesticideBrandName:** column header incorrectly named ([dba0939](https://gitlab.com/hestia-earth/hestia-glossary/commit/dba09393264095b71ecdc163b8547b200e62644f))
* **property:** correct typos ([8479b1a](https://gitlab.com/hestia-earth/hestia-glossary/commit/8479b1a721a575bbe8e90bbf4249456ff01ee29e))
* **property:** remove termType from id ([3555193](https://gitlab.com/hestia-earth/hestia-glossary/commit/35551937bdbd4a15d405897f990997d8e0fb6b3b)), closes [#38](https://gitlab.com/hestia-earth/hestia-glossary/issues/38)
* **region:** also remove "Bananas" district ([06da8e3](https://gitlab.com/hestia-earth/hestia-glossary/commit/06da8e3d9ec666fee975ad2ffc319cf3c04ef042))
* **region:** remove duplicate term ([dcefa8e](https://gitlab.com/hestia-earth/hestia-glossary/commit/dcefa8eb4e0d92f851e21b645103777dae956fe3))
* **region:** rename duplicate in measurement glossary ([a153f73](https://gitlab.com/hestia-earth/hestia-glossary/commit/a153f73a08511b4a22655e5c4a320d002d7ad278))
* **scripts:** fix write data dir and split synonyms ([b44178c](https://gitlab.com/hestia-earth/hestia-glossary/commit/b44178ceb384958b13ba12d2204b168af4970e59))
* **scripts:** re-enable convert to csv for download ([afc152a](https://gitlab.com/hestia-earth/hestia-glossary/commit/afc152a0b4ce97082f77d772a4f082917915525b))
* **soilTexture:** term duplication issues ([a413d16](https://gitlab.com/hestia-earth/hestia-glossary/commit/a413d164cce0528ddee23fe9fe77df03a66f7095))
* **usdaSoilType:** fixes very minor typo ([c0da935](https://gitlab.com/hestia-earth/hestia-glossary/commit/c0da935ea3748e79bcad5a75b40def87840d2eea))
