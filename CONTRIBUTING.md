# Contributing

To contribute as an external user, please [create an Issue](#submit-issue) for our team to address.

If you have been granted access to the Glossary on GitLab, please follow the below instructions to add new terms or change existing ones.

 - [Submit an Issue](#submit-issue)
 - [Create a New Term or Add Data to Existing Terms](#create-add-term)
 - [Create a New Property](#create-property)
 - [Schema Development](#schema-dev)
 - [Commit Message Guidelines](#commit)
 - [Naming Rules for the Crop Glossary](#naming-rules-crop)

## <a name="submit-issue"></a> Submitting an Issue

To get a new Term added, change a Term, or change data associated with a Term, please create an issue detailing your request. We will then review your request and implement any relevant changes for you.

Before you submit an issue, please [search for existing issues](https://gitlab.com/hestia-earth/hestia-glossary/-/issues), as your request may already exist.

If the issue does not exist, create a new Issue using a bug or feature template.

## <a name="create-add-term"></a> Create a New Term or Add Data to Existing Terms

1. Check that the Term or Property of the Term doesn't currently exist in the [Glossary](https://hestia.earth/glossary).

1. [Fork](https://gitlab.com/hestia-earth/hestia-glossary/-/forks/new) this repository.

1. Create a new git branch from the develop branch:

     ```shell
     git checkout -b my-new-term-branch develop
     ```

1. Open the Excel file representing the type of Term you want to edit (e.g. `emission.xlsx`). These are best opened with Microsoft Excel 2013 or later because there are formulas in the first few columns that rely on Excel 2013 functionality.

1. Open the [schema for Term](https://hestia.earth/schema/Term) and read it carefully. Terms can have additional Properties added to them, which have their own [schema](https://hestia.earth/schema/Property) and [Glossary](https://hestia.earth/glossary?termType=property).

1. The order of each Glossary, from left to right: identifiers, Term meta data, Properties, lookups, and then calculations related to lookups.

1. To add a new Term, go to the first empty row and add data starting in column C. You need to copy the formulas in columns A and B down into your row(s) - these formulas create the unique `id` for the Term. Don't leave an empty row, as the converter stops at the first empty row.

1. To add a Property (where a Properties are data items that describe a Term in more depth or differentiate a Term from another Term):
    * Go to the last column at the end of the Properties section and add new columns as required. If there are currently no Properties, go to the first empty column or add a column before the Lookups section.
    * Define which Property is being added using `term.defaultProperties.0.term.id` where 0 refers to the first Property in the file, and 1 to the next Property. `id`'s can be found in the [Glossary](https://hestia.earth/glossary?termType=property). Unlike with HESTIA file upload, you cannot refer to other terms using their `name` here, and must use their `id`'s.
    * If the Property doesn't exist, you will need to [create a new property](#create-property) in a separate merge request first.
    * Add data or meta data using additional columns. Specifically define a value for the Property using `term.defaultProperties.0.value`. If the value of the property is an IRI (e.g. a URL or a link to a page on the web), replace `value` with `iri` in the column header (e.g. `term.defaultProperties.0.iri`).
    * The values of terms can be the following JSON types `number`, `string`, or `boolean`. The converter will automatically detect types, but if there is any ambiguity (e.g. numbers that should be treated as strings), you can define the value type with a new column (e.g. `term.defaultProperties.0.term.valueType`).
    * Add any further detail to the Property using the fields defined in the schema.
    * Define the data state of the Property using `term.defaultProperties.0.dataState`.

1. To add a new Lookup to the existing Terms (where Lookups provide additional information about each Term for use in environmental models):
    * Go to the first empty column in the lookups section.
    * Define the name of the lookup using `lookups.0.name`. Names should match the lookup name in the model. Note: only the following chars are allowed for lookup names: lower/upper case letters, numbers, `-` and `_`.
    * Define the value of the lookup using `lookups.0.value`. Units need to match what is used by the model.
    * To add the standard deviation, maximum, or minimum, use `lookups.0.sd`, `lookups.0.min`, and `lookups.0.max`.
    * Ideally define the lookup source using `lookups.0.source` and add any notes if required using `lookups.0.notes`.
    * If the lookup simply uses a value from another row in the same sheet as a proxy, define which using `lookups.0.proxy.id`, where `id` is the `id` of the Term from column A.
    * Define the data state using `lookups.0.dataState` and colour code the cells accordingly. Options and colours for the data state are as follows:

    | `dataState`           | Colour      | Definition                                                                  | Allowed values for `lookups.0.name` | Allowed values for `lookups.0.value` |
    |-----------------------|-------------|-----------------------------------------------------------------------------|-------------------------------------|--------------------------------------|
    | `complete`            | Dark green  | The data are complete with no validation issues identified.                 | Must not be empty or `-`            | Must not be empty or `-`             |
    | `not required`        | Dark green  | The data are not required or relevant.                                      | Must not be empty or `-`            | Must be `-`                          |
    | `requires validation` | Light Green | The data require validation due to possible or known errors.                | Must not be empty or `-`            | Must not be empty (can be `-`)       |
    | `missing`             | Pink        | The data are missing and need to be added to run models based on this Term. | Must not be empty or `-`            | Must be empty (can not be `-`)       |
    | not set               | White       | A data state has not been assigned.                                         | Any value                           | Any value                            |

1. In general for Term meta data and Properties, use a small dash `-` (U+002D in UTF-8) to indicate no data (rather than leaving cells blank using an error related encoding such as #N/A). For lookups, cells can be left blank if the data is marked as `missing` or `requires validation`.

1. Save the file, and commit your changes using a descriptive commit message that explains the content of the changes. See [commit message guidelines](#commit) below.

1. Push your branch to GitLab:

    ```shell
    git push origin my-new-term-branch
    ```

1. In GitLab, create a merge request to `hestia-glossary:develop` and tag a member of our team to review it. If we suggest changes then:
    * Make the required updates.
    * Rebase your branch to develop and force push to your GitLab repository (this will update your Merge Request):


    ```shell
    git rebase develop -i
    git push -f
    ```


## <a name="create-property"></a> Create a New Property

Properties of Terms need to exist in HESTIA before they can be added to Terms. Therefore, if the Property doesn't exist, you should create it with an initial Merge Request before [creating a new Term or adding Properties to an existing Term](#create-add-term).

1. Follow steps (1-3) above.

1. Open the file property.xlsx in the /property subfolder. This file is best opened with Microsoft Excel 2013 or later because there are formulas in the first few columns that rely on Excel 2013 functionality.

1. Go to the first empty row and add data starting in column C. You need to copy the formulas in columns A and B down into your row(s) - these formulas create the unique `id` for the Term. Don't leave an empty row, as the converter stops at the first empty row.

1. Follow steps (8-11) above.

1. HESTIA can automatically extract the properties of `crop`, `forage`, and `processedFood` terms from [Feedipedia](https://www.feedipedia.org/). If the Property you are adding has a corresponding feedipedia term, fill in the `feedipediaName` lookup, and the `feedipediaConversionTermId` and `feedipediaConversionEnum` lookups if a unit conversion is required.

The possible values for `feedipediaConversionEnum` are:
- `dmToFmSimple`, when the feedipedia value only needs to be corrected for the `feedipediaConversionTermId` (e.g., the feedipedia unit is % dry matter while the HESTIA unit is % fresh matter). The correction formula will be feedipedia value * `feedipediaConversionTermId`/100.
- `dmToFm10FactorCorrection`, when the feedipedia value must be corrected for the `feedipediaConversionTermId` and then divided by 10 (e.g., the feedipedia unit is g/kg dry matter while the HESTIA unit is % fresh matter. The correction formula will be (feedipedia value * `feedipediaConversionTermId`/100)/10.
- `dmToFm10000FactorCorrection`, when the feedipedia value must be corrected for the `feedipediaConversionTermId` and then divided by 10000 (e.g., the feedipedia unit is mg/kg dry matter while the HESTIA unit is % fresh matter. The correction formula will be (feedipedia value * `feedipediaConversionTermId`/100)/10000.

Note: you can also use the lookup `averagePropertiesTermIds` to specify a list of properties that should be averaged to get the property of the Term, and it can work in "cascading" order. Example:
```
term.id,lookup.name,lookup.value
term1,averagePropertiesTermIds,-
term2,averagePropertiesTermIds,-
term3,averagePropertiesTermIds,term1;term2
term4,averagePropertiesTermIds,-
term5,averagePropertiesTermIds,term3;term4
```

In this example, both `term1` and `term2` properties will be directly extracted from feedipedia. Then `term3` will use the properties of `term1` and `term2` and average the properties. Then `term4` is extracted directly as well, but `term5` will average the properties of `term3` and `term4`. However for this to work, this order in the file **must** be preserved.

## <a name="schema-dev"></a> Schema Development

We also welcome contributions to the data schema. Please see the separate [contribution guidelines](https://gitlab.com/hestia-earth/hestia-schema/-/blob/master/CONTRIBUTING.md) for the schema.

## <a name="commit"></a> Commit Message Guidelines

We have very precise rules over how our git commit messages can be formatted.
This leads to **more readable messages** that are easy to follow when looking through the **project history**.
But also, we use the git commit messages to **generate the HESTIA change log**.

### Commit Message Format
Each commit message consists of a **header**, a **body** and a **footer**.
The header has a special format that includes a **type**, a **scope** and a **subject**:
```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

The **header** and the **scope** of the header are mandatory.

Any line of the commit message cannot be longer than `100` characters! This allows the message to be easier
to read on GitLab as well as in various git tools.

The footer can contain a [closing reference to an issue](https://docs.gitlab.com/ee/user/project/issues/crosslinking_issues.html) if any.

Samples: (even more [samples](https://gitlab.com/hestia-earth/hestia-glossary/-/commits/develop))

```
docs(region): describe source of data
```

```
fix(crop): fix wrong `dryMatter` value for `Wheat, grain`

resolves issue #123
```

### Type
Must be one of the following:

* **build**: Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)
* **ci**: Changes to our CI configuration files and scripts (example scopes: Circle, BrowserStack, SauceLabs)
* **docs**: Documentation only changes
* **feat**: A new feature
* **fix**: A bug fix
* **perf**: A code change that improves performance
* **refactor**: A code change that neither fixes a bug nor adds a feature
* **style**: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
* **test**: Adding missing tests or correcting existing tests

### Scope
The scope should be the name of the Excel file that was modified. Must be one of the following:

* **animalBreed**
* **animalProduct**
* **animalManagement**
* **aquacultureManagement**
* **antibiotic**
* **biologicalControlAgent**
* **building**
* **characterisedIndicator**
* **crop**
* **cropEstablishment**
* **cropResidue**
* **cropResidueManagement**
* **cropSupport**
* **electricity**
* **emission**
* **endpointIndicator**
* **excreta**
* **excretaManagement**
* **experimentDesign**
* **feedFoodAdditive**
* **fertiliserBrandName**
* **forage**
* **fuel**
* **inorganicFertiliser**
* **irrigation**
* **landCover**
* **landUseManagement**
* **liveAnimal**
* **liveAquaticSpecies**
* **machinery**
* **material**
* **measurement**
* **methodEmissionResourceUse**
* **methodMeasurement**
* **model**
* **operation**
* **organicFertiliser**
* **otherOrganicChemical**
* **otherInorganicChemical**
* **pesticideAI**
* **pesticideBrandName**
* **processedFood**
* **processingAid**
* **property**
* **region**
* **resourceUse**
* **sampleDesign**
* **seed**
* **soilAmendment**
* **soilTexture**
* **soilType**
* **standardsLabels**
* **system**
* **tillage**
* **transport**
* **usdaSoilType**
* **veterinaryDrug**
* **waste**
* **wasteManagement**
* **water**
* **waterRegime**
* **waste**
* **wasteManagement**

### Subject
The subject contains a succinct description of the change:

* use the imperative, present tense: "change" not "changed" nor "changes"
* don't capitalize the first letter
* no dot (.) at the end
* use code block formatting for termTypes, term id's and term names
* if a term is moved from one glossary to another, the `termType` in the scoped subject should be the one the term is being moved to (e.g., "feat(landCover): move `Short bare fallow` from `landUseManagement`")
* use `docs()` for changes to any text based fields in the Excel files which provide documentation including `synonyms`, `definition`, `wikipedia`, `pubchem`, `agrovoc` etc.

### Body
Just as in the **subject**, use the imperative, present tense: "change" not "changed" nor "changes".
The body should include the motivation for the change and contrast this with previous behavior.

### Footer
The footer should contain any information about **Breaking Changes** and is also the place to reference GitLab issues that this commit **Closes**.

**Breaking Changes** should start with the word `BREAKING CHANGE:` and a space. The rest of the commit message is then used for this. If you have made multiple breaking changes in the same commit, please write each breaking change on its own line.

A change is considered a "breaking change" when either:
- an existing Term is renamed (or the `id` is modified) or deleted
- an existing Term is moved from one `termType` to another
- a lookup table column or row is renamed (changing of a lookup value is **not** a breaking change)

:warning: The breaking change **must** be the last element in the message. Anything mentioning the issue (like `Closes #101`) must be added __before__ the breaking change.

Example:
```
fix(crop): rename grain terms

Closes #101

BREAKING CHANGE: Rename `Wheat, grain` `Wheat`.
BREAKING CHANGE: Rename `Maize, grain` `Maize`.
```

## <a name="naming-rules-crop"></a> Naming Rules for the Crop Glossary

### General rules

- Always add a term describing the **plant, vine, tree, fungus or algae** from which the product is obtained. The term name should be **singular** and there should be **no comma** (e.g., _Wheat plant_). Set the unit as "ha" (hectare).
- When adding a product, use a **comma** to demarcate the plant name from the product (e.g., _Wheat, grain_). Further specifications can be added using **round brackets** (e.g., _Wheat, grain (in husk)_). All terms should be **singular**.
- When adding scientific names, use the following conventions:
    * Sp. means species. E.g., _Lupinus_ sp. means an unspecified species of the genus _Lupinus_.
    * Spp. means many/several species. E.g., _Lupinus_ spp. means two or more unspecified species of the genus _Lupinus_.
    * Ssp. means subspecies. E.g., _Lupinus albus_ ssp. termis means the termis subspecies of _Lupinus albus_.
    * Var. means variety. E.g., _Lupinus albus_ ssp. termis var. subroseus means the subroseus variety of the termis subspecies of _Lupinus albus_.
    * Do not write "strain" (e.g., _Bacillus amyloliquefaciens_ strain MBI 600), instead simply follow the genus/species/subspecies names with the strain (e.g., _Bacillus amyloliquefaciens_ MBI 600).
    * Do not follow scientific names with "L.", "Boiss." etc. to denote the individual who discovered the species.

### Standard forage/straw terms

To describe the parts of the plant that are used as forage, or for bedding, use the following terms:
- **fresh forage**: for the aerial part of the plant, fresh, directly grazed by animals or used as a "cut-and-carry" fodder.
- **hay**: for the aerial part of the plant, cut, dried and stored to be used in colder months.
- **silage**: for the aerial part of the plant, cut, compacted and stored in airtight conditions without first being dried.
- **straw**: for the dry stalk of the plant left after harvesting the seed and typically used as animal litter or for thatching.

### Standard terms for grains

To describe grains and pseudo-grains, use the following terms:
- **grain (in husk)**: for the seed surrounded by its inedible husk.
- **grain (whole)**: for the seed after the removal of the inedible husk (usually via threshing).
- **grain (refined)**: for the seed after the removal of the fibrous, nutrient-rich bran (via milling).
- **husk**: for the inedible outer covering of the seed.
- **bran**: for the external layer of the seed, rich in fibre and other nutrients.

### Standard terms for legumes

To describe pulses and other plants in the legume family, use the following terms:

- **pod**: for the mature fruit of the plant (pod husk + dry seeds).
- **young pod**: for the unripe fruit of the plant (tender pod husk + immature seeds).
- **seed (whole)**: for the dry seed with its outer covering (or "skin").
- **seed (dehulled)**: for the dry seed without outer covering (or "skin").
- **hull**: for the protective outer covering of the seed (also known as "skin").
- **pod husk**: for the husk of the pod without the seeds.

### Standard terms for nuts

For culinary nuts such as almonds and pistachios, which are technically the seeds of a fruit called "drupe", the following terms should be used:

- **fruit**: for the whole fruit of the tree (fleshy hull + shell + kernel).
- **hull**: for the fleshy part of the fruit (exocarp + mesocarp).
- **in shell**: for the nut within its shell.
- **kernel**: for the nut without shell.
- **shell**: for the lignified endocarp surrounding the nut kernel.

For true botanical nuts, such as chestnuts and hazelnuts, the terms fruit and hull should be replaced with the following terms:
- **in husk**: for the nut enclosed in its protective leafy "husk".
- **husk**:for the protective leafy "husk" enclosing the nut.

### Standard terms for seeds

For seeds and spices use the following terms:

- **seed**: for both true seeds and products that are technically achenes (dry fruits), but are commonly known as "seeds". When relevant, split the term into:
    - **seed (whole)**: for the seed with its outer hull.
    - **seed (dehulled)**: for the seed without its outer hull.
