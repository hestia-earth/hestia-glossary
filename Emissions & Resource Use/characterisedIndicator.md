**Sources:** HESTIA Team; [OpenLCA](https://www.openlca.org/); [ecoinvent](https://ecoinvent.org/).
<br/>

**Note:** Includes both mid-point and end-point characterised indicators.

<!--

## Columns:

`defaultModelId` model ids in this column define what is for display on the frontend, and when an emission or `characterisedIndicator` has multiple models.

-->
