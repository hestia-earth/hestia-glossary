**Source:** HESTIA Team.

<!--
# Internal structure:

## Lookup columns:

### inHestiaDefaultSystemBoundary

If this term is known to be used in one of the models on HESTIA, then `inHestiaDefaultSystemBoundary` should be `True`
and `False` otherwise.

#### Source:
https://green-business.ec.europa.eu/environmental-footprint-methods/life-cycle-assessment-ef-methods_en

#### Completing:
For any new term, lookup if any "similar" terms are used by a hestia model:
- If the new term is being added for use with a new/updated model that uses a lookup factor then `inHestiaDefaultSystemBoundary` should be `True`.
- If the term is known to be returned by a model then `inHestiaDefaultSystemBoundary` should be `True`. For example the model [co2ToAirFuelCombustion.md](https://gitlab.com/hestia-earth/hestia-engine-models/-/blob/develop/hestia_earth/models/emepEea2019/co2ToAirFuelCombustion.md?ref_type=heads) returns the term `co2ToAirFuelCombustion`

#### Used by:
 - (models.emissionNotRelevant)[https://gitlab.com/hestia-earth/hestia-engine-models/-/blob/develop/hestia_earth/models/emissionNotRelevant/_model.md?ref_type=heads] model


### po4-EqEutrophicationExcludingFateCml2001Baseline
Lookup column used by the "cml2001Baseline" "Eutrophication potential, excluding fate" model

#### Source:
https://www.universiteitleiden.nl/en/research/research-output/science/cml-ia-characterisation-factors

#### Completing:
For any new term, download [cmlia.zip](http://www.leidenuniv.nl/cml/ssp/databases/cmlia/cmlia.zip) from [https://www.universiteitleiden.nl](https://www.universiteitleiden.nl/en/research/research-output/science/cml-ia-characterisation-factors#downloads) and search `CML-IA_aug_2016.xls` for factors in the colum "Problem oriented approach: baseline (CML, 2001)
eutrophication (fate not incl.)"

#### Used by:
 - (cml2001Baseline.eutrophicationPotentialExcludingFate)[https://gitlab.com/hestia-earth/hestia-engine-models/-/blob/develop/hestia_earth/models/cml2001Baseline/eutrophicationPotentialExcludingFate.md?ref_type=heads] model

-->