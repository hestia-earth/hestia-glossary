**Source:** [WULCA](https://wulca-waterlca.org/aware/download-aware-factors/).

<!--

# Changelog

## 2021-05-11

- rename column `term.id` to `awareWaterBasinId`
- remove unused columns and only keep `awareWaterBasinId`, `YR_IRRI`, `YR_NONIRRI` and `YR_TOT`

-->
