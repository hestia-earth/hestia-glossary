| Key | Source |
| ------ | ------ |
| genericCropGrain | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| beansPulses | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| tubers | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| rootCropsOther | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| maize | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| wheatGrain | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| winterWheat | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| springWheat | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| ricePaddy | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| barley | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| oats | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| sorghum | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| rye | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| cassava | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| soybeans | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| dryBean | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| potatoes | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| groundnutsWithShell | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| rapeseed | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| sunflowerSeed | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| vegetables | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| sugarcane | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| oilPalmFruit | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| bananas | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| citrus | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| apples | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| orchardFruit | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| berries | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| grapes | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| olives | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| coffee | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| cocoa | [R. Köble, “The Global Nitrous Oxide Calculator (GNOC) online tool manual, version 1.2.4 (European Union, 2014)](http://gnoc.jrc.ec.europa.eu/.) |
| silage | [HESTIA Team] |
