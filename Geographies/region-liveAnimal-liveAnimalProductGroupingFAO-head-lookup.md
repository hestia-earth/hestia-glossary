**Source:** [FAOSTAT (2024)](https://www.fao.org/faostat/en/#data/QCL), Accessed 2025-02-19.

Data was generated by applying the following filters:
* Countries: All
* Element:
<table>
      <thead>
        <tr>
            <th>Name</th><th>Unit</th><th>Definition</th>
        </tr>
      </thead>
      <tbody>
        <tr>
            <td>Stocks</td><td>No</td><td>This variable indicates the number of animals of the species present in the country at the time of enumeration. It includes animals raised either for draft purposes or for meat, eggs and dairy production or kept for breeding. Live animals in captivity for fur or skin such as foxes, minks etc. are not included in the system although furskin trade is reported. The enumeration to be chosen, when more than one survey is taken, is the closest to the beginning of the calendar year. Livestock data are reported in number of heads (units) except for poultry, rabbits and other rodents which are reported in thousand units. Source: FAO Statistics Division</td>
        </tr>
      </tbody>
    </table>

* Items:
<table>
      <thead>
        <tr>
            <th>Name</th><th>Definition</th>
        </tr>
      </thead>
      <tbody>
        <tr>
            <td>Asses</td><td>Asses This subclass is defined through the following headings/subheadings of the HS 2007: 0101.30.</td></tr><tr><td>Bees</td><td>Bees This subclass includes: -  bees, particularly Apis mellifera, dorsata, florea, indica.</td></tr><tr><td>Buffalo</td><td>Buffalo This subclass includes: -  buffalo, species of Bubalus, mainly bubalus, ami, depressicornis, nanus (buffalo, Indian buffalo, water buffalo, carabao, and Pigmi buffalo) - caffers, African buffalo, species of Syncerus -  bisons, species of Bison, American and European This subclass does not include: - cattle, species of Bos, mainly bovis, taurus, indicus, grunniens, gaurus, grontalis and sondaicus, cf. 02111</td></tr><tr><td>Camels</td><td>Bactrian camel (Camelus bactrianus); Arabian camel (C. dromedarius)  Animals of the genus listed, regardless of age, sex, or purpose raised. Data are expressed in number of heads. (Unofficial definition)</td></tr><tr><td>Cattle</td><td>Cattle This subclass includes: - cattle, species of Bos, mainly bovis, taurus, indicus, grunniens, gaurus, grontalis and sondaicus, known with many different names: ox, zebu, yak, gaur, gayal, banteng, etc. This subclass does not include: -  buffalo, species of Bubalus, Syncerus and bisons, species of Bison, cf. 02112</td></tr><tr><td>Chickens</td><td>Chickens This subclass is defined through the following headings/subheadings of the HS 2007: 0105.11, .94.</td></tr><tr><td>Ducks</td><td>Ducks This subclass includes: - ducks, species of Anas , mainlyplatyrhynchos</td></tr><tr><td>Geese</td><td>Geese This subclass includes: - geese, species of Anser , mainly anser, albifrons, arvensis</td></tr><tr><td>Goats</td><td>Goats This subclass is defined through the following headings/subheadings of the HS 2007: 0104.20.</td></tr><tr><td>Horses</td><td>Horses This subclass is defined through the following headings/subheadings of the HS 2007: 0101.21, .29.</td></tr><tr><td>Mules and hinnies</td><td>Mules and hinnies This subclass is defined through the following headings/subheadings of the HS 2007: 0101.90.</td></tr><tr><td>Other camelids</td><td>Various species of Lama: e.g. glama pacos (alpaca); peruana (llama); huanacos (guanaco>); vicugna (vicuna)  Animals of the genus listed, regardless of age, sex, or purpose raised. Data are expressed in number of heads. (Unofficial definition)</td></tr><tr><td>Other rodents</td><td>Includes only those used mainly for meat, e.g. Guinea pig. Rodents used mainly for fur skins are included in Code 02199.10. Data are expressed in thousands. (Unofficial definition)</td></tr><tr><td>Rabbits and hares</td><td>Rabbits and hares This subclass includes: -  rabbits, Oryctolagus cuniculus -  hares, species of Lepus</td></tr><tr><td>Sheep</td><td>Sheep This subclass is defined through the following headings/subheadings of the HS 2007: 0104.10.</td></tr><tr><td>Swine / pigs</td><td>Swine / pigs This subclass is defined through the following headings/subheadings of the HS 2007: 0103.</td></tr><tr><td>Turkeys</td><td>Turkeys This subclass includes: - common and ocellated turkeys, Meleagris gallopavo</td>
        </tr>
      </tbody>
    </table>

* Years: <code>2000</code>, <code>2001</code>, <code>2002</code>, <code>2003</code>, <code>2004</code>, <code>2005</code>, <code>2006</code>, <code>2007</code>, <code>2008</code>, <code>2009</code>, <code>2010</code>, <code>2011</code>, <code>2012</code>, <code>2013</code>, <code>2014</code>, <code>2015</code>, <code>2016</code>, <code>2017</code>, <code>2018</code>, <code>2019</code>, <code>2020</code>, <code>2021</code>, <code>2022</code>, <code>2023</code>
