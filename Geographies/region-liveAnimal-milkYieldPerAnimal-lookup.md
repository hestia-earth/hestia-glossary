**Source:** [IPCC (2019)]( https://www.ipcc-nggip.iges.or.jp/public/2019rf/pdf/4_Volume4/19R_V4_Ch10_Livestock.pdf), Tables 10A.1 for Dairy cattle, 10A.2 and 10A.3 for "Other cattle", 10A.4 for buffalo. For generic terms such as `Dairy cattle`, `Beef cattle`, `Dairy cattle, cow`, and `Beef cattle, cow`, milk yields have been halved, assuming only 50% of the animals in such categories are lactating.
<br/>

**Notes:** units are kg raw milk/day (before fat-protein correction).

<!--

The Excel file used to generate the CSV, with all matchings and assumptions, is attached to issue #1506 in the HESTIA Glossary repo. https://gitlab.com/hestia-earth/hestia-glossary/-/issues/1506

-->
