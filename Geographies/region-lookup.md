| Key | Source |
| ------ | ------ |
| HDI | [UNDP. 2014. Human Development Report 2014: Sustaining Human Progress - Reducing Vulnerabilities and Building Resilience. New York.](http://hdr.undp.org/en/content/human-development-report-2014) |
| EF_NOX | [Stehfest & Bouwman (2006) N2O and NO emission from agricultural fields and soils under natural vegetation: Summarizing available measurement data and modeling of global annual emissions. Nutrient Cycling in Agroecosystems](https://link.springer.com/article/10.1007/s10705-006-9000-7) |
| EF_P_C2 | [Scherer & Pfister (2015) Modelling spatially explicit impacts from phosphorus emissions in agriculture](https://link.springer.com/article/10.1007/s11367-015-0880-0) |
| Practice factor | [Scherer & Pfister (2015) Modelling spatially explicit impacts from phosphorus emissions in agriculture](https://link.springer.com/article/10.1007/s11367-015-0880-0) |
| Annual_crops_TAXA_AGGREGATED_Median_occupation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Annual_crops_TAXA_AGGREGATED_lower95CI_occupation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Annual_crops_TAXA_AGGREGATED_upper95CI_occupation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Permanent_crops_TAXA_AGGREGATED_Median_occupation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Permanent_crops_TAXA_AGGREGATED_lower95CI_occupation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Permanent_crops_TAXA_AGGREGATED_upper95CI_occupation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Pasture_TAXA_AGGREGATED_Median_occupation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Pasture_TAXA_AGGREGATED_lower95CI_occupation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Pasture_TAXA_AGGREGATED_upper95CI_occupation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Urban_TAXA_AGGREGATED_Median_occupation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Urban_TAXA_AGGREGATED_lower95CI_occupation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Urban_TAXA_AGGREGATED_upper95CI_occupation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Extensive_forestry_TAXA_AGGREGATED_Median_occupation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Extensive_forestry_TAXA_AGGREGATED_lower95CI_occupation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Extensive_forestry_TAXA_AGGREGATED_upper95CI_occupation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Intensive_forestry_TAXA_AGGREGATED_Median_occupation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Intensive_forestry_TAXA_AGGREGATED_lower95CI_occupation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Intensive_forestry_TAXA_AGGREGATED_upper95CI_occupation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Annual_crops_TAXA_AGGREGATED_Median_transformation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Annual_crops_TAXA_AGGREGATED_lower95CI_transformation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Annual_crops_TAXA_AGGREGATED_upper95CI_transformation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Permanent_crops_TAXA_AGGREGATED_Median_transformation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Permanent_crops_TAXA_AGGREGATED_lower95CI_transformation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Permanent_crops_TAXA_AGGREGATED_upper95CI_transformation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Pasture_TAXA_AGGREGATED_Median_transformation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Pasture_TAXA_AGGREGATED_lower95CI_transformation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Pasture_TAXA_AGGREGATED_upper95CI_transformation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Urban_TAXA_AGGREGATED_Median_transformation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Urban_TAXA_AGGREGATED_lower95CI_transformation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Urban_TAXA_AGGREGATED_upper95CI_transformation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Extensive_forestry_TAXA_AGGREGATED_Median_transformation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Extensive_forestry_TAXA_AGGREGATED_lower95CI_transformation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Extensive_forestry_TAXA_AGGREGATED_upper95CI_transformation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Intensive_forestry_TAXA_AGGREGATED_Median_transformation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Intensive_forestry_TAXA_AGGREGATED_lower95CI_transformation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Intensive_forestry_TAXA_AGGREGATED_upper95CI_transformation | [Chaudhary et al. (2015) Environmental Science & Technology, 49, 9987-9995](https://doi.org/10.1021/acs.est.5b02507) |
| Conveyancing_Efficiency_Annual_crops | [AQUASTAT (2012)](http://www.fao.org/aquastat/en/); [Poore & Nemecek (2018)](https://doi.org/10.1126/science.aaq0216) |
| Conveyancing_Efficiency_Permanent_crops | [AQUASTAT (2012)](http://www.fao.org/aquastat/en/); [Poore & Nemecek (2018)](https://doi.org/10.1126/science.aaq0216) |
| Conveyancing_Efficiency_Perennial_crops | [AQUASTAT (2012)](http://www.fao.org/aquastat/en/); [Poore & Nemecek (2018)](https://doi.org/10.1126/science.aaq0216) |
