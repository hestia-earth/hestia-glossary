| Key | Source |
| ------ | ------ |
| ecoClimateZone | [Hiederer et al. (2010) Biofuels: A new methodology to estimate GHG emissions from global land use change, European Commission Joint Research Centre](https://ec.europa.eu/jrc/en/publication/eur-scientific-and-technical-research-reports/biofuels-new-methodology-estimate-ghg-emissions-due-global-land-use-change-methodology) |
