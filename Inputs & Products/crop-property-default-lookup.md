**Sources:** [Feed Tables](https://www.feedtables.com), [FoodData Central - USDA](https://fdc.nal.usda.gov), and HESTIA team.

<!--

Note: this file contains default values or proxies for Crop. Setting a value will override the value present in `crop-property-lookup.csv` if present.

-->
