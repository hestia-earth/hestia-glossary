**Sources:** [FAO System of National Accounts](http://www.fao.org/3/a0135e/A0135E10.htm); [FAO ESS](http://www.fao.org/economic/the-statistics-division-ess/world-census-of-agriculture/programme-for-the-world-census-of-agriculture-2000/appendix-4-crop-list/en/); [Feedipedia](https://feedipedia.org/); HESTIA Team.
<br/>

**Note:** All crops have a default dry matter composition which corresponds to marketable weight (ready for storage or sale) and defines the term. To override this, add the Property [Dry Matter](https://hestia.earth/term/dryMatter) to the Product.

<!--

When modifications are made involving the feedipedia links, please re-run `scripts/extract_f_properties_lookup.py`.

-->
