**Source:** [NAICS](https://classcodes.com/lookup/naics-5-digit-industry-22111/).


<!--
# Internal structure:

## Lookup columns:

### abioticResourceDepletionFossilFuelsCml2001Baseline
True if the electricity from this term was produced from non-renewable sources according to the EU PEF impact category "Abiotic resource depletion, fossil fuels – ADP-fossil (MJ)". False otherwise

#### Examples:

`electricityGridHardCoal` Should be `True`
`electricityGridSolarPv` Should be `False`

#### Source:
https://green-business.ec.europa.eu/environmental-footprint-methods/life-cycle-assessment-ef-methods_en

#### Completing:
For any new term, lookup on wikipedia or the EU PEF site here, and set column to boolean TRUE if:
- The energy source is non-renewable
- The energy source comes from any kind of fossil fuel.
- The energy source depletes abiotic material that will no longer be available to future generations. (Including nuclear material)

#### Used by:
 - cml2001Baseline.abioticResourceDepletionFossilFuels model

-->