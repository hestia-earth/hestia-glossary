**Sources:** [feedipedia](https://feedipedia.org/); HESTIA Team.
<br/>

**Notes:** Includes grasses, forage legumes, and forage trees. Crop residues or immature cereal crops fed to animals are included in the [crop](/glossary?termType=crop) glossary.
