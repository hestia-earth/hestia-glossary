**Sources:** [International Recommendations for Energy Statistics](https://unstats.un.org/unsd/energystats/methodology/ires/); [Engineering Toolbox](https://www.engineeringtoolbox.com); HESTIA Team.


<!--
# Internal structure:

## Lookup columns:

### abioticResourceDepletionFossilFuelsCml2001Baseline
True if the electricity from this term was produced from non-renewable sources according to the EU PEF impact category "Abiotic resource depletion, fossil fuels – ADP-fossil (MJ)". False otherwise

#### Examples:

`diesel` Should be `True`
`propane` Should be `True`
`woodFuel` Should be `False`

#### Source:
https://green-business.ec.europa.eu/environmental-footprint-methods/life-cycle-assessment-ef-methods_en

#### Completing:
For any new term, lookup on wikipedia or the EU PEF site here, and set column to boolean TRUE if:
- The energy source is non-renewable or
- The energy source comes from any kind of fossil fuel or
- The energy source depletes abiotic material that will no longer be available to future generations. (Including nuclear material)

#### Used by:
 - cml2001Baseline.abioticResourceDepletionFossilFuels model

-->