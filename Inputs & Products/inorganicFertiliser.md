**Sources:** [FAOSTAT Commodity List (FCL)](http://www.fao.org/economic/ess/ess-standards/commodity/fr/); HESTIA Team.
<br/>

**Note:** Fertilisers are defined as substances which primarily provide nutrients to the plant, in contrast to [soil amendments](/glossary?termType=soilAmendment), which primarily improve the physical properties of the soil.
