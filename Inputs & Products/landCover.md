**Sources:** [FAO System of National Accounts](http://www.fao.org/3/a0135e/A0135E10.htm); [FAO ESS](http://www.fao.org/economic/the-statistics-division-ess/world-census-of-agriculture/programme-for-the-world-census-of-agriculture-2000/appendix-4-crop-list/en/); [Feedipedia](https://feedipedia.org/); HESTIA Team.
<br/>

**Note:** Land cover terms are added as Products to the Cycle (there are often Inputs and Practices required to change land cover e.g., you may need to sow seed to change from one land cover type to another).


<!--
# Internal structure:

## Lookup columns:

### IPCC_LAND_USE_CATEGORY
Based on lookup `cropGroupingFAO`. For use in the IPCC (2019) tier 1 SOC stock change model.

#### Examples:
Grassland
Annual crops
Set aside
Perennial crops

#### Source:
[IPCC (2019)](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html)

### Completing:
"Used in a lot of IPCC models, including the SOC model, and it should not be modified, or the model will fail."

#### Used by:
 - ipcc2019/organicCarbonPerHa model

### pefTermGrouping

Column mapping hestia landCover terms to the closest known Corine+ land classification system term

#### Examples:
`forest`
`forest, intensive`
`grassland/pasture/meadow`
`artificial areas`
`urban`
`urban, discontinuously built`

#### Source:
Corine+ land classification system terms as used in https://eplca.jrc.ec.europa.eu/permalink/EF3_1/EF-LCIAMethod_CF(EF-v3.1).xlsx

### Completing:
When adding a new `landCover` term, check the [corine+ nomenclature](https://land.copernicus.eu/content/corine-land-cover-nomenclature-guidelines/html/) for the closest equivalent.
Terms should match column names as seen in:
`Geographies/region-pefTermGrouping-landOccupation-lookup.csv`
Values can be:
- a corine lowercase class levels 1 or 2: `<CORINE_class_level1_or_2>`
- a corine lowercase class levels 1 or 2 followed by a level3, seperated by a `, ` comma and space: `<CORINE_class_level1_or_2>, <CORINE_class_level3>`
If you cannot find a direct equivalent, use the `pefTermGrouping` value of one of the parent Class terms listed in `subClassOf.0.id` `subClassOf.1.id` or `subClassOf.2.id`.

#### Used by:
 - soilQualityIndexLandOccupation model
 - soilQualityIndexLandTransformation model
 - soilQualityIndexTotalLandUseEffects model

-->