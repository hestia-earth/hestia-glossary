**Source:** HESTIA Team.

<!--

# Internal structure:

## Lookup columns:

### abioticResourceDepletionMineralsAndMetalsCml2001Baseline

Map terms to known EU PEF characterisation factors to calculate "Abiotic resource depletion, for minerals and metals
Indicator".

#### Source:

Source: [eplca.jrc.ec.europa.eu](https://eplca.jrc.ec.europa.eu/permalink/EF3_1/EF-LCIAMethod_CF(EF-v3.1).xlsx),
Accessed 2024-09-05.

#### Used by:

- [cml2001Baseline](https://hestia.earth/term/cml2001Baseline) model. 


-->
 