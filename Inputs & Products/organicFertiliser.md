**Sources:** [University of Maryland Extension](https://extension.umd.edu/sites/extension.umd.edu/files/_images/programs/hgic/Publications/HG42_Soil_Amendments_and_Fertilizers.pdf); [Poore & Nemecek (2018)](https://science.sciencemag.org/content/360/6392/987); [Webb et al. (2012)](https://link.springer.com/chapter/10.1007/978-94-007-1905-7_4); [IPCC (2019)](https://www.ipcc-nggip.iges.or.jp/public/2019rf/pdf/4_Volume4/19R_V4_Ch05_Cropland.pdf); HESTIA Team.
<br/>

**Note:** Fertilisers are defined as substances which primarily provide nutrients to the plant, in contrast to [soil amendments](/glossary?termType=soilAmendment), which primarily improve the physical properties of the soil.
