**Source:** [Feed Tables](https://www.feedtables.com), [FoodData Central - USDA](https://fdc.nal.usda.gov).

<!--

Note: this file contains default values or proxies for Processed food. Setting a value will override the value present in `processedFood-property-lookup.csv` if present.
-->


