**Source:** HESTIA Team.
<br/>

**Note:** Processing aids are substances used in the processing of raw materials, foods or food ingredients to fulfil a certain technological purpose. Their eventual residue in the finished product must not perform any technological effect on it.
