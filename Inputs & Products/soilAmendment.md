**Sources:** [University of Maryland Extension](https://extension.umd.edu/sites/extension.umd.edu/files/_images/programs/hgic/Publications/HG42_Soil_Amendments_and_Fertilizers.pdf); HESTIA Team.
<br/>

**Note:** Soil amendments are defined as substances which primarily improve the physical properties of the soil, in contrast to [organic](/glossary?termType=organicFertiliser) and [inorganic fertilisers](/glossary?termType=inorganicFertiliser), which primarily provide substances which are absorbed by the plant.

<!--

# Internal structure:

## Lookup columns:

### abioticResourceDepletionMineralsAndMetalsCml2001Baseline

Map terms to known EU PEF characterisation factors to calculate "Abiotic resource depletion, for minerals and metals
Indicator".

#### Source:

Source: [eplca.jrc.ec.europa.eu](https://eplca.jrc.ec.europa.eu/permalink/EF3_1/EF-LCIAMethod_CF(EF-v3.1).xlsx),
Accessed 2024-09-05.

#### Used by:

- [cml2001Baseline](https://hestia.earth/term/cml2001Baseline) model. 
 
-->
