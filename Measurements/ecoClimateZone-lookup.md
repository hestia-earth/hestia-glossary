| Key | Source | Date Accessed |
| ------ | ------ | ------ |
| ecoClimateZone | [Hiederer et al. (2010) Biofuels: A new methodology to estimate GHG emissions from global land use change, European Commission Joint Research Centre](https://ec.europa.eu/jrc/en/publication/eur-scientific-and-technical-research-reports/biofuels-new-methodology-estimate-ghg-emissions-due-global-land-use-change-methodology) | 2021 |
| IPCC_2006_ORGANIC_SOILS_TONNES_CO2-C_HECTARE | [IPCC (2006) Volume 4](https://www.ipcc-nggip.iges.or.jp/public/2006gl/vol4.html)           | 2021       |
| IPCC_2006_ORGANIC_SOILS_KG_N2O-N_HECTARE     | [IPCC (2006) Volume 4](https://www.ipcc-nggip.iges.or.jp/public/2006gl/vol4.html)           | 2021       |
| STEHFEST_BOUWMAN_2006_N2O-N_FACTOR           | [Stehfest & Bouwman (2006)](https://link.springer.com/article/10.1007/s10705-006-9000-7)    | 2021       |
| STEHFEST_BOUWMAN_2006_NOX-N_FACTOR           | [Stehfest & Bouwman (2006)](https://link.springer.com/article/10.1007/s10705-006-9000-7)    | 2021       |
| IPCC_2019_SOC_REF_KG_C_HECTARE_HAC           | [IPCC (2019) Volume 4 Chapter 2](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 06/06/2023 |
| IPCC_2019_SOC_REF_SD_HAC                     | [IPCC (2019) Volume 4 Chapter 2](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 06/06/2023 |
| IPCC_2019_SOC_REF_KG_C_HECTARE_LAC           | [IPCC (2019) Volume 4 Chapter 2](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 06/06/2023 |
| IPCC_2019_SOC_REF_SD_LAC                     | [IPCC (2019) Volume 4 Chapter 2](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 06/06/2023 |
| IPCC_2019_SOC_REF_KG_C_HECTARE_SAN           | [IPCC (2019) Volume 4 Chapter 2](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 06/06/2023 |
| IPCC_2019_SOC_REF_SD_SAN                     | [IPCC (2019) Volume 4 Chapter 2](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 06/06/2023 |
| IPCC_2019_SOC_REF_KG_C_HECTARE_POD           | [IPCC (2019) Volume 4 Chapter 2](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 06/06/2023 |
| IPCC_2019_SOC_REF_SD_POD                     | [IPCC (2019) Volume 4 Chapter 2](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 06/06/2023 |
| IPCC_2019_SOC_REF_KG_C_HECTARE_VOL           | [IPCC (2019) Volume 4 Chapter 2](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 06/06/2023 |
| IPCC_2019_SOC_REF_SD_VOL                     | [IPCC (2019) Volume 4 Chapter 2](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 06/06/2023 |
| IPCC_2019_SOC_REF_KG_C_HECTARE_WET           | [IPCC (2019) Volume 4 Chapter 2](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 06/06/2023 |
| IPCC_2019_SOC_REF_SD_WET                     | [IPCC (2019) Volume 4 Chapter 2](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 06/06/2023 |
| IPCC_2019_LANDUSE_FACTOR_ANNUAL_CROPS        | [IPCC (2019) Volume 4 Chapter 5](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 07/06/2023 |
| IPCC_2019_LANDUSE_FACTOR_SD_ANNUAL_CROPS     | [IPCC (2019) Volume 4 Chapter 5](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 07/06/2023 |
| IPCC_2019_LANDUSE_FACTOR_ANNUAL_CROPS_WET    | [IPCC (2014) Chapter 5](https://www.ipcc-nggip.iges.or.jp/public/wetlands/) | 07/06/2023 |
| IPCC_2019_LANDUSE_FACTOR_SD_ANNUAL_CROPS_WET | [IPCC (2014) Chapter 5](https://www.ipcc-nggip.iges.or.jp/public/wetlands/) | 07/06/2023 |
| IPCC_2019_LANDUSE_FACTOR_PADDY_RICE_CULTIVATION | [IPCC (2019) Volume 4 Chapter 5](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 07/06/2023 |
| IPCC_2019_LANDUSE_FACTOR_SD_PADDY_RICE_CULTIVATION | [IPCC (2019) Volume 4 Chapter 5](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 07/06/2023 |
| IPCC_2019_LANDUSE_FACTOR_PERENNIAL_CROPS     | [IPCC (2019) Volume 4 Chapter 5](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 07/06/2023 |
| IPCC_2019_LANDUSE_FACTOR_SD_PERENNIAL_CROPS  | [IPCC (2019) Volume 4 Chapter 5](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 07/06/2023 |
| IPCC_2019_LANDUSE_FACTOR_SET_ASIDE           | [IPCC (2019) Volume 4 Chapter 5](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 07/06/2023 |
| IPCC_2019_LANDUSE_FACTOR_SD_SET_ASIDE        | [IPCC (2019) Volume 4 Chapter 5](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 07/06/2023 |
| IPCC_2019_LANDUSE_FACTOR_GRASSLAND           | [IPCC (2019) Volume 4 Chapter 6](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 08/06/2023 |
| IPCC_2019_TILLAGE_MANAGEMENT_FACTOR_FULL_TILLAGE | [IPCC (2019) Volume 4 Chapter 5](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 07/06/2023 |
| IPCC_2019_TILLAGE_MANAGEMENT_FACTOR_REDUCED_TILLAGE | [IPCC (2019) Volume 4 Chapter 5](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 07/06/2023 |
| IPCC_2019_TILLAGE_MANAGEMENT_FACTOR_SD_REDUCED_TILLAGE | [IPCC (2019) Volume 4 Chapter 5](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 07/06/2023 |
| IPCC_2019_TILLAGE_MANAGEMENT_FACTOR_NO_TILLAGE | [IPCC (2019) Volume 4 Chapter 5](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 07/06/2023 |
| IPCC_2019_TILLAGE_MANAGEMENT_FACTOR_SD_NO_TILLAGE | [IPCC (2019) Volume 4 Chapter 5](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 07/06/2023 |
| IPCC_2019_GRASSLAND_MANAGEMENT_FACTOR_NOMINALLY_MANAGED | [IPCC (2019) Volume 4 Chapter 6](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 08/06/2023 |
| IPCC_2019_GRASSLAND_MANAGEMENT_FACTOR_HIGH_INTENSITY_GRAZING | [IPCC (2019) Volume 4 Chapter 6](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 08/06/2023 |
| IPCC_2019_GRASSLAND_MANAGEMENT_FACTOR_SD_HIGH_INTENSITY_GRAZING | [IPCC (2019) Volume 4 Chapter 6](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 08/06/2023 |
| IPCC_2019_GRASSLAND_MANAGEMENT_FACTOR_SEVERELY_DEGRADED | [IPCC (2019) Volume 4 Chapter 6](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 08/06/2023 |
| IPCC_2019_GRASSLAND_MANAGEMENT_FACTOR_SD_SEVERELY_DEGRADED | [IPCC (2019) Volume 4 Chapter 6](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 08/06/2023 |
| IPCC_2019_GRASSLAND_MANAGEMENT_FACTOR_IMPROVED_GRASSLAND | [IPCC (2019) Volume 4 Chapter 6](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 08/06/2023 |
| IPCC_2019_GRASSLAND_MANAGEMENT_FACTOR_SD_IMPROVED_GRASSLAND | [IPCC (2019) Volume 4 Chapter 6](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 08/06/2023 |
| IPCC_2019_CROPLAND_CARBON_INPUT_FACTOR_MEDIUM         | [IPCC (2019) Volume 4 Chapter 5](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 07/06/2023 |
| IPCC_2019_CROPLAND_CARBON_INPUT_FACTOR_LOW            | [IPCC (2019) Volume 4 Chapter 5](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 07/06/2023 |
| IPCC_2019_CROPLAND_CARBON_INPUT_FACTOR_SD_LOW         | [IPCC (2019) Volume 4 Chapter 5](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 07/06/2023 |
| IPCC_2019_CROPLAND_CARBON_INPUT_FACTOR_HIGH_WITHOUT_MANURE | [IPCC (2019) Volume 4 Chapter 5](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 07/06/2023 |
| IPCC_2019_CROPLAND_CARBON_INPUT_FACTOR_SD_HIGH_WITHOUT_MANURE | [IPCC (2019) Volume 4 Chapter 5](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 07/06/2023 |
| IPCC_2019_CROPLAND_CARBON_INPUT_FACTOR_HIGH_WITH_MANURE | [IPCC (2019) Volume 4 Chapter 5](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 07/06/2023 |
| IPCC_2019_CROPLAND_CARBON_INPUT_FACTOR_SD_HIGH_WITH_MANURE | [IPCC (2019) Volume 4 Chapter 5](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 07/06/2023 |
| IPCC_2019_GRASSLAND_CARBON_INPUT_FACTOR_MEDIUM        | [IPCC (2019) Volume 4 Chapter 6](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 08/06/2023 |
| IPCC_2019_GRASSLAND_CARBON_INPUT_FACTOR_HIGH          | [IPCC (2019) Volume 4 Chapter 6](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 08/06/2023 |
| IPCC_2019_GRASSLAND_CARBON_INPUT_FACTOR_SD_HIGH       | [IPCC (2019) Volume 4 Chapter 6](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html) | 08/06/2023 |
