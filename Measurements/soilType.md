**Source:** FAO (2015) [World reference base for soil resources 2014](http://www.fao.org/3/i3794en/I3794en.pdf).
<br/>

**Notes:** This glossary includes reference soil groups and principal qualifiers to one level only. Supplementary qualifiers are not included in this glossary.
