**Source:** Soil Survey Staff (1999) [Soil Taxonomy: A basic system of soil classification for making and interpreting soil surveys.](https://www.nrcs.usda.gov/wps/portal/nrcs/main/soils/survey/class/taxonomy/) Second edition.
<br/>

**Note:** Obsolete soil types, [listed here](https://soilseries.sc.egov.usda.gov/osdquery_codes.aspx?domain=subgroup), are excluded.
