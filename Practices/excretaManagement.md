**Source:** [IPCC (2019)](https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html).

<!--
Lookup Sources:
| Lookup | Source |
| ------ | ------ |
| EF_N2O-N | [IPCC (2019) Vol.4 Tables 10A-4 - 10A-9](https://www.ipcc-nggip.iges.or.jp/public/2019rf/pdf/4_Volume4/19R_V4_Ch10_Livestock.pdf) |
| EF_NON-N | [Dämmgen et al. (2009) Table 5.4](https://www.thuenen.de/media/publikationen/landbauforschung-sonderhefte/lbf_sh324.pdf) |
| EF_NO3-N | [EPA (2014), Anexx 3, p. 280, Table A-211](https://www.epa.gov/sites/default/files/2015-12/documents/us-ghg-inventory-2014-annexes.pdf) |
-->
