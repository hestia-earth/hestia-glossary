**Source:** HESTIA Team.
<br/>

**Notes:** Properties allow further information to be added to [Products](https://hestia.earth/schema/Product), [Inputs](https://hestia.earth/schema/Input), [Emissions](https://hestia.earth/schema/Emission) and [Practices](https://hestia.earth/schema/Practice). Sometimes default values for Properties are associated with the Terms that describe the [Products](https://hestia.earth/schema/Product#properties), [Inputs](https://hestia.earth/schema/Input#properties), [Emissions](https://hestia.earth/schema/Emission#properties), and [Practices](https://hestia.earth/schema/Practice#properties) (such as the average nitrogen content of manure).
