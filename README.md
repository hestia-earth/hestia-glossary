# HESTIA Glossary

Describing data consistently with the same terms makes data exchange possible. HESTIA maintains a [Glossary of Terms](https://hestia.earth/glossary) to describe the agri-food system. Only these Terms can be used to describe data.

Terms are created or amended as follows:

1) Glossaries for each termType are initially created in Excel files. Each column header must either be a field in the schema for Term, a Property from the Glossary of Terms, or a lookup.

2) To update or change an Excel file, carefully follow the [contribution guidelines](/CONTRIBUTING.md). The HESTIA team will review your updates and approve them.

3) Each row of the Excel file is converted into a JSON-LD file following the schema for the Term.

4) The terms are then indexed, assigned `@id` fields, and are available for use when uploading new data or performing calculations on existing data.

# Properties versus lookups

Properties are data items that describe a Term in more depth or differentiate a Term from another Term. For example, the dry matter of a crop defines the nature of the crop (e.g. whether it is pre-drying, dried, or expressed in dry matter equivalents). Properties can be overridden by the user.

Lookups provide additional information about each Term for use in environmental models. Lookups cannot be overridden by the user.

# Property and lookup colour coding

The data in Properties and lookups can be of variable quality and might be missing. For lookups we indicate this with `dataState`. For both Properties and lookups we also use the following colour coding.

| `dataState`           | Colour      | Definition                                                                  |
|-----------------------|-------------|-----------------------------------------------------------------------------|
| `complete`            | Dark green  | The data are complete with no validation issues identified.                 |
| `not required`        | Dark green  | The data are not required or relevant.                                      |
| `requires validation` | Light Green | The data require validation due to possible or known errors.                |
| `missing`             | Pink        | The data are missing and need to be added to run models based on this Term. |
| `unassigned`          | White       | A data state has not been assigned.                                         |

# Contributing

Please read the [contribution guidelines](/CONTRIBUTING.md) before making contributions to the Glossary of Terms.
