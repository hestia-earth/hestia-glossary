#!/usr/bin/env bash

echo "Commiting changes..."

cd $DEPLOY_REPO

git add --all
git commit -m "add changes from ${SOURCE_BRANCH}"
git push --force --set-upstream origin $SOURCE_BRANCH

echo "Checking for existing MR..."

MR_URL="https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests?state=opened&source_branch=${SOURCE_BRANCH}&target_branch=${TARGET_BRANCH}"
RES=$(curl -s -X GET -H "PRIVATE-TOKEN:${GITLAB_CI_TOKEN}" -H "Content-Type: application/json" "$MR_URL")
DEPLOY_URL=$(python -c "import json; print(json.loads('$RES')[0]['web_url'])")

if [ -z "$DEPLOY_URL" ]; then
  echo "No existing MR found, creating..."

  BODY="{
    \"id\": ${CI_PROJECT_ID},
    \"source_branch\": \"${SOURCE_BRANCH}\",
    \"target_branch\": \"${TARGET_BRANCH}\",
    \"assignee_id\": \"${GITLAB_USER_ID}\",
    \"remove_source_branch\": true,
    \"title\": \"Draft: merge ${SOURCE_BRANCH} onto ${TARGET_BRANCH}\"
  }";
  MR_URL="https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests"

  RES=$(curl -s -X POST -H "PRIVATE-TOKEN:${GITLAB_CI_TOKEN}" -H "Content-Type: application/json" -d "${BODY}" "$MR_URL")

  DEPLOY_URL=$(python -c "import json; print(json.loads('$RES')['web_url'])")
  echo "Created MR: $DEPLOY_URL"
else
  echo "Existing MR found: $DEPLOY_URL"
fi

echo "DEPLOY_URL=${DEPLOY_URL}" >> deploy.env
