# Hestia Glossary - GADM

## Setup

First, you will need to download the GADM zip files containing the shapefiles.

Use the following script to download all the files for GADM version `4.0.4` (check https://gadm.org/data.html for latest version):
```sh
./download-gadm.sh 40
```

You should end up having the following files:
- downloads/shp/gadm40_ABW_shp.zip
- downloads/shp/gadm40_AFG_shp.zip
- etc.

## Running locally

1. Move the downloaded files to the `files` directory:
```sh
mkdir files
mv downloads/shp/* files/.
```
1. Install python3
1. Install the dependencies using `pip install -r requirements.txt`
1. Run `python import.py` to convert all countries (will take a long time)

Note: you can use `python import.py -h` to see the list of options.

To merge all country generated files into a single one, you can use `python merge.py`.

## Running with Docker

Once the files are downloaded, you can run `./run-docker.sh` to extract and compile the data from the files.

The result will be saved under `./data` folder.

Note: you can use `./run-docker.sh -h` to see the list of options.

## Troubleshoot

### Handling duplicates

If you are updating the gadm files, you might need to re-generate the list of duplicate country names.

1. Run `python scripts/convert_to_csv.py` to generate the csv files under the `csv/` dir
2. Run `python gadm/duplicate_mappings.py` to generate the duplicate mapping file

### Encoding errors

If some files seem corrupted (` ` in the file), you can use `python corrupted.py` to find those files.
It will print in return the list of commands to run.

### Updating the region subClass

To make changes to the region `term.subClassOf.0.id`:
1. Update the region `term.subClassOf.0.id` in `region.xlsx`
2. Run `python3 generate_region_area.py` to update the `additional-regions.csv` file
3. Run `python3 sync_region_area.py` to update the `area` field in `region.xlsx` file

Now you can commit both `Geographies/region.xlsx` and `gadm/additional-regions.csv` files.
