import sys
from os.path import dirname, join, abspath
import numpy
import pandas as pd

CURRENT_DIR = dirname(abspath(__file__))
GEO_DIR = join(CURRENT_DIR, '..', 'Geographies')
AREA_COLUMN = 'term.area'  # last column
FILEPATH = join(CURRENT_DIR, 'additional-regions.csv')


def non_empty_list(values: list): return list(filter(lambda v: v is not None and v != '', values))


with open(join(CURRENT_DIR, 'headers.txt'), 'r') as f:
    headers = non_empty_list(f.read().split('\n'))
df = pd.read_csv(FILEPATH, index_col=None, header=None, names=headers)


def comute_area(source_df: pd.DataFrame):
    for _index, row in df.iterrows():
        term_id = row['term.id']
        value = row[AREA_COLUMN]
        if term_id.startswith('region-') and not value:
            values = source_df[source_df['term.subClassOf.0.id'] == term_id][AREA_COLUMN].to_dict().values()
            values = [v for v in values if not numpy.isnan(v)]
            value = sum(values) if len(values) > 0 else 0
            df.loc[df['term.id'] == term_id, [AREA_COLUMN]] = value
    return df


def main(args: list):
    df_region = pd.read_excel(join(GEO_DIR, 'region.xlsx'), index_col='term.id')
    # compute from countries
    comute_area(df_region)

    # compute from regions
    comute_area(df)

    df.to_csv(FILEPATH, index=False, header=False)


if __name__ == "__main__":
    main(sys.argv[1:])
