import os
import re


def non_empty_list(values: list): return list(filter(lambda x: len(x) > 0, values))


with open('./headers.txt', 'r') as f:
    headers = non_empty_list(f.read().split('\n'))


def merge_files(files: list):
    with open('data/region.csv', 'w', encoding='utf-8-sig') as outfile:
        outfile.write(','.join(headers) + '\n')
        for fname in files:
            with open(f"data/{fname}") as infile:
                outfile.writelines(infile.readlines())
        with open('additional-regions.csv') as missing:
            outfile.writelines(missing.readlines())


file_regex = re.compile(r'[A-Z]{3}_[\d]+\.csv')
files = os.listdir('data')
files = list(filter(lambda f: file_regex.match(f), files))
files.sort(key=lambda x: int(x[4:].replace('.csv', '')))
print(f"Merging {len(files)} files")
merge_files(files)
