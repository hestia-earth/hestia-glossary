#!/bin/sh

docker build -t hestia-glossary-gadm:test . && \
docker run --rm --name hestia-glossary-gadm \
  -v ${PWD}/downloads/shp:/app/files \
  -v ${PWD}/data:/app/data \
  hestia-glossary-gadm:test python import.py "$@"
