# updates the names based on duplicates in the region.xlsx file
# deps: pip3 install pandas openpyxl
from os.path import dirname, join, abspath
import sys
import pandas as pd

CURRENT_DIR = dirname(abspath(__file__))
SRC_DIR = join(CURRENT_DIR, '..', 'Geographies')
filename = 'region.xlsx'

AREA_FILEPATH = join(CURRENT_DIR, 'additional-regions.csv')
AREA_COLUMN = 'term.area'  # last column


def non_empty_list(values: list): return list(filter(lambda x: len(x) > 0, values))


with open(join(CURRENT_DIR, 'headers.txt'), 'r') as f:
    headers = non_empty_list(f.read().split('\n'))
area_df = pd.read_csv(AREA_FILEPATH, index_col=None, header=None, names=headers)


def main(args):
    df = pd.read_excel(join(SRC_DIR, filename))

    for _index, row in area_df.iterrows():
        term_id = row['term.id']
        value = row[AREA_COLUMN]
        df.loc[df['term.id'] == term_id, [AREA_COLUMN]] = value

    df.to_excel(join(SRC_DIR, filename), index=None, header=True)


if __name__ == "__main__":
    main(sys.argv[1:])
