const loadResource = filename => require(`./resources/${filename}`);

/**
 * Get a value from a resource.
 *
 * @param {string} filename The name of the file containing the resources.
 * @param {string} key The key in the resource file.
 * @returns {any} The data associated with the `key`.
 */
const loadResourceKey = (filename, key) => loadResource(filename)[key];

/**
 * Converts the iso31662 code to the [term.name](https://hestia.earth/schema/Term#name) from the HESTIA Glossary.
 *
 * @param {string} code The [iso31662 country code](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2).
 * @returns {string} The HESTIA country [name](https://hestia.earth/schema/Term#name).
 */
const iso31662ToName = (code = '') => loadResourceKey('iso3166-2-to-name.json', code.toUpperCase());

/**
 * Converts the iso31662 code to the [term.id](https://hestia.earth/schema/Term#id) from the HESTIA Glossary.
 *
 * @param {string} code The [iso31662 country code](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2).
 * @returns {string} The HESTIA country [id](https://hestia.earth/schema/Term#id).
 */
const iso31662ToId = (code = '') => loadResourceKey('iso3166-2-to-id.json', code.toUpperCase());

/**
 * Converts the [term.id](https://hestia.earth/schema/Term#id) from the HESTIA Glossary to a iso31662 code.
 *
 * @param {string} id The HESTIA country [id](https://hestia.earth/schema/Term#id).
 * @returns {string} The [iso31662 country code](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2).
 */
const idToIso31662 = (id = '') => loadResourceKey('id-to-iso3166-2.json', id.toUpperCase());

/**
 * Determines how an array of values should be handled (if summed up, or averaged, etc.).
 *
 * @param {string} id The HESTIA term [id](https://hestia.earth/schema/Term#id).
 * @returns {string} Possible values are: `mean`, `mode` and `sum`.
 */
const getArrayTreatment = (id) => loadResourceKey('arrayTreatment.json', id) || 'sum';

/**
 * Returns the `@id` of the default model that should be used for display of this Term.
 *
 * @param {string} id The HESTIA term [id](https://hestia.earth/schema/Term#id).
 * @returns {string} The `@id` of the model if set.
 */
const getDefaultModelId = (id) => loadResourceKey('defaultModelId.json', id);

/**
 * Returns the list of `@id` of the default models that should be used for display.
 *
 * @param {string} id The HESTIA term [id](https://hestia.earth/schema/Term#id).
 * @returns {string[]} The list of default `@id`.
 */
const getDefaultModels = () => Object.entries(loadResource('defaultModelId.json')).filter(([key, value]) => !!value).map(([key]) => key);

/**
 * Returns the list of restrictions of the Term.
 *
 * @param {string} id The HESTIA term [id](https://hestia.earth/schema/Term#id).
 * @returns A list of restrictions as an object.
 */
const getTermRestrictions = (id) => loadResourceKey('term-restrictions.json', id);

/**
 * Returns the list of restrictions of the Term/Model mapping.
 *
 * @param {string} id The HESTIA term [id](https://hestia.earth/schema/Term#id).
 * @param {string} modelId The HESTIA term [id](https://hestia.earth/schema/Term#id).
 * @returns A list of restrictions as an object.
 */
const getTermModelRestrictions = (id, modelId) => (loadResourceKey('term-restrictions.json', id) || {})[modelId];

/**
 * Checks whether the emission is in the HESTIA System Boundary.
 *
 * @param {string} id The HESTIA term [id](https://hestia.earth/schema/Term#id).
 * @returns {boolean} True if the Term is included.
 */
const isInSystemBoundary = (id) => {
  const value = loadResourceKey('inHESTIADefaultSystemBoundary.json', id);
  return !value || value === 'true';
};

module.exports = {
  loadResourceKey,
  iso31662ToName,
  iso31662ToId,
  idToIso31662,
  getArrayTreatment,
  getDefaultModelId,
  getDefaultModels,
  getTermRestrictions,
  getTermModelRestrictions,
  isInSystemBoundary
};
