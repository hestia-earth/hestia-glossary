#!/bin/sh
docker build -t hestia-glossary:script .
docker run --rm \
  --name hestia-glossary-script \
  --env-file .env \
  -v ${PWD}:/app \
  hestia-glossary:script python "$@"
