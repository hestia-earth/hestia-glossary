import os
import sys
from functools import reduce
import requests
from requests.adapters import HTTPAdapter
from concurrent.futures import ThreadPoolExecutor
import pandas as pd

EXTENSION = '.xlsx'
BASE_DIR = './'
EXCLUDED_FOLDERS = ['csv', 'data', 'gadm', 'lookups', 'envs', 'scripts']


def is_emtpy(value): return str(value) == '-' or str(value) == '' or pd.isna(value)


def non_empty_value(value): return not is_emtpy(value)


def non_empty_list(values): return list(filter(non_empty_value, values))


def flatten(values: list): return list(reduce(lambda x, y: x + (y if isinstance(y, list) else [y]), values, []))


def valid_files(files): return list(filter(lambda f: f.endswith(EXTENSION), files))


def get_files(folder: str):
    files = valid_files(os.listdir(os.path.join(BASE_DIR, folder)))
    return list(map(lambda f: os.path.join(folder, f), files))


def get_all_files(files_to_ignore=[]):
    folders = list(filter(lambda f: f not in EXCLUDED_FOLDERS and os.path.isdir(f), os.listdir(BASE_DIR)))
    files = list(reduce(lambda prev, curr: prev + get_files(curr), folders, []))
    for ignore in [f"Geographies/region{EXTENSION}"] + files_to_ignore:
        files.remove(ignore)
    return files


def valid_link(link: str):
    return not any([
        'chem.nlm.nih.gov' in link
    ])


def check_broken_link(link: str):
    try:
        s = requests.Session()
        s.mount(link.strip(), HTTPAdapter(max_retries=5))
        resp = s.get(link.strip())
        code = resp.status_code
        return link if code >= 300 else None
    except Exception as e:
        print('Error fetching url', link, str(e))
        return link


def check_broken_links(filepath: str):
    df = pd.read_excel(filepath, index_col=0, dtype=str)
    links = set(flatten([
        list(df[col].loc[df[col].str.startswith('http', na=False)].values) for col in df
    ]))

    with ThreadPoolExecutor() as executor:
        return non_empty_list(executor.map(check_broken_link, list(filter(valid_link, links))))


def main(args: list):
    files = non_empty_list(args[0].split('\n') if len(args) == 1 else args if len(args) > 1 else get_all_files())
    files = valid_files([f for f in files if os.path.exists(f)])
    print(files)

    exit_code = 0
    for filepath in files:
        links = check_broken_links(filepath)
        if len(links) > 0:
            print('Found potential broken links in', filepath, '\n')
            print('\n'.join(links))
            exit_code = 2

    sys.exit(exit_code)


if __name__ == "__main__":
    main(sys.argv[1:])
