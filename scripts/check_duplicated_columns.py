import os
import sys
from functools import reduce
import pandas as pd

EXTENSION = '.xlsx'
BASE_DIR = './'
EXCLUDED_FOLDERS = ['csv', 'data', 'gadm', 'lookups', 'envs', 'scripts']


def flatten(values: list): return list(reduce(lambda x, y: x + (y if isinstance(y, list) else [y]), values, []))


def _list_files(folder: str):
    return list(map(
        lambda filename: os.path.join(folder, filename),
        list(filter(lambda file: file.endswith(EXTENSION), os.listdir(folder)))
    ))


def _validate_file(filepath: str):
    df = pd.read_excel(filepath, index_col='term.id')
    columns = list(df.columns)
    # pandas automatically renames duplicated column adding `.1` suffix
    duplicates = [
        x for x in columns if x + '.1' in columns and not x.startswith('-')
    ]
    return duplicates


def main():
    folders = [
        os.path.join(BASE_DIR, f) for f in os.listdir(BASE_DIR) if f not in EXCLUDED_FOLDERS and os.path.isdir(f)
    ]
    files = flatten(map(_list_files, folders))

    exit_code = ''

    for filepath in files:
        errors = _validate_file(filepath)
        if len(errors) > 0:
            err = '\n\t'.join(errors)
            exit_code += f"\nDuplicated column names in '{filepath}':\n\t{err}"

    sys.exit(0 if exit_code == '' else exit_code)


if __name__ == "__main__":
    main()
