import os
import sys
from functools import reduce
import pandas as pd
from unidecode import unidecode

DATA_DIR = 'csv'


def flatten(values: list): return list(reduce(lambda x, y: x + (y if isinstance(y, list) else [y]), values, []))


def non_empty_value(value): return str(value) != '-' and str(value) != '' and not pd.isnull(value)


def non_empty_list(values): return list(filter(non_empty_value, values))


def _get_id(value):
    values = str(value).split('/')
    return values[1] if len(values) == 2 else values[0]


GET_BY_COL = {
    'id': _get_id
}


def clean_values(data, column: str):
    values = map(GET_BY_COL.get(column, lambda v: v), data[column].values) if column in data else []
    return non_empty_list(values)


def _get_name(value): return unidecode(value)


FORMAT_BY_COL = {
    'name': _get_name
}


def _format_value(value, column: str): return FORMAT_BY_COL.get(column, lambda v: v)(value).strip().lower()


def find_duplicates(column: str):
    def get_values(folder: str):
        def get_all(filename: str):
            try:
                filepath = f"{folder}/{filename}"
                data = pd.read_csv(f"{DATA_DIR}/{filepath}")
                return filepath.replace('.csv', '.xlsx'), clean_values(data, column)
            except Exception as e:
                raise Exception(str(e), folder, filename)

        return get_all


    def get_folder_values(folder: str):
        files = list(filter(lambda file: file.endswith('.csv'), os.listdir(f"{DATA_DIR}/{folder}")))
        return list(map(get_values(folder), files))


    folders = list(filter(os.path.isdir, os.listdir(DATA_DIR)))
    values_by_file = list(reduce(lambda prev, curr: prev + get_folder_values(curr), folders, []))

    mapping = {}
    for filename, values in values_by_file:
        for value in values:
            value_key = _format_value(value, column)
            mapping[value_key] = mapping.get(value_key, []) + [filename]
    return mapping


def exec_column(column: str):
    code = ''
    for value, files in find_duplicates(column).items():
        if len(files) > 1:
            ff = '\n\t'.join(files)
            code += f"\nfound duplicate {column}: '{value}' in the following files:\n\t{ff}"
    return code


exit_code = '\n'.join(map(exec_column, [
    'name',
    'id',
    'openLCAId'
])).strip()

sys.exit(0 if exit_code == '' else exit_code)
