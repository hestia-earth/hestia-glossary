import os
import sys
import argparse
from functools import reduce
from openpyxl import load_workbook, Workbook
from openpyxl.cell import Cell

EXTENSION = '.xlsx'
BASE_DIR = './'
EXCLUDED_FOLDERS = ['csv', 'data', 'gadm', 'lookups', 'envs', 'scripts']


parser = argparse.ArgumentParser(description='Check formatting of Excel files')
parser.add_argument('--filename-filter', type=str,
                    help='Restrict files to convert by name. Skip to convert all files.')
args = parser.parse_args()


def flatten(values: list): return list(reduce(lambda x, y: x + (y if isinstance(y, list) else [y]), values, []))


def valid_file(file: str):
    return all([
        file.endswith(EXTENSION),
        not args.filename_filter or args.filename_filter in file
    ])


def get_files(folder: str):
    return list(map(
        lambda filename: os.path.join(folder, filename),
        list(filter(valid_file, os.listdir(os.path.join(BASE_DIR, folder))))
    ))


def get_all_files(files_to_ignore=[]):
    folders = list(filter(os.path.isdir, os.listdir(BASE_DIR)))
    files = list(reduce(lambda prev, curr: prev + get_files(curr), folders, []))
    for ignore in ['Geographies/region.xlsx'] + files_to_ignore:
        if ignore in files:
            files.remove(ignore)
    return files


def _validate_single_sheet(workbook: Workbook, *args):
    sheets = workbook.worksheets
    nb_sheets = len(sheets)
    return nb_sheets == 1 or f"File has {nb_sheets} sheets, only 1 allowed"


def _validate_zoom(workbook: Workbook, *args):
    sheet = workbook.worksheets[0]
    zoom = sheet.sheet_view.zoomScale
    return zoom == 85 or f"Zoom set to {zoom}%, must be 85%"


def _validate_sheet_name(workbook: Workbook, filename: str):
    sheet = workbook.worksheets[0]
    sheet_name = sheet.title
    # sheet name limit is 31
    return sheet_name in filename or f"Sheet name is '{sheet_name}', must be '{filename}' (or a substring of it)"


def _validate_active_cell(workbook: Workbook, *args):
    sheet = workbook.worksheets[0]
    cell = sheet.sheet_view.selection[0].activeCell
    return cell == 'A1' or f"Active cell on '{cell}', must be set on 'A1'"


def _validate_first_row_cell(cell: Cell):
    font = cell.font
    font_name = font.name
    font_size = font.sz
    # font_color = font.color.rgb
    is_bold = font.b is True
    errors = [
        font_name == 'Calibri' or f"Font for cell '{cell.coordinate}' is {font_name}, must be 'Calibri'",
        int(font_size) == 11 or f"Font for cell '{cell.coordinate}' size is {font_size}, must be 11",
        is_bold or f"Font for cell '{cell.coordinate}' must be bold"
    ]
    return [err for err in errors if err is not True]


def _validate_header_font(workbook: Workbook, *args):
    sheet = workbook.worksheets[0]
    first_row = list(sheet.rows)[0]
    cells = []
    for cell in first_row:
        cells.append(cell)
    return flatten(map(_validate_first_row_cell, cells))


ERROR_FUNCTIONS = [
    _validate_single_sheet,
    _validate_zoom,
    _validate_sheet_name,
    # _validate_active_cell,  # not working as always giving an error
    _validate_header_font
]


def _validate_file(filepath: str):
    filename = os.path.basename(filepath).split('.')[0]
    workbook = load_workbook(filepath, rich_text=True)
    errors = flatten(map(lambda func: func(workbook, filename), ERROR_FUNCTIONS))
    return [err for err in errors if err is not True]


def main():
    files = get_all_files()

    exit_code = ''

    for filepath in files:
        errors = _validate_file(filepath)
        if len(errors) > 0:
            err = '\n\t'.join(errors)
            exit_code += f"\nFormatting errors in '{filepath}':\n\t{err}"

    sys.exit(0 if exit_code == '' else exit_code)


if __name__ == "__main__":
    main()
