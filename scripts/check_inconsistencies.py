import os
import sys
from typing import List
from functools import reduce
import re
from concurrent.futures import ThreadPoolExecutor
import pandas as pd

DATA_DIR = 'csv'
SUBCLASS_PATTERN = re.compile("^subClassOf\.[\d]+\.id$")
properties_filepath = 'Properties/property.csv'


def is_emtpy(value): return str(value) == '-' or str(value) == '' or pd.isna(value)


def non_empty_value(value): return not is_emtpy(value)


def non_empty_list(values): return list(filter(non_empty_value, values))


def flatten(values: list): return list(reduce(lambda x, y: x + (y if isinstance(y, list) else [y]), values, []))


def get_files(folder: str):
    return list(map(
        lambda filename: os.path.join(folder, filename),
        list(filter(lambda file: file.endswith('.csv'), os.listdir(os.path.join(DATA_DIR, folder))))
    ))


def get_all_files(files_to_ignore=[]):
    folders = list(filter(os.path.isdir, os.listdir(DATA_DIR)))
    files = list(reduce(lambda prev, curr: prev + get_files(curr), folders, []))
    for ignore in ['Geographies/region.csv'] + files_to_ignore:
        files.remove(ignore)
    return files


def get_all_ids():
    files = get_all_files()
    return reduce(lambda prev, curr: prev + pd.read_csv(os.path.join(DATA_DIR, curr))['id'].tolist(), files, [])


def check_subClassOf(files):
    def get_file_ids(filename: str):
        filepath = os.path.join(DATA_DIR, filename)
        src_filepath = filepath.replace('.csv', '.xlsx')

        try:
            df = pd.read_csv(filepath)
            subclasses = [col for col in df if col.startswith('subClassOf')]
            columns = ['id']
            columns.extend(subclasses)

            for col in subclasses:
                if not SUBCLASS_PATTERN.search(col):
                    raise Exception(f"column {col} is incorrect. From: {', '.join(subclasses)}")

            # check we dont use the same id on subClass
            parent_ids = []
            for index, row in df[columns].iterrows():
                id = row['id']
                for col in subclasses:
                    sub_id = row[col]
                    if sub_id == id:
                        parent_ids.append(sub_id)

            # check that every subClass points to a term
            missing_ids = []
            for col in subclasses:
                missing = df[~df[col].isin(df['id'])][['id', col]]
                missing_subclass_ids = non_empty_list(missing[col].unique())
                missing_ids.extend(missing_subclass_ids)


            # check that there is no "hole" in the array
            missing_array_item_ids = []
            for _index, row in df.iterrows():
                last_index = -1
                for index in range(len(subclasses)):
                    col = subclasses[index]
                    if is_emtpy(row[col]) and index > last_index + 1:
                        missing_array_item_ids.append(row['id'])
                        break
                    last_index = index


            return (src_filepath, parent_ids, missing_ids, missing_array_item_ids)
        except Exception as e:
            raise Exception(str(e), src_filepath)

    with ThreadPoolExecutor() as executor:
        return list(executor.map(get_file_ids, files))


def check_defaultProperties_term():
    properties = pd.read_csv(f"{DATA_DIR}/{properties_filepath}")['id'].values

    def check_file(filename: str):
        filepath = os.path.join(DATA_DIR, filename)
        df = pd.read_csv(filepath)
        columns = [col for col in df if col.startswith('defaultProperties') and col.endswith('term.id')]
        values = set().union(*df[columns].values.tolist())
        ids = [str(x) for x in values if non_empty_value(x) and str(x) not in properties]
        return filepath.replace('.csv', '.xlsx'), ids

    files = get_all_files([properties_filepath])
    with ThreadPoolExecutor() as executor:
        return list(executor.map(check_file, files))


def check_defaultProperties_key(term_ids: list):
    def check_file(filename: str):
        filepath = os.path.join(DATA_DIR, filename)
        df = pd.read_csv(filepath)
        columns = [col for col in df if col.startswith('defaultProperties') and col.endswith('key.id')]
        values = set().union(*df[columns].values.tolist())
        ids = [str(x) for x in values if non_empty_value(x) and str(x) not in term_ids]
        return filepath.replace('.csv', '.xlsx'), ids

    files = get_all_files([properties_filepath])
    with ThreadPoolExecutor() as executor:
        return list(executor.map(check_file, files))


def main(args: List[str]):
    exit_code = ''

    files = [args[0]] if len(args) > 0 else get_all_files()
    term_ids = get_all_ids()

    for filepath, parent_ids, missing_ids, missing_array_item_ids in check_subClassOf(files):
        if len(parent_ids) > 0:
            iids = '\n\t'.join(parent_ids)
            exit_code += f"\nfound {len(parent_ids)} inconsistent subClassOf in '{filepath}' for the following ids:\n\t{iids}"

        if len(missing_ids) > 0:
            iids = '\n\t'.join(list(map(str, missing_ids)))
            exit_code += f"\nfound {len(missing_ids)} non-existing subClassOf.id in '{filepath}':\n\t{iids}"

        if len(missing_array_item_ids) > 0:
            iids = '\n\t'.join(missing_array_item_ids)
            exit_code += f"\nfound {len(missing_array_item_ids)} missing array item for subClassOf.id in '{filepath}' for the following ids:\n\t{iids}"

        if exit_code != '':
            exit_code += '\nPlease verify that the subClassOf.id points to a different term.id in this file.'

    for filepath, ids in check_defaultProperties_term():
        if len(ids) > 0:
            iids = '\n\t'.join(ids)
            exit_code += f"\nfound {len(ids)} non-existing defaultProperties in '{filepath}' for the following terms:\n\t{iids}"

    for filepath, ids in check_defaultProperties_key(term_ids):
        if len(ids) > 0:
            iids = '\n\t'.join(ids)
            exit_code += f"\nfound {len(ids)} non-existing defaultProperties in '{filepath}' for the following keys:\n\t{iids}"

    sys.exit(0 if exit_code == '' else exit_code)


if __name__ == "__main__":
    main(sys.argv[1:])
