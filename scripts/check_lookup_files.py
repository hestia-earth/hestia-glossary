import os
import sys
from functools import reduce
from concurrent.futures import ThreadPoolExecutor
import pandas as pd

EXTENSION = '.xlsx'
LOOKUP_DIR = './lookups'
SUFFIX = '-lookup.csv'
COL_ID = 'term.id'
BASE_DIR = './'
EXCLUDED_FOLDERS = ['csv', 'data', 'gadm', 'envs', 'scripts']


def non_empty_value(value): return str(value) != '-' and str(value) != '' and pd.notna(value)


def non_empty_list(values): return list(filter(non_empty_value, values))


def flatten(values: list): return list(reduce(lambda x, y: x + (y if isinstance(y, list) else [y]), values, []))


def _file_exists(path: str):
    try:
        return os.path.exists(path)
    except Exception:
        return False


def valid_file(filepath: str):
    return _file_exists(filepath) and all([not filepath.startswith(folder) for folder in EXCLUDED_FOLDERS])


def get_folders(): return list(filter(lambda f: f not in EXCLUDED_FOLDERS and os.path.isdir(f), os.listdir(BASE_DIR)))


def get_files(folder: str):
    return list(map(
        lambda filename: os.path.join(folder, filename),
        list(filter(lambda file: file.endswith(SUFFIX), os.listdir(os.path.join(BASE_DIR, folder))))
    ))


def get_all_files():
    folders = get_folders()
    return list(reduce(lambda prev, curr: prev + get_files(curr), folders, []))


def get_files_from_termTypes(term_types: list):
    files = get_all_files()
    return  flatten([
        list(filter(lambda f: t in f.split('/')[1].split('-'), files)) for t in term_types
    ])


def _full_filepath(filename: str):
    folders = get_folders()
    for folder in folders:
        for file in os.listdir(os.path.join(BASE_DIR, folder)):
            if file == filename:
                return os.path.join(BASE_DIR, folder, file)
    return None


def _has_lookup_value(filepath: str, lookup: str):
    df = pd.read_csv(filepath) if _file_exists(filepath) else None
    return lookup in df.columns if df is not None else False


def _check_lookup(level: str, all_filepath: str, all_values: list, current_filepath: str, current_values: list):
    if all([
        # skip checking for region as we only add the data on regions we have
        'region' not in all_filepath,
        # skip as auto-generated
        'property-lookup' not in current_filepath
    ]):
        missing = []
        for value in all_values:
            if value not in current_values:
                missing.append(str(value))

        has_missing = len(missing) > 0 and len(missing) != len(all_values)
        if has_missing:
            iids = '\n\t'.join(missing)
            print(f"{level}: missing value present in '{all_filepath} but not in '{current_filepath}':\n\t{iids}")
    else:
        has_missing = False

    extra = []
    for value in current_values:
        if value not in all_values:
            extra.append(str(value))

    has_extra = len(extra) > 0 and len(extra) != len(current_values)
    if has_extra:
        iids = '\n\t'.join(extra)
        print(f"{level}: extra value in '{current_filepath}' but not present in '{all_filepath}':\n\t{iids}")

    return has_missing or has_extra


def _check_lookup_rows(filepath: str, df: pd.DataFrame, src_filepath: str, level: str):
    src_df = pd.read_excel(src_filepath, index_col=COL_ID)
    all_terms = list(src_df.index.values)
    current_terms = list(df.index.values)

    return _check_lookup(
        level=level,
        all_filepath=src_filepath, all_values=all_terms,
        current_filepath=filepath, current_values=current_terms
    )


def _check_lookup_columns(filepath: str, df: pd.DataFrame, src_filepath: str, level: str):
    src_df = pd.read_excel(src_filepath, index_col=COL_ID)
    all_terms = list(src_df.index.values)
    current_terms = list(df.columns.values)

    return _check_lookup(
        level=level,
        all_filepath=src_filepath, all_values=all_terms,
        current_filepath=filepath, current_values=current_terms
    )


def _check_lookup_columns_rows(filepath: str, df: pd.DataFrame, src_filepath: str, col_name: str, level: str):
    src_df = pd.read_csv(src_filepath, index_col=0)
    all_values = non_empty_list(list(src_df[col_name].unique()))
    current_values = list(df.columns.values)

    return _check_lookup(
        level=level,
        all_filepath=src_filepath, all_values=all_values,
        current_filepath=filepath, current_values=current_values
    )


def check_lookup(filename: str):
    print('Checking', filename)
    filepath = os.path.join(BASE_DIR, filename)

    parts = filename.split('-')
    # remove 'lookup' part
    parts.pop()
    row_term_type = parts[0]

    src_filepath = os.path.join(BASE_DIR, f"{row_term_type}{EXTENSION}")

    is_emission_lookup = 'region-emission' in filename

    col_term_type = parts[1] if len(parts) >= 2 else None
    term_type_filepath = _full_filepath(f"{col_term_type}{EXTENSION}")
    check_term_type = _file_exists(term_type_filepath)

    # 3rd part can be a lookup on the 2nd part, like "cropGroupingFaostatProduction" on "crop"
    col_term_type_col = parts[2] if len(parts) >= 3 else None
    term_type_col_filepath = os.path.join(LOOKUP_DIR, f"{col_term_type}.csv")
    check_term_type_col = col_term_type_col is not None and _has_lookup_value(term_type_col_filepath, col_term_type_col)

    try:
        lookup_df = pd.read_csv(filepath, index_col=COL_ID)
    except Exception:
        lookup_df = None

    return [
        {
            'filepath': filepath,
            'type': 'check_lookup_rows',
            'result': _check_lookup_rows(
                filepath, lookup_df, src_filepath, level='Error'
            ) if _file_exists(src_filepath) else False,
            'level': 'error'
        },
        {
            'filepath': filepath,
            'type': 'check_lookup_columns',
            'result': _check_lookup_columns(
                filepath, lookup_df, term_type_filepath, level='Error' if is_emission_lookup else 'Warning'
            ) if check_term_type and not check_term_type_col else False,
            'level': 'error' if is_emission_lookup else 'warning'
        },
        {
            'filepath': filepath,
            'type': 'check_lookup_columns_rows',
            'result': _check_lookup_columns_rows(
                filepath, lookup_df, term_type_col_filepath, col_term_type_col,
                level='Error' if is_emission_lookup else 'Warning'
            ) if check_term_type_col else False,
            'level': 'error' if is_emission_lookup else 'warning'
        }
    ] if lookup_df is not None else []


def check_lookups(files: list):
    with ThreadPoolExecutor() as executor:
        return flatten(executor.map(check_lookup, files))


def match_level(values: list, level: str):
    return any([v.get('level') == level for v in values if v.get('result', False)])


def main(args: list):
    files = non_empty_list(args[0].split('\n') if len(args) == 1 else args if len(args) > 1 else get_all_files())
    diffs = list(filter(valid_file, files))
    print(diffs)
    lookup_files = list(filter(lambda f: f.endswith(SUFFIX), diffs))
    source_files = list(filter(lambda f: f.endswith(EXTENSION), diffs))
    term_types = [src.split('/')[1].replace(EXTENSION, '') for src in source_files]
    additional_lookups = get_files_from_termTypes(term_types)
    all_lookups = list(set(lookup_files + additional_lookups))

    results = check_lookups(all_lookups)
    has_warnings = match_level(results, 'warning')
    has_errors = match_level(results, 'error')

    exit_code = 1 if has_errors else 2 if has_warnings else 0
    sys.exit(exit_code)


if __name__ == "__main__":
    main(sys.argv[1:])
