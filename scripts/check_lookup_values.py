import os
import sys
from concurrent.futures import ThreadPoolExecutor
from functools import reduce

import pandas as pd
from hestia_earth.schema import TermTermType, SiteSiteType

DATA_DIR = 'csv'
LOOKUP_DIR = 'lookups'
IGNORE_VALUES = ['all', 'none', '-']

ID_COLUMNS = [
    'defaultModelId',
    'productTermIdsAllowed', 'inputTermIdsAllowed',
    'complementaryTermIds',
    'sensitivityAlternativeTerms',
    'fuelUse',
    'inputProductionGroupId',
    'liveAnimalTermId',
    'liveweightPerHead',
    'weightAtMaturity',
    'animalProductId',
    'grazedPastureGrassInputId',
    'animalProductGroupingEquivalentProperty',
    'animalProductGroupingFAOEquivalent',
    'excretaKgMassTermId', 'excretaKgNTermId', 'excretaKgVsTermId',
    'allowedExcretaKgMassTermIds', 'allowedExcretaKgNTermIds', 'allowedExcretaKgVsTermIds',
    'recommendedExcretaKgMassTermIds', 'recommendedExcretaKgNTermIds', 'recommendedExcretaKgVsTermIds',
    'digestibility', 'digestibleEnergy',
    'primaryMeatProductFaoProductionTermId',
    'primaryMeatProductFaoPriceTermId',
    'allowedRiceTermIds',
    'landCoverTermId',
    'feedipediaConversionTermId', 'averagePropertiesTermIds',
    'milkYieldPracticeTermIds',
    'milkYieldPracticeTermId',
    'allowedLiveAnimalTermIds',
    'allowedAnimalProductTermIds',
    'correspondingSeedTermIds',
    'ipcc2019MilkYieldPerAnimalTermId',
    'correspondingWaterRegimeTermId',
    'linkedImpactAssessmentTermId',
    'pefTerm-methodModel-whiteList-v3-1'
]

SCHEMA_CHECKS = [
    (TermTermType, ['termTypesAllowed', 'productTermTypesAllowed', 'inputTermTypesAllowed']),
    (SiteSiteType, ['siteTypesAllowed'])
]


class ValuesError(Exception):
    def __init__(self, message = 'multiple-values'):
        super().__init__(message)


def _enum_to_list(enum): return [e.value for e in enum]


def _flatten(values: list): return list(reduce(lambda x, y: x + (y if isinstance(y, list) else [y]), values, []))


def _is_emtpy(value): return str(value) == '-' or str(value) == '' or str(value) == 'nan' or pd.isna(value)


def _non_empty_value(value): return not _is_emtpy(value)


def _non_empty_list(values): return list(filter(_non_empty_value, values))


def _get_files(folder: str):
    return list(map(
        lambda filename: os.path.join(folder, filename),
        list(filter(lambda file: file.endswith('.csv'), os.listdir(folder)))
    ))


def _get_all_files(folder: str):
    folders = [os.path.join(folder, f) for f in os.listdir(folder) if os.path.isdir(os.path.join(folder, f))]
    return list(reduce(lambda prev, curr: prev + _get_files(curr), folders, []))


def _get_all_ids():
    files = _get_all_files(DATA_DIR)
    return reduce(lambda prev, curr: prev + pd.read_csv(curr)['id'].tolist(), files, [])


def _extract_values(value: str, allow_multiple_values: bool = False):
    # handle "id1:10;id2:100"
    values = value.split(';') if ';' in value else [value]
    values = [v.split(':')[0] for v in values]
    if len(values) > 1 and not allow_multiple_values:
        raise ValuesError()
    return values


def _check_lookup_values(files: list, allowed_values: list, column: str, ignore_values=IGNORE_VALUES):
    allow_multiple_values = any([
        'Term' not in column and 'Id' not in column,
        'Ids' in column,
        'Types' in column,
        'Terms' in column
    ])

    def check_file(filepath: str):
        df = pd.read_csv(filepath)
        columns = [col for col in df if col == column]
        error = ''
        values = []

        try:
            values = _non_empty_list(set(_flatten([
                _extract_values(str(x), allow_multiple_values) for x in set().union(*df[columns].values.tolist())
            ])))
        except ValuesError:
            error = f"You have used multiple values in {column} lookup, consider renaming to {column.replace('Id', 'Ids')} or use a single value instead."

        results = [str(x) for x in values if str(x) not in (allowed_values + ignore_values)]
        return filepath, results, error

    with ThreadPoolExecutor() as executor:
        return list(executor.map(check_file, files))


def _log_values(values: list): return '\n- ' + '\n- '.join([f"\"{v}\"" for v in values])


def main():
    files = _get_files(LOOKUP_DIR)

    exit_code = ''

    # validate Term `@id`
    term_ids = _get_all_ids()
    for column in ID_COLUMNS:
        for filepath, values, error in _check_lookup_values(files, term_ids, column):
            if error:
                exit_code += f"\n{error}"
            elif len(values) > 0:
                txt = _log_values(values)
                exit_code += f"\nFound {len(values)} non-existing values for '{column}' in '{filepath}':{txt}"

    # validate schema mappings
    for enum, columns in SCHEMA_CHECKS:
        for column in columns:
            for filepath, values, error in _check_lookup_values(files, _enum_to_list(enum), column):
                if error:
                    exit_code += f"\n{error}"
                elif len(values) > 0:
                    txt = _log_values(values)
                    exit_code += f"\nFound {len(values)} non-existing values for '{column}' in '{filepath}':{txt}"

    sys.exit(0 if exit_code == '' else exit_code)


if __name__ == "__main__":
    main()
