import os
import argparse
import re
from functools import reduce
from concurrent.futures import ThreadPoolExecutor
import pandas as pd

EXTENSION = '.xlsx'
BASE_DIR = './'
DEST_DIR = 'data'
LOOKUP_SUFFIX = '-lookup'
EXCLUDED_FOLDERS = ['csv', 'data', 'envs', 'scripts']
UPLOAD_TERMS_LIMIT = 5000


parser = argparse.ArgumentParser(description='Convert Excel files to CSV')
parser.add_argument('--dest-folder', type=str, default=DEST_DIR,
                    choices=[DEST_DIR, 'csv', 'lookups'],
                    help='Folder to generate the files.')
parser.add_argument('--filename-filter', type=str,
                    help='Restrict files to convert by name. Skip to convert all files.')
args = parser.parse_args()


def mkdirs(dest: str):
    os.makedirs(dest, exist_ok=True)


def flatten(values: list): return list(reduce(lambda x, y: x + (y if isinstance(y, list) else [y]), values, []))


def valid_file(file: str):
    return all([
        file.endswith(EXTENSION),
        (args.filename_filter and args.filename_filter in file) or not LOOKUP_SUFFIX in file
    ])


def ignore_columns(df: pd.DataFrame, keep_lookups = False):
    try:
        return df.drop(columns=df.filter(regex=("\-.*" if keep_lookups else "(\-.*|^lookups\.)")).columns).dropna(axis=0, how='all').dropna(axis=1, how='all')
    except KeyError:
        return df


def clean_cells(df: pd.DataFrame):
    return df.replace('\n', '', regex=True).replace('\t', '', regex=True).apply(lambda x: x.apply(lambda y: y.strip() if type(y) == type('') else y), axis=0)


def convert_for_indexing(dest_folder: str, src_folder: str, filename: str):
    src_file = f"{src_folder}/{filename}{EXTENSION}"
    dest_file = f"{dest_folder}/{filename}.csv"
    float_formatting = '%.8f' if filename == 'region' else None
    clean_cells(ignore_columns(pd.read_excel(src_file))).to_csv(dest_file, index=None, header=True, float_format=float_formatting)
    csvfile = open(dest_file, 'r').read().splitlines()
    headers = csvfile[0] + ',term.termType\n'
    del csvfile[0]

    def with_term_type(lines):
        # remove float_format 8 digits on integers
        return list(map(lambda l: re.sub(r'([0]{8,}[\d]{1})([^\d])', r'\2', f"{l.replace('.00000000', '')},{filename}\n"), lines))

    part = 1
    for i in range(len(csvfile)):
        if i % UPLOAD_TERMS_LIMIT == 0:
            filepath = f"{dest_folder}/{filename}-{part}.csv"
            open(filepath, 'w+').writelines(headers)
            open(filepath, 'a+').writelines(with_term_type(csvfile[i:i+UPLOAD_TERMS_LIMIT]))
            part += 1
    os.remove(dest_file)


def convert_for_download(dest_folder: str, src_folder: str, filename: str):
    src_file = f"{src_folder}/{filename}{EXTENSION}"
    dest_file = f"{dest_folder}/{src_folder}/{filename}.csv"
    mkdirs(f"{dest_folder}/{src_folder}")
    data = pd.read_excel(src_file)
    data.rename(columns=lambda x: x[5:] if x.startswith('term.') else x, inplace=True)
    ignore_columns(data, True).to_csv(dest_file, index=None, header=True)


CONVERTER_DIR = {
    DEST_DIR: convert_for_indexing,
    'csv': convert_for_download
}


def convert_file(data):
    dest_folder = data.get('dest_folder')
    return CONVERTER_DIR[dest_folder](dest_folder, data.get('src_folder'), data.get('file'))


def convert_folder(dest_folder: str):
    def convert(folder: str):
        folderpath = os.path.join(BASE_DIR, folder)
        if os.path.isdir(folderpath):
            files = list(filter(valid_file, os.listdir(folderpath)))
            return list(map(lambda f: { 'dest_folder': dest_folder, 'src_folder': folderpath, 'file': f.replace(EXTENSION, '') }, files))
        return []
    return convert


def main():
    mkdirs(args.dest_folder)
    folders = list(filter(lambda f: f not in EXCLUDED_FOLDERS, os.listdir(BASE_DIR)))
    files = flatten(list(map(convert_folder(args.dest_folder), folders)))

    with ThreadPoolExecutor() as executor:
        return list(executor.map(convert_file, files))


if __name__ == "__main__":
    main()
