# parses all excel files and extract lookup data
# correct cases per dataState are described in `CONTRIBUTING.MD` file.
import sys
import os
import argparse
import re
import numpy as np
import pandas as pd
import traceback
from functools import reduce
from concurrent.futures import ThreadPoolExecutor

EXTENSION = '.xlsx'
BASE_DIR = './'
DEST_DIR = 'lookups'
EXCLUDED_FOLDERS = ['csv', 'data', 'envs', 'scripts']


parser = argparse.ArgumentParser(description='Convert Excel files to lookups')
parser.add_argument('--dest-folder', type=str, default=DEST_DIR,
                    help='Folder to generate the files.')
parser.add_argument('--folder', type=str, nargs='+', default=[],
                    help='Search in folders.')
parser.add_argument('--filename-filter', type=str,
                    help='Restrict files to convert by name. Skip to convert all files.')
args = parser.parse_args()


def mkdirs(dest: str):
    os.makedirs(dest, exist_ok=True)


def _default_folders(): return list(filter(lambda f: f not in EXCLUDED_FOLDERS, os.listdir(BASE_DIR)))


def _valid_file(file: str):
    return all([
        file.endswith(EXTENSION),
        not args.filename_filter or args.filename_filter in file
    ])


def flatten(values: list): return list(reduce(lambda x, y: x + (y if isinstance(y, list) else [y]), values, []))


def is_nan(v: str, allowDash=True):
    return v is None or any([
        isinstance(v, str) and (v == 'nan' or v == '' or (allowDash and v == '-')),
        isinstance(v, float) and np.isnan(v)
    ])


def filter_nan(values: list, allowDash=True): return [v for v in values if not is_nan(v, allowDash)]


def fix_9(match: re.Match):
    digit, *_ = match.groups()
    end = match.groups()[-1]
    return f"{int(digit) + 1}{end if 'e' in end else ''}"


def fix_float_rounding(value):
    # remove `(.)00000008`
    val = re.sub(r'(\.?)[0]{8,}[\d]{1}$', '', f"{value}")
    # handle `1.999999995`` round to `2`
    # handle `5.599999995` round to `5.6`
    # handle `5.599999995e-05` round to `5.6e-05`
    val = re.sub(r'([\d]{1})(\.?)([9]{8,}[\d]{1})($|[e])', fix_9, val)
    return val


def fix_value(value: str):
    if is_nan(value):
        return ''
    if isinstance(value, str):
        if value.lower() == 'false' or value.lower() == 'true':
            return value.lower()
        return fix_float_rounding(value)
    if isinstance(value, bool):
        return f"{value}".lower()
    if isinstance(value, float):
        return fix_float_rounding(value)
    return value


NO_DATA_STATE = 'unset'
DATA_STATE_VALID = {
    'complete': lambda name, value: all([not is_nan(name), not is_nan(value)]),
    'not required': lambda name, value: all([not is_nan(name), value == '-']),
    'requires validation': lambda name, value: all([not is_nan(name), value != '']),
    'missing': lambda name, value: all([not is_nan(name), is_nan(value, allowDash=False)]),
    # default state when none is assigned
    NO_DATA_STATE: lambda *args: True
}


def _get_extra_lookups(df: pd.DataFrame, key: str):
    columns = [col for col in df.columns.values if col.startswith('lookups.') and col.endswith(key)]
    columns = flatten([list(df[col.replace(f".{key}", '.name')].values) for col in columns])
    return [f"{col}-{key}" for col in columns if not is_nan(col)]


def _handle_extra_value(data: dict, row: pd.Series, index: int, key: str):
    name = row[f"lookups.{index}.name"]
    key_col = f"lookups.{index}.{key}"
    if key_col in row and not is_nan(name) and not is_nan(row[key_col]):
        col_name = f"{name}-{key}"
        data[col_name] = row[key_col]
    return data


def generate_lookup(dest_folder, src_folder, filename):
    src_file = f"{src_folder}/{filename}{EXTENSION}"
    print('Processing', src_file)
    dest_file = f"{dest_folder}/{filename}.csv"

    errors = []

    try:
        df = pd.read_excel(src_file, dtype=str, engine='openpyxl')

        names = filter_nan(np.unique(df.astype(str).filter(regex=r'^lookups\.[\d]+\.name$').values))
        total_lookups = len(names)

        columns = [col for col in df.columns.values if col.startswith('lookups.') and col.endswith('.name')]
        nb_lookups = len(columns)

        for col in columns:
            values = filter_nan(df[col].unique())
            if len(values) > 1:
                vv = ', '.join(values)
                errors.append(
                    f"There are different names used in the {col} column. Check for misspellings or move different data into a new column: {vv}.")

        # add the additional lookups
        names = names + filter_nan(np.unique(
            _get_extra_lookups(df, 'sd') + _get_extra_lookups(df, 'min') + _get_extra_lookups(df, 'max')
        ))

        data = {}

        if nb_lookups > 0:
            for _i, row in df.iterrows():
                term_id = row['term.id']
                # skip empty rows
                if isinstance(term_id, str) and not is_nan(term_id):
                    lookup_data = reduce(lambda prev, curr: {**prev, curr: ''}, names, {})

                    for i in range(0, nb_lookups):
                        name = row[f"lookups.{i}.name"]
                        value = row[f"lookups.{i}.value"]
                        data_state = row[f"lookups.{i}.dataState"] if f"lookups.{i}.dataState" in row else NO_DATA_STATE

                        if data_state == NO_DATA_STATE and any([not is_nan(name), not is_nan(value)]):
                            errors.append(f"dataState is missing for '{term_id}': {data_state} (lookups.{i}.dataState)")

                        # data state spelling error
                        if data_state not in DATA_STATE_VALID:
                            errors.append(f"Unknown dataState for '{term_id}': {data_state} (lookups.{i}.dataState)")
                            continue
                        # unmatching condition for dataState
                        if not DATA_STATE_VALID.get(data_state)(name, value):
                            errors.append(f"Incorrect lookup values for '{term_id}' ({data_state}): lookups.{i}")
                            continue

                        if not is_nan(name):
                            lookup_data[name] = fix_value(value)

                        lookup_data = _handle_extra_value(lookup_data, row, i, 'sd')
                        lookup_data = _handle_extra_value(lookup_data, row, i, 'min')
                        lookup_data = _handle_extra_value(lookup_data, row, i, 'max')

                    # skip rows with no lookup data
                    if len(filter_nan(lookup_data.values())) > 0:
                        data['term.id'] = data.get('term.id', []) + [term_id]
                        data['term.name'] = data.get('term.name', []) + [row['term.name']]
                        for col in sorted(names):
                            data[col.strip()] = data.get(col.strip(), []) + [lookup_data.get(col, '')]

        # skip empty lookups
        if len(data.keys()) > 1:
            new_df = pd.DataFrame(data)
            new_df.replace('\n', '', regex=True).replace('\t', '', regex=True).dropna(
                thresh=2).to_csv(dest_file, index=False, float_format='%.8f')
    except Exception:
        errors.append(traceback.format_exc())

    if len(errors) > 0:
        return src_file, '\n\t'.join(errors)

    return src_file, None


def convert_file(data):
    dest_folder = data.get('dest_folder')
    return generate_lookup(dest_folder, data.get('src_folder'), data.get('file'))


def convert_folder(folder: str):
    folderpath = os.path.join(BASE_DIR, folder)
    if os.path.isdir(folderpath):
        files = list(filter(_valid_file, os.listdir(folderpath)))
        return list(map(lambda f: {
            'dest_folder': folder if '-lookup' in f else args.dest_folder,
            'src_folder': folderpath,
            'file': f.replace(EXTENSION, '')
        }, files))
    return []


def main():
    mkdirs(args.dest_folder)

    folders = args.folder or _default_folders()

    files = flatten(list(map(convert_folder, folders)))

    with ThreadPoolExecutor() as executor:
        errors = list(executor.map(convert_file, files))

    exit_code = ''
    for filename, error in errors:
        if error:
            exit_code += f"\nErrors in '{filename}':\n\t{error}"
    sys.exit(0 if exit_code == '' else exit_code)


if __name__ == "__main__":
    main()
