# Electricity mix emissions data extractor

This script creates a lookup CSV from Ember energy sources.

The result is stored in `Geographies/region-ember-energySources-lookup.csv`. 

When running the script, if search terms in the Ember page are missing, warning messages will be shown and the script 
will stop. The script will need to be updated to match the Ember website and re-run.

## Usage

- Run `python scripts/electricityMix/run.py` first to generate lookup tables
