import csv
import datetime
import os
from collections import defaultdict
from typing import List, Tuple, Dict

import requests
from bs4 import BeautifulSoup

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
OUTPUT_FOLDER = os.path.join(CURRENT_DIR, "..", "..", "Geographies")
EMBER_ECOINVENT_MAPPING_FILE = os.path.join(
    CURRENT_DIR, "..", "..", "Inputs & Products", "ember-ecoinvent-mapping-lookup.csv"
)
OUTPUT_FILENAME = "region-ember-energySources-lookup"
CSV_EXT = ".csv"
MD_EXT = ".md"
SOURCE_NAME = "Ember Electricity Yearly Data"
BASE_URL = "https://ember-climate.org/"
PAGE_FOLDER = "data-catalogue/yearly-electricity-data/"
UPDATED_CLASS = "b-header-dataset-single__updated-on"
DOWNLOAD_SEARCH_TERM = "download"
FILE_SEARCH_TERM = "yearly_full"
REGION_PREFIX = "GADM-"
COUNTRY_MAPPING_LOOKUP = os.path.join(CURRENT_DIR, 'country-mapping.csv')
EMBER_MATCH_DICT = {
    "Area type": "Country",
    "Category": "Electricity generation",
    "Subcategory": "Fuel",
    "Unit": "%"
}
DEBUG = True


def _get_download_url_and_update_date() -> Tuple[str, str]:
    """Search PAGE_URL for download URL and text stating update date. Return both."""
    page_url = BASE_URL + PAGE_FOLDER
    r = requests.get(page_url)
    soup = BeautifulSoup(r.text, "html.parser")
    elems = soup.body.find_all()
    url_end = [
        e.attrs["href"]
        for e in elems
        if e.name == "a"
        and DOWNLOAD_SEARCH_TERM in e.attrs
        and FILE_SEARCH_TERM in e.attrs[DOWNLOAD_SEARCH_TERM]
    ]
    if not url_end:
        raise ValueError(f"{FILE_SEARCH_TERM} or {DOWNLOAD_SEARCH_TERM} not found in {page_url}")
    url_full = "/".join(u.strip("/") for u in [BASE_URL, url_end[0]])
    updated_class = soup.body.find(class_=UPDATED_CLASS)
    if updated_class is None:
        raise ValueError(f"{UPDATED_CLASS} not found in {page_url}")

    if DEBUG:
        print(f"Retrieved: {url_full=}, {updated_class.text=}")

    return url_full, updated_class.text


def _download_file(url: str) -> List[Dict]:
    with requests.Session() as s:
        download = s.get(url)
        decoded_content = download.content.decode("utf-8")
        cr = csv.DictReader(decoded_content.splitlines(), delimiter=",")
        list_of_dicts = list(cr)

    if DEBUG:
        print(f"Downloaded file with {len(list_of_dicts)} rows.")

    return list_of_dicts


def _get_ember_energy_sources() -> set:
    """Get the 'ember' column from the lookup file."""
    result = set()
    with open(EMBER_ECOINVENT_MAPPING_FILE, "r") as mapping_file:
        reader = csv.DictReader(mapping_file)
        for row_dict in reader:
            result.add(row_dict["ember"])

    return result


def is_empty(value): return str(value) == "-" or str(value) == ""


def _hestia_region_id(country_code: str) -> str:
    region_mapping = dict()
    with open(COUNTRY_MAPPING_LOOKUP, "r") as country_lookup:
        reader = csv.DictReader(country_lookup)
        for row in reader:
            region_mapping[row["ember_code"]] = row["term.id"]

    if country_code in region_mapping:
        return region_mapping[country_code]

    return REGION_PREFIX + country_code


def _has_relevant_values(row_dict: Dict, energy_sources: set) -> bool:
    if is_empty(row_dict["Country code"]):
        return False

    for field, value in EMBER_MATCH_DICT.items():
        if field not in row_dict:
            return False
        if row_dict[field] != value:
            return False

    return row_dict["Variable"] in energy_sources


def _fill_default_values(data_dict: dict, unique_values: List, default: str = "-") -> Dict:
    result_dict = {k: default for k in unique_values}
    result_dict.update(data_dict)
    return result_dict


def _region_row_from_extracted_data(region_id: str, data_dict: dict, energy_sources: set, years: List) -> dict:
    """Create outline region dictionary without years and values."""
    output = {"term.id": region_id}
    for s in energy_sources:
        dict_with_defaults = _fill_default_values(data_dict=data_dict[s], unique_values=years)
        output[s] = ";".join(
            [f"{year}:{value}" if not is_empty(value) else f"{year}:-" for year, value in dict_with_defaults.items()]
        )

    return output


def _extract_data(rows: List[Dict]) -> List[Dict]:
    """
    term.id,Bioenergy,Coal...
    GADM-AFG,2000:0.01;2001:0.02,2000:0.02;...
    """
    regions_dict = defaultdict(lambda: defaultdict(dict))
    energy_sources = _get_ember_energy_sources()
    years_set = set()

    for row_dict in rows:
        if not _has_relevant_values(row_dict=row_dict, energy_sources=energy_sources):
            continue

        region_id = _hestia_region_id(row_dict["Country code"])
        years_set.add(row_dict["Year"])
        # For example: regions_dict["GADM-AFG"]["Hydro"]["2004"] = 70.89
        regions_dict[region_id][row_dict["Variable"]][row_dict["Year"]] = row_dict["Value"]

    if DEBUG:
        print(f"Extracted data from file for {len(regions_dict.keys())} regions.")

    return [
        _region_row_from_extracted_data(
            region_id=r_id,
            data_dict=data_dict,
            energy_sources=energy_sources,
            years=sorted(list(years_set))
        )
        for r_id, data_dict in regions_dict.items()
    ]


def _save_csv_file(rows: List[Dict]):
    with open(os.path.join(OUTPUT_FOLDER, OUTPUT_FILENAME + CSV_EXT), "w") as csv_file:
        csv_writer = csv.DictWriter(csv_file, dialect=csv.excel, fieldnames=rows[0].keys())
        csv_writer.writeheader()
        csv_writer.writerows(rows)

    if DEBUG:
        print(f"Written file to: {os.path.join(OUTPUT_FOLDER, OUTPUT_FILENAME + CSV_EXT)} with {len(rows)} rows.")


def _write_md_file(updated_text: str):
    date_now = datetime.datetime.now().strftime("%Y-%m-%d")
    md_file_text = f"**Source:** [{SOURCE_NAME}]({BASE_URL + PAGE_FOLDER}), {updated_text}, Accessed {date_now}."
    with open(os.path.join(OUTPUT_FOLDER, OUTPUT_FILENAME + MD_EXT), "w", encoding="utf-8") as md_file:
        md_file.write(md_file_text)

    if DEBUG:
        print(f"Wrote Markdown file to: {os.path.join(OUTPUT_FOLDER, OUTPUT_FILENAME + MD_EXT)}")


download_url, text = _get_download_url_and_update_date()
_save_csv_file(_extract_data(_download_file(download_url)))
_write_md_file(text)
