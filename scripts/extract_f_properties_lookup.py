# extract properties from feedipedia
import sys
from functools import reduce
import argparse
from statistics import mean
import math
from datetime import date
import traceback
import requests
import pandas as pd
from bs4 import BeautifulSoup


PROPERTY_NAME = 'feedipediaName'
CONVERSION_COLUMN = 'feedipediaConversionEnum'
AVERAGE_PROPERTIES_COLUMN = 'averagePropertiesTermIds'
DM_PROP = 'Dry matter'
FETCH_COLUMNS = ['Avg', 'Min', 'Max', 'SD']
SKIP_COLS = ['', 'Unit', 'Nb']


parser = argparse.ArgumentParser(description='Extract feedipedia data from properties')
parser.add_argument('--input-file', type=str, required=True,
                    help='The path to the file containing the Terms. Example: "Inputs & Products/crop.xlsx"')
parser.add_argument('--skip-md', action='store_true',
                    help='Skip generating the markdown file.')
parser.add_argument('--term-id', type=str, nargs='+', default=[],
                    help='Select terms by their ids.')
args = parser.parse_args()


def non_empty_value(value): return str(value) != '-' and str(value) != '' and str(value) != '-' and pd.notna(value)


def non_empty_list(values: list): return list(filter(non_empty_value, values))


def float_precision(value): return "{:.5f}".format(float(value) if type(value) == str else value)


def flatten(values: list): return list(reduce(lambda x, y: x + (y if isinstance(y, list) else [y]), values, []))


def default_dm_value(term):
    value = term['term.defaultProperties.0.value']
    return float(value) if value and value != '-' else 0


def row_to_list(row):
    def td_content(td):
        try:
            return td.a.string.strip()
        except Exception:
            return td.string.strip() if td.string is not None else ''

    return [td_content(td) for td in row.find_all('td') if td is not None]


def clean_value(value):
    return float(value.replace('*', ''))


_CONVERSION_FORMULA = {
    'dmToFmSimple': lambda value, dm_value: float_precision(value * (dm_value / 100)),
    'dmToFm10FactorCorrection': lambda value, dm_value: float_precision(value * (dm_value / 100) / 10),
    'dmToFm10000FactorCorrection': lambda value, dm_value: float_precision(value * (dm_value / 100) / 10000)
}


def _convert_property_value(conversion_formula: str, col_name: str, value: float, dm_value: float):
    if conversion_formula and conversion_formula not in _CONVERSION_FORMULA:
        raise Exception(f"Missing {CONVERSION_COLUMN}: '{conversion_formula}'")

    should_convert = all([
        col_name in FETCH_COLUMNS,
        value != '',
        conversion_formula
    ])
    # if we should convert, but `dryMatter` is missing => no value
    return '' if should_convert and not dm_value else (
        _CONVERSION_FORMULA[conversion_formula](value, dm_value) if should_convert else value
    )


def _property_name_to_column(property_id: str, value_name: str):
    return property_id if value_name == 'Avg' else f"{property_id}-{value_name.lower()}"


def _extract_values(headers: list, cols: list, property: dict, dm_value: float):
    prop_values = {}
    for index, col in enumerate(cols[1:]):
        if col != '' and len(headers) > index + 1 and headers[index + 1] not in SKIP_COLS:
            col_name = headers[index + 1]
            value = _convert_property_value(property.get(CONVERSION_COLUMN), col_name, clean_value(col), dm_value)
            if value != '':
                prop_values[_property_name_to_column(property['term.id'], col_name)] = str(value)
    return prop_values


def _find_property(tables, property_name: str):
    for table in tables:
        rows = table.find_all('tr')
        first_row = rows[0]
        if 'tableheader' in first_row.attrs.get('class', []):
            headers = row_to_list(first_row)
            for row in rows[1:]:
                cols = row_to_list(row)
                prop_name = cols[0]
                if prop_name == property_name:
                    for index, col in enumerate(cols[1:]):
                        if col != '' and len(headers) > index + 1 and headers[index + 1] == 'Avg':
                            return clean_value(col)
    return 0


def _extract_term(term: dict, url: str, properties: list, default_dm_value: float):
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')

    # make sure the url points to a correct page
    has_field_content = len(soup.find_all('div', {'class': 'field-content'}))
    if has_field_content:
        return None

    tables = soup.find_all('table')

    term_id = term['term.id']
    row_dict = {'term.id': term_id}
    dm_value = _find_property(tables, DM_PROP) or default_dm_value
    if not dm_value:
        print('Warning: missing dry matter property and table value for', term['term.id'])

    for table in tables:
        rows = table.find_all('tr')
        first_row = rows[0]
        if 'tableheader' in first_row.attrs.get('class', []):
            headers = row_to_list(first_row)
            for row in rows[1:]:
                cols = row_to_list(row)
                prop_name = cols[0]
                matching_properties = list(filter(lambda v: prop_name in v.get(PROPERTY_NAME, '').split(';'), properties))

                # multiple properties can have the same name, add them all
                for property in matching_properties:
                    value = _extract_values(headers, cols, property, dm_value)
                    if value:
                        row_dict = row_dict | value

    return row_dict, dm_value


def _extract_averages(src_df: pd.DataFrame, lookup_col: str, rows: list):
    new_rows = []

    for index, term in src_df.iterrows():
        term_id = term['term.id']

        try:
            average_term_ids = term[lookup_col.replace('name', 'value')].split(';')
            average_term_ids = [v for v in average_term_ids if non_empty_value(v)]

            if not average_term_ids:
                continue

            print('Averaging term', term_id)

            # [{'term.id': 'term', 'dryMatter': '0.06699', 'dryMatter-min': '0.02871'}]
            values = [row for row in rows + new_rows if row['term.id'] in average_term_ids]
            properties = flatten(map(lambda v: list(v.keys()), values))
            # ['dryMatter']
            properties = list(set([k for k in properties if k != 'term.id']))
            properties_values = {'term.id': term_id}

            for property in properties:
                # property = 'dryMatter'
                prop_values = [
                    float(v) for v in non_empty_list([value.get(property, '') for value in values])
                ]
                avg_value = mean(prop_values) if prop_values else ''

                properties_values = properties_values | {property: avg_value}

            new_rows.append(properties_values)
        except Exception:
            stack = traceback.format_exc()
            print(stack)
            pass

    return new_rows


def _extract(src_file: str):
    src_df = pd.read_excel(src_file)
    properties_df = pd.read_csv('lookups/property.csv', index_col=None)
    properties = properties_df[['term.id', PROPERTY_NAME, CONVERSION_COLUMN]].fillna('').to_dict('records')
    rows = []
    errors = []
    for index, term in src_df.iterrows():
        term_id = term['term.id']

        if args.term_id and term_id not in args.term_id:
            continue

        link = term['term.feedipedia']
        if non_empty_value(link):
            dm_value = default_dm_value(term)
            try:
                row_dict, dm_value = _extract_term(term, link, properties, dm_value)
                if row_dict:
                    rows.append(row_dict)
                else:
                    print(f"{term_id} / {link} does not contain the correct content. Please verify it points to a single factsheet.")
            except ConnectionResetError:
                stack = traceback.format_exc()
                print(f"A network error occured while fetching {link}:\n\t{stack}")
            except Exception:
                stack = traceback.format_exc()
                errors.append(f"Error for {term_id} (link={link}):\n\t{stack}")

    # run averages on some terms
    # 1. Find the lookup column
    lookup_columns = [col for col in src_df if col.startswith('lookups')]
    lookup_col = next((col for col in lookup_columns if str(src_df.loc[0][col]).strip() == AVERAGE_PROPERTIES_COLUMN), None)

    rows = rows + (_extract_averages(src_df, lookup_col, rows) if lookup_col else [])

    dest_file = src_file.replace('.xlsx', '-property-lookup.csv')
    pd.DataFrame.from_records(rows).to_csv(dest_file, index=False, na_rep='')

    if not args.skip_md:
        filepath = src_file.replace('.xlsx', '-property-lookup.md')
        with open(filepath, 'w') as f:
            f.write(f"**Source:** [feedipedia](https://www.feedipedia.org), Auto-generated on {date.today()}.")

    exit_code = '\n'.join(errors) if len(errors) > 0 else 0
    sys.exit(exit_code)


def main():
    _extract(args.input_file)


if __name__ == "__main__":
    main()
