# FAOSTAT data extractor

This script can extract data from the FAO website and format the results as `year:value`.

Mapping between FAO countries and HESTIA Term `@id` is located in `country-mapping.csv`. When running the script, if mappings are missing, warning will be shown. Please update the file to add the missing mappings and re-run the script.

## Usage

1. Run `python scripts/convert_to_lookups.py` first to generate lookup tables
1. Run alls scripts: `./scripts/faostat/run-all.sh` or see examples below for individual files

## Examples

- Generate `averageColdCarcassWeight` for `animalProduct`: `python scripts/faostat/run.py averageColdCarcassWeight animalProduct`
- Generate `area` for `crop`: `python scripts/faostat/run.py faostatArea crop`
- Generate `head` for `animalProduct`: `python scripts/faostat/run.py head animalProduct`
- Generate `head` for `liveAnimal`: `python scripts/faostat/run.py head liveAnimal`
- Generate `price` lookup for `crop`: `python scripts/faostat/run.py price crop Average_price_per_tonne`
- Generate `price` lookup for `animalProduct`: `python scripts/faostat/run.py price animalProduct Average_price_per_tonne`
- Generate `yield` for `crop`: `python scripts/faostat/run.py yield crop`

## Improvements

- Currently the list of regions and years is set manually in the JSON configuration files (see `area` and `year`). This is not ideal as if those evolve we would need to get them again by inspecting the queries made from the page. Also, as some queries are too big we have manually restricted the `year` values to the minimum viable.
