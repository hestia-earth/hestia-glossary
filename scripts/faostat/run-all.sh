#!/bin/sh

python3 scripts/faostat/run.py --type=irrigated
python3 scripts/generate_region_data.py Geographies/region-irrigated-lookup.csv
python3 scripts/faostat/run.py --type=averageColdCarcassWeight --term-type=animalProduct
python3 scripts/generate_region_data.py Geographies/region-animalProduct-animalProductGroupingFAO-averageColdCarcassWeight-lookup.csv AVERAGE
python3 scripts/faostat/run.py --type=faostatArea
python3 scripts/faostat/run.py --type=head --term-type=animalProduct
python3 scripts/generate_region_data.py Geographies/region-animalProduct-animalProductGroupingFAO-head-lookup.csv AVERAGE
python3 scripts/faostat/run.py --type=head --term-type=liveAnimal
python3 scripts/generate_region_data.py Geographies/region-liveAnimal-liveAnimalProductGroupingFAO-head-lookup.csv AVERAGE
python3 scripts/faostat/run.py --type=price --term-type=crop --average-key=Average_price_per_tonne
python3 scripts/generate_region_data.py Geographies/region-crop-cropGroupingFaostatProduction-price-lookup.csv AVERAGE
python3 scripts/faostat/run.py --type=price --term-type=animalProduct --average-key=Average_price_per_tonne
python3 scripts/generate_region_data.py Geographies/region-animalProduct-animalProductGroupingFAO-price-lookup.csv AVERAGE
python3 scripts/faostat/run.py --type=productionQuantity --term-type=crop
python3 scripts/generate_region_data.py Geographies/region-crop-cropGroupingFaostatProduction-productionQuantity-lookup.csv SUM
python3 scripts/faostat/run.py --type=productionQuantity --term-type=animalProduct
python3 scripts/generate_region_data.py Geographies/region-animalProduct-animalProductGroupingFAO-productionQuantity-lookup.csv SUM
python3 scripts/faostat/run.py --type=yield --term-type=crop
python3 scripts/generate_region_data.py Geographies/region-crop-cropGroupingFaostatProduction-yield-lookup.csv
python3 scripts/faostat/run.py --type=areaHarvested --term-type=crop
python3 scripts/generate_region_data.py Geographies/region-crop-cropGroupingFaostatProduction-areaHarvested-lookup.csv SUM
python3 scripts/faostat/run.py --type=pesticidesUsage --term-type=crop
python3 scripts/generate_region_data.py Geographies/region-crop-pesticidesUsage-lookup.csv
python3 scripts/faostat/run.py --type=fertilisersUsage --term-type=inorganicFertiliser
python3 scripts/generate_region_data.py Geographies/region-inorganicFertiliser-fertilisersUsage-lookup.csv
