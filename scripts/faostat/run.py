import os
import traceback
import argparse
import json
from datetime import date
from functools import reduce
from statistics import mean
import urllib
import urllib.parse
import requests
from io import StringIO
import pandas as pd


API_URL = 'https://faostatservices.fao.org/api/v1'
LANG = 'en'
CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
OUTPUT_FOLDER = os.path.join(CURRENT_DIR, '..', '..', 'Geographies')
DEFAULT_PARAMS = {
    "show_codes": "false",
    "show_unit": "false",
    "show_flags": "false",
    "show_notes": "false",
    "null_values": "false",
    "output_type": "csv",
    "datasource": "DB4",
    "year": "1990,1991,1992,1993,1994,1995,1996,1997,1998,1999,2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,2020,2021,2022,2023",
    "item_cs": "FAO"
}
FIX_GROUPING = {
    'Maté leaves': 'Mate leaves'
}
# Some meat products only have value for "(biological)" item. Use as fallback
BIOLOGICAL_FALLBACK = '(biological)'

lookup_filepath = os.path.join(CURRENT_DIR, 'country-mapping.csv')
lookup = pd.read_csv(lookup_filepath, index_col=None)

available_types = [f.replace('.json', '') for f in os.listdir(CURRENT_DIR) if f.endswith('.json')]
available_term_types = ['crop', 'animalProduct', 'liveAnimal', 'inorganicFertiliser']

parser = argparse.ArgumentParser(description='Generating FAOSTAT data')
parser.add_argument('--type', type=str, required=True,
                    choices=available_types,
                    help='The type of data to generate.')
parser.add_argument('--term-type', type=str,
                    choices=available_term_types,
                    help='The termType of data to generate.')
parser.add_argument('--average-key', type=str,
                    help='The name of the key to create averages.')
parser.add_argument('--country-id', type=str,
                    help='Filter by country ID for testing.')
parser.add_argument('--country-name', type=str,
                    help='Filter by country Name for testing.')
parser.add_argument('--save-file', action='store_true',
                    help='Set to store the file for debugging.')
args = parser.parse_args()


def is_emtpy(value): return value is None or str(value) == '-' or str(value) == '' or pd.isna(value)


def non_empty_value(value): return not is_emtpy(value)


def non_empty_list(values): return list(filter(non_empty_value, values))


def _to_code(value: str): return '<code>' + value + '</code>'


def _to_table(values: list[dict]):
    keys = values[0].keys()
    return f"""
    <table>
      <thead>
        <tr>
            {''.join([f"<th>{key}</th>" for key in keys])}
        </tr>
      </thead>
      <tbody>
        <tr>
            {'</tr><tr>'.join([
                ''.join([
                    f"<td>{value.get(key, 'N/A')}</td>" for key in keys
                ]) for value in values
            ])}
        </tr>
      </tbody>
    </table>
    """.strip()


def _load_config(name: str):
    with open(os.path.join(CURRENT_DIR, f"{name}.json"), 'r') as f:
        return json.load(f)


def _get_all_groupings(term_type: str, grouping: str):
    try:
        filepath = os.path.join(CURRENT_DIR, '..', '..', 'lookups', f"{term_type}.csv")
        df = pd.read_csv(filepath)
        return non_empty_list(list(df[grouping].unique()))
    except Exception:
        return []


def _mean(values: list):
    return mean(values) if len(values) > 1 else values[0] if len(values) == 0 else '-'


IGNORE_UNITS = [
    'No/An'
]

# FAOSTAT > Definitions and standards > Units
#
# Unit Name	Description
# An	    Animals
# ha	    Hectares
# 100 g	    hundred Grams
# 100 g/An	hundred Grams per animal
# 100 g/ha	hundred Grams per hectare
# 100 g/t	hundred Grams per tonne
# 100 mg/An	hundred Milligrams per animal
# No	    Number
# No/An	    Number per animal
# 0.1 g/An	tenth Grams per animal
# 1000 An	thousand Animals
# 1000 No	thousand Number
# t	        Tonnes
VALUE_BY_UNIT = {
    '1000 Head': lambda v: v * 1000,
    '1000 An': lambda v: v * 1000,
    '1000 No': lambda v: v * 1000,
    # make sure units are all `hg/An`
    '0.1g/An': lambda v: v / 1000,
    '0.1 g/An': lambda v: v / 1000,
    '100 mg/An': lambda v: v / 1000,
    '100 g/An': lambda v: v,
}


def _parse_value(row: dict):
    value = row.get('Value')
    unit = row.get('Unit')
    return VALUE_BY_UNIT.get(unit, lambda v: v)(value) if unit not in IGNORE_UNITS else None


def _term_id_from_name(name: str):
    value = lookup.loc[lookup['FAOSTAT Name'] == name]
    if not value.empty:
        return value.to_dict('records')[0]['term.id']
    else:
        print('Warning: matching "term.id" not found for', name, 'in', lookup_filepath)
        return None


def _term_id_from_mapping(itemMapping: dict): return lambda val: itemMapping.get(val, val)


def _no_biological_item(item: str): return item.replace(f" {BIOLOGICAL_FALLBACK}", '')


def _group_by_country(itemMapping: dict):
    def exec(group: dict, row: dict):
        country_name = row.get('Area')
        value_year = row.get('Year')
        term_id = _term_id_from_name(country_name)
        value = _parse_value(row)
        grouping = _no_biological_item(_term_id_from_mapping(itemMapping)(row.get('Item')))

        if all([
            term_id,
            not is_emtpy(value),
            not args.country_id or args.country_id == term_id
        ]):
            group[term_id] = group.get(term_id, {})
            group[term_id][grouping] = group[term_id].get(grouping, {})
            if value_year not in group[term_id][grouping]:
                group[term_id][grouping][value_year] = value

        return group
    return exec


def _process_result(
    df: pd.DataFrame, file_suffix: str, groupings: list = [], itemMapping: dict = {},
    average_key: str = None, element_code: str = None
):
    years = sorted(list(df['Year'].unique()))

    rows = df.to_dict('records')
    # filter results by "Element Code"
    if element_code:
        rows = [r for r in rows if str(r.get('Element Code')) == str(element_code)]

    data = reduce(_group_by_country(itemMapping), rows, {})

    records = []
    for term_id, value in data.items():
        record = {'term.id': term_id}
        for ggroup in groupings:
            grouping = _no_biological_item(ggroup)
            year_values = value.get(grouping)
            if year_values:
                grouping_values = list(map(lambda year: f"{year}:{year_values.get(year, '-')}", years))
                grouping_values.extend([f"{average_key}:{_mean(year_values.values())}"] if average_key else [])
                record[grouping] = ';'.join(grouping_values)
            else:
                record[grouping] = '-'

        records.append(record)

    filepath = os.path.join(OUTPUT_FOLDER, f"{file_suffix}.csv")
    pd.DataFrame.from_records(records, index=['term.id']).to_csv(filepath)


def _get_countries(config: dict):
    url = f"{API_URL}/{LANG}/codes/countries/{config.get('url')}/?show_lists=true&datasource=DB3"
    resp = requests.get(url)
    try:
        items = resp.json().get('data', [])
        return [item for item in items if not args.country_name or args.country_name == item.get('label')]
    except requests.exceptions.JSONDecodeError as e:
        print(str(e))
        print(resp)
        raise e


def _get_elements(config: dict):
    url = f"{API_URL}/{LANG}/codes/elements/{config.get('url')}/?show_lists=true&datasource=DB3"
    items = requests.get(url).json().get('data', [])
    return items


def _get_elements_definitions(config: dict):
    url = f"{API_URL}/{LANG}/definitions/domain/{config.get('url')}/element?output_type=objects"
    items = requests.get(url).json().get('data', [])
    return reduce(lambda p, c: p | {c.get('Element'): c}, items, {})


def _format_item_label(label: str): return label.replace('-', '').strip()


def _get_items(config: dict):
    # filter by available data if any
    try:
        url = f"{API_URL}/{LANG}/codes/items/{config.get('url')}/?show_lists=true&datasource=DB3"
        items = requests.get(url).json().get('data', [])
        # remove the --- used for subvalues
        return [i | {'label': _format_item_label(i.get('label'))} for i in items]
    except requests.exceptions.JSONDecodeError:
        return []


def _get_items_definitions(config: dict):
    url = f"{API_URL}/{LANG}/definitions/domain/{config.get('url')}/item?output_type=objects"
    items = requests.get(url).json().get('data', [])
    return reduce(lambda p, c: p | {c.get('Item'): c}, items, {})


def _save_file(df: pd.DataFrame, file_suffix: str):
    filepath = f"{file_suffix}-temp.csv"
    print('Saving raw data under', filepath)
    df.to_csv(filepath)


def main():
    config = _load_config(args.type)
    default_config = config.get('default', {})
    key_config = config[args.term_type] if args.term_type else default_config
    grouping = key_config.get('grouping', '')
    if 'grouping' in key_config:
        del key_config['grouping']

    itemMapping = key_config.get('itemMapping', {})
    if 'itemMapping' in key_config:
        del key_config['itemMapping']

    term_type_groupings = _get_all_groupings(args.term_type, grouping) if args.term_type else []

    # filter by available data if any
    all_items = _get_items(config)
    available_items = [
        item for item in all_items if any([
            item.get('label') in term_type_groupings,
            _no_biological_item(item.get('label')) in term_type_groupings
        ])
    ]

    all_countries = _get_countries(config)

    # map element label in config to codes
    all_elements = _get_elements(config)
    selected_element = key_config.get('element', default_config.get('element'))
    element = next((i for i in all_elements if i.get('label') == selected_element or i.get('code') == selected_element), None)

    if not element:
        raise Exception(f"No matching element found with value '{selected_element}'")

    config_params = {
        **DEFAULT_PARAMS,
        **default_config,
        **key_config,
        'area_cs': 'M49',
        'area': ','.join(map(lambda i: i.get('code'), all_countries))
    } | (
        {'item': ','.join(map(lambda i: i.get('code'), available_items))} if len(available_items) > 0 else {}
    ) | (
        {'element': element.get('code')}
    )
    url_params = urllib.parse.urlencode(config_params)
    url = f"{API_URL}/{LANG}/data/{config.get('url')}?{url_params}"
    print('Query:', url)
    result = requests.get(url).text
    df = pd.read_csv(StringIO(result), encoding='utf8')

    file_suffix = '-'.join(non_empty_list(['region', args.term_type, grouping, args.type, 'lookup']))

    try:
        if args.save_file:
            _save_file(df, file_suffix)

        # get the groupings from the lookup file (if exists), and merge with groupings from file
        groupings = sorted(set(
            [i.get('label') for i in available_items] if len(available_items) > 0 else (
                term_type_groupings +
                list(map(_term_id_from_mapping(itemMapping), df['Item'].unique()))
            )
        ))
        # fix grouping with encoding issues
        groupings = [FIX_GROUPING.get(v, v) for v in groupings]

        # note: to use `elementCode`, make sure `default.show_codes=true`
        element_code = config.get('elementCode')

        element_definition = _get_elements_definitions(config).get(element.get('label'), {})

        _process_result(df, file_suffix, groupings,
                        itemMapping=itemMapping, average_key=args.average_key, element_code=element_code)

        filepath = os.path.join(OUTPUT_FOLDER, f"{file_suffix}.md")

        items = config_params.get('item').split(',')
        item_definitions = _get_items_definitions(config)

        lines = [
            f"**Source:** [FAOSTAT (2024)](https://www.fao.org/faostat/{LANG}/#data/{config.get('url')}), Accessed {date.today()}.",
            '',
            'Data was generated by applying the following filters:'
            '',
            '* Countries: All'
            '',
            '* Element:'
            '',
            _to_table([
                {
                    'Name': element.get('label'),
                    'Unit': element_definition.get('Unit'),
                    'Definition': element_definition.get('Description')
                }
            ]),
            '',
            '* Items:'
            '',
            _to_table([
                {
                    'Name': i.get('label'),
                    'Definition': item_definitions.get(i.get('label'), {}).get('Description')
                } for i in all_items if i.get('code') in items
            ]),
            '',
            f"* Years: {', '.join(map(_to_code, config_params.get('year').split(',')))}",
            ''
        ]
        with open(filepath, 'w') as f:
            f.writelines('\n'.join(lines))
    except Exception:
        stack = traceback.format_exc()
        print('An error occured while processing the file:')
        print(stack)
        _save_file(df, file_suffix)


if __name__ == "__main__":
    main()
