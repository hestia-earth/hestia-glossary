# script to add lookup data on the region file from gadm/lookup.csv
# install python packages: pandas openpyxl
from os.path import dirname, join, abspath
import json
from functools import reduce
import pandas as pd


CURRENT_DIR = dirname(abspath(__file__))

GADM_DIR = join(CURRENT_DIR, '..', 'gadm')
lookup = pd.read_csv(join(GADM_DIR, 'lookup.csv'), index_col=None)
LOOKUP_COLUMN = 'gadmId'
LOOKUP_COLUMNS = [col for col in lookup if col != LOOKUP_COLUMN]

SRC_DIR = join(CURRENT_DIR, '..', 'Geographies')
filename = 'region'
region = pd.read_excel(join(SRC_DIR, f"{filename}.xlsx"))

region['-'] = ''

for index, row in lookup.iterrows():
    gadmId = row[LOOKUP_COLUMN]
    region_data = region[region['term.id'] == gadmId]
    if not region_data.empty:
        data = row.to_dict()
        content = reduce(lambda x, y: {**x, **{y: data[y]}} if data[y] != '-' else x, LOOKUP_COLUMNS, {})
        data = json.dumps(content) if bool(content) else ''
        region.at[region_data.index, '-'] = data

region.to_excel(join(SRC_DIR, f"{filename}-lookup.xlsx"), index=None, header=True)
