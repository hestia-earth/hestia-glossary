const { readLookup, writeJson, listLookups } = require('./generate-utils');

const lookupColumn = 'generateImpactAssessment';
const outputFile = 'impactAssessment.json';

const getTermIds = async file => {
  const data = await readLookup(file);
  return data
    .filter(val => lookupColumn in val && val[lookupColumn].toLowerCase() === 'false')
    .map(val => val.term.id);
};

const run = async () => {
  const files = listLookups();
  const skipTermIds = (await Promise.all(files.map(getTermIds))).flat();
  const content = { skipTermIds };
  writeJson(outputFile, content);
};

run().then(() => process.exit(0)).catch(err => {
  console.error(err);
  process.exit(1);
});
