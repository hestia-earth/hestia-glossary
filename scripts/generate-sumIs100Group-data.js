const { readLookup, writeJson, listLookups } = require("./generate-utils");

const lookupColumn = "sumIs100Group";
const outputFile = `${lookupColumn}.json`;

const fileData = async (file) => {
  const data = await readLookup(file);
  return data
    .filter((val) => lookupColumn in val)
    .map((val) => ({ id: val.term.id, value: val[lookupColumn] }));
};

const run = async () => {
  const files = listLookups();
  const data = (await Promise.all(files.map(fileData))).flat().reduce(
    (prev, { id, value }) => ({
      ...prev,
      [id]: value,
    }),
    {}
  );
  writeJson(outputFile, data);
};

run()
  .then(() => process.exit(0))
  .catch((err) => {
    console.error(err);
    process.exit(1);
  });
