const { readLookup, writeJson, listLookups } = require('./generate-utils');

const lookupColumns = [
  'siteTypesAllowed',
  'productTermIdsAllowed', 'productTermTypesAllowed',
  'inputTermIdsAllowed', 'inputTermTypesAllowed',
  'typesAllowed'
];
const outputFile = 'term-restrictions.json';

const fileData = async file => {
  const data = await readLookup(file);
  const restrictedLookupColumn = lookupColumns.find(col => file.includes(col));
  return restrictedLookupColumn
    ? data
      .map(({ term, ...val }) => ({
        id: term.id,
        value: Object.fromEntries(Object.entries(val).map(([model, value]) => [model, { [restrictedLookupColumn]: value.split(';').filter(Boolean) }]))
      }))
    : data
      .filter(val => lookupColumns.some(col => col in val))
      .map(val => ({
        id: val.term.id,
        value: Object.fromEntries(lookupColumns.filter(col => col in val).map(col => [col, val[col].split(';').filter(Boolean)]))
      }));
};

const run = async () => {
  const files = listLookups();
  const data = (await Promise.all(files.map(fileData))).flat().reduce((prev, { id, value }) => ({
    ...prev, [id]: { ...(prev[id] || {}), ...value }
  }), {});
  writeJson(outputFile, data);
};

run().then(() => process.exit(0)).catch(err => {
  console.error(err);
  process.exit(1);
});
