# script to generate area harvested 20 year expansion data grouped by countries
# reads Geographies/region-crop-cropGroupingFaostatProduction-areaHarvested-lookup.csv for delta of date - 20 years
# previously.
# example: `python scripts/generate_area_harvested_20_year_expansion_lookup.py`
# Output: `Geographies/region-crop-cropGroupingFaostatProduction-areaHarvestedUpTo20YearExpansion-lookup.csv`
from os.path import dirname, join, abspath

from generate_value_up_to_20_year_expansion_lookup import write_files

CURRENT_DIR = dirname(abspath(__file__))
GEO_DIR = join(CURRENT_DIR, '..', 'Geographies')
INPUT_FILE = "region-crop-cropGroupingFaostatProduction-areaHarvested-lookup.csv"
OUTPUT_FILE = "region-crop-cropGroupingFaostatProduction-areaHarvestedUpTo20YearExpansion-lookup.csv"
FILEPATH = join(GEO_DIR, INPUT_FILE)
OUTPUT_FILEPATH = join(GEO_DIR, OUTPUT_FILE)


def main():
    write_files(filepath=FILEPATH, output_filepath=OUTPUT_FILEPATH)


if __name__ == "__main__":
    main()
