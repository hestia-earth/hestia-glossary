import os
import sys
import time
from io import StringIO
import pandas as pd
from unidiff import PatchSet


def current_time_ms(): return str(int(time.time() * 1000))


def _parse_line(line: str):
    # e.g. `-termId,"term name",units`
    try:
        df = pd.read_table(StringIO(line[1:]), sep=',')
        return list(df.columns.values)
    except Exception:
        return line[1:].split(',')


def escape_field(value):
    val = str(value).replace('"', '""').replace('\r', '').replace('\n', '')
    return f"\"{val}\"" if ',' in val or '"' in val else val


def main(args: list):
    diff_file = args[0]
    dest_folder = args[1]
    patches = PatchSet.from_filename(diff_file)
    print('nb files', len(patches))

    term_ids = []
    term_names = {}
    term_types = {}
    for patch in patches:
        for hunk in patch:
            for line in hunk:
                if line.is_removed:
                    old_term_id, old_term_name, *values = _parse_line(str(line))
                    if old_term_id != 'term.id':
                        term_type = values[-1]
                        term_ids.append(old_term_id)
                        term_names[old_term_id] = {'old': old_term_name}
                        term_types[old_term_id] = {'old': term_type}

    # make sure we skip updated terms
    for patch in patches:
        for hunk in patch:
            for line in hunk:
                if line.is_added:
                    new_term_id, new_term_name, *values = _parse_line(str(line))
                    new_term_type = values[-1]
                    old_term_type = term_types.get(new_term_id, {}).get('old', '')

                    # term id was removed and added - skip only if same termType
                    if new_term_id in term_ids and new_term_type == old_term_type:
                        term_ids.remove(new_term_id)

                    # handle old/new term name
                    old_term_name = term_names.get(new_term_id, {}).get('old')
                    if old_term_name:
                        # term name has not been changed
                        if old_term_name == new_term_name:
                            del term_names[new_term_id]
                        else:
                            term_names[new_term_id]['new'] = new_term_name

                    # handle old/new termType
                    if old_term_type:
                        # term name has not been changed
                        if old_term_type == new_term_type:
                            del term_types[new_term_id]
                        else:
                            term_types[new_term_id]['new'] = new_term_type

    print('nb term ids', len(term_ids))
    if len(term_ids) > 0 or term_names or term_types:
        filepath = os.path.join(dest_folder, 'migrations', f"{current_time_ms()}.csv")
        open(filepath, 'w+').writelines('oldTermId,newTermId,instructions,oldTermName,newTermName,oldTermType,newTermType\n')

        for term_id in term_ids:
            name_mapping = term_names.get(term_id, {})
            term_type_mapping = term_types.get(term_id, {})
            open(filepath, 'a+').writelines(','.join([
                escape_field(term_id),  # oldTermId
                escape_field(term_id) if term_type_mapping.get('new') else '',  # newTermId
                '',  # instructions
                escape_field(name_mapping.get('old', '')),  # oldTermName
                f"{escape_field(name_mapping.get('new', ''))}",  # newTermName
                escape_field(term_type_mapping.get('old', '')),  # oldTermType
                f"{escape_field(term_type_mapping.get('new', ''))}\n"  # newTermType
            ]))

        # add all name changes not linked to a change in term id
        for term_id, name_mapping in term_names.items():
            if term_id not in term_ids:
                open(filepath, 'a+').writelines(','.join([
                    '',  # oldTermId
                    '',  # newTermId
                    '',  # instructions
                    escape_field(name_mapping.get('old')),  # oldTermName
                    f"{escape_field(name_mapping.get('new', ''))}",  # newTermName
                    '',  # oldTermType
                    '\n'  # newTermType
                ]))

if __name__ == "__main__":
    main(sys.argv[1:])
