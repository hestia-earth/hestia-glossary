# script to generate region data grouped by countries
# reads gadm/additional-regions.csv and Geographies/region.xlsx to determine hierarchy between countries
# example: `python scripts/generate_region_data.py Geographies/region-crop-cropGroupingFaostatProduction-yield-lookup.csv`
import sys
import os
import traceback
from enum import Enum
from datetime import datetime
from os.path import dirname, join, abspath, exists
import json
from functools import reduce, cmp_to_key
import pandas as pd

CURRENT_DIR = dirname(abspath(__file__))
DATA_DIR = join(CURRENT_DIR, 'data')
GADM_DIR = join(CURRENT_DIR, '..', 'gadm')
GEO_DIR = join(CURRENT_DIR, '..', 'Geographies')
AREA_COLUMN = 'term.area'  # last column
_FILTER_COLUMNS = ['term.id', 'term.subClassOf.0.id', AREA_COLUMN]


class Mode(Enum):
    AVERAGE = 'average'
    AVERAGE_AREA = 'average area'
    SUM = 'sum'


def is_emtpy(value): return value is None or str(value) == '-' or str(value) == '' or pd.isna(value)


def non_empty_list(values: list): return list(filter(lambda v: not is_emtpy(v), values))


def get_regions():
    regions = []

    os.makedirs(DATA_DIR, exist_ok=True)
    filepath = join(DATA_DIR, 'regions.json')
    if exists(filepath):
        with open(filepath, 'r') as f:
            regions = json.load(f)
    else:
        with open(join(GADM_DIR, 'headers.txt'), 'r') as f:
            headers = non_empty_list(f.read().split('\n'))

        df = pd.read_csv(join(GADM_DIR, 'additional-regions.csv'), index_col=None, header=None, names=headers)
        for _index, row in df[_FILTER_COLUMNS].iterrows():
            if row['term.id'].startswith('region-') and not is_emtpy(row[AREA_COLUMN]):
                regions.append(row.to_dict())

        with open(filepath, 'w') as f:
            f.write(json.dumps(regions, indent=2))

    return regions


def get_countries():
    filepath = join(DATA_DIR, 'countries.json')
    if exists(filepath):
        with open(filepath, 'r') as f:
            return json.load(f)

    df = pd.read_excel(join(GEO_DIR, 'region.xlsx'), index_col=None)
    countries = []
    for _index, row in df[df['term.id'].str.match(r'GADM-[A-Z]{3}$')][_FILTER_COLUMNS].iterrows():
        id = row['term.subClassOf.0.id']
        if isinstance(id, str) and id.startswith('region-'):
            countries.append(row.to_dict())

    with open(filepath, 'w') as f:
        f.write(json.dumps(countries, indent=2))

    return countries


def _group_countries(prev, curr):
    id = curr['term.subClassOf.0.id']
    prev[id] = prev.get(id, []) + [curr['term.id']]
    return prev


def get_grouped_regions(regions: list):
    countries = get_countries()
    return reduce(_group_countries, regions + countries, {}), countries


def _extract_grouped_data(data: str, key: str):
    return reduce(lambda prev, curr: {
        **prev,
        **{curr.split(':')[0]: curr.split(':')[1]}
    }, data.split(';'), {})[key] if data is not None and len(data) > 1 else None


def _extract_value(value: str, date: str):
    try:
        return float(_extract_grouped_data(value, date))
    except Exception:
        return None


def get_value(values: dict, date: str, countries: list, mode: Mode, column):
    try:
        results = []
        for term_id, value in values.items():
            country = next((c for c in countries if c['term.id'] == term_id))
            val = _extract_value(value, date)
            if val is not None:
                results.append(tuple((val, country[AREA_COLUMN])))

        if len(results) > 0:
            if mode == Mode.AVERAGE_AREA:
                total_weight = sum(weight for _v, weight in results)
                weighted_value = [value * weight for value, weight in results]
                return round(sum(weighted_value) / (total_weight if total_weight != 0 else 1), 2)
            elif mode == Mode.AVERAGE:
                total_value = sum(value for value, _w in results)
                return round(total_value / len(results), 2)
            elif mode == Mode.SUM:
                total_value = sum(value for value, _w in results)
                return round(total_value, 2)

        return None
    except Exception:
        stack = traceback.format_exc()
        print(stack)
        return None


def append_region_value(df: pd.DataFrame, region: dict, groups: dict, countries: list, mode = Mode.AVERAGE_AREA):
    name = region['term.id']
    # skip regions with no children
    if not name in groups:
        return df

    parent_ids = []
    for id in groups[name]:
        if id in df.index:
            parent_ids.append(id)

    print('processing', name, 'using', parent_ids)
    region_value = {}
    filtered_df = df.loc[parent_ids]

    for column in filtered_df:
        values = filtered_df[column].to_dict()
        new_value = []

        for x in range(1961, datetime.now().year):
            value = get_value(values, str(x), countries, mode, column)
            if value is not None:
                new_value.append(':'.join([str(x), str(value)]))

        region_value[column] = ';'.join(new_value) if len(new_value) > 0 else '-'

    return df.append(pd.Series(region_value, name=name))


def _sort_region(a: dict, b: dict):
    if b['term.id'] == 'region-world':
        return -1
    if a['term.id'] == 'region-world':  # region-world is last
        return 1
    elif a['term.subClassOf.0.id'] == 'region-world':  # then regions below world after
        return 0
    else:  # finally the other ones
        return -1


def main(args):
    regions = sorted(get_regions(), key=cmp_to_key(_sort_region))

    groups, countries = get_grouped_regions(regions)

    file = args[0]
    mode = Mode.AVERAGE_AREA if len(args) == 1 else Mode[args[1]]

    df = pd.read_csv(file, index_col='term.id')

    for region in regions:
        df = append_region_value(df, region, groups, regions + countries, mode)

    df.to_csv(file, index=True)


if __name__ == "__main__":
    main(sys.argv[1:])
