# script to generate 20 year expansion data grouped by countries
# reads Geographies/region-crop-cropGroupingFaostatProduction-areaHarvested-lookup.csv for delta of date - 20 years
# previously.
# example: Do not call this file directly.
# Output: `Geographies/*UpTo20YearExpansion-lookup.csv`
import csv
from os.path import dirname, join, abspath

from functools import reduce
import pandas as pd
from hestia_earth.utils.lookup import extract_grouped_data_closest_date, _is_missing_value
from hestia_earth.utils.tools import safe_parse_float


MISSING_VALUE = "-"
MISSING = -99999
MINIMUM_YEARS_SEPARATION = 5


def is_empty(value): return value is None or str(value) == MISSING_VALUE or str(value) == "" or pd.isna(value)


def non_empty_list(values: list): return list(filter(lambda v: not is_empty(v), values))


def _extract_grouped_data(data: str, key: str):
    return reduce(lambda prev, curr: {
        **prev,
        **{curr.split(':')[0]: curr.split(':')[1]}
    }, data.split(';'), {})[key] if data is not None and len(data) > 1 else None


def _get_closest_year(data: str, year: int) -> int:
    # Based on hestia_earth.utils.lookup.extract_grouped_data_closest_date
    data_by_date = reduce(
        lambda prev, curr: {
            **prev,
            **{curr.split(':')[0]: curr.split(':')[1]}
        } if len(curr) > 0 and not _is_missing_value(curr.split(':')[1]) else prev,
        data.split(';'),
        {}
    ) if data is not None and isinstance(data, str) and len(data) > 1 else {}
    dist_years = list(data_by_date.keys())
    return min(dist_years, key=lambda x: abs(int(x) - year)) if len(dist_years) > 0 else None


def _extract_value(value: str, date: str):
    try:
        return float(_extract_grouped_data(value, date))
    except Exception:
        return None


def _get_year_value_pairs(data: str) -> dict[int, str]:
    return {int(pair.split(":")[0]): pair.split(":")[1] for pair in data.split(";")}


def _has_valid_earlier_dates(target_year: int, years_and_values: dict) -> bool:
    return any([year < target_year and not _is_missing_value(value) for year, value in years_and_values.items()])


def _scale_to_20_years(target_year: int, earlier_year: int, area_difference: float) -> float:
    return round(area_difference * 20 / (target_year - earlier_year), 1)


def _get_20_year_expansion(data: str) -> str:
    if is_empty(data):
        return MISSING_VALUE
    year_value_pairs = _get_year_value_pairs(data)
    result = dict()
    for year, area in year_value_pairs.items():
        earlier_year = int(_get_closest_year(data=data, year=year - 20))
        if (not _has_valid_earlier_dates(target_year=year, years_and_values=year_value_pairs)
            or _is_missing_value(area)
            or (year - earlier_year) < MINIMUM_YEARS_SEPARATION
        ):
            continue
        area_20_years_ago_or_nearest = safe_parse_float(extract_grouped_data_closest_date(data=data, year=earlier_year))

        result[year] = _scale_to_20_years(
            target_year=year,
            earlier_year=earlier_year,
            area_difference=safe_parse_float(area) - area_20_years_ago_or_nearest
        )

    return ";".join([f"{y}:{a}" for y, a in result.items()])


def _process_row(row: dict) -> dict:
    keys_to_skip = {"term.id"}
    return {k: _get_20_year_expansion(v) if k not in keys_to_skip else v for k, v in row.items()}


def write_files(filepath: str, output_filepath: str):
    with open(filepath, "r") as csv_input:
        reader = csv.DictReader(csv_input)
        output_rows = [_process_row(row) for row in list(reader)]

    with open(output_filepath, "w") as csv_output:
        writer = csv.DictWriter(csv_output, fieldnames=output_rows[0].keys())
        writer.writeheader()
        writer.writerows(output_rows)
