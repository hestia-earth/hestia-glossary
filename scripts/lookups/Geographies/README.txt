This tool will read all files in ./Geographies/ that look like `region-emission-[SOMETHING]-lookup.csv` and
check their columns and terms against ./Emissions & Resource Use/emission.xlsx.
Any missing columns will be added and filled with blank `-` values.

It is probably best to edit these files manually and only use this tool when mass importing terms

==============================================================
WARNING! This script WILL CHANGE THE FORMAT OF EXISTING FILES
==============================================================
You _MUST_ run `git diff` on the files to see any unwanted changes and remove them!

Known bugs:
    - Columns with no names will be renamed as `Unnamed: [COLUMN NUMBER]
    - The number `0` will be renamed to `0.0`
    - Scientific style floating point numbers like `1.86E-14` will turn lowercase like `1.86e-14`

## Example
```
checking region-emission-FreshwaterEutrophicationDamageToFreshwaterEcosystemsLCImpactCF-lookup.csv
checking region-emission-OzoneFormationDamageToHumanHealthLCImpactCF-lookup.csv
checking region-emission-OzoneFormationDamageToTerrestrialEcosystemsLCImpactCF-lookup.csv
checking region-emission-ParticulateMatterFormationAllEffectsDamageToHumanHealthLCImpactCF-lookup.csv
checking region-emission-ParticulateMatterFormationCertainEffectsDamageToHumanHealthLCImpactCF-lookup.csv
Updated region-emission-ParticulateMatterFormationCertainEffectsDamageToHumanHealthLCImpactCF-lookup.csv
checking region-emission-TerrestrialAcidificationDamageToTerrestrialEcosystemsLCImpactCF-lookup.csv
checking region-emission-MarineEutrophicationDamageToMarineEcosystemsLCImpactCF-lookup.csv
=======
WARNING!
=======
Unwanted changes added to file! You must now run `git diff` and ONLY commit the edits you want!
```