import pandas as pd
import os


def update_lookup_files(main_file, lookup_dir):
    # Read the main emission Excel file
    main_df = pd.read_excel(main_file, sheet_name='emission')

    # Get all emission types from the main file
    all_emissions = main_df['term.id'].unique()

    # Iterate through all CSV files in the lookup directory
    for filename in os.listdir(lookup_dir):
        if filename.startswith('region-emission-') and filename.endswith('-lookup.csv'):
            print(f"checking {filename}")

            filepath = os.path.join(lookup_dir, filename)

            # Read the lookup CSV file
            lookup_df = pd.read_csv(filepath)

            # Get the emission column name (might vary between files)
            emission_col_terms_only = [col for col in lookup_df.columns][1:]

            # Add missing emissions to the lookup file
            missing_emissions = set(all_emissions) - set(emission_col_terms_only)
            for emission in missing_emissions:
                lookup_df[emission] = ["-" for i in range(0,lookup_df.shape[0])]

            # Remove extra emissions from the lookup file
            # extra_emissions = set(emission_col_terms_only) - set(all_emissions)
            # lookup_df = lookup_df[~lookup_df[emission_col].isin(extra_emissions)] # noga

            # Save the updated lookup file
            if missing_emissions:
                lookup_df.to_csv(filepath, index=False)
                print(f"Updated {filename}")


# Usage
main_file = './Emissions & Resource Use/emission.xlsx'
lookup_dir = './Geographies/'
print(f"This tool will read all files in {lookup_dir} that look like `region-emission-[SOMETHING]-lookup.csv` and \n"
      f"check their columns and terms against {main_file}.\n"
      f"Any missing columns will be added and filled with blank `-` values.\n"
      f"==============================================================\n"
      f"WARNING! This script WILL CHANGE THE FORMAT OF EXISTING FILES\n"
      f"==============================================================\n"
      f"You _MUST_ run `git diff` on the files to see any unwanted changes and remove them!\n"
      f"\n"
      f"Known bugs:\n"
      f"    - Columns with no names will be renamed as `Unnamed: [COLUMN NUMBER]\n"
      f"    - The number `0` will be renamed to `0.0`\n"
      f"    - Scientific style floating point numbers like `1.86E-14` will turn lowercase like `1.86e-14`\n"
      )
update_lookup_files(main_file, lookup_dir)
print("=======")
print("WARNING!")
print("=======")
print("Unwanted changes added to file! You must now run `git diff` and ONLY commit the edits you want!")