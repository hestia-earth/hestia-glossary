# merge "property-default" with "property-lookup"
import argparse
import os
import pandas as pd
from functools import reduce
from concurrent.futures import ThreadPoolExecutor


SUFFIX = '-property-default-lookup.csv'
BASE_DIR = './'
DEST_DIR = 'lookups'
EXCLUDED_FOLDERS = ['csv', 'data', 'envs', 'scripts']


parser = argparse.ArgumentParser(description='Merge property lookup files together.')
parser.add_argument('--folder', type=str, nargs='+', default=[],
                    help='Search in folders.')
args = parser.parse_args()


def mkdirs(dest: str):
    os.makedirs(dest, exist_ok=True)


def flatten(values: list): return list(reduce(lambda x, y: x + (y if isinstance(y, list) else [y]), values, []))


def handle_lookup(folder: str, filename: str):
    src_filepath = os.path.join(folder, filename)
    dest_filepath = os.path.join(DEST_DIR, filename.replace(SUFFIX, '-property.csv'))

    src_df = pd.read_csv(src_filepath, index_col=0)
    # merge existing lookup with current one
    dest_df = pd.read_csv(dest_filepath, index_col=0) if os.path.isfile(dest_filepath) else None
    print('Merging', src_filepath, 'and', dest_filepath)
    df = src_df.append(dest_df) if dest_df is not None else src_df
    df.to_csv(dest_filepath)


def handle_file(data: dict): return handle_lookup(data.get('folder'), data.get('filename'))


def convert_folder(folder: str):
    folderpath = os.path.join(BASE_DIR, folder)
    if os.path.isdir(folderpath):
        files = list(filter(lambda f: f.endswith(SUFFIX), os.listdir(folderpath)))
        return list(map(lambda f: {'folder': folderpath, 'filename': f}, files))
    return []


def main():
    mkdirs(DEST_DIR)

    folders = args.folder or  list(filter(lambda f: f not in EXCLUDED_FOLDERS, os.listdir(BASE_DIR)))
    files = flatten(list(map(convert_folder, folders)))

    with ThreadPoolExecutor() as executor:
        return list(executor.map(handle_file, files))


if __name__ == "__main__":
    main()
