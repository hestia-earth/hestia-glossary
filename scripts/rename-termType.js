require('dotenv').config();
const axios = require('axios');

const [oldTermType, newTermType] = process.argv.slice(2);

const API_URL = process.env.API_URL;
const JSON_LD_API_URL = process.env.JSON_LD_API_URL;
const JSON_LD_API_KEY = process.env.JSON_LD_API_KEY;
const limit = 100;

const findTerms = async () => {
  const { data: { results } } = await axios.post(`${API_URL}/search`, {
    limit,
    fields: ['@type', '@id'],
    query: {
      bool: {
        must: [
          {
            match: { '@type': 'Term' }
          },
          {
            match: { termType: oldTermType }
          }
        ]
      }
    }
  });
  return results;
};

const replaceTermType = async (term) => {
  const data = (await axios.get(`${API_URL}/terms/${term['@id']}`)).data;
  return { ...data, termType: newTermType };
};

const updateNodes = (nodes) => axios.put(`${JSON_LD_API_URL}/node`, nodes, {
  headers: { 'x-api-key': JSON_LD_API_KEY }
});

const updateTerms = async () => {
  const terms = await findTerms();
  console.log('Updating', terms.length, 'terms.');
  const data = await Promise.all(terms.map(replaceTermType));
  await updateNodes(data);
  return terms.length === limit ? await updateTerms() : null;
};

const run = () => updateTerms();

run().then(() => process.exit(0)).catch(err => {
  console.error(err);
  process.exit(1);
});
